package net.client.by.lock.f;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Base64;
import java.util.Map;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/* compiled from: MyApp */
public class q implements SharedPreferences {
    private static final char[] c = "INSERT A RANDOM PASSWORD HERE".toCharArray();
    protected SharedPreferences a;
    protected Context b;

    public q(Context context) {
        this.a = context.getSharedPreferences("AppPrefFile", 0);
        this.b = context;
    }

    /* renamed from: a */
    public r edit() {
        return new r(this);
    }

    @Override // android.content.SharedPreferences
    public Map getAll() {
        throw new UnsupportedOperationException();
    }

    public boolean getBoolean(String str, boolean z) {
        String string = this.a.getString(str, null);
        return string != null ? Boolean.parseBoolean(b(string)) : z;
    }

    public float getFloat(String str, float f) {
        String string = this.a.getString(str, null);
        return string != null ? Float.parseFloat(b(string)) : f;
    }

    public int getInt(String str, int i) {
        String string = this.a.getString(str, null);
        return string != null ? Integer.parseInt(b(string)) : i;
    }

    public long getLong(String str, long j) {
        String string = this.a.getString(str, null);
        return string != null ? Long.parseLong(b(string)) : j;
    }

    public String getString(String str, String str2) {
        String string = this.a.getString(str, null);
        return string != null ? b(string) : str2;
    }

    public String a(String str, String str2, String str3) {
        String string = this.a.getString(str, null);
        return string != null ? b(string, str3) : str2;
    }

    public boolean contains(String str) {
        return this.a.contains(str);
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        this.a.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        this.a.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        return a(str, "");
    }

    /* access modifiers changed from: protected */
    public String a(String str, String str2) {
        byte[] bArr;
        String str3 = String.valueOf(new String(c)) + str2;
        if (str != null) {
            try {
                bArr = str.getBytes("utf-8");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            bArr = new byte[0];
        }
        SecretKey generateSecret = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(new PBEKeySpec(str3.toCharArray()));
        Cipher instance = Cipher.getInstance("PBEWithMD5AndDES");
        instance.init(1, generateSecret, new PBEParameterSpec(Settings.Secure.getString(this.b.getContentResolver(), "android_id").getBytes("utf-8"), 20));
        return new String(Base64.encode(instance.doFinal(bArr), 2), "utf-8");
    }

    /* access modifiers changed from: protected */
    public String b(String str) {
        return b(str, "");
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        byte[] bArr;
        String str3 = String.valueOf(new String(c)) + str2;
        if (str != null) {
            try {
                bArr = Base64.decode(str, 0);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            bArr = new byte[0];
        }
        SecretKey generateSecret = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(new PBEKeySpec(str3.toCharArray()));
        Cipher instance = Cipher.getInstance("PBEWithMD5AndDES");
        instance.init(2, generateSecret, new PBEParameterSpec(Settings.Secure.getString(this.b.getContentResolver(), "android_id").getBytes("utf-8"), 20));
        return new String(instance.doFinal(bArr), "utf-8");
    }

    @Override // android.content.SharedPreferences
    public Set getStringSet(String str, Set set) {
        throw new UnsupportedOperationException();
    }
}
