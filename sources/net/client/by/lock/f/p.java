package net.client.by.lock.f;

import net.client.by.lock.a;

/* compiled from: MyApp */
public class p {
    private static String H() {
        return a.a;
    }

    public static String a() {
        return String.valueOf(H()) + "/Login";
    }

    public static String b() {
        return String.valueOf(H()) + "/Logout";
    }

    public static String c() {
        return String.valueOf(H()) + "/UpdatePublicMessage";
    }

    public static String d() {
        return String.valueOf(H()) + "/SendChat";
    }

    public static String e() {
        return String.valueOf(H()) + "/ReceiveChat";
    }

    public static String f() {
        return String.valueOf(H()) + "/GetRosterEvent";
    }

    public static String g() {
        return String.valueOf(H()) + "/AddFriendToRoster";
    }

    public static String h() {
        return String.valueOf(H()) + "/GetFriendInformation";
    }

    public static String i() {
        return String.valueOf(H()) + "/RenameFriend";
    }

    public static String j() {
        return String.valueOf(H()) + "/RemoveFriend";
    }

    public static String k() {
        return String.valueOf(H()) + "/SendFile";
    }

    public static String l() {
        return String.valueOf(H()) + "/ReceiveFile";
    }

    public static String m() {
        return String.valueOf(H()) + "/DeleteFileTransfer";
    }

    public static String n() {
        return String.valueOf(H()) + "/GetFileTransferInformation";
    }

    public static String o() {
        return String.valueOf(H()) + "/ChangePassword";
    }

    public static String p() {
        return String.valueOf(H()) + "/MakeCall";
    }

    public static String q() {
        return String.valueOf(H()) + "/AnswerCall";
    }

    public static String r() {
        return String.valueOf(H()) + "/RejectCall";
    }

    public static String s() {
        return String.valueOf(H()) + "/CancelCall";
    }

    public static String t() {
        return String.valueOf(H()) + "/CloseCall";
    }

    public static String u() {
        return String.valueOf(H()) + "/CreateGroup";
    }

    public static String v() {
        return String.valueOf(H()) + "/RemoveGroup";
    }

    public static String w() {
        return String.valueOf(H()) + "/RenameGroup";
    }

    public static String x() {
        return String.valueOf(H()) + "/SetNewPassword";
    }

    public static String y() {
        return String.valueOf(H()) + "/SendMail";
    }

    public static String z() {
        return String.valueOf(H()) + "/ReadMail";
    }

    public static String A() {
        return String.valueOf(H()) + "/DeleteMail";
    }

    public static String B() {
        return String.valueOf(H()) + "/UpdateStatus";
    }

    public static String C() {
        return String.valueOf(H()) + "/UpdateName";
    }

    public static String D() {
        return String.valueOf(H()) + "/DeleteChat";
    }

    public static String E() {
        return String.valueOf(H()) + "/AddFriendToGroup";
    }

    public static String F() {
        return String.valueOf(H()) + "/RemoveFriendFromGroup";
    }

    public static String G() {
        return String.valueOf(H()) + "/Register";
    }
}
