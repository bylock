package net.client.by.lock.f;

import java.util.Observable;
import java.util.Observer;

/* compiled from: MyApp */
public class h extends Observable {
    protected Object a = null;
    protected Object b = null;
    protected String c;
    private Object d = new Object();
    private Observer e;

    public h(String str) {
        this.c = str;
        this.e = new i(this, null);
    }

    public h(Object obj, String str) {
        this.a = obj;
        this.c = str;
        this.e = new i(this, null);
    }

    public Object a() {
        return this.a;
    }

    public Object b() {
        return this.b;
    }

    public void a(Object obj) {
        synchronized (this.d) {
            this.b = this.a;
            this.a = obj;
        }
        setChanged();
        notifyObservers(this.c);
    }

    public void a(h hVar) {
        hVar.addObserver(this.e);
    }

    public String toString() {
        return this.a.toString();
    }
}
