package net.client.by.lock.f;

/* compiled from: MyApp */
public class k {
    public static l a(String str) {
        if (str == null || str.length() < 6) {
            return l.MISSING_LENGTH;
        }
        if (!str.matches(".*[a-zA-Z].*")) {
            return l.MISSING_LETTER;
        }
        if (str.matches("[a-zA-Z0-9]*")) {
            return l.MISSING_SYMBOL;
        }
        return l.SECURE;
    }
}
