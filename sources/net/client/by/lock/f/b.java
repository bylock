package net.client.by.lock.f;

import java.io.FileOutputStream;

/* compiled from: MyApp */
public class b {
    FileOutputStream a;
    byte[] b = new byte[4096];
    int c = 0;

    public b(FileOutputStream fileOutputStream) {
        this.a = fileOutputStream;
    }

    public void a(byte[] bArr) {
        if (bArr == null) {
            return;
        }
        if (bArr.length + this.c < 4096) {
            System.arraycopy(bArr, 0, this.b, this.c, bArr.length);
            this.c += bArr.length;
            return;
        }
        System.arraycopy(bArr, 0, this.b, this.c, 4096 - this.c);
        this.a.write(this.b);
        int i = 4096 - this.c;
        int length = (bArr.length - i) % 4096;
        this.a.write(bArr, i, (bArr.length - i) - length);
        System.arraycopy(bArr, bArr.length - length, this.b, 0, length);
        this.c = length;
    }

    public void a() {
        this.a.write(this.b, 0, this.c);
        this.a.flush();
        this.a.close();
    }
}
