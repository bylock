package net.client.by.lock.f;

import android.content.Context;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import net.client.by.lock.R;
import org.apache.http.conn.ssl.StrictHostnameVerifier;

/* compiled from: MyApp */
public class e {
    private static KeyStore a;
    private static KeyStore b = null;

    public static HttpsURLConnection a(String str, Context context) {
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) new URL(str).openConnection();
        httpsURLConnection.setDoOutput(true);
        httpsURLConnection.setRequestMethod("POST");
        httpsURLConnection.setDoInput(true);
        httpsURLConnection.setHostnameVerifier(new StrictHostnameVerifier());
        httpsURLConnection.setSSLSocketFactory(a(context));
        httpsURLConnection.setConnectTimeout(36000);
        httpsURLConnection.setReadTimeout(36000);
        httpsURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.6) Gecko/20061201 Firefox/2.0.0.6 (Ubuntu-feisty)");
        httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        return httpsURLConnection;
    }

    private static SSLSocketFactory a(Context context) {
        KeyManagementException e;
        SSLContext sSLContext;
        NoSuchAlgorithmException e2;
        b(context);
        try {
            sSLContext = SSLContext.getInstance("TLS");
            try {
                sSLContext.init(null, new TrustManager[]{new f()}, null);
            } catch (NoSuchAlgorithmException e3) {
                e2 = e3;
                j.a("HttpsURLConnectionFactory", e2);
                return sSLContext.getSocketFactory();
            } catch (KeyManagementException e4) {
                e = e4;
                e.printStackTrace();
                return sSLContext.getSocketFactory();
            }
        } catch (NoSuchAlgorithmException e5) {
            sSLContext = null;
            e2 = e5;
            j.a("HttpsURLConnectionFactory", e2);
            return sSLContext.getSocketFactory();
        } catch (KeyManagementException e6) {
            sSLContext = null;
            e = e6;
            e.printStackTrace();
            return sSLContext.getSocketFactory();
        }
        return sSLContext.getSocketFactory();
    }

    private static void b(Context context) {
        try {
            if (a == null) {
                a = KeyStore.getInstance("BKS");
                InputStream openRawResource = context.getApplicationContext().getResources().openRawResource(R.raw.issuers);
                a.load(openRawResource, "caPaski.toBeTheKing".toCharArray());
                openRawResource.close();
            }
            if (b == null) {
                b = KeyStore.getInstance("BKS");
                InputStream openRawResource2 = context.getApplicationContext().getResources().openRawResource(R.raw.subjects);
                b.load(openRawResource2, "caPaski.toBeTheKing".toCharArray());
                openRawResource2.close();
            }
        } catch (Exception e) {
            throw new KeyStoreException("Certificates cannot be loaded: " + e.getMessage());
        }
    }
}
