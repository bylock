package net.client.by.lock.f;

import java.security.cert.X509Certificate;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class n {
    X509Certificate a = null;
    n b = null;
    n c = null;

    public n(X509Certificate x509Certificate) {
        this.a = x509Certificate;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        if (this.a == null && nVar.a == null) {
            return true;
        }
        if (this.a == null || nVar.a == null) {
            return false;
        }
        return this.a.equals(nVar.a);
    }

    public int hashCode() {
        if (this.a == null) {
            return 0;
        }
        return this.a.hashCode();
    }
}
