package net.client.by.lock.f;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Observable;

/* compiled from: MyApp */
public class g extends Observable implements List {
    private ArrayList a = new ArrayList();

    @Override // java.util.List, java.util.Collection
    public boolean add(Object obj) {
        boolean add = this.a.add(obj);
        setChanged();
        notifyObservers(obj);
        return add;
    }

    @Override // java.util.List
    public void add(int i, Object obj) {
        this.a.add(i, obj);
        setChanged();
        notifyObservers(obj);
    }

    @Override // java.util.List, java.util.Collection
    public boolean addAll(Collection collection) {
        boolean addAll = this.a.addAll(collection);
        setChanged();
        notifyObservers(collection);
        return addAll;
    }

    @Override // java.util.List
    public boolean addAll(int i, Collection collection) {
        boolean addAll = this.a.addAll(i, collection);
        setChanged();
        notifyObservers(Integer.valueOf(i));
        return addAll;
    }

    public void clear() {
        this.a.clear();
        setChanged();
        notifyObservers();
    }

    public boolean contains(Object obj) {
        return this.a.contains(obj);
    }

    @Override // java.util.List, java.util.Collection
    public boolean containsAll(Collection collection) {
        return this.a.containsAll(collection);
    }

    @Override // java.util.List
    public Object get(int i) {
        return this.a.get(i);
    }

    public int indexOf(Object obj) {
        return this.a.indexOf(obj);
    }

    public boolean isEmpty() {
        return this.a.isEmpty();
    }

    @Override // java.util.List, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return this.a.iterator();
    }

    public int lastIndexOf(Object obj) {
        return this.a.lastIndexOf(obj);
    }

    @Override // java.util.List
    public ListIterator listIterator() {
        return this.a.listIterator();
    }

    @Override // java.util.List
    public ListIterator listIterator(int i) {
        return this.a.listIterator(i);
    }

    @Override // java.util.List
    public Object remove(int i) {
        Object remove = this.a.remove(i);
        hasChanged();
        notifyObservers();
        return remove;
    }

    @Override // java.util.List
    public boolean remove(Object obj) {
        boolean remove = this.a.remove(obj);
        hasChanged();
        notifyObservers();
        return remove;
    }

    @Override // java.util.List, java.util.Collection
    public boolean removeAll(Collection collection) {
        boolean removeAll = this.a.removeAll(collection);
        hasChanged();
        notifyObservers();
        return removeAll;
    }

    @Override // java.util.List, java.util.Collection
    public boolean retainAll(Collection collection) {
        boolean retainAll = this.a.retainAll(collection);
        hasChanged();
        notifyObservers();
        return retainAll;
    }

    @Override // java.util.List
    public Object set(int i, Object obj) {
        Object obj2 = this.a.set(i, obj);
        hasChanged();
        notifyObservers();
        return obj2;
    }

    public int size() {
        return this.a.size();
    }

    @Override // java.util.List
    public List subList(int i, int i2) {
        return this.a.subList(i, i2);
    }

    public Object[] toArray() {
        return this.a.toArray();
    }

    @Override // java.util.List, java.util.Collection
    public Object[] toArray(Object[] objArr) {
        return this.a.toArray(objArr);
    }
}
