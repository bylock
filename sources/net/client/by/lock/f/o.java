package net.client.by.lock.f;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.os.Build;
import android.support.v4.app.j;
import android.text.ClipboardManager;

/* compiled from: MyApp */
public class o {
    public static int a() {
        if (a(12)) {
            return c();
        }
        return b();
    }

    private static int b() {
        return 1;
    }

    @TargetApi(12)
    private static int c() {
        return 3;
    }

    public static boolean a(int i) {
        return i <= Build.VERSION.SDK_INT;
    }

    public static void a(j jVar) {
        if (a(11)) {
            b(jVar);
        }
    }

    @TargetApi(13)
    private static void b(j jVar) {
        jVar.invalidateOptionsMenu();
    }

    public static void a(Object obj, String str, String str2) {
        if (a(11)) {
            c(obj, str, str2);
        } else {
            b(obj, str, str2);
        }
    }

    private static void b(Object obj, String str, String str2) {
        ((ClipboardManager) obj).setText(str2);
    }

    @TargetApi(11)
    private static void c(Object obj, String str, String str2) {
        ((android.content.ClipboardManager) obj).setPrimaryClip(ClipData.newPlainText(str, str2));
    }
}
