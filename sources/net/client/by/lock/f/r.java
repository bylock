package net.client.by.lock.f;

import android.content.SharedPreferences;
import java.util.Set;

/* compiled from: MyApp */
public class r implements SharedPreferences.Editor {
    protected SharedPreferences.Editor a;
    final /* synthetic */ q b;

    public r(q qVar) {
        this.b = qVar;
        this.a = qVar.a.edit();
    }

    /* renamed from: a */
    public r putBoolean(String str, boolean z) {
        this.a.putString(str, this.b.a(Boolean.toString(z)));
        return this;
    }

    /* renamed from: a */
    public r putFloat(String str, float f) {
        this.a.putString(str, this.b.a(Float.toString(f)));
        return this;
    }

    /* renamed from: a */
    public r putInt(String str, int i) {
        this.a.putString(str, this.b.a(Integer.toString(i)));
        return this;
    }

    /* renamed from: a */
    public r putLong(String str, long j) {
        this.a.putString(str, this.b.a(Long.toString(j)));
        return this;
    }

    /* renamed from: a */
    public r putString(String str, String str2) {
        this.a.putString(str, this.b.a(str2));
        return this;
    }

    public r a(String str, String str2, String str3) {
        this.a.putString(str, this.b.a(str2, str3));
        return this;
    }

    public void apply() {
        this.a.apply();
    }

    /* renamed from: a */
    public r clear() {
        this.a.clear();
        return this;
    }

    public boolean commit() {
        return this.a.commit();
    }

    /* renamed from: a */
    public r remove(String str) {
        this.a.remove(str);
        return this;
    }

    /* renamed from: a */
    public r putStringSet(String str, Set set) {
        throw new UnsupportedOperationException();
    }
}
