package net.client.by.lock.f;

import java.security.KeyStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.net.ssl.X509TrustManager;

/* compiled from: MyApp */
class f implements X509TrustManager {
    f() {
    }

    @Override // javax.net.ssl.X509TrustManager
    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    @Override // javax.net.ssl.X509TrustManager
    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        if (x509CertificateArr == null || x509CertificateArr.length == 0 || str == null || str.length() == 0) {
            throw new IllegalArgumentException();
        } else if (!m.a(x509CertificateArr, getAcceptedIssuers(), a())) {
            throw new CertificateException("Cannot verify certificate chain.");
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        if (e.a() == null) {
            return null;
        }
        try {
            Enumeration<String> aliases = e.a().aliases();
            ArrayList arrayList = new ArrayList();
            while (aliases.hasMoreElements()) {
                arrayList.add((X509Certificate) e.a().getCertificate(aliases.nextElement()));
            }
            return (X509Certificate[]) arrayList.toArray(new X509Certificate[0]);
        } catch (KeyStoreException e) {
            e.printStackTrace();
            return null;
        }
    }

    public X509Certificate[] a() {
        if (e.b() == null) {
            return null;
        }
        try {
            Enumeration<String> aliases = e.b().aliases();
            ArrayList arrayList = new ArrayList();
            while (aliases.hasMoreElements()) {
                arrayList.add((X509Certificate) e.b().getCertificate(aliases.nextElement()));
            }
            return (X509Certificate[]) arrayList.toArray(new X509Certificate[0]);
        } catch (KeyStoreException e) {
            e.printStackTrace();
            return null;
        }
    }
}
