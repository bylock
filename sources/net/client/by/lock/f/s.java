package net.client.by.lock.f;

import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.http.HttpEntity;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class s {
    public static Document a(HttpEntity httpEntity) {
        Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        TransformerFactory.newInstance().newTransformer().transform(new StreamSource(new InputStreamReader(httpEntity.getContent(), "UTF-8")), new DOMResult(newDocument));
        return newDocument;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a4 A[SYNTHETIC, Splitter:B:27:0x00a4] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b1 A[SYNTHETIC, Splitter:B:34:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r11, org.w3c.dom.Document r12) {
        /*
        // Method dump skipped, instructions count: 196
        */
        throw new UnsupportedOperationException("Method not decompiled: net.client.by.lock.f.s.a(java.lang.String, org.w3c.dom.Document):void");
    }
}
