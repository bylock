package net.client.by.lock.f;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

/* compiled from: MyApp */
public class m {
    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            int length = digest.length;
            String str2 = "";
            int i = 0;
            while (i < length) {
                byte b = digest[i];
                i++;
                str2 = String.valueOf(str2) + String.format("%02x", Byte.valueOf(b));
            }
            return str2;
        } catch (NoSuchAlgorithmException e) {
            j.a("SecurityUtils", e);
            return null;
        }
    }

    public static boolean a(X509Certificate[] x509CertificateArr, X509Certificate[] x509CertificateArr2, X509Certificate[] x509CertificateArr3) {
        if (x509CertificateArr.length == 1) {
            return a(x509CertificateArr[0], x509CertificateArr2, x509CertificateArr3);
        }
        n[] nVarArr = new n[x509CertificateArr.length];
        for (int i = 0; i < x509CertificateArr.length; i++) {
            nVarArr[i] = new n(x509CertificateArr[i]);
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < x509CertificateArr.length - 1; i2++) {
            try {
                x509CertificateArr[i2].checkValidity();
                for (int i3 = 0; i3 < x509CertificateArr.length; i3++) {
                    if (i3 != i2) {
                        if (nVarArr[i3].b == null && nVarArr[i2].c == null && a(x509CertificateArr[i3], x509CertificateArr[i2])) {
                            nVarArr[i3].b = nVarArr[i2];
                            nVarArr[i2].c = nVarArr[i3];
                        }
                        if (nVarArr[i3].c == null && nVarArr[i2].b == null && a(x509CertificateArr[i2], x509CertificateArr[i3])) {
                            nVarArr[i3].c = nVarArr[i2];
                            nVarArr[i2].b = nVarArr[i3];
                        }
                    }
                }
            } catch (CertificateExpiredException e) {
                j.a("Certificate is not valid: " + x509CertificateArr[i2].getSubjectX500Principal().getName(), e);
                return false;
            } catch (CertificateNotYetValidException e2) {
                return false;
            }
        }
        n nVar = nVarArr[0];
        while (nVar.c != null) {
            nVar = nVar.c;
        }
        do {
            arrayList.add(nVar.a);
            nVar = nVar.b;
        } while (nVar != null);
        if (arrayList.size() == x509CertificateArr.length) {
            return a((X509Certificate) arrayList.get(0), x509CertificateArr2, x509CertificateArr3);
        }
        j.a("SecurityUtils", new CertPathValidatorException("Broken certificate chain: Unable to sort well"));
        return false;
    }

    private static boolean a(X509Certificate x509Certificate, X509Certificate[] x509CertificateArr, X509Certificate[] x509CertificateArr2) {
        if (x509CertificateArr != null) {
            for (X509Certificate x509Certificate2 : x509CertificateArr) {
                if (a(x509Certificate, x509Certificate2)) {
                    return true;
                }
            }
        }
        if (x509CertificateArr2 != null) {
            for (X509Certificate x509Certificate3 : x509CertificateArr2) {
                if (x509Certificate.equals(x509Certificate3)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean a(X509Certificate x509Certificate, X509Certificate x509Certificate2) {
        if (!x509Certificate.getIssuerX500Principal().getName().equals(x509Certificate2.getSubjectX500Principal().getName())) {
            return false;
        }
        try {
            x509Certificate.verify(x509Certificate2.getPublicKey());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
