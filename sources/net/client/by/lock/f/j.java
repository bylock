package net.client.by.lock.f;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import net.client.by.lock.b;
import net.client.by.lock.b.l;
import net.client.by.lock.b.n;
import org.apache.http.entity.InputStreamEntity;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class j {
    public static void a(String str, Throwable th) {
        String str2 = "\r\n*** START EXCEPTION *** \r\n" + new SimpleDateFormat("dd.MM.yy HH:mm:ss", Locale.getDefault()).format(new Timestamp(System.currentTimeMillis())) + "\r\n" + "Message: " + th.getMessage() + "\r\n" + "Cause: " + th.getCause() + "\r\n" + "Name: " + th.getClass().getName();
        StringBuilder sb = new StringBuilder();
        StackTraceElement[] stackTrace = th.getStackTrace();
        int length = stackTrace.length;
        for (int i = 0; i < length; i++) {
            sb.append(String.valueOf(stackTrace[i].toString()) + "\r\n");
        }
        b.e(str, String.valueOf(str2) + sb.toString() + "\r\n" + "*** END EXCEPTION ***" + "\r\n");
    }

    public static String a(int i) {
        if (i > 3600) {
            return String.format(Locale.getDefault(), "%02d:%02d:%02d", Integer.valueOf(i / 3600), Integer.valueOf((i % 3600) / 60), Integer.valueOf(i % 60));
        }
        return String.format(Locale.getDefault(), "%02d:%02d", Integer.valueOf(i / 60), Integer.valueOf(i % 60));
    }

    public static String a(double d) {
        if (d > 1024.0d) {
            double d2 = d / 1024.0d;
            if (d2 > 1024.0d) {
                double d3 = d2 / 1024.0d;
                if (d3 > 1024.0d) {
                    return String.valueOf(String.format("%.1f", Double.valueOf(d3 / 1024.0d))) + " GB/s";
                }
                return String.valueOf(String.format("%.1f", Double.valueOf(d3))) + " MB/s";
            }
            return String.valueOf(String.format("%.1f", Double.valueOf(d2))) + " KB/s";
        }
        return String.valueOf(String.format("%.1f", Double.valueOf(d))) + " B/s";
    }

    public static boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(0);
        if (networkInfo2 != null && networkInfo2.isConnected()) {
            return true;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static byte[] a(String str) {
        int digit;
        int length = str.length();
        if (length % 2 != 0) {
            return null;
        }
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            int digit2 = Character.digit(str.charAt(i), 16);
            if (digit2 == -1 || (digit = Character.digit(str.charAt(i + 1), 16)) == -1) {
                return null;
            }
            bArr[i / 2] = (byte) ((digit2 << 4) + digit);
        }
        return bArr;
    }

    public static String a(byte[] bArr) {
        String str = "";
        for (int i = 0; i < bArr.length; i++) {
            str = String.valueOf(str) + String.format("%02x", Byte.valueOf(bArr[i]));
        }
        return str;
    }

    public static boolean a(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        if (bArr.length < i + i3 || bArr2.length < i2 + i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (bArr[i + i4] != bArr2[i2 + i4]) {
                return false;
            }
        }
        return true;
    }

    public static int a(byte[] bArr, byte[] bArr2) {
        boolean z;
        for (int i = 0; i < (bArr.length - bArr2.length) + 1; i++) {
            int i2 = 0;
            while (true) {
                if (i2 >= bArr2.length) {
                    z = true;
                    break;
                } else if (bArr[i + i2] != bArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                return i;
            }
        }
        return -1;
    }

    public static boolean a(Document document, OutputStream outputStream, n nVar) {
        InputStreamEntity inputStreamEntity;
        try {
            StringWriter stringWriter = new StringWriter();
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(document), new StreamResult(stringWriter));
            if (nVar == null) {
                inputStreamEntity = new InputStreamEntity(new ByteArrayInputStream(stringWriter.toString().getBytes()), -1);
            } else {
                inputStreamEntity = new InputStreamEntity(new l(new ByteArrayInputStream(stringWriter.toString().getBytes()), nVar), -1);
            }
            inputStreamEntity.setChunked(true);
            inputStreamEntity.writeTo(outputStream);
            outputStream.flush();
        } catch (Exception e) {
            a("Error in transforming xml to post", e);
        }
        return true;
    }

    public static String a(long j) {
        if (j <= 0) {
            return "0 B";
        }
        String[] strArr = {"B", "KB", "MB", "GB", "TB"};
        int log10 = (int) (Math.log10((double) j) / Math.log10(1024.0d));
        return String.valueOf(new DecimalFormat("#,##0.#").format(((double) j) / Math.pow(1024.0d, (double) log10))) + " " + strArr[log10];
    }
}
