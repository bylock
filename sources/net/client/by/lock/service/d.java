package net.client.by.lock.service;

import android.media.MediaPlayer;

/* compiled from: MyApp */
class d implements MediaPlayer.OnErrorListener {
    private d() {
    }

    /* synthetic */ d(d dVar) {
        this();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        mediaPlayer.reset();
        return true;
    }
}
