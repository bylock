package net.client.by.lock.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.c.i;
import net.client.by.lock.d.c;
import net.client.by.lock.gui.activity.CallActivity;
import net.client.by.lock.gui.b.an;

/* compiled from: MyApp */
class a extends BroadcastReceiver {
    final /* synthetic */ BackgroundService a;

    a(BackgroundService backgroundService) {
        this.a = backgroundService;
    }

    public void onReceive(Context context, Intent intent) {
        boolean z = true;
        switch (intent.getIntExtra("what", -1)) {
            case 4:
                try {
                    Intent intent2 = new Intent(BackgroundService.i, CallActivity.class);
                    intent2.putExtra("id", intent.getIntExtra("id", -1));
                    BackgroundService.i.startActivity(intent2.setFlags(268435456));
                    return;
                } catch (Exception e) {
                    b.e("BackgroundService", e.toString());
                    return;
                }
            case 5:
                b.i("BackgroundService", "MSG_GET_ROSTER_EVENT");
                net.client.by.lock.c.a.b(BackgroundService.i);
                return;
            case 6:
                if (intent.getIntExtra("arg1", -1) == 1) {
                    BackgroundService.c.acquire();
                    return;
                } else {
                    BackgroundService.c.release();
                    return;
                }
            case 7:
                b.i("BackgroundService", "MSG_TRY_LOGIN");
                net.client.by.lock.c.a.c(BackgroundService.i);
                return;
            case 8:
                an.a(BackgroundService.i, intent.getStringExtra("error"));
                return;
            case 9:
                BackgroundService.q = intent.getIntExtra("arg1", -1);
                BackgroundService.r = intent.getIntExtra("arg2", -1);
                c a2 = i.c().a(Integer.valueOf(intent.getIntExtra("friendUserId", -1)));
                if (BackgroundService.r == 1) {
                    BackgroundService.b(a2, BackgroundService.q);
                    return;
                }
                int hashCode = a2.C().hashCode();
                switch (BackgroundService.q) {
                    case 1:
                        hashCode += R.drawable.noti_chat_white;
                        break;
                    case 2:
                        hashCode += R.drawable.noti_file_white;
                        break;
                    case 3:
                        hashCode += R.drawable.noti_mail_white;
                        break;
                    case 4:
                        hashCode += R.drawable.noti_call_white;
                        break;
                }
                BackgroundService.i.o.cancel(hashCode);
                if (BackgroundService.j.contains(Integer.valueOf(hashCode))) {
                    BackgroundService.j.remove(BackgroundService.j.indexOf(Integer.valueOf(hashCode)));
                    return;
                }
                return;
            case 10:
                c a3 = i.c().a(Integer.valueOf(intent.getIntExtra("friendUserId", -1)));
                if (intent.getIntExtra("arg1", -1) != 1) {
                    z = false;
                }
                BackgroundService.b(a3, z);
                return;
            case 11:
                if (intent.getIntExtra("arg1", -1) != 1) {
                    z = false;
                }
                BackgroundService.c(z);
                return;
            case 12:
                BackgroundService.k();
                return;
            case 13:
                b.i("BackgroundService", "MSG_SAVE_PASSWORD");
                net.client.by.lock.c.a.a(BackgroundService.i);
                return;
            default:
                return;
        }
    }
}
