package net.client.by.lock.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Process;
import android.support.v4.app.ap;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.c.i;
import net.client.by.lock.c.l;
import net.client.by.lock.c.m;
import net.client.by.lock.d.c;
import net.client.by.lock.d.r;
import net.client.by.lock.gui.activity.ChatActivity;
import net.client.by.lock.gui.activity.SplashActivity;
import net.client.by.lock.gui.b.an;
import net.client.by.lock.reciever.NetworkStatusReciever;
import net.client.by.lock.reciever.ScreenReciever;
import net.client.by.lock.reciever.a;

/* compiled from: MyApp */
public class BackgroundService extends Service {
    private static WifiManager b;
    private static WifiManager.WifiLock c;
    private static PowerManager d;
    private static PowerManager.WakeLock e;
    private static NetworkStatusReciever f;
    private static a g;
    private static ScreenReciever h;
    private static BackgroundService i;
    private static ArrayList j;
    private static int k = 0;
    private static int l = 0;
    private static boolean m = false;
    private static int q = 0;
    private static int r = 0;
    private static MediaPlayer s;
    private static MediaPlayer t;
    private static MediaPlayer u;
    private static long w = 0;
    i a;
    private MediaPlayer.OnErrorListener n;
    private NotificationManager o;
    private int p;
    private BroadcastReceiver v = new a(this);

    public static BackgroundService a() {
        return i;
    }

    public void onCreate() {
    }

    /* access modifiers changed from: private */
    public static synchronized void b(c cVar, int i2) {
        int i3 = 0;
        synchronized (BackgroundService.class) {
            Intent intent = new Intent(i, ChatActivity.class);
            intent.putExtra("id", cVar.K());
            intent.addFlags(67108864);
            switch (i2) {
                case 1:
                    a(m.a().i(i), 1);
                    i3 = cVar.C().hashCode() + R.drawable.noti_chat_white;
                    i.o.cancel(i3);
                    i.o.notify(i3, new ap(i).a(false).a(R.drawable.noti_chat_white).a("byLock").c("New message from " + cVar.G()).b(true).a(PendingIntent.getActivity(i, i3, intent, 268435456)).b(cVar.G()).a());
                    break;
                case 2:
                    a(m.a().h(i), 2);
                    i3 = R.drawable.noti_file_white + cVar.C().hashCode();
                    i.o.cancel(i3);
                    i.o.notify(i3, new ap(i).a(false).a(R.drawable.noti_file_white).a("byLock").c("New file from " + cVar.G()).b(true).a(PendingIntent.getActivity(i, i3, intent, 134217728)).b(cVar.G()).a());
                    break;
                case 3:
                    a(m.a().i(i), 3);
                    i3 = R.drawable.noti_mail_white + cVar.C().hashCode();
                    i.o.cancel(i3);
                    i.o.notify(i3, new ap(i).a(false).a(R.drawable.noti_mail_white).a("byLock").c("New mail from " + cVar.G()).b(true).a(PendingIntent.getActivity(i, i3, intent, 268435456)).b(cVar.G()).a());
                    break;
                case 4:
                    i3 = cVar.C().hashCode() + R.drawable.noti_call_white;
                    i.o.cancel(i3);
                    i.o.notify(i3, new ap(i).a(false).a(R.drawable.noti_call_white).a("byLock").c("Missed call from " + cVar.G()).b(true).a(PendingIntent.getActivity(i, i3, intent, 268435456)).b(cVar.G()).a());
                    break;
            }
            if (!j.contains(Integer.valueOf(i3))) {
                j.add(Integer.valueOf(i3));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void a(android.net.Uri r8, int r9) {
        /*
        // Method dump skipped, instructions count: 110
        */
        throw new UnsupportedOperationException("Method not decompiled: net.client.by.lock.service.BackgroundService.a(android.net.Uri, int):void");
    }

    public void onDestroy() {
        b();
        b.v("BackgroundService", "destroy " + r.i().r());
        m.a().f(this);
        unregisterReceiver(this.v);
        unregisterReceiver(f);
        unregisterReceiver(g);
        unregisterReceiver(h);
        net.client.by.lock.c.a.a();
        this.o.cancel(this.p);
        if (c.isHeld()) {
            c.release();
        }
        if (e.isHeld()) {
            e.release();
        }
        s.release();
        s = null;
        t.release();
        t = null;
        u.release();
        u = null;
        b.i("BackgroundService", "onDestroy");
        System.gc();
        Process.killProcess(Process.myPid());
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        i = this;
        j = new ArrayList();
        this.o = (NotificationManager) getSystemService("notification");
        this.p = R.id.cancel;
        d = (PowerManager) getSystemService("power");
        e = d.newWakeLock(1, "BackgroundService");
        e.acquire();
        b = (WifiManager) getSystemService("wifi");
        c = b.createWifiLock(1, "ByLock_BackgroundService");
        c.setReferenceCounted(false);
        c.acquire();
        f = new NetworkStatusReciever();
        g = new a();
        h = new ScreenReciever();
        j();
        j.add(Integer.valueOf(this.p));
        this.a = i.c();
        m.a().a(this);
        t = new MediaPlayer();
        s = new MediaPlayer();
        u = new MediaPlayer();
        this.n = new d(null);
        u.setOnErrorListener(this.n);
        s.setOnErrorListener(this.n);
        t.setOnErrorListener(this.n);
        try {
            t.setDataSource(this, Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.outgoing));
            t.setAudioStreamType(0);
            t.setLooping(true);
            u.setDataSource(this, Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.busy));
            u.setAudioStreamType(0);
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        } catch (SecurityException e3) {
            e3.printStackTrace();
        } catch (IllegalStateException e4) {
            e4.printStackTrace();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
        u.prepareAsync();
        t.prepareAsync();
        i.registerReceiver(f, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        i.registerReceiver(g, new IntentFilter("android.intent.action.ACTION_SHUTDOWN"));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        i.registerReceiver(h, intentFilter);
        i.registerReceiver(this.v, new IntentFilter("by.lock.BackgroundService"));
        b.v("BackgroundService", "onStartCommand");
        if (intent == null) {
            b.v("BackgroundService", "login");
            if (r.i().q()) {
                b.v("BackgroundService", "already login");
            }
            l.a().e();
        }
        return 1;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void j() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(268435456);
        this.o.notify(this.p, new ap(this).a(true).a(R.drawable.noti_app).a("by Lock").a(PendingIntent.getActivity(getApplicationContext(), 0, intent, 0)).b("The Most Secure Messaging!").a());
    }

    public synchronized void b() {
        Iterator it = j.iterator();
        while (it.hasNext()) {
            this.o.cancel(((Integer) it.next()).intValue());
        }
        j.clear();
    }

    public boolean c() {
        return m;
    }

    public void a(boolean z) {
        b.e("BackgroundService", "connectionTypeChanged: " + m + " -> " + z);
        m = z;
    }

    /* access modifiers changed from: private */
    public static void b(c cVar, boolean z) {
        if (z) {
            Uri g2 = m.a().g(i);
            try {
                s.setDataSource(i, g2);
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            } catch (SecurityException e3) {
                e3.printStackTrace();
            } catch (IllegalStateException e4) {
                s.reset();
                try {
                    s.setDataSource(i, g2);
                    e4.printStackTrace();
                } catch (Exception e5) {
                    an.a(i, "Some kind error(0) happened while playing: " + g2.getPath());
                    return;
                }
            } catch (IOException e6) {
                e6.printStackTrace();
            }
            s.setAudioStreamType(2);
            s.setLooping(true);
            try {
                s.prepareAsync();
                s.setOnPreparedListener(new b());
            } catch (IllegalStateException e7) {
                an.a(i, "Some kind error(1) happened while playing: " + g2.getPath());
            }
        } else {
            if (s.isPlaying()) {
                s.pause();
                s.release();
            }
            s = new MediaPlayer();
        }
    }

    /* access modifiers changed from: private */
    public static void c(boolean z) {
        if (z) {
            t.start();
        } else if (t.isPlaying()) {
            t.pause();
            t.seekTo(0);
        }
    }

    /* access modifiers changed from: private */
    public static void k() {
        if (t.isPlaying()) {
            t.pause();
            t.seekTo(0);
        }
        u.start();
    }
}
