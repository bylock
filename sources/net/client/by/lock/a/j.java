package net.client.by.lock.a;

import java.security.MessageDigest;

/* compiled from: MyApp */
public class j {
    private byte[] a;
    private int b;
    private MessageDigest c = MessageDigest.getInstance("MD5");

    public void a(byte[] bArr, byte[] bArr2) {
        this.c.update(bArr2);
        this.a = this.c.digest(bArr);
        this.b = 0;
    }

    public byte a() {
        if (this.b < this.a.length) {
            byte b2 = this.a[this.b];
            this.b++;
            return b2;
        }
        this.a = this.c.digest(this.a);
        this.b = 1;
        return this.a[0];
    }
}
