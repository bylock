package net.client.by.lock.a;

/* compiled from: MyApp */
public class f extends b implements Comparable {
    private int a;

    public f(byte[] bArr, int i) {
        super(null, 0, 0);
        a(bArr, 4, i - 4);
        this.a = a(bArr);
    }

    public int e() {
        return this.a;
    }

    private static int a(byte[] bArr) {
        return (bArr[0] << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255);
    }

    /* renamed from: a */
    public int compareTo(f fVar) {
        if (this.a > fVar.a) {
            return 1;
        }
        if (this.a < fVar.a) {
            return -1;
        }
        return 0;
    }
}
