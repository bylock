package net.client.by.lock.a;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.b;
import net.client.by.lock.codec.Speex;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class o extends AsyncTask {
    DatagramSocket a;
    t b;
    SecretKeySpec c;
    IvParameterSpec d;
    byte[] e;
    byte[] f;
    final int g = 1600;
    long h;
    int i;
    int j;
    Speex k;
    byte[] l;
    byte[] m;
    ArrayBlockingQueue n;
    public p o;
    MessageDigest p = null;
    Cipher q = null;
    j r = null;

    public o(DatagramSocket datagramSocket, t tVar, SecretKeySpec secretKeySpec, IvParameterSpec ivParameterSpec, byte[] bArr, byte[] bArr2) {
        this.a = datagramSocket;
        this.b = tVar;
        this.c = secretKeySpec;
        this.d = ivParameterSpec;
        this.e = bArr;
        this.f = bArr2;
        this.i = 0;
        this.j = 0;
        this.k = new Speex();
        this.l = new byte[3200];
        this.m = new byte[4];
        this.n = new ArrayBlockingQueue(10, true);
        try {
            this.p = MessageDigest.getInstance("MD5");
            this.q = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.q.init(1, secretKeySpec, ivParameterSpec, r.i().a());
            this.r = new j();
        } catch (NoSuchAlgorithmException e2) {
            j.a("Error in startup of sender thread.", e2);
        } catch (NoSuchPaddingException e3) {
            e3.printStackTrace();
        } catch (InvalidKeyException e4) {
            e4.printStackTrace();
        } catch (InvalidAlgorithmParameterException e5) {
            e5.printStackTrace();
        }
        this.o = new p(this);
    }

    public void a() {
        b.e("SenderThread", "start");
        if (Build.VERSION.SDK_INT >= 11) {
            c();
        } else {
            b();
        }
    }

    private void b() {
        super.execute(new Void[0]);
    }

    @TargetApi(11)
    private void c() {
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        a aVar;
        this.h = System.currentTimeMillis();
        while (!net.client.by.lock.c.b.a.get()) {
            try {
                a aVar2 = (a) this.n.poll(200, TimeUnit.MILLISECONDS);
                if (aVar2 != null) {
                    aVar = aVar2;
                    if (aVar.b != 1600) {
                        b.e("SenderThread", "Unexpected size: " + aVar.b);
                        if (!this.b.c()) {
                            break;
                        }
                    } else {
                        try {
                            byte[] doFinal = this.q.doFinal(this.l, 0, this.k.encode(aVar.a, 0, aVar.b, this.l, 0));
                            r.i().a().nextBytes(this.m);
                            this.r.a(this.e, this.m);
                            this.p.update(this.e);
                            this.p.update(doFinal, 0, 8);
                            byte[] digest = this.p.digest();
                            byte[] bArr = new byte[(this.f.length + this.m.length + doFinal.length + 8)];
                            System.arraycopy(this.f, 0, bArr, 0, this.f.length);
                            System.arraycopy(this.m, 0, bArr, this.f.length, this.m.length);
                            int length = this.m.length + this.f.length;
                            for (int i2 = 0; i2 < doFinal.length; i2++) {
                                bArr[length + i2] = (byte) (doFinal[i2] ^ this.r.a());
                            }
                            int length2 = length + doFinal.length;
                            for (int i3 = 0; i3 < 8; i3++) {
                                bArr[length2 + i3] = (byte) (digest[i3] ^ this.r.a());
                            }
                            try {
                                this.a.send(new DatagramPacket(bArr, bArr.length, this.a.getInetAddress(), this.a.getPort()));
                                this.i++;
                                this.j += doFinal.length;
                            } catch (IOException e2) {
                                if (net.client.by.lock.c.b.a.get()) {
                                    break;
                                }
                                j.a("SenderThread", e2);
                                if (e2 instanceof SocketException) {
                                    break;
                                }
                            }
                        } catch (IllegalBlockSizeException e3) {
                            j.a("SenderThread Error in encrypting encoded audio.1", e3);
                        } catch (BadPaddingException e4) {
                            j.a("SenderThreadError in encrypting encoded audio.2", e4);
                        }
                    }
                } else {
                    continue;
                }
            } catch (InterruptedException e5) {
                if (net.client.by.lock.c.b.a.get()) {
                    break;
                }
                aVar = null;
            }
        }
        double currentTimeMillis = ((double) (System.currentTimeMillis() - this.h)) / 1000.0d;
        System.out.println("SAverage packet size: " + (((double) this.j) / ((double) this.i)));
        System.out.println("SAverage bwd: " + (((double) this.j) / currentTimeMillis));
        System.out.println("SAverage packet count: " + (((double) this.i) / currentTimeMillis));
        this.b.b();
        try {
            this.o.join();
        } catch (InterruptedException e6) {
            e6.printStackTrace();
        }
        this.n.clear();
        return null;
    }
}
