package net.client.by.lock.a;

import android.media.AudioRecord;
import net.client.by.lock.b;
import net.client.by.lock.c.m;
import net.client.by.lock.f.c;

/* compiled from: MyApp */
public class t extends e {
    public static AudioRecord a;
    private int b;
    private short[] c;

    public t() {
        this.b = 8000;
        this.c = null;
        this.b = m.a().d();
        int c2 = c.c();
        int minBufferSize = AudioRecord.getMinBufferSize(this.b, 16, c2) * 4;
        a = new AudioRecord(1, this.b, 16, c2, minBufferSize);
        b.e("TargetDataLine", "@Open Bonk " + minBufferSize);
    }

    public synchronized int a(short[] sArr, int i, int i2) {
        if (this.b == 8000) {
            int i3 = 0;
            int i4 = i2;
            while (true) {
                if (!net.client.by.lock.c.b.a.get()) {
                    try {
                        int read = a.read(sArr, i + i3, i4);
                        if (read <= 0) {
                            b.e("TargetDataLine", "Number of samples are wrong: " + read + " remaining: " + i4);
                        }
                        i3 += read;
                    } catch (Exception e) {
                        if (net.client.by.lock.c.b.a.get()) {
                            i2 = 0;
                            break;
                        }
                    }
                    i4 = i2 - i3;
                    if (i4 <= 0) {
                        break;
                    }
                } else {
                    i2 = 0;
                    break;
                }
            }
        } else {
            i2 = b(sArr, i, i2);
        }
        return i2;
    }

    private int b(short[] sArr, int i, int i2) {
        int i3 = this.b / 8000;
        if (this.c == null || this.c.length != i2 * i3) {
            this.c = new short[(i2 * i3)];
        }
        int i4 = i2 * i3;
        int i5 = 0;
        while (!net.client.by.lock.c.b.a.get()) {
            try {
                i5 += a.read(this.c, i + i5, i4);
            } catch (Exception e) {
                if (net.client.by.lock.c.b.a.get()) {
                    return 0;
                }
            }
            i4 = (i2 * i3) - i5;
            if (i4 <= 0) {
                for (int i6 = 0; i6 < i2; i6++) {
                    if (net.client.by.lock.c.b.a.get()) {
                        return 0;
                    }
                    sArr[i6 + i] = this.c[i3 * i6];
                }
                return i2;
            }
        }
        return 0;
    }

    public void a() {
        try {
            a.startRecording();
        } catch (IllegalStateException e) {
            int c2 = c.c();
            a = new AudioRecord(1, this.b, 16, c2, AudioRecord.getMinBufferSize(this.b, 16, c2) * 4);
            a.startRecording();
        }
    }

    public synchronized void b() {
        if (a != null) {
            if (c() && a.getRecordingState() == 3) {
                a.stop();
            }
            a.release();
            a = null;
        }
    }

    public synchronized boolean c() {
        boolean z = true;
        synchronized (this) {
            if (a == null || a.getState() != 1) {
                z = false;
            }
        }
        return z;
    }
}
