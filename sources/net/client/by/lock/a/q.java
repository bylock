package net.client.by.lock.a;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.b;
import net.client.by.lock.codec.Speex;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class q extends AsyncTask {
    DatagramSocket a;
    t b;
    SecretKeySpec c;
    IvParameterSpec d;
    byte[] e;
    byte[] f;
    final int g = 160;
    Speex h;
    byte[] i;
    byte[] j;
    ArrayBlockingQueue k;
    public r l;
    MessageDigest m = null;
    Cipher n = null;
    j o = null;

    public q(DatagramSocket datagramSocket, t tVar, SecretKeySpec secretKeySpec, IvParameterSpec ivParameterSpec, byte[] bArr, byte[] bArr2) {
        this.a = datagramSocket;
        this.b = tVar;
        this.c = secretKeySpec;
        this.d = ivParameterSpec;
        this.e = bArr;
        this.f = bArr2;
        this.h = new Speex(true);
        this.i = new byte[324];
        this.j = new byte[4];
        this.k = new ArrayBlockingQueue(10, true);
        try {
            this.m = MessageDigest.getInstance("MD5");
            this.n = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.n.init(1, secretKeySpec, ivParameterSpec, r.i().a());
            this.o = new j();
        } catch (NoSuchAlgorithmException e2) {
            j.a("Error in startup of sender thread.", e2);
        } catch (NoSuchPaddingException e3) {
            e3.printStackTrace();
        } catch (InvalidKeyException e4) {
            e4.printStackTrace();
        } catch (InvalidAlgorithmParameterException e5) {
            e5.printStackTrace();
        }
        this.l = new r(this);
    }

    public void a() {
        b.e("SenderThread", "start");
        if (Build.VERSION.SDK_INT >= 11) {
            c();
        } else {
            b();
        }
    }

    private void b() {
        super.execute(new Void[0]);
    }

    @TargetApi(11)
    private void c() {
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        a aVar;
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(4);
        int nextInt = 536870912 + r.i().a().nextInt(536870912);
        while (!net.client.by.lock.c.b.a.get()) {
            try {
                a aVar2 = (a) this.k.poll(20, TimeUnit.MILLISECONDS);
                if (aVar2 != null) {
                    aVar = aVar2;
                    if (aVar.b != 160) {
                        b.e("SenderThreadV2", "Unexpected size: " + aVar.b);
                        if (!this.b.c()) {
                            break;
                        }
                    } else {
                        allocateDirect.clear();
                        allocateDirect.putInt(nextInt);
                        allocateDirect.rewind();
                        allocateDirect.get(this.i, 0, 4);
                        int i2 = nextInt + 1;
                        try {
                            byte[] doFinal = this.n.doFinal(this.i, 0, this.h.encode(aVar.a, 0, aVar.b, this.i, 4) + 4);
                            r.i().a().nextBytes(this.j);
                            this.o.a(this.e, this.j);
                            this.m.update(this.e);
                            this.m.update(doFinal, 0, 8);
                            byte[] digest = this.m.digest();
                            byte[] bArr = new byte[(this.f.length + this.j.length + doFinal.length + 8)];
                            System.arraycopy(this.f, 0, bArr, 0, this.f.length);
                            System.arraycopy(this.j, 0, bArr, this.f.length, this.j.length);
                            int length = this.j.length + this.f.length;
                            for (int i3 = 0; i3 < doFinal.length; i3++) {
                                bArr[length + i3] = (byte) (doFinal[i3] ^ this.o.a());
                            }
                            int length2 = length + doFinal.length;
                            for (int i4 = 0; i4 < 8; i4++) {
                                bArr[length2 + i4] = (byte) (digest[i4] ^ this.o.a());
                            }
                            try {
                                this.a.send(new DatagramPacket(bArr, bArr.length, this.a.getInetAddress(), this.a.getPort()));
                                nextInt = i2;
                            } catch (IOException e2) {
                                if (net.client.by.lock.c.b.a.get()) {
                                    break;
                                }
                                j.a("SenderThreadV2", e2);
                                if (e2 instanceof SocketException) {
                                    break;
                                }
                                nextInt = i2;
                            }
                        } catch (IllegalBlockSizeException e3) {
                            j.a("SenderThreadV2 Error in encrypting encoded audio.1", e3);
                        } catch (BadPaddingException e4) {
                            j.a("SenderThreadV2Error in encrypting encoded audio.2", e4);
                        }
                    }
                } else {
                    continue;
                }
            } catch (InterruptedException e5) {
                if (net.client.by.lock.c.b.a.get()) {
                    break;
                }
                aVar = null;
            }
        }
        this.b.b();
        try {
            this.l.join();
        } catch (InterruptedException e6) {
            e6.printStackTrace();
        }
        this.k.clear();
        return null;
    }
}
