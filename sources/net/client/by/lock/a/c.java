package net.client.by.lock.a;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.c.b;
import net.client.by.lock.c.n;
import net.client.by.lock.d.k;
import net.client.by.lock.d.r;
import net.client.by.lock.f.h;
import net.client.by.lock.gui.a.a;
import net.client.by.lock.gui.a.e;

/* compiled from: MyApp */
public class c extends k {
    private SecretKeySpec a;
    private IvParameterSpec b;
    private int c = 0;
    private h i;
    private byte[] j;
    private byte[] k;
    private byte[] l;
    private byte[] m = null;
    private byte[] n;
    private d o;
    private boolean p;

    private c(net.client.by.lock.d.c cVar, boolean z, String str, boolean z2) {
        b(cVar);
        a(z);
        this.p = z2;
        this.i = new h("0:00", "seconds");
        this.f = new h("stateProperty");
        this.f.a(str);
        this.o = new d(this, null);
        cVar.q().addObserver(this.o);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.d.k
    public String a() {
        return "Sesli Arama" + (((String) this.f.a()).equals("CLOSED") ? " (" + ((String) this.i.a()) + ")" : "");
    }

    public h b() {
        return this.i;
    }

    public void a(IvParameterSpec ivParameterSpec) {
        this.b = ivParameterSpec;
    }

    public void a(SecretKeySpec secretKeySpec) {
        this.a = secretKeySpec;
    }

    public IvParameterSpec c() {
        return this.b;
    }

    public SecretKeySpec d() {
        return this.a;
    }

    public byte[] e() {
        return this.n;
    }

    public void a(byte[] bArr) {
        this.n = bArr;
    }

    public byte[] f() {
        return this.j;
    }

    public byte[] g() {
        return this.k;
    }

    public byte[] h() {
        return this.l;
    }

    public void b(byte[] bArr) {
        this.l = bArr;
    }

    public static c a(net.client.by.lock.d.c cVar, byte[] bArr, byte[] bArr2) {
        c cVar2 = new c(cVar, true, "CALLING", false);
        cVar2.a(new SecretKeySpec(bArr, "AES"));
        cVar2.a(new IvParameterSpec(bArr2));
        return cVar2;
    }

    public static c a(net.client.by.lock.d.c cVar, byte[] bArr, byte[] bArr2, boolean z) {
        c cVar2 = new c(cVar, false, "RINGING", z);
        cVar2.c(bArr);
        cVar2.a(bArr2);
        return cVar2;
    }

    public static c a(net.client.by.lock.d.c cVar) {
        return new c(cVar, false, "CALLEE BUSY", false);
    }

    public synchronized void i() {
        if (a("RINGING")) {
            n.a().b(this);
        }
    }

    public synchronized void a(byte[] bArr, byte[] bArr2, byte[] bArr3, int i2) {
        if (a("RINGING")) {
            this.f.a("ANSWERED");
            this.j = bArr;
            this.k = bArr2;
            this.l = bArr3;
            if (!w() || !E().u().a(this)) {
                t();
            } else {
                b(i2, this.p);
            }
        }
    }

    public synchronized void a(int i2, boolean z) {
        if (a("RINGING")) {
            this.f.a("ANSWERED");
            b(i2, z);
        }
    }

    public synchronized void j() {
        if (a("RINGING")) {
            H();
            this.f.a("REJECTED");
            n.a().c(this);
        }
    }

    public synchronized void k() {
        if (a("RINGING")) {
            H();
            this.f.a("REJECTED");
        }
    }

    public synchronized void l() {
        if (a("RINGING")) {
            H();
            this.f.a("MISSED");
        }
    }

    public synchronized void m() {
        if (a("RINGING") || a("CALLING")) {
            H();
            this.f.a("CANCELED");
            n.a().d(this);
        }
    }

    public synchronized void n() {
        if (a("RINGING")) {
            H();
            this.f.a("CANCELED");
        }
    }

    public synchronized void o() {
        if (a("CALLING")) {
            H();
            this.f.a("CALLER BUSY");
        }
    }

    public synchronized void p() {
        if ((u() && a("CALLING")) || v()) {
            H();
            this.f.a("CALLEE BUSY");
        }
    }

    public synchronized void a(byte[] bArr, byte[] bArr2) {
        if (a("CALLING")) {
            c(bArr);
            a(bArr2);
            this.f.a("RINGING");
        }
    }

    private synchronized void c(byte[] bArr) {
        this.m = bArr;
    }

    public byte[] q() {
        return this.m;
    }

    public synchronized void r() {
        if (a("ANSWERED")) {
            y();
            H();
            this.f.a("CLOSED");
            n.a().e(this);
        }
    }

    public synchronized void s() {
        if (a("ANSWERED")) {
            y();
            H();
            this.f.a("CLOSED");
        }
    }

    public synchronized void t() {
        if (a("ANSWERED")) {
            s();
        }
        H();
        this.f.a("SOME ERROR");
    }

    public boolean u() {
        return b_();
    }

    public boolean v() {
        return !b_();
    }

    public boolean a(String str) {
        return ((String) this.f.a()).equalsIgnoreCase(str);
    }

    public synchronized boolean w() {
        boolean z;
        byte[] b2 = r.i().p().b(this.j);
        byte[] b3 = r.i().p().b(this.k);
        if (b2 == null || b3 == null) {
            z = false;
        } else {
            this.a = new SecretKeySpec(b2, "AES");
            this.b = new IvParameterSpec(b3);
            z = true;
        }
        return z;
    }

    public synchronized boolean x() {
        this.j = this.e.u().a(this.a.getEncoded());
        this.k = this.e.u().a(this.b.getIV());
        return (this.j == null || this.k == null) ? false : true;
    }

    private boolean G() {
        return a("CALLING") || a("RINGING") || a("ANSWERED");
    }

    private void H() {
        if (G()) {
            h.a().c();
        }
        this.e.q().deleteObserver(this.o);
    }

    public void b(int i2, boolean z) {
        b.a(this, i2, z);
    }

    public void y() {
        b.a();
    }

    @Override // net.client.by.lock.d.k
    public void z() {
    }

    @Override // net.client.by.lock.d.k
    public a A() {
        if (this.h == null) {
            this.h = new e(this);
        }
        return this.h;
    }
}
