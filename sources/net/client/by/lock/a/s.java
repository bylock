package net.client.by.lock.a;

import android.media.AudioTrack;
import net.client.by.lock.b;
import net.client.by.lock.f.c;

/* compiled from: MyApp */
public class s extends e {
    public static AudioTrack a;

    public s() {
        int b = c.b();
        int c = c.c();
        int minBufferSize = AudioTrack.getMinBufferSize(b, 4, c);
        a = new AudioTrack(0, b, 4, c, minBufferSize, 1);
        b.e("SourceDataLine", "@Open Bonk " + minBufferSize);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003e, code lost:
        if (net.client.by.lock.c.b.a.get() == false) goto L_0x0012;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if (net.client.by.lock.c.b.a.get() == false) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(short[] r4, int r5, int r6) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.concurrent.atomic.AtomicBoolean r0 = net.client.by.lock.c.b.a     // Catch:{ all -> 0x0034 }
            boolean r0 = r0.get()     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x000b
        L_0x0009:
            monitor-exit(r3)
            return
        L_0x000b:
            android.media.AudioTrack r0 = net.client.by.lock.a.s.a     // Catch:{ Exception -> 0x0037 }
            int r1 = r6 / 2
            r0.write(r4, r5, r1)     // Catch:{ Exception -> 0x0037 }
        L_0x0012:
            java.util.concurrent.atomic.AtomicBoolean r0 = net.client.by.lock.c.b.a
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0009
            android.media.AudioTrack r0 = net.client.by.lock.a.s.a     // Catch:{ Exception -> 0x0041 }
            int r1 = r6 / 2
            int r1 = r1 + r5
            int r2 = r6 / 2
            int r2 = r6 - r2
            r0.write(r4, r1, r2)     // Catch:{ Exception -> 0x0041 }
        L_0x0026:
            java.util.concurrent.atomic.AtomicBoolean r0 = net.client.by.lock.c.b.a
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0009
            android.media.AudioTrack r0 = net.client.by.lock.a.s.a
            r0.flush()
            goto L_0x0009
        L_0x0034:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0037:
            r0 = move-exception
            java.util.concurrent.atomic.AtomicBoolean r0 = net.client.by.lock.c.b.a
            boolean r0 = r0.get()
            if (r0 == 0) goto L_0x0012
            goto L_0x0009
        L_0x0041:
            r0 = move-exception
            java.util.concurrent.atomic.AtomicBoolean r0 = net.client.by.lock.c.b.a
            boolean r0 = r0.get()
            if (r0 == 0) goto L_0x0026
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: net.client.by.lock.a.s.a(short[], int, int):void");
    }

    public void a() {
        try {
            a.play();
        } catch (IllegalStateException e) {
            int b = c.b();
            int c = c.c();
            int minBufferSize = AudioTrack.getMinBufferSize(b, 4, c);
            a = new AudioTrack(0, b, 4, c, minBufferSize, 1);
            b.e("SourceDataLine", "@Start Bonk " + minBufferSize);
            a.play();
        }
    }

    public synchronized void b() {
        if (a != null) {
            if (c() && a.getPlayState() == 3) {
                a.stop();
            }
            a.release();
            a = null;
        }
    }

    public synchronized boolean c() {
        boolean z = true;
        synchronized (this) {
            if (a == null || a.getState() != 1) {
                z = false;
            }
        }
        return z;
    }
}
