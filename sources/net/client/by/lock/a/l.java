package net.client.by.lock.a;

import android.os.Process;
import java.util.concurrent.TimeUnit;
import net.client.by.lock.c.b;

/* compiled from: MyApp */
public class l extends Thread {
    final /* synthetic */ k a;

    public l(k kVar) {
        this.a = kVar;
    }

    public void run() {
        a aVar;
        Process.setThreadPriority(-16);
        while (!b.a.get()) {
            try {
                int size = this.a.l.size();
                if (size > 1) {
                    net.client.by.lock.b.e("ReceiverThread", "problem: " + size);
                }
                aVar = (a) this.a.l.poll(200, TimeUnit.MILLISECONDS);
                if (aVar == null) {
                    continue;
                }
            } catch (InterruptedException e) {
                if (!b.a.get()) {
                    e.printStackTrace();
                    aVar = null;
                } else {
                    return;
                }
            }
            if (!b.a.get()) {
                this.a.b.a(aVar.a, 0, aVar.b);
                try {
                    Thread.sleep(6);
                } catch (InterruptedException e2) {
                    return;
                }
            } else {
                return;
            }
        }
    }
}
