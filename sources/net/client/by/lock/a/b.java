package net.client.by.lock.a;

/* compiled from: MyApp */
public class b {
    private short[] a;
    private byte[] b;
    private int c;
    private int d;

    public b(short[] sArr, int i, int i2) {
        a(sArr, i, i2);
    }

    /* access modifiers changed from: protected */
    public final void a(short[] sArr, int i, int i2) {
        this.a = sArr;
        this.c = i;
        this.d = i2;
    }

    /* access modifiers changed from: protected */
    public final void a(byte[] bArr, int i, int i2) {
        this.b = bArr;
        this.c = i;
        this.d = i2;
    }

    public byte[] a() {
        return this.b;
    }

    public short[] b() {
        return this.a;
    }

    public int c() {
        return this.c;
    }

    public int d() {
        return this.d;
    }
}
