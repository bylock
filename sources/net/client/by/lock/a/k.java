package net.client.by.lock.a;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.b;
import net.client.by.lock.codec.Speex;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class k extends AsyncTask {
    DatagramSocket a;
    s b;
    SecretKeySpec c;
    IvParameterSpec d;
    byte[] e;
    public l f;
    Speex g;
    int h;
    int i;
    int j;
    long k;
    i l;
    MessageDigest m = null;
    Cipher n = null;

    public k(DatagramSocket datagramSocket, s sVar, SecretKeySpec secretKeySpec, IvParameterSpec ivParameterSpec, byte[] bArr) {
        this.a = datagramSocket;
        this.b = sVar;
        this.c = secretKeySpec;
        this.d = ivParameterSpec;
        this.e = bArr;
        this.g = new Speex();
        this.h = 3212;
        this.i = 0;
        this.j = 0;
        this.l = new i();
        try {
            this.m = MessageDigest.getInstance("MD5");
            this.n = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.n.init(2, secretKeySpec, ivParameterSpec);
        } catch (NoSuchAlgorithmException e2) {
            j.a("Error in reciever of sender thread.", e2);
        } catch (NoSuchPaddingException e3) {
            j.a("Error in reciever of sender thread.", e3);
        } catch (InvalidKeyException e4) {
            j.a("Error in reciever of sender thread.", e4);
        } catch (InvalidAlgorithmParameterException e5) {
            j.a("Error in reciever of sender thread.", e5);
        }
        this.f = new l(this);
    }

    public void a() {
        b.e("RecieverThread", "start");
        if (Build.VERSION.SDK_INT >= 11) {
            c();
        } else {
            b();
        }
    }

    private void b() {
        super.execute(new Void[0]);
    }

    @TargetApi(11)
    private void c() {
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        this.k = System.currentTimeMillis();
        while (!net.client.by.lock.c.b.a.get()) {
            DatagramPacket datagramPacket = new DatagramPacket(new byte[this.h], this.h);
            try {
                this.a.receive(datagramPacket);
                this.m.reset();
                this.m.update(this.e);
                this.m.update(datagramPacket.getData(), datagramPacket.getOffset(), 8);
                if (j.a(this.m.digest(), 0, datagramPacket.getData(), (datagramPacket.getOffset() + datagramPacket.getLength()) - 8, 8)) {
                    this.i++;
                    this.j += datagramPacket.getLength();
                    try {
                        byte[] doFinal = this.n.doFinal(datagramPacket.getData(), datagramPacket.getOffset(), datagramPacket.getLength() - 8);
                        short[] sArr = new short[1600];
                        this.l.a(new a(sArr, this.g.decode(doFinal, 0, doFinal.length, sArr, 0)));
                    } catch (IllegalBlockSizeException e2) {
                        b.e("ReceiverThread", "IllegalBlockSizeException");
                        j.a("ReceiverThread Error in decrypting audio.", e2);
                    } catch (BadPaddingException e3) {
                        b.e("ReceiverThread", "BadPaddingException");
                        j.a("ReceiverThread Error in decrypting audio.", e3);
                    }
                } else {
                    j.a("Possibly spoofed packet.", new StreamCorruptedException());
                }
            } catch (IOException e4) {
                b.e("ReceiverThread", "Possible isPaused: " + net.client.by.lock.c.b.a.get());
                if (net.client.by.lock.c.b.a.get()) {
                    break;
                }
                j.a("ReceiverThread", e4);
                if (e4 instanceof SocketException) {
                    break;
                }
            }
        }
        double currentTimeMillis = ((double) (System.currentTimeMillis() - this.k)) / 1000.0d;
        System.out.println("RAverage packet size: " + (((double) this.j) / ((double) this.i)));
        System.out.println("RAverage bwd: " + (((double) this.j) / currentTimeMillis));
        System.out.println("RAverage packet count: " + (((double) this.i) / currentTimeMillis));
        this.b.b();
        try {
            this.f.interrupt();
            this.f.join();
        } catch (InterruptedException e5) {
            e5.printStackTrace();
        }
        this.l.clear();
        return null;
    }
}
