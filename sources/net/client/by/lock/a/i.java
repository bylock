package net.client.by.lock.a;

import java.util.concurrent.ArrayBlockingQueue;
import net.client.by.lock.f.c;

/* compiled from: MyApp */
public class i extends ArrayBlockingQueue {
    public i() {
        super(50);
    }

    public void a(a aVar) {
        int pow = ((int) ((25.0d * Math.pow((double) size(), 2.0d)) * ((double) aVar.b()))) / 100;
        if (pow == 0) {
            add(aVar);
            return;
        }
        int a = c.a() / 16;
        if (pow * a < aVar.b()) {
            short[] sArr = new short[(aVar.b() - (pow * a))];
            int length = sArr.length / pow;
            for (int i = 0; i < pow; i++) {
                System.arraycopy(aVar.a(), (length + a) * i, sArr, length * i, length);
            }
            System.arraycopy(aVar.a(), (a + length) * pow, sArr, length * pow, sArr.length - (pow * length));
            add(new a(sArr, sArr.length));
        }
    }
}
