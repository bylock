package net.client.by.lock.a;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.b;
import net.client.by.lock.codec.Speex;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class m extends AsyncTask {
    DatagramSocket a;
    s b;
    SecretKeySpec c;
    IvParameterSpec d;
    byte[] e;
    public n f;
    Speex g;
    int h;
    g i;
    MessageDigest j = null;
    Cipher k = null;

    public m(DatagramSocket datagramSocket, s sVar, SecretKeySpec secretKeySpec, IvParameterSpec ivParameterSpec, byte[] bArr) {
        this.a = datagramSocket;
        this.b = sVar;
        this.c = secretKeySpec;
        this.d = ivParameterSpec;
        this.e = bArr;
        this.g = new Speex(true);
        this.i = new g(this.g);
        this.h = 332;
        try {
            this.j = MessageDigest.getInstance("MD5");
            this.k = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.k.init(2, secretKeySpec, ivParameterSpec);
        } catch (NoSuchAlgorithmException e2) {
            j.a("Error in reciever of sender thread.", e2);
        } catch (NoSuchPaddingException e3) {
            j.a("Error in reciever of sender thread.", e3);
        } catch (InvalidKeyException e4) {
            j.a("Error in reciever of sender thread.", e4);
        } catch (InvalidAlgorithmParameterException e5) {
            j.a("Error in reciever of sender thread.", e5);
        }
        this.f = new n(this);
    }

    public void a() {
        b.e("RecieverThread", "start");
        if (Build.VERSION.SDK_INT >= 11) {
            c();
        } else {
            b();
        }
    }

    private void b() {
        super.execute(new Void[0]);
    }

    @TargetApi(11)
    private void c() {
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        while (!net.client.by.lock.c.b.a.get()) {
            DatagramPacket datagramPacket = new DatagramPacket(new byte[this.h], this.h);
            try {
                this.a.receive(datagramPacket);
                this.j.update(this.e);
                this.j.update(datagramPacket.getData(), datagramPacket.getOffset(), 8);
                if (j.a(this.j.digest(), 0, datagramPacket.getData(), (datagramPacket.getOffset() + datagramPacket.getLength()) - 8, 8)) {
                    try {
                        byte[] doFinal = this.k.doFinal(datagramPacket.getData(), datagramPacket.getOffset(), datagramPacket.getLength() - 8);
                        this.i.a(new f(doFinal, doFinal.length));
                    } catch (IllegalBlockSizeException e2) {
                        b.e("ReceiverThreadV2", "IllegalBlockSizeException");
                        j.a("ReceiverThreadV2 Error in decrypting audio.", e2);
                    } catch (BadPaddingException e3) {
                        b.e("ReceiverThreadV2", "BadPaddingException");
                        j.a("ReceiverThreadV2 Error in decrypting audio.", e3);
                    }
                } else {
                    j.a("Possibly spoofed packet.", new StreamCorruptedException());
                }
            } catch (IOException e4) {
                b.e("ReceiverThreadV2", "Possible isPaused: " + net.client.by.lock.c.b.a.get());
                if (net.client.by.lock.c.b.a.get()) {
                    break;
                }
                j.a("ReceiverThreadV2", e4);
                if (e4 instanceof SocketException) {
                    break;
                }
            }
        }
        this.b.b();
        try {
            this.f.interrupt();
            this.f.join();
            return null;
        } catch (InterruptedException e5) {
            e5.printStackTrace();
            return null;
        }
    }
}
