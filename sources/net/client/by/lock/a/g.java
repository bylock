package net.client.by.lock.a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import net.client.by.lock.b;
import net.client.by.lock.codec.Speex;
import net.client.by.lock.f.c;

/* compiled from: MyApp */
public class g {
    private static final int a = (c.a() / 16);
    private ArrayList b = new ArrayList();
    private ArrayBlockingQueue c = new ArrayBlockingQueue(5);
    private int d = 0;
    private int e = 0;
    private int f = Integer.MAX_VALUE;
    private Speex g;

    public g(Speex speex) {
        this.g = speex;
    }

    public void a(f fVar) {
        int binarySearch;
        if (this.d == 0) {
            this.d = fVar.e();
        }
        this.e = Math.max(this.e, fVar.d());
        this.f = Math.min(this.f, fVar.d());
        if (this.b.size() < 15 && (binarySearch = Collections.binarySearch(this.b, fVar)) < 0) {
            this.b.add(-(binarySearch + 1), fVar);
        }
        try {
            b();
        } catch (Exception e2) {
            b.i("JitterBuffer", e2.getMessage());
        }
    }

    private void b() {
        short[] sArr;
        int decodeLost;
        double d2;
        boolean z;
        int i;
        if (c()) {
            int size = this.b.size();
            int i2 = 1;
            while (i2 < size && (((f) this.b.get(i2 - 1)).e() + 1 >= ((f) this.b.get(i2)).e() || this.b.size() >= 15)) {
                i2++;
            }
            short[] sArr2 = new short[(((((f) this.b.get(i2 - 1)).e() - this.d) + 1) * 160)];
            int i3 = 0;
            int i4 = 0;
            while (i4 < i2) {
                double d3 = ((double) (i2 / 3)) * 0.01d;
                if (this.d == ((f) this.b.get(0)).e()) {
                    f fVar = (f) this.b.remove(0);
                    if (this.f < this.e && d3 > 0.0d) {
                        d3 *= 1.75d - (((double) (fVar.d() - this.f)) / ((double) (this.e - this.f)));
                    }
                    if (d3 > 0.0d) {
                        short[] sArr3 = new short[160];
                        this.g.decode(fVar.a(), fVar.c(), fVar.d(), sArr3, 0);
                        z = true;
                        d2 = d3;
                        sArr = sArr3;
                        i = i3;
                        decodeLost = 0;
                    } else {
                        d2 = d3;
                        decodeLost = 0;
                        sArr = null;
                        i = this.g.decode(fVar.a(), fVar.c(), fVar.d(), sArr2, i3);
                        z = false;
                    }
                } else {
                    short[] sArr4 = new short[160];
                    sArr = sArr4;
                    decodeLost = this.g.decodeLost(sArr4, 0);
                    d2 = 1.5d * d3;
                    z = true;
                    i = i3;
                }
                if (d2 < 1.0d && z) {
                    int i5 = (int) (((double) (decodeLost / a)) * d2);
                    if (i5 > 0) {
                        int i6 = (decodeLost - (a * i5)) / i5;
                        int i7 = i6 - (i6 % a);
                        int i8 = i;
                        for (int i9 = 0; i9 < i5; i9++) {
                            System.arraycopy(sArr, (a + i7) * i9, sArr2, i8, i7);
                            i8 += i7;
                        }
                        System.arraycopy(sArr, (a + i7) * i5, sArr2, i8, (decodeLost - (a * i5)) - (i7 * i5));
                        i = ((decodeLost - (a * i5)) - (i5 * i7)) + i8;
                    } else {
                        System.arraycopy(sArr, 0, sArr2, i, decodeLost);
                        i += decodeLost;
                    }
                }
                if (this.d > 0) {
                    this.d++;
                }
                i4++;
                i3 = i;
            }
            try {
                this.c.add(new b(sArr2, 0, i3));
            } catch (Exception e2) {
            }
        }
    }

    private boolean c() {
        if (this.b.isEmpty()) {
            return false;
        }
        if (((f) this.b.get(0)).e() == this.d) {
            return true;
        }
        return this.b.size() >= 15;
    }

    public b a() {
        return (b) this.c.take();
    }
}
