package net.client.by.lock.c;

import android.os.AsyncTask;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;
import net.client.by.lock.a;
import net.client.by.lock.a.c;
import net.client.by.lock.a.k;
import net.client.by.lock.a.m;
import net.client.by.lock.a.o;
import net.client.by.lock.a.q;
import net.client.by.lock.a.s;
import net.client.by.lock.a.t;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class b {
    public static AtomicBoolean a = new AtomicBoolean(false);
    private static DatagramSocket b = null;
    private static AsyncTask c = null;
    private static AsyncTask d = null;
    private static t e;
    private static s f;

    public static void a(c cVar, int i, boolean z) {
        a.set(false);
        try {
            InetAddress byName = InetAddress.getByName(a.a());
            e = new t();
            e.a();
            f = new s();
            f.a();
            try {
                b = new DatagramSocket();
                b.connect(byName, i);
                if (z) {
                    c = new q(b, e, cVar.d(), cVar.c(), cVar.e(), cVar.q());
                    d = new m(b, f, cVar.d(), cVar.c(), cVar.e());
                } else {
                    c = new o(b, e, cVar.d(), cVar.c(), cVar.e(), cVar.q());
                    d = new k(b, f, cVar.d(), cVar.c(), cVar.e());
                }
                if (z) {
                    ((m) d).f.start();
                    ((q) c).l.start();
                } else {
                    ((k) d).f.start();
                    ((o) c).o.start();
                }
                if (z) {
                    ((m) d).a();
                    ((q) c).a();
                    return;
                }
                ((k) d).a();
                ((o) c).a();
            } catch (SocketException e2) {
                j.a("CallThreadsManager", e2);
                f.b();
                e.b();
                cVar.t();
            }
        } catch (UnknownHostException e3) {
            j.a("CallThreadsManager", e3);
            cVar.t();
        }
    }

    public static void a() {
        f = null;
        e = null;
        if (b != null) {
            b.close();
        }
        c = null;
        d = null;
    }
}
