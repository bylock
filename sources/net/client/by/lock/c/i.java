package net.client.by.lock.c;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Observable;
import net.client.by.lock.a;
import net.client.by.lock.d.c;
import net.client.by.lock.d.g;
import net.client.by.lock.d.h;
import net.client.by.lock.d.s;

/* compiled from: MyApp */
public class i extends Observable {
    private static i a = new i();
    private s b = new s();
    private s c;
    private SparseArray d;
    private SparseArray e;
    private SparseArray f;
    private final Comparator g = new j(this);
    private final Comparator h = new k(this);

    public i() {
        this.b.a(true);
        this.c = new s();
        this.c.a(true);
        this.d = new SparseArray();
        this.e = new SparseArray();
        this.f = new SparseArray();
        h hVar = new h(new g(0, "Friends"), this.g);
        hVar.a(true);
        h hVar2 = new h(new g(0, "Friends"), this.h);
        hVar2.a(true);
        this.d.put(0, hVar);
        this.e.put(0, hVar2);
        this.b.e().add(hVar);
        this.c.e().add(hVar2);
    }

    public synchronized void a(g gVar) {
        h hVar = new h(gVar, this.g);
        this.b.e().add(this.d.size() - 1, hVar);
        h hVar2 = new h(gVar, this.h);
        this.c.e().add(this.e.size() - 1, hVar2);
        this.d.put(gVar.a(), hVar);
        hVar.a(true);
        this.e.put(gVar.a(), hVar2);
        hVar2.a(true);
        setChanged();
        notifyObservers();
    }

    public synchronized void a(int i) {
        h hVar = (h) this.d.get(i);
        if (hVar != null) {
            hVar.a();
            this.d.remove(i);
            this.b.e().remove(hVar);
        }
        h hVar2 = (h) this.e.get(i);
        if (hVar2 != null) {
            hVar2.a();
            this.e.remove(i);
            this.c.e().remove(hVar2);
        }
        setChanged();
        notifyObservers();
    }

    public synchronized void a(c cVar) {
        ((h) this.d.get(0)).a(new s(cVar));
        ((h) this.e.get(0)).a(new s(cVar));
        this.f.put(cVar.K(), cVar);
        setChanged();
        notifyObservers(cVar);
    }

    public synchronized void b(c cVar) {
        Iterator it = cVar.a().iterator();
        while (it.hasNext()) {
            Integer num = (Integer) it.next();
            h hVar = (h) this.d.get(num.intValue());
            if (hVar != null) {
                hVar.a(cVar.K());
            }
            h hVar2 = (h) this.e.get(num.intValue());
            if (hVar2 != null) {
                hVar2.a(cVar.K());
            }
        }
        ((h) this.d.get(0)).a(cVar.K());
        ((h) this.e.get(0)).a(cVar.K());
        this.f.remove(cVar.K());
        net.client.by.lock.c.c().a(Integer.valueOf(cVar.K()), cVar.C());
        setChanged();
        notifyObservers();
    }

    public synchronized c a(Integer num) {
        return (c) this.f.get(num.intValue());
    }

    public synchronized h b(int i) {
        return (h) this.d.get(i);
    }

    public s a() {
        return this.b;
    }

    public s b() {
        return this.c;
    }

    public static synchronized i c() {
        i iVar;
        synchronized (i.class) {
            if (a == null) {
                a = new i();
                if (a.e != null) {
                    a.e.b();
                }
            }
            iVar = a;
        }
        return iVar;
    }

    public static void d() {
        a = null;
    }

    public synchronized ArrayList e() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (int i = 0; i < this.d.size(); i++) {
            arrayList.add((g) ((h) this.d.get(this.d.keyAt(i))).d());
        }
        return arrayList;
    }

    public ArrayList f() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.f.size(); i++) {
            arrayList.add((c) this.f.get(this.f.keyAt(i)));
        }
        return arrayList;
    }

    public void g() {
        for (int i = 0; i < this.f.size(); i++) {
            ((c) this.f.get(this.f.keyAt(i))).v();
        }
    }

    public synchronized void a(Integer num, Integer num2) {
        c cVar = (c) this.f.get(num.intValue());
        if (cVar != null) {
            cVar.a(num2);
            h hVar = (h) this.d.get(num2.intValue());
            if (hVar != null) {
                hVar.a(new s(cVar));
            }
            h hVar2 = (h) this.e.get(num2.intValue());
            if (hVar2 != null) {
                hVar2.a(new s(cVar));
            }
        }
        setChanged();
        notifyObservers();
    }

    public synchronized void b(Integer num, Integer num2) {
        c cVar = (c) this.f.get(num.intValue());
        if (cVar != null) {
            cVar.b(num2);
            h hVar = (h) this.d.get(num2.intValue());
            if (hVar != null) {
                hVar.a(num.intValue());
            }
            h hVar2 = (h) this.e.get(num2.intValue());
            if (hVar2 != null) {
                hVar2.a(num.intValue());
            }
        }
        setChanged();
        notifyObservers();
    }

    public synchronized void a(Integer num, String str) {
        h hVar = (h) this.d.get(num.intValue());
        if (hVar != null) {
            ((g) hVar.d()).a(str);
        }
        h hVar2 = (h) this.e.get(num.intValue());
        if (hVar2 != null) {
            ((g) hVar2.d()).a(str);
        }
        setChanged();
        notifyObservers();
    }

    public void h() {
        if (a.e != null) {
            a.e.c();
        }
        a = null;
    }
}
