package net.client.by.lock.c;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ap;
import java.util.Calendar;
import java.util.Date;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.d.r;
import net.client.by.lock.gui.activity.SplashActivity;
import net.client.by.lock.reciever.AlarmReceiver;
import net.client.by.lock.reciever.NetworkStatusReciever;
import net.client.by.lock.service.BackgroundService;

/* compiled from: MyApp */
public class a {
    public static int a = 21337;
    static Date b = null;
    private static PendingIntent c;
    private static PendingIntent d;
    private static int e = 3;
    private static String f = "AlarmRecieverManager";

    public static void a(Context context) {
        b.d(f, "setupSavePassword start!");
        if (r.i().q() && r.i().r()) {
            m.a().d(context);
            Calendar instance = Calendar.getInstance();
            instance.add(13, 1800);
            Intent intent = new Intent("by.lock.BackgroundService");
            intent.putExtra("what", 13);
            c = PendingIntent.getBroadcast(context, 2928371, intent, 268435456);
            ((AlarmManager) context.getSystemService("alarm")).set(0, instance.getTimeInMillis(), c);
        }
        b.d(f, "setupSavePassword end!");
    }

    public static void b(Context context) {
        e = 3;
        b.d(f, "setupGetRosterEvent start!");
        if (!NetworkStatusReciever.a().booleanValue()) {
            b.d(f, "setupGetRosterEvent not online!");
            e = 30;
        }
        Calendar instance = Calendar.getInstance();
        instance.add(13, e);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("alarm_message", "roster_event");
        c = PendingIntent.getBroadcast(context, 292837, intent, 268435456);
        ((AlarmManager) context.getSystemService("alarm")).set(0, instance.getTimeInMillis(), c);
        b.d(f, "setupGetRosterEvent end!");
    }

    public static void c(Context context) {
        int i;
        b.d(f, "setupTryLogin start!");
        Date date = new Date();
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(13, -10);
        if (b == null || !b.after(instance.getTime())) {
            b = date;
            try {
                if (!r.i().r()) {
                    b.v(f, "no user pass");
                    m.a().e(context);
                    if (!r.i().r()) {
                        b.v(f, "still no user pass");
                        Intent intent = new Intent(context, SplashActivity.class);
                        intent.addFlags(67108864);
                        String string = l.a().b().getString(R.string.session_logged_off);
                        BackgroundService.a(m.a().h(context), 1);
                        ((NotificationManager) context.getSystemService("notification")).notify(a, new ap(context).a(false).a(R.drawable.action_logout).a("byLock").c(string).b(true).a(PendingIntent.getActivity(context, a, intent, 268435456)).b("Session logged off, please relogin").a());
                        n.a().d();
                        return;
                    }
                }
            } catch (Exception e2) {
                b.e(f, "setupTryLogin no username check failed!" + e2.toString());
            }
            if (!NetworkStatusReciever.a().booleanValue()) {
                b.d(f, "setupTryLogin not online!");
                i = 30;
            } else {
                i = 3;
            }
            instance.setTime(new Date());
            instance.add(13, i);
            Intent intent2 = new Intent(context, AlarmReceiver.class);
            intent2.putExtra("alarm_message", "try_login");
            d = PendingIntent.getBroadcast(context, 192837, intent2, 268435456);
            ((AlarmManager) context.getSystemService("alarm")).set(0, instance.getTimeInMillis(), d);
            b.d(f, "setupTryLogin end!");
            return;
        }
        b.d(f, "setupTryLogin early retry!");
    }

    public static void a() {
        if (c != null) {
            c.cancel();
        }
        if (d != null) {
            d.cancel();
        }
    }
}
