package net.client.by.lock.c;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import com.google.android.gms.d.p;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import net.client.by.lock.b.g;
import net.client.by.lock.b.h;
import net.client.by.lock.c;
import net.client.by.lock.d.b;
import net.client.by.lock.d.l;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.activity.ae;
import net.client.by.lock.gui.activity.aw;
import net.client.by.lock.service.BackgroundService;
import net.client.by.lock.task.AddFriendToGroupTask;
import net.client.by.lock.task.AddFriendToRosterTask;
import net.client.by.lock.task.ChangePasswordTask;
import net.client.by.lock.task.CreateGroupWithFriendTask;
import net.client.by.lock.task.DeleteChatTask;
import net.client.by.lock.task.DeleteFileTransferTask;
import net.client.by.lock.task.DeleteMailTask;
import net.client.by.lock.task.FileTask;
import net.client.by.lock.task.GetFileTransferInformationTask;
import net.client.by.lock.task.GetFriendInformationTask;
import net.client.by.lock.task.GetRosterEventTask;
import net.client.by.lock.task.LoginTask;
import net.client.by.lock.task.LogoutTask;
import net.client.by.lock.task.ReadMailTask;
import net.client.by.lock.task.ReceiveChatTask;
import net.client.by.lock.task.ReceiveFileTask;
import net.client.by.lock.task.RegisterTask;
import net.client.by.lock.task.RemoveFriendFromGroupTask;
import net.client.by.lock.task.RemoveFriendTask;
import net.client.by.lock.task.RemoveGroupTask;
import net.client.by.lock.task.RenameFriendTask;
import net.client.by.lock.task.RenameGroupTask;
import net.client.by.lock.task.SendChatTask;
import net.client.by.lock.task.SendFileTask;
import net.client.by.lock.task.SendMailTask;
import net.client.by.lock.task.SetNewPasswordTask;
import net.client.by.lock.task.UpdateNameTask;
import net.client.by.lock.task.UpdatePublicMessageTask;
import net.client.by.lock.task.UpdateStatusTask;
import net.client.by.lock.task.a;
import net.client.by.lock.task.call.AnswerCallTask;
import net.client.by.lock.task.call.CancelCallTask;
import net.client.by.lock.task.call.CloseCallTask;
import net.client.by.lock.task.call.MakeCallTask;
import net.client.by.lock.task.call.RejectCallTask;

/* compiled from: MyApp */
public class n {
    private static n c = null;
    private static /* synthetic */ int[] d;
    private HashMap a = new HashMap();
    private final Object b = new Object();

    static /* synthetic */ int[] g() {
        int[] iArr = d;
        if (iArr == null) {
            iArr = new int[f.values().length];
            try {
                iArr[f.Idle.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[f.Loadeed.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[f.Loading.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            d = iArr;
        }
        return iArr;
    }

    private n() {
    }

    public static n a() {
        if (c == null) {
            c = new n();
        }
        return c;
    }

    private a a(Class cls, Object obj) {
        return a((Context) null, cls, obj);
    }

    private a a(Context context, Class cls, Object obj) {
        a aVar;
        NoSuchMethodException e;
        InvocationTargetException e2;
        IllegalAccessException e3;
        IllegalArgumentException e4;
        InstantiationException e5;
        a aVar2 = null;
        if (context == null) {
            context = g.a().b();
        }
        if (r.i().h() && !cls.equals(LogoutTask.class)) {
            aVar = null;
        } else if (cls.equals(RegisterTask.class) || cls.equals(LoginTask.class) || r.i().q()) {
            Random random = new Random();
            random.setSeed(System.nanoTime());
            String str = System.currentTimeMillis() + "_" + random.nextInt();
            synchronized (this.b) {
                try {
                    aVar = (a) cls.getConstructor(Context.class, Object.class).newInstance(context, obj);
                    try {
                        aVar.setTaskId(str);
                        aVar.execute();
                        this.a.put(str, aVar);
                    } catch (InstantiationException e6) {
                        aVar2 = aVar;
                        e5 = e6;
                        j.a("TaskManager", e5);
                        aVar = aVar2;
                        return aVar;
                    } catch (IllegalArgumentException e7) {
                        aVar2 = aVar;
                        e4 = e7;
                        j.a("TaskManager", e4);
                        aVar = aVar2;
                        return aVar;
                    } catch (IllegalAccessException e8) {
                        aVar2 = aVar;
                        e3 = e8;
                        j.a("TaskManager", e3);
                        aVar = aVar2;
                        return aVar;
                    } catch (InvocationTargetException e9) {
                        aVar2 = aVar;
                        e2 = e9;
                        j.a("TaskManager", e2);
                        aVar = aVar2;
                        return aVar;
                    } catch (NoSuchMethodException e10) {
                        aVar2 = aVar;
                        e = e10;
                        j.a("TaskManager", e);
                        aVar = aVar2;
                        return aVar;
                    }
                } catch (InstantiationException e11) {
                    e5 = e11;
                    j.a("TaskManager", e5);
                    aVar = aVar2;
                    return aVar;
                } catch (IllegalArgumentException e12) {
                    e4 = e12;
                    j.a("TaskManager", e4);
                    aVar = aVar2;
                    return aVar;
                } catch (IllegalAccessException e13) {
                    e3 = e13;
                    j.a("TaskManager", e3);
                    aVar = aVar2;
                    return aVar;
                } catch (InvocationTargetException e14) {
                    e2 = e14;
                    j.a("TaskManager", e2);
                    aVar = aVar2;
                    return aVar;
                } catch (NoSuchMethodException e15) {
                    e = e15;
                    j.a("TaskManager", e);
                    aVar = aVar2;
                    return aVar;
                }
            }
        } else if (cls.equals(LogoutTask.class)) {
            l.a().c();
            Context b2 = g.a().b();
            if (b2 != null && (b2 instanceof Activity)) {
                ((Activity) b2).finish();
            }
            r.a(true);
            return null;
        } else {
            a.c(context);
            return null;
        }
        return aVar;
    }

    public void a(String str) {
        synchronized (this.b) {
            if (((a) this.a.get(str)) != null) {
                this.a.remove(str);
            }
        }
    }

    public void b() {
        switch (g()[c.b().ordinal()]) {
            case 1:
                c.a().d();
                a(LoginTask.class, (Object) null);
                return;
            case 2:
                new Handler().postDelayed(new p(this), 300);
                return;
            case 3:
                p.a(BackgroundService.a()).a("GTM-TSDSM8", -1).a(new o(this), 5, TimeUnit.SECONDS);
                b();
                return;
            default:
                return;
        }
    }

    public void c() {
        a(UpdatePublicMessageTask.class, (Object) null);
    }

    public void a(String str, String str2, ae aeVar) {
        a(AddFriendToRosterTask.class, new Object[]{str, str2, aeVar});
    }

    public void b(String str) {
        a(GetFileTransferInformationTask.class, str);
    }

    public void a(Integer num) {
        a(GetFriendInformationTask.class, num);
    }

    public void d() {
        Map.Entry[] entryArr;
        r.i().g();
        try {
            c.c().a();
        } catch (NullPointerException e) {
            j.a("TaskManager", e);
        }
        synchronized (this.b) {
            for (Map.Entry entry : (Map.Entry[]) this.a.entrySet().toArray(new Map.Entry[0])) {
                ((a) entry.getValue()).cancelTask(true);
            }
            a(LogoutTask.class, (Object) null);
        }
    }

    public void c(String str) {
        a(ChangePasswordTask.class, str);
    }

    public void a(net.client.by.lock.d.c cVar) {
        a(RenameFriendTask.class, cVar);
    }

    public void b(net.client.by.lock.d.c cVar) {
        a(RemoveFriendTask.class, cVar);
    }

    public void c(net.client.by.lock.d.c cVar) {
        a(ReceiveChatTask.class, cVar);
    }

    public void a(b bVar) {
        a(SendChatTask.class, bVar);
    }

    public FileTask a(h hVar) {
        return (FileTask) a(SendFileTask.class, hVar);
    }

    public FileTask a(g gVar) {
        String substring;
        String substring2;
        if (gVar.j() == null) {
            String b2 = m.a().b();
            if (!b2.endsWith(File.separator)) {
                b2 = String.valueOf(b2) + File.separator;
            }
            int lastIndexOf = gVar.e().lastIndexOf(".");
            if (lastIndexOf == -1) {
                substring = gVar.e();
                substring2 = "";
            } else {
                substring = gVar.e().substring(0, lastIndexOf);
                substring2 = gVar.e().substring(lastIndexOf);
            }
            String str = String.valueOf(b2) + substring + substring2;
            int i = 1;
            while (new File(str).exists()) {
                str = String.valueOf(b2) + substring + " (" + i + ")" + substring2;
                i++;
            }
            gVar.a(new File(str));
        }
        return (FileTask) a(ReceiveFileTask.class, gVar);
    }

    public void d(String str) {
        a(DeleteFileTransferTask.class, str);
    }

    public void b(Integer num) {
        a(UpdateStatusTask.class, num);
    }

    public void e(String str) {
        a(UpdateNameTask.class, str);
    }

    public void a(net.client.by.lock.a.c cVar) {
        a(MakeCallTask.class, cVar);
    }

    public void b(net.client.by.lock.a.c cVar) {
        a(AnswerCallTask.class, cVar);
    }

    public void c(net.client.by.lock.a.c cVar) {
        a(RejectCallTask.class, cVar);
    }

    public void d(net.client.by.lock.a.c cVar) {
        a(CancelCallTask.class, cVar);
    }

    public void e(net.client.by.lock.a.c cVar) {
        a(CloseCallTask.class, cVar);
    }

    public void a(net.client.by.lock.b.a aVar, ArrayList arrayList) {
        h hVar = new h(aVar);
        hVar.a(arrayList);
        a(SendFileTask.class, hVar);
    }

    public void a(Integer num, Integer num2) {
        a(AddFriendToGroupTask.class, new Integer[]{num, num2});
    }

    public void b(Integer num, Integer num2) {
        a(RemoveFriendFromGroupTask.class, new Integer[]{num, num2});
    }

    public void a(net.client.by.lock.d.c cVar, String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(str);
        arrayList.add(cVar);
        a(CreateGroupWithFriendTask.class, arrayList);
    }

    public void c(Integer num) {
        a(RemoveGroupTask.class, num);
    }

    public void a(net.client.by.lock.d.g gVar) {
        a(RenameGroupTask.class, gVar);
    }

    public void e() {
        a(SetNewPasswordTask.class, (Object) null);
    }

    public void a(net.client.by.lock.d.n nVar, aw awVar) {
        a(SendMailTask.class, new Object[]{nVar, awVar});
    }

    public void a(l lVar) {
        a(ReadMailTask.class, lVar);
    }

    public void f(String str) {
        a(DeleteMailTask.class, str);
    }

    public void f() {
        a(BackgroundService.a(), GetRosterEventTask.class, (Object) null);
    }

    public void b(b bVar) {
        a(DeleteChatTask.class, bVar);
    }

    public void a(String str, String str2) {
        if (a(RegisterTask.class, new String[]{str, str2}) == null) {
            net.client.by.lock.b.e("TaskManager", "register task null");
        }
    }
}
