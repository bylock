package net.client.by.lock.c;

import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import net.client.by.lock.b;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;
import net.client.by.lock.f.q;

/* compiled from: MyApp */
public class m {
    private static m a = null;
    private static boolean j = false;
    private String b = "";
    private int c = -1;
    private String d = "";
    private ArrayList e = new ArrayList();
    private Uri f;
    private Uri g;
    private Uri h;
    private Uri i;
    private boolean k = false;
    private String l = "";

    public static m a() {
        if (a == null) {
            a = new m();
        }
        return a;
    }

    private m() {
    }

    public String b() {
        return this.b;
    }

    public void a(Context context) {
        q qVar = new q(context);
        this.b = qVar.getString("downloadDirectory", String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/byLock-Downloads");
        this.d = qVar.getString("cache", "null");
        this.c = qVar.getInt("frequency", -1);
        String[] split = qVar.getString("expandedGroups", "0;").split(";");
        for (String str : split) {
            if (str.length() > 0) {
                this.e.add(Integer.valueOf(Integer.parseInt(str)));
            }
        }
        this.f = Uri.parse(qVar.getString("ringTone", RingtoneManager.getDefaultUri(1).toString()));
        this.g = Uri.parse(qVar.getString("fileTone", RingtoneManager.getDefaultUri(2).toString()));
        this.h = Uri.parse(qVar.getString("msgTone", RingtoneManager.getDefaultUri(2).toString()));
        this.i = Uri.parse(qVar.getString("mailTone", RingtoneManager.getDefaultUri(2).toString()));
    }

    public void b(Context context) {
        q qVar = new q(context);
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e2) {
            j.a("SettingsManager", e2);
        }
        messageDigest.reset();
        messageDigest.update(r.i().k().getBytes());
        String bigInteger = new BigInteger(1, messageDigest.digest()).toString(16);
        this.l = qVar.getString("pin@" + bigInteger, "");
        messageDigest.reset();
        this.k = Boolean.valueOf(qVar.getBoolean("lock" + bigInteger, false)).booleanValue();
        j = true;
    }

    public void c(Context context) {
        a(context, "", "");
    }

    public void d(Context context) {
        a(context, r.i().k(), r.i().l());
    }

    public void a(Context context, String str, String str2) {
        net.client.by.lock.f.r a2 = new q(context).edit();
        String a3 = a((Boolean) false);
        a2.a("usr", str, a3);
        a2.a("psw", str2, a3);
        a2.commit();
    }

    private String a(Boolean bool) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyyHH");
        try {
            Date date = new Date();
            if (bool.booleanValue()) {
                date.setTime(date.getTime() - 3600000);
            }
            return String.valueOf("letItBE!1") + simpleDateFormat.format(date);
        } catch (Exception e2) {
            e2.printStackTrace();
            return "letItBE!1";
        }
    }

    public void e(Context context) {
        try {
            q qVar = new q(context);
            String a2 = a((Boolean) false);
            String a3 = qVar.a("usr", "", a2);
            if (a3 == null || a3.equals("")) {
                a2 = a((Boolean) true);
                a3 = qVar.a("usr", "", a2);
            }
            r.i().d(a3);
            r.i().f(qVar.a("psw", "", a2));
        } catch (Exception e2) {
            b.e("SettingsManager", "loadusr failed:" + e2.toString());
        }
    }

    public void f(Context context) {
        net.client.by.lock.f.r a2 = new q(context).edit();
        a2.putString("downloadDirectory", this.b);
        a2.putString("cache", this.d);
        a2.putInt("frequency", this.c);
        String str = "";
        Iterator it = this.e.iterator();
        while (it.hasNext()) {
            str = String.valueOf(str) + ((Integer) it.next()).intValue() + ";";
        }
        a2.putString("expandedGroups", str);
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e2) {
            j.a("SettingsManager", e2);
        }
        a2.putString("ringTone", this.f.toString());
        a2.putString("msgTone", this.h.toString());
        a2.putString("fileTone", this.g.toString());
        a2.putString("mailTone", this.i.toString());
        messageDigest.reset();
        messageDigest.update(r.i().k().getBytes());
        String bigInteger = new BigInteger(1, messageDigest.digest()).toString(16);
        a2.putString("pin@" + bigInteger, this.l);
        a2.putBoolean("lock@" + bigInteger, this.k);
        if (!a2.commit()) {
            b.e("SettingsManager", "save, cannot commit");
        }
    }

    public ArrayList c() {
        return this.e;
    }

    public void a(ArrayList arrayList) {
        this.e = arrayList;
    }

    public int d() {
        return this.c;
    }

    public void a(int i2) {
        this.c = i2;
    }

    public String e() {
        return this.d;
    }

    public void a(String str) {
        this.d = str;
    }

    public Uri g(Context context) {
        if (RingtoneManager.getRingtone(context, this.f) == null) {
            this.f = RingtoneManager.getDefaultUri(1);
        }
        return this.f;
    }

    public Uri h(Context context) {
        if (RingtoneManager.getRingtone(context, this.g) == null) {
            this.g = RingtoneManager.getDefaultUri(2);
        }
        return this.g;
    }

    public Uri i(Context context) {
        if (RingtoneManager.getRingtone(context, this.h) == null) {
            this.h = RingtoneManager.getDefaultUri(2);
        }
        return this.h;
    }

    public Uri j(Context context) {
        if (RingtoneManager.getRingtone(context, this.i) == null) {
            this.i = RingtoneManager.getDefaultUri(2);
        }
        return this.i;
    }

    public void a(boolean z) {
        this.k = z;
    }

    public boolean f() {
        return this.k;
    }

    public void b(String str) {
        this.l = str;
    }

    public String g() {
        if (this.l.equals("")) {
            this.l = "1234";
        }
        return this.l;
    }

    public static boolean h() {
        return j;
    }
}
