package net.client.by.lock.c;

import com.google.android.gms.d.g;
import com.google.android.gms.d.h;

/* compiled from: MyApp */
public class c {
    private static final String a = c.class.getSimpleName();
    private static g b = null;
    private static f c = f.Idle;
    private static h d = null;

    private c() {
    }

    public static g a() {
        return b;
    }

    public static void a(g gVar) {
        b = gVar;
    }

    public static f b() {
        return c;
    }

    public static void a(f fVar) {
        c = fVar;
    }

    public static h c() {
        if (d == null) {
            d = new d();
        }
        return d;
    }

    public static void d() {
        new e().execute(new Void[0]);
    }
}
