package net.client.by.lock.c;

import java.util.Comparator;
import net.client.by.lock.d.c;
import net.client.by.lock.d.s;

/* compiled from: MyApp */
class k implements Comparator {
    final /* synthetic */ i a;

    k(i iVar) {
        this.a = iVar;
    }

    /* renamed from: a */
    public int compare(s sVar, s sVar2) {
        if (!(sVar.d() instanceof c) || !(sVar2.d() instanceof c)) {
            return 0;
        }
        return ((c) sVar.d()).G().compareToIgnoreCase(((c) sVar2.d()).G());
    }
}
