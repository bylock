package net.client.by.lock.c;

import android.content.Context;
import net.client.by.lock.gui.activity.MainActivity;
import net.client.by.lock.service.BackgroundService;

/* compiled from: MyApp */
public class g {
    private static g a = null;
    private Context b;

    private g() {
    }

    public static g a() {
        if (a == null) {
            a = new g();
        }
        return a;
    }

    public Context b() {
        if (this.b == null) {
            if (MainActivity.i() == null) {
                return BackgroundService.a();
            }
            this.b = MainActivity.i();
        }
        return this.b;
    }

    public void a(Context context) {
        this.b = context;
    }
}
