package net.client.by.lock.c;

import android.content.Context;
import android.content.Intent;
import net.client.by.lock.a.c;
import net.client.by.lock.b;
import net.client.by.lock.service.BackgroundService;

/* compiled from: MyApp */
public class l {
    private static l a = null;
    private Context b;

    public static l a() {
        try {
            if (a == null) {
                a = new l();
            }
        } catch (Exception e) {
            b.e("ServiceManager", e.toString());
        }
        return a;
    }

    /* access modifiers changed from: package-private */
    public Context b() {
        if (this.b == null) {
            return BackgroundService.a();
        }
        return this.b;
    }

    public synchronized void a(Context context) {
        this.b = context;
        this.b.startService(new Intent(this.b, BackgroundService.class));
    }

    public synchronized void c() {
        BackgroundService a2 = BackgroundService.a();
        if (a2 != null) {
            a2.stopService(new Intent(a2, BackgroundService.class));
        }
    }

    public void a(c cVar) {
        Intent intent = new Intent("by.lock.BackgroundService");
        intent.putExtra("what", 4);
        intent.putExtra("id", cVar.E().K());
        intent.putExtra("callId", cVar.q());
        b().sendBroadcast(intent);
        b.i("ServiceManager", "call.getId() " + cVar.q().length);
    }

    public void d() {
        Intent intent = new Intent("by.lock.BackgroundService");
        intent.putExtra("what", 5);
        b().sendBroadcast(intent);
    }

    public void e() {
        Intent intent = new Intent("by.lock.BackgroundService");
        intent.putExtra("what", 7);
        b().sendBroadcast(intent);
    }

    public void a(net.client.by.lock.d.c cVar, int i) {
        Intent intent = new Intent("by.lock.BackgroundService");
        intent.putExtra("what", 9);
        intent.putExtra("friendUserId", cVar.K());
        intent.putExtra("arg1", i);
        intent.putExtra("arg2", 1);
        b().sendBroadcast(intent);
    }

    public void b(net.client.by.lock.d.c cVar, int i) {
        Intent intent = new Intent("by.lock.BackgroundService");
        intent.putExtra("what", 9);
        intent.putExtra("friendUserId", cVar.K());
        intent.putExtra("arg1", i);
        intent.putExtra("arg2", 0);
        b().sendBroadcast(intent);
    }

    public void a(String str) {
        Intent intent = new Intent("by.lock.BackgroundService");
        intent.putExtra("what", 8);
        intent.putExtra("error", str);
        b().sendBroadcast(intent);
    }

    public void c(net.client.by.lock.d.c cVar, int i) {
        Intent intent = new Intent("by.lock.BackgroundService");
        intent.putExtra("what", i);
        intent.putExtra("friendUserId", cVar.K());
        intent.putExtra("arg1", 1);
        b().sendBroadcast(intent);
    }

    public void a(int i) {
        Intent intent = new Intent("by.lock.BackgroundService");
        intent.putExtra("what", i);
        intent.putExtra("arg1", 0);
        b().sendBroadcast(intent);
    }
}
