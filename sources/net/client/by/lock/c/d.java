package net.client.by.lock.c;

import com.google.android.gms.d.a;
import com.google.android.gms.d.g;
import com.google.android.gms.d.h;
import java.util.ArrayList;
import java.util.StringTokenizer;
import net.client.by.lock.b;

/* compiled from: MyApp */
class d implements h {
    d() {
    }

    @Override // com.google.android.gms.d.h
    public void a(g gVar, String str) {
        synchronized (c.e()) {
            c.a(gVar);
            a c = gVar.c();
            net.client.by.lock.a.a(c.a("domain"));
            b.i(c.f(), "Loaded domain: " + net.client.by.lock.a.a());
            try {
                ArrayList arrayList = new ArrayList();
                StringTokenizer stringTokenizer = new StringTokenizer(c.a("ips"), ";");
                while (stringTokenizer.hasMoreElements()) {
                    arrayList.add(stringTokenizer.nextToken());
                }
                if (arrayList.size() > 0) {
                    net.client.by.lock.a.c.clear();
                    net.client.by.lock.a.c.addAll(arrayList);
                }
                b.i(c.f(), "Number of ips: " + net.client.by.lock.a.c.size());
            } catch (Exception e) {
                b.e(c.f(), "Error Loading ips: " + e);
            }
            try {
                net.client.by.lock.a.b = Integer.valueOf(c.a("interval")).intValue();
                b.i(c.f(), "Interval: " + net.client.by.lock.a.b);
            } catch (Exception e2) {
                b.e(c.f(), "Error Loading interval: " + e2);
            }
            c.b(f.Loadeed);
        }
    }
}
