package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.n;
import net.client.by.lock.d.b;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class SendChatTask extends a {
    private b chat;

    public SendChatTask(Context context, Object obj) {
        super(context, obj);
        this.chat = (b) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.d();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        if (!this.chat.g() || !r.i().p().b(this.chat)) {
            return null;
        }
        return c.a(this.chat);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().a(this.chat);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        this.chat.d(d.f(document));
        this.chat.e("COMPLETED");
        return new String[]{"onSuccess"};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            this.chat.A().a();
            this.chat.A().a(8);
        } else if (objArr[0].equals("onException")) {
            this.chat.f(((Throwable) objArr[1]).toString());
        } else if (objArr[0].equals("onFail")) {
            this.chat.f((String) objArr[1]);
        }
    }
}
