package net.client.by.lock.task;

import android.content.Context;
import java.util.ArrayList;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class CreateGroupWithFriendTask extends a {
    c friend;
    String groupName;

    public CreateGroupWithFriendTask(Context context, Object obj) {
        super(context, obj);
        this.groupName = (String) ((ArrayList) obj).get(0);
        this.friend = (c) ((ArrayList) obj).get(1);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.u();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return net.client.by.lock.e.c.a(this.friend, this.groupName);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().a(this.friend, this.groupName);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            d.a((Document) objArr[1], this.startupParameter);
        }
    }
}
