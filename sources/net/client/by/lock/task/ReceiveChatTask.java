package net.client.by.lock.task;

import android.content.Context;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.c.n;
import net.client.by.lock.d.b;
import net.client.by.lock.d.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class ReceiveChatTask extends a {
    private c friend;

    public ReceiveChatTask(Context context, Object obj) {
        super(context, obj);
        this.friend = (c) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.e();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return net.client.by.lock.e.c.a(this.friend);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().c(this.friend);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        return new Object[]{"onSuccess", d.a(document, (c) this.startupParameter)};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            c cVar = (c) this.startupParameter;
            Iterator it = ((ArrayList) objArr[1]).iterator();
            while (it.hasNext()) {
                cVar.b((b) it.next());
            }
            cVar.b();
        }
    }
}
