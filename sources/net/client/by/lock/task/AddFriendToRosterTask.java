package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import net.client.by.lock.gui.activity.ae;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class AddFriendToRosterTask extends a {
    Object[] parameter;

    public AddFriendToRosterTask(Context context, Object obj) {
        super(context, obj);
        this.parameter = (Object[]) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.g();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.a(new String[]{(String) this.parameter[0], (String) this.parameter[1]});
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            d.a((Document) objArr[1], (Boolean) false);
            if (this.parameter.length > 2 && this.parameter[2] != null && (this.parameter[2] instanceof ae)) {
                ae aeVar = (ae) this.parameter[2];
                if (aeVar.S != null) {
                    aeVar.S.dismiss();
                }
                ae.P.setText("");
                ae.Q.setText("");
            }
        } else if (this.parameter.length > 2 && this.parameter[2] != null && (this.parameter[2] instanceof ae)) {
            ae aeVar2 = (ae) this.parameter[2];
            if (aeVar2.S != null) {
                aeVar2.S.dismiss();
            }
        }
    }
}
