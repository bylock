package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.i;
import net.client.by.lock.c.n;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class RemoveFriendFromGroupTask extends a {
    public RemoveFriendFromGroupTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.F();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.c(((Integer[]) this.startupParameter)[0], ((Integer[]) this.startupParameter)[1]);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            i.c().b(((Integer[]) this.startupParameter)[0], ((Integer[]) this.startupParameter)[1]);
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().b(((Integer[]) this.startupParameter)[0], ((Integer[]) this.startupParameter)[1]);
    }
}
