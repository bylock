package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.i;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class SetNewPasswordTask extends a {
    public SetNewPasswordTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.x();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.f();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess") || objArr[0].equals("onFail") || !objArr[0].equals("onException") || r.i().h()) {
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().e();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        net.client.by.lock.c.c().b();
        i.c().g();
        return new Object[]{"onSuccess", document};
    }
}
