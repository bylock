package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class UpdateStatusTask extends a {
    Integer status;

    public UpdateStatusTask(Context context, Object obj) {
        super(context, obj);
        this.status = (Integer) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.B();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.c(this.status);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
    }
}
