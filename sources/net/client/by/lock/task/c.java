package net.client.by.lock.task;

import java.net.HttpURLConnection;
import java.net.URLConnection;

/* compiled from: MyApp */
public class c implements Runnable {
    Thread a;
    URLConnection b;

    public c(Thread thread, URLConnection uRLConnection) {
        this.a = thread;
        this.b = uRLConnection;
    }

    public void run() {
        try {
            Thread.sleep(60000);
            HttpURLConnection httpURLConnection = (HttpURLConnection) this.b;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
