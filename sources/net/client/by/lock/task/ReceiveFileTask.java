package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.b.g;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class ReceiveFileTask extends FileTask {
    g fileTransfer;

    public ReceiveFileTask(Context context, Object obj) {
        super(context, obj);
        this.fileTransfer = (g) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.l();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.a(this.fileTransfer.b());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0107  */
    @Override // net.client.by.lock.task.a
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.w3c.dom.Document call() {
        /*
        // Method dump skipped, instructions count: 290
        */
        throw new UnsupportedOperationException("Method not decompiled: net.client.by.lock.task.ReceiveFileTask.call():org.w3c.dom.Document");
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        if (this.fileTransfer != null) {
            this.fileTransfer.g();
            if (this.fileTransfer.i() != null) {
                r.i().b(this.fileTransfer.i());
            }
        }
        return new Object[]{"onSuccess", document};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onException(Throwable th) {
        this.fileTransfer.a(th);
        return new Object[]{"onException", th};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
    }
}
