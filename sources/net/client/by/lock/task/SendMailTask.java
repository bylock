package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.d.n;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import net.client.by.lock.gui.activity.aw;
import net.client.by.lock.gui.b.an;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class SendMailTask extends a {
    n mail = ((n) this.parameter[0]);
    Object[] parameter;

    public SendMailTask(Context context, Object obj) {
        super(context, obj);
        this.parameter = (Object[]) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.y();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onException(Throwable th) {
        this.mail.f(th.getMessage());
        return new Object[]{"onException", th};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onFailAtServer(String str) {
        this.mail.f(str);
        return new Object[]{"onFail", str};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        if (!this.mail.e() || !r.i().p().a(this.mail)) {
            return null;
        }
        return c.a(this.mail);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        this.mail.d(d.f(document));
        this.mail.e("COMPLETED");
        return new String[]{"onSuccess"};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            this.mail.A().a();
            this.mail.A().a(8);
            an.a(this.ctx, "Mail Sent!!");
            if (this.parameter.length > 1 && this.parameter[1] != null && (this.parameter[1] instanceof aw)) {
                aw awVar = (aw) this.parameter[1];
                if (awVar.P != null) {
                    awVar.P.dismiss();
                }
                awVar.A();
            }
        } else if (this.parameter.length > 1 && this.parameter[1] != null && (this.parameter[1] instanceof aw)) {
            aw awVar2 = (aw) this.parameter[1];
            if (awVar2.P != null) {
                awVar2.P.dismiss();
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
    }
}
