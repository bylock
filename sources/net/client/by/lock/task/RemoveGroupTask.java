package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.a;
import net.client.by.lock.c.i;
import net.client.by.lock.c.n;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class RemoveGroupTask extends a {
    public RemoveGroupTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.v();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.b((Integer) this.startupParameter);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().c((Integer) this.startupParameter);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            i.c().a(((Integer) this.startupParameter).intValue());
            a.e.notifyDataSetChanged();
        }
    }
}
