package net.client.by.lock.task;

import android.content.Context;
import java.io.File;
import java.util.Iterator;
import net.client.by.lock.b;
import net.client.by.lock.b.c;
import net.client.by.lock.b.h;
import net.client.by.lock.b.k;
import net.client.by.lock.c.i;
import net.client.by.lock.c.n;
import net.client.by.lock.e.d;
import net.client.by.lock.f.a;
import net.client.by.lock.f.p;
import net.client.by.lock.gui.a.m;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class SendFileTask extends FileTask {
    private static final String TAG = "SendFileTask";
    h fileTransfer;

    public SendFileTask(Context context, Object obj) {
        super(context, obj);
        this.fileTransfer = (h) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.k();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b8  */
    @Override // net.client.by.lock.task.a
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.w3c.dom.Document call() {
        /*
        // Method dump skipped, instructions count: 205
        */
        throw new UnsupportedOperationException("Method not decompiled: net.client.by.lock.task.SendFileTask.call():org.w3c.dom.Document");
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        String[] e = d.e(document);
        boolean z = false;
        if (this.fileTransfer.b() == null) {
            this.fileTransfer.a(e[0]);
        } else {
            z = true;
        }
        this.fileTransfer.d(e[1]);
        k.b().a(this.fileTransfer);
        this.fileTransfer.g();
        this.fileTransfer.A().a(8);
        return new Object[]{"onSuccess", z};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().a(this.fileTransfer);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a, net.client.by.lock.task.FileTask
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            try {
                File j = this.fileTransfer.j();
                if (new File(a.a(), j.getName()).exists()) {
                    j.delete();
                }
            } catch (Exception e) {
                b.i(TAG, "file could not be deleted");
            }
            if (((Boolean) objArr[1]).booleanValue()) {
                Iterator it = this.fileTransfer.l().iterator();
                while (it.hasNext()) {
                    i.c().a(((c) it.next()).e()).b(this.fileTransfer);
                }
                return;
            }
            ((m) this.fileTransfer.A()).a();
        }
    }
}
