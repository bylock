package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.n;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class AddFriendToGroupTask extends a {
    public AddFriendToGroupTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.E();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.b(((Integer[]) this.startupParameter)[0], ((Integer[]) this.startupParameter)[1]);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        Integer[] numArr = (Integer[]) this.startupParameter;
        n.a().a(numArr[0], numArr[1]);
    }
}
