package net.client.by.lock.task.call;

import android.content.Context;
import net.client.by.lock.a.c;
import net.client.by.lock.c.n;
import net.client.by.lock.f.p;
import net.client.by.lock.task.a;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class RejectCallTask extends a {
    c call;

    public RejectCallTask(Context context, Object obj) {
        super(context, obj);
        this.call = (c) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.r();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return net.client.by.lock.e.c.b(this.call.q());
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().c((c) this.startupParameter);
    }
}
