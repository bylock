package net.client.by.lock.task.call;

import android.content.Context;
import net.client.by.lock.a.c;
import net.client.by.lock.c.n;
import net.client.by.lock.f.p;
import net.client.by.lock.task.a;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class CancelCallTask extends a {
    byte[] callId;

    public CancelCallTask(Context context, Object obj) {
        super(context, obj);
        this.callId = ((c) obj).q();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.s();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return net.client.by.lock.e.c.c(this.callId);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().d((c) this.startupParameter);
    }
}
