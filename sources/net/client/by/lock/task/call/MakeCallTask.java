package net.client.by.lock.task.call;

import android.app.Activity;
import android.content.Context;
import net.client.by.lock.a.c;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.e.d;
import net.client.by.lock.f.j;
import net.client.by.lock.f.p;
import net.client.by.lock.task.a;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class MakeCallTask extends a {
    c call;

    public MakeCallTask(Context context, Object obj) {
        super(context, obj);
        this.call = (c) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.p();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        if (!this.call.x() || !r.i().p().b(this.call)) {
            return null;
        }
        return net.client.by.lock.e.c.a(this.call);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().a(this.call);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        String[] a = d.a(document, (c) this.startupParameter);
        if (a == null) {
            return new String[]{"onFail"};
        }
        String str = a[0];
        String str2 = a[1];
        this.call.d(a[2]);
        if (str.equalsIgnoreCase("CALLEE IS BUSY") || str.equalsIgnoreCase("CALLER IS BUSY")) {
            return new Object[]{"onSuccess", str};
        } else if (str.length() == 0) {
            return new String[]{"onFail"};
        } else {
            byte[] a2 = j.a(str);
            byte[] a3 = j.a(str2);
            if (a2 == null || a3 == null) {
                return new String[]{"onFail"};
            }
            return new Object[]{"onSuccess", a2, a3};
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (!objArr[0].equals("onSuccess")) {
            this.call.t();
            if (this.ctx instanceof Activity) {
                ((Activity) this.ctx).finish();
            }
        } else if (!(objArr[1] instanceof String)) {
            this.call.a((byte[]) objArr[1], (byte[]) objArr[2]);
        } else if (((String) objArr[1]).equalsIgnoreCase("CALLEE IS BUSY")) {
            this.call.p();
        } else if (((String) objArr[1]).equalsIgnoreCase("CALLER IS BUSY")) {
            this.call.o();
        }
    }
}
