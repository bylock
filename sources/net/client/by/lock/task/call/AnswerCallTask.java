package net.client.by.lock.task.call;

import android.content.Context;
import net.client.by.lock.a.c;
import net.client.by.lock.c.n;
import net.client.by.lock.e.b;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import net.client.by.lock.task.a;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class AnswerCallTask extends a {
    c call;

    public AnswerCallTask(Context context, Object obj) {
        super(context, obj);
        this.call = (c) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.q();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return net.client.by.lock.e.c.a(this.call.q());
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().b((c) this.startupParameter);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        String[] b = d.b(document, (c) this.startupParameter);
        if (b == null) {
            return new String[]{"onFail"};
        }
        this.call.a(b.a(b[0]), b.a(b[1]), b.a(b[2]), Integer.parseInt(b[3]));
        return new Object[]{"onSuccess"};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (!objArr[0].equals("onSuccess")) {
            this.call.t();
        }
    }
}
