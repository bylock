package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import net.client.by.lock.gui.b.an;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class ChangePasswordTask extends a {
    String oldPassword;

    public ChangePasswordTask(Context context, Object obj) {
        super(context, obj);
        this.oldPassword = (String) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.o();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.e();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().c(this.oldPassword);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onException(Throwable th) {
        r.i().f((String) this.startupParameter);
        r.i().p().c();
        return new Object[]{"onException", th};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        if (objArr[0].equals("onSuccess")) {
            an.a(this.ctx, "Password has changed successfully.");
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onFailAtServer(String str) {
        r.i().f((String) this.startupParameter);
        r.i().p().c();
        return new Object[]{"onFailAtServer", str};
    }
}
