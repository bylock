package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.n;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class DeleteFileTransferTask extends a {
    public DeleteFileTransferTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.m();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.b((String) this.startupParameter);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().d((String) this.startupParameter);
    }
}
