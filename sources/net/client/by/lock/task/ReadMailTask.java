package net.client.by.lock.task;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import net.client.by.lock.c.n;
import net.client.by.lock.d.l;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import net.client.by.lock.gui.activity.ReadIMailActivity;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class ReadMailTask extends a {
    l mail = ((l) this.startupParameter);

    public ReadMailTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.z();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.d(this.mail.i());
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().a(this.mail);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        d.a(document, this.mail);
        if (((String) this.mail.F().a()).equals("COMPLETED")) {
            this.mail.b();
        }
        return new Object[]{"onSuccess", Html.fromHtml(this.mail.f())};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onException(Throwable th) {
        this.mail.f(th.getMessage());
        return new Object[]{"onException", th};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onFailAtServer(String str) {
        this.mail.f(str);
        return new Object[]{"onFail", str};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess") && (this.ctx instanceof ReadIMailActivity)) {
            ReadIMailActivity readIMailActivity = (ReadIMailActivity) this.ctx;
            readIMailActivity.o.setText((Spanned) objArr[1]);
            readIMailActivity.u.a(this.mail.l());
            readIMailActivity.i();
            readIMailActivity.b(0);
        }
    }
}
