package net.client.by.lock.task;

import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import javax.xml.transform.TransformerException;
import net.client.by.lock.b;
import net.client.by.lock.c.g;
import net.client.by.lock.c.l;
import net.client.by.lock.c.n;
import net.client.by.lock.d.o;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.j;
import net.client.by.lock.f.p;
import net.client.by.lock.gui.activity.MainActivity;
import net.client.by.lock.gui.b.an;
import org.w3c.dom.Document;
import org.xml.sax.SAXParseException;

/* compiled from: MyApp */
public class GetRosterEventTask extends a {
    public static final String TAG = "GetRosterEventTask";
    public static int rosterEventErrorCount = 0;
    String[] fileTransferIds;
    Set lastChatIds;
    String[] mailIds;

    public GetRosterEventTask(Context context, Object obj) {
        super(context, obj);
        b.v(TAG, "roster create");
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.f();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        this.mailIds = r.i().d();
        this.fileTransferIds = r.i().e();
        this.lastChatIds = r.i().c();
        return c.a(r.i().s(), this.mailIds, this.fileTransferIds, this.lastChatIds);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().f();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        b.v(TAG, "roster success");
        rosterEventErrorCount = 0;
        try {
            ArrayList b = d.b(document);
            r.i().a(this.mailIds, this.fileTransferIds, this.lastChatIds);
            Intent intent = new Intent(MainActivity.t);
            intent.putExtra("caption", "");
            g.a().b().sendBroadcast(intent);
            l.a().d();
            if (!((Boolean) r.i().t().a()).booleanValue()) {
                r.i().t().a((Object) true);
            }
            ArrayList arrayList = new ArrayList();
            Iterator it = b.iterator();
            while (it.hasNext()) {
                o oVar = (o) it.next();
                if (oVar.a().equalsIgnoreCase("ANSWER CALL")) {
                    oVar.b();
                } else {
                    arrayList.add(oVar);
                }
            }
            return new Object[]{"onSuccess", arrayList};
        } catch (Exception e) {
            an.a(this.ctx, "Some kind of error occured, unexpected behaviour may occur");
            b.e(TAG, "-=GET ROSTER EVENT LEAK=-");
            j.a(TAG, e);
            b.e(TAG, "-=GET ROSTER EVENT LEAK=-");
            Object[] objArr = {"onFail", document};
            l.a().d();
            if (!((Boolean) r.i().t().a()).booleanValue()) {
                r.i().t().a((Object) true);
            }
            return objArr;
        } catch (Throwable th) {
            l.a().d();
            if (!((Boolean) r.i().t().a()).booleanValue()) {
                r.i().t().a((Object) true);
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            Iterator it = ((ArrayList) objArr[1]).iterator();
            while (it.hasNext()) {
                ((o) it.next()).b();
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onException(Throwable th) {
        rosterEventErrorCount++;
        if ((th instanceof TransformerException) || (th instanceof SAXParseException)) {
            l.a().e();
        } else if (rosterEventErrorCount < 5) {
            l.a().d();
        } else {
            if (((Boolean) r.i().t().a()).booleanValue()) {
                r.i().t().a((Object) false);
            }
            l.a().e();
        }
        return new Object[]{"onException", th};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onFailAtServer(String str) {
        if (str.contains("SessionClosedException") || str.contains("SessionExpiredException")) {
            if (((Boolean) r.i().t().a()).booleanValue()) {
                r.i().t().a((Object) false);
            }
            l.a().e();
            return new Object[]{"onFail", str};
        }
        rosterEventErrorCount++;
        if (rosterEventErrorCount < 5) {
            l.a().d();
        } else {
            if (((Boolean) r.i().t().a()).booleanValue()) {
                r.i().t().a((Object) false);
            }
            l.a().e();
        }
        return new Object[]{"onFail", str};
    }
}
