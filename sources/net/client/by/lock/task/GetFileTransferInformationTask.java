package net.client.by.lock.task;

import android.content.Context;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.b.g;
import net.client.by.lock.c.n;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class GetFileTransferInformationTask extends a {
    String fileId;
    ArrayList fileList;

    public GetFileTransferInformationTask(Context context, Object obj) {
        super(context, obj);
        this.fileId = (String) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.n();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.c(this.fileId);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        this.fileList = d.a(document);
        return new String[]{"onSuccess"};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            Iterator it = this.fileList.iterator();
            while (it.hasNext()) {
                g gVar = (g) it.next();
                if (!gVar.E().a(gVar)) {
                    n.a().d(gVar.i());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().b(this.fileId);
    }
}
