package net.client.by.lock.task;

import android.content.Context;
import android.content.Intent;
import javax.net.ssl.SSLPeerUnverifiedException;
import net.client.by.lock.b;
import net.client.by.lock.c.a;
import net.client.by.lock.c.g;
import net.client.by.lock.c.i;
import net.client.by.lock.c.l;
import net.client.by.lock.c.m;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import net.client.by.lock.gui.activity.LoginActivity;
import net.client.by.lock.gui.activity.MainActivity;
import net.client.by.lock.gui.activity.PrngActivity;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class LoginTask extends a {
    public LoginTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.a();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        r.a(false);
        Intent intent = new Intent(MainActivity.t);
        intent.putExtra("caption", "Connecting");
        g.a().b().sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.a();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onCancel() {
        if (this.ctx instanceof LoginActivity) {
            ((LoginActivity) this.ctx).f.setEnabled(true);
            ((LoginActivity) this.ctx).a(false);
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().b();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        try {
            i.d();
            d.c(document);
            r.i().t().a((Object) true);
            m.a().b(this.ctx);
            Intent intent = new Intent(MainActivity.t);
            intent.putExtra("caption", "");
            g.a().b().sendBroadcast(intent);
            return new Object[]{"onSuccess"};
        } catch (Exception e) {
            return new Object[]{"onException", document, e};
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (this.ctx instanceof LoginActivity) {
            ((LoginActivity) this.ctx).f.setEnabled(true);
        }
        if (!r.i().h()) {
            if (objArr[0].equals("onSuccess")) {
                if (this.ctx instanceof LoginActivity) {
                    LoginActivity loginActivity = (LoginActivity) this.ctx;
                    if (r.i().q()) {
                        a.a(this.ctx);
                        loginActivity.a(false);
                        this.ctx.startActivity(new Intent(this.ctx, PrngActivity.class));
                        loginActivity.finish();
                        n.a().f();
                        return;
                    }
                    b.e("Logintask", "session is not logged in");
                } else if (r.i().q()) {
                    a.a(this.ctx);
                    n.a().f();
                }
            } else if (objArr[0].equals("onFail")) {
                if (this.ctx instanceof LoginActivity) {
                    LoginActivity loginActivity2 = (LoginActivity) this.ctx;
                    loginActivity2.a(false);
                    loginActivity2.e.setError((String) objArr[1]);
                    loginActivity2.e.requestFocus();
                    return;
                }
                l.a().e();
            } else if (!objArr[0].equals("onException")) {
                l.a().e();
            } else if (this.ctx instanceof LoginActivity) {
                ((LoginActivity) this.ctx).a(false);
            } else {
                l.a().e();
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onException(Throwable th) {
        if (r.i().h()) {
            return new Object[]{"onException", th};
        }
        if (th instanceof SSLPeerUnverifiedException) {
            l.a().a("Certification error: Check your internet settings!");
        } else {
            l.a().a(th.getMessage());
        }
        return new Object[]{"onException", th};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onFailAtServer(String str) {
        if (r.i().h()) {
            return new Object[]{"onFail", str};
        }
        l.a().a(str);
        return new Object[]{"onFail", str};
    }
}
