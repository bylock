package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.n;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class GetFriendInformationTask extends a {
    Integer userId;

    public GetFriendInformationTask(Context context, Object obj) {
        super(context, obj);
        this.userId = (Integer) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.h();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.a(this.userId);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        d.a(document, (Boolean) false);
        return new String[]{"onSuccess"};
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().a(this.userId);
    }
}
