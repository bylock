package net.client.by.lock.task;

import android.content.Context;
import android.content.Intent;
import net.client.by.lock.b;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.e.d;
import net.client.by.lock.f.p;
import net.client.by.lock.gui.activity.PrngActivity;
import net.client.by.lock.gui.activity.RegisterActivity;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class RegisterTask extends a {
    private static final String TAG = "RegisterTask";
    String[] parameter;

    public RegisterTask(Context context, Object obj) {
        super(context, obj);
        this.parameter = (String[]) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.G();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.a(this.parameter[0], this.parameter[1]);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().a(this.parameter[0], this.parameter[1]);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onSuccess(Document document) {
        try {
            if (d.d(document)) {
                r.i().d(this.parameter[0]);
                r.i().f(this.parameter[1]);
                return new Object[]{"onSuccess"};
            }
            return new Object[]{"onFail"};
        } catch (Exception e) {
            return new Object[]{"onException", document, e};
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (this.ctx instanceof RegisterActivity) {
            RegisterActivity registerActivity = (RegisterActivity) this.ctx;
            registerActivity.e.dismiss();
            if (objArr[0].equals("onSuccess")) {
                this.ctx.startActivity(new Intent(this.ctx, PrngActivity.class));
                registerActivity.finish();
                n.a().f();
            } else if (objArr[0].equals("onException")) {
                b.e(TAG, "onException");
            } else if (objArr[0].equals("onFail")) {
                b.e(TAG, "onException");
            }
        }
    }
}
