package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.n;
import net.client.by.lock.d.b;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class DeleteChatTask extends a {
    b chat = ((b) this.startupParameter);

    public DeleteChatTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.D();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.a(Integer.valueOf(this.chat.E().K()), this.chat.c());
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().b(this.chat);
    }
}
