package net.client.by.lock.task;

import android.app.Activity;
import android.content.Context;
import net.client.by.lock.c.g;
import net.client.by.lock.c.l;
import net.client.by.lock.d.r;
import net.client.by.lock.e.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class LogoutTask extends a {
    public LogoutTask(Context context, Object obj) {
        super(context, obj);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.b();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return c.b();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        exit();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onCancel() {
        exit();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        exit();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] onFailAtServer(String str) {
        exit();
        return new Object[]{"onFail", str};
    }

    /* access modifiers changed from: package-private */
    public void exit() {
        r.a(true);
        if (this.ctx instanceof Activity) {
            ((Activity) this.ctx).finish();
        }
        Context b = g.a().b();
        if (b instanceof Activity) {
            ((Activity) b).finish();
        }
        l.a().c();
    }
}
