package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class RenameFriendTask extends a {
    c friend;

    public RenameFriendTask(Context context, Object obj) {
        super(context, obj);
        this.friend = (c) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.i();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return net.client.by.lock.e.c.b(this.friend);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().a(this.friend);
    }
}
