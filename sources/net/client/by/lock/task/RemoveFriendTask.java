package net.client.by.lock.task;

import android.content.Context;
import net.client.by.lock.c.i;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;
import net.client.by.lock.f.p;
import org.w3c.dom.Document;

/* compiled from: MyApp */
public class RemoveFriendTask extends a {
    c friend;

    public RemoveFriendTask(Context context, Object obj) {
        super(context, obj);
        this.friend = (c) obj;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public String getUrl() {
        return p.j();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Document getRequest() {
        return net.client.by.lock.e.c.c(this.friend);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void restart() {
        n.a().b(this.friend);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        super.onPostExecute(objArr);
        if (objArr[0].equals("onSuccess")) {
            i.c().b((c) this.startupParameter);
        }
    }
}
