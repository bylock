package net.client.by.lock.task;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.PowerManager;
import android.os.Process;
import javax.xml.transform.TransformerException;
import net.client.by.lock.b;
import net.client.by.lock.c.l;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.service.BackgroundService;
import org.apache.http.client.methods.HttpPost;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

/* compiled from: MyApp */
public abstract class a extends AsyncTask {
    public static final int CANCELLED = 3;
    public static final int FAILED = 2;
    public static final int PENDING = 0;
    public static final int RUNNING = 1;
    public static final int SUCCEEDED = 4;
    protected static int errorCount = 0;
    private static int logRequestId = 0;
    private final String TAG = "GenericTask";
    public Context ctx;
    private int logReqId = 0;
    protected HttpPost post;
    protected Object startupParameter;
    int state = 0;
    public String taskId;
    private PowerManager.WakeLock wakeLock;
    private WifiManager.WifiLock wifiLock;

    /* access modifiers changed from: protected */
    public abstract Document getRequest();

    /* access modifiers changed from: protected */
    public abstract String getUrl();

    /* access modifiers changed from: protected */
    public abstract void restart();

    public a(Context context, Object obj) {
        this.startupParameter = obj;
        this.ctx = context;
        this.post = null;
        initLock(context);
    }

    /* access modifiers changed from: protected */
    public void lock() {
        try {
            this.wakeLock.acquire();
            this.wifiLock.acquire();
        } catch (Exception e) {
            b.e("WlanSilencer", "Error getting Lock: " + e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void unlock() {
        if (this.wakeLock.isHeld()) {
            this.wakeLock.release();
        }
        if (this.wifiLock.isHeld()) {
            this.wifiLock.release();
        }
    }

    private void initLock(Context context) {
        this.wifiLock = ((WifiManager) context.getSystemService("wifi")).createWifiLock(1, "GenericTaskWifiLock");
        this.wakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "GenericTaskWakeLock");
    }

    public boolean cancelTask(boolean z) {
        if (this.post != null && z) {
            new Thread(new b(this)).start();
        }
        return super.cancel(z);
    }

    public void execute() {
        this.state = 1;
        if (Build.VERSION.SDK_INT >= 11) {
            startNew();
        } else {
            startOld();
        }
    }

    private void startOld() {
        super.execute(new Object[0]);
    }

    @TargetApi(11)
    private void startNew() {
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object[] objArr) {
        n.a().a(this.taskId);
        unlock();
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        this.state = 3;
        genericOnCancel();
        unlock();
    }

    /* access modifiers changed from: protected */
    @Override // android.os.AsyncTask
    public Object[] doInBackground(Object... objArr) {
        lock();
        Process.setThreadPriority(-8);
        try {
            Document call = call();
            if (!isCancelled()) {
                return genericOnSuccess(call);
            }
            this.state = 3;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            if (!isCancelled()) {
                return genericOnException(e);
            }
            this.state = 3;
            return null;
        }
    }

    public String getTaskId() {
        return this.taskId;
    }

    public void setTaskId(String str) {
        this.taskId = str;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x011b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.w3c.dom.Document call() {
        /*
        // Method dump skipped, instructions count: 380
        */
        throw new UnsupportedOperationException("Method not decompiled: net.client.by.lock.task.a.call():org.w3c.dom.Document");
    }

    private boolean generalCheck(Document document) {
        if (document == null || document.getDocumentElement() == null || document.getDocumentElement().getNodeName() == null) {
            b.e("GenericTask", "null doc ");
            return false;
        } else if (!document.getDocumentElement().getNodeName().equalsIgnoreCase("response")) {
            l.a().a("Bad packet: no response! ERROR_" + getClass().getName());
            return false;
        } else {
            Element documentElement = document.getDocumentElement();
            String attribute = documentElement.getAttribute("id");
            if (!r.i().q() || (this instanceof LoginTask)) {
                if (attribute.length() > 0) {
                    r.i().c(attribute);
                } else {
                    NodeList elementsByTagName = documentElement.getElementsByTagName("error");
                    if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
                        l.a().a("Bad packet: no id! ERROR_" + getClass().getName());
                        return false;
                    }
                }
            } else if (!attribute.equalsIgnoreCase(r.i().j())) {
                b.e("GenericTask", "id " + attribute);
                b.e("GenericTask", "Session.getInstance().getSessionId() " + r.i().j());
                l.a().a("Bad packet: id unmatch! ERROR_" + getClass().getName());
                return false;
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public Object[] genericOnSuccess(Document document) {
        try {
            if (BackgroundService.a().c()) {
                BackgroundService.a().a(false);
            }
        } catch (Exception e) {
            b.w("GenericTask OnSuccess", "BG Service instance: " + e.getMessage());
        }
        try {
            if (generalCheck(document)) {
                NodeList elementsByTagName = document.getDocumentElement().getElementsByTagName("error");
                if (elementsByTagName == null || elementsByTagName.getLength() <= 0) {
                    this.state = 4;
                    errorCount = 0;
                    if (!isCancelled()) {
                        return onSuccess(document);
                    }
                    this.state = 3;
                    return null;
                }
                this.state = 2;
                return genericOnFailAtServer(elementsByTagName.item(0).getTextContent());
            }
            this.state = 2;
            return genericOnFailAtServer("package check failed");
        } catch (Exception e2) {
            b.e("GenericTask OnSuccess Exception!<" + getClass().getName() + ">", ": " + e2.getMessage());
            return genericOnException(e2);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized Object[] genericOnException(Throwable th) {
        Object[] objArr;
        this.state = 2;
        if ((th instanceof TransformerException) || (th instanceof SAXParseException)) {
            l.a().e();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            b.e("GenericTask", "errorCount : " + errorCount);
            if ((this instanceof GetRosterEventTask) || (this instanceof LoginTask)) {
                objArr = onException(th);
            }
        }
        errorCount++;
        if (errorCount >= 3) {
            errorCount = 0;
            l.a().a("Cannot connect server, check your internet connection.");
        } else if (r.i().q()) {
            restart();
        }
        objArr = onException(th);
        return objArr;
    }

    /* access modifiers changed from: protected */
    public void genericOnCancel() {
        n.a().a(this.taskId);
        unlock();
        onCancel();
    }

    private Object[] genericOnFailAtServer(String str) {
        if (str.contains("SessionExpiredException") || str.contains("SessionClosedException")) {
            r.a(false);
            if (!getClass().equals(LogoutTask.class)) {
                l.a().a(String.valueOf(str) + " ERROR_" + getClass().getName());
            }
        }
        return onFailAtServer(str);
    }

    /* access modifiers changed from: protected */
    public Object[] onSuccess(Document document) {
        return new Object[]{"onSuccess", document};
    }

    /* access modifiers changed from: protected */
    public Object[] onException(Throwable th) {
        return new Object[]{"onException", th};
    }

    /* access modifiers changed from: protected */
    public void onCancel() {
    }

    /* access modifiers changed from: protected */
    public Object[] onFailAtServer(String str) {
        l.a().a(String.valueOf(str) + " ERROR_" + getClass().getName());
        return new Object[]{"onFail", str};
    }

    public int getState() {
        return this.state;
    }
}
