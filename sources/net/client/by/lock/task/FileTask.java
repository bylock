package net.client.by.lock.task;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.support.v4.app.ap;
import java.io.IOException;
import net.client.by.lock.b.d;
import net.client.by.lock.b.e;
import net.client.by.lock.b.i;
import net.client.by.lock.b.j;
import net.client.by.lock.gui.a.a;
import net.client.by.lock.service.BackgroundService;

/* compiled from: MyApp */
public abstract class FileTask extends a implements j {
    private static final String TAG = "FileTask";
    private a ftBubble;
    e ftis;
    private ap mBuilder;
    private NotificationManager mNM;
    private int mNotificationId;

    public FileTask(Context context, Object obj) {
        super(context, obj);
        this.ftBubble = ((d) obj).A();
    }

    public i getProgressController() {
        if (this.ftis == null) {
            return null;
        }
        return this.ftis.a();
    }

    @Override // net.client.by.lock.task.a
    public boolean cancelTask(boolean z) {
        boolean cancelTask = super.cancelTask(z);
        if (z && this.ftis != null) {
            try {
                this.ftis.close();
            } catch (IOException e) {
                net.client.by.lock.f.j.a(TAG, e);
            }
        }
        return cancelTask;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onCancel() {
        this.mNM.cancel(this.mNotificationId);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a, net.client.by.lock.task.a, android.os.AsyncTask
    public Object[] doInBackground(Object... objArr) {
        this.mNotificationId = (int) System.currentTimeMillis();
        this.mNM = (NotificationManager) BackgroundService.a().getSystemService("notification");
        lock();
        if (!(this instanceof FileTask)) {
            Process.setThreadPriority(-8);
        }
        try {
            return genericOnSuccess(call());
        } catch (Exception e) {
            if (isCancelled()) {
                return null;
            }
            return genericOnException(e);
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... numArr) {
        this.ftBubble.b(numArr[0].intValue());
    }

    @Override // net.client.by.lock.b.j
    public void updateProgress(int i) {
        publishProgress(new Integer[]{Integer.valueOf(i)});
    }

    public void initProgress() {
    }

    public void finalizeProgress() {
        this.ftis.a().b();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public Object[] genericOnException(Throwable th) {
        this.state = 2;
        ((d) this.startupParameter).a(th);
        this.mBuilder = new ap(this.ctx).a(false).c(true).a(17301504).a(((d) this.startupParameter).e()).a(PendingIntent.getBroadcast(BackgroundService.a(), 0, new Intent(), 134217728)).b("Some kind of error occured").c("Error");
        this.mNM.notify(this.mNotificationId, this.mBuilder.a());
        this.mNM.cancel(this.mNotificationId);
        return onException(th);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.task.a
    public void onPostExecute(Object[] objArr) {
        int i;
        super.onPostExecute(objArr);
        ap c = new ap(this.ctx).c(true).a(((d) this.startupParameter).e()).a(false).c(String.valueOf(((d) this.startupParameter).e()) + " is " + (this instanceof SendFileTask ? "uploaded" : "downloaded"));
        if (this instanceof SendFileTask) {
            i = 17301641;
        } else {
            i = 17301634;
        }
        this.mBuilder = c.a(i).b(this instanceof SendFileTask ? "Uploaded" : "Downloaded").a(PendingIntent.getBroadcast(BackgroundService.a(), 0, new Intent(), 134217728));
        this.mNM.notify(this.mNotificationId, this.mBuilder.a());
        this.mNM.cancel(this.mNotificationId);
        if (this.ftBubble != null) {
            this.ftBubble.a(8);
        }
    }
}
