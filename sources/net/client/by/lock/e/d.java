package net.client.by.lock.e;

import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.b.g;
import net.client.by.lock.c.i;
import net.client.by.lock.c.n;
import net.client.by.lock.d.a;
import net.client.by.lock.d.b;
import net.client.by.lock.d.c;
import net.client.by.lock.d.l;
import net.client.by.lock.d.o;
import net.client.by.lock.d.q;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/* compiled from: MyApp */
public class d {
    private static byte[] a(String str) {
        return b.a(str);
    }

    public static synchronized void a(Document document, Boolean bool) {
        synchronized (d.class) {
            if (bool.booleanValue()) {
                i.c().h();
            }
            NodeList elementsByTagName = document.getElementsByTagName("friend");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                Element element = (Element) elementsByTagName.item(i);
                Element element2 = (Element) element.getElementsByTagName("id").item(0);
                Element element3 = (Element) element.getElementsByTagName("username").item(0);
                Element element4 = (Element) element.getElementsByTagName("nickname").item(0);
                Element element5 = (Element) element.getElementsByTagName("name").item(0);
                Element element6 = (Element) element.getElementsByTagName("publicMessage").item(0);
                Element element7 = (Element) element.getElementsByTagName("lastOnlineTime").item(0);
                Element element8 = (Element) element.getElementsByTagName("status").item(0);
                Element element9 = (Element) element.getElementsByTagName("modulus").item(0);
                c cVar = new c();
                if (element3 != null) {
                    cVar.c(element3.getTextContent());
                }
                if (element4 != null) {
                    cVar.d(element4.getTextContent());
                }
                if (element5 != null) {
                    cVar.f(element5.getTextContent());
                }
                if (element6 != null) {
                    cVar.b(element6.getTextContent());
                }
                if (element7 != null && element7.getTextContent().length() > 0) {
                    cVar.e(element7.getTextContent());
                }
                if (element8 != null) {
                    try {
                        cVar.a(Integer.parseInt(element8.getTextContent()));
                    } catch (Exception e) {
                    }
                }
                if (element9 != null) {
                    cVar.a(a(element9.getTextContent()));
                }
                if (element2 != null) {
                    try {
                        cVar.b(Integer.parseInt(element2.getTextContent()));
                    } catch (Exception e2) {
                    }
                }
                i.c().a(cVar);
            }
        }
    }

    public static void a(Document document, l lVar) {
        Element documentElement = document.getDocumentElement();
        Element element = (Element) documentElement.getElementsByTagName("id").item(0);
        if (element == null) {
            lVar.f("No mail id in response!");
        } else if (!element.getTextContent().equals(lVar.i())) {
            lVar.f("id values do not match!");
        } else {
            Element element2 = (Element) documentElement.getElementsByTagName("subject").item(0);
            Element element3 = (Element) documentElement.getElementsByTagName("body").item(0);
            Element element4 = (Element) documentElement.getElementsByTagName("other-recipients").item(0);
            Element element5 = (Element) documentElement.getElementsByTagName("mail-signature").item(0);
            if (element2 == null || element3 == null || element5 == null) {
                lVar.f("some fields are missing!");
                return;
            }
            lVar.a(a(element2.getTextContent()), a(element3.getTextContent()), a(element5.getTextContent()));
            if (element4 == null || element4.getTextContent() == null || element4.getTextContent().length() <= 0) {
                lVar.b(new byte[0]);
            } else {
                lVar.b(a(element4.getTextContent()));
            }
            NodeList elementsByTagName = documentElement.getElementsByTagName("attachment");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                Element element6 = (Element) elementsByTagName.item(i);
                Element element7 = (Element) element6.getElementsByTagName("fileId").item(0);
                Element element8 = (Element) element6.getElementsByTagName("filename").item(0);
                Element element9 = (Element) element6.getElementsByTagName("filesize").item(0);
                Element element10 = (Element) element6.getElementsByTagName("key").item(0);
                Element element11 = (Element) element6.getElementsByTagName("iv").item(0);
                Element element12 = (Element) element6.getElementsByTagName("signature").item(0);
                if (!(element7 == null || element8 == null || element10 == null || element11 == null || element12 == null)) {
                    lVar.a(new a(element7.getTextContent(), lVar.B(), a(element8.getTextContent()), a(element10.getTextContent()), a(element11.getTextContent()), a(element12.getTextContent()), Integer.parseInt(element9.getTextContent())));
                }
            }
            if (lVar.E().u() == null) {
                return;
            }
            if (!lVar.E().u().a(lVar)) {
                lVar.f("Cannot verify mail!");
            } else if (lVar.d()) {
                lVar.e("COMPLETED");
            } else {
                lVar.f("Cannot decrypt mail!");
            }
        }
    }

    public static ArrayList a(Document document) {
        c cVar;
        NodeList elementsByTagName = document.getElementsByTagName("fileTransfer");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < elementsByTagName.getLength(); i++) {
            Element element = (Element) elementsByTagName.item(i);
            Element element2 = (Element) element.getElementsByTagName("fileTransferId").item(0);
            if (element2 != null) {
                String textContent = element2.getTextContent();
                if (textContent.length() != 0) {
                    Element element3 = (Element) element.getElementsByTagName("fileId").item(0);
                    Element element4 = (Element) element.getElementsByTagName("senderId").item(0);
                    Element element5 = (Element) element.getElementsByTagName("filename").item(0);
                    Element element6 = (Element) element.getElementsByTagName("fileSize").item(0);
                    Element element7 = (Element) element.getElementsByTagName("key").item(0);
                    Element element8 = (Element) element.getElementsByTagName("iv").item(0);
                    Element element9 = (Element) element.getElementsByTagName("signature").item(0);
                    Element element10 = (Element) element.getElementsByTagName("sentTime").item(0);
                    if (element3 == null || element4 == null || element5 == null || element6 == null || element7 == null || element8 == null || element9 == null || element10 == null) {
                        n.a().d(textContent);
                    } else {
                        try {
                            cVar = i.c().a(Integer.valueOf(Integer.parseInt(element4.getTextContent())));
                        } catch (Exception e) {
                            cVar = null;
                        }
                        if (cVar == null) {
                            n.a().d(textContent);
                        } else if (cVar.u() == null) {
                            n.a().d(textContent);
                        } else {
                            g gVar = new g();
                            gVar.c(textContent);
                            gVar.a(element3.getTextContent());
                            gVar.b(cVar);
                            gVar.a(a(element5.getTextContent()));
                            try {
                                gVar.a(Integer.parseInt(element6.getTextContent()));
                                gVar.b(a(element7.getTextContent()));
                                gVar.c(a(element8.getTextContent()));
                                gVar.d(a(element9.getTextContent()));
                                gVar.d(element10.getTextContent());
                                arrayList.add(gVar);
                            } catch (NumberFormatException e2) {
                            }
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public static ArrayList a(Document document, c cVar) {
        NodeList elementsByTagName;
        NodeList elementsByTagName2;
        ArrayList arrayList = new ArrayList();
        NodeList elementsByTagName3 = document.getDocumentElement().getElementsByTagName("chat");
        for (int i = 0; i < elementsByTagName3.getLength(); i++) {
            Element element = (Element) elementsByTagName3.item(i);
            NodeList elementsByTagName4 = element.getElementsByTagName("id");
            if (!(elementsByTagName4 == null || elementsByTagName4.getLength() == 0)) {
                String trim = elementsByTagName4.item(0).getTextContent().trim();
                if (trim.length() != 0) {
                    Integer.valueOf(-1);
                    try {
                        Integer valueOf = Integer.valueOf(Integer.parseInt(trim));
                        NodeList elementsByTagName5 = element.getElementsByTagName("ciphertext");
                        if (!(elementsByTagName5 == null || elementsByTagName5.getLength() == 0)) {
                            String trim2 = elementsByTagName5.item(0).getTextContent().trim();
                            if (!(trim2.length() == 0 || (elementsByTagName = element.getElementsByTagName("signature")) == null || elementsByTagName.getLength() == 0)) {
                                String trim3 = elementsByTagName.item(0).getTextContent().trim();
                                if (!(trim3.length() == 0 || (elementsByTagName2 = element.getElementsByTagName("sentTime")) == null || elementsByTagName2.getLength() == 0)) {
                                    String trim4 = elementsByTagName2.item(0).getTextContent().trim();
                                    if (trim4.length() != 0) {
                                        arrayList.add(new b(cVar, valueOf, a(trim2), a(trim3), trim4));
                                    }
                                }
                            }
                        }
                    } catch (NumberFormatException e) {
                    }
                }
            }
        }
        return arrayList;
    }

    public static ArrayList b(Document document) {
        ArrayList arrayList = new ArrayList();
        if (document == null) {
            System.out.println("A possible bug around...");
        } else {
            NodeList elementsByTagName = document.getDocumentElement().getElementsByTagName("event");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                try {
                    Element element = (Element) elementsByTagName.item(i);
                    o oVar = new o();
                    oVar.a(element.getElementsByTagName("type").item(0).getTextContent());
                    oVar.a(Integer.parseInt(element.getElementsByTagName("friendUserId").item(0).getTextContent()));
                    oVar.b(element.getElementsByTagName("parameter").item(0).getTextContent());
                    oVar.b(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()));
                    arrayList.add(oVar);
                } catch (Exception e) {
                }
            }
        }
        return arrayList;
    }

    public static String[] a(Document document, net.client.by.lock.a.c cVar) {
        String str;
        Element documentElement = document.getDocumentElement();
        Element element = (Element) documentElement.getElementsByTagName("callId").item(0);
        if (element == null) {
            return null;
        }
        Element element2 = (Element) documentElement.getElementsByTagName("nonce").item(0);
        if (element2 == null) {
            return null;
        }
        Element element3 = (Element) documentElement.getElementsByTagName("callTime").item(0);
        if (element3 != null) {
            str = element3.getTextContent();
        } else {
            str = "";
        }
        return new String[]{element.getTextContent(), element2.getTextContent(), str};
    }

    public static String[] b(Document document, net.client.by.lock.a.c cVar) {
        try {
            Element documentElement = document.getDocumentElement();
            String textContent = documentElement.getElementsByTagName("sessionKey").item(0).getTextContent();
            String textContent2 = documentElement.getElementsByTagName("sessionIv").item(0).getTextContent();
            String textContent3 = documentElement.getElementsByTagName("signature").item(0).getTextContent();
            String textContent4 = documentElement.getElementsByTagName("port").item(0).getTextContent();
            int parseInt = Integer.parseInt(textContent4);
            if (textContent.length() > 0 && textContent2.length() > 0 && textContent3.length() > 0 && parseInt > 0) {
                return new String[]{textContent, textContent2, textContent3, textContent4};
            }
        } catch (Exception e) {
            j.a("", e);
        }
        return null;
    }

    public static void c(Document document) {
        Integer num;
        Element documentElement = document.getDocumentElement();
        NodeList elementsByTagName = documentElement.getElementsByTagName("userId");
        if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
            net.client.by.lock.c.l.a().a("Packet error: No UserId!");
            return;
        }
        try {
            r.i().a(Integer.parseInt(elementsByTagName.item(0).getTextContent()));
            NodeList elementsByTagName2 = documentElement.getElementsByTagName("name");
            if (elementsByTagName2 == null || elementsByTagName2.getLength() == 0) {
                net.client.by.lock.c.l.a().a("Packet error: No Name");
                return;
            }
            String textContent = elementsByTagName2.item(0).getTextContent();
            NodeList elementsByTagName3 = documentElement.getElementsByTagName("publicMessage");
            if (elementsByTagName3 == null || elementsByTagName3.getLength() == 0) {
                net.client.by.lock.c.l.a().a("Packet error: No PublicMessage!");
            }
            r.i().i(elementsByTagName3.item(0).getTextContent());
            r.i().g(textContent);
            NodeList elementsByTagName4 = documentElement.getElementsByTagName("status");
            if (elementsByTagName4 != null && elementsByTagName4.getLength() > 0) {
                try {
                    r.i().a(Integer.valueOf(Integer.parseInt(elementsByTagName4.item(0).getTextContent())));
                } catch (NumberFormatException e) {
                    j.a("ResponseParser", e);
                }
            }
            NodeList elementsByTagName5 = documentElement.getElementsByTagName("admin");
            if (elementsByTagName5 == null || elementsByTagName5.getLength() == 0) {
                r.i().b(false);
            } else {
                r.i().b(Integer.parseInt(elementsByTagName5.item(0).getTextContent()) == 1);
            }
            if (documentElement.getElementsByTagName("privateExponent").getLength() > 0 && documentElement.getElementsByTagName("modulus").getLength() > 0) {
                r.i().a(new q(a(documentElement.getElementsByTagName("modulus").item(0).getTextContent()), a(documentElement.getElementsByTagName("privateExponent").item(0).getTextContent())));
            }
            a(document, (Boolean) true);
            NodeList elementsByTagName6 = document.getElementsByTagName("group");
            for (int i = 0; i < elementsByTagName6.getLength(); i++) {
                Element element = (Element) elementsByTagName6.item(i);
                Element element2 = (Element) element.getElementsByTagName("id").item(0);
                try {
                    Integer valueOf = Integer.valueOf(Integer.parseInt(element2.getTextContent()));
                    Element element3 = (Element) element.getElementsByTagName("name").item(0);
                    if (element2 != null && element2.getTextContent().length() > 0 && element3 != null && element3.getTextContent().length() > 0) {
                        i.c().a(new net.client.by.lock.d.g(valueOf.intValue(), element3.getTextContent()));
                    }
                    NodeList elementsByTagName7 = element.getElementsByTagName("userId");
                    for (int i2 = 0; i2 < elementsByTagName7.getLength(); i2++) {
                        try {
                            num = Integer.valueOf(Integer.parseInt(((Element) elementsByTagName7.item(i2)).getTextContent()));
                        } catch (NumberFormatException e2) {
                            j.a("", e2);
                            num = -1;
                        }
                        if (num.intValue() > 0) {
                            i.c().a(num, valueOf);
                        }
                    }
                } catch (NumberFormatException e3) {
                    j.a("", e3);
                }
            }
            if (r.i().p() != null) {
                Iterator it = a(document).iterator();
                while (it.hasNext()) {
                    g gVar = (g) it.next();
                    if (!gVar.E().a(gVar)) {
                        n.a().d(gVar.i());
                    }
                }
                NodeList elementsByTagName8 = document.getElementsByTagName("chat");
                for (int i3 = 0; i3 < elementsByTagName8.getLength(); i3++) {
                    Element element4 = (Element) elementsByTagName8.item(i3);
                    Element element5 = (Element) element4.getElementsByTagName("id").item(0);
                    Element element6 = (Element) element4.getElementsByTagName("senderUserId").item(0);
                    Element element7 = (Element) element4.getElementsByTagName("ciphertext").item(0);
                    Element element8 = (Element) element4.getElementsByTagName("signature").item(0);
                    Element element9 = (Element) element4.getElementsByTagName("sentTime").item(0);
                    if (!(element5 == null || element6 == null || element7 == null || element8 == null || element9 == null)) {
                        try {
                            c a = i.c().a(Integer.valueOf(Integer.parseInt(element6.getTextContent())));
                            if (a != null) {
                                a.a(new b(a, Integer.valueOf(Integer.parseInt(element5.getTextContent())), a(element7.getTextContent()), a(element8.getTextContent()), element9.getTextContent()));
                            }
                        } catch (NumberFormatException e4) {
                            j.a("", e4);
                        }
                    }
                }
            }
        } catch (NumberFormatException e5) {
            net.client.by.lock.c.l.a().a("Packet error: UserId is not numeric!");
        }
    }

    public static boolean d(Document document) {
        String str;
        String str2 = null;
        Element documentElement = document.getDocumentElement();
        NodeList elementsByTagName = documentElement.getElementsByTagName("userId");
        if (elementsByTagName == null || elementsByTagName.getLength() <= 0) {
            str = null;
        } else {
            str = elementsByTagName.item(0).getTextContent();
        }
        NodeList elementsByTagName2 = documentElement.getElementsByTagName("error");
        if (elementsByTagName2 != null && elementsByTagName2.getLength() > 0) {
            str2 = elementsByTagName2.item(0).getTextContent();
        }
        if (str != null || str2 == null) {
            try {
                r.i().a(Integer.parseInt(str));
                return true;
            } catch (NumberFormatException e) {
                net.client.by.lock.c.l.a().a("Packet error: UserID is not numeric!");
                return false;
            }
        } else {
            net.client.by.lock.c.l.a().a(str2);
            return false;
        }
    }

    public static String[] e(Document document) {
        String str;
        String str2 = "";
        Element element = (Element) document.getElementsByTagName("time").item(0);
        if (element != null) {
            str2 = element.getTextContent();
        }
        Element element2 = (Element) document.getElementsByTagName("fileId").item(0);
        if (element2 != null) {
            str = element2.getTextContent();
        } else {
            str = "";
        }
        return new String[]{str, str2};
    }

    public static String f(Document document) {
        Element element = (Element) document.getElementsByTagName("time").item(0);
        if (element != null) {
            return element.getTextContent();
        }
        return "";
    }

    public static void a(Document document, Object obj) {
        Element element = (Element) document.getElementsByTagName("groupId").item(0);
        if (element != null) {
            int parseInt = Integer.parseInt(element.getTextContent());
            i.c().a(new net.client.by.lock.d.g(parseInt, (String) ((ArrayList) obj).get(0)));
            i.c().a(Integer.valueOf(((c) ((ArrayList) obj).get(1)).K()), Integer.valueOf(parseInt));
        }
    }
}
