package net.client.by.lock.e;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.client.by.lock.d.a;
import net.client.by.lock.d.b;
import net.client.by.lock.d.g;
import net.client.by.lock.d.n;
import net.client.by.lock.d.q;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;
import net.client.by.lock.f.m;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: MyApp */
public class c {
    private static String e(byte[] bArr) {
        return b.a(bArr);
    }

    public static Document a() {
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("request");
            newDocument.appendChild(createElement);
            Element createElement2 = newDocument.createElement("username");
            createElement2.appendChild(newDocument.createTextNode(r.i().k()));
            createElement.appendChild(createElement2);
            Element createElement3 = newDocument.createElement("password");
            createElement3.setTextContent(m.a(r.i().l()));
            Element createElement4 = newDocument.createElement("edition");
            createElement4.setTextContent("android");
            createElement.appendChild(createElement3);
            createElement.appendChild(createElement4);
            Element createElement5 = newDocument.createElement("version");
            createElement5.setTextContent("0.8-24");
            createElement.appendChild(createElement5);
            Element createElement6 = newDocument.createElement("tzid");
            createElement6.setTextContent(TimeZone.getDefault().getID());
            createElement.appendChild(createElement6);
            Element createElement7 = newDocument.createElement("audioV2");
            createElement7.setTextContent("1");
            createElement.appendChild(createElement7);
            return newDocument;
        } catch (ParserConfigurationException e) {
            j.a("RequestBuilder: Error in creating xml for request", e);
            return null;
        }
    }

    public static Document b() {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        String[] d2 = r.i().d();
        for (String str : d2) {
            Element createElement = d.createElement("mailId");
            createElement.setTextContent(str);
            documentElement.appendChild(createElement);
        }
        String[] e = r.i().e();
        for (String str2 : e) {
            Element createElement2 = d.createElement("fileTransferId");
            createElement2.setTextContent(str2);
            documentElement.appendChild(createElement2);
        }
        for (Map.Entry entry : r.i().c()) {
            Element createElement3 = d.createElement("lastChatId");
            Element createElement4 = d.createElement("friendUserId");
            createElement4.setTextContent(new StringBuilder(String.valueOf(((net.client.by.lock.d.c) entry.getKey()).K())).toString());
            createElement3.appendChild(createElement4);
            Element createElement5 = d.createElement("chatId");
            createElement5.setTextContent(new StringBuilder().append(entry.getValue()).toString());
            createElement3.appendChild(createElement5);
            documentElement.appendChild(createElement3);
        }
        return d;
    }

    public static Document c() {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("message");
        createElement.setTextContent((String) r.i().o().a());
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document a(b bVar) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        net.client.by.lock.d.c E = bVar.E();
        Element createElement = d.createElement("toUserId");
        createElement.setTextContent(new StringBuilder(String.valueOf(E.K())).toString());
        documentElement.appendChild(createElement);
        Element createElement2 = d.createElement("ciphertext");
        createElement2.setTextContent(e(bVar.e()));
        documentElement.appendChild(createElement2);
        Element createElement3 = d.createElement("signature");
        createElement3.setTextContent(e(bVar.f()));
        documentElement.appendChild(createElement3);
        return d;
    }

    public static Document a(Integer num, String[] strArr, String[] strArr2, Set set) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("lastEventId");
        createElement.setTextContent(new StringBuilder().append(num).toString());
        documentElement.appendChild(createElement);
        for (String str : strArr) {
            Element createElement2 = d.createElement("mailId");
            createElement2.setTextContent(str);
            documentElement.appendChild(createElement2);
        }
        for (String str2 : strArr2) {
            Element createElement3 = d.createElement("fileTransferId");
            createElement3.setTextContent(str2);
            documentElement.appendChild(createElement3);
        }
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Element createElement4 = d.createElement("lastChatId");
            Element createElement5 = d.createElement("friendUserId");
            createElement5.setTextContent(new StringBuilder(String.valueOf(((net.client.by.lock.d.c) entry.getKey()).K())).toString());
            createElement4.appendChild(createElement5);
            Element createElement6 = d.createElement("chatId");
            createElement6.setTextContent(new StringBuilder().append(entry.getValue()).toString());
            createElement4.appendChild(createElement6);
            documentElement.appendChild(createElement4);
        }
        return d;
    }

    public static Document a(net.client.by.lock.d.c cVar) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("lastChatId");
        createElement.setTextContent(new StringBuilder().append(cVar.l()).toString());
        Element createElement2 = d.createElement("fromUserId");
        createElement2.setTextContent(new StringBuilder(String.valueOf(cVar.K())).toString());
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document d() {
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("request");
            if (r.i().j() != null) {
                createElement.setAttribute("id", r.i().j());
            }
            newDocument.appendChild(createElement);
            Element createElement2 = newDocument.createElement("userId");
            createElement2.appendChild(newDocument.createTextNode(new StringBuilder(String.valueOf(r.i().m())).toString()));
            createElement.appendChild(createElement2);
            return newDocument;
        } catch (ParserConfigurationException e) {
            j.a("Error in creating xml for request", e);
            return null;
        }
    }

    public static Document a(String[] strArr) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("username");
        createElement.setTextContent(strArr[0]);
        Element createElement2 = d.createElement("nickname");
        createElement2.setTextContent(strArr[1]);
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document a(Integer num) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("friendUserId");
        createElement.setTextContent(new StringBuilder().append(num).toString());
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document b(net.client.by.lock.d.c cVar) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("friendUserId");
        createElement.setTextContent(new StringBuilder(String.valueOf(cVar.K())).toString());
        Element createElement2 = d.createElement("friendNickname");
        createElement2.setTextContent(cVar.D());
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document c(net.client.by.lock.d.c cVar) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("friendUserId");
        createElement.setTextContent(new StringBuilder(String.valueOf(cVar.K())).toString());
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document a(String str) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("fileId");
        createElement.setTextContent(str);
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document b(String str) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("fileTransferId");
        createElement.setTextContent(str);
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document c(String str) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("fileTransferId");
        createElement.setTextContent(str);
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document e() {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("password");
        createElement.setTextContent(m.a(r.i().l()));
        Element createElement2 = d.createElement("privateExponent");
        createElement2.setTextContent(e(r.i().p().d()));
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document a(net.client.by.lock.a.c cVar) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("calleeUserId");
        createElement.setTextContent(new StringBuilder(String.valueOf(cVar.E().K())).toString());
        Element createElement2 = d.createElement("sessionKey");
        createElement2.setTextContent(e(cVar.f()));
        Element createElement3 = d.createElement("sessionIv");
        createElement3.setTextContent(e(cVar.g()));
        Element createElement4 = d.createElement("signature");
        createElement4.setTextContent(e(cVar.h()));
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        documentElement.appendChild(createElement3);
        documentElement.appendChild(createElement4);
        return d;
    }

    public static Document a(byte[] bArr) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("callId");
        createElement.setTextContent(j.a(bArr));
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document b(byte[] bArr) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("callId");
        createElement.setTextContent(j.a(bArr));
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document c(byte[] bArr) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("callId");
        createElement.setTextContent(j.a(bArr));
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document d(byte[] bArr) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("callId");
        createElement.setTextContent(j.a(bArr));
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document a(net.client.by.lock.d.c cVar, String str) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("friendUserId");
        createElement.setTextContent(new StringBuilder(String.valueOf(cVar.K())).toString());
        Element createElement2 = d.createElement("groupName");
        createElement2.setTextContent(str);
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document b(Integer num) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("groupId");
        createElement.setTextContent(new StringBuilder().append(num).toString());
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document a(g gVar) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("groupId");
        createElement.setTextContent(new StringBuilder(String.valueOf(gVar.a())).toString());
        Element createElement2 = d.createElement("groupName");
        createElement2.setTextContent((String) gVar.b().a());
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document f() {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("password");
        createElement.setTextContent(m.a(r.i().l()));
        Element createElement2 = d.createElement("privateExponent");
        createElement2.setTextContent(e(r.i().p().d()));
        Element createElement3 = d.createElement("modulus");
        createElement3.setTextContent(e(r.i().p().a()));
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        documentElement.appendChild(createElement3);
        return d;
    }

    public static Document a(n nVar) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Iterator it = nVar.c().iterator();
        while (it.hasNext()) {
            n nVar2 = (n) it.next();
            Element createElement = d.createElement("mail");
            Element createElement2 = d.createElement("toUserId");
            createElement2.setTextContent(new StringBuilder(String.valueOf(nVar2.E().K())).toString());
            createElement.appendChild(createElement2);
            Element createElement3 = d.createElement("subject");
            createElement3.setTextContent(e(nVar2.j()));
            createElement.appendChild(createElement3);
            Element createElement4 = d.createElement("body");
            createElement4.setTextContent(e(nVar2.k()));
            createElement.appendChild(createElement4);
            Element createElement5 = d.createElement("other-recipients");
            createElement5.setTextContent(e(nVar2.m()));
            createElement.appendChild(createElement5);
            Element createElement6 = d.createElement("mail-signature");
            createElement6.setTextContent(e(nVar2.h()));
            createElement.appendChild(createElement6);
            Iterator it2 = nVar2.l().iterator();
            while (it2.hasNext()) {
                a aVar = (a) it2.next();
                Element createElement7 = d.createElement("attachment");
                Element createElement8 = d.createElement("fileId");
                createElement8.setTextContent(aVar.b());
                createElement7.appendChild(createElement8);
                Element createElement9 = d.createElement("filename");
                createElement9.setTextContent(e(aVar.g()));
                createElement7.appendChild(createElement9);
                Element createElement10 = d.createElement("key");
                createElement10.setTextContent(e(aVar.h()));
                createElement7.appendChild(createElement10);
                Element createElement11 = d.createElement("iv");
                createElement11.setTextContent(e(aVar.i()));
                createElement7.appendChild(createElement11);
                Element createElement12 = d.createElement("signature");
                createElement12.setTextContent(e(aVar.j()));
                createElement7.appendChild(createElement12);
                createElement.appendChild(createElement7);
            }
            documentElement.appendChild(createElement);
        }
        return d;
    }

    public static Document d(String str) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("mailId");
        createElement.setTextContent(str);
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document e(String str) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("mailId");
        createElement.setTextContent(str);
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document c(Integer num) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("status");
        createElement.setTextContent(num.toString());
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document f(String str) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("name");
        createElement.setTextContent(str);
        documentElement.appendChild(createElement);
        return d;
    }

    public static Document a(Integer num, Integer num2) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("senderId");
        createElement.setTextContent(new StringBuilder().append(num).toString());
        Element createElement2 = d.createElement("chatId");
        createElement2.setTextContent(new StringBuilder().append(num2).toString());
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document b(Integer num, Integer num2) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("friendUserId");
        createElement.setTextContent(new StringBuilder().append(num).toString());
        Element createElement2 = d.createElement("groupId");
        createElement2.setTextContent(new StringBuilder().append(num2).toString());
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document c(Integer num, Integer num2) {
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("friendUserId");
        createElement.setTextContent(new StringBuilder().append(num).toString());
        Element createElement2 = d.createElement("groupId");
        createElement2.setTextContent(new StringBuilder().append(num2).toString());
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        return d;
    }

    public static Document a(String str, String str2) {
        r.i().f(str2);
        r.i().a(new q());
        Document d = d();
        Element documentElement = d.getDocumentElement();
        Element createElement = d.createElement("username");
        createElement.setTextContent(str);
        Element createElement2 = d.createElement("password");
        createElement2.setTextContent(m.a(str2));
        Element createElement3 = d.createElement("privateExponent");
        createElement3.setTextContent(e(r.i().p().d()));
        Element createElement4 = d.createElement("modulus");
        createElement4.setTextContent(e(r.i().p().a()));
        Element createElement5 = d.createElement("edition");
        createElement5.setTextContent("android");
        Element createElement6 = d.createElement("tzid");
        createElement6.setTextContent(TimeZone.getDefault().getID());
        Element createElement7 = d.createElement("audioV2");
        createElement7.setTextContent("1");
        documentElement.appendChild(createElement);
        documentElement.appendChild(createElement2);
        documentElement.appendChild(createElement3);
        documentElement.appendChild(createElement4);
        documentElement.appendChild(createElement5);
        documentElement.appendChild(createElement6);
        documentElement.appendChild(createElement7);
        return d;
    }
}
