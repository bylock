package net.client.by.lock.e;

/* compiled from: MyApp */
public class a {
    public static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i += 3) {
            stringBuffer.append(a(bArr, i));
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:21:0x002a */
    /* JADX DEBUG: Multi-variable search result rejected for r10v0, resolved type: byte[] */
    /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: byte */
    /* JADX DEBUG: Multi-variable search result rejected for r3v6, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v3 */
    protected static char[] a(byte[] bArr, int i) {
        int length = (bArr.length - i) - 1;
        int i2 = length >= 2 ? 2 : length;
        int i3 = 0;
        for (int i4 = 0; i4 <= i2; i4++) {
            byte b = bArr[i + i4];
            if (b < 0) {
                b += 256;
            }
            i3 += (b == true ? 1 : 0) << ((2 - i4) * 8);
        }
        char[] cArr = new char[4];
        for (int i5 = 0; i5 < 4; i5++) {
            cArr[i5] = a((i3 >>> ((3 - i5) * 6)) & 63);
        }
        if (length < 1) {
            cArr[2] = '=';
        }
        if (length < 2) {
            cArr[3] = '=';
        }
        return cArr;
    }

    protected static char a(int i) {
        if (i >= 0 && i <= 25) {
            return (char) (i + 65);
        }
        if (i >= 26 && i <= 51) {
            return (char) ((i - 26) + 97);
        }
        if (i >= 52 && i <= 61) {
            return (char) ((i - 52) + 48);
        }
        if (i == 62) {
            return '+';
        }
        if (i == 63) {
            return '/';
        }
        return '?';
    }

    public static byte[] a(String str) {
        int i = 0;
        for (int length = str.length() - 1; str.charAt(length) == '='; length--) {
            i++;
        }
        byte[] bArr = new byte[(((str.length() * 6) / 8) - i)];
        int i2 = 0;
        for (int i3 = 0; i3 < str.length(); i3 += 4) {
            int a = a(str.charAt(i3 + 3)) + (a(str.charAt(i3)) << 18) + (a(str.charAt(i3 + 1)) << 12) + (a(str.charAt(i3 + 2)) << 6);
            int i4 = 0;
            while (i4 < 3 && i2 + i4 < bArr.length) {
                bArr[i2 + i4] = (byte) ((a >> ((2 - i4) * 8)) & 255);
                i4++;
            }
            i2 += 3;
        }
        return bArr;
    }

    protected static int a(char c) {
        if (c >= 'A' && c <= 'Z') {
            return c - 'A';
        }
        if (c >= 'a' && c <= 'z') {
            return (c - 'a') + 26;
        }
        if (c >= '0' && c <= '9') {
            return (c - '0') + 52;
        }
        if (c == '+') {
            return 62;
        }
        if (c == '/') {
            return 63;
        }
        if (c == '=') {
            return 0;
        }
        return -1;
    }
}
