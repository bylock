package net.client.by.lock.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import net.client.by.lock.c.g;
import net.client.by.lock.c.l;
import net.client.by.lock.gui.activity.LoginActivity;
import net.client.by.lock.gui.activity.MainActivity;

/* compiled from: MyApp */
public class NetworkStatusReciever extends BroadcastReceiver {
    static Boolean a = true;

    public static Boolean a() {
        boolean z;
        NetworkInfo[] allNetworkInfo = ((ConnectivityManager) g.a().b().getSystemService("connectivity")).getAllNetworkInfo();
        int length = allNetworkInfo.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            }
            NetworkInfo networkInfo = allNetworkInfo[i];
            if (networkInfo != null && networkInfo.isConnected()) {
                z = true;
                break;
            }
            i++;
        }
        return Boolean.valueOf(z);
    }

    public void onReceive(Context context, Intent intent) {
        boolean booleanValue = a().booleanValue();
        if (a.booleanValue() != booleanValue && !(context instanceof LoginActivity)) {
            if (booleanValue) {
                l.a().e();
            } else {
                Intent intent2 = new Intent(MainActivity.t);
                intent2.putExtra("caption", "No Network!");
                g.a().b().sendBroadcast(intent2);
            }
        }
        a = Boolean.valueOf(booleanValue);
    }
}
