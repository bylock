package net.client.by.lock.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import net.client.by.lock.b;
import net.client.by.lock.c.l;
import net.client.by.lock.c.n;

/* compiled from: MyApp */
public class AlarmReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            String string = intent.getExtras().getString("alarm_message");
            b.v("AlarmReceiver", "alarm:" + string);
            boolean booleanValue = NetworkStatusReciever.a().booleanValue();
            if (string.equals("roster_event")) {
                if (booleanValue) {
                    n.a().f();
                } else {
                    l.a().d();
                }
            } else if (!string.equals("try_login")) {
            } else {
                if (booleanValue) {
                    n.a().b();
                } else {
                    l.a().e();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
