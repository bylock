package net.client.by.lock.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import net.client.by.lock.d.r;

/* compiled from: MyApp */
public class ScreenReciever extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (r.i().q()) {
            if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                r.i().c(true);
            } else if (intent.getAction().equals("android.intent.action.SCREEN_ON")) {
                r.i().c(false);
            }
        }
    }
}
