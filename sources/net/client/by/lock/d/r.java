package net.client.by.lock.d;

import android.content.Intent;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import net.client.by.lock.c.g;
import net.client.by.lock.c.n;
import net.client.by.lock.f.h;
import net.client.by.lock.gui.activity.MainActivity;
import net.client.by.lock.service.c;

/* compiled from: MyApp */
public class r {
    private static r l = null;
    private static SecureRandom n = null;
    String a = null;
    String b;
    int c;
    String d = null;
    boolean e;
    boolean f = false;
    q g;
    private h h = new h("", "name");
    private h i = new h("publicMessage");
    private h j = new h("description");
    private h k = new h(false, "isConnected");
    private Integer m = -1;
    private ArrayList o = new ArrayList();
    private ArrayList p = new ArrayList();
    private HashMap q = new HashMap();
    private h r = new h(3, "status");
    private boolean s = false;
    private String t = null;

    private r() {
    }

    public void a(byte[] bArr) {
        n = new SecureRandom(bArr);
    }

    public SecureRandom a() {
        return n;
    }

    public static void a(boolean z) {
        if (z) {
            l.k.a((Object) false);
            l.t = null;
            l.i.deleteObservers();
            l.j.deleteObservers();
            l.k.deleteObservers();
            l.r.deleteObservers();
            l.h.deleteObservers();
            l.b = "";
            l.d = "";
            l.f = false;
            c.a = null;
            l.s = false;
            n = null;
        }
        l.m = -1;
        l.a = "";
        Intent intent = new Intent(MainActivity.t);
        intent.putExtra("caption", "Disconnected");
        g.a().b().sendBroadcast(intent);
        l.e = false;
        l.i.a("");
        l.g = null;
        l.j.a("");
        l.o.clear();
        l.p.clear();
        l.q.clear();
    }

    public synchronized void a(Integer num) {
        if (this.r.a() != num) {
            this.r.a(num);
            this.s = false;
        }
    }

    public synchronized void b(Integer num) {
        if (this.r.a() != num) {
            this.r.a(num);
            n.a().b(num);
            this.s = false;
        }
    }

    public h b() {
        return this.r;
    }

    public synchronized void a(c cVar, Integer num) {
        Integer num2 = (Integer) this.q.get(cVar);
        if (num2 == null) {
            this.q.put(cVar, num);
        } else if (num.intValue() > num2.intValue()) {
            this.q.put(cVar, num);
        }
    }

    public synchronized Set c() {
        HashMap hashMap;
        hashMap = new HashMap();
        hashMap.putAll(this.q);
        return hashMap.entrySet();
    }

    public synchronized void a(String str) {
        this.o.add(str);
    }

    public synchronized void b(String str) {
        this.p.add(str);
    }

    public synchronized String[] d() {
        return (String[]) this.o.toArray(new String[0]);
    }

    public synchronized String[] e() {
        return (String[]) this.p.toArray(new String[0]);
    }

    public synchronized void a(String[] strArr, String[] strArr2, Set set) {
        synchronized (this) {
            for (String str : strArr) {
                this.o.remove(str);
            }
            for (String str2 : strArr2) {
                this.p.remove(str2);
            }
            Iterator it = set.iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                Integer num = (Integer) this.q.get(entry.getKey());
                if (num != null && num.equals(entry.getValue())) {
                    this.q.remove(entry.getKey());
                }
            }
        }
    }

    private void v() {
        if (this.h.a() == null || ((String) this.h.a()).length() <= 0) {
            this.j.a(this.b);
        } else {
            this.j.a((String) this.h.a());
        }
    }

    public h f() {
        return this.j;
    }

    public void g() {
        this.f = true;
    }

    public boolean h() {
        return this.f;
    }

    public static r i() {
        if (l == null) {
            if (c.a == null) {
                l = new r();
                c.a = l;
            } else {
                l = c.a;
            }
        } else if (c.a == null) {
            c.a = l;
        }
        return l;
    }

    public String j() {
        return this.a;
    }

    public void c(String str) {
        this.a = str;
    }

    public String k() {
        return this.b;
    }

    public void d(String str) {
        this.b = str;
        v();
    }

    public String l() {
        return this.d;
    }

    public void a(int i2) {
        this.c = i2;
    }

    public int m() {
        return this.c;
    }

    public void e(String str) {
        String str2 = this.d;
        this.d = str;
        this.g.c();
        n.a().c(str2);
    }

    public void f(String str) {
        this.d = str;
    }

    public void b(boolean z) {
        this.e = z;
    }

    public h n() {
        return this.h;
    }

    public void g(String str) {
        this.h.a(str);
        v();
    }

    public void h(String str) {
        if (!str.equals(this.h.a())) {
            this.h.a(str);
            v();
            n.a().e(str);
        }
    }

    public h o() {
        return this.i;
    }

    public void i(String str) {
        this.i.a(str);
    }

    public q p() {
        return this.g;
    }

    public void a(q qVar) {
        this.g = qVar;
    }

    public boolean q() {
        return this.a != null && this.a.length() > 0;
    }

    public boolean r() {
        return this.b != null && this.b.length() > 0 && this.d != null && this.d.length() > 0;
    }

    public Integer s() {
        return this.m;
    }

    public void c(Integer num) {
        if (num.intValue() > this.m.intValue()) {
            this.m = num;
        }
    }

    public h t() {
        return this.k;
    }

    public String u() {
        return this.t;
    }

    public void c(boolean z) {
        if (z) {
            i().b((Integer) 2);
        } else if (this.s) {
            i().b((Integer) 3);
        }
        this.s = z;
    }
}
