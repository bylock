package net.client.by.lock.d;

import java.util.Observable;
import java.util.Observer;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class e implements Observer {
    final /* synthetic */ c a;

    private e(c cVar) {
        this.a = cVar;
    }

    /* synthetic */ e(c cVar, e eVar) {
        this(cVar);
    }

    public void update(Observable observable, Object obj) {
        if (((Integer) this.a.j.b()).intValue() == 0 && ((Integer) this.a.j.a()).intValue() != 0) {
            this.a.p.a((Object) true);
        } else if (((Integer) this.a.j.b()).intValue() != 0 && ((Integer) this.a.j.a()).intValue() == 0) {
            this.a.p.a((Object) false);
        }
    }
}
