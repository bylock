package net.client.by.lock.d;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.a.a;
import net.client.by.lock.gui.a.o;
import net.client.by.lock.gui.activity.aw;

/* compiled from: MyApp */
public class n extends m {
    ArrayList o;
    n p;
    private boolean q;
    private boolean r;

    public n() {
        this.o = null;
        this.p = null;
        this.o = new ArrayList();
        e("IDLE");
        this.q = false;
        this.r = false;
        this.a = UUID.randomUUID().toString();
    }

    public n(List list, String str, String str2, List list2) {
        this.o = null;
        this.p = null;
        this.o = new ArrayList();
        this.b = str;
        this.c = str2;
        this.a = UUID.randomUUID().toString();
        Iterator it = list2.iterator();
        while (it.hasNext()) {
            super.a((a) it.next());
        }
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            this.o.add(new n(this, (c) it2.next(), list));
        }
        e("IDLE");
        this.q = false;
        this.r = false;
    }

    private n(n nVar, c cVar, List list) {
        this.o = null;
        this.p = null;
        this.p = nVar;
        b(cVar);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            c cVar2 = (c) it.next();
            if (cVar2 != cVar) {
                this.i.add(Integer.valueOf(cVar2.K()));
            }
        }
        this.b = nVar.g();
        this.c = nVar.f();
        this.f.a(nVar.F());
        this.d.a(nVar.C());
        this.a = nVar.i();
        Iterator it2 = nVar.l().iterator();
        while (it2.hasNext()) {
            a(new a((a) it2.next()));
        }
    }

    public ArrayList b() {
        if (this.p != null) {
            return this.p.b();
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = this.o.iterator();
        while (it.hasNext()) {
            arrayList.add(((n) it.next()).E());
        }
        return arrayList;
    }

    public ArrayList c() {
        return this.o;
    }

    public void b(boolean z) {
        this.r = z;
    }

    public boolean d() {
        return this.r;
    }

    @Override // net.client.by.lock.d.k
    public boolean b_() {
        return true;
    }

    public boolean e() {
        boolean z;
        if (this.q) {
            return true;
        }
        this.r = false;
        if (this.o == null) {
            if (!(this.b == null || this.c == null || this.e == null || this.e.u() == null)) {
                try {
                    this.j = this.e.u().a(this.b.getBytes("UTF-8"));
                    this.k = this.e.u().a(this.c.getBytes("UTF-8"));
                    String str = "";
                    if (this.i.size() > 0) {
                        str = String.valueOf(str) + this.i.get(0);
                        int i = 1;
                        while (i < this.i.size()) {
                            String str2 = String.valueOf(str) + "," + this.i.get(i);
                            i++;
                            str = str2;
                        }
                    }
                    if (str.length() > 0) {
                        this.l = this.e.u().a(str.getBytes("UTF-8"));
                    } else {
                        this.l = new byte[0];
                    }
                    Iterator it = this.n.iterator();
                    while (it.hasNext()) {
                        if (!((a) it.next()).a(this.e.u())) {
                            this.q = false;
                            return false;
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    j.a("", e);
                    this.j = null;
                    this.k = null;
                }
            }
            if (this.j == null || this.k == null || this.l == null) {
                z = false;
            } else {
                z = true;
            }
            this.q = z;
            return this.q;
        }
        Iterator it2 = this.o.iterator();
        while (it2.hasNext()) {
            if (!((n) it2.next()).e()) {
                this.q = false;
                return false;
            }
        }
        this.q = true;
        return true;
    }

    public void a(aw awVar) {
        Iterator it = this.o.iterator();
        while (it.hasNext()) {
            n nVar = (n) it.next();
            nVar.E().b(nVar);
        }
        net.client.by.lock.c.n.a().a(this, awVar);
    }

    @Override // net.client.by.lock.d.k
    public void z() {
    }

    @Override // net.client.by.lock.d.k
    public a A() {
        if (this.h == null) {
            this.h = new o(this);
        }
        return this.h;
    }
}
