package net.client.by.lock.d;

import java.util.Collections;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.f.h;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class i implements Observer {
    final /* synthetic */ h a;

    private i(h hVar) {
        this.a = hVar;
    }

    /* synthetic */ i(h hVar, i iVar) {
        this(hVar);
    }

    public void update(Observable observable, Object obj) {
        h hVar = (h) observable;
        boolean booleanValue = ((Boolean) hVar.a()).booleanValue();
        if (hVar.b() == null) {
            Collections.sort(this.a.e(), this.a.e);
        } else if (((Boolean) hVar.b()).booleanValue() != booleanValue) {
            if (booleanValue) {
                this.a.b.a(Integer.valueOf(((Integer) this.a.b.a()).intValue() + 1));
            } else {
                this.a.b.a(Integer.valueOf(((Integer) this.a.b.a()).intValue() - 1));
            }
            Collections.sort(this.a.e(), this.a.e);
        }
    }
}
