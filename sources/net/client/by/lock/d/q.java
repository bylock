package net.client.by.lock.d;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Iterator;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.b.c;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public final class q extends f {
    private PrivateKey e;
    private byte[] f;

    public q() {
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA");
            instance.initialize(new RSAKeyGenParameterSpec(2048, RSAKeyGenParameterSpec.F4), r.i().a());
            KeyPair genKeyPair = instance.genKeyPair();
            this.e = genKeyPair.getPrivate();
            this.a = genKeyPair.getPublic();
            c();
        } catch (NoSuchAlgorithmException e2) {
            j.a("SelfKey", e2);
        } catch (InvalidAlgorithmParameterException e3) {
            j.a("SelfKey", e3);
        }
        e();
    }

    public q(byte[] bArr, byte[] bArr2) {
        byte[] bArr3;
        this.f = bArr2;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(r.i().l().getBytes());
            byte[] digest = instance.digest();
            Cipher instance2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance2.init(2, new SecretKeySpec(digest, 0, 16, "AES"), new IvParameterSpec(digest, digest.length - 16, 16));
            bArr3 = instance2.doFinal(bArr2);
        } catch (NoSuchPaddingException e2) {
            j.a("Error in decrypting private key", e2);
            bArr3 = null;
        } catch (NoSuchAlgorithmException e3) {
            j.a("Error in decrypting private key", e3);
            bArr3 = null;
        } catch (InvalidKeyException e4) {
            j.a("Error in decrypting private key", e4);
            bArr3 = null;
        } catch (InvalidAlgorithmParameterException e5) {
            j.a("Error in decrypting private key", e5);
            bArr3 = null;
        } catch (IllegalBlockSizeException e6) {
            j.a("Error in decrypting private key", e6);
            bArr3 = null;
        } catch (BadPaddingException e7) {
            j.a("Error in decrypting private key", e7);
            bArr3 = null;
        }
        if (!(bArr3 == null || this.b == null)) {
            try {
                BigInteger bigInteger = new BigInteger(1, bArr3);
                BigInteger bigInteger2 = new BigInteger(1, bArr);
                this.a = this.b.generatePublic(new RSAPublicKeySpec(bigInteger2, RSAKeyGenParameterSpec.F4));
                this.e = this.b.generatePrivate(new RSAPrivateKeySpec(bigInteger2, bigInteger));
            } catch (InvalidKeySpecException e8) {
                j.a("Error in instantiating KeyPair.", e8);
            }
        }
        e();
    }

    private void e() {
        if (this.e != null) {
            if (this.d != null) {
                synchronized (this.d) {
                    try {
                        this.d.initSign(this.e);
                    } catch (InvalidKeyException e2) {
                        j.a("Error while initializing signature state.", e2);
                    }
                }
            }
            if (this.c != null) {
                synchronized (this.c) {
                    try {
                        this.c.init(2, this.e);
                    } catch (InvalidKeyException e3) {
                        j.a("Error while initializing cipher state.", e3);
                    }
                }
            }
        }
    }

    public void c() {
        if (this.e != null && r.i().l() != null && this.b != null) {
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-256");
                instance.update(r.i().l().getBytes());
                byte[] digest = instance.digest();
                Cipher instance2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
                instance2.init(1, new SecretKeySpec(digest, 0, 16, "AES"), new IvParameterSpec(digest, digest.length - 16, 16));
                this.f = instance2.doFinal(((RSAPrivateKeySpec) this.b.getKeySpec(this.e, RSAPrivateKeySpec.class)).getPrivateExponent().toByteArray());
            } catch (NoSuchPaddingException e2) {
                j.a("Error in encrypting private key", e2);
            } catch (NoSuchAlgorithmException e3) {
                j.a("Error in encrypting private key", e3);
            } catch (InvalidKeyException e4) {
                j.a("Error in encrypting private key", e4);
            } catch (InvalidAlgorithmParameterException e5) {
                j.a("Error in encrypting private key", e5);
            } catch (InvalidKeySpecException e6) {
                j.a("Error in encrypting private key", e6);
            } catch (IllegalBlockSizeException e7) {
                j.a("Error in encrypting private key", e7);
            } catch (BadPaddingException e8) {
                j.a("Error in encrypting private key", e8);
            }
        }
    }

    public byte[] d() {
        return this.f;
    }

    public byte[] b(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayInputStream byteArrayInputStream;
        Throwable th;
        IOException e2;
        BadPaddingException e3;
        byte[] bArr2 = null;
        if (this.c != null) {
            synchronized (this.c) {
                try {
                    byteArrayInputStream = new ByteArrayInputStream(bArr);
                    try {
                        byteArrayOutputStream = new ByteArrayOutputStream();
                    } catch (IllegalBlockSizeException e4) {
                        e = e4;
                        byteArrayOutputStream = null;
                        try {
                            j.a("Error during decryption.", e);
                            try {
                                byteArrayInputStream.close();
                                byteArrayOutputStream.close();
                            } catch (IOException e5) {
                                e5.printStackTrace();
                            }
                            return bArr2;
                        } catch (Throwable th2) {
                            th = th2;
                            try {
                                byteArrayInputStream.close();
                                byteArrayOutputStream.close();
                            } catch (IOException e6) {
                                e6.printStackTrace();
                            }
                            throw th;
                        }
                    } catch (BadPaddingException e7) {
                        e3 = e7;
                        byteArrayOutputStream = null;
                        j.a("Error during decryption.", e3);
                        try {
                            byteArrayInputStream.close();
                            byteArrayOutputStream.close();
                        } catch (IOException e8) {
                            e8.printStackTrace();
                        }
                        return bArr2;
                    } catch (IOException e9) {
                        e2 = e9;
                        byteArrayOutputStream = null;
                        j.a("Error during decryption.", e2);
                        try {
                            byteArrayInputStream.close();
                            byteArrayOutputStream.close();
                        } catch (IOException e10) {
                            e10.printStackTrace();
                        }
                        return bArr2;
                    } catch (Throwable th3) {
                        byteArrayOutputStream = null;
                        th = th3;
                        byteArrayInputStream.close();
                        byteArrayOutputStream.close();
                        throw th;
                    }
                    try {
                        byte[] bArr3 = new byte[256];
                        int read = byteArrayInputStream.read() + 1;
                        while (true) {
                            if (read <= 0) {
                                break;
                            } else if (byteArrayInputStream.read(bArr3, 0, read) < read) {
                                byteArrayOutputStream.reset();
                                break;
                            } else {
                                byteArrayOutputStream.write(this.c.doFinal(bArr3, 0, read));
                                read = byteArrayInputStream.read() + 1;
                            }
                        }
                        bArr2 = byteArrayOutputStream.toByteArray();
                        try {
                            byteArrayInputStream.close();
                            byteArrayOutputStream.close();
                        } catch (IOException e11) {
                            e11.printStackTrace();
                        }
                    } catch (IllegalBlockSizeException e12) {
                        e = e12;
                        j.a("Error during decryption.", e);
                        byteArrayInputStream.close();
                        byteArrayOutputStream.close();
                        return bArr2;
                    } catch (BadPaddingException e13) {
                        e3 = e13;
                        j.a("Error during decryption.", e3);
                        byteArrayInputStream.close();
                        byteArrayOutputStream.close();
                        return bArr2;
                    } catch (IOException e14) {
                        e2 = e14;
                        j.a("Error during decryption.", e2);
                        byteArrayInputStream.close();
                        byteArrayOutputStream.close();
                        return bArr2;
                    }
                } catch (IllegalBlockSizeException e15) {
                    e = e15;
                    byteArrayOutputStream = null;
                    byteArrayInputStream = null;
                    j.a("Error during decryption.", e);
                    byteArrayInputStream.close();
                    byteArrayOutputStream.close();
                    return bArr2;
                } catch (BadPaddingException e16) {
                    e3 = e16;
                    byteArrayOutputStream = null;
                    byteArrayInputStream = null;
                    j.a("Error during decryption.", e3);
                    byteArrayInputStream.close();
                    byteArrayOutputStream.close();
                    return bArr2;
                } catch (IOException e17) {
                    e2 = e17;
                    byteArrayOutputStream = null;
                    byteArrayInputStream = null;
                    j.a("Error during decryption.", e2);
                    byteArrayInputStream.close();
                    byteArrayOutputStream.close();
                    return bArr2;
                } catch (Throwable th4) {
                    byteArrayOutputStream = null;
                    byteArrayInputStream = null;
                    th = th4;
                    byteArrayInputStream.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            }
        }
        return bArr2;
    }

    public boolean b(b bVar) {
        if (!(this.d == null || bVar == null || bVar.e() == null)) {
            synchronized (this.d) {
                try {
                    this.d.update(bVar.e());
                    bVar.a(this.d.sign());
                } catch (SignatureException e2) {
                    j.a("Error while signing chat.", e2);
                }
            }
            return true;
        }
        return false;
    }

    public boolean a(c cVar) {
        if (!(this.d == null || cVar == null || cVar.a() == null || cVar.b() == null || cVar.c() == null)) {
            synchronized (this.d) {
                try {
                    this.d.update(cVar.a());
                    this.d.update(cVar.b());
                    this.d.update(cVar.c());
                    cVar.d(this.d.sign());
                } catch (SignatureException e2) {
                    j.a("Error in signing file recipient.", e2);
                }
            }
            return true;
        }
        return false;
    }

    public boolean b(net.client.by.lock.a.c cVar) {
        if (!(this.d == null || cVar == null || cVar.f() == null || cVar.g() == null)) {
            synchronized (this.d) {
                try {
                    this.d.update(cVar.f());
                    this.d.update(cVar.g());
                    cVar.b(this.d.sign());
                } catch (SignatureException e2) {
                    j.a("Error in signing file recipient.", e2);
                }
            }
            return true;
        }
        return false;
    }

    public boolean a(n nVar) {
        if (nVar.d()) {
            return true;
        }
        if (nVar.c() == null) {
            if (!(this.d == null || nVar == null || nVar.j() == null || nVar.k() == null || nVar.m() == null)) {
                synchronized (this.d) {
                    try {
                        Iterator it = nVar.l().iterator();
                        while (it.hasNext()) {
                            if (!a((a) it.next())) {
                                nVar.b(false);
                                return false;
                            }
                        }
                        this.d.update(nVar.j());
                        this.d.update(nVar.k());
                        this.d.update(nVar.m());
                        for (int i = 0; i < nVar.l().size(); i++) {
                            this.d.update(((a) nVar.l().get(i)).j());
                        }
                        nVar.a(this.d.sign());
                        nVar.b(true);
                        return true;
                    } catch (SignatureException e2) {
                        j.a("Error in signing mail.", e2);
                    }
                }
            }
            return false;
        }
        Iterator it2 = nVar.c().iterator();
        while (it2.hasNext()) {
            if (!a((n) it2.next())) {
                return false;
            }
        }
        nVar.b(true);
        return true;
    }

    private boolean a(a aVar) {
        if (!(this.d == null || aVar == null || aVar.g() == null || aVar.h() == null || aVar.i() == null)) {
            synchronized (this.d) {
                try {
                    this.d.update(aVar.g());
                    this.d.update(aVar.h());
                    this.d.update(aVar.i());
                    aVar.a(this.d.sign());
                } catch (SignatureException e2) {
                    e2.printStackTrace();
                }
            }
            return true;
        }
        return false;
    }
}
