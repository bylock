package net.client.by.lock.d;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import net.client.by.lock.c.n;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.a.a;
import net.client.by.lock.gui.a.g;

/* compiled from: MyApp */
public class b extends k {
    private Integer a;
    private String b = null;
    private byte[] c = null;
    private byte[] i = null;

    public b(c cVar, String str) {
        super.b(cVar);
        this.b = str;
        a(true);
    }

    public b(c cVar, Integer num, byte[] bArr, byte[] bArr2, String str) {
        this.a = num;
        super.b(cVar);
        d(str);
        this.c = bArr;
        this.i = bArr2;
        a(false);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.d.k
    public String a() {
        return this.b;
    }

    public String b() {
        return this.b;
    }

    public Integer c() {
        return this.a;
    }

    public void d() {
        n.a().a(this);
    }

    public byte[] e() {
        return this.c;
    }

    public byte[] f() {
        return this.i;
    }

    public void a(byte[] bArr) {
        this.i = bArr;
    }

    public boolean g() {
        if (this.e == null || this.b == null) {
            return false;
        }
        try {
            this.c = this.e.u().a(this.b.getBytes("UTF-8"));
            if (this.c != null) {
                return true;
            }
            return false;
        } catch (IOException e) {
            j.a("", e);
            this.c = null;
            return false;
        }
    }

    public boolean h() {
        if (this.c != null) {
            try {
                byte[] b2 = r.i().p().b(this.c);
                if (b2 != null) {
                    this.b = new String(b2, "UTF-8");
                    return true;
                }
            } catch (UnsupportedEncodingException e) {
                j.a("", e);
                this.b = "";
            }
        }
        return false;
    }

    @Override // net.client.by.lock.d.k
    public void z() {
        if (!b_()) {
            n.a().b(this);
        }
    }

    @Override // net.client.by.lock.d.k
    public a A() {
        if (this.h == null) {
            this.h = new g(this);
        }
        return this.h;
    }
}
