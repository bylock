package net.client.by.lock.d;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.StringTokenizer;
import net.client.by.lock.c.n;
import net.client.by.lock.f.h;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.a.a;

/* compiled from: MyApp */
public class l extends m {
    private h o;

    public l(c cVar, String str, String str2, byte[] bArr, boolean z) {
        b(cVar);
        this.a = str;
        d(str2);
        this.f.a("IDLE");
        this.j = bArr;
        this.b = "";
        this.o = new h(Boolean.valueOf(z), "isRead");
        try {
            this.b = new String(r.i().p().b(bArr), "UTF-8");
        } catch (Exception e) {
            this.b = "<No Subject>";
        }
    }

    @Override // net.client.by.lock.d.k
    public boolean b_() {
        return false;
    }

    public synchronized void b() {
        this.o.a((Object) true);
        r.i().a(this.a);
    }

    public void a(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        this.j = bArr;
        this.k = bArr2;
        this.m = bArr3;
    }

    public void c() {
        n.a().a(this);
    }

    public boolean d() {
        if (!(this.e == null || this.j == null || this.k == null || this.l == null)) {
            try {
                Iterator it = this.n.iterator();
                while (it.hasNext()) {
                    if (!((a) it.next()).k()) {
                        return false;
                    }
                }
                byte[] b = r.i().p().b(this.j);
                byte[] b2 = r.i().p().b(this.k);
                byte[] bArr = new byte[0];
                if (this.l.length > 0) {
                    bArr = r.i().p().b(this.l);
                }
                if (!(b == null || b2 == null || bArr == null)) {
                    this.b = new String(b, "UTF-8");
                    this.c = new String(b2, "UTF-8");
                    StringTokenizer stringTokenizer = new StringTokenizer(new String(bArr, "UTF-8"), ",");
                    while (stringTokenizer.hasMoreTokens()) {
                        try {
                            this.i.add(Integer.valueOf(Integer.parseInt(stringTokenizer.nextToken())));
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
            } catch (UnsupportedEncodingException e2) {
                j.a("", e2);
                this.b = null;
                this.c = null;
            }
        }
        return false;
    }

    @Override // net.client.by.lock.d.k
    public void z() {
        n.a().f(this.a);
    }

    @Override // net.client.by.lock.d.k
    public a A() {
        if (this.h == null) {
            this.h = new net.client.by.lock.gui.a.j(this);
        }
        return this.h;
    }

    public boolean e() {
        return ((Boolean) this.o.a()).booleanValue();
    }
}
