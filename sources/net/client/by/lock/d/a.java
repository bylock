package net.client.by.lock.d;

import java.io.UnsupportedEncodingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.b.d;
import net.client.by.lock.f.h;

/* compiled from: MyApp */
public class a implements net.client.by.lock.b.a {
    h a = new h("fileId");
    SecretKeySpec b;
    byte[] c;
    IvParameterSpec d;
    byte[] e;
    String f;
    byte[] g;
    byte[] h;
    int i;
    d j = null;

    public a(String str, String str2, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, int i2) {
        this.a.a(str);
        this.g = bArr;
        this.c = bArr2;
        this.e = bArr3;
        this.h = bArr4;
        this.i = i2;
    }

    public a(net.client.by.lock.b.a aVar) {
        if (aVar instanceof d) {
            this.a.a(((d) aVar).h());
        } else {
            this.a.a(aVar.b());
        }
        this.b = aVar.c();
        this.d = aVar.d();
        this.i = aVar.f().intValue();
        this.f = aVar.e();
    }

    public void a(d dVar) {
        this.j = dVar;
    }

    public d a() {
        return this.j;
    }

    @Override // net.client.by.lock.b.a
    public String b() {
        return (String) this.a.a();
    }

    public byte[] g() {
        return this.g;
    }

    @Override // net.client.by.lock.b.a
    public String e() {
        return this.f;
    }

    public byte[] h() {
        return this.c;
    }

    public byte[] i() {
        return this.e;
    }

    public byte[] j() {
        return this.h;
    }

    @Override // net.client.by.lock.b.a
    public Integer f() {
        return Integer.valueOf(this.i);
    }

    public void a(byte[] bArr) {
        this.h = bArr;
    }

    public boolean a(f fVar) {
        this.c = fVar.a(this.b.getEncoded());
        this.e = fVar.a(this.d.getIV());
        try {
            this.g = fVar.a(this.f.getBytes("UTF-8"));
            if (this.c == null || this.e == null || this.g == null) {
                return false;
            }
            return true;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean k() {
        this.b = new SecretKeySpec(r.i().p().b(this.c), "AES");
        this.d = new IvParameterSpec(r.i().p().b(this.e));
        try {
            this.f = new String(r.i().p().b(this.g), "UTF-8");
            if (this.b == null || this.d == null || this.f == null) {
                return false;
            }
            return true;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @Override // net.client.by.lock.b.a
    public SecretKeySpec c() {
        return this.b;
    }

    @Override // net.client.by.lock.b.a
    public IvParameterSpec d() {
        return this.d;
    }
}
