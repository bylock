package net.client.by.lock.d;

import java.util.ArrayList;
import net.client.by.lock.b.k;

/* compiled from: MyApp */
public abstract class m extends k {
    protected String a;
    protected String b;
    protected String c;
    protected ArrayList i = new ArrayList();
    protected byte[] j;
    protected byte[] k;
    protected byte[] l;
    protected byte[] m;
    protected ArrayList n = new ArrayList();

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.d.k
    public String a() {
        return String.valueOf(this.b) + " (Posta)";
    }

    public String f() {
        return this.c;
    }

    public String g() {
        return this.b;
    }

    public byte[] h() {
        return this.m;
    }

    public void a(byte[] bArr) {
        this.m = bArr;
    }

    public String i() {
        return this.a;
    }

    public byte[] j() {
        return this.j;
    }

    public byte[] k() {
        return this.k;
    }

    public void a(a aVar) {
        k.b().a(aVar);
        if (this.n.isEmpty()) {
            this.n.add(aVar);
        } else if (aVar.b().compareTo(((a) this.n.get(0)).b()) < 0) {
            this.n.add(0, aVar);
        } else {
            for (int size = this.n.size() - 1; size >= 0; size--) {
                int compareTo = aVar.b().compareTo(((a) this.n.get(size)).b());
                if (compareTo == 0) {
                    return;
                }
                if (compareTo > 0) {
                    this.n.add(size + 1, aVar);
                    return;
                }
            }
        }
    }

    public ArrayList l() {
        return this.n;
    }

    public byte[] m() {
        return this.l;
    }

    public void b(byte[] bArr) {
        this.l = bArr;
    }

    public ArrayList n() {
        return this.i;
    }
}
