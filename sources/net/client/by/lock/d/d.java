package net.client.by.lock.d;

import java.util.Observable;
import java.util.Observer;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class d implements Observer {
    final /* synthetic */ c a;

    private d(c cVar) {
        this.a = cVar;
    }

    /* synthetic */ d(c cVar, d dVar) {
        this(cVar);
    }

    public void update(Observable observable, Object obj) {
        this.a.w.a(Boolean.valueOf(((Integer) this.a.k.a()).intValue() > 0 || ((Boolean) this.a.l.a()).booleanValue() || ((Boolean) this.a.m.a()).booleanValue() || ((Boolean) this.a.n.a()).booleanValue()));
    }
}
