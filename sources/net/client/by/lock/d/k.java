package net.client.by.lock.d;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import net.client.by.lock.f.h;
import net.client.by.lock.gui.a.a;

/* compiled from: MyApp */
public abstract class k {
    private boolean a;
    protected h d = new h("01.01.2970 00:00", "dateTimeProperty");
    protected c e;
    protected h f = new h("stateProperty");
    protected String g;
    protected a h = null;

    public abstract a A();

    /* access modifiers changed from: protected */
    public abstract String a();

    public abstract void z();

    public String toString() {
        if (this.a) {
            return String.valueOf((String) r.i().f().a()) + " (" + ((String) this.d.a()) + "): " + a();
        }
        return String.valueOf((String) this.e.F().a()) + " (" + ((String) this.d.a()) + "): " + a();
    }

    public String B() {
        return (String) this.d.a();
    }

    public h C() {
        return this.d;
    }

    public void d(String str) {
        this.d.a(str);
    }

    public void a(boolean z) {
        this.a = z;
    }

    public boolean b_() {
        return this.a;
    }

    public boolean D() {
        return this.a;
    }

    public c E() {
        return this.e;
    }

    public void b(c cVar) {
        this.e = cVar;
    }

    public h F() {
        return this.f;
    }

    public synchronized void e(String str) {
        this.f.a(str);
    }

    public synchronized void f(String str) {
        this.f.a("ERROR");
        this.g = str;
    }

    public int a(k kVar) {
        try {
            try {
                return new SimpleDateFormat("dd.MM.yyyy HH:mm", net.client.by.lock.a.f).parse((String) this.d.a()).compareTo(new SimpleDateFormat("dd.MM.yyyy HH:mm", net.client.by.lock.a.f).parse((String) kVar.d.a()));
            } catch (ParseException e2) {
                return -1;
            }
        } catch (ParseException e3) {
            return 1;
        }
    }
}
