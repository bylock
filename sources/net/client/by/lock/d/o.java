package net.client.by.lock.d;

import android.content.Context;
import java.util.StringTokenizer;
import net.client.by.lock.c.g;
import net.client.by.lock.c.i;
import net.client.by.lock.c.l;
import net.client.by.lock.c.n;
import net.client.by.lock.e.b;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.activity.ChatActivity;
import net.client.by.lock.gui.activity.a;

/* compiled from: MyApp */
public class o {
    private Integer a;
    private String b;
    private int c;
    private String d;

    public String a() {
        return this.b;
    }

    public void a(String str) {
        this.b = str;
    }

    public void a(int i) {
        this.c = i;
    }

    public void b(String str) {
        this.d = str;
    }

    public void b() {
        boolean z = false;
        c a2 = i.c().a(Integer.valueOf(this.c));
        if (a2 != null) {
            if (this.b.equalsIgnoreCase("LOGIN")) {
                a2.a(3);
                a2.e(this.d);
            } else if (this.b.equalsIgnoreCase("LOGOUT")) {
                a2.a(0);
            } else if (this.b.equalsIgnoreCase("UPDATE STATUS")) {
                try {
                    a2.a(Integer.parseInt(this.d));
                } catch (NumberFormatException e) {
                }
            } else if (this.b.equalsIgnoreCase("UPDATE PUBLIC MESSAGE")) {
                a2.b(this.d);
            } else if (this.b.equalsIgnoreCase("CHAT")) {
                a2.m();
                Context b2 = g.a().b();
                if (b2 instanceof ChatActivity) {
                    c i = ((ChatActivity) b2).i();
                    if ((i != null && !i.equals(a2)) || a.a()) {
                        l.a().a(a2, 1);
                    }
                } else {
                    l.a().a(a2, 1);
                }
            } else if (this.b.equalsIgnoreCase("UPDATE KEY")) {
                a2.a(b.a(this.d));
            } else if (this.b.equalsIgnoreCase("UPDATE NAME")) {
                a2.f(this.d);
            } else if (this.b.equalsIgnoreCase("REMOVE FROM ROSTER")) {
                i.c().b(a2);
            } else if (this.b.equalsIgnoreCase("SEND FILE")) {
                n.a().b(this.d);
                Context b3 = g.a().b();
                if (!(b3 instanceof ChatActivity)) {
                    l.a().a(a2, 2);
                } else if (!((ChatActivity) b3).i().equals(a2) || a.a()) {
                    l.a().a(a2, 2);
                }
            } else if (this.b.equalsIgnoreCase("MAKE CALL")) {
                StringTokenizer stringTokenizer = new StringTokenizer(this.d, "#");
                if (stringTokenizer.countTokens() == 4) {
                    byte[] a3 = j.a(stringTokenizer.nextToken());
                    byte[] a4 = j.a(stringTokenizer.nextToken());
                    String nextToken = stringTokenizer.nextToken();
                    boolean equals = stringTokenizer.nextToken().equals("1");
                    if (!(a3 == null || a4 == null)) {
                        a2.a(a3, a4, nextToken, equals);
                    }
                }
            } else if (this.b.equalsIgnoreCase("ANSWER CALL")) {
                StringTokenizer stringTokenizer2 = new StringTokenizer(this.d, "#");
                if (stringTokenizer2.countTokens() == 2) {
                    try {
                        a2.a(Integer.parseInt(stringTokenizer2.nextToken()), stringTokenizer2.nextToken().equals("1"));
                    } catch (NumberFormatException e2) {
                    }
                }
            } else if (this.b.equalsIgnoreCase("REJECT CALL")) {
                a2.d();
            } else if (this.b.equalsIgnoreCase("MISS CALL")) {
                a2.e();
            } else if (this.b.equalsIgnoreCase("CANCEL CALL")) {
                a2.f();
            } else if (this.b.equalsIgnoreCase("CLOSE CALL")) {
                a2.g();
            } else if (this.b.equalsIgnoreCase("BUSY CALL")) {
                a2.i();
            } else if (this.b.equalsIgnoreCase("CALL ERROR")) {
                a2.h();
            } else if (this.b.equalsIgnoreCase("MAIL")) {
                StringTokenizer stringTokenizer3 = new StringTokenizer(this.d, "#");
                if (stringTokenizer3.countTokens() == 4) {
                    String nextToken2 = stringTokenizer3.nextToken();
                    String nextToken3 = stringTokenizer3.nextToken();
                    String nextToken4 = stringTokenizer3.nextToken();
                    z = stringTokenizer3.nextToken().trim().equals("1");
                    a2.a(nextToken2, nextToken3, b.a(nextToken4), z);
                }
                if (!z) {
                    Context b4 = g.a().b();
                    if (!(b4 instanceof ChatActivity)) {
                        l.a().a(a2, 3);
                    } else if (((ChatActivity) b4).j() != this.c || a.a()) {
                        l.a().a(a2, 3);
                    }
                }
            }
        } else if (this.b.equalsIgnoreCase("ADD TO ROSTER")) {
            n.a().a(Integer.valueOf(this.c));
        } else if (this.b.equalsIgnoreCase("UPDATE SELF STATUS")) {
            try {
                r.i().a(Integer.valueOf(Integer.parseInt(this.d)));
            } catch (NumberFormatException e3) {
                j.a("", e3);
            }
        } else if (this.b.equalsIgnoreCase("UPDATE SELF PUBLIC MESSAGE")) {
            r.i().i(this.d);
        } else if (this.b.equalsIgnoreCase("CREATE GROUP")) {
            String[] split = this.d.split("#", -1);
            if (split.length == 3) {
                try {
                    Integer valueOf = Integer.valueOf(Integer.parseInt(split[0]));
                    String str = split[1];
                    Integer valueOf2 = Integer.valueOf(Integer.parseInt(split[2]));
                    i.c().a(new g(valueOf.intValue(), str));
                    i.c().a(valueOf2, valueOf);
                } catch (NumberFormatException e4) {
                    j.a("", e4);
                }
            }
        } else if (this.b.equalsIgnoreCase("RENAME GROUP")) {
            String[] split2 = this.d.split("#", -1);
            if (split2.length == 2) {
                try {
                    i.c().a(Integer.valueOf(Integer.parseInt(split2[0])), split2[1]);
                } catch (NumberFormatException e5) {
                    j.a("", e5);
                }
            }
        } else if (this.b.equalsIgnoreCase("ADD TO GROUP")) {
            String[] split3 = this.d.split("#", -1);
            if (split3.length == 2) {
                try {
                    Integer valueOf3 = Integer.valueOf(Integer.parseInt(split3[0]));
                    i.c().a(Integer.valueOf(Integer.parseInt(split3[1])), valueOf3);
                } catch (NumberFormatException e6) {
                    j.a("", e6);
                }
            }
        } else if (this.b.equalsIgnoreCase("REMOVE FROM GROUP")) {
            String[] split4 = this.d.split("#", -1);
            if (split4.length == 2) {
                try {
                    Integer valueOf4 = Integer.valueOf(Integer.parseInt(split4[0]));
                    i.c().b(Integer.valueOf(Integer.parseInt(split4[1])), valueOf4);
                } catch (NumberFormatException e7) {
                    j.a("", e7);
                }
            }
        } else if (this.b.equalsIgnoreCase("DELETE GROUP")) {
            try {
                i.c().a(Integer.valueOf(Integer.parseInt(this.d)).intValue());
            } catch (NumberFormatException e8) {
                j.a("", e8);
            }
        } else if (this.b.equalsIgnoreCase("RENAME FRIEND")) {
            String[] split5 = this.d.split("#", -1);
            if (split5.length == 2) {
                try {
                    i.c().a(Integer.valueOf(Integer.parseInt(split5[0]))).d(split5[1]);
                } catch (NumberFormatException e9) {
                    j.a("", e9);
                }
            }
        }
        r.i().c(this.a);
    }

    public void b(int i) {
        this.a = Integer.valueOf(i);
    }
}
