package net.client.by.lock.d;

import java.util.Comparator;
import java.util.Iterator;

/* compiled from: MyApp */
public class h extends s {
    private net.client.by.lock.f.h a;
    private net.client.by.lock.f.h b;
    private i c;
    private j d = new j(this, null);
    private Comparator e;

    public h(g gVar, Comparator comparator) {
        super(gVar);
        this.e = comparator;
        this.a = new net.client.by.lock.f.h(0, "totalCountProperty");
        this.b = new net.client.by.lock.f.h(0, "onlineCountProperty");
        this.c = new i(this, null);
    }

    public synchronized void a(s sVar) {
        if (sVar.d() instanceof c) {
            c cVar = (c) sVar.d();
            e().add(sVar);
            cVar.a(this.c);
            if (((Boolean) cVar.q().a()).booleanValue()) {
                this.b.a(Integer.valueOf(((Integer) this.b.a()).intValue() + 1));
            }
            cVar.J().addObserver(this.d);
            cVar.F().addObserver(this.d);
            this.d.update(null, null);
            this.a.a(Integer.valueOf(((Integer) this.a.a()).intValue() + 1));
        }
    }

    public synchronized void a(int i) {
        s sVar;
        Iterator it = e().iterator();
        while (true) {
            if (it.hasNext()) {
                sVar = (s) it.next();
                if (((c) sVar.d()).K() == i) {
                    break;
                }
            } else {
                sVar = null;
                break;
            }
        }
        if (sVar != null && e().remove(sVar)) {
            c cVar = (c) sVar.d();
            cVar.b(this.c);
            if (((Boolean) cVar.q().a()).booleanValue()) {
                cVar.q().a((Object) false);
            }
            this.c.update(cVar.q(), null);
            cVar.J().deleteObserver(this.d);
            cVar.F().deleteObserver(this.d);
            this.a.a(Integer.valueOf(((Integer) this.a.a()).intValue() - 1));
        }
    }

    public synchronized void a() {
        for (s sVar : (s[]) e().toArray(new s[0])) {
            a(((c) sVar.d()).K());
        }
    }

    public net.client.by.lock.f.h b() {
        return this.a;
    }

    public net.client.by.lock.f.h c() {
        return this.b;
    }
}
