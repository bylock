package net.client.by.lock.d;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Iterator;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import net.client.by.lock.a.c;
import net.client.by.lock.b.g;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class f {
    protected PublicKey a;
    protected KeyFactory b;
    protected Cipher c;
    protected Signature d;

    public f() {
        c();
    }

    public f(byte[] bArr) {
        c();
        try {
            this.a = this.b.generatePublic(new RSAPublicKeySpec(new BigInteger(bArr), RSAKeyGenParameterSpec.F4));
        } catch (InvalidKeySpecException e) {
            j.a("Error in instantiating PublicKey.", e);
        }
        d();
    }

    private void c() {
        if (this.b == null || this.d == null || this.c == null) {
            try {
                this.b = KeyFactory.getInstance("RSA");
                this.d = Signature.getInstance("SHA1withRSA");
                this.c = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
            } catch (NoSuchAlgorithmException e) {
                j.a("Error in initializing byLockKeyPair", e);
                this.b = null;
                this.d = null;
                this.c = null;
            } catch (NoSuchPaddingException e2) {
                j.a("Error in initializing byLockKeyPair", e2);
                this.b = null;
                this.d = null;
                this.c = null;
            }
        }
    }

    private void d() {
        if (this.a != null) {
            if (this.d != null) {
                synchronized (this.d) {
                    try {
                        this.d.initVerify(this.a);
                    } catch (InvalidKeyException e) {
                        j.a("Error while initializing signature state.", e);
                    }
                }
            }
            if (this.c != null) {
                synchronized (this.c) {
                    try {
                        this.c.init(1, this.a);
                    } catch (InvalidKeyException e2) {
                        j.a("Error while initializing cipher state.", e2);
                    }
                }
            }
        }
    }

    public byte[] a() {
        if (this.a == null || this.b == null) {
            return null;
        }
        try {
            return ((RSAPublicKeySpec) this.b.getKeySpec(this.a, RSAPublicKeySpec.class)).getModulus().toByteArray();
        } catch (InvalidKeySpecException e) {
            j.a("", e);
            return null;
        }
    }

    public BigInteger b() {
        if (this.a == null || this.b == null) {
            return null;
        }
        try {
            return ((RSAPublicKeySpec) this.b.getKeySpec(this.a, RSAPublicKeySpec.class)).getModulus();
        } catch (InvalidKeySpecException e) {
            j.a("", e);
            return null;
        }
    }

    public boolean a(b bVar) {
        boolean verify;
        if (this.d != null) {
            synchronized (this.d) {
                try {
                    this.d.update(bVar.e());
                    verify = this.d.verify(bVar.f());
                } catch (SignatureException e) {
                    j.a("Error while verifying chat.", e);
                }
            }
            return verify;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0042 A[SYNTHETIC, Splitter:B:25:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0070 A[SYNTHETIC, Splitter:B:40:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0082 A[SYNTHETIC, Splitter:B:48:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0090 A[SYNTHETIC, Splitter:B:55:0x0090] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] a(byte[] r8) {
        /*
        // Method dump skipped, instructions count: 162
        */
        throw new UnsupportedOperationException("Method not decompiled: net.client.by.lock.d.f.a(byte[]):byte[]");
    }

    public boolean a(g gVar) {
        boolean verify;
        if (this.d != null) {
            synchronized (this.d) {
                try {
                    this.d.update(gVar.m());
                    this.d.update(gVar.n());
                    this.d.update(gVar.o());
                    verify = this.d.verify(gVar.p());
                } catch (SignatureException e) {
                    j.a("Error while verifying chat.", e);
                }
            }
            return verify;
        }
        return false;
    }

    public boolean a(c cVar) {
        boolean verify;
        if (this.d != null) {
            synchronized (this.d) {
                try {
                    this.d.update(cVar.f());
                    this.d.update(cVar.g());
                    verify = this.d.verify(cVar.h());
                } catch (SignatureException e) {
                    j.a("Error while verifying chat.", e);
                }
            }
            return verify;
        }
        return false;
    }

    public boolean a(l lVar) {
        if (this.d != null) {
            synchronized (this.d) {
                try {
                    this.d.update(lVar.j());
                    this.d.update(lVar.k());
                    this.d.update(lVar.m());
                    for (int i = 0; i < lVar.l().size(); i++) {
                        this.d.update(((a) lVar.l().get(i)).j());
                    }
                    if (this.d.verify(lVar.h())) {
                        Iterator it = lVar.l().iterator();
                        while (it.hasNext()) {
                            if (!a((a) it.next())) {
                                return false;
                            }
                        }
                        return true;
                    }
                } catch (SignatureException e) {
                    j.a("Error while verifying mail.", e);
                }
            }
        }
        return false;
        return false;
    }

    private boolean a(a aVar) {
        if (!(this.d == null || aVar == null || aVar.g() == null || aVar.h() == null || aVar.i() == null || aVar.j() == null)) {
            try {
                this.d.update(aVar.g());
                this.d.update(aVar.h());
                this.d.update(aVar.i());
                return this.d.verify(aVar.j());
            } catch (SignatureException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
