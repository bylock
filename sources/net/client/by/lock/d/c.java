package net.client.by.lock.d;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observer;
import javax.crypto.KeyGenerator;
import net.client.by.lock.b.d;
import net.client.by.lock.b.k;
import net.client.by.lock.c.l;
import net.client.by.lock.c.n;
import net.client.by.lock.f.g;
import net.client.by.lock.f.h;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.b.a;

/* compiled from: MyApp */
public class c extends p implements Comparable {
    private f a = null;
    private String b;
    private int c;
    private String d = "";
    private String e = "";
    private ArrayList f = new ArrayList();
    private h g = new h("", "sas");
    private h h = new h("", "publicMessage");
    private h i = new h("(No Login.)", "lastOnline");
    private h j = new h(0, "status");
    private h k = new h(0, "chatCount");
    private h l = new h(false, "fileExists");
    private h m = new h(false, "mailExists");
    private h n = new h(false, "callExists");
    private h o = new h("description");
    private h p = new h(false, "onlineProperty");
    private boolean q = false;
    private boolean r = false;
    private Integer s = -1;
    private net.client.by.lock.a.c t;
    private g u = new g();
    private h v = new h("NK", "verificationStateProperty");
    private h w = new h(false, "notificationExists");
    private SoftReference x;

    public c() {
        d dVar = new d(this, null);
        this.k.addObserver(dVar);
        this.l.addObserver(dVar);
        this.m.addObserver(dVar);
        this.n.addObserver(dVar);
        this.j.addObserver(new e(this, null));
    }

    public synchronized void a(k kVar) {
        this.u.remove(kVar);
    }

    public synchronized void a(b bVar) {
        if (this.a != null && this.a.a(bVar) && bVar.h()) {
            bVar.b(this);
            b((k) bVar);
        }
        if (bVar.c().intValue() > this.s.intValue()) {
            this.s = bVar.c();
        }
        r.i().a(this, bVar.c());
    }

    public synchronized void b(b bVar) {
        if (this.a != null && this.a.a(bVar) && bVar.h()) {
            bVar.b(this);
            b((k) bVar);
        }
        if (bVar.c().intValue() > this.s.intValue()) {
            this.s = bVar.c();
        }
        r.i().a(this, bVar.c());
        this.k.a(Integer.valueOf(((Integer) this.k.a()).intValue() - 1));
    }

    public synchronized void a(Integer num) {
        this.f.add(num);
    }

    public synchronized void b(Integer num) {
        this.f.remove(num);
    }

    public ArrayList a() {
        return this.f;
    }

    public synchronized boolean a(net.client.by.lock.b.g gVar) {
        boolean z = true;
        synchronized (this) {
            if (this.a == null || !this.a.a(gVar) || !gVar.q()) {
                z = false;
            } else {
                b(gVar);
                k.b().a(gVar);
                if (!this.q) {
                    this.l.a((Object) true);
                }
            }
        }
        return z;
    }

    public synchronized void b() {
        if (((Integer) this.k.a()).intValue() <= 0 || !this.q) {
            this.r = false;
        } else {
            n.a().c(this);
            this.r = true;
        }
    }

    public synchronized void a(String str) {
        b bVar = new b(this, str);
        b((k) bVar);
        bVar.d();
    }

    public synchronized void b(k kVar) {
        if (this.u.size() == 0) {
            this.u.add(kVar);
        } else if (kVar.a((k) this.u.get(this.u.size() - 1)) >= 0) {
            this.u.add(kVar);
        } else {
            int i2 = 0;
            while (true) {
                if (i2 >= this.u.size()) {
                    break;
                } else if (kVar.a((k) this.u.get(i2)) < 0) {
                    this.u.add(i2, kVar);
                    break;
                } else {
                    i2++;
                }
            }
        }
    }

    public synchronized void a(File file) {
        if (this.a == null) {
            l.a().a(String.valueOf(G()) + " has no key yet. No communication is possible.");
        } else {
            net.client.by.lock.b.h hVar = new net.client.by.lock.b.h(file);
            hVar.b(this);
            net.client.by.lock.b.c a2 = a((d) hVar);
            if (a2 != null) {
                hVar.l().add(a2);
                b(hVar);
                hVar.m();
            } else {
                j.a("Error in sending file: FileRecipient object is null.", new Exception());
            }
        }
    }

    public synchronized void c() {
        if (net.client.by.lock.a.h.a().b()) {
            try {
                KeyGenerator instance = KeyGenerator.getInstance("AES");
                instance.init(128, r.i().a());
                net.client.by.lock.a.c a2 = net.client.by.lock.a.c.a(this, instance.generateKey().getEncoded(), instance.generateKey().getEncoded());
                n.a().a(a2);
                b(a2);
                this.t = a2;
            } catch (NoSuchAlgorithmException e2) {
                j.a("Error in creating call key", e2);
            }
        }
    }

    public synchronized void a(byte[] bArr, byte[] bArr2, String str, boolean z) {
        net.client.by.lock.a.c a2 = net.client.by.lock.a.c.a(this, bArr, bArr2, z);
        if (r.i().p() == null) {
            n.a().c(a2);
        } else if (net.client.by.lock.a.h.a().b()) {
            b(a2);
            a2.d(str);
            this.t = a2;
            l.a().a(a2);
        } else {
            n.a().c(a2);
            net.client.by.lock.a.c a3 = net.client.by.lock.a.c.a(this);
            b(a3);
            a3.d(str);
        }
    }

    public synchronized void a(int i2, boolean z) {
        this.t.a(i2, z);
    }

    public synchronized void d() {
        this.t.k();
    }

    public synchronized void e() {
        this.t.l();
    }

    public synchronized void f() {
        this.t.n();
        if (!this.q) {
            this.n.a((Object) true);
        }
    }

    public synchronized void g() {
        this.t.s();
    }

    public synchronized void h() {
        this.t.t();
    }

    public synchronized void i() {
        b(net.client.by.lock.a.c.a(this));
    }

    public synchronized void j() {
        this.q = true;
        if (((Integer) this.k.a()).intValue() > 0 && !this.r) {
            n.a().c(this);
            this.r = true;
        }
        this.l.a((Object) false);
        this.n.a((Object) false);
        this.m.a((Object) false);
    }

    public synchronized void k() {
        this.q = false;
    }

    public Integer l() {
        return this.s;
    }

    public synchronized void m() {
        this.k.a(Integer.valueOf(((Integer) this.k.a()).intValue() + 1));
        if (this.q && !this.r && ((Integer) this.k.a()).intValue() > 0) {
            n.a().c(this);
            this.r = true;
        }
    }

    public g n() {
        return this.u;
    }

    public synchronized int o() {
        return ((Integer) this.k.a()).intValue();
    }

    public h p() {
        return this.k;
    }

    public h q() {
        return this.p;
    }

    public synchronized void a(int i2) {
        this.j.a(Integer.valueOf(i2));
    }

    public synchronized void a(Observer observer) {
        this.p.addObserver(observer);
    }

    public synchronized void b(Observer observer) {
        this.p.deleteObserver(observer);
    }

    public h r() {
        return this.j;
    }

    public String s() {
        return (String) this.h.a();
    }

    public h t() {
        return this.h;
    }

    public void b(String str) {
        if (str.length() > 0) {
            this.h.a("\"" + str + "\"");
        } else {
            this.h.a("");
        }
    }

    public f u() {
        return this.a;
    }

    public void a(byte[] bArr) {
        if (bArr != null) {
            this.a = new f(bArr);
        } else {
            this.a = null;
            I();
            this.l.a((Object) false);
            this.m.a((Object) false);
            this.n.a((Object) false);
            this.k.a((Object) 0);
        }
        v();
        w();
    }

    public void v() {
        String str;
        if (this.a == null || r.i().p() == null) {
            this.g.a("");
            return;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            BigInteger b2 = r.i().p().b();
            BigInteger b3 = this.a.b();
            if (b2.compareTo(b3) < 0) {
                instance.update(b2.toByteArray());
                instance.update(b3.toByteArray());
            } else {
                instance.update(b3.toByteArray());
                instance.update(b2.toByteArray());
            }
            byte[] digest = instance.digest();
            String str2 = "";
            int i2 = 0;
            while (i2 < 10) {
                digest[i2] = (byte) (digest[i2] & 31);
                if (digest[i2] < 10) {
                    str = String.valueOf(str2) + ((int) digest[i2]);
                } else {
                    char c2 = (char) ((digest[i2] - 10) + 65);
                    if (c2 == 'O') {
                        c2 = 'Y';
                    } else if (c2 == 'Q') {
                        c2 = 'Z';
                    }
                    str = String.valueOf(str2) + c2;
                }
                i2++;
                str2 = str;
            }
            this.g.a(str2);
        } catch (NoSuchAlgorithmException e2) {
            j.a("", e2);
        }
    }

    public void w() {
        if (this.a == null) {
            this.v.a("NK");
        } else if (!net.client.by.lock.c.c().b(Integer.valueOf(this.c), this.b)) {
            this.v.a("KWOS");
        } else if (net.client.by.lock.c.c().b(Integer.valueOf(this.c), this.b, this.a.a())) {
            this.v.a("KWCS");
        } else {
            this.v.a("KWIS");
        }
    }

    public h x() {
        return this.v;
    }

    public h y() {
        return this.l;
    }

    public h z() {
        return this.m;
    }

    public h A() {
        return this.n;
    }

    public h B() {
        return this.g;
    }

    public String C() {
        return this.b;
    }

    public void c(String str) {
        this.b = str;
        N();
    }

    public String D() {
        return this.e;
    }

    public void d(String str) {
        this.e = str;
        N();
    }

    public void e(String str) {
        this.i.a(str);
    }

    public String E() {
        return this.d;
    }

    public void f(String str) {
        this.d = str;
        N();
    }

    private void N() {
        if (this.e != null && this.e.length() > 0) {
            this.o.a(this.e);
        } else if (this.d == null || this.d.length() <= 0) {
            this.o.a(this.b);
        } else {
            this.o.a(this.d);
        }
    }

    public h F() {
        return this.o;
    }

    public String G() {
        return (String) this.o.a();
    }

    public net.client.by.lock.b.c a(d dVar) {
        net.client.by.lock.b.c cVar = new net.client.by.lock.b.c(this);
        try {
            cVar.a(this.a.a(dVar.e().getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e2) {
            j.a("", e2);
        }
        cVar.b(this.a.a(dVar.c().getEncoded()));
        cVar.c(this.a.a(dVar.d().getIV()));
        if (r.i().p().a(cVar)) {
            return cVar;
        }
        return null;
    }

    public void H() {
        if (net.client.by.lock.c.c().b(Integer.valueOf(this.c), this.b)) {
            net.client.by.lock.c.c().a(Integer.valueOf(this.c), this.b);
        } else {
            net.client.by.lock.c.c().a(Integer.valueOf(K()), C(), this.a.a());
        }
        w();
    }

    public synchronized void I() {
        this.u.clear();
    }

    public synchronized void a(String str, String str2, byte[] bArr, boolean z) {
        if (this.a != null) {
            b(new l(this, str, str2, bArr, z));
            if (!this.q && !z) {
                this.m.a((Object) true);
            }
        }
    }

    public h J() {
        return this.w;
    }

    /* renamed from: a */
    public int compareTo(c cVar) {
        if (((Boolean) this.w.a()).booleanValue() && !((Boolean) cVar.w.a()).booleanValue()) {
            return -1;
        }
        if (!((Boolean) this.w.a()).booleanValue() && ((Boolean) cVar.w.a()).booleanValue()) {
            return 1;
        }
        if ((((Integer) this.j.a()).intValue() == 3 || ((Integer) this.j.a()).intValue() == 2 || ((Integer) this.j.a()).intValue() == 1) && ((Integer) cVar.j.a()).intValue() == 0) {
            return -2;
        }
        if ((((Integer) cVar.j.a()).intValue() == 3 || ((Integer) cVar.j.a()).intValue() == 2 || ((Integer) cVar.j.a()).intValue() == 1) && ((Integer) this.j.a()).intValue() == 0) {
            return 2;
        }
        return ((String) this.o.a()).compareToIgnoreCase((String) cVar.o.a());
    }

    public int K() {
        return this.c;
    }

    public void b(int i2) {
        this.c = i2;
    }

    public net.client.by.lock.a.c L() {
        return this.t;
    }

    public a M() {
        if (this.x == null || this.x.get() == null) {
            this.x = new SoftReference(new a(this.u));
        }
        return (a) this.x.get();
    }

    public m g(String str) {
        m mVar;
        Iterator it = this.u.iterator();
        while (it.hasNext()) {
            k kVar = (k) it.next();
            if ((kVar instanceof m) && (mVar = (m) kVar) != null && mVar.i() != null && mVar.i().equals(str)) {
                return (m) kVar;
            }
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof c) && ((c) obj).C().equals(this.b)) {
            return true;
        }
        return false;
    }
}
