package net.client.by.lock;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import net.client.by.lock.c.m;
import net.client.by.lock.d.r;
import net.client.by.lock.e.b;

/* compiled from: MyApp */
public class c {
    private static c a = null;
    private HashMap b;

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 123
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:72)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.visit(BlockProcessor.java:46)
        */
    private c() {
        /*
        // Method dump skipped, instructions count: 120
        */
        throw new UnsupportedOperationException("Method not decompiled: net.client.by.lock.c.<init>():void");
    }

    public void a() {
        StringBuilder append = new StringBuilder((String) r.i().f().a()).append("\r\n");
        for (Map.Entry entry : this.b.entrySet()) {
            append.append((String) entry.getKey()).append("#").append(b.a((byte[]) entry.getValue())).append("\r\n");
        }
        m.a().a(append.toString());
    }

    public void b() {
        this.b.clear();
    }

    public static c c() {
        if (a == null) {
            if (net.client.by.lock.service.c.b == null) {
                a = new c();
                net.client.by.lock.service.c.b = a;
            } else {
                a = net.client.by.lock.service.c.b;
            }
        } else if (net.client.by.lock.service.c.b == null) {
            net.client.by.lock.service.c.b = a;
        }
        return a;
    }

    private String c(Integer num, String str) {
        String a2 = net.client.by.lock.f.m.a(String.valueOf(str) + num);
        if (a2 != null) {
            int i = 0;
            while (i < 10) {
                String a3 = net.client.by.lock.f.m.a(String.valueOf(i) + a2 + i);
                i++;
                a2 = a3;
            }
        }
        return a2;
    }

    public void a(Integer num, String str, byte[] bArr) {
        String c = c(num, str);
        if (c != null && c.length() > 0) {
            this.b.put(c, bArr);
        }
    }

    public void a(Integer num, String str) {
        String c = c(num, str);
        if (c != null && c.length() > 0) {
            this.b.remove(c);
        }
    }

    public boolean b(Integer num, String str) {
        String c = c(num, str);
        if (c == null || c.length() <= 0) {
            return false;
        }
        return this.b.containsKey(c);
    }

    public boolean b(Integer num, String str, byte[] bArr) {
        String c = c(num, str);
        byte[] bArr2 = null;
        if (c != null && c.length() > 0) {
            bArr2 = (byte[]) this.b.get(c);
        }
        if (bArr2 != null) {
            return Arrays.equals(bArr2, bArr);
        }
        return false;
    }
}
