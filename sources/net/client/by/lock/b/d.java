package net.client.by.lock.b;

import java.io.File;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.d.k;
import net.client.by.lock.f.h;
import net.client.by.lock.f.j;
import net.client.by.lock.task.FileTask;

/* compiled from: MyApp */
public abstract class d extends k implements a {
    protected int a = -1;
    protected FileTask b;
    private String c;
    private String i;
    private File j;
    private String k = null;
    private h l = new h("fileId");
    private SecretKeySpec m;
    private IvParameterSpec n;

    protected d() {
        this.f = new h("IDLE", "stateProperty");
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.d.k
    public String a() {
        return String.valueOf(this.i) + " (Dosya) (" + j.a((long) this.a) + ")";
    }

    public void g() {
        this.f.a("COMPLETED");
    }

    public void a(Throwable th) {
        this.f.a("ERROR");
        this.b = null;
        j.a("Error occured in file transfer", th);
    }

    @Override // net.client.by.lock.b.a
    public Integer f() {
        if (this.a > 0) {
            return Integer.valueOf(this.a);
        }
        if (this.j == null || !this.j.exists()) {
            return 0;
        }
        return Integer.valueOf((int) this.j.length());
    }

    public void a(int i2) {
        this.a = i2;
    }

    public void a(String str) {
        this.l.a(str);
    }

    @Override // net.client.by.lock.b.a
    public String b() {
        return (String) this.l.a();
    }

    public h h() {
        return this.l;
    }

    public void a(IvParameterSpec ivParameterSpec) {
        this.n = ivParameterSpec;
    }

    @Override // net.client.by.lock.b.a
    public IvParameterSpec d() {
        return this.n;
    }

    @Override // net.client.by.lock.b.a
    public String e() {
        if (this.i != null || this.j == null) {
            return this.i;
        }
        return this.j.getName();
    }

    public void b(String str) {
        this.i = str;
    }

    @Override // net.client.by.lock.b.a
    public SecretKeySpec c() {
        return this.m;
    }

    public void a(SecretKeySpec secretKeySpec) {
        this.m = secretKeySpec;
    }

    public String i() {
        return this.c;
    }

    public void c(String str) {
        this.c = str;
    }

    public void a(File file) {
        this.j = file;
    }

    public File j() {
        return this.j;
    }

    public void k() {
        if (this.b != null) {
            this.b.cancelTask(true);
            this.f.a("IDLE");
            this.b = null;
        }
    }
}
