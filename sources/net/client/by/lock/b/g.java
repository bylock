package net.client.by.lock.b;

import java.io.UnsupportedEncodingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.a.a;
import net.client.by.lock.gui.a.h;

/* compiled from: MyApp */
public class g extends d {
    private byte[] c;
    private byte[] i;
    private byte[] j;
    private byte[] k;

    public g() {
        e("IDLE");
    }

    public void l() {
        this.b = n.a().a(this);
        this.f.a("IN PROGRESS");
    }

    public byte[] m() {
        return this.c;
    }

    public void a(byte[] bArr) {
        this.c = bArr;
    }

    public byte[] n() {
        return this.i;
    }

    public void b(byte[] bArr) {
        this.i = bArr;
    }

    public byte[] o() {
        return this.j;
    }

    public void c(byte[] bArr) {
        this.j = bArr;
    }

    public byte[] p() {
        return this.k;
    }

    public void d(byte[] bArr) {
        this.k = bArr;
    }

    public boolean q() {
        try {
            byte[] b = r.i().p().b(this.c);
            if (b == null) {
                return false;
            }
            b(new String(b, "UTF-8"));
            a(new SecretKeySpec(r.i().p().b(this.i), "AES"));
            a(new IvParameterSpec(r.i().p().b(this.j)));
            if (e() == null || c() == null || d() == null) {
                return false;
            }
            return true;
        } catch (UnsupportedEncodingException e) {
            j.a("", e);
        }
    }

    @Override // net.client.by.lock.d.k
    public boolean b_() {
        return false;
    }

    @Override // net.client.by.lock.d.k
    public void z() {
        n.a().d(i());
        k.b().a(i());
    }

    @Override // net.client.by.lock.d.k
    public a A() {
        if (this.h == null) {
            this.h = new h(this);
        }
        return this.h;
    }
}
