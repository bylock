package net.client.by.lock.b;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.a.a;
import net.client.by.lock.gui.a.m;

/* compiled from: MyApp */
public class h extends d {
    ArrayList c;

    public h(File file) {
        try {
            KeyGenerator instance = KeyGenerator.getInstance("AES");
            instance.init(128, r.i().a());
            a(new SecretKeySpec(instance.generateKey().getEncoded(), "AES"));
            a(new IvParameterSpec(instance.generateKey().getEncoded()));
            a(file);
            b(file.getName());
            this.c = new ArrayList();
        } catch (NoSuchAlgorithmException e) {
            j.a("Error in generating key for file transfer", e);
        }
    }

    public h(a aVar) {
        b(aVar.e());
        a(aVar.c());
        a(aVar.d());
        a(aVar.f().intValue());
        a(aVar.b());
        this.f.a("IDLE");
        this.c = new ArrayList();
    }

    public ArrayList l() {
        return this.c;
    }

    public void a(ArrayList arrayList) {
        this.c = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            c a = ((c) it.next()).a((d) this);
            if (a != null) {
                this.c.add(a);
            }
        }
    }

    public void m() {
        this.b = n.a().a(this);
        this.f.a("IN PROGRESS");
    }

    @Override // net.client.by.lock.d.k
    public boolean b_() {
        return true;
    }

    @Override // net.client.by.lock.d.k
    public void z() {
    }

    @Override // net.client.by.lock.d.k
    public a A() {
        if (this.h == null) {
            this.h = new m(this);
        }
        return this.h;
    }
}
