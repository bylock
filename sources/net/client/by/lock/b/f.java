package net.client.by.lock.b;

import java.io.FileInputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Iterator;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import net.client.by.lock.e.b;
import net.client.by.lock.e.c;
import net.client.by.lock.f.j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: MyApp */
public class f extends e {
    Document a = c.d();
    byte[] b;
    int c;
    int d;
    int e;
    byte[] f;
    int l;
    byte[] m;
    int n;
    int o = 0;
    Cipher p;
    private boolean q;

    public f(h hVar) {
        boolean z = true;
        this.q = hVar.b() == null ? false : z;
        if (!this.q) {
            this.j = new FileInputStream(hVar.j());
            this.i = false;
            try {
                this.p = Cipher.getInstance("AES/CBC/PKCS5Padding");
                this.p.init(1, hVar.c(), hVar.d());
                this.m = new byte[12352];
                this.n = 0;
            } catch (InvalidAlgorithmParameterException e2) {
                j.a("Error in initializing FileUploadStream.", e2);
                this.p = null;
            } catch (InvalidKeyException e3) {
                j.a("Error in initializing FileUploadStream.", e3);
                this.p = null;
            } catch (NoSuchAlgorithmException e4) {
                j.a("Error in initializing FileUploadStream.", e4);
                this.p = null;
            } catch (NoSuchPaddingException e5) {
                j.a("Error in initializing FileUploadStream.", e5);
                this.p = null;
            }
            Element createElement = this.a.createElement("fileContent");
            createElement.setTextContent("MY SPECIAL STRING");
            this.a.getDocumentElement().appendChild(createElement);
            this.d = ((int) Math.ceil(((double) ((int) ((hVar.j().length() + 16) - (hVar.j().length() % 16)))) / 3.0d)) * 4;
            this.l = 0;
            this.f = null;
        } else if (hVar.l() == null) {
            throw new IllegalArgumentException("No recipient and no file content!");
        } else if (hVar.l().isEmpty()) {
            throw new IllegalArgumentException("No recipient and no file content!");
        } else {
            Element createElement2 = this.a.createElement("fileId");
            createElement2.setTextContent(hVar.b());
            this.a.getDocumentElement().appendChild(createElement2);
        }
        Iterator it = hVar.l().iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            Element createElement3 = this.a.createElement("recipient");
            Element createElement4 = this.a.createElement("userId");
            createElement4.setTextContent(new StringBuilder().append(cVar.e()).toString());
            Element createElement5 = this.a.createElement("filename");
            createElement5.setTextContent(b.a(cVar.a()));
            Element createElement6 = this.a.createElement("key");
            createElement6.setTextContent(b.a(cVar.b()));
            Element createElement7 = this.a.createElement("iv");
            createElement7.setTextContent(b.a(cVar.c()));
            Element createElement8 = this.a.createElement("signature");
            createElement8.setTextContent(b.a(cVar.d()));
            createElement3.appendChild(createElement4);
            createElement3.appendChild(createElement5);
            createElement3.appendChild(createElement6);
            createElement3.appendChild(createElement7);
            createElement3.appendChild(createElement8);
            this.a.getDocumentElement().appendChild(createElement3);
        }
        StringWriter stringWriter = new StringWriter();
        try {
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(this.a), new StreamResult(stringWriter));
            try {
                this.b = stringWriter.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException e6) {
                j.a("", e6);
            }
            if (!this.q) {
                this.c = j.a(this.b, "<fileContent>MY SPECIAL STRING</fileContent>".getBytes()) + 13;
                a((this.b.length - "MY SPECIAL STRING".length()) + this.d);
            } else {
                a(this.b.length);
            }
            this.e = 0;
        } catch (TransformerException e7) {
            this.b = new byte[0];
            this.c = 0;
            this.k = 0;
            this.e = 0;
            j.a("Error in creating xml for sending file", e7);
        }
    }

    @Override // java.io.InputStream
    public int read() {
        if (this.g) {
            return -1;
        }
        int c2 = c();
        if (c2 != -1) {
            this.h.a();
        }
        return c2;
    }

    public int c() {
        byte b2 = -1;
        if (!this.g) {
            if (this.e >= this.k) {
                this.g = true;
            } else {
                if (this.q) {
                    b2 = this.b[this.e];
                } else if (this.e < this.c) {
                    b2 = this.b[this.e];
                } else if (this.e < this.c + this.d) {
                    if (this.f != null && this.l != this.f.length) {
                        b2 = this.f[this.l];
                        this.l++;
                    } else if (d()) {
                        b2 = this.f[0];
                        this.l = 1;
                    } else {
                        this.e = this.c + this.d;
                        b2 = this.b[(this.e - this.d) + "MY SPECIAL STRING".length()];
                    }
                } else if (this.e < this.k) {
                    b2 = this.b[(this.e - this.d) + "MY SPECIAL STRING".length()];
                }
                this.e++;
            }
        }
        return b2;
    }

    private boolean d() {
        if (this.i) {
            return false;
        }
        if (this.p == null) {
            b();
            return false;
        }
        byte[] bArr = new byte[12288];
        int read = this.j.read(bArr);
        this.h.a();
        if (read == -1) {
            b();
            try {
                this.n += this.p.doFinal(this.m, this.n);
                this.f = b.a(Arrays.copyOf(this.m, this.n)).getBytes();
                return this.f.length > 0;
            } catch (IllegalBlockSizeException e2) {
                j.a("Error in encrypting file content for sending file", e2);
                return false;
            } catch (ShortBufferException e3) {
                j.a("Error in encrypting file content for sending file", e3);
                return false;
            } catch (BadPaddingException e4) {
                j.a("Error in encrypting file content for sending file", e4);
                return false;
            }
        } else {
            this.o += read;
            try {
                this.n = this.p.update(bArr, 0, read, this.m, this.n) + this.n;
                int i = this.n % 3;
                byte[] copyOf = Arrays.copyOf(this.m, this.n - i);
                for (int i2 = 0; i2 < i; i2++) {
                    this.m[i2] = this.m[(this.n - i) + i2];
                }
                this.n = i;
                this.f = b.a(copyOf).getBytes();
                if (this.f.length > 0) {
                    return true;
                }
                return d();
            } catch (ShortBufferException e5) {
                j.a("Error in encrypting file content for sending file", e5);
                return false;
            }
        }
    }
}
