package net.client.by.lock.b;

import java.util.HashMap;
import java.util.Iterator;
import net.client.by.lock.f.g;

/* compiled from: MyApp */
public class k {
    private static k a = new k();
    private g b = new g();
    private HashMap c = new HashMap();

    private k() {
    }

    public synchronized void a(a aVar) {
        if (this.c.get(aVar.b()) == null) {
            this.b.add(aVar);
            this.c.put(aVar.b(), 1);
        } else {
            this.c.put(aVar.b(), Integer.valueOf(((Integer) this.c.get(aVar.b())).intValue() + 1));
        }
    }

    public synchronized void a(String str) {
        a aVar;
        Integer num = (Integer) this.c.get(str);
        if (num != null) {
            if (num.intValue() == 1) {
                this.c.remove(str);
                Iterator it = this.b.iterator();
                while (true) {
                    if (it.hasNext()) {
                        aVar = (a) it.next();
                        if (aVar.b().equals(str)) {
                            break;
                        }
                    } else {
                        aVar = null;
                        break;
                    }
                }
                if (aVar != null) {
                    this.b.remove(aVar);
                }
            } else {
                this.c.put(str, Integer.valueOf(num.intValue() - 1));
            }
        }
    }

    public synchronized g a() {
        return this.b;
    }

    public static k b() {
        return a;
    }

    public a b(String str) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            a aVar = (a) it.next();
            if (aVar.b().equals(str)) {
                return aVar;
            }
        }
        return null;
    }
}
