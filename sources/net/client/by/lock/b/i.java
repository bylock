package net.client.by.lock.b;

import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class i implements m {
    private int a;
    private int b = 0;
    private int c = 0;
    private String d = "";
    private String e = "";
    private String f = "";
    private String g = "";
    private long h = System.currentTimeMillis();
    private long i = this.h;
    private double j = 0.0d;
    private Object k = new Object();
    private j l;

    public void a() {
        synchronized (this.k) {
            this.c++;
        }
        b();
    }

    public void b() {
        if ((((double) (this.c - this.b)) * 100.0d) / ((double) this.a) >= 1.0d || this.c == this.a) {
            synchronized (this.k) {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis <= this.h || this.c <= this.b) {
                    this.d = "";
                    this.f = "";
                } else {
                    double d2 = ((double) (currentTimeMillis - this.i)) / 1000.0d;
                    this.g = j.a((int) Math.round(d2));
                    double d3 = ((double) (this.c - this.b)) / (((double) (currentTimeMillis - this.h)) / 1000.0d);
                    this.d = j.a(d3);
                    this.f = j.a((int) Math.round(((double) (this.a - this.c)) / d3));
                    this.e = j.a(((double) this.c) / d2);
                    this.j = ((double) this.c) / ((double) this.a);
                    this.l.updateProgress((int) (this.j * 100.0d));
                    this.b = this.c;
                    this.h = currentTimeMillis;
                }
            }
        }
    }

    public void a(int i2) {
        this.a = i2;
    }

    @Override // net.client.by.lock.b.m
    public void b(int i2) {
        if (i2 != -1) {
            a();
        }
    }

    public void a(j jVar) {
        this.l = jVar;
    }
}
