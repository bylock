package net.client.by.lock.b;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class b extends e {
    byte[] a;
    int b;
    int c;
    int d = 15;
    Cipher e;
    net.client.by.lock.f.b f;
    private boolean l;

    public b(g gVar) {
        this.f = new net.client.by.lock.f.b(new FileOutputStream(gVar.j()));
        this.l = false;
        try {
            this.e = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.e.init(2, gVar.c(), gVar.d());
        } catch (InvalidAlgorithmParameterException e2) {
            j.a("Error in initializing FileDownloadInputStream", e2);
            this.e = null;
        } catch (InvalidKeyException e3) {
            j.a("Error in initializing FileDownloadInputStream", e3);
            this.e = null;
        } catch (NoSuchAlgorithmException e4) {
            j.a("Error in initializing FileDownloadInputStream", e4);
            this.e = null;
        } catch (NoSuchPaddingException e5) {
            j.a("Error in initializing FileDownloadInputStream", e5);
            this.e = null;
        }
        this.c = 0;
        this.b = 0;
    }

    @Override // net.client.by.lock.b.e
    public void a(InputStream inputStream) {
        this.j = new l(inputStream, this.h);
        this.i = false;
    }

    @Override // java.io.InputStream
    public int read() {
        if (this.g) {
            return -1;
        }
        if (this.d == 15) {
            if (this.c - this.b < "<fileContent>".length() && !e()) {
                close();
                return -1;
            } else if (Arrays.equals(Arrays.copyOfRange(this.a, this.b, this.b + "<fileContent>".getBytes().length), "<fileContent>".getBytes())) {
                if (this.e != null) {
                    try {
                        c();
                    } catch (IllegalBlockSizeException e2) {
                        j.a("Error in handling file content", e2);
                    } catch (BadPaddingException e3) {
                        j.a("Error in handling file content", e3);
                    }
                }
                this.d = 35;
                this.b++;
                return this.a[this.b - 1];
            } else {
                this.b++;
                return this.a[this.b - 1];
            }
        } else if (this.b < this.c) {
            this.b++;
            return this.a[this.b - 1];
        } else if (this.i) {
            close();
            return -1;
        } else if (!e()) {
            close();
            return -1;
        } else {
            this.b = 1;
            return this.a[0];
        }
    }

    private void c() {
        boolean z = false;
        this.b += "<fileContent>".getBytes().length;
        while (!z) {
            if (this.b == this.c && !e()) {
                close();
                throw new IOException("Error in filling buffer.");
            } else if (this.a[this.b] == 60) {
                if (this.c - this.b > "</fileContent>".getBytes().length || e()) {
                    this.b += "</fileContent>".getBytes().length;
                    z = true;
                    try {
                        this.f.a(this.e.doFinal());
                        d();
                    } catch (IllegalBlockSizeException e2) {
                        close();
                        throw e2;
                    }
                } else {
                    close();
                    throw new IOException("Error in filling buffer.");
                }
            } else if (this.c - this.b >= 4 || e()) {
                try {
                    this.f.a(this.e.update(net.client.by.lock.e.b.a(new String(this.a, this.b, 4))));
                    this.b += 4;
                } catch (IOException e3) {
                    close();
                    throw e3;
                }
            } else {
                close();
                throw new IOException("Error in filling buffer.");
            }
        }
    }

    private void d() {
        if (!this.l) {
            if (this.f != null) {
                try {
                    this.f.a();
                } catch (IOException e2) {
                    j.a("", e2);
                }
                this.f = null;
            }
            this.l = true;
        }
    }

    private boolean e() {
        byte[] bArr = new byte[0];
        if (this.b < this.c) {
            bArr = Arrays.copyOfRange(this.a, this.b, this.c);
        }
        this.a = new byte[(bArr.length + 4096)];
        System.arraycopy(bArr, 0, this.a, 0, bArr.length);
        try {
            int read = this.j.read(this.a, bArr.length, 4096);
            if (read == -1) {
                this.c = bArr.length;
                return false;
            }
            this.c = bArr.length + read;
            this.b = 0;
            return true;
        } catch (IOException e2) {
            j.a("Error in fillBuffer", e2);
            return false;
        }
    }

    @Override // net.client.by.lock.b.e, java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
    public void close() {
        super.close();
        this.g = true;
        b();
        d();
    }
}
