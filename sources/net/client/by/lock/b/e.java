package net.client.by.lock.b;

import java.io.IOException;
import java.io.InputStream;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public abstract class e extends InputStream {
    protected boolean g = false;
    protected i h = new i();
    protected boolean i = false;
    protected InputStream j;
    protected int k;

    public i a() {
        return this.h;
    }

    public void a(int i2) {
        this.k = i2;
        this.h.a(i2);
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (!this.i) {
            this.i = true;
            if (this.j != null) {
                try {
                    this.j.close();
                } catch (IOException e) {
                    j.a("FileTransferInputStream", e);
                }
                this.j = null;
            }
        }
    }

    public void a(InputStream inputStream) {
        this.j = inputStream;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
    public void close() {
        super.close();
        this.g = true;
        b();
    }
}
