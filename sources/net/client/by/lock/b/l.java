package net.client.by.lock.b;

import java.io.InputStream;

/* compiled from: MyApp */
public class l extends InputStream {
    InputStream a;
    m b;

    public l(InputStream inputStream, m mVar) {
        this.a = inputStream;
        this.b = mVar;
    }

    @Override // java.io.InputStream
    public int read() {
        int read = this.a.read();
        this.b.b(read);
        return read;
    }
}
