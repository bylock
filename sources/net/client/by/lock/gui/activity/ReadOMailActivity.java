package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewAnimator;
import net.client.by.lock.R;
import net.client.by.lock.c.i;
import net.client.by.lock.d.c;
import net.client.by.lock.d.m;
import net.client.by.lock.d.n;
import net.client.by.lock.gui.b.ai;
import net.client.by.lock.gui.b.ak;
import net.client.by.lock.gui.b.an;

/* compiled from: MyApp */
public class ReadOMailActivity extends q {
    private View A;
    private ViewAnimator B;
    private c o;
    private n p;
    private TextView r;
    private TextView s;
    private ListView t;
    private ListView u;
    private ai v;
    private ak w;
    private Button x;
    private Button y;
    private Button z;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        int i;
        String string;
        super.onCreate(bundle);
        setContentView(R.layout.activity_omail_read);
        if (bundle != null) {
            i = bundle.getInt("id");
            string = bundle.getString("mailId");
        } else if (getIntent() != null) {
            i = getIntent().getExtras().getInt("id");
            string = getIntent().getExtras().getString("mailId");
        } else {
            finish();
            return;
        }
        this.o = i.c().a(Integer.valueOf(i));
        m g = this.o.g(string);
        if (g == null || !(g instanceof n)) {
            finish();
        } else {
            this.p = (n) g;
        }
        this.r = (TextView) findViewById(R.id.mail_body);
        this.u = (ListView) findViewById(R.id.mail_to);
        this.s = (TextView) findViewById(R.id.mail_subject);
        this.t = (ListView) findViewById(R.id.attachments);
        this.x = (Button) findViewById(R.id.button1);
        this.y = (Button) findViewById(R.id.button2);
        this.z = (Button) findViewById(R.id.button3);
        this.B = (ViewAnimator) findViewById(R.id.viewAnimator1);
        this.A = findViewById(R.id.linearLayout3);
        this.v = new ai(this);
        this.t.setAdapter((ListAdapter) this.v);
        this.w = new ak(this);
        this.u.setAdapter((ListAdapter) this.w);
        this.s.setText(this.p.g());
        this.r.setText(Html.fromHtml(this.p.f()));
        this.v.a(this.p.l());
        this.w.a(this.p.b());
        f().a("");
        f().c(true);
        this.x.setOnClickListener(new bh(this));
        this.y.setOnClickListener(new bi(this));
        this.z.setOnClickListener(new bj(this));
        b(0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_mail_read, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                setResult(0);
                finish();
                return true;
            case R.id.menu_forward_mail /*{ENCODED_INT: 2131165402}*/:
                if (this.p.f() != null) {
                    Intent intent = new Intent();
                    intent.putExtra("forward", true);
                    intent.putExtra("id", this.o.K());
                    intent.putExtra("mailId", this.p.i());
                    setResult(-1, intent);
                    finish();
                    return false;
                }
                an.b(this, "Body has not loaded yet.");
                return true;
            case R.id.menu_delete_mail /*{ENCODED_INT: 2131165404}*/:
                this.o.a(this.p);
                net.client.by.lock.c.n.a().f(this.p.i());
                finish();
                return false;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("id", this.o.K());
        bundle.putString("mailId", this.p.i());
    }

    public void b(int i) {
        int color = getResources().getColor(R.color.aaa_blue_button_background);
        int color2 = getResources().getColor(R.color.White);
        int color3 = getResources().getColor(R.color.aaa_blue_text_over_white_button);
        this.B.setDisplayedChild(i);
        switch (i) {
            case 0:
                this.x.setBackgroundColor(color);
                this.y.setBackgroundColor(color2);
                this.z.setBackgroundColor(color2);
                this.x.setTextColor(color2);
                this.y.setTextColor(color3);
                this.z.setTextColor(color3);
                this.x.setEnabled(false);
                this.y.setEnabled(true);
                this.z.setEnabled(true);
                break;
            case 1:
                this.x.setBackgroundColor(color2);
                this.y.setBackgroundColor(color);
                this.z.setBackgroundColor(color2);
                this.x.setTextColor(color3);
                this.y.setTextColor(color2);
                this.z.setTextColor(color3);
                this.x.setEnabled(true);
                this.y.setEnabled(false);
                this.z.setEnabled(true);
                break;
            case 2:
                this.x.setBackgroundColor(color2);
                this.y.setBackgroundColor(color2);
                this.z.setBackgroundColor(color);
                this.x.setTextColor(color3);
                this.y.setTextColor(color3);
                this.z.setTextColor(color2);
                this.x.setEnabled(true);
                this.y.setEnabled(true);
                this.z.setEnabled(false);
                break;
            default:
                throw new UnsupportedOperationException("Unknown type");
        }
        if (this.w.getCount() == 0) {
            this.z.setVisibility(8);
        } else {
            this.z.setVisibility(0);
        }
        if (this.v.getCount() == 0) {
            this.y.setVisibility(8);
        } else {
            this.y.setVisibility(0);
        }
        if (this.w.getCount() == 0 && this.v.getCount() == 0) {
            this.A.setVisibility(8);
        } else {
            this.A.setVisibility(0);
        }
    }
}
