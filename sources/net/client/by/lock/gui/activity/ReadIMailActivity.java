package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewAnimator;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.R;
import net.client.by.lock.c.i;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;
import net.client.by.lock.d.l;
import net.client.by.lock.d.m;
import net.client.by.lock.gui.b.ai;
import net.client.by.lock.gui.b.am;
import net.client.by.lock.gui.b.an;

/* compiled from: MyApp */
public class ReadIMailActivity extends q {
    private View A;
    private ViewAnimator B;
    private am C;
    public TextView o;
    public TextView p;
    public TextView r;
    public ListView s;
    public ListView t;
    public ai u;
    private c v;
    private m w;
    private Button x;
    private Button y;
    private Button z;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        int i;
        String string;
        super.onCreate(bundle);
        setContentView(R.layout.activity_imail_read);
        if (bundle != null) {
            i = bundle.getInt("id");
            string = bundle.getString("mailId");
        } else if (getIntent() != null) {
            i = getIntent().getExtras().getInt("id");
            string = getIntent().getExtras().getString("mailId");
        } else {
            finish();
            return;
        }
        this.v = i.c().a(Integer.valueOf(i));
        this.w = this.v.g(string);
        if (this.w == null) {
            finish();
        }
        this.o = (TextView) findViewById(R.id.mail_body);
        this.p = (TextView) findViewById(R.id.mail_from);
        this.r = (TextView) findViewById(R.id.mail_subject);
        this.t = (ListView) findViewById(R.id.mail_to);
        this.s = (ListView) findViewById(R.id.attachments);
        this.x = (Button) findViewById(R.id.button1);
        this.y = (Button) findViewById(R.id.button2);
        this.z = (Button) findViewById(R.id.button3);
        this.B = (ViewAnimator) findViewById(R.id.viewAnimator1);
        this.A = findViewById(R.id.linearLayout3);
        this.u = new ai(this);
        this.C = new am(this);
        this.s.setAdapter((ListAdapter) this.u);
        this.t.setAdapter((ListAdapter) this.C);
        this.p.setText(this.w.E().G());
        i();
        ((TextView) findViewById(R.id.textView1)).setTypeface(null, 1);
        ((TextView) findViewById(R.id.textView2)).setTypeface(null, 1);
        this.r.setText(this.w.g());
        if (((String) this.w.F().a()).equals("COMPLETED")) {
            new bd(this).execute(new Void[0]);
            this.u.a(this.w.l());
        } else {
            ((l) this.w).c();
        }
        f().a("");
        f().c(true);
        this.x.setOnClickListener(new be(this));
        this.y.setOnClickListener(new bf(this));
        this.z.setOnClickListener(new bg(this));
        b(0);
    }

    public void i() {
        ArrayList arrayList = new ArrayList();
        Iterator it = this.w.n().iterator();
        while (it.hasNext()) {
            c a = i.c().a(Integer.valueOf(((Integer) it.next()).intValue()));
            if (a != null) {
                arrayList.add(a.G());
            }
        }
        if (arrayList.size() > 0) {
            b(2);
            this.C.a(arrayList);
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onStop() {
        finish();
        super.onStop();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_mail_read, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            case R.id.menu_forward_mail /*{ENCODED_INT: 2131165402}*/:
                if (this.w.f() != null) {
                    Intent intent = new Intent();
                    intent.putExtra("forward", true);
                    intent.putExtra("id", this.v.K());
                    intent.putExtra("mailId", this.w.i());
                    setResult(-1, intent);
                    finish();
                    return false;
                }
                an.b(this, "Body has not loaded yet.");
                return true;
            case R.id.menu_reply_mail /*{ENCODED_INT: 2131165403}*/:
                if (this.w.f() != null) {
                    Intent intent2 = new Intent();
                    intent2.putExtra("reply", true);
                    intent2.putExtra("id", this.v.K());
                    intent2.putExtra("mailId", this.w.i());
                    setResult(-1, intent2);
                    finish();
                    return false;
                }
                an.b(this, "Body has not loaded yet.");
                return true;
            case R.id.menu_delete_mail /*{ENCODED_INT: 2131165404}*/:
                this.v.a(this.w);
                n.a().f(this.w.i());
                finish();
                return false;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("id", this.v.K());
        bundle.putString("mailId", this.w.i());
    }

    public void b(int i) {
        int color = getResources().getColor(R.color.aaa_blue_button_background);
        int color2 = getResources().getColor(R.color.White);
        int color3 = getResources().getColor(R.color.aaa_blue_text_over_white_button);
        this.B.setDisplayedChild(i);
        switch (i) {
            case 0:
                this.x.setBackgroundColor(color);
                this.y.setBackgroundColor(color2);
                this.z.setBackgroundColor(color2);
                this.x.setTextColor(color2);
                this.y.setTextColor(color3);
                this.z.setTextColor(color3);
                this.x.setEnabled(false);
                this.y.setEnabled(true);
                this.z.setEnabled(true);
                break;
            case 1:
                this.x.setBackgroundColor(color2);
                this.y.setBackgroundColor(color);
                this.z.setBackgroundColor(color2);
                this.x.setTextColor(color3);
                this.y.setTextColor(color2);
                this.z.setTextColor(color3);
                this.x.setEnabled(true);
                this.y.setEnabled(false);
                this.z.setEnabled(true);
                break;
            case 2:
                this.x.setBackgroundColor(color2);
                this.y.setBackgroundColor(color2);
                this.z.setBackgroundColor(color);
                this.x.setTextColor(color3);
                this.y.setTextColor(color3);
                this.z.setTextColor(color2);
                this.x.setEnabled(true);
                this.y.setEnabled(true);
                this.z.setEnabled(false);
                break;
            default:
                throw new UnsupportedOperationException("Unknown type");
        }
        if (this.C.getCount() == 0) {
            this.z.setVisibility(8);
        } else {
            this.z.setVisibility(0);
        }
        if (this.u.getCount() == 0) {
            this.y.setVisibility(8);
        } else {
            this.y.setVisibility(0);
        }
        if (this.C.getCount() == 0 && this.u.getCount() == 0) {
            this.A.setVisibility(8);
        } else {
            this.A.setVisibility(0);
        }
    }
}
