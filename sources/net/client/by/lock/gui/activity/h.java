package net.client.by.lock.gui.activity;

import android.view.View;
import android.widget.Chronometer;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.a.c;
import net.client.by.lock.c.b;

/* compiled from: MyApp */
class h implements View.OnClickListener {
    private c a;
    private Chronometer b;
    private CallActivity c;
    private boolean d = false;

    public h(c cVar, Chronometer chronometer, CallActivity callActivity) {
        this.a = cVar;
        this.b = chronometer;
        this.c = callActivity;
    }

    public void onClick(View view) {
        if (!this.d) {
            this.d = true;
            b.a.set(true);
            ((TextView) CallActivity.b(this.c).getCurrentView().findViewById(R.id.call_status)).setText(R.string.call_ended);
            this.b.stop();
            view.postDelayed(new i(this, this.b.getText().toString()), 80);
        }
    }
}
