package net.client.by.lock.gui.activity;

import android.view.View;

/* compiled from: MyApp */
class s implements View.OnClickListener {
    final /* synthetic */ LoginActivity a;

    s(LoginActivity loginActivity) {
        this.a = loginActivity;
    }

    public void onClick(View view) {
        this.a.f.setEnabled(false);
        LoginActivity.a(this.a).hideSoftInputFromWindow(this.a.f.getWindowToken(), 0);
        this.a.c();
    }
}
