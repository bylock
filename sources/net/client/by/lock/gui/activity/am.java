package net.client.by.lock.gui.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.R;
import net.client.by.lock.a;
import net.client.by.lock.c.i;
import net.client.by.lock.c.m;
import net.client.by.lock.c.n;
import net.client.by.lock.d.g;
import net.client.by.lock.d.s;
import net.client.by.lock.gui.b.ac;

/* compiled from: MyApp */
public class am extends Fragment {
    public static ExpandableListView P;
    private static ViewGroup Q;
    private static ContextMenu R;
    private int S;

    @Override // android.support.v4.app.Fragment
    public void c(Bundle bundle) {
        ad.b = b();
        super.c(bundle);
        b(true);
        a((LayoutInflater) ad.b.getSystemService("layout_inflater"));
    }

    @Override // android.support.v4.app.Fragment
    public void j() {
        if (a.e != null) {
            int groupCount = a.e.getGroupCount();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < groupCount; i++) {
                if (P.isGroupExpanded(i)) {
                    arrayList.add(Integer.valueOf(i));
                }
            }
            if (m.h()) {
                m.a().a(arrayList);
                m.a().f(b());
            }
        }
        super.j();
    }

    @Override // android.support.v4.app.Fragment
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ad.b = b();
        if (Q == null) {
            a(layoutInflater);
        } else if (Q.getParent() != null && (Q.getParent() instanceof ViewGroup)) {
            ((ViewGroup) Q.getParent()).removeView(Q);
        }
        return Q;
    }

    @Override // android.support.v4.app.Fragment
    public void a(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.action_home, menu);
        super.a(menu, menuInflater);
    }

    @Override // android.support.v4.app.Fragment
    public boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_settings /*{ENCODED_INT: 2131165401}*/:
                a(new Intent(ad.b, SettingsActivity.class));
                return true;
            default:
                return super.a(menuItem);
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        MenuInflater menuInflater = b().getMenuInflater();
        long j = ((ExpandableListView.ExpandableListContextMenuInfo) contextMenuInfo).packedPosition;
        int packedPositionGroup = ExpandableListView.getPackedPositionGroup(j);
        if (ExpandableListView.getPackedPositionChild(j) == -1) {
            menuInflater.inflate(R.menu.activity_group, contextMenu);
            g gVar = (g) ((s) i.c().a().e().get(packedPositionGroup)).d();
            R = contextMenu.setHeaderTitle((CharSequence) gVar.b().a());
            if (gVar.a() == 0) {
                R.findItem(R.id.menu_delete_group).setVisible(false);
                R.findItem(R.id.menu_rename_group).setVisible(false);
            }
        }
    }

    @Override // android.support.v4.app.Fragment
    public boolean b(MenuItem menuItem) {
        this.S = ExpandableListView.getPackedPositionGroup(((ExpandableListView.ExpandableListContextMenuInfo) menuItem.getMenuInfo()).packedPosition);
        g gVar = (g) ((s) i.c().a().e().get(this.S)).d();
        switch (menuItem.getItemId()) {
            case R.id.menu_rename_group /*{ENCODED_INT: 2131165412}*/:
                a(gVar);
                break;
            case R.id.menu_delete_group /*{ENCODED_INT: 2131165413}*/:
                n.a().c(Integer.valueOf(gVar.a()));
                break;
            default:
                return super.b(menuItem);
        }
        return super.b(menuItem);
    }

    private void a(LayoutInflater layoutInflater) {
        Q = (ViewGroup) layoutInflater.inflate(R.layout.fragment_friendlist, (ViewGroup) null);
        P = (ExpandableListView) Q.findViewById(R.id.listView1);
        a.e = new ac(ad.b);
        P.setAdapter(a.e);
        P.setOnItemLongClickListener(new an(this));
        P.setOnChildClickListener(new ao(this));
        a(P);
        ArrayList c = m.a().c();
        int groupCount = a.e.getGroupCount();
        Iterator it = c.iterator();
        while (it.hasNext()) {
            int intValue = ((Integer) it.next()).intValue();
            if (groupCount > intValue) {
                P.expandGroup(intValue);
            }
        }
    }

    private void a(g gVar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(b());
        EditText editText = new EditText(b());
        editText.setText((CharSequence) gVar.b().a());
        editText.setInputType(1);
        builder.setView(editText).setPositiveButton(R.string.rename_group, new ap(this, gVar, editText)).setNegativeButton(R.string.cancel, new aq(this)).show();
        editText.setSelection(0, editText.getText().length());
    }

    @Override // android.support.v4.app.Fragment
    public void l() {
        super.l();
        P = null;
        Q = null;
        R = null;
    }
}
