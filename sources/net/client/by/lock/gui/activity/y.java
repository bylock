package net.client.by.lock.gui.activity;

import android.os.Handler;
import android.support.v4.view.cb;

/* compiled from: MyApp */
class y extends cb {
    final /* synthetic */ MainActivity a;

    y(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    @Override // android.support.v4.view.cb, android.support.v4.view.bx
    public void a(int i) {
        if (MainActivity.u == null || MainActivity.u.length() == 0) {
            this.a.f().a(this.a.s[i]);
        }
        if (i == 1 || i == 4) {
            this.a.getWindow().setSoftInputMode(16);
        } else {
            this.a.getWindow().setSoftInputMode(32);
        }
        for (int i2 = 0; i2 < 5; i2++) {
            this.a.r[i2].setEnabled(i2 != i);
        }
        new Handler().postDelayed(new z(this), 500);
    }
}
