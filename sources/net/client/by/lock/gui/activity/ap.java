package net.client.by.lock.gui.activity;

import android.content.DialogInterface;
import android.widget.EditText;
import net.client.by.lock.c.n;
import net.client.by.lock.d.g;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ap implements DialogInterface.OnClickListener {
    final /* synthetic */ am a;
    private final /* synthetic */ g b;
    private final /* synthetic */ EditText c;

    ap(am amVar, g gVar, EditText editText) {
        this.a = amVar;
        this.b = gVar;
        this.c = editText;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.b.a(this.c.getText().toString());
        n.a().a(this.b);
    }
}
