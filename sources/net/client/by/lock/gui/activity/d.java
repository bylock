package net.client.by.lock.gui.activity;

import android.view.View;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.c.b;

/* compiled from: MyApp */
class d implements View.OnClickListener {
    final /* synthetic */ CallActivity a;

    d(CallActivity callActivity) {
        this.a = callActivity;
    }

    public void onClick(View view) {
        b.a.set(true);
        ((TextView) CallActivity.b(this.a).getCurrentView().findViewById(R.id.call_status)).setText(R.string.call_cancelled);
        view.postDelayed(new e(this), 80);
    }
}
