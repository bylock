package net.client.by.lock.gui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import net.client.by.lock.R;

/* compiled from: MyApp */
public class ae extends Fragment {
    public static TextView P;
    public static TextView Q;
    public static Button R;
    private static ViewGroup T;
    public Dialog S;

    @Override // android.support.v4.app.Fragment
    public void c(Bundle bundle) {
        super.c(bundle);
        ad.a((Activity) b());
        a((LayoutInflater) ad.c().getSystemService("layout_inflater"));
    }

    @Override // android.support.v4.app.Fragment
    public void j() {
        super.j();
    }

    @Override // android.support.v4.app.Fragment
    public void i() {
        super.i();
    }

    @Override // android.support.v4.app.Fragment
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ad.a((Activity) b());
        if (T == null) {
            a(layoutInflater);
        } else if (T.getParent() != null && (T.getParent() instanceof ViewGroup)) {
            ((ViewGroup) T.getParent()).removeView(T);
        }
        return T;
    }

    private void a(LayoutInflater layoutInflater) {
        T = (ViewGroup) layoutInflater.inflate(R.layout.fragment_addfriend, (ViewGroup) null);
        P = (TextView) T.findViewById(R.id.editText1);
        Q = (TextView) T.findViewById(R.id.editText2);
        R = (Button) T.findViewById(R.id.button1);
        R.setOnClickListener(new af(this));
        T.setOnTouchListener(new ag(this));
    }

    @Override // android.support.v4.app.Fragment
    public void l() {
        super.l();
        T = null;
        P = null;
        Q = null;
        R = null;
    }
}
