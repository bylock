package net.client.by.lock.gui.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.c.l;
import net.client.by.lock.c.m;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.b.x;

/* compiled from: MyApp */
public class LoginActivity extends a {
    public EditText e;
    public Button f;
    public Button g;
    private String h;
    private String i;
    private EditText j;
    private InputMethodManager k;
    private Dialog l;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (r.i().u() != null || !r.i().q()) {
            setContentView(R.layout.activity_login);
            r.a(true);
            m.a().a(this);
            l.a().a(getApplicationContext());
            this.j = (EditText) findViewById(R.id.username);
            this.e = (EditText) findViewById(R.id.password);
            this.f = (Button) findViewById(R.id.sign_in_button);
            this.g = (Button) findViewById(R.id.register_button);
            this.k = (InputMethodManager) getSystemService("input_method");
            this.j.setText("");
            this.e.setText("");
            this.e.setOnEditorActionListener(new r(this));
            this.f.setOnClickListener(new s(this));
            this.g.setOnClickListener(new t(this));
            View findViewById = findViewById(R.id.content);
            findViewById.setOnTouchListener(new u(this, findViewById));
        } else {
            b.e(this.d, "Session.getInstance().loggedIn(): " + r.i().q());
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(131072);
            startActivity(intent);
            finish();
        }
        if (m.a().d() == -1) {
            new x(this).show();
        }
    }

    public void c() {
        boolean z;
        EditText editText = null;
        if (r.i().q()) {
            this.f.setEnabled(true);
            return;
        }
        this.j.setError(null);
        this.e.setError(null);
        this.h = this.j.getText().toString();
        this.i = this.e.getText().toString();
        r.i().d(this.h);
        r.i().f(this.i);
        if (TextUtils.isEmpty(this.i)) {
            this.e.setError(getString(R.string.error_field_required));
            editText = this.e;
            z = true;
        } else {
            z = false;
        }
        if (TextUtils.isEmpty(this.h)) {
            this.j.setError(getString(R.string.error_field_required));
            editText = this.j;
            z = true;
        }
        if (z) {
            editText.requestFocus();
            a(false);
            this.f.setEnabled(true);
        } else if (j.a(this)) {
            a(true);
            n.a().b();
        } else {
            new AlertDialog.Builder(this).setTitle(R.string.broke_connection).setPositiveButton(R.string.ok, new v(this)).setNegativeButton(R.string.cancel, new w(this)).create().show();
        }
    }

    public void a(boolean z) {
        if (z) {
            try {
                if (this.l == null) {
                    this.l = net.client.by.lock.gui.b.b.a(this, (int) R.string.login_progress_signing_in);
                }
                this.l.show();
            } catch (Exception e2) {
                b.w(this.d, "dialog error:" + e2.toString());
            }
        } else if (this.l != null) {
            this.l.dismiss();
        }
    }

    @Override // net.client.by.lock.gui.activity.a
    public void onResume() {
        super.onResume();
    }

    public void onBackPressed() {
        m.a().f(this);
        moveTaskToBack(false);
        r.i().g();
        n.a().d();
    }
}
