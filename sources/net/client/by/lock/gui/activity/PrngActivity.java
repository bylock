package net.client.by.lock.gui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.ProgressBar;
import java.util.concurrent.atomic.AtomicBoolean;
import net.client.by.lock.R;
import net.client.by.lock.c.n;
import net.client.by.lock.d.r;
import net.client.by.lock.f.k;
import net.client.by.lock.f.l;
import net.client.by.lock.gui.b.b;

/* compiled from: MyApp */
public class PrngActivity extends a implements GestureOverlayView.OnGestureListener {
    ProgressBar e;
    GestureOverlayView f;
    double g = -1.0d;
    double h = -1.0d;
    Dialog i = null;
    private byte[] j;
    private int k;
    private int l;
    private AtomicBoolean m = new AtomicBoolean(false);

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_prng);
        this.e = (ProgressBar) findViewById(R.id.progressBar1);
        this.f = (GestureOverlayView) findViewById(R.id.gestureOverlayView1);
        this.f.addOnGestureListener(this);
        this.j = new byte[64];
        for (int i2 = 0; i2 < 64; i2++) {
            this.j[i2] = 0;
        }
        this.k = 0;
        this.l = 0;
    }

    public void onBackPressed() {
        moveTaskToBack(true);
        r.i().g();
        n.a().d();
    }

    private void a(boolean z) {
        if (this.k < 64) {
            if (z) {
                byte[] bArr = this.j;
                int i2 = this.k;
                bArr[i2] = (byte) (bArr[i2] | (1 << this.l));
            }
            this.l++;
            if (this.l == 8) {
                this.l = 0;
                this.k++;
            }
            this.e.setProgress((int) (100.0d * (((double) ((this.k * 8) + this.l)) / 512.0d)));
            return;
        }
        this.f.removeAllOnGestureListeners();
        r.i().a(this.j);
        if (!this.m.get()) {
            this.m.set(true);
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    public void onGesture(GestureOverlayView gestureOverlayView, MotionEvent motionEvent) {
        double x = (double) motionEvent.getX();
        double y = (double) motionEvent.getY();
        if (!(this.g == -1.0d || this.h == -1.0d)) {
            a(x > this.g);
            a(y > this.h);
        }
        this.g = x;
        this.h = y;
        for (int i2 = 0; i2 < motionEvent.getHistorySize(); i2++) {
            double historicalX = (double) motionEvent.getHistoricalX(i2);
            double historicalY = (double) motionEvent.getHistoricalY(i2);
            if (!(this.g == -1.0d || this.h == -1.0d)) {
                a(historicalX > this.g);
                a(historicalY > this.h);
            }
            this.g = historicalX;
            this.h = historicalY;
        }
    }

    public void onGestureCancelled(GestureOverlayView gestureOverlayView, MotionEvent motionEvent) {
    }

    public void onGestureEnded(GestureOverlayView gestureOverlayView, MotionEvent motionEvent) {
    }

    public void onGestureStarted(GestureOverlayView gestureOverlayView, MotionEvent motionEvent) {
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onResume() {
        super.onResume();
        if (k.a(r.i().l()) != l.SECURE) {
            if (this.i == null) {
                this.i = b.d(this);
            }
            this.i.show();
        }
    }
}
