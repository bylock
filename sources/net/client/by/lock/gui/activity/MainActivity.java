package net.client.by.lock.gui.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.c.a;
import net.client.by.lock.c.i;
import net.client.by.lock.d.c;
import net.client.by.lock.d.r;
import net.client.by.lock.f.o;

/* compiled from: MyApp */
public class MainActivity extends q {
    public static String t = "bylock.mainactivity";
    static String u = null;
    private static MainActivity w;
    ad o;
    ViewPager p;
    View[] r;
    String[] s;
    private InputMethodManager v;
    private BroadcastReceiver x = new x(this);

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        w = this;
        if (!o.a(10)) {
            requestWindowFeature(7);
        }
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        this.p = (ViewPager) findViewById(R.id.viewFlipper1);
        this.o = new ad(e(), (InputMethodManager) getSystemService("input_method"));
        this.r = new View[5];
        this.r[0] = findViewById(R.id.button1);
        this.r[1] = findViewById(R.id.button2);
        this.r[2] = findViewById(R.id.button3);
        this.r[3] = findViewById(R.id.button4);
        this.r[4] = findViewById(R.id.button5);
        this.s = getResources().getStringArray(R.array.pages);
        for (int i = 0; i < 5; i++) {
            this.r[i].setOnClickListener(new ac(this, i));
        }
        this.p.setAdapter(this.o);
        this.p.setCurrentItem(2);
        this.r[2].setEnabled(false);
        this.v = (InputMethodManager) getSystemService("input_method");
        this.p.setOnPageChangeListener(new y(this));
        f().a(this.s[this.p.getCurrentItem()]);
        f().a(false);
        f().b(false);
        f().c(false);
        f().d(true);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onNewIntent(Intent intent) {
        Bundle extras;
        super.onNewIntent(intent);
        b.e(this.q, "new intent");
        if (intent != null && (extras = intent.getExtras()) != null) {
            c a = i.c().a(Integer.valueOf(extras.getInt("id", -1)));
            if (a == null) {
                b.e(this.q, "no friend is associated");
            }
            if (intent.hasExtra("mail") && intent.getBooleanExtra("mail", false)) {
                this.p.setCurrentItem(1);
                ((aw) this.o.a(1)).a(a);
            } else if (intent.hasExtra("forward") && intent.getBooleanExtra("forward", false)) {
                this.p.setCurrentItem(1);
                ((aw) this.o.a(1)).a(a.g(intent.getStringExtra("mailId")));
            } else if (intent.hasExtra("reply") && intent.getBooleanExtra("reply", false)) {
                this.p.setCurrentItem(1);
                ((aw) this.o.a(1)).a(a, a.g(intent.getStringExtra("mailId")));
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onResume() {
        super.onResume();
        registerReceiver(this.x, new IntentFilter(t));
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onPause() {
        unregisterReceiver(this.x);
        super.onPause();
    }

    @Override // android.support.v7.a.f, android.support.v4.app.j
    public void onBackPressed() {
        if (!r.i().h()) {
            new AlertDialog.Builder(this).setMessage(R.string.are_you_leaving).setPositiveButton(R.string.yes, new aa(this)).setNegativeButton(R.string.cancel, new ab(this)).show();
        }
    }

    public static MainActivity i() {
        return w;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onDestroy() {
        b.v(this.q, "main activity destroy " + r.i().r());
        if (r.i().q() && r.i().r()) {
            a.a(w);
        }
        super.onDestroy();
    }
}
