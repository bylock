package net.client.by.lock.gui.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import java.io.File;
import net.client.by.lock.c.m;
import net.client.by.lock.gui.b.b;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class aj implements AdapterView.OnItemLongClickListener {
    final /* synthetic */ ah a;

    aj(ah ahVar) {
        this.a = ahVar;
    }

    @Override // android.widget.AdapterView.OnItemLongClickListener
    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        b.a(ad.b, new File(String.valueOf(m.a().b()) + "/" + ((Object) ((TextView) view).getText()))).show();
        return true;
    }
}
