package net.client.by.lock.gui.activity;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: MyApp */
class n implements View.OnTouchListener {
    final /* synthetic */ ChatActivity a;

    n(ChatActivity chatActivity) {
        this.a = chatActivity;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.a.u.hideSoftInputFromWindow(this.a.o.getWindowToken(), 0);
        return false;
    }
}
