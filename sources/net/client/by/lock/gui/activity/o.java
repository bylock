package net.client.by.lock.gui.activity;

import android.text.Editable;
import android.view.View;

/* compiled from: MyApp */
class o implements View.OnClickListener {
    final /* synthetic */ ChatActivity a;

    o(ChatActivity chatActivity) {
        this.a = chatActivity;
    }

    public void onClick(View view) {
        Editable text = this.a.s.getText();
        if (text.length() > 0) {
            this.a.r.a(text.toString());
            this.a.s.setText("");
        }
    }
}
