package net.client.by.lock.gui.activity;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: MyApp */
class u implements View.OnTouchListener {
    final /* synthetic */ LoginActivity a;
    private final /* synthetic */ View b;

    u(LoginActivity loginActivity, View view) {
        this.a = loginActivity;
        this.b = view;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        LoginActivity.a(this.a).hideSoftInputFromWindow(this.b.getWindowToken(), 0);
        return false;
    }
}
