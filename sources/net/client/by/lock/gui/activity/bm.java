package net.client.by.lock.gui.activity;

import android.text.TextUtils;
import android.view.View;
import net.client.by.lock.c.n;
import net.client.by.lock.f.k;
import net.client.by.lock.f.l;

/* compiled from: MyApp */
class bm implements View.OnClickListener {
    final /* synthetic */ RegisterActivity a;
    private final /* synthetic */ String[] b;

    bm(RegisterActivity registerActivity, String[] strArr) {
        this.a = registerActivity;
        this.b = strArr;
    }

    public void onClick(View view) {
        String charSequence = RegisterActivity.b(this.a).getText().toString();
        String charSequence2 = RegisterActivity.c(this.a).getText().toString();
        String charSequence3 = RegisterActivity.d(this.a).getText().toString();
        if (TextUtils.isEmpty(charSequence)) {
            RegisterActivity.b(this.a).setError("field is required");
            return;
        }
        l a2 = k.a(charSequence2);
        if (a2 != l.SECURE) {
            RegisterActivity.c(this.a).setError(this.b[a2.ordinal()]);
        } else if (!charSequence2.equals(charSequence3)) {
            RegisterActivity.d(this.a).setError("passwords do not match");
        } else {
            this.a.e.show();
            n.a().a(charSequence, charSequence2);
        }
    }
}
