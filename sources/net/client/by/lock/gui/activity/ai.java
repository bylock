package net.client.by.lock.gui.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import net.client.by.lock.c.m;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ai implements AdapterView.OnItemClickListener {
    final /* synthetic */ ah a;

    ai(ah ahVar) {
        this.a = ahVar;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        File file = new File(String.valueOf(m.a().b()) + "/" + ((Object) ((TextView) view).getText()));
        try {
            this.a.a(new Intent().setDataAndType(Uri.fromFile(file), MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(file.toURI().toString()))).setAction("android.intent.action.VIEW"));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this.a.b(), "No handler for this type of file.", 1).show();
        }
    }
}
