package net.client.by.lock.gui.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.gui.b.b;

/* compiled from: MyApp */
public class RegisterActivity extends a {
    public Dialog e;
    private TextView f;
    private TextView g;
    private TextView h;
    private Button i;
    private Button j;
    private InputMethodManager k;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_register);
        this.f = (TextView) findViewById(R.id.username);
        this.g = (TextView) findViewById(R.id.password);
        this.h = (TextView) findViewById(R.id.password_re);
        this.i = (Button) findViewById(R.id.register);
        this.j = (Button) findViewById(R.id.cancel);
        String[] stringArray = getResources().getStringArray(R.array.pass_security);
        this.k = (InputMethodManager) getSystemService("input_method");
        this.j.setOnClickListener(new bk(this));
        View findViewById = findViewById(R.id.content);
        findViewById.setOnTouchListener(new bl(this, findViewById));
        this.i.setOnClickListener(new bm(this, stringArray));
        this.e = b.a(this, (int) R.string.registering);
    }
}
