package net.client.by.lock.gui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.b.k;
import android.text.Editable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewAnimator;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.R;
import net.client.by.lock.b.h;
import net.client.by.lock.c.i;
import net.client.by.lock.d.a;
import net.client.by.lock.d.c;
import net.client.by.lock.d.l;
import net.client.by.lock.d.m;
import net.client.by.lock.d.n;
import net.client.by.lock.d.r;
import net.client.by.lock.f.j;
import net.client.by.lock.gui.b.an;
import net.client.by.lock.gui.b.b;

/* compiled from: MyApp */
public class aw extends Fragment {
    private static ViewGroup Q;
    private static LinearLayout R;
    private static EditText S;
    private static EditText T;
    private static EditText U;
    private static Button V;
    private static Button W;
    private static ViewAnimator X;
    private static View Y;
    private static ArrayList Z = new ArrayList();
    private static ArrayList aa = new ArrayList();
    private static ArrayList ab = new ArrayList();
    public Dialog P;

    @Override // android.support.v4.app.Fragment
    public void c(Bundle bundle) {
        super.c(bundle);
        ad.a((Activity) b());
        a((LayoutInflater) ad.c().getSystemService("layout_inflater"));
        b(true);
    }

    @Override // android.support.v4.app.Fragment
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ad.a((Activity) b());
        if (Q == null) {
            a(layoutInflater);
        } else if (Q.getParent() != null && (Q.getParent() instanceof ViewGroup)) {
            ((ViewGroup) Q.getParent()).removeView(Q);
        }
        return Q;
    }

    @Override // android.support.v4.app.Fragment
    public void a(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.action_mail_send, menu);
        super.a(menu, menuInflater);
    }

    @Override // android.support.v4.app.Fragment
    public boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_attach /*{ENCODED_INT: 2131165405}*/:
                a(new Intent(b(), SelectFilesActivity.class), 17);
                return true;
            case R.id.menu_send /*{ENCODED_INT: 2131165406}*/:
                D();
                return true;
            default:
                return super.a(menuItem);
        }
    }

    private void D() {
        boolean z;
        boolean z2 = false;
        Editable editableText = S.getEditableText();
        if (aa.size() == 0) {
            an.b(b(), "You need to add some recipients.");
            z = false;
        } else {
            z = true;
        }
        Iterator it = ab.iterator();
        while (true) {
            if (it.hasNext()) {
                a aVar = (a) it.next();
                if (aVar.a() != null && ((String) aVar.a().F().a()).equals("IN PROGRESS")) {
                    an.b(b(), "There are files to be uplaoded.");
                    break;
                }
            } else {
                z2 = z;
                break;
            }
        }
        if (z2) {
            String str = "<html><body>" + Html.toHtml(editableText) + "</body></html>";
            String editable = T.getText().toString();
            if (editable.length() == 0) {
                editable = "No Subject";
            }
            if (this.P == null) {
                this.P = b.a(ad.c(), (int) R.string.sending);
            }
            this.P.show();
            new n(aa, editable, str, ab).a(this);
        }
    }

    @Override // android.support.v4.app.Fragment
    public void a(int i, int i2, Intent intent) {
        if (i2 == -1) {
            switch (i) {
                case 14:
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("selected");
                    ArrayList arrayList = new ArrayList();
                    Iterator<Integer> it = integerArrayListExtra.iterator();
                    while (it.hasNext()) {
                        c a = i.c().a(Integer.valueOf(it.next().intValue()));
                        if (a != null) {
                            arrayList.add(a);
                        }
                    }
                    z();
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        a((c) it2.next());
                    }
                    return;
                case 15:
                case 16:
                default:
                    super.a(i, i2, intent);
                    return;
                case k.ActionBar_progressBarPadding:
                    b(intent);
                    return;
            }
        }
    }

    private void b(Intent intent) {
        ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("files");
        if (stringArrayListExtra != null && stringArrayListExtra.size() > 0) {
            Iterator<String> it = stringArrayListExtra.iterator();
            while (it.hasNext()) {
                h hVar = new h(new File(it.next()));
                a aVar = new a(hVar);
                aVar.a(hVar);
                b(aVar);
                hVar.m();
            }
        }
        ArrayList<String> stringArrayListExtra2 = intent.getStringArrayListExtra("virtual");
        if (stringArrayListExtra2 != null) {
            Iterator<String> it2 = stringArrayListExtra2.iterator();
            while (it2.hasNext()) {
                b(new a(net.client.by.lock.b.k.b().b(it2.next())));
            }
        }
    }

    private void a(LayoutInflater layoutInflater) {
        Q = (ViewGroup) layoutInflater.inflate(R.layout.fragment_sendmail, (ViewGroup) null);
        R = (LinearLayout) Q.findViewById(R.id.mail_attachs);
        S = (EditText) Q.findViewById(R.id.mail_body);
        T = (EditText) Q.findViewById(R.id.mail_subject);
        U = (EditText) Q.findViewById(R.id.mail_to);
        V = (Button) Q.findViewById(R.id.button1);
        W = (Button) Q.findViewById(R.id.button2);
        X = (ViewAnimator) Q.findViewById(R.id.viewAnimator1);
        Y = Q.findViewById(R.id.linearLayout1);
        Q.setOnTouchListener(new ax(this));
        V.setOnClickListener(new ay(this));
        W.setOnClickListener(new az(this));
        U.setOnClickListener(new ba(this));
        f(true);
    }

    /* access modifiers changed from: private */
    public static void f(boolean z) {
        boolean z2;
        int i = R.color.aaa_blue_text_over_white_button;
        int i2 = 0;
        int i3 = R.color.White;
        if (ab.isEmpty()) {
            Y.setVisibility(8);
        } else {
            Y.setVisibility(0);
        }
        Button button = V;
        if (z) {
            z2 = false;
        } else {
            z2 = true;
        }
        button.setEnabled(z2);
        W.setEnabled(z);
        V.setTextColor(ad.c().getResources().getColor(z ? R.color.White : R.color.aaa_blue_text_over_white_button));
        V.setBackgroundColor(ad.c().getResources().getColor(z ? R.color.aaa_blue_button_background : R.color.White));
        Button button2 = W;
        Resources resources = ad.c().getResources();
        if (!z) {
            i = R.color.White;
        }
        button2.setTextColor(resources.getColor(i));
        Button button3 = W;
        Resources resources2 = ad.c().getResources();
        if (!z) {
            i3 = R.color.aaa_blue_button_background;
        }
        button3.setBackgroundColor(resources2.getColor(i3));
        ViewAnimator viewAnimator = X;
        if (!z) {
            i2 = 1;
        }
        viewAnimator.setDisplayedChild(i2);
    }

    public synchronized void a(c cVar) {
        if (!aa.contains(cVar) && aa.add(cVar)) {
            StringBuilder sb = new StringBuilder();
            Iterator it = aa.iterator();
            while (it.hasNext()) {
                sb.append(((c) it.next()).G()).append(", ");
            }
            if (sb.length() > 2) {
                U.setText(sb.subSequence(0, sb.length() - 2));
            } else {
                U.setText("");
            }
        }
    }

    public synchronized void z() {
        aa.clear();
        U.setText("");
    }

    private synchronized void b(a aVar) {
        if (!ab.contains(aVar)) {
            View a = aVar.a().A().a((Context) b(), true);
            Button button = (Button) a.findViewById(R.id.cancel);
            button.setVisibility(0);
            ProgressBar progressBar = (ProgressBar) a.findViewById(R.id.chat_bar);
            button.setOnClickListener(new bb(aVar));
            ((TextView) a.findViewById(R.id.file_name)).setText(aVar.e());
            ((TextView) a.findViewById(R.id.size)).setText(j.a((long) aVar.f().intValue()));
            if (aVar.a() == null || ((String) aVar.a().F().a()).equals("COMPLETED")) {
                progressBar.setVisibility(8);
            } else {
                progressBar.setVisibility(0);
            }
            ab.add(aVar);
            Z.add(a);
            R.addView(a);
            f(false);
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void c(a aVar) {
        synchronized (aw.class) {
            int indexOf = ab.indexOf(aVar);
            Z.remove(indexOf);
            ab.remove(indexOf);
            R.removeViewAt(indexOf);
            f(ab.isEmpty());
        }
    }

    public void A() {
        S.setText("");
        T.setText("");
        U.setText("");
        Z.clear();
        aa.clear();
        ab.clear();
        R.removeAllViews();
        f(true);
    }

    public void a(c cVar, m mVar) {
        a(mVar);
        T.setText("Re: " + mVar.g());
        a(cVar);
    }

    public void a(m mVar) {
        String k;
        A();
        T.setText("Fw: " + mVar.g());
        if (mVar instanceof l) {
            if (mVar.E().E() == null || mVar.E().E().length() == 0) {
                k = mVar.E().C();
            } else {
                k = String.valueOf(mVar.E().E()) + " (" + mVar.E().C() + ")";
            }
        } else if (r.i().n().a() == null || ((String) r.i().n().a()).length() == 0) {
            k = r.i().k();
        } else {
            k = String.valueOf((String) r.i().n().a()) + " (" + r.i().k() + ")";
        }
        S.setText(Html.fromHtml(mVar.f().replaceAll("<body>", "<body><p><br></p><p>" + mVar.B() + ", " + k + "  yazd˝:</p><blockquote style=\"border-left: 1px solid #CCCCCC; margin: 0 0 0 0.8ex; padding-left: 1ex; color: #222222;\">").replaceAll("</body>", "</blockquote></body>")));
        Iterator it = mVar.l().iterator();
        while (it.hasNext()) {
            b((a) it.next());
        }
    }

    @Override // android.support.v4.app.Fragment
    public void l() {
        super.l();
        V = null;
        W = null;
        Q = null;
        R = null;
        S = null;
        T = null;
        Z.clear();
    }
}
