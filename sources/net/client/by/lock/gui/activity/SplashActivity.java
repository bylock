package net.client.by.lock.gui.activity;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.d.p;
import java.util.concurrent.TimeUnit;
import net.client.by.lock.R;
import net.client.by.lock.c.a;
import net.client.by.lock.c.m;
import net.client.by.lock.d.r;

/* compiled from: MyApp */
public class SplashActivity extends a {
    private Intent e = null;
    private boolean f = false;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onStart() {
        super.onStart();
        p.a(this).a("GTM-TSDSM8", -1).a(new bv(this), 5, TimeUnit.SECONDS);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_splash);
        this.f = false;
        ((NotificationManager) getSystemService("notification")).cancel(a.a);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onResume() {
        super.onResume();
        int i = 1800;
        if (!r.i().r()) {
            this.e = new Intent(this, LoginActivity.class);
            this.e.addFlags(268435456);
            this.e.addFlags(67108864);
        } else if (r.i().u() != null) {
            this.e = new Intent(this, LoginActivity.class);
            this.e.addFlags(131072);
        } else if (m.a().f()) {
            this.e = new Intent(this, PinActivity.class);
            i = 0;
        } else {
            this.e = new Intent(this, MainActivity.class);
            this.e.addFlags(131072);
            i = 0;
        }
        findViewById(R.id.imageView1).postDelayed(new bw(this), (long) i);
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onPause() {
        super.onPause();
        this.f = true;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onDestroy() {
        super.onDestroy();
    }
}
