package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ExpandableListView;
import net.client.by.lock.a;
import net.client.by.lock.d.c;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ao implements ExpandableListView.OnChildClickListener {
    final /* synthetic */ am a;

    ao(am amVar) {
        this.a = amVar;
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        c a2 = a.e.getChild(i, i2);
        if (a2 == null) {
            return false;
        }
        if (a2.u() == null) {
            Intent intent = new Intent(ad.b, ContactActivity.class);
            intent.putExtra("id", a2.K());
            ad.b.startActivityForResult(intent, 13);
            return false;
        }
        Intent intent2 = new Intent(ad.b, ChatActivity.class);
        intent2.putExtra("id", a2.K());
        ad.b.startActivityForResult(intent2, 3);
        return false;
    }
}
