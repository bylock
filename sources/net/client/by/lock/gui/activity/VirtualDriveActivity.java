package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import net.client.by.lock.R;
import net.client.by.lock.gui.b.ao;

/* compiled from: MyApp */
public class VirtualDriveActivity extends q {
    private ao o;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_virtual_drive);
        this.o = new ao(this);
        ((ListView) findViewById(R.id.listView1)).setAdapter((ListAdapter) this.o);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_virtual_drive, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_done /*{ENCODED_INT: 2131165408}*/:
                Intent intent = new Intent();
                intent.putExtra("virtual", this.o.a());
                setResult(-1, intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
