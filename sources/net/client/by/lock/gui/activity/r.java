package net.client.by.lock.gui.activity;

import android.view.KeyEvent;
import android.widget.TextView;
import net.client.by.lock.R;

/* compiled from: MyApp */
class r implements TextView.OnEditorActionListener {
    final /* synthetic */ LoginActivity a;

    r(LoginActivity loginActivity) {
        this.a = loginActivity;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != R.id.login && i != 0) {
            return false;
        }
        this.a.f.setEnabled(false);
        this.a.c();
        return true;
    }
}
