package net.client.by.lock.gui.activity;

import android.os.FileObserver;
import java.io.File;
import java.util.Arrays;
import net.client.by.lock.c.m;

/* compiled from: MyApp */
class ak extends FileObserver {
    public ak() {
        super(m.a().b(), 960);
    }

    public void onEvent(int i, String str) {
        if (i <= 2048) {
            File file = new File(m.a().b());
            ah.T.clear();
            ah.T.addAll(Arrays.asList(file.list()));
            if (ah.Q != null) {
                ah.Q.post(new al(this));
            }
        }
    }
}
