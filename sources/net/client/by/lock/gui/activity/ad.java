package net.client.by.lock.gui.activity;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.p;
import android.support.v4.app.v;
import android.view.inputmethod.InputMethodManager;

public class ad extends v {
    private static InputMethodManager a;
    private static Activity b = null;

    public ad(p pVar, InputMethodManager inputMethodManager) {
        super(pVar);
        a = inputMethodManager;
    }

    @Override // android.support.v4.app.v
    public Fragment a(int i) {
        switch (i) {
            case 0:
                return new ar();
            case 1:
                return new aw();
            case 2:
                return new am();
            case 3:
                return new ah();
            case 4:
                return new ae();
            default:
                return new am();
        }
    }

    @Override // android.support.v4.view.ap
    public int b() {
        return 5;
    }
}
