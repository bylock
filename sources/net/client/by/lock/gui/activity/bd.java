package net.client.by.lock.gui.activity;

import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;

/* compiled from: MyApp */
class bd extends AsyncTask {
    final /* synthetic */ ReadIMailActivity a;

    bd(ReadIMailActivity readIMailActivity) {
        this.a = readIMailActivity;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Spanned doInBackground(Void... voidArr) {
        return Html.fromHtml(this.a.w.f());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Spanned spanned) {
        this.a.o.setText(spanned);
    }
}
