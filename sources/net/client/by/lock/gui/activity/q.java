package net.client.by.lock.gui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.a.f;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.c.g;
import net.client.by.lock.c.m;

/* compiled from: MyApp */
public abstract class q extends f {
    private static Activity o;
    protected String q = "FRAGMENT_ACTIVITY";

    /* access modifiers changed from: protected */
    @Override // android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.q = getClass().getName();
        b.i(this.q, "onCreate");
        g.a().a(this);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onResume() {
        super.onResume();
        o = this;
        g.a().a(this);
        b.i(this.q, "onResume");
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onStart() {
        super.onStart();
        b.i(this.q, "onStart");
        g.a().a(this);
        if (a.c.incrementAndGet() > 0) {
            a.a.set(false);
        }
        if (a.c.get() == 1 && (((this instanceof SelectFilesActivity) || (this instanceof SelectFriendsActivity) || (this instanceof VirtualDriveActivity) || (this instanceof ContactActivity) || (this instanceof ChatActivity) || (this instanceof ReadOMailActivity) || (this instanceof SettingsActivity) || (this instanceof ReadIMailActivity) || (this instanceof MainActivity)) && m.a().f())) {
            a.b.set(true);
        }
        if (a.b.get()) {
            Intent intent = new Intent(this, PinActivity.class);
            intent.addFlags(131072);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.a.f, android.support.v4.app.j
    public void onStop() {
        super.onStop();
        b.i(this.q, "onStop");
        if (a.c.decrementAndGet() < 1) {
            a.a.set(true);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onDestroy() {
        super.onDestroy();
        b.i(this.q, "onDestroy");
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onPause() {
        b.i(this.q, "onPause");
        if (isFinishing()) {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
        super.onPause();
        o = null;
    }
}
