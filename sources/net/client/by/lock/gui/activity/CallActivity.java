package net.client.by.lock.gui.activity;

import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewAnimator;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.R;
import net.client.by.lock.a.c;
import net.client.by.lock.c.b;
import net.client.by.lock.c.i;
import net.client.by.lock.c.l;
import net.client.by.lock.f.o;
import net.client.by.lock.gui.b.ag;
import net.client.by.lock.gui.b.ah;

/* compiled from: MyApp */
public class CallActivity extends a implements Observer, ah {
    Animation.AnimationListener e = new b(this);
    private c f;
    private net.client.by.lock.d.c g;
    private ViewAnimator h;
    private View[] i;
    private Chronometer j;
    private View k;
    private String l;
    private AudioManager m;
    private WifiManager.WifiLock n;
    private boolean o = false;
    private ag p;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().addFlags(6291584);
        setContentView(R.layout.activity_call);
        this.h = (ViewAnimator) findViewById(R.id.viewAnimator1);
        this.i = new View[]{findViewById(R.id.call_0), findViewById(R.id.call_1), findViewById(R.id.call_2)};
        this.g = i.c().a(Integer.valueOf(getIntent().getExtras().getInt("id")));
        this.f = this.g.L();
        this.j = (Chronometer) this.i[1].findViewById(R.id.textView1);
        this.p = new ag(this);
        this.f.F().addObserver(this);
        this.f.F().a((String) this.f.F().a());
        this.l = this.g.G();
        this.o = a();
        if (this.f.b_()) {
            a(2, -1);
        } else if (!this.f.b_()) {
            a(0, -1);
        }
        this.m = (AudioManager) getSystemService("audio");
        ((ToggleButton) this.i[1].findViewById(R.id.toggleButton1)).setOnCheckedChangeListener(new l(this.m));
        this.i[0].findViewById(R.id.imageView2).setOnClickListener(new k(this.f, this));
        this.i[0].findViewById(R.id.imageView3).setOnClickListener(new c(this));
        this.i[1].findViewById(R.id.button1).setOnClickListener(new h(this.f, this.j, this));
        this.i[2].findViewById(R.id.button1).setOnClickListener(new d(this));
        ((ToggleButton) this.i[1].findViewById(R.id.toggleButton2)).setOnCheckedChangeListener(new j(this.m));
        this.k = this.i[1].findViewById(R.id.relativeLayout1);
        c();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onPause() {
        if (isFinishing()) {
            if (this.n != null && this.n.isHeld()) {
                this.n.release();
            }
            this.m.setSpeakerphoneOn(false);
            this.m.setStreamMute(0, false);
            if (this.o) {
                moveTaskToBack(true);
            }
        }
        super.onPause();
    }

    public void onBackPressed() {
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyCode != 24 && (keyCode != 25 || !this.f.a("RINGING") || this.f.b_())) {
            return super.dispatchKeyEvent(keyEvent);
        }
        l.a().a(10);
        return true;
    }

    public void onAttachedToWindow() {
        getWindow().addFlags(6815872);
    }

    public void a(Object obj) {
        if (obj.equals("ANSWERED")) {
            if (this.h.getCurrentView() != this.i[1]) {
                a(1, -1);
            }
            this.j.setBase(SystemClock.elapsedRealtime());
            this.j.start();
            if (this.f.b_()) {
                l.a().a(11);
            } else {
                this.f.b_();
            }
        } else if (obj.equals("CALLEE BUSY")) {
            l.a().a(12);
            ((TextView) this.i[2].findViewById(R.id.call_status)).setText(R.string.call_busy);
            this.f.b().a("Busy");
            a(2000);
        } else if (obj.equals("CALLER BUSY")) {
            this.f.b().a("Busy");
            a(0);
        } else if (obj.equals("CALLING")) {
        } else {
            if (obj.equals("CANCELED")) {
                if (!this.f.b_()) {
                    l.a().a(this.g, 4);
                    this.f.b().a("Missed Call");
                } else {
                    this.f.b().a("Cancelled");
                }
                l.a().a(10);
                ((TextView) this.h.getCurrentView().findViewById(R.id.call_status)).setText(R.string.call_cancelled);
                a(1200);
            } else if (obj.equals("CLOSED")) {
                b.a.set(true);
                ((TextView) this.h.getCurrentView().findViewById(R.id.call_status)).setText(R.string.call_ended);
                a(1500);
                l.a().a(11);
                this.j.stop();
                this.f.b().a(this.j.getText().toString());
                this.p.a();
            } else if (obj.equals("SOME ERROR")) {
                ((TextView) this.h.getCurrentView().findViewById(R.id.call_status)).setText(R.string.call_error);
                if (this.f.b_()) {
                    l.a().a(11);
                } else if (!this.f.b_()) {
                    l.a().a(10);
                }
                this.f.b().a("Error");
                a(2000);
                this.p.a();
            } else if (obj.equals("MISSED")) {
                ((TextView) this.h.getCurrentView().findViewById(R.id.call_status)).setText(R.string.call_missed);
                l.a().c(this.g, 12);
                this.f.b().a("Missed Call");
                a(2000);
            } else if (obj.equals("REJECTED")) {
                this.f.b().a("Rejected");
                l.a().c(this.g, 12);
                ((TextView) this.h.getCurrentView().findViewById(R.id.call_status)).setText(R.string.call_rejected);
                a(2000);
            } else if (!obj.equals("RINGING")) {
            } else {
                if (this.f.b_()) {
                    l.a().c(this.g, 11);
                    this.i[2].findViewById(R.id.button1).setEnabled(true);
                } else if (!this.f.b_()) {
                    l.a().c(this.g, 10);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3) {
        this.h.setDisplayedChild(i2);
        ((TextView) this.i[i2].findViewById(R.id.call_name)).setText(this.l);
    }

    private void c() {
        if (((ConnectivityManager) getSystemService("connectivity")).getNetworkInfo(1).isConnected()) {
            this.n = ((WifiManager) getSystemService("wifi")).createWifiLock(o.a(), this.d);
            this.n.acquire();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        new Handler().postDelayed(new f(this), (long) i2);
    }

    public void update(Observable observable, Object obj) {
        if (((String) obj).equals("stateProperty")) {
            runOnUiThread(new g(this, observable));
        }
    }

    @Override // net.client.by.lock.gui.b.ah
    public void a(boolean z) {
        TranslateAnimation translateAnimation;
        if (z) {
            translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) this.k.getHeight());
            translateAnimation.setAnimationListener(this.e);
        } else {
            this.k.setVisibility(0);
            translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) this.k.getHeight(), 0.0f);
        }
        translateAnimation.setDuration(300);
        translateAnimation.setInterpolator(new AccelerateInterpolator(1.0f));
        this.k.startAnimation(translateAnimation);
    }
}
