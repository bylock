package net.client.by.lock.gui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: MyApp */
class x extends BroadcastReceiver {
    final /* synthetic */ MainActivity a;

    x(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            MainActivity.u = intent.getStringExtra("caption");
            if (MainActivity.u == null || MainActivity.u.length() == 0) {
                this.a.f().a(this.a.s[this.a.p.getCurrentItem()]);
            } else {
                this.a.f().a(MainActivity.u);
            }
        }
    }
}
