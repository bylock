package net.client.by.lock.gui.activity;

import android.view.View;
import android.widget.CheckedTextView;
import net.client.by.lock.d.c;
import net.client.by.lock.d.s;

/* compiled from: MyApp */
class bt implements View.OnClickListener {
    final /* synthetic */ SelectFriendsActivity a;

    bt(SelectFriendsActivity selectFriendsActivity) {
        this.a = selectFriendsActivity;
    }

    public void onClick(View view) {
        CheckedTextView checkedTextView = (CheckedTextView) view;
        boolean z = !checkedTextView.isChecked();
        s sVar = (s) view.getTag();
        checkedTextView.setChecked(z);
        for (int i = 0; i < sVar.e().size(); i++) {
            this.a.a((c) ((s) sVar.e().get(i)).d(), z);
        }
    }
}
