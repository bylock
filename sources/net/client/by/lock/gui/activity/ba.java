package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.d.c;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ba implements View.OnClickListener {
    final /* synthetic */ aw a;

    ba(aw awVar) {
        this.a = awVar;
    }

    public void onClick(View view) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        Iterator it = aw.aa.iterator();
        while (it.hasNext()) {
            arrayList.add(Integer.valueOf(((c) it.next()).K()));
        }
        Intent intent = new Intent(this.a.b(), SelectFriendsActivity.class);
        intent.putIntegerArrayListExtra("mSelectedFriends", arrayList);
        this.a.a(intent, 14);
    }
}
