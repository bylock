package net.client.by.lock.gui.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import net.client.by.lock.a;
import net.client.by.lock.d.c;
import net.client.by.lock.d.g;
import net.client.by.lock.gui.b.b;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class an implements AdapterView.OnItemLongClickListener {
    final /* synthetic */ am a;

    an(am amVar) {
        this.a = amVar;
    }

    @Override // android.widget.AdapterView.OnItemLongClickListener
    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        if (ExpandableListView.getPackedPositionType(j) != 1) {
            return false;
        }
        int packedPositionGroup = ExpandableListView.getPackedPositionGroup(j);
        c a2 = a.e.getChild(packedPositionGroup, ExpandableListView.getPackedPositionChild(j));
        g gVar = (g) a.e.getGroup(packedPositionGroup).d();
        if (gVar == null || a2 == null) {
            return false;
        }
        b.a(ad.b, a2, gVar).show();
        return true;
    }
}
