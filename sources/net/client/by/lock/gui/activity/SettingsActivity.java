package net.client.by.lock.gui.activity;

import android.os.Bundle;
import android.support.v7.a.a;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ToggleButton;
import net.client.by.lock.R;
import net.client.by.lock.c.m;

/* compiled from: MyApp */
public class SettingsActivity extends q {
    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_settings);
        findViewById(R.id.textView1).setOnClickListener(new bu(this));
        ((ToggleButton) findViewById(R.id.toggleButton1)).setChecked(m.a().f());
        a f = f();
        f.b(true);
        f.a(17170445);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_settings, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            case R.id.menu_done /*{ENCODED_INT: 2131165408}*/:
                m.a().a(((ToggleButton) findViewById(R.id.toggleButton1)).isChecked());
                finish();
                return true;
            default:
                return false;
        }
    }
}
