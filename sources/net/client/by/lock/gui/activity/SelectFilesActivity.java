package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.b.k;
import android.view.View;
import android.widget.TextView;
import group.pals.android.lib.ui.filechooser.FileChooserActivity;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import net.client.by.lock.R;
import net.client.by.lock.f.a;

/* compiled from: MyApp */
public class SelectFilesActivity extends q {
    private static String o;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_select_files);
        o = null;
        findViewById(R.id.relativeLayout1).setOnClickListener(new bn(this));
        findViewById(R.id.relativeLayout2).setOnClickListener(new bo(this));
        findViewById(R.id.relativeLayout3).setOnClickListener(new bp(this));
        findViewById(R.id.relativeLayout4).setOnClickListener(new bq(this));
        View inflate = View.inflate(this, R.layout.actionbar_select_friends, null);
        f().b(16);
        f().a(inflate);
        inflate.findViewById(R.id.actionBarBack).setOnClickListener(new br(this));
        ((TextView) inflate.findViewById(R.id.actionBarTitle)).setText(R.string.attach_file);
    }

    /* access modifiers changed from: private */
    public static File j() {
        File file = new File(a.a());
        file.mkdirs();
        File createTempFile = File.createTempFile("byLock-Captured_" + new SimpleDateFormat("yyyyMMdd_HHmmss", net.client.by.lock.a.f).format(new Date()) + "_", ".JPG", file);
        createTempFile.mkdirs();
        o = createTempFile.getAbsolutePath();
        return createTempFile;
    }

    @Override // android.support.v4.app.j
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1) {
            switch (i) {
                case k.ActionBar_itemPadding:
                    k();
                    return;
                case 19:
                    d(intent);
                    return;
                case 20:
                    e(intent);
                    return;
                case 21:
                    c(intent);
                    return;
                default:
                    super.onActivityResult(i, i2, intent);
                    return;
            }
        } else if (i2 == 0) {
            setResult(0);
            finish();
        }
    }

    private void c(Intent intent) {
        Intent intent2 = new Intent();
        ArrayList arrayList = new ArrayList();
        Iterator it = ((ArrayList) intent.getSerializableExtra(FileChooserActivity.n)).iterator();
        while (it.hasNext()) {
            arrayList.add(((File) it.next()).getAbsolutePath());
        }
        intent2.putExtra("files", arrayList);
        setResult(-1, intent2);
        finish();
    }

    private void d(Intent intent) {
        if (intent != null && intent.getData() != null) {
            Uri data = intent.getData();
            Cursor query = getContentResolver().query(data, new String[]{"_data"}, null, null, null);
            if (query != null) {
                query.moveToFirst();
                o = query.getString(0);
                query.close();
                b(o);
            } else if (intent.getData() != null) {
                o = intent.getData().getPath();
                if (o != null && new File(o).exists()) {
                    b(o);
                }
            }
        }
    }

    private void k() {
        b(o);
    }

    private void e(Intent intent) {
        setResult(-1, intent);
        finish();
    }

    private void b(String str) {
        Intent intent = new Intent();
        ArrayList arrayList = new ArrayList();
        arrayList.add(str);
        intent.putExtra("files", arrayList);
        setResult(-1, intent);
        finish();
    }
}
