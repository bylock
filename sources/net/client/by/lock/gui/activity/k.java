package net.client.by.lock.gui.activity;

import android.view.View;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.a.c;
import net.client.by.lock.c.l;

/* compiled from: MyApp */
class k implements View.OnClickListener {
    private c a;
    private CallActivity b;
    private boolean c = false;

    public k(c cVar, CallActivity callActivity) {
        this.a = cVar;
        this.b = callActivity;
    }

    public void onClick(View view) {
        if (!this.c) {
            ((TextView) CallActivity.b(this.b).getCurrentView().findViewById(R.id.call_status)).setText(R.string.call_rejected);
            l.a().a(10);
            this.a.j();
            this.a.b().a("Rejected");
        }
    }
}
