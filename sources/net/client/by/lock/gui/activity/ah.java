package net.client.by.lock.gui.activity;

import android.os.Bundle;
import android.os.FileObserver;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import net.client.by.lock.R;
import net.client.by.lock.c.m;

/* compiled from: MyApp */
public class ah extends Fragment {
    private static ViewGroup P;
    private static ListView Q;
    private static ArrayAdapter R;
    private static FileObserver S = new ak();
    private static ArrayList T = new ArrayList();

    @Override // android.support.v4.app.Fragment
    public void c(Bundle bundle) {
        super.c(bundle);
        ad.b = b();
        a((LayoutInflater) ad.b.getSystemService("layout_inflater"));
    }

    @Override // android.support.v4.app.Fragment
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ad.b = b();
        if (P == null) {
            a(layoutInflater);
        } else if (P.getParent() != null && (P.getParent() instanceof ViewGroup)) {
            ((ViewGroup) P.getParent()).removeView(P);
        }
        return P;
    }

    private void a(LayoutInflater layoutInflater) {
        P = (ViewGroup) layoutInflater.inflate(R.layout.fragment_downloads, (ViewGroup) null);
        Q = (ListView) P.findViewById(R.id.listView1);
        File file = new File(m.a().b());
        if (!file.exists()) {
            file.mkdirs();
        }
        T.clear();
        String[] list = file.list();
        if (list != null) {
            T.addAll(Arrays.asList(list));
        }
        R = new ArrayAdapter(ad.b, (int) R.layout.item_downloads, T);
        Q.setAdapter((ListAdapter) R);
        S.startWatching();
        Q.setOnItemClickListener(new ai(this));
        Q.setOnItemLongClickListener(new aj(this));
    }

    @Override // android.support.v4.app.Fragment
    public void l() {
        super.l();
        P = null;
        R.notifyDataSetInvalidated();
        R = null;
        Q = null;
        S.stopWatching();
    }
}
