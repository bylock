package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import java.io.IOException;
import net.client.by.lock.gui.b.an;

/* compiled from: MyApp */
class bn implements View.OnClickListener {
    final /* synthetic */ SelectFilesActivity a;

    bn(SelectFilesActivity selectFilesActivity) {
        this.a = selectFilesActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        try {
            intent.putExtra("output", Uri.fromFile(SelectFilesActivity.j()));
            this.a.startActivityForResult(intent, 18);
        } catch (IOException e) {
            an.a(this.a, "Error in access to writable directory");
            this.a.setResult(0);
            this.a.finish();
        }
    }
}
