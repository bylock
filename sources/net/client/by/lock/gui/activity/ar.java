package net.client.by.lock.gui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.R;
import net.client.by.lock.d.r;

/* compiled from: MyApp */
public class ar extends Fragment implements Observer {
    private static ViewGroup P;
    private static TextView Q;
    private static TextView R;
    private static TextView S;
    private static TextView T;
    private static ImageView U;
    private static View V;
    private static View W;
    private static View X;

    @Override // android.support.v4.app.Fragment
    public void c(Bundle bundle) {
        super.c(bundle);
        ad.b = b();
        b(true);
        a((LayoutInflater) ad.b.getSystemService("layout_inflater"));
    }

    @Override // android.support.v4.app.Fragment
    public void i() {
        super.i();
        r.i().b().addObserver(this);
        r.i().o().addObserver(this);
        r.i().n().addObserver(this);
    }

    @Override // android.support.v4.app.Fragment
    public void j() {
        r.i().b().deleteObserver(this);
        r.i().o().deleteObserver(this);
        r.i().n().deleteObserver(this);
        super.j();
    }

    @Override // android.support.v4.app.Fragment
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ad.b = b();
        if (P == null) {
            a(layoutInflater);
        } else if (P.getParent() != null && (P.getParent() instanceof ViewGroup)) {
            ((ViewGroup) P.getParent()).removeView(P);
        }
        z();
        return P;
    }

    @Override // android.support.v4.app.Fragment
    public void a(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.action_profile, menu);
        super.a(menu, menuInflater);
    }

    @Override // android.support.v4.app.Fragment
    public boolean a(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_logout /*{ENCODED_INT: 2131165407}*/:
                ad.b.onBackPressed();
                return true;
            default:
                return super.a(menuItem);
        }
    }

    private void a(LayoutInflater layoutInflater) {
        P = (ViewGroup) layoutInflater.inflate(R.layout.fragment_profile, (ViewGroup) null);
        Q = (TextView) P.findViewById(R.id.name);
        R = (TextView) P.findViewById(R.id.public_message);
        V = P.findViewById(R.id.edit_name);
        W = P.findViewById(R.id.edit_password);
        X = P.findViewById(R.id.edit_public_message);
        S = (TextView) P.findViewById(R.id.username);
        U = (ImageView) P.findViewById(R.id.status_img);
        T = (TextView) P.findViewById(R.id.status_text);
        S.setText(r.i().k());
        Q.setText((CharSequence) r.i().n().a());
        R.setText((CharSequence) r.i().o().a());
        V.setOnClickListener(new as(this));
        W.setOnClickListener(new at(this));
        X.setOnClickListener(new au(this));
        av avVar = new av(this);
        U.setOnClickListener(avVar);
        T.setOnClickListener(avVar);
    }

    private void z() {
        switch (((Integer) r.i().b().a()).intValue()) {
            case 0:
                T.setText(R.string.status_offline);
                U.setImageResource(R.drawable.big_online_0);
                return;
            case 1:
                T.setText(R.string.status_busy);
                U.setImageResource(R.drawable.big_online_1);
                return;
            case 2:
                T.setText(R.string.status_away);
                U.setImageResource(R.drawable.big_online_2);
                return;
            case 3:
                T.setText(R.string.status_available);
                U.setImageResource(R.drawable.big_online_3);
                return;
            default:
                return;
        }
    }

    public void update(Observable observable, Object obj) {
        if (obj == null) {
            return;
        }
        if (obj.equals("status")) {
            z();
        } else if (obj.equals("publicMessage")) {
            R.setText((CharSequence) r.i().o().a());
        } else if (obj.equals("name")) {
            Q.setText((CharSequence) r.i().n().a());
        }
    }

    @Override // android.support.v4.app.Fragment
    public void l() {
        super.l();
        P = null;
        Q = null;
        V = null;
        W = null;
        X = null;
        R = null;
        S = null;
        T = null;
        U = null;
    }
}
