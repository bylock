package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.b.k;
import android.support.v7.c.a;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import group.pals.android.lib.ui.filechooser.FileChooserActivity;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.R;
import net.client.by.lock.b.d;
import net.client.by.lock.c.g;
import net.client.by.lock.c.i;
import net.client.by.lock.c.l;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;
import net.client.by.lock.d.r;
import net.client.by.lock.f.h;
import net.client.by.lock.f.j;
import net.client.by.lock.f.o;
import net.client.by.lock.gui.b.an;

/* compiled from: MyApp */
public class ChatActivity extends q implements Observer {
    public ListView o;
    a p;
    private c r;
    private EditText s;
    private d t;
    private InputMethodManager u;
    private android.support.v7.a.a v;
    private p w;
    private net.client.by.lock.gui.b.a x;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_chat);
        if (getIntent() == null) {
            finish();
        } else if (getIntent().hasExtra("id")) {
            int i = getIntent().getExtras().getInt("id");
            this.r = i.c().a(Integer.valueOf(i));
            if (this.r == null) {
                an.a(this, "Friend with id: " + i + " could not be found.");
                finish();
                return;
            }
            this.o = (ListView) findViewById(R.id.chat_log);
            this.s = (EditText) findViewById(R.id.chat_input_text);
            this.s.setOnEditorActionListener(new m(this));
            this.u = (InputMethodManager) getSystemService("input_method");
            this.o.setOnTouchListener(new n(this));
            findViewById(R.id.chat_send).setOnClickListener(new o(this));
            this.x = this.r.M();
            this.x.a(this);
            this.o.setAdapter((ListAdapter) this.x);
            this.v = f();
            this.v.b(true);
            this.v.d(true);
            this.v.a(this.r.G());
            this.v.a(17170445);
            this.w = new p(this, null);
        } else {
            finish();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_chat, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            case R.id.menu_lock /*{ENCODED_INT: 2131165387}*/:
                q();
                return true;
            case R.id.menu_call /*{ENCODED_INT: 2131165388}*/:
                l();
                return true;
            case R.id.menu_file /*{ENCODED_INT: 2131165389}*/:
                o();
                return true;
            case R.id.menu_mail /*{ENCODED_INT: 2131165390}*/:
                n();
                return true;
            case R.id.menu_profile /*{ENCODED_INT: 2131165391}*/:
                m();
                return true;
            case R.id.menu_clear_history /*{ENCODED_INT: 2131165392}*/:
                p();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onActivityResult(int i, int i2, Intent intent) {
        Uri data;
        super.onActivityResult(i, i2, intent);
        g.a().a(this);
        if (i2 == -1) {
            switch (i) {
                case 5:
                    Iterator it = ((ArrayList) intent.getSerializableExtra(FileChooserActivity.n)).iterator();
                    while (it.hasNext()) {
                        this.r.a((File) it.next());
                    }
                    return;
                case 10:
                    if (intent != null && intent.getData() != null && (data = intent.getData()) != null) {
                        Cursor query = getContentResolver().query(data, new String[]{"_data"}, null, null, null);
                        query.moveToFirst();
                        String string = query.getString(0);
                        query.close();
                        this.r.a(new File(string));
                        return;
                    }
                    return;
                case 14:
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("selected");
                    ArrayList arrayList = new ArrayList();
                    Iterator<Integer> it2 = integerArrayListExtra.iterator();
                    while (it2.hasNext()) {
                        c a = i.c().a(Integer.valueOf(it2.next().intValue()));
                        if (a != null) {
                            arrayList.add(a);
                        }
                    }
                    n.a().a(this.t, arrayList);
                    return;
                case k.ActionBar_progressBarPadding:
                    d(intent);
                    return;
                case 22:
                    c(intent);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("id", this.r.K());
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onPause() {
        try {
            this.r.k();
            this.r.q().deleteObserver(this);
            r.i().t().deleteObserver(this);
            this.r.x().deleteObserver(this);
            this.r.r().deleteObserver(this);
            this.u.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            if (isFinishing()) {
                i.c().deleteObserver(this);
                this.x = null;
            }
        } catch (Exception e) {
            j.a(String.valueOf(this.q) + "ChatActivity", e);
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onResume() {
        super.onResume();
        if (a.b()) {
            Intent intent = new Intent(this, CallActivity.class);
            intent.addFlags(131072);
            startActivity(intent);
            return;
        }
        l.a().b(this.r, 1);
        l.a().b(this.r, 2);
        l.a().b(this.r, 3);
        l.a().b(this.r, 4);
        setTitle(String.valueOf((String) r.i().f().a()) + "   :   " + ((String) r.i().o().a()));
        this.r.q().addObserver(this);
        this.r.x().addObserver(this);
        this.r.r().addObserver(this);
        r.i().t().addObserver(this);
        i.c().addObserver(this);
        this.r.j();
        o.a(this);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        int i;
        if (this.r == null) {
            return false;
        }
        MenuItem item = menu.getItem(0);
        MenuItem item2 = menu.getItem(1);
        String str = (String) this.r.x().a();
        if (str.equals("NK")) {
            item.setVisible(false);
            item.setEnabled(false);
        } else if (str.equals("KWCS")) {
            item.setVisible(true);
            item.setEnabled(true);
            item.setIcon(R.drawable.action_lock_2);
        } else if (str.equals("KWIS")) {
            item.setVisible(true);
            item.setEnabled(true);
            item.setIcon(R.drawable.action_lock_3);
        } else if (str.equals("KWOS")) {
            item.setVisible(true);
            item.setEnabled(true);
            item.setIcon(R.drawable.action_lock_1);
        }
        if (((Boolean) this.r.q().a()).booleanValue()) {
            item2.setEnabled(true);
        } else {
            item2.setEnabled(false);
        }
        switch (((Integer) this.r.r().a()).intValue()) {
            case 0:
                i = R.drawable.action_call_0;
                break;
            case 1:
                i = R.drawable.action_call_1;
                break;
            case 2:
                i = R.drawable.action_call_2;
                break;
            case 3:
                i = R.drawable.action_call_3;
                break;
            default:
                i = R.drawable.action_call;
                break;
        }
        item2.setIcon(i);
        return true;
    }

    private void c(Intent intent) {
        Intent intent2 = new Intent(this, MainActivity.class);
        intent2.putExtras(intent.getExtras());
        intent2.addFlags(131072);
        startActivity(intent2);
        finish();
    }

    private void d(Intent intent) {
        ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("files");
        if (stringArrayListExtra != null) {
            Iterator<String> it = stringArrayListExtra.iterator();
            while (it.hasNext()) {
                this.r.a(new File(it.next()));
            }
        }
        ArrayList<String> stringArrayListExtra2 = intent.getStringArrayListExtra("virtual");
        if (stringArrayListExtra2 != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(this.r);
            Iterator<String> it2 = stringArrayListExtra2.iterator();
            while (it2.hasNext()) {
                n.a().a(net.client.by.lock.b.k.b().b(it2.next()), arrayList);
            }
        }
    }

    private void l() {
        Intent intent = new Intent(this, CallActivity.class);
        intent.putExtra("id", this.r.K());
        this.r.c();
        startActivityForResult(intent, 12477);
    }

    private void m() {
        Intent intent = new Intent(this, ContactActivity.class);
        intent.putExtra("id", this.r.K());
        startActivityForResult(intent, 12477);
    }

    private void n() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("id", this.r.K());
        intent.putExtra("mail", true);
        intent.addFlags(131072);
        startActivity(intent);
        finish();
    }

    private void o() {
        startActivityForResult(new Intent(this, SelectFilesActivity.class), 17);
    }

    private void p() {
        this.x.b(true);
    }

    private void q() {
        Intent intent = new Intent(this, ValidateActivity.class);
        intent.putExtra("id", this.r.K());
        startActivityForResult(intent, 12477);
    }

    public c i() {
        return this.r;
    }

    public void update(Observable observable, Object obj) {
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.equals("status") || str.equals("onlineProperty") || str.equals("verificationStateProperty")) {
                o.a(this);
            } else if (!str.equals("isConnected")) {
            } else {
                if (((Boolean) ((h) observable).a()).booleanValue() && !((Boolean) r.i().t().b()).booleanValue()) {
                    this.r.n().addObserver(this);
                    this.r.q().addObserver(this);
                } else if (!((Boolean) ((h) observable).a()).booleanValue() && ((Boolean) r.i().t().b()).booleanValue()) {
                    this.r.n().deleteObserver(this);
                    this.r.q().deleteObserver(this);
                }
            }
        } else if ((observable instanceof i) && i.c().a(Integer.valueOf(this.r.K())) == null) {
            finishActivity(12477);
            finishActivity(17);
            an.a(this, String.valueOf(this.r.G()) + " has removed.");
            finish();
        }
    }

    public int j() {
        return this.r.K();
    }

    public void k() {
        this.p = a(this.w);
    }
}
