package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import net.client.by.lock.R;

/* compiled from: MyApp */
public class PinActivity extends a {
    private static EditText f;
    private static String g = "";
    private static PinActivity h = null;
    private View[] e;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_pin);
        f = (EditText) findViewById(R.id.editText1);
        f.setKeyListener(null);
        this.e = new View[11];
        this.e[0] = findViewById(R.id.button1);
        this.e[1] = findViewById(R.id.button2);
        this.e[2] = findViewById(R.id.button3);
        this.e[3] = findViewById(R.id.button4);
        this.e[4] = findViewById(R.id.button5);
        this.e[5] = findViewById(R.id.button6);
        this.e[6] = findViewById(R.id.button7);
        this.e[7] = findViewById(R.id.button8);
        this.e[8] = findViewById(R.id.button9);
        this.e[9] = findViewById(R.id.button10);
        this.e[10] = findViewById(R.id.button11);
        for (int i = 0; i < this.e.length; i++) {
            this.e[i].setOnClickListener(new bc(i));
        }
        h = this;
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onPause() {
        if (isFinishing()) {
            h = null;
        }
        super.onPause();
    }

    public void onBackPressed() {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        intent.addCategory("android.intent.category.DEFAULT");
        startActivity(intent);
        finish();
    }
}
