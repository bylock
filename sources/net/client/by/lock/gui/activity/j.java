package net.client.by.lock.gui.activity;

import android.media.AudioManager;
import android.widget.CompoundButton;

/* compiled from: MyApp */
class j implements CompoundButton.OnCheckedChangeListener {
    private AudioManager a;

    public j(AudioManager audioManager) {
        this.a = audioManager;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.a.setStreamMute(0, z);
    }
}
