package net.client.by.lock.gui.activity;

import android.support.v7.c.a;
import android.support.v7.c.b;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import net.client.by.lock.R;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class p implements b {
    final /* synthetic */ ChatActivity a;

    private p(ChatActivity chatActivity) {
        this.a = chatActivity;
    }

    /* synthetic */ p(ChatActivity chatActivity, p pVar) {
        this(chatActivity);
    }

    @Override // android.support.v7.c.b
    public boolean a(a aVar, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.actionmod_select_all /*{ENCODED_INT: 2131165409}*/:
                this.a.x.a();
                break;
            case R.id.actionmod_copy /*{ENCODED_INT: 2131165410}*/:
                Toast.makeText(this.a.getBaseContext(), (int) R.string.copied, 1).show();
                this.a.x.b();
                aVar.b();
                break;
            case R.id.actionmod_delete /*{ENCODED_INT: 2131165411}*/:
                this.a.x.b(false);
                aVar.b();
                break;
        }
        return false;
    }

    @Override // android.support.v7.c.b
    public boolean a(a aVar, Menu menu) {
        this.a.getMenuInflater().inflate(R.menu.actionmod_chat, menu);
        return true;
    }

    @Override // android.support.v7.c.b
    public void a(a aVar) {
        if (aVar.equals(this.a.p)) {
            this.a.x.a(false);
        }
    }

    @Override // android.support.v7.c.b
    public boolean b(a aVar, Menu menu) {
        return false;
    }
}
