package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import group.pals.android.lib.ui.filechooser.FileChooserActivity;
import group.pals.android.lib.ui.filechooser.io.localfile.LocalFile;
import net.client.by.lock.c.m;

/* compiled from: MyApp */
class bp implements View.OnClickListener {
    final /* synthetic */ SelectFilesActivity a;

    bp(SelectFilesActivity selectFilesActivity) {
        this.a = selectFilesActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, FileChooserActivity.class);
        intent.putExtra(FileChooserActivity.c, (Parcelable) new LocalFile(m.a().b()));
        this.a.startActivityForResult(intent, 21);
    }
}
