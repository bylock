package net.client.by.lock.gui.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.c.i;
import net.client.by.lock.d.c;

/* compiled from: MyApp */
public class ValidateActivity extends a implements Observer {
    private c e;
    private Button f;
    private TextView g;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.a
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_validate);
        this.e = i.c().a(Integer.valueOf(getIntent().getExtras().getInt("id")));
        if (this.e == null) {
            finish();
            return;
        }
        ((TextView) findViewById(R.id.textView1)).setText(this.e.C());
        ((TextView) findViewById(R.id.textView2)).setText(this.e.E());
        ((TextView) findViewById(R.id.textView3)).setText(this.e.D());
        this.g = (TextView) findViewById(R.id.textView4);
        this.g.setText((CharSequence) this.e.B().a());
        ((Button) findViewById(R.id.button1)).setOnClickListener(new bx(this));
        this.f = (Button) findViewById(R.id.button2);
        this.f.setOnClickListener(new by(this));
    }

    @Override // net.client.by.lock.gui.activity.a
    public void onResume() {
        super.onResume();
        this.e.x().addObserver(this);
        c();
    }

    @Override // net.client.by.lock.gui.activity.a
    public void onPause() {
        this.e.x().deleteObserver(this);
        super.onPause();
    }

    public void update(Observable observable, Object obj) {
        if (!(obj instanceof String)) {
            return;
        }
        if (((String) obj).equals("verificationStateProperty")) {
            c();
        } else {
            b.e(this.d, "unknown update material#2");
        }
    }

    private void c() {
        String str = (String) this.e.x().a();
        if (str.equals("NK")) {
            this.f.setVisibility(8);
            this.g.setError(null);
        } else if (str.equals("KWCS")) {
            this.f.setText(R.string.invalidate);
            this.f.setVisibility(0);
            this.g.setError(null);
        } else if (str.equals("KWIS")) {
            this.f.setText(R.string.ignore);
            this.f.setVisibility(0);
            this.g.setError(getResources().getString(R.string.fake_sas));
        } else if (str.equals("KWOS")) {
            this.f.setText(R.string.validate);
            this.f.setVisibility(0);
            this.g.setError(null);
        }
    }
}
