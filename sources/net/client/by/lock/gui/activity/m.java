package net.client.by.lock.gui.activity;

import android.text.Editable;
import android.view.KeyEvent;
import android.widget.TextView;

/* compiled from: MyApp */
class m implements TextView.OnEditorActionListener {
    final /* synthetic */ ChatActivity a;

    m(ChatActivity chatActivity) {
        this.a = chatActivity;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        Editable text = this.a.s.getText();
        if (text.length() > 0) {
            this.a.r.a(text.toString());
            this.a.s.setText("");
        }
        return true;
    }
}
