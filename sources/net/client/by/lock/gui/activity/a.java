package net.client.by.lock.gui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.c.g;
import net.client.by.lock.c.m;

/* compiled from: MyApp */
public abstract class a extends Activity {
    static AtomicBoolean a = new AtomicBoolean(false);
    static AtomicBoolean b = new AtomicBoolean(false);
    static AtomicInteger c = new AtomicInteger(0);
    private static Activity e;
    private static boolean f = false;
    protected String d = "ACTIVITY";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = getClass().getName();
        b.i(this.d, "onCreate");
        g.a().a(this);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        if (this instanceof CallActivity) {
            f = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        e = this;
        b.i(this.d, "onResume");
        g.a().a(this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        b.i(this.d, "onStart");
        g.a().a(this);
        if (c.incrementAndGet() > 0) {
            a.set(false);
        }
        if (c.get() == 1 && (((this instanceof ValidateActivity) || (this instanceof PrngActivity)) && m.a().f())) {
            b.set(true);
        }
        if (b.get()) {
            Intent intent = new Intent(this, PinActivity.class);
            intent.addFlags(131072);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        b.i(this.d, "onStop");
        super.onStop();
        if (c.decrementAndGet() < 1) {
            a.set(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        b.i(this.d, "onPause");
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        if (isFinishing()) {
            b.i(getClass().getName(), "onPause finishing");
            if (this instanceof CallActivity) {
                f = false;
            }
        }
        super.onPause();
        e = null;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        b.i(this.d, "onDestroy");
        if (this instanceof CallActivity) {
            f = false;
        }
    }

    public static boolean a() {
        return a.get();
    }

    public static boolean b() {
        return f;
    }
}
