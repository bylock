package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.a.a;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.R;
import net.client.by.lock.c.i;
import net.client.by.lock.d.c;
import net.client.by.lock.d.s;
import net.client.by.lock.f.g;

/* compiled from: MyApp */
public class SelectFriendsActivity extends q {
    private ArrayList o;
    private LinearLayout p;
    private g r;
    private ArrayList s;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_select_friends);
        this.p = (LinearLayout) findViewById(R.id.linearLayout1);
        this.o = new ArrayList();
        this.s = new ArrayList();
        ArrayList<Integer> integerArrayListExtra = getIntent().getIntegerArrayListExtra("mSelectedFriends");
        if (integerArrayListExtra != null) {
            this.s.addAll(integerArrayListExtra);
        }
        this.r = i.c().b().e();
        bs bsVar = new bs(this);
        bt btVar = new bt(this);
        for (int i = 0; i < this.r.size(); i++) {
            s sVar = (s) this.r.get(i);
            CheckedTextView checkedTextView = (CheckedTextView) CheckedTextView.inflate(this, R.layout.item_select_group, null);
            CheckedTextView checkedTextView2 = (CheckedTextView) CheckedTextView.inflate(this, R.layout.item_select_friend, null);
            checkedTextView.setText((CharSequence) ((net.client.by.lock.d.g) sVar.d()).b().a());
            checkedTextView2.setText("Select All");
            checkedTextView2.setTag(sVar);
            checkedTextView2.setOnClickListener(btVar);
            this.o.add(checkedTextView);
            this.o.add(checkedTextView2);
            for (int i2 = 0; i2 < sVar.e().size(); i2++) {
                c cVar = (c) ((s) sVar.e().get(i2)).d();
                CheckedTextView checkedTextView3 = (CheckedTextView) CheckedTextView.inflate(this, R.layout.item_select_friend, null);
                checkedTextView3.setText(cVar.G());
                checkedTextView3.setTag(cVar);
                if (this.s.contains(Integer.valueOf(cVar.K()))) {
                    checkedTextView3.setChecked(true);
                }
                checkedTextView3.setOnClickListener(bsVar);
                this.o.add(checkedTextView3);
            }
        }
        a f = f();
        f.b(true);
        f.d(true);
        f.c(true);
        f.a("Select Friends");
        f.a(17170445);
    }

    /* access modifiers changed from: protected */
    public void a(c cVar, boolean z) {
        if (!z) {
            this.s.remove(Integer.valueOf(cVar.K()));
        } else if (!this.s.contains(Integer.valueOf(cVar.K()))) {
            this.s.add(Integer.valueOf(cVar.K()));
        }
        Iterator it = this.o.iterator();
        while (it.hasNext()) {
            CheckedTextView checkedTextView = (CheckedTextView) it.next();
            Object tag = checkedTextView.getTag();
            if (tag != null && (tag instanceof c) && cVar.equals(tag)) {
                checkedTextView.setChecked(z);
            }
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onResume() {
        super.onResume();
        Iterator it = this.o.iterator();
        while (it.hasNext()) {
            this.p.addView((View) it.next());
        }
    }

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onPause() {
        super.onPause();
        this.p.removeAllViews();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_select_friends, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 16908332:
                finish();
                return true;
            case R.id.menu_done /*{ENCODED_INT: 2131165408}*/:
                Intent intent = new Intent();
                intent.putIntegerArrayListExtra("selected", this.s);
                setResult(-1, intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }
}
