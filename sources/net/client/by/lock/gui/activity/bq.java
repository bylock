package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.view.View;

/* compiled from: MyApp */
class bq implements View.OnClickListener {
    final /* synthetic */ SelectFilesActivity a;

    bq(SelectFilesActivity selectFilesActivity) {
        this.a = selectFilesActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction("android.intent.action.GET_CONTENT");
        this.a.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 19);
    }
}
