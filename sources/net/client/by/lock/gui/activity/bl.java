package net.client.by.lock.gui.activity;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: MyApp */
class bl implements View.OnTouchListener {
    final /* synthetic */ RegisterActivity a;
    private final /* synthetic */ View b;

    bl(RegisterActivity registerActivity, View view) {
        this.a = registerActivity;
        this.b = view;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        RegisterActivity.a(this.a).hideSoftInputFromWindow(this.b.getWindowToken(), 0);
        return false;
    }
}
