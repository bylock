package net.client.by.lock.gui.activity;

import android.view.View;
import net.client.by.lock.c.m;

/* compiled from: MyApp */
class bc implements View.OnClickListener {
    private int a;

    public bc(int i) {
        this.a = i;
    }

    public void onClick(View view) {
        switch (this.a) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                int i = (this.a + 1) % 10;
                PinActivity.a(String.valueOf(PinActivity.c()) + i);
                PinActivity.d().append(new StringBuilder(String.valueOf(i)).toString());
                if (PinActivity.c().length() == 1) {
                    PinActivity.d().setError(null);
                }
                if (PinActivity.c().length() == 4) {
                    if (m.a().g().equals(PinActivity.c())) {
                        a.b.set(false);
                        PinActivity.e().finish();
                    } else {
                        PinActivity.d().setError("Wrong Pin");
                    }
                    PinActivity.d().setText("");
                    PinActivity.a("");
                    return;
                }
                return;
            case 10:
                if (PinActivity.c().length() > 0) {
                    PinActivity.a(PinActivity.c().substring(0, PinActivity.c().length() - 1));
                    PinActivity.d().setText(PinActivity.c());
                    return;
                }
                return;
            default:
                return;
        }
    }
}
