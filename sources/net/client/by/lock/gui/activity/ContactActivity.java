package net.client.by.lock.gui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import group.pals.android.lib.ui.filechooser.FileChooserActivity;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.R;
import net.client.by.lock.b;
import net.client.by.lock.c.h;
import net.client.by.lock.c.i;
import net.client.by.lock.d.c;
import net.client.by.lock.d.g;
import net.client.by.lock.gui.b.an;

/* compiled from: MyApp */
public class ContactActivity extends q implements Observer {
    private c o;
    private ImageView p;
    private TextView r;
    private TextView s;
    private TextView t;
    private TextView u;

    /* access modifiers changed from: protected */
    @Override // net.client.by.lock.gui.activity.q, android.support.v7.a.f, android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_contact);
        this.o = i.c().a(Integer.valueOf(getIntent().getExtras().getInt("id")));
        if (this.o == null) {
            finish();
            return;
        }
        this.p = (ImageView) findViewById(R.id.contact_image);
        this.r = (TextView) findViewById(R.id.contact_username);
        this.s = (TextView) findViewById(R.id.contact_name);
        this.t = (TextView) findViewById(R.id.contact_status);
        this.u = (TextView) findViewById(R.id.contact_message);
        this.r.setText(this.o.C());
        this.s.setText(this.o.E());
        this.u.setText(this.o.s());
        String str = "";
        Iterator it = this.o.a().iterator();
        while (it.hasNext()) {
            str = String.valueOf(str) + ((String) ((g) i.c().b(((Integer) it.next()).intValue()).d()).b().a()) + ", ";
        }
        if (str.length() > 2) {
            str.substring(0, str.length() - 2);
        }
        f().b(true);
        f().a("");
    }

    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onResume() {
        super.onResume();
        this.o.r().addObserver(this);
        i();
    }

    @Override // net.client.by.lock.gui.activity.q, android.support.v4.app.j
    public void onPause() {
        this.o.r().deleteObserver(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onActivityResult(int i, int i2, Intent intent) {
        Uri data;
        if (i2 == -1) {
            switch (i) {
                case 5:
                    Iterator it = ((ArrayList) intent.getSerializableExtra(FileChooserActivity.n)).iterator();
                    while (it.hasNext()) {
                        h.a().a(this.o, (File) it.next());
                    }
                    return;
                case 10:
                    if (intent != null && intent.getData() != null && (data = intent.getData()) != null) {
                        Cursor query = getContentResolver().query(data, new String[]{"_data"}, null, null, null);
                        query.moveToFirst();
                        String string = query.getString(0);
                        query.close();
                        File file = new File(string);
                        an.a(this, file.getPath());
                        h.a().a(this.o, file);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void i() {
        switch (((Integer) this.o.r().a()).intValue()) {
            case 0:
                this.t.setText(R.string.status_offline);
                this.p.setImageResource(R.drawable.big_online_0);
                return;
            case 1:
                this.t.setText(R.string.status_busy);
                this.p.setImageResource(R.drawable.big_online_1);
                return;
            case 2:
                this.t.setText(R.string.status_away);
                this.p.setImageResource(R.drawable.big_online_2);
                return;
            case 3:
                this.t.setText(R.string.status_available);
                this.p.setImageResource(R.drawable.big_online_3);
                return;
            default:
                return;
        }
    }

    public void update(Observable observable, Object obj) {
        b.e(this.q, "updated");
        if (!(obj instanceof String)) {
            return;
        }
        if (((String) obj).equals("status")) {
            i();
        } else {
            b.e(this.q, "unknown update material#2");
        }
    }
}
