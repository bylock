package net.client.by.lock.gui.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import net.client.by.lock.c.g;
import net.client.by.lock.gui.activity.ReadIMailActivity;
import net.client.by.lock.gui.activity.ReadOMailActivity;

/* compiled from: MyApp */
public class l implements View.OnClickListener {
    final /* synthetic */ k a;

    protected l(k kVar) {
        this.a = kVar;
    }

    public void onClick(View view) {
        Class cls;
        Context b = g.a().b();
        if (b == null) {
            b = view.getContext();
        }
        if (this.a.k instanceof net.client.by.lock.d.l) {
            cls = ReadIMailActivity.class;
        } else {
            cls = ReadOMailActivity.class;
        }
        Intent intent = new Intent(b, cls);
        intent.putExtra("id", this.a.k.E().K());
        intent.putExtra("mailId", this.a.k.i());
        ((Activity) b).startActivityForResult(intent, 22);
    }
}
