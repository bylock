package net.client.by.lock.gui.a;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.b.h;

/* compiled from: MyApp */
public class m extends a {
    private h j;

    public m(h hVar) {
        super(hVar);
        this.j = hVar;
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(Context context) {
        int i;
        boolean z = ((h) this.c).l().size() == 0;
        if (z) {
            this.a = View.inflate(context, R.layout.item_mail_attachment, null);
        } else {
            this.a = TextView.inflate(context, R.layout.bubble, null);
            ((TextView) this.a.findViewById(R.id.chat_chat_right)).setText("File");
            ((ImageView) this.a.findViewById(R.id.chat_icon_right)).setImageResource(R.drawable.noti_file_white);
            this.a.findViewById(R.id.left).setVisibility(8);
            ((TextView) this.a.findViewById(R.id.chat_schat_right)).setText(this.j.e());
        }
        if (((String) this.j.F().a()).equals("COMPLETED")) {
            a();
            this.a.findViewById(R.id.chat_bar).setVisibility(8);
            a(8);
        } else {
            ((ProgressBar) this.a.findViewById(R.id.chat_bar)).setProgress(this.f);
        }
        View view = this.a;
        if (z) {
            i = R.id.file_name;
        } else {
            i = R.id.right;
        }
        view.findViewById(i).setOnClickListener(new n(this));
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(String str) {
    }
}
