package net.client.by.lock.gui.a;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Observer;
import java.util.TimeZone;
import net.client.by.lock.R;
import net.client.by.lock.d.k;
import net.client.by.lock.f.h;

/* compiled from: MyApp */
public abstract class a implements View.OnClickListener, View.OnLongClickListener, Checkable {
    protected View a = null;
    protected CheckBox b = null;
    protected k c = null;
    protected h d;
    protected h e;
    protected int f;
    protected boolean g = false;
    protected boolean h = false;
    protected Observer i = new b(this);
    private ProgressBar j = null;
    private boolean k = false;
    private d l;

    /* access modifiers changed from: protected */
    public abstract void a(Context context);

    public abstract void a(String str);

    public a(k kVar) {
        this.c = kVar;
        this.d = new h("", "time");
        this.e = new h(0, "barVisibility");
        this.f = 0;
        this.d.addObserver(this.i);
        this.e.addObserver(this.i);
    }

    /* access modifiers changed from: protected */
    public void b(Context context) {
    }

    public final View a(Context context, boolean z) {
        View findViewById;
        if (this.a == null) {
            a(context);
            this.j = (ProgressBar) this.a.findViewById(R.id.chat_bar);
            this.b = (CheckBox) this.a.findViewById(R.id.chat_check);
            this.a.setOnLongClickListener(this);
            this.a.setOnClickListener(this);
        } else {
            b(context);
        }
        this.k = z;
        if (!(this.a == null || (findViewById = this.a.findViewById(R.id.chat_hour)) == null || !(findViewById instanceof TextView))) {
            TextView textView = (TextView) findViewById;
            if (z) {
                String B = this.c.B();
                synchronized (this.d) {
                    if (!B.matches("[0-9]{2}[.][0-9]{2}[.][0-9]{4} [0-9]{2}[:][0-9]{2}") || textView == null || B.equals("01.01.2970 00:00")) {
                        textView.setVisibility(8);
                    } else {
                        textView.setVisibility(0);
                    }
                }
            } else {
                textView.setVisibility(8);
            }
        }
        a(this.h);
        this.a.setBackgroundColor(this.a.getContext().getResources().getColor(this.g ? R.color.Orange : R.color.WhiteSmoke));
        return this.a;
    }

    public void a() {
        String B = this.c.B();
        synchronized (this.d) {
            if (B.matches("[0-9]{2}[.][0-9]{2}[.][0-9]{4} [0-9]{2}[:][0-9]{2}") && !((String) this.d.a()).equals(B)) {
                this.d.a(B);
            }
        }
    }

    public void a(int i2) {
        this.e.a(Integer.valueOf(i2));
    }

    public void b(int i2) {
        this.f = i2;
        if (this.a != null && this.j != null) {
            this.j.setProgress(i2);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        View findViewById;
        String str2;
        if (this.a == null) {
            return;
        }
        if (str.equals("time")) {
            String str3 = (String) this.d.a();
            View findViewById2 = this.a.findViewById(R.id.chat_hour);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
            try {
                simpleDateFormat.setTimeZone(TimeZone.getDefault());
                str2 = simpleDateFormat.format(simpleDateFormat.parse(str3));
            } catch (Exception e2) {
                e2.printStackTrace();
                str2 = str3;
            }
            if (findViewById2 != null && (findViewById2 instanceof TextView)) {
                TextView textView = (TextView) findViewById2;
                if (str2.matches("[0-9]{2}[.][0-9]{2}[.][0-9]{4} [0-9]{2}[:][0-9]{2}") && !str2.equals("01.01.2970 00:00")) {
                    textView.setText(str2);
                }
                textView.setVisibility(this.k ? 0 : 8);
            }
        } else if (str.equals("barVisibility") && (findViewById = this.a.findViewById(R.id.chat_bar)) != null) {
            findViewById.setVisibility(((Integer) this.e.a()).intValue());
        }
    }

    public synchronized boolean isChecked() {
        return this.g;
    }

    public synchronized void setChecked(boolean z) {
        this.g = z;
        if (this.a != null) {
            this.a.setBackgroundColor(this.a.getContext().getResources().getColor(this.g ? R.color.Orange : R.color.WhiteSmoke));
        }
        if (this.b != null) {
            this.b.setChecked(z);
        }
    }

    public synchronized void toggle() {
        setChecked(!this.g);
    }

    public synchronized boolean onLongClick(View view) {
        if (!this.h) {
            if (this.l != null) {
                this.l.a(true);
            }
            setChecked(true);
        }
        return true;
    }

    public synchronized void onClick(View view) {
        if (this.h) {
            setChecked(!this.g);
        }
    }

    public synchronized void a(boolean z) {
        int i2 = 0;
        synchronized (this) {
            if (this.h && !z) {
                setChecked(false);
            }
            this.h = z;
            if (this.a != null) {
                this.a.setHapticFeedbackEnabled(z);
                this.a.setSoundEffectsEnabled(z);
            }
            if (z) {
                setChecked(this.g);
            }
            if (this.b != null) {
                CheckBox checkBox = this.b;
                if (!z) {
                    i2 = 8;
                }
                checkBox.setVisibility(i2);
            }
        }
    }

    public void a(d dVar) {
        this.l = dVar;
    }
}
