package net.client.by.lock.gui.a;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.d.l;

/* compiled from: MyApp */
public class j extends k {
    l j;

    public j(l lVar) {
        super(lVar);
        this.j = lVar;
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(Context context) {
        this.a = View.inflate(context, R.layout.bubble, null);
        this.a.findViewById(R.id.chat_bar).setVisibility(8);
        this.a.findViewById(R.id.right).setVisibility(8);
        ((TextView) this.a.findViewById(R.id.chat_schat_left)).setText("read");
        ((ImageView) this.a.findViewById(R.id.chat_icon_left)).setImageResource(R.drawable.noti_mail_blue);
        TextView textView = (TextView) this.a.findViewById(R.id.chat_chat_left);
        textView.setText(this.j.g());
        if (this.j.e() || ((String) this.j.F().a()).equals("COMPLETED")) {
            textView.setTypeface(null, 0);
        } else {
            textView.setTypeface(null, 1);
        }
        a();
        this.a.findViewById(R.id.left).setOnClickListener(this.l);
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(String str) {
        if (this.a != null && str.equals("stateProperty")) {
            TextView textView = (TextView) this.a.findViewById(R.id.chat_schat_left);
            if (((String) this.j.F().a()).equals("COMPLETED")) {
                textView.setTypeface(null, 0);
            } else {
                textView.setTypeface(null, 1);
            }
        }
    }
}
