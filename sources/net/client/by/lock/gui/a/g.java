package net.client.by.lock.gui.a;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.d.b;

/* compiled from: MyApp */
public class g extends a {
    private b j;
    private boolean k;

    public g(b bVar) {
        super(bVar);
        this.j = bVar;
        this.k = bVar.D();
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(Context context) {
        this.a = View.inflate(context, R.layout.bubble_text, null);
        this.a.findViewById(this.k ? R.id.left : R.id.right).setVisibility(8);
        this.a.findViewById(R.id.chat_bar).setVisibility(8);
        if (!this.k) {
            a();
            a(8);
        }
        ((TextView) this.a.findViewById(this.k ? R.id.chat_chat_right : R.id.chat_chat_left)).setText(this.j.b());
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(String str) {
    }
}
