package net.client.by.lock.gui.a;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import java.io.File;

/* compiled from: MyApp */
class i implements View.OnClickListener {
    final /* synthetic */ h a;

    i(h hVar) {
        this.a = hVar;
    }

    public void onClick(View view) {
        Context context = view.getContext();
        if (((String) this.a.j.F().a()).equals("COMPLETED")) {
            File j = this.a.j.j();
            if (j == null || !j.exists()) {
                Toast.makeText(context, "File cannot be found", 0).show();
                return;
            }
            try {
                context.startActivity(new Intent().setDataAndType(Uri.fromFile(j), MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(j.toURI().toString()))).setAction("android.intent.action.VIEW"));
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context, "No handler for this type of file.", 1).show();
            }
        } else if (((String) this.a.j.F().a()).equals("IN PROGRESS")) {
            Toast.makeText(context, "File is still being downloaded", 0).show();
        } else {
            Toast.makeText(context, "File download has been started", 0).show();
            this.a.j.l();
        }
    }
}
