package net.client.by.lock.gui.a;

import java.util.Observable;
import java.util.Observer;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class b implements Observer {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public void update(Observable observable, Object obj) {
        if (this.a.a != null) {
            this.a.a.post(new c(this, obj));
        }
    }
}
