package net.client.by.lock.gui.a;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.b.g;

/* compiled from: MyApp */
public class h extends a {
    private g j;

    public h(g gVar) {
        super(gVar);
        this.j = gVar;
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(Context context) {
        this.a = View.inflate(context, R.layout.bubble, null);
        ((TextView) this.a.findViewById(R.id.chat_chat_left)).setText("File");
        ((ImageView) this.a.findViewById(R.id.chat_icon_left)).setImageResource(R.drawable.noti_file_blue);
        this.a.findViewById(R.id.right).setVisibility(8);
        ((TextView) this.a.findViewById(R.id.chat_schat_left)).setText(this.j.e());
        a();
        if (((String) this.j.F().a()).equals("COMPLETED")) {
            this.a.findViewById(R.id.chat_bar).setVisibility(8);
            a(8);
        } else {
            ((ProgressBar) this.a.findViewById(R.id.chat_bar)).setProgress(this.f);
        }
        this.a.findViewById(R.id.left).setOnClickListener(new i(this));
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(String str) {
    }
}
