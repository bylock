package net.client.by.lock.gui.a;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.a.c;

/* compiled from: MyApp */
public class e extends a {
    private c j;
    private boolean k;

    public e(c cVar) {
        super(cVar);
        this.j = cVar;
        cVar.F().addObserver(this.i);
        cVar.b().addObserver(this.i);
        this.k = cVar.D();
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(Context context) {
        this.a = View.inflate(context, R.layout.bubble, null);
        this.a.findViewById(this.k ? R.id.left : R.id.right).setVisibility(8);
        ((ImageView) this.a.findViewById(this.k ? R.id.chat_icon_right : R.id.chat_icon_left)).setImageResource(this.k ? R.drawable.noti_call_white : R.drawable.noti_call_blue);
        this.a.findViewById(R.id.chat_bar).setVisibility(8);
        ((TextView) this.a.findViewById(this.k ? R.id.chat_chat_right : R.id.chat_chat_left)).setText("Voice Call");
        if (this.k) {
            this.j.C().addObserver(new f(this));
        } else {
            a();
        }
        TextView textView = (TextView) this.a.findViewById(this.k ? R.id.chat_schat_left : R.id.chat_schat_right);
        if (textView != null) {
            textView.setText((CharSequence) this.j.b().a());
        }
        b();
    }

    private void b() {
        String str = (String) this.j.F().a();
        TextView textView = (TextView) this.a.findViewById(this.k ? R.id.chat_schat_right : R.id.chat_schat_left);
        if (str.equals("MISSED")) {
            textView.setText("(Missed)");
        } else if (str.equals("REJECTED")) {
            textView.setText("(Rejected)");
        } else if (str.equals("CALLEE BUSY") || str.equals("CALLER BUSY")) {
            textView.setText("(Busy)");
        } else if (str.equals("CANCELED")) {
            textView.setText("(Cancelled)");
        } else if (str.equals("SOME ERROR")) {
            textView.setText("Error");
        } else if (str.equals("CLOSED")) {
            textView.setText("(Ended)");
        } else {
            textView.setText("(Awaiting)");
        }
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(String str) {
        if (this.a == null) {
            return;
        }
        if (str.equals("seconds")) {
            TextView textView = (TextView) this.a.findViewById(this.k ? R.id.chat_schat_left : R.id.chat_schat_right);
            if (textView != null) {
                textView.setText((CharSequence) this.j.b().a());
            }
        } else if (str.equals("stateProperty")) {
            b();
        }
    }
}
