package net.client.by.lock.gui.a;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import net.client.by.lock.R;
import net.client.by.lock.d.n;

/* compiled from: MyApp */
public class o extends k {
    private n j;

    public o(n nVar) {
        super(nVar);
        this.j = nVar;
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(Context context) {
        this.a = View.inflate(context, R.layout.bubble, null);
        this.a.findViewById(R.id.left).setVisibility(8);
        if (((String) this.j.F().a()).equals("COMPLETED")) {
            this.a.findViewById(R.id.chat_bar).setVisibility(8);
            a();
        }
        ((TextView) this.a.findViewById(R.id.chat_schat_right)).setText("read");
        ((ImageView) this.a.findViewById(R.id.chat_icon_right)).setImageResource(R.drawable.noti_mail_white);
        ((TextView) this.a.findViewById(R.id.chat_chat_right)).setText(this.j.g());
        this.a.findViewById(R.id.right).setOnClickListener(this.l);
    }

    @Override // net.client.by.lock.gui.a.a
    public void a(String str) {
        if (((String) this.j.F().a()).equals("COMPLETED")) {
            this.a.findViewById(R.id.chat_bar).setVisibility(8);
            a();
        }
    }
}
