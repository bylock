package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.view.View;
import android.widget.TextView;
import net.client.by.lock.d.r;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class w implements View.OnClickListener {
    private final /* synthetic */ TextView a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ TextView c;
    private final /* synthetic */ TextView d;
    private final /* synthetic */ Dialog e;

    w(TextView textView, TextView textView2, TextView textView3, TextView textView4, Dialog dialog) {
        this.a = textView;
        this.b = textView2;
        this.c = textView3;
        this.d = textView4;
        this.e = dialog;
    }

    public void onClick(View view) {
        int i = 0;
        if (view != this.a) {
            if (view == this.b) {
                i = 1;
            } else if (view == this.c) {
                i = 2;
            } else if (view == this.d) {
                i = 3;
            }
        }
        r.i().b(Integer.valueOf(i));
        this.e.dismiss();
    }
}
