package net.client.by.lock.gui.b;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import net.client.by.lock.R;
import net.client.by.lock.d.a;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class ai extends BaseAdapter {
    private Context a;
    private ArrayList b = new ArrayList();

    public ai(Context context) {
        this.a = context;
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        aj ajVar;
        a aVar = (a) this.b.get(i);
        if (view == null) {
            view = View.inflate(this.a, R.layout.item_mail_attachment, null);
            aj ajVar2 = new aj(null);
            view.findViewById(R.id.chat_bar).setVisibility(8);
            view.findViewById(R.id.cancel).setVisibility(8);
            ajVar2.a = (TextView) view.findViewById(R.id.file_name);
            ajVar2.b = (TextView) view.findViewById(R.id.size);
            view.setTag(ajVar2);
            ajVar = ajVar2;
        } else {
            ajVar = (aj) view.getTag();
        }
        ajVar.a.setText(aVar.e());
        ajVar.b.setText(j.a((long) aVar.f().intValue()));
        return view;
    }

    public boolean hasStableIds() {
        return true;
    }

    public void a(ArrayList arrayList) {
        this.b = arrayList;
        notifyDataSetChanged();
    }
}
