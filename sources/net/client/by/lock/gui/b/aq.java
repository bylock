package net.client.by.lock.gui.b;

import android.view.View;
import net.client.by.lock.b.a;

/* compiled from: MyApp */
class aq implements View.OnClickListener {
    final /* synthetic */ ao a;
    private a b;

    private aq(ao aoVar) {
        this.a = aoVar;
    }

    /* synthetic */ aq(ao aoVar, aq aqVar) {
        this(aoVar);
    }

    public void onClick(View view) {
        if (this.a.b.contains(this.b.b())) {
            this.a.b.remove(this.b.b());
        } else {
            this.a.b.add(this.b.b());
        }
        this.a.notifyDataSetChanged();
    }

    public void a(a aVar) {
        this.b = aVar;
    }
}
