package net.client.by.lock.gui.b;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import net.client.by.lock.R;
import net.client.by.lock.b;

/* compiled from: MyApp */
public class an {
    static String a = "TOST";

    public static void a(Context context, String str, Boolean bool) {
        try {
            Toast toast = new Toast(context);
            View inflate = View.inflate(context, R.layout.toast, null);
            ((TextView) inflate.findViewById(R.id.textView1)).setText(str);
            toast.setView(inflate);
            if (bool.booleanValue()) {
                toast.setDuration(1);
            } else {
                toast.setDuration(0);
            }
            toast.setGravity(17, 0, 0);
            toast.show();
        } catch (Exception e) {
            b.e(a, e.toString());
        }
    }

    public static void a(Context context, String str) {
        a(context, str, true);
    }

    public static void b(Context context, String str) {
        a(context, str, false);
    }
}
