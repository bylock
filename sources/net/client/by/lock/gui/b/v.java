package net.client.by.lock.gui.b;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import net.client.by.lock.f.k;
import net.client.by.lock.f.l;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class v implements TextWatcher {
    private final /* synthetic */ Button a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ String[] c;

    v(Button button, EditText editText, String[] strArr) {
        this.a = button;
        this.b = editText;
        this.c = strArr;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        l a2 = k.a(editable.toString());
        if (a2 == l.SECURE) {
            this.a.setEnabled(a2 == l.SECURE);
            this.b.setError(null);
            return;
        }
        this.b.setError(this.c[a2.ordinal()]);
    }
}
