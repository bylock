package net.client.by.lock.gui.b;

import android.media.AudioRecord;
import android.os.AsyncTask;
import net.client.by.lock.R;
import net.client.by.lock.c.m;
import net.client.by.lock.f.c;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class aa extends AsyncTask {
    final /* synthetic */ x a;

    private aa(x xVar) {
        this.a = xVar;
    }

    /* synthetic */ aa(x xVar, aa aaVar) {
        this(xVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Integer doInBackground(Void... voidArr) {
        int[] iArr = {8000, 16000, 32000, 48000};
        short[] sArr = new short[1600];
        int c = c.c();
        for (int i : iArr) {
            publishProgress(Integer.valueOf(i));
            try {
                this.a.e = new AudioRecord(1, i, 16, c, AudioRecord.getMinBufferSize(i, 16, c) * 4);
                this.a.e.startRecording();
                int read = this.a.e.read(sArr, 0, sArr.length);
                this.a.e.stop();
                this.a.e.release();
                this.a.e = null;
                for (int i2 = 0; i2 < read; i2++) {
                    if (sArr[i2] != 0) {
                        return Integer.valueOf(i);
                    }
                }
                continue;
            } catch (IllegalArgumentException e) {
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        this.a.a.setText(new StringBuilder().append(numArr[0]).toString());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Integer num) {
        this.a.d.setVisibility(8);
        if (num.intValue() > 0) {
            this.a.b.setText(R.string.fr_found);
            this.a.a.setText(new StringBuilder().append(num).toString());
            m.a().a(num.intValue());
            this.a.a.postDelayed(new ab(this), 2600);
            return;
        }
        this.a.f.setVisibility(0);
        this.a.g.setVisibility(0);
        this.a.a.setVisibility(8);
        this.a.b.setText(R.string.fr_corrupt);
        m.a().a(8000);
    }
}
