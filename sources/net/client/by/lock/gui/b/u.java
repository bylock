package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.view.View;
import android.widget.EditText;
import net.client.by.lock.d.r;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class u implements View.OnClickListener {
    private final /* synthetic */ EditText a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Dialog c;

    u(EditText editText, EditText editText2, Dialog dialog) {
        this.a = editText;
        this.b = editText2;
        this.c = dialog;
    }

    public void onClick(View view) {
        String editable = this.a.getText().toString();
        if (editable.equals(this.b.getText().toString())) {
            r.i().e(editable);
            this.c.dismiss();
            return;
        }
        this.b.setError("Passwords does not match");
    }
}
