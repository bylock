package net.client.by.lock.gui.b;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

/* compiled from: MyApp */
public class am extends BaseAdapter {
    private Context a;
    private ArrayList b = new ArrayList();
    private Typeface c;

    public am(Context context) {
        this.a = context;
    }

    public boolean hasStableIds() {
        return true;
    }

    public void a(ArrayList arrayList) {
        this.b = arrayList;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView;
        if (view == null) {
            textView = new TextView(this.a);
            textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            textView.setTypeface(this.c);
        } else {
            textView = (TextView) view;
        }
        textView.setText((CharSequence) this.b.get(i));
        return textView;
    }
}
