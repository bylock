package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.view.View;
import android.widget.EditText;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class l implements View.OnClickListener {
    private final /* synthetic */ c a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Dialog c;

    l(c cVar, EditText editText, Dialog dialog) {
        this.a = cVar;
        this.b = editText;
        this.c = dialog;
    }

    public void onClick(View view) {
        this.a.d(this.b.getText().toString());
        n.a().a(this.a);
        this.c.dismiss();
    }
}
