package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.R;
import net.client.by.lock.c.i;
import net.client.by.lock.d.c;
import net.client.by.lock.d.g;
import net.client.by.lock.f.j;

/* compiled from: MyApp */
public class b {
    public static Dialog a(Context context) {
        View inflate = View.inflate(context, R.layout.dialog_change_name, null);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.button1).setOnClickListener(new c((EditText) inflate.findViewById(R.id.editText1), dialog));
        inflate.findViewById(R.id.button2).setOnClickListener(new n(dialog));
        return dialog;
    }

    public static Dialog b(Context context) {
        View inflate = View.inflate(context, R.layout.dialog_change_public_message, null);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.button1).setOnClickListener(new q((EditText) inflate.findViewById(R.id.editText1), dialog));
        inflate.findViewById(R.id.button2).setOnClickListener(new r(dialog));
        return dialog;
    }

    public static Dialog c(Context context) {
        View inflate = View.inflate(context, R.layout.dialog_change_password, null);
        Dialog dialog = new Dialog(context);
        String[] stringArray = context.getResources().getStringArray(R.array.pass_security);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.button1).setOnClickListener(new s((EditText) inflate.findViewById(R.id.editText1), (EditText) inflate.findViewById(R.id.editText2), (EditText) inflate.findViewById(R.id.editText3), stringArray, dialog));
        inflate.findViewById(R.id.button2).setOnClickListener(new t(dialog));
        return dialog;
    }

    public static Dialog d(Context context) {
        View inflate = View.inflate(context, R.layout.dialog_force_change_password, null);
        EditText editText = (EditText) inflate.findViewById(R.id.editText1);
        Button button = (Button) inflate.findViewById(R.id.button1);
        String[] stringArray = context.getResources().getStringArray(R.array.pass_security);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        button.setOnClickListener(new u(editText, (EditText) inflate.findViewById(R.id.editText2), dialog));
        editText.addTextChangedListener(new v(button, editText, stringArray));
        return dialog;
    }

    public static Dialog e(Context context) {
        View inflate = View.inflate(context, R.layout.dialog_change_status, null);
        TextView textView = (TextView) inflate.findViewById(R.id.textView1);
        TextView textView2 = (TextView) inflate.findViewById(R.id.textView2);
        TextView textView3 = (TextView) inflate.findViewById(R.id.textView3);
        TextView textView4 = (TextView) inflate.findViewById(R.id.textView4);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        w wVar = new w(textView, textView2, textView3, textView4, dialog);
        textView.setOnClickListener(wVar);
        textView2.setOnClickListener(wVar);
        textView3.setOnClickListener(wVar);
        textView4.setOnClickListener(wVar);
        return dialog;
    }

    public static Dialog a(Context context, int i) {
        View inflate = View.inflate(context, R.layout.dialog_in_progress, null);
        ((TextView) inflate.findViewById(R.id.textView1)).setText(i);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.setContentView(inflate);
        dialog.setCancelable(false);
        return dialog;
    }

    public static Dialog f(Context context) {
        View inflate = View.inflate(context, R.layout.dialog_change_pin, null);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.button1).setOnClickListener(new d((EditText) inflate.findViewById(R.id.editText1), (EditText) inflate.findViewById(R.id.editText2), (EditText) inflate.findViewById(R.id.editText3), dialog));
        inflate.findViewById(R.id.button2).setOnClickListener(new e(dialog));
        return dialog;
    }

    public static Dialog a(Context context, File file) {
        View inflate = View.inflate(context, R.layout.dialog_delete_file, null);
        ((TextView) inflate.findViewById(R.id.textView1)).setText(String.valueOf(file.getName()) + " " + j.a(file.length()));
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.button1).setOnClickListener(new f(file, dialog));
        inflate.findViewById(R.id.button2).setOnClickListener(new g(dialog));
        return dialog;
    }

    public static Dialog a(Context context, c cVar, g gVar) {
        View inflate = View.inflate(context, R.layout.dialog_friend_menu, null);
        ((TextView) inflate.findViewById(R.id.name)).setText(cVar.G());
        if (gVar.a() == 0) {
            inflate.findViewById(R.id.textView4).setVisibility(8);
        }
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.textView1).setOnClickListener(new h(cVar, dialog));
        inflate.findViewById(R.id.textView2).setOnClickListener(new i(cVar, dialog));
        inflate.findViewById(R.id.textView3).setOnClickListener(new j(cVar, gVar, dialog));
        inflate.findViewById(R.id.textView4).setOnClickListener(new k(cVar, gVar, dialog));
        return dialog;
    }

    public static Dialog a(Context context, c cVar) {
        View inflate = View.inflate(context, R.layout.dialog_change_name, null);
        EditText editText = (EditText) inflate.findViewById(R.id.editText1);
        editText.setText(cVar.G());
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.button1).setOnClickListener(new l(cVar, editText, dialog));
        inflate.findViewById(R.id.button2).setOnClickListener(new m(dialog));
        return dialog;
    }

    public static Dialog b(Context context, c cVar, g gVar) {
        View inflate = View.inflate(context, R.layout.dialog_move_to_group, null);
        ((TextView) inflate.findViewById(R.id.name)).setText(cVar.G());
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) inflate.findViewById(R.id.editText1);
        ArrayList arrayList = new ArrayList();
        Iterator it = i.c().e().iterator();
        while (it.hasNext()) {
            g gVar2 = (g) it.next();
            if (!(gVar2.a() == 0 || gVar.a() == gVar2.a())) {
                arrayList.add(gVar2);
            }
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(context, 17367050, arrayList);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setAdapter(arrayAdapter);
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(1);
        dialog.setContentView(inflate);
        inflate.findViewById(R.id.button1).setOnClickListener(new o(autoCompleteTextView, dialog, arrayList, cVar));
        inflate.findViewById(R.id.button2).setOnClickListener(new p(dialog));
        return dialog;
    }
}
