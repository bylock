package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.view.View;
import net.client.by.lock.d.c;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class h implements View.OnClickListener {
    private final /* synthetic */ c a;
    private final /* synthetic */ Dialog b;

    h(c cVar, Dialog dialog) {
        this.a = cVar;
        this.b = dialog;
    }

    public void onClick(View view) {
        b.a(view.getContext(), this.a).show();
        this.b.dismiss();
    }
}
