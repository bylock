package net.client.by.lock.gui.b;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import net.client.by.lock.R;
import net.client.by.lock.d.c;

/* compiled from: MyApp */
public class ak extends BaseAdapter {
    private Context a;
    private ArrayList b = new ArrayList();

    public ak(Context context) {
        this.a = context;
    }

    public boolean hasStableIds() {
        return true;
    }

    public void a(ArrayList arrayList) {
        this.b = arrayList;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        al alVar;
        c cVar = (c) this.b.get(i);
        if (view == null) {
            view = View.inflate(this.a, R.layout.item_mail_attachment, null);
            al alVar2 = new al(null);
            view.findViewById(R.id.chat_bar).setVisibility(8);
            view.findViewById(R.id.cancel).setVisibility(8);
            alVar2.a = (TextView) view.findViewById(R.id.file_name);
            alVar2.b = (TextView) view.findViewById(R.id.size);
            view.setTag(alVar2);
            alVar = alVar2;
        } else {
            alVar = (al) view.getTag();
        }
        alVar.a.setText(cVar.G());
        alVar.b.setText(cVar.C());
        return view;
    }
}
