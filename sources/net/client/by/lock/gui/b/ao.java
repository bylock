package net.client.by.lock.gui.b;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.R;
import net.client.by.lock.b.a;
import net.client.by.lock.b.k;
import net.client.by.lock.f.g;

/* compiled from: MyApp */
public class ao extends BaseAdapter implements Observer {
    private Context a;
    private ArrayList b = new ArrayList();
    private g c = k.b().a();

    public ao(Context context) {
        this.a = context;
        this.c.addObserver(this);
    }

    public int getCount() {
        return this.c.size();
    }

    public Object getItem(int i) {
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        CheckedTextView checkedTextView;
        ap apVar;
        a aVar = (a) this.c.get(i);
        if (view == null) {
            CheckedTextView checkedTextView2 = (CheckedTextView) View.inflate(this.a, R.layout.select_item, null);
            ap apVar2 = new ap(null);
            apVar2.a = new aq(this, null);
            checkedTextView2.setOnClickListener(apVar2.a);
            checkedTextView2.setTag(apVar2);
            checkedTextView = checkedTextView2;
            apVar = apVar2;
        } else {
            checkedTextView = (CheckedTextView) view;
            apVar = (ap) checkedTextView.getTag();
        }
        apVar.a.a(aVar);
        checkedTextView.setText(aVar.e());
        checkedTextView.setChecked(this.b.contains(aVar.b()));
        return checkedTextView;
    }

    public ArrayList a() {
        this.c.deleteObserver(this);
        return this.b;
    }

    public void update(Observable observable, Object obj) {
        notifyDataSetChanged();
    }
}
