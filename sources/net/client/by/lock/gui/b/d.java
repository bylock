package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import net.client.by.lock.c.m;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class d implements View.OnClickListener {
    private final /* synthetic */ EditText a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ EditText c;
    private final /* synthetic */ Dialog d;

    d(EditText editText, EditText editText2, EditText editText3, Dialog dialog) {
        this.a = editText;
        this.b = editText2;
        this.c = editText3;
        this.d = dialog;
    }

    public void onClick(View view) {
        String editable = this.a.getText().toString();
        String editable2 = this.b.getText().toString();
        String editable3 = this.c.getText().toString();
        if (!m.a().g().equals(editable)) {
            this.a.setError("pin incorrect");
        } else if (editable.equals(editable2)) {
            this.b.setError("new ping is same with previous");
        } else if (!editable2.equals(editable3) || TextUtils.isEmpty(editable2)) {
            this.c.setError("pin does not match");
        } else {
            m.a().b(editable2);
            this.d.dismiss();
        }
    }
}
