package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import net.client.by.lock.d.r;
import net.client.by.lock.f.k;
import net.client.by.lock.f.l;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class s implements View.OnClickListener {
    private final /* synthetic */ EditText a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ EditText c;
    private final /* synthetic */ String[] d;
    private final /* synthetic */ Dialog e;

    s(EditText editText, EditText editText2, EditText editText3, String[] strArr, Dialog dialog) {
        this.a = editText;
        this.b = editText2;
        this.c = editText3;
        this.d = strArr;
        this.e = dialog;
    }

    public void onClick(View view) {
        String editable = this.a.getText().toString();
        String editable2 = this.b.getText().toString();
        String editable3 = this.c.getText().toString();
        if (r.i().l().equals(editable)) {
            l a2 = k.a(editable2);
            if (a2 != l.SECURE) {
                this.b.setError(this.d[a2.ordinal()]);
            } else if (editable.equals(editable2)) {
                this.b.setError("new password is same with previous");
            } else if (!editable2.equals(editable3) || TextUtils.isEmpty(editable2)) {
                this.c.setError("password does not match");
            } else {
                r.i().e(editable2);
                this.e.dismiss();
            }
        } else {
            this.a.setError("password incorrect");
        }
    }
}
