package net.client.by.lock.gui.b;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.d.k;
import net.client.by.lock.f.g;
import net.client.by.lock.f.o;
import net.client.by.lock.gui.a.d;
import net.client.by.lock.gui.activity.ChatActivity;

/* compiled from: MyApp */
public class a extends BaseAdapter implements Observer, d {
    private ChatActivity a;
    private g b;
    private boolean c = false;
    private boolean d = false;

    public a(g gVar) {
        this.b = gVar;
        this.b.addObserver(this);
    }

    public void a(ChatActivity chatActivity) {
        this.a = chatActivity;
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        boolean z;
        k kVar = (k) this.b.get(i);
        if (i > 0) {
            String B = ((k) this.b.get(i - 1)).B();
            String B2 = kVar.B();
            z = !B.startsWith(B2.substring(0, 15)) && !"01.01.2970 00:00".startsWith(B2.substring(0, 15));
        } else {
            z = true;
        }
        kVar.A().a(this);
        return kVar.A().a(this.a, z);
    }

    public void update(Observable observable, Object obj) {
        if (obj != null && (obj instanceof k)) {
            ((k) obj).A().a(this.d);
        }
        notifyDataSetChanged();
    }

    @Override // net.client.by.lock.gui.a.d
    public void a(boolean z) {
        this.d = z;
        if (z) {
            this.a.k();
        }
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ((k) it.next()).A().a(z);
        }
    }

    public void a() {
        boolean z = false;
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ((k) it.next()).A().setChecked(!this.c);
        }
        if (!this.c) {
            z = true;
        }
        this.c = z;
    }

    public void b(boolean z) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            k kVar = (k) it.next();
            if (z || kVar.A().isChecked()) {
                kVar.z();
                it.remove();
            }
        }
        notifyDataSetChanged();
    }

    public void b() {
        Iterator it = this.b.iterator();
        String str = "";
        while (it.hasNext()) {
            k kVar = (k) it.next();
            if (kVar.A().isChecked()) {
                str = String.valueOf(str) + kVar.toString() + System.getProperty("line.separator");
            }
        }
        o.a(this.a.getSystemService("clipboard"), str, str);
    }

    public int getItemViewType(int i) {
        return -1;
    }
}
