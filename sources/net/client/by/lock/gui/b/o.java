package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import java.util.ArrayList;
import java.util.Iterator;
import net.client.by.lock.c.i;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;
import net.client.by.lock.d.g;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class o implements View.OnClickListener {
    private final /* synthetic */ AutoCompleteTextView a;
    private final /* synthetic */ Dialog b;
    private final /* synthetic */ ArrayList c;
    private final /* synthetic */ c d;

    o(AutoCompleteTextView autoCompleteTextView, Dialog dialog, ArrayList arrayList, c cVar) {
        this.a = autoCompleteTextView;
        this.b = dialog;
        this.c = arrayList;
        this.d = cVar;
    }

    public void onClick(View view) {
        int i;
        String editable = this.a.getText().toString();
        if (TextUtils.isEmpty(editable)) {
            this.b.dismiss();
            return;
        }
        Iterator it = this.c.iterator();
        while (true) {
            if (it.hasNext()) {
                g gVar = (g) it.next();
                if (gVar.toString().equals(editable)) {
                    i = gVar.a();
                    break;
                }
            } else {
                i = -1;
                break;
            }
        }
        if (i < 0) {
            n.a().a(this.d, editable);
        } else {
            i.c().a(Integer.valueOf(this.d.K()), Integer.valueOf(i));
            n.a().a(Integer.valueOf(this.d.K()), Integer.valueOf(i));
        }
        this.b.dismiss();
    }
}
