package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.content.Context;
import android.media.AudioRecord;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import net.client.by.lock.R;

/* compiled from: MyApp */
public class x extends Dialog {
    private TextView a = ((TextView) findViewById(R.id.textView3));
    private TextView b = ((TextView) findViewById(R.id.textView2));
    private aa c;
    private ProgressBar d = ((ProgressBar) findViewById(R.id.progressBar1));
    private AudioRecord e;
    private Button f = ((Button) findViewById(R.id.button1));
    private Button g = ((Button) findViewById(R.id.button2));

    public x(Context context) {
        super(context);
        setContentView(R.layout.find_frequency_dialog);
        setTitle(R.string.fr_title);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        this.f.setOnClickListener(new y(this));
        this.g.setOnClickListener(new z(this));
        this.c = new aa(this, null);
        this.c.execute(new Void[0]);
    }
}
