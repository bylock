package net.client.by.lock.gui.b;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import net.client.by.lock.b;

/* compiled from: MyApp */
public class ag implements SensorEventListener {
    private SensorManager a;
    private Sensor b;
    private ah c = null;

    public ag(Context context) {
        context.getApplicationContext();
        this.a = (SensorManager) context.getSystemService("sensor");
        this.b = this.a.getDefaultSensor(8);
    }

    public void a() {
        this.c = null;
        if (!Build.MANUFACTURER.contains("HTC") && !Build.MANUFACTURER.contains("htc") && !Build.MANUFACTURER.contains("Htc")) {
            if (this.b == null) {
                b.e("ProximityListener", "No registered proximity sensor found!");
                return;
            }
            b.e("ProximityListener", "Proximity stop");
            this.a.unregisterListener(this, this.b);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
        b.e("ProximityListener", "onAccuracyChanged");
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        boolean z = false;
        b.e("ProximityListener", "mProximitySensor.getMaximumRange(): " + this.b.getMaximumRange());
        b.e("ProximityListener", "Long/Short: " + sensorEvent.values[0]);
        if (this.c != null) {
            synchronized (this.c) {
                ah ahVar = this.c;
                if (sensorEvent.values[0] != this.b.getMaximumRange() && sensorEvent.values[0] <= 5.0f) {
                    z = true;
                }
                ahVar.a(z);
            }
        }
    }
}
