package net.client.by.lock.gui.b;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import net.client.by.lock.R;
import net.client.by.lock.c.i;
import net.client.by.lock.d.c;
import net.client.by.lock.d.r;
import net.client.by.lock.d.s;
import net.client.by.lock.f.g;
import net.client.by.lock.f.h;
import net.client.by.lock.f.j;
import net.client.by.lock.f.o;
import net.client.by.lock.gui.activity.MainActivity;

/* compiled from: MyApp */
public class ac extends BaseExpandableListAdapter implements Observer {
    public final int a = -100000;
    private LayoutInflater b = null;
    private MainActivity c;
    private int d = -1;

    private g d() {
        return i.c().a().e();
    }

    public ac(Context context) {
        this.c = (MainActivity) context;
        this.b = (LayoutInflater) context.getSystemService("layout_inflater");
        a(true);
        notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.c != null) {
            this.c.runOnUiThread(new ad(this));
        }
    }

    public void b() {
        a(true);
        a();
    }

    public void update(Observable observable, Object obj) {
        if (obj instanceof String) {
            if (((String) obj).equals("isConnected")) {
                Boolean bool = (Boolean) r.i().t().b();
                if (((Boolean) ((h) observable).a()).booleanValue() && bool != null && !bool.booleanValue()) {
                    a(false);
                    a(true);
                } else if (!((Boolean) ((h) observable).a()).booleanValue() && bool != null && !bool.booleanValue()) {
                    this.c.runOnUiThread(new ae(this));
                    a(false);
                }
            }
        } else if ((observable instanceof i) && (obj instanceof c)) {
            a((c) obj);
        }
        a();
    }

    /* renamed from: a */
    public c getChild(int i, int i2) {
        try {
            return (c) ((s) ((s) d().get(i)).e().get(i2)).d();
        } catch (IndexOutOfBoundsException e) {
            j.a("FriendAdapter", e);
            return null;
        }
    }

    public long getChildId(int i, int i2) {
        return (long) i2;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        af afVar;
        c a2 = getChild(i, i2);
        if (a2 != null) {
            if (view == null) {
                view = this.b.inflate(R.layout.flist_child_view, (ViewGroup) null);
                afVar = new af();
                afVar.g = (ImageView) view.findViewById(R.id.item_count);
                afVar.a = (TextView) view.findViewById(R.id.item_name);
                afVar.b = (TextView) view.findViewById(R.id.item_message);
                afVar.c = (ImageView) view.findViewById(R.id.missed_chat_i);
                afVar.d = (ImageView) view.findViewById(R.id.missed_call_i);
                afVar.f = (ImageView) view.findViewById(R.id.missed_mail_i);
                afVar.e = (ImageView) view.findViewById(R.id.missed_file_i);
                view.setTag(afVar);
            } else {
                afVar = (af) view.getTag();
            }
            afVar.a.setText(a2.G());
            afVar.b.setText(a2.s());
            if (a2.o() > 0) {
                afVar.c.setVisibility(0);
            } else {
                afVar.c.setVisibility(4);
            }
            if (((Boolean) a2.A().a()).booleanValue()) {
                afVar.d.setVisibility(0);
            } else {
                afVar.d.setVisibility(4);
            }
            if (((Boolean) a2.y().a()).booleanValue()) {
                afVar.e.setVisibility(0);
            } else {
                afVar.e.setVisibility(4);
            }
            if (((Boolean) a2.z().a()).booleanValue()) {
                afVar.f.setVisibility(0);
            } else {
                afVar.f.setVisibility(4);
            }
            if (((Integer) a2.r().a()).intValue() == 0) {
                afVar.a.setTextColor(this.c.getResources().getColor(R.color.Gray));
            } else {
                afVar.a.setTextColor(this.c.getResources().getColor(R.color.LightSlateGray));
            }
            switch (((Integer) a2.r().a()).intValue()) {
                case 0:
                    afVar.g.setImageResource(R.drawable.online_0);
                    break;
                case 1:
                    afVar.g.setImageResource(R.drawable.online_1);
                    break;
                case 2:
                    afVar.g.setImageResource(R.drawable.online_2);
                    break;
                case 3:
                    afVar.g.setImageResource(R.drawable.online_3);
                    break;
            }
            view.setTag(R.string.drag_tag_int, Integer.valueOf(i));
        }
        return view;
    }

    public int getChildrenCount(int i) {
        return ((s) d().get(i)).e().size();
    }

    /* renamed from: a */
    public s getGroup(int i) {
        try {
            return (s) d().get(i);
        } catch (IndexOutOfBoundsException e) {
            j.a("FriendAdapter", e);
            return null;
        }
    }

    public int getGroupCount() {
        return d().size();
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    @TargetApi(11)
    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        TextView textView;
        TextView textView2 = (TextView) view;
        if (view == null) {
            textView = (TextView) this.b.inflate(R.layout.item_group_title, (ViewGroup) null);
        } else {
            textView = textView2;
        }
        s a2 = getGroup(i);
        if (a2 == null) {
            return view;
        }
        textView.setText(String.valueOf((String) ((net.client.by.lock.d.g) a2.d()).b().a()) + " (" + ((net.client.by.lock.d.h) a2).c().a() + "/" + a2.e().size() + ")");
        textView.setSingleLine(true);
        textView.setTag(R.string.drag_tag_int, Integer.valueOf(i));
        if (o.a(11)) {
            a(textView, i);
        }
        return textView;
    }

    @TargetApi(11)
    private void a(View view, int i) {
        if (this.d == i) {
            view.setScaleY(1.2f);
            view.setScaleX(1.2f);
            view.setTranslationX(40.0f);
            return;
        }
        view.setScaleY(1.0f);
        view.setScaleX(1.0f);
        view.setTranslationX(0.0f);
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    private void a(boolean z) {
        int i = 0;
        g d2 = d();
        if (z) {
            i.c().addObserver(this);
            net.client.by.lock.c.h.a().b().addObserver(this);
            Iterator it = i.c().f().iterator();
            while (it.hasNext()) {
                a((c) it.next());
            }
            while (i < d2.size()) {
                ((net.client.by.lock.d.h) d2.get(i)).c().addObserver(this);
                ((net.client.by.lock.d.h) d2.get(i)).b().addObserver(this);
                i++;
            }
            r.i().t().addObserver(this);
            return;
        }
        i.c().deleteObserver(this);
        net.client.by.lock.c.h.a().b().deleteObserver(this);
        Iterator it2 = i.c().f().iterator();
        while (it2.hasNext()) {
            c cVar = (c) it2.next();
            cVar.p().deleteObserver(this);
            cVar.F().deleteObserver(this);
            cVar.r().deleteObserver(this);
            cVar.y().deleteObserver(this);
            cVar.z().deleteObserver(this);
            cVar.A().deleteObserver(this);
            cVar.t().deleteObserver(this);
        }
        while (i < d2.size()) {
            ((net.client.by.lock.d.h) d2.get(i)).c().deleteObserver(this);
            ((net.client.by.lock.d.h) d2.get(i)).b().deleteObserver(this);
            i++;
        }
        r.i().t().deleteObserver(this);
    }

    private void a(c cVar) {
        cVar.p().addObserver(this);
        cVar.y().addObserver(this);
        cVar.z().addObserver(this);
        cVar.F().addObserver(this);
        cVar.A().addObserver(this);
        cVar.r().addObserver(this);
        cVar.t().addObserver(this);
    }

    public void c() {
        a(false);
        a();
    }
}
