package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.view.View;
import java.io.File;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class f implements View.OnClickListener {
    private final /* synthetic */ File a;
    private final /* synthetic */ Dialog b;

    f(File file, Dialog dialog) {
        this.a = file;
        this.b = dialog;
    }

    public void onClick(View view) {
        this.a.delete();
        this.b.dismiss();
    }
}
