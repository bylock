package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.view.View;
import android.widget.EditText;
import net.client.by.lock.d.r;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class c implements View.OnClickListener {
    private final /* synthetic */ EditText a;
    private final /* synthetic */ Dialog b;

    c(EditText editText, Dialog dialog) {
        this.a = editText;
        this.b = dialog;
    }

    public void onClick(View view) {
        r.i().h(this.a.getText().toString());
        this.b.dismiss();
    }
}
