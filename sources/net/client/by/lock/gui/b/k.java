package net.client.by.lock.gui.b;

import android.app.Dialog;
import android.view.View;
import net.client.by.lock.c.n;
import net.client.by.lock.d.c;
import net.client.by.lock.d.g;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class k implements View.OnClickListener {
    private final /* synthetic */ c a;
    private final /* synthetic */ g b;
    private final /* synthetic */ Dialog c;

    k(c cVar, g gVar, Dialog dialog) {
        this.a = cVar;
        this.b = gVar;
        this.c = dialog;
    }

    public void onClick(View view) {
        n.a().b(Integer.valueOf(this.a.K()), Integer.valueOf(this.b.a()));
        this.c.dismiss();
    }
}
