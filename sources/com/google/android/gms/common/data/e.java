package com.google.android.gms.common.data;

import android.net.Uri;
import com.google.android.gms.internal.ar;
import com.google.android.gms.internal.aw;

public abstract class e {
    protected final DataHolder a;
    protected final int b;
    private final int c;

    public e(DataHolder dataHolder, int i) {
        this.a = (DataHolder) aw.a(dataHolder);
        aw.a(i >= 0 && i < dataHolder.g());
        this.b = i;
        this.c = dataHolder.a(this.b);
    }

    public boolean a_(String str) {
        return this.a.a(str);
    }

    /* access modifiers changed from: protected */
    public long b(String str) {
        return this.a.a(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public int c(String str) {
        return this.a.b(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public boolean d(String str) {
        return this.a.d(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public String e(String str) {
        return this.a.c(str, this.b, this.c);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return ar.a(Integer.valueOf(eVar.b), Integer.valueOf(this.b)) && ar.a(Integer.valueOf(eVar.c), Integer.valueOf(this.c)) && eVar.a == this.a;
    }

    /* access modifiers changed from: protected */
    public byte[] f(String str) {
        return this.a.e(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public Uri g(String str) {
        return this.a.f(str, this.b, this.c);
    }

    /* access modifiers changed from: protected */
    public boolean h(String str) {
        return this.a.g(str, this.b, this.c);
    }

    public int hashCode() {
        return ar.a(Integer.valueOf(this.b), Integer.valueOf(this.c), this.a);
    }
}
