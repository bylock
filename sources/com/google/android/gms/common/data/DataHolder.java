package com.google.android.gms.common.data;

import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.aw;

public final class DataHolder implements SafeParcelable {
    public static final c a = new c();
    private static final b m = new a(new String[0], null);
    Bundle b;
    int[] c;
    int d;
    boolean e = false;
    private final int f;
    private final String[] g;
    private final CursorWindow[] h;
    private final int i;
    private final Bundle j;
    private Object k;
    private boolean l = true;

    DataHolder(int i2, String[] strArr, CursorWindow[] cursorWindowArr, int i3, Bundle bundle) {
        this.f = i2;
        this.g = strArr;
        this.h = cursorWindowArr;
        this.i = i3;
        this.j = bundle;
    }

    private void a(String str, int i2) {
        if (this.b == null || !this.b.containsKey(str)) {
            throw new IllegalArgumentException("No such column: " + str);
        } else if (h()) {
            throw new IllegalArgumentException("Buffer is closed.");
        } else if (i2 < 0 || i2 >= this.d) {
            throw new CursorIndexOutOfBoundsException(i2, this.d);
        }
    }

    public int a(int i2) {
        int i3 = 0;
        aw.a(i2 >= 0 && i2 < this.d);
        while (true) {
            if (i3 >= this.c.length) {
                break;
            } else if (i2 < this.c[i3]) {
                i3--;
                break;
            } else {
                i3++;
            }
        }
        return i3 == this.c.length ? i3 - 1 : i3;
    }

    public long a(String str, int i2, int i3) {
        a(str, i2);
        return this.h[i3].getLong(i2, this.b.getInt(str));
    }

    public void a() {
        this.b = new Bundle();
        for (int i2 = 0; i2 < this.g.length; i2++) {
            this.b.putInt(this.g[i2], i2);
        }
        this.c = new int[this.h.length];
        int i3 = 0;
        for (int i4 = 0; i4 < this.h.length; i4++) {
            this.c[i4] = i3;
            i3 += this.h[i4].getNumRows() - (i3 - this.h[i4].getStartPosition());
        }
        this.d = i3;
    }

    public boolean a(String str) {
        return this.b.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.f;
    }

    public int b(String str, int i2, int i3) {
        a(str, i2);
        return this.h[i3].getInt(i2, this.b.getInt(str));
    }

    public String c(String str, int i2, int i3) {
        a(str, i2);
        return this.h[i3].getString(i2, this.b.getInt(str));
    }

    /* access modifiers changed from: package-private */
    public String[] c() {
        return this.g;
    }

    public boolean d(String str, int i2, int i3) {
        a(str, i2);
        return Long.valueOf(this.h[i3].getLong(i2, this.b.getInt(str))).longValue() == 1;
    }

    /* access modifiers changed from: package-private */
    public CursorWindow[] d() {
        return this.h;
    }

    public int describeContents() {
        return 0;
    }

    public int e() {
        return this.i;
    }

    public byte[] e(String str, int i2, int i3) {
        a(str, i2);
        return this.h[i3].getBlob(i2, this.b.getInt(str));
    }

    public Uri f(String str, int i2, int i3) {
        String c2 = c(str, i2, i3);
        if (c2 == null) {
            return null;
        }
        return Uri.parse(c2);
    }

    public Bundle f() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    @Override // java.lang.Object
    public void finalize() {
        try {
            if (this.l && this.h.length > 0 && !h()) {
                Log.e("DataBuffer", "Internal data leak within a DataBuffer object detected!  Be sure to explicitly call close() on all DataBuffer extending objects when you are done with them. (" + (this.k == null ? "internal object: " + toString() : this.k.toString()) + ")");
                i();
            }
        } finally {
            super.finalize();
        }
    }

    public int g() {
        return this.d;
    }

    public boolean g(String str, int i2, int i3) {
        a(str, i2);
        return this.h[i3].isNull(i2, this.b.getInt(str));
    }

    public boolean h() {
        boolean z;
        synchronized (this) {
            z = this.e;
        }
        return z;
    }

    public void i() {
        synchronized (this) {
            if (!this.e) {
                this.e = true;
                for (int i2 = 0; i2 < this.h.length; i2++) {
                    this.h[i2].close();
                }
            }
        }
    }

    public void writeToParcel(Parcel parcel, int i2) {
        c.a(this, parcel, i2);
    }
}
