package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class c implements Parcelable.Creator {
    static void a(DataHolder dataHolder, Parcel parcel, int i) {
        int a = com.google.android.gms.common.internal.safeparcel.c.a(parcel);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 1, dataHolder.c(), false);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 1000, dataHolder.b());
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 2, (Parcelable[]) dataHolder.d(), i, false);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 3, dataHolder.e());
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 4, dataHolder.f(), false);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, a);
    }

    /* renamed from: a */
    public DataHolder createFromParcel(Parcel parcel) {
        int i = 0;
        Bundle bundle = null;
        int b = a.b(parcel);
        CursorWindow[] cursorWindowArr = null;
        String[] strArr = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    strArr = a.z(parcel, a);
                    break;
                case 2:
                    cursorWindowArr = (CursorWindow[]) a.b(parcel, a, CursorWindow.CREATOR);
                    break;
                case 3:
                    i = a.g(parcel, a);
                    break;
                case 4:
                    bundle = a.p(parcel, a);
                    break;
                case 1000:
                    i2 = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() != b) {
            throw new b("Overread allowed size end=" + b, parcel);
        }
        DataHolder dataHolder = new DataHolder(i2, strArr, cursorWindowArr, i, bundle);
        dataHolder.a();
        return dataHolder;
    }

    /* renamed from: a */
    public DataHolder[] newArray(int i) {
        return new DataHolder[i];
    }
}
