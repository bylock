package com.google.android.gms.common.api;

import android.os.Looper;
import android.util.Log;
import com.google.android.gms.internal.aq;
import com.google.android.gms.internal.aw;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public abstract class i implements b {
    private final Object a = new Object();
    private j b;
    private final CountDownLatch c = new CountDownLatch(1);
    private final ArrayList d = new ArrayList();
    private f e;
    private volatile e f;
    private volatile boolean g;
    private boolean h;
    private boolean i;
    private aq j;

    i() {
    }

    public i(Looper looper) {
        this.b = new j(looper);
    }

    private void b(e eVar) {
        this.f = eVar;
        this.j = null;
        this.c.countDown();
        Status b2 = this.f.b();
        if (this.e != null) {
            this.b.a();
            if (!this.h) {
                this.b.a(this.e, d());
            }
        }
        Iterator it = this.d.iterator();
        while (it.hasNext()) {
            ((c) it.next()).a(b2);
        }
        this.d.clear();
    }

    private void c(e eVar) {
        if (eVar instanceof d) {
            try {
                ((d) eVar).a();
            } catch (Exception e2) {
                Log.w("AbstractPendingResult", "Unable to release " + this, e2);
            }
        }
    }

    private e d() {
        e eVar;
        synchronized (this.a) {
            aw.a(!this.g, "Result has already been consumed.");
            aw.a(a(), "Result is not ready.");
            eVar = this.f;
            c();
        }
        return eVar;
    }

    /* access modifiers changed from: private */
    public void e() {
        synchronized (this.a) {
            if (!a()) {
                a(a(Status.d));
                this.i = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract e a(Status status);

    public final void a(e eVar) {
        boolean z = true;
        synchronized (this.a) {
            if (this.i || this.h) {
                c(eVar);
                return;
            }
            aw.a(!a(), "Results have already been set");
            if (this.g) {
                z = false;
            }
            aw.a(z, "Result has already been consumed");
            b(eVar);
        }
    }

    @Override // com.google.android.gms.common.api.b
    public final void a(f fVar, long j2, TimeUnit timeUnit) {
        aw.a(!this.g, "Result has already been consumed.");
        synchronized (this.a) {
            if (!b()) {
                if (a()) {
                    this.b.a(fVar, d());
                } else {
                    this.e = fVar;
                    this.b.a(this, timeUnit.toMillis(j2));
                }
            }
        }
    }

    public final boolean a() {
        return this.c.getCount() == 0;
    }

    public boolean b() {
        boolean z;
        synchronized (this.a) {
            z = this.h;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.g = true;
        this.f = null;
        this.e = null;
    }
}
