package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;

public final class Status implements e, SafeParcelable {
    public static final Status a = new Status(0);
    public static final Status b = new Status(14);
    public static final Status c = new Status(8);
    public static final Status d = new Status(15);
    public static final Status e = new Status(16);
    public static final g f = new g();
    private final int g;
    private final int h;
    private final String i;
    private final PendingIntent j;

    public Status(int i2) {
        this(1, i2, null, null);
    }

    Status(int i2, int i3, String str, PendingIntent pendingIntent) {
        this.g = i2;
        this.h = i3;
        this.i = str;
        this.j = pendingIntent;
    }

    private String f() {
        return this.i != null ? this.i : a.a(this.h);
    }

    /* access modifiers changed from: package-private */
    public PendingIntent a() {
        return this.j;
    }

    @Override // com.google.android.gms.common.api.e
    public Status b() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.g;
    }

    public int describeContents() {
        return 0;
    }

    public int e() {
        return this.h;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Status)) {
            return false;
        }
        Status status = (Status) obj;
        return this.g == status.g && this.h == status.h && ar.a(this.i, status.i) && ar.a(this.j, status.j);
    }

    public int hashCode() {
        return ar.a(Integer.valueOf(this.g), Integer.valueOf(this.h), this.i, this.j);
    }

    public String toString() {
        return ar.a(this).a("statusCode", f()).a("resolution", this.j).toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        g.a(this, parcel, i2);
    }
}
