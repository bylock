package com.google.android.gms.common.api;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Pair;

public class j extends Handler {
    public j() {
        this(Looper.getMainLooper());
    }

    public j(Looper looper) {
        super(looper);
    }

    public void a() {
        removeMessages(2);
    }

    public void a(f fVar, e eVar) {
        sendMessage(obtainMessage(1, new Pair(fVar, eVar)));
    }

    public void a(i iVar, long j) {
        sendMessageDelayed(obtainMessage(2, iVar), j);
    }

    /* access modifiers changed from: protected */
    public void b(f fVar, e eVar) {
        fVar.a(eVar);
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                Pair pair = (Pair) message.obj;
                b((f) pair.first, (e) pair.second);
                return;
            case 2:
                i.a((i) message.obj);
                return;
            default:
                Log.wtf("GoogleApi", "Don't know how to handle this message.");
                return;
        }
    }
}
