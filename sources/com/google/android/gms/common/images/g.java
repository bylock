package com.google.android.gms.common.images;

import android.graphics.drawable.Drawable;
import com.google.android.gms.internal.ar;
import java.lang.ref.WeakReference;

public final class g extends e {
    private WeakReference d;

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.common.images.e
    public void a(Drawable drawable, boolean z, boolean z2, boolean z3) {
        a aVar;
        if (!z2 && (aVar = (a) this.d.get()) != null) {
            aVar.a(this.a.a, drawable, z3);
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof g)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        g gVar = (g) obj;
        a aVar = (a) this.d.get();
        a aVar2 = (a) gVar.d.get();
        return aVar2 != null && aVar != null && ar.a(aVar2, aVar) && ar.a(gVar.a, this.a);
    }

    public int hashCode() {
        return ar.a(this.a);
    }
}
