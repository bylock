package com.google.android.gms.common.images;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class h implements Parcelable.Creator {
    static void a(WebImage webImage, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, webImage.a());
        c.a(parcel, 2, (Parcelable) webImage.b(), i, false);
        c.a(parcel, 3, webImage.c());
        c.a(parcel, 4, webImage.d());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public WebImage createFromParcel(Parcel parcel) {
        int g;
        int i;
        Uri uri;
        int i2;
        int i3 = 0;
        int b = a.b(parcel);
        Uri uri2 = null;
        int i4 = 0;
        int i5 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = i4;
                    uri = uri2;
                    i2 = a.g(parcel, a);
                    g = i3;
                    break;
                case 2:
                    i2 = i5;
                    uri = (Uri) a.a(parcel, a, Uri.CREATOR);
                    g = i3;
                    i = i4;
                    break;
                case 3:
                    uri = uri2;
                    i2 = i5;
                    i = a.g(parcel, a);
                    g = i3;
                    break;
                case 4:
                    g = a.g(parcel, a);
                    i = i4;
                    uri = uri2;
                    i2 = i5;
                    break;
                default:
                    a.b(parcel, a);
                    g = i3;
                    i = i4;
                    uri = uri2;
                    i2 = i5;
                    break;
            }
            i5 = i2;
            uri2 = uri;
            i4 = i;
            i3 = g;
        }
        if (parcel.dataPosition() == b) {
            return new WebImage(i5, uri2, i4, i3);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public WebImage[] newArray(int i) {
        return new WebImage[i];
    }
}
