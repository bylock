package com.google.android.gms.common.images;

import android.graphics.Bitmap;
import com.google.android.gms.internal.ax;

/* access modifiers changed from: package-private */
public final class b extends ax {
    /* access modifiers changed from: protected */
    public int a(f fVar, Bitmap bitmap) {
        return bitmap.getHeight() * bitmap.getRowBytes();
    }

    /* access modifiers changed from: protected */
    public void a(boolean z, f fVar, Bitmap bitmap, Bitmap bitmap2) {
        super.a(z, (Object) fVar, (Object) bitmap, (Object) bitmap2);
    }
}
