package com.google.android.gms.common.images;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.google.android.gms.internal.al;
import com.google.android.gms.internal.am;
import com.google.android.gms.internal.an;
import com.google.android.gms.internal.ao;

public abstract class e {
    final f a;
    protected int b;
    protected int c;

    private Drawable a(Context context, am amVar, int i) {
        Resources resources = context.getResources();
        if (this.c <= 0) {
            return resources.getDrawable(i);
        }
        an anVar = new an(i, this.c);
        Drawable drawable = (Drawable) amVar.a(anVar);
        if (drawable != null) {
            return drawable;
        }
        Drawable drawable2 = resources.getDrawable(i);
        if ((this.c & 1) != 0) {
            drawable2 = a(resources, drawable2);
        }
        amVar.b(anVar, drawable2);
        return drawable2;
    }

    /* access modifiers changed from: protected */
    public Drawable a(Resources resources, Drawable drawable) {
        return al.a(resources, drawable);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Bitmap bitmap, boolean z) {
        ao.a(bitmap);
        if ((this.c & 1) != 0) {
            bitmap = al.a(bitmap);
        }
        a(new BitmapDrawable(context.getResources(), bitmap), z, false, true);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, am amVar, boolean z) {
        Drawable drawable = null;
        if (this.b != 0) {
            drawable = a(context, amVar, this.b);
        }
        a(drawable, z, false, false);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Drawable drawable, boolean z, boolean z2, boolean z3);
}
