package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class d implements Parcelable.Creator {
    static void a(LocationRequest locationRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, locationRequest.a);
        c.a(parcel, 1000, locationRequest.a());
        c.a(parcel, 2, locationRequest.b);
        c.a(parcel, 3, locationRequest.c);
        c.a(parcel, 4, locationRequest.d);
        c.a(parcel, 5, locationRequest.e);
        c.a(parcel, 6, locationRequest.f);
        c.a(parcel, 7, locationRequest.g);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public LocationRequest createFromParcel(Parcel parcel) {
        boolean z = false;
        int b = a.b(parcel);
        int i = 102;
        long j = 3600000;
        long j2 = 600000;
        long j3 = Long.MAX_VALUE;
        int i2 = Integer.MAX_VALUE;
        float f = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    j = a.i(parcel, a);
                    break;
                case 3:
                    j2 = a.i(parcel, a);
                    break;
                case 4:
                    z = a.c(parcel, a);
                    break;
                case 5:
                    j3 = a.i(parcel, a);
                    break;
                case 6:
                    i2 = a.g(parcel, a);
                    break;
                case 7:
                    f = a.k(parcel, a);
                    break;
                case 1000:
                    i3 = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new LocationRequest(i3, i, j, j2, z, j3, i2, f);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public LocationRequest[] newArray(int i) {
        return new LocationRequest[i];
    }
}
