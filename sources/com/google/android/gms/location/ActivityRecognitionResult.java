package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.List;

public class ActivityRecognitionResult implements SafeParcelable {
    public static final a a = new a();
    List b;
    long c;
    long d;
    private final int e = 1;

    public ActivityRecognitionResult(int i, List list, long j, long j2) {
        this.b = list;
        this.c = j;
        this.d = j2;
    }

    public int a() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "ActivityRecognitionResult [probableActivities=" + this.b + ", timeMillis=" + this.c + ", elapsedRealtimeMillis=" + this.d + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        a.a(this, parcel, i);
    }
}
