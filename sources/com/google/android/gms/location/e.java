package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class e implements Parcelable.Creator {
    static void a(b bVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, bVar.a);
        c.a(parcel, 1000, bVar.a());
        c.a(parcel, 2, bVar.b);
        c.a(parcel, 3, bVar.c);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public b createFromParcel(Parcel parcel) {
        int i = 1;
        int b = a.b(parcel);
        int i2 = 0;
        long j = 0;
        int i3 = 1;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i3 = a.g(parcel, a);
                    break;
                case 2:
                    i = a.g(parcel, a);
                    break;
                case 3:
                    j = a.i(parcel, a);
                    break;
                case 1000:
                    i2 = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new b(i2, i3, i, j);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public b[] newArray(int i) {
        return new b[i];
    }
}
