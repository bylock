package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class DetectedActivity implements SafeParcelable {
    public static final c a = new c();
    int b;
    int c;
    private final int d;

    public DetectedActivity(int i, int i2, int i3) {
        this.d = i;
        this.b = i2;
        this.c = i3;
    }

    private int a(int i) {
        if (i > 8) {
            return 4;
        }
        return i;
    }

    public int a() {
        return a(this.b);
    }

    public int b() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "DetectedActivity [type=" + a() + ", confidence=" + this.c + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        c.a(this, parcel, i);
    }
}
