package com.google.android.gms.identity.intents.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;

public class b implements Parcelable.Creator {
    static void a(UserAddress userAddress, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, userAddress.a());
        c.a(parcel, 2, userAddress.a, false);
        c.a(parcel, 3, userAddress.b, false);
        c.a(parcel, 4, userAddress.c, false);
        c.a(parcel, 5, userAddress.d, false);
        c.a(parcel, 6, userAddress.e, false);
        c.a(parcel, 7, userAddress.f, false);
        c.a(parcel, 8, userAddress.g, false);
        c.a(parcel, 9, userAddress.h, false);
        c.a(parcel, 10, userAddress.i, false);
        c.a(parcel, 11, userAddress.j, false);
        c.a(parcel, 12, userAddress.k, false);
        c.a(parcel, 13, userAddress.l, false);
        c.a(parcel, 14, userAddress.m);
        c.a(parcel, 15, userAddress.n, false);
        c.a(parcel, 16, userAddress.o, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public UserAddress createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        boolean z = false;
        String str13 = null;
        String str14 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    str2 = a.n(parcel, a);
                    break;
                case 4:
                    str3 = a.n(parcel, a);
                    break;
                case 5:
                    str4 = a.n(parcel, a);
                    break;
                case 6:
                    str5 = a.n(parcel, a);
                    break;
                case 7:
                    str6 = a.n(parcel, a);
                    break;
                case 8:
                    str7 = a.n(parcel, a);
                    break;
                case 9:
                    str8 = a.n(parcel, a);
                    break;
                case 10:
                    str9 = a.n(parcel, a);
                    break;
                case 11:
                    str10 = a.n(parcel, a);
                    break;
                case 12:
                    str11 = a.n(parcel, a);
                    break;
                case 13:
                    str12 = a.n(parcel, a);
                    break;
                case 14:
                    z = a.c(parcel, a);
                    break;
                case 15:
                    str13 = a.n(parcel, a);
                    break;
                case 16:
                    str14 = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new UserAddress(i, str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, z, str13, str14);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public UserAddress[] newArray(int i) {
        return new UserAddress[i];
    }
}
