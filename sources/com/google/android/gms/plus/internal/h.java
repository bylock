package com.google.android.gms.plus.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;
import java.util.Arrays;

public class h implements SafeParcelable {
    public static final b a = new b();
    private final int b;
    private final String c;
    private final String[] d;
    private final String[] e;
    private final String[] f;
    private final String g;
    private final String h;
    private final String i;
    private final String j;
    private final PlusCommonExtras k;

    h(int i2, String str, String[] strArr, String[] strArr2, String[] strArr3, String str2, String str3, String str4, String str5, PlusCommonExtras plusCommonExtras) {
        this.b = i2;
        this.c = str;
        this.d = strArr;
        this.e = strArr2;
        this.f = strArr3;
        this.g = str2;
        this.h = str3;
        this.i = str4;
        this.j = str5;
        this.k = plusCommonExtras;
    }

    public int a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }

    public String[] c() {
        return this.d;
    }

    public String[] d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public String[] e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return this.b == hVar.b && ar.a(this.c, hVar.c) && Arrays.equals(this.d, hVar.d) && Arrays.equals(this.e, hVar.e) && Arrays.equals(this.f, hVar.f) && ar.a(this.g, hVar.g) && ar.a(this.h, hVar.h) && ar.a(this.i, hVar.i) && ar.a(this.j, hVar.j) && ar.a(this.k, hVar.k);
    }

    public String f() {
        return this.g;
    }

    public String g() {
        return this.h;
    }

    public String h() {
        return this.i;
    }

    public int hashCode() {
        return ar.a(Integer.valueOf(this.b), this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k);
    }

    public String i() {
        return this.j;
    }

    public PlusCommonExtras j() {
        return this.k;
    }

    public String toString() {
        return ar.a(this).a("versionCode", Integer.valueOf(this.b)).a("accountName", this.c).a("requestedScopes", this.d).a("visibleActivities", this.e).a("requiredFeatures", this.f).a("packageNameForAuth", this.g).a("callingPackageName", this.h).a("applicationName", this.i).a("extra", this.k.toString()).toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        b.a(this, parcel, i2);
    }
}
