package com.google.android.gms.plus.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;

public class b implements Parcelable.Creator {
    static void a(h hVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, hVar.b(), false);
        c.a(parcel, 1000, hVar.a());
        c.a(parcel, 2, hVar.c(), false);
        c.a(parcel, 3, hVar.d(), false);
        c.a(parcel, 4, hVar.e(), false);
        c.a(parcel, 5, hVar.f(), false);
        c.a(parcel, 6, hVar.g(), false);
        c.a(parcel, 7, hVar.h(), false);
        c.a(parcel, 8, hVar.i(), false);
        c.a(parcel, 9, (Parcelable) hVar.j(), i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public h createFromParcel(Parcel parcel) {
        PlusCommonExtras plusCommonExtras = null;
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String[] strArr = null;
        String[] strArr2 = null;
        String[] strArr3 = null;
        String str5 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str5 = a.n(parcel, a);
                    break;
                case 2:
                    strArr3 = a.z(parcel, a);
                    break;
                case 3:
                    strArr2 = a.z(parcel, a);
                    break;
                case 4:
                    strArr = a.z(parcel, a);
                    break;
                case 5:
                    str4 = a.n(parcel, a);
                    break;
                case 6:
                    str3 = a.n(parcel, a);
                    break;
                case 7:
                    str2 = a.n(parcel, a);
                    break;
                case 8:
                    str = a.n(parcel, a);
                    break;
                case 9:
                    plusCommonExtras = (PlusCommonExtras) a.a(parcel, a, PlusCommonExtras.b);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new h(i, str5, strArr3, strArr2, strArr, str4, str3, str2, str, plusCommonExtras);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public h[] newArray(int i) {
        return new h[i];
    }
}
