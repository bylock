package com.google.android.gms.plus.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;

public class PlusCommonExtras implements SafeParcelable {
    public static String a = "PlusCommonExtras";
    public static final a b = new a();
    private final int c;
    private String d;
    private String e;

    public PlusCommonExtras() {
        this.c = 1;
        this.d = "";
        this.e = "";
    }

    PlusCommonExtras(int i, String str, String str2) {
        this.c = i;
        this.d = str;
        this.e = str2;
    }

    public int a() {
        return this.c;
    }

    public String b() {
        return this.d;
    }

    public String c() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof PlusCommonExtras)) {
            return false;
        }
        PlusCommonExtras plusCommonExtras = (PlusCommonExtras) obj;
        return this.c == plusCommonExtras.c && ar.a(this.d, plusCommonExtras.d) && ar.a(this.e, plusCommonExtras.e);
    }

    public int hashCode() {
        return ar.a(Integer.valueOf(this.c), this.d, this.e);
    }

    public String toString() {
        return ar.a(this).a("versionCode", Integer.valueOf(this.c)).a("Gpsrc", this.d).a("ClientCallingPackage", this.e).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        a.a(this, parcel, i);
    }
}
