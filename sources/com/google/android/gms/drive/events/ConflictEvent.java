package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.DriveId;

public final class ConflictEvent implements SafeParcelable, DriveEvent {
    public static final Parcelable.Creator CREATOR = new b();
    final int a;
    final DriveId b;

    ConflictEvent(int i, DriveId driveId) {
        this.a = i;
        this.b = driveId;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return String.format("ConflictEvent [id=%s]", this.b);
    }

    public void writeToParcel(Parcel parcel, int i) {
        b.a(this, parcel, i);
    }
}
