package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.DriveId;

public class b implements Parcelable.Creator {
    static void a(ConflictEvent conflictEvent, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, conflictEvent.a);
        c.a(parcel, 2, (Parcelable) conflictEvent.b, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ConflictEvent createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        DriveId driveId = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    driveId = (DriveId) a.a(parcel, a, DriveId.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ConflictEvent(i, driveId);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ConflictEvent[] newArray(int i) {
        return new ConflictEvent[i];
    }
}
