package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.query.internal.LogicalFilter;

public class b implements Parcelable.Creator {
    static void a(Query query, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1000, query.d);
        c.a(parcel, 1, (Parcelable) query.a, i, false);
        c.a(parcel, 3, query.b, false);
        c.a(parcel, 4, (Parcelable) query.c, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public Query createFromParcel(Parcel parcel) {
        SortOrder sortOrder;
        String str;
        LogicalFilter logicalFilter;
        int i;
        SortOrder sortOrder2 = null;
        int b = a.b(parcel);
        int i2 = 0;
        String str2 = null;
        LogicalFilter logicalFilter2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = i2;
                    logicalFilter = (LogicalFilter) a.a(parcel, a, LogicalFilter.CREATOR);
                    sortOrder = sortOrder2;
                    str = str2;
                    break;
                case 3:
                    logicalFilter = logicalFilter2;
                    i = i2;
                    str = a.n(parcel, a);
                    sortOrder = sortOrder2;
                    break;
                case 4:
                    sortOrder = (SortOrder) a.a(parcel, a, SortOrder.CREATOR);
                    str = str2;
                    logicalFilter = logicalFilter2;
                    i = i2;
                    break;
                case 1000:
                    str = str2;
                    logicalFilter = logicalFilter2;
                    i = a.g(parcel, a);
                    sortOrder = sortOrder2;
                    break;
                default:
                    a.b(parcel, a);
                    sortOrder = sortOrder2;
                    str = str2;
                    logicalFilter = logicalFilter2;
                    i = i2;
                    break;
            }
            i2 = i;
            logicalFilter2 = logicalFilter;
            str2 = str;
            sortOrder2 = sortOrder;
        }
        if (parcel.dataPosition() == b) {
            return new Query(i2, logicalFilter2, str2, sortOrder2);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public Query[] newArray(int i) {
        return new Query[i];
    }
}
