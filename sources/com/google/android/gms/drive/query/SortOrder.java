package com.google.android.gms.drive.query;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.List;

public class SortOrder implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new c();
    final List a;
    final int b;

    SortOrder(int i, List list) {
        this.b = i;
        this.a = list;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        c.a(this, parcel, i);
    }
}
