package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.query.a;

public class FilterHolder implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new d();
    final int a;
    final ComparisonFilter b;
    final FieldOnlyFilter c;
    final LogicalFilter d;
    final NotFilter e;
    final InFilter f;
    final MatchAllFilter g;
    private final a h;

    FilterHolder(int i, ComparisonFilter comparisonFilter, FieldOnlyFilter fieldOnlyFilter, LogicalFilter logicalFilter, NotFilter notFilter, InFilter inFilter, MatchAllFilter matchAllFilter) {
        this.a = i;
        this.b = comparisonFilter;
        this.c = fieldOnlyFilter;
        this.d = logicalFilter;
        this.e = notFilter;
        this.f = inFilter;
        this.g = matchAllFilter;
        if (this.b != null) {
            this.h = this.b;
        } else if (this.c != null) {
            this.h = this.c;
        } else if (this.d != null) {
            this.h = this.d;
        } else if (this.e != null) {
            this.h = this.e;
        } else if (this.f != null) {
            this.h = this.f;
        } else if (this.g != null) {
            this.h = this.g;
        } else {
            throw new IllegalArgumentException("At least one filter must be set.");
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        d.a(this, parcel, i);
    }
}
