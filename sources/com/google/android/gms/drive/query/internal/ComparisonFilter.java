package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.query.a;

public class ComparisonFilter implements SafeParcelable, a {
    public static final a a = new a();
    final Operator b;
    final MetadataBundle c;
    final int d;
    final com.google.android.gms.drive.metadata.a e;

    ComparisonFilter(int i, Operator operator, MetadataBundle metadataBundle) {
        this.d = i;
        this.b = operator;
        this.c = metadataBundle;
        this.e = e.a(metadataBundle);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        a.a(this, parcel, i);
    }
}
