package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class d implements Parcelable.Creator {
    static void a(FilterHolder filterHolder, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, (Parcelable) filterHolder.b, i, false);
        c.a(parcel, 1000, filterHolder.a);
        c.a(parcel, 2, (Parcelable) filterHolder.c, i, false);
        c.a(parcel, 3, (Parcelable) filterHolder.d, i, false);
        c.a(parcel, 4, (Parcelable) filterHolder.e, i, false);
        c.a(parcel, 5, (Parcelable) filterHolder.f, i, false);
        c.a(parcel, 6, (Parcelable) filterHolder.g, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public FilterHolder createFromParcel(Parcel parcel) {
        MatchAllFilter matchAllFilter = null;
        int b = a.b(parcel);
        int i = 0;
        InFilter inFilter = null;
        NotFilter notFilter = null;
        LogicalFilter logicalFilter = null;
        FieldOnlyFilter fieldOnlyFilter = null;
        ComparisonFilter comparisonFilter = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    comparisonFilter = (ComparisonFilter) a.a(parcel, a, ComparisonFilter.a);
                    break;
                case 2:
                    fieldOnlyFilter = (FieldOnlyFilter) a.a(parcel, a, FieldOnlyFilter.CREATOR);
                    break;
                case 3:
                    logicalFilter = (LogicalFilter) a.a(parcel, a, LogicalFilter.CREATOR);
                    break;
                case 4:
                    notFilter = (NotFilter) a.a(parcel, a, NotFilter.CREATOR);
                    break;
                case 5:
                    inFilter = (InFilter) a.a(parcel, a, InFilter.a);
                    break;
                case 6:
                    matchAllFilter = (MatchAllFilter) a.a(parcel, a, MatchAllFilter.a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new FilterHolder(i, comparisonFilter, fieldOnlyFilter, logicalFilter, notFilter, inFilter, matchAllFilter);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public FilterHolder[] newArray(int i) {
        return new FilterHolder[i];
    }
}
