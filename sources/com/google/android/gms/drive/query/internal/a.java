package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class a implements Parcelable.Creator {
    static void a(ComparisonFilter comparisonFilter, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1000, comparisonFilter.d);
        c.a(parcel, 1, (Parcelable) comparisonFilter.b, i, false);
        c.a(parcel, 2, (Parcelable) comparisonFilter.c, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ComparisonFilter createFromParcel(Parcel parcel) {
        MetadataBundle metadataBundle;
        Operator operator;
        int i;
        MetadataBundle metadataBundle2 = null;
        int b = com.google.android.gms.common.internal.safeparcel.a.b(parcel);
        int i2 = 0;
        Operator operator2 = null;
        while (parcel.dataPosition() < b) {
            int a = com.google.android.gms.common.internal.safeparcel.a.a(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.a.a(a)) {
                case 1:
                    Operator operator3 = (Operator) com.google.android.gms.common.internal.safeparcel.a.a(parcel, a, Operator.CREATOR);
                    i = i2;
                    metadataBundle = metadataBundle2;
                    operator = operator3;
                    break;
                case 2:
                    metadataBundle = (MetadataBundle) com.google.android.gms.common.internal.safeparcel.a.a(parcel, a, MetadataBundle.CREATOR);
                    operator = operator2;
                    i = i2;
                    break;
                case 1000:
                    operator = operator2;
                    i = com.google.android.gms.common.internal.safeparcel.a.g(parcel, a);
                    metadataBundle = metadataBundle2;
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.a.b(parcel, a);
                    metadataBundle = metadataBundle2;
                    operator = operator2;
                    i = i2;
                    break;
            }
            i2 = i;
            operator2 = operator;
            metadataBundle2 = metadataBundle;
        }
        if (parcel.dataPosition() == b) {
            return new ComparisonFilter(i2, operator2, metadataBundle2);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ComparisonFilter[] newArray(int i) {
        return new ComparisonFilter[i];
    }
}
