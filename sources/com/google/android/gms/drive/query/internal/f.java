package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class f implements Parcelable.Creator {
    static void a(InFilter inFilter, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1000, inFilter.c);
        c.a(parcel, 1, (Parcelable) inFilter.b, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public InFilter createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        MetadataBundle metadataBundle = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    metadataBundle = (MetadataBundle) a.a(parcel, a, MetadataBundle.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new InFilter(i, metadataBundle);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public InFilter[] newArray(int i) {
        return new InFilter[i];
    }
}
