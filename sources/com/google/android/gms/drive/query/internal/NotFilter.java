package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.query.a;

public class NotFilter implements SafeParcelable, a {
    public static final Parcelable.Creator CREATOR = new i();
    final FilterHolder a;
    final int b;

    NotFilter(int i, FilterHolder filterHolder) {
        this.b = i;
        this.a = filterHolder;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        i.a(this, parcel, i);
    }
}
