package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.query.a;
import java.util.List;

public class LogicalFilter implements SafeParcelable, a {
    public static final Parcelable.Creator CREATOR = new g();
    final Operator a;
    final List b;
    final int c;

    LogicalFilter(int i, Operator operator, List list) {
        this.c = i;
        this.a = operator;
        this.b = list;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        g.a(this, parcel, i);
    }
}
