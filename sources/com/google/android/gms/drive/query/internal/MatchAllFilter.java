package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.query.a;

public class MatchAllFilter implements SafeParcelable, a {
    public static final h a = new h();
    final int b;

    public MatchAllFilter() {
        this(1);
    }

    MatchAllFilter(int i) {
        this.b = i;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        h.a(this, parcel, i);
    }
}
