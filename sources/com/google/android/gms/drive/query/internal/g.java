package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;

public class g implements Parcelable.Creator {
    static void a(LogicalFilter logicalFilter, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1000, logicalFilter.c);
        c.a(parcel, 1, (Parcelable) logicalFilter.a, i, false);
        c.b(parcel, 2, logicalFilter.b, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public LogicalFilter createFromParcel(Parcel parcel) {
        ArrayList c;
        Operator operator;
        int i;
        ArrayList arrayList = null;
        int b = a.b(parcel);
        int i2 = 0;
        Operator operator2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    Operator operator3 = (Operator) a.a(parcel, a, Operator.CREATOR);
                    i = i2;
                    c = arrayList;
                    operator = operator3;
                    break;
                case 2:
                    c = a.c(parcel, a, FilterHolder.CREATOR);
                    operator = operator2;
                    i = i2;
                    break;
                case 1000:
                    operator = operator2;
                    i = a.g(parcel, a);
                    c = arrayList;
                    break;
                default:
                    a.b(parcel, a);
                    c = arrayList;
                    operator = operator2;
                    i = i2;
                    break;
            }
            i2 = i;
            operator2 = operator;
            arrayList = c;
        }
        if (parcel.dataPosition() == b) {
            return new LogicalFilter(i2, operator2, arrayList);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public LogicalFilter[] newArray(int i) {
        return new LogicalFilter[i];
    }
}
