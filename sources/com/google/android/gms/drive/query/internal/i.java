package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class i implements Parcelable.Creator {
    static void a(NotFilter notFilter, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1000, notFilter.b);
        c.a(parcel, 1, (Parcelable) notFilter.a, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public NotFilter createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        FilterHolder filterHolder = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    filterHolder = (FilterHolder) a.a(parcel, a, FilterHolder.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new NotFilter(i, filterHolder);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public NotFilter[] newArray(int i) {
        return new NotFilter[i];
    }
}
