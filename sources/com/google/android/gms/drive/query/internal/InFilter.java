package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.metadata.g;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.query.a;

public class InFilter implements SafeParcelable, a {
    public static final f a = new f();
    final MetadataBundle b;
    final int c;
    private final g d;

    InFilter(int i, MetadataBundle metadataBundle) {
        this.c = i;
        this.b = metadataBundle;
        this.d = (g) e.a(metadataBundle);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        f.a(this, parcel, i);
    }
}
