package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class j implements Parcelable.Creator {
    static void a(Operator operator, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1000, operator.k);
        c.a(parcel, 1, operator.j, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public Operator createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str = a.n(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new Operator(i, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public Operator[] newArray(int i) {
        return new Operator[i];
    }
}
