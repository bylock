package com.google.android.gms.drive.metadata;

import com.google.android.gms.internal.aw;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class f implements a {
    private final String a;
    private final Set b;
    private final int c;

    protected f(String str, int i) {
        this.a = (String) aw.a(str, "fieldName");
        this.b = Collections.singleton(str);
        this.c = i;
    }

    protected f(String str, Collection collection, int i) {
        this.a = (String) aw.a(str, "fieldName");
        this.b = Collections.unmodifiableSet(new HashSet(collection));
        this.c = i;
    }

    @Override // com.google.android.gms.drive.metadata.a
    public final String a() {
        return this.a;
    }

    public String toString() {
        return this.a;
    }
}
