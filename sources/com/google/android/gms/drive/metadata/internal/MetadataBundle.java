package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;
import com.google.android.gms.internal.aw;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public final class MetadataBundle implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new f();
    final int a;
    final Bundle b;

    MetadataBundle(int i, Bundle bundle) {
        this.a = i;
        this.b = (Bundle) aw.a(bundle);
        this.b.setClassLoader(getClass().getClassLoader());
        ArrayList<String> arrayList = new ArrayList();
        for (String str : this.b.keySet()) {
            if (c.a(str) == null) {
                arrayList.add(str);
                Log.w("MetadataBundle", "Ignored unknown metadata field in bundle: " + str);
            }
        }
        for (String str2 : arrayList) {
            this.b.remove(str2);
        }
    }

    public Set a() {
        HashSet hashSet = new HashSet();
        for (String str : this.b.keySet()) {
            hashSet.add(c.a(str));
        }
        return hashSet;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MetadataBundle)) {
            return false;
        }
        MetadataBundle metadataBundle = (MetadataBundle) obj;
        Set<String> keySet = this.b.keySet();
        if (!keySet.equals(metadataBundle.b.keySet())) {
            return false;
        }
        for (String str : keySet) {
            if (!ar.a(this.b.get(str), metadataBundle.b.get(str))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 1;
        for (String str : this.b.keySet()) {
            i = this.b.get(str).hashCode() + (i * 31);
        }
        return i;
    }

    public String toString() {
        return "MetadataBundle [values=" + this.b + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        f.a(this, parcel, i);
    }
}
