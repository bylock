package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class f implements Parcelable.Creator {
    static void a(MetadataBundle metadataBundle, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, metadataBundle.a);
        c.a(parcel, 2, metadataBundle.b, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public MetadataBundle createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    bundle = a.p(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new MetadataBundle(i, bundle);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public MetadataBundle[] newArray(int i) {
        return new MetadataBundle[i];
    }
}
