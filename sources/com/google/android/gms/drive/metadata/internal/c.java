package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.internal.bp;
import com.google.android.gms.internal.by;
import com.google.android.gms.internal.ch;
import java.util.HashMap;
import java.util.Map;

public final class c {
    private static Map a = new HashMap();

    static {
        a(bp.a);
        a(bp.x);
        a(bp.q);
        a(bp.v);
        a(bp.y);
        a(bp.k);
        a(bp.l);
        a(bp.i);
        a(bp.n);
        a(bp.t);
        a(bp.b);
        a(bp.s);
        a(bp.c);
        a(bp.j);
        a(bp.d);
        a(bp.e);
        a(bp.f);
        a(bp.p);
        a(bp.m);
        a(bp.r);
        a(bp.u);
        a(bp.z);
        a(bp.A);
        a(bp.h);
        a(bp.g);
        a(bp.w);
        a(bp.o);
        a(by.a);
        a(by.c);
        a(by.d);
        a(by.e);
        a(by.b);
        a(ch.a);
        a(ch.b);
    }

    public static a a(String str) {
        return (a) a.get(str);
    }

    private static void a(a aVar) {
        if (a.containsKey(aVar.a())) {
            throw new IllegalArgumentException("Duplicate field name registered: " + aVar.a());
        }
        a.put(aVar.a(), aVar);
    }
}
