package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.Contents;

public class q implements Parcelable.Creator {
    static void a(CloseContentsRequest closeContentsRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, closeContentsRequest.a);
        c.a(parcel, 2, (Parcelable) closeContentsRequest.b, i, false);
        c.a(parcel, 3, closeContentsRequest.c, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public CloseContentsRequest createFromParcel(Parcel parcel) {
        Boolean d;
        Contents contents;
        int i;
        Boolean bool = null;
        int b = a.b(parcel);
        int i2 = 0;
        Contents contents2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    contents = contents2;
                    i = a.g(parcel, a);
                    d = bool;
                    break;
                case 2:
                    Contents contents3 = (Contents) a.a(parcel, a, Contents.CREATOR);
                    i = i2;
                    d = bool;
                    contents = contents3;
                    break;
                case 3:
                    d = a.d(parcel, a);
                    contents = contents2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, a);
                    d = bool;
                    contents = contents2;
                    i = i2;
                    break;
            }
            i2 = i;
            contents2 = contents;
            bool = d;
        }
        if (parcel.dataPosition() == b) {
            return new CloseContentsRequest(i2, contents2, bool);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public CloseContentsRequest[] newArray(int i) {
        return new CloseContentsRequest[i];
    }
}
