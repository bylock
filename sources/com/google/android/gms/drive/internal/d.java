package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ConflictEvent;

public class d implements Parcelable.Creator {
    static void a(OnEventResponse onEventResponse, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, onEventResponse.a);
        c.a(parcel, 2, onEventResponse.b);
        c.a(parcel, 3, (Parcelable) onEventResponse.c, i, false);
        c.a(parcel, 4, (Parcelable) onEventResponse.d, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public OnEventResponse createFromParcel(Parcel parcel) {
        ConflictEvent conflictEvent;
        ChangeEvent changeEvent;
        int i;
        int i2;
        ConflictEvent conflictEvent2 = null;
        int i3 = 0;
        int b = a.b(parcel);
        ChangeEvent changeEvent2 = null;
        int i4 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = a.g(parcel, a);
                    conflictEvent = conflictEvent2;
                    break;
                case 2:
                    i2 = i4;
                    i = a.g(parcel, a);
                    conflictEvent = conflictEvent2;
                    changeEvent = changeEvent2;
                    break;
                case 3:
                    i = i3;
                    i2 = i4;
                    changeEvent = (ChangeEvent) a.a(parcel, a, ChangeEvent.CREATOR);
                    conflictEvent = conflictEvent2;
                    break;
                case 4:
                    conflictEvent = (ConflictEvent) a.a(parcel, a, ConflictEvent.CREATOR);
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = i4;
                    break;
                default:
                    a.b(parcel, a);
                    conflictEvent = conflictEvent2;
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = i4;
                    break;
            }
            i4 = i2;
            i3 = i;
            changeEvent2 = changeEvent;
            conflictEvent2 = conflictEvent;
        }
        if (parcel.dataPosition() == b) {
            return new OnEventResponse(i4, i3, changeEvent2, conflictEvent2);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public OnEventResponse[] newArray(int i) {
        return new OnEventResponse[i];
    }
}
