package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.internal.aw;

public class CreateFileRequest implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new t();
    final int a;
    final DriveId b;
    final MetadataBundle c;
    final Contents d;

    CreateFileRequest(int i, DriveId driveId, MetadataBundle metadataBundle, Contents contents) {
        this.a = i;
        this.b = (DriveId) aw.a(driveId);
        this.c = (MetadataBundle) aw.a(metadataBundle);
        this.d = (Contents) aw.a(contents);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        t.a(this, parcel, i);
    }
}
