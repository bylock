package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.DriveId;

public class j implements Parcelable.Creator {
    static void a(OpenFileIntentSenderRequest openFileIntentSenderRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, openFileIntentSenderRequest.a);
        c.a(parcel, 2, openFileIntentSenderRequest.b, false);
        c.a(parcel, 3, openFileIntentSenderRequest.c, false);
        c.a(parcel, 4, (Parcelable) openFileIntentSenderRequest.d, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public OpenFileIntentSenderRequest createFromParcel(Parcel parcel) {
        DriveId driveId = null;
        int b = a.b(parcel);
        int i = 0;
        String[] strArr = null;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    strArr = a.z(parcel, a);
                    break;
                case 4:
                    driveId = (DriveId) a.a(parcel, a, DriveId.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new OpenFileIntentSenderRequest(i, str, strArr, driveId);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public OpenFileIntentSenderRequest[] newArray(int i) {
        return new OpenFileIntentSenderRequest[i];
    }
}
