package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.Contents;

public class z implements Parcelable.Creator {
    static void a(OnContentsResponse onContentsResponse, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, onContentsResponse.a);
        c.a(parcel, 2, (Parcelable) onContentsResponse.b, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public OnContentsResponse createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        Contents contents = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    contents = (Contents) a.a(parcel, a, Contents.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new OnContentsResponse(i, contents);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public OnContentsResponse[] newArray(int i) {
        return new OnContentsResponse[i];
    }
}
