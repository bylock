package com.google.android.gms.drive.internal;

import android.support.v7.b.k;
import com.google.android.gms.internal.ds;
import com.google.android.gms.internal.dt;
import com.google.android.gms.internal.dv;

public final class y extends dv {
    public int a;
    public String b;
    public long c;
    public long d;

    public y() {
        a();
    }

    public y a() {
        this.a = 1;
        this.b = "";
        this.c = -1;
        this.d = -1;
        this.r = null;
        this.s = -1;
        return this;
    }

    /* renamed from: a */
    public y b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.a = dsVar.e();
                    break;
                case k.ActionBar_itemPadding:
                    this.b = dsVar.g();
                    break;
                case 24:
                    this.c = dsVar.h();
                    break;
                case 32:
                    this.d = dsVar.h();
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        dtVar.a(1, this.a);
        dtVar.a(2, this.b);
        dtVar.b(3, this.c);
        dtVar.b(4, this.d);
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b() + dt.b(1, this.a) + dt.b(2, this.b) + dt.d(3, this.c) + dt.d(4, this.d);
        this.s = b2;
        return b2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof y)) {
            return false;
        }
        y yVar = (y) obj;
        if (this.a != yVar.a) {
            return false;
        }
        if (this.b == null) {
            if (yVar.b != null) {
                return false;
            }
        } else if (!this.b.equals(yVar.b)) {
            return false;
        }
        if (this.c == yVar.c && this.d == yVar.d) {
            return (this.r == null || this.r.isEmpty()) ? yVar.r == null || yVar.r.isEmpty() : this.r.equals(yVar.r);
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((this.b == null ? 0 : this.b.hashCode()) + ((this.a + 527) * 31)) * 31) + ((int) (this.c ^ (this.c >>> 32)))) * 31) + ((int) (this.d ^ (this.d >>> 32)))) * 31;
        if (this.r != null && !this.r.isEmpty()) {
            i = this.r.hashCode();
        }
        return hashCode + i;
    }
}
