package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.query.Query;

public class k implements Parcelable.Creator {
    static void a(QueryRequest queryRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, queryRequest.a);
        c.a(parcel, 2, (Parcelable) queryRequest.b, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public QueryRequest createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        Query query = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    query = (Query) a.a(parcel, a, Query.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new QueryRequest(i, query);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public QueryRequest[] newArray(int i) {
        return new QueryRequest[i];
    }
}
