package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.Contents;

public class CloseContentsRequest implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new q();
    final int a;
    final Contents b;
    final Boolean c;

    CloseContentsRequest(int i, Contents contents, Boolean bool) {
        this.a = i;
        this.b = contents;
        this.c = bool;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        q.a(this, parcel, i);
    }
}
