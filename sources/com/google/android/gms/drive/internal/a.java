package com.google.android.gms.drive.internal;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.DriveId;

public class a implements Parcelable.Creator {
    static void a(AddEventListenerRequest addEventListenerRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, addEventListenerRequest.a);
        c.a(parcel, 2, (Parcelable) addEventListenerRequest.b, i, false);
        c.a(parcel, 3, addEventListenerRequest.c);
        c.a(parcel, 4, (Parcelable) addEventListenerRequest.d, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public AddEventListenerRequest createFromParcel(Parcel parcel) {
        PendingIntent pendingIntent;
        int i;
        DriveId driveId;
        int i2;
        PendingIntent pendingIntent2 = null;
        int i3 = 0;
        int b = com.google.android.gms.common.internal.safeparcel.a.b(parcel);
        DriveId driveId2 = null;
        int i4 = 0;
        while (parcel.dataPosition() < b) {
            int a = com.google.android.gms.common.internal.safeparcel.a.a(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.a.a(a)) {
                case 1:
                    i = i3;
                    driveId = driveId2;
                    i2 = com.google.android.gms.common.internal.safeparcel.a.g(parcel, a);
                    pendingIntent = pendingIntent2;
                    break;
                case 2:
                    i2 = i4;
                    driveId = (DriveId) com.google.android.gms.common.internal.safeparcel.a.a(parcel, a, DriveId.CREATOR);
                    pendingIntent = pendingIntent2;
                    i = i3;
                    break;
                case 3:
                    driveId = driveId2;
                    i2 = i4;
                    i = com.google.android.gms.common.internal.safeparcel.a.g(parcel, a);
                    pendingIntent = pendingIntent2;
                    break;
                case 4:
                    pendingIntent = (PendingIntent) com.google.android.gms.common.internal.safeparcel.a.a(parcel, a, PendingIntent.CREATOR);
                    i = i3;
                    driveId = driveId2;
                    i2 = i4;
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.a.b(parcel, a);
                    pendingIntent = pendingIntent2;
                    i = i3;
                    driveId = driveId2;
                    i2 = i4;
                    break;
            }
            i4 = i2;
            driveId2 = driveId;
            i3 = i;
            pendingIntent2 = pendingIntent;
        }
        if (parcel.dataPosition() == b) {
            return new AddEventListenerRequest(i4, driveId2, i3, pendingIntent2);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public AddEventListenerRequest[] newArray(int i) {
        return new AddEventListenerRequest[i];
    }
}
