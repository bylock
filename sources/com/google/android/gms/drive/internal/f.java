package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class f implements Parcelable.Creator {
    static void a(OnListParentsResponse onListParentsResponse, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, onListParentsResponse.a);
        c.a(parcel, 2, (Parcelable) onListParentsResponse.b, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public OnListParentsResponse createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        DataHolder dataHolder = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    dataHolder = (DataHolder) a.a(parcel, a, DataHolder.a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new OnListParentsResponse(i, dataHolder);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public OnListParentsResponse[] newArray(int i) {
        return new OnListParentsResponse[i];
    }
}
