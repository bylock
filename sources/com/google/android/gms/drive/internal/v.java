package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class v implements Parcelable.Creator {
    static void a(DisconnectRequest disconnectRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, disconnectRequest.a);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public DisconnectRequest createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new DisconnectRequest(i);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public DisconnectRequest[] newArray(int i) {
        return new DisconnectRequest[i];
    }
}
