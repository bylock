package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.ConflictEvent;

public class OnEventResponse implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new d();
    final int a;
    final int b;
    final ChangeEvent c;
    final ConflictEvent d;

    OnEventResponse(int i, int i2, ChangeEvent changeEvent, ConflictEvent conflictEvent) {
        this.a = i;
        this.b = i2;
        this.c = changeEvent;
        this.d = conflictEvent;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        d.a(this, parcel, i);
    }
}
