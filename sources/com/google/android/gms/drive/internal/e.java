package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class e implements Parcelable.Creator {
    static void a(OnListEntriesResponse onListEntriesResponse, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, onListEntriesResponse.a);
        c.a(parcel, 2, (Parcelable) onListEntriesResponse.b, i, false);
        c.a(parcel, 3, onListEntriesResponse.c);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public OnListEntriesResponse createFromParcel(Parcel parcel) {
        boolean c;
        DataHolder dataHolder;
        int i;
        boolean z = false;
        int b = a.b(parcel);
        DataHolder dataHolder2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    dataHolder = dataHolder2;
                    i = a.g(parcel, a);
                    c = z;
                    break;
                case 2:
                    DataHolder dataHolder3 = (DataHolder) a.a(parcel, a, DataHolder.a);
                    i = i2;
                    c = z;
                    dataHolder = dataHolder3;
                    break;
                case 3:
                    c = a.c(parcel, a);
                    dataHolder = dataHolder2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, a);
                    c = z;
                    dataHolder = dataHolder2;
                    i = i2;
                    break;
            }
            i2 = i;
            dataHolder2 = dataHolder;
            z = c;
        }
        if (parcel.dataPosition() == b) {
            return new OnListEntriesResponse(i2, dataHolder2, z);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public OnListEntriesResponse[] newArray(int i) {
        return new OnListEntriesResponse[i];
    }
}
