package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public class p implements Parcelable.Creator {
    static void a(CloseContentsAndUpdateMetadataRequest closeContentsAndUpdateMetadataRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, closeContentsAndUpdateMetadataRequest.a);
        c.a(parcel, 2, (Parcelable) closeContentsAndUpdateMetadataRequest.b, i, false);
        c.a(parcel, 3, (Parcelable) closeContentsAndUpdateMetadataRequest.c, i, false);
        c.a(parcel, 4, (Parcelable) closeContentsAndUpdateMetadataRequest.d, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public CloseContentsAndUpdateMetadataRequest createFromParcel(Parcel parcel) {
        Contents contents;
        MetadataBundle metadataBundle;
        DriveId driveId;
        int i;
        Contents contents2 = null;
        int b = a.b(parcel);
        int i2 = 0;
        MetadataBundle metadataBundle2 = null;
        DriveId driveId2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    metadataBundle = metadataBundle2;
                    driveId = driveId2;
                    i = a.g(parcel, a);
                    contents = contents2;
                    break;
                case 2:
                    i = i2;
                    driveId = (DriveId) a.a(parcel, a, DriveId.CREATOR);
                    contents = contents2;
                    metadataBundle = metadataBundle2;
                    break;
                case 3:
                    driveId = driveId2;
                    i = i2;
                    metadataBundle = (MetadataBundle) a.a(parcel, a, MetadataBundle.CREATOR);
                    contents = contents2;
                    break;
                case 4:
                    contents = (Contents) a.a(parcel, a, Contents.CREATOR);
                    metadataBundle = metadataBundle2;
                    driveId = driveId2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, a);
                    contents = contents2;
                    metadataBundle = metadataBundle2;
                    driveId = driveId2;
                    i = i2;
                    break;
            }
            i2 = i;
            driveId2 = driveId;
            metadataBundle2 = metadataBundle;
            contents2 = contents;
        }
        if (parcel.dataPosition() == b) {
            return new CloseContentsAndUpdateMetadataRequest(i2, driveId2, metadataBundle2, contents2);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public CloseContentsAndUpdateMetadataRequest[] newArray(int i) {
        return new CloseContentsAndUpdateMetadataRequest[i];
    }
}
