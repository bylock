package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.internal.y;
import com.google.android.gms.internal.aw;
import com.google.android.gms.internal.ea;

public class DriveId implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new b();
    final int a;
    final String b;
    final long c;
    final long d;
    private volatile String e = null;

    DriveId(int i, String str, long j, long j2) {
        boolean z = false;
        this.a = i;
        this.b = str;
        aw.b(!"".equals(str));
        aw.b((str == null && j == -1) ? z : true);
        this.c = j;
        this.d = j2;
    }

    public final String a() {
        if (this.e == null) {
            this.e = "DriveId:" + Base64.encodeToString(b(), 10);
        }
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final byte[] b() {
        y yVar = new y();
        yVar.a = this.a;
        yVar.b = this.b == null ? "" : this.b;
        yVar.c = this.c;
        yVar.d = this.d;
        return ea.a(yVar);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DriveId)) {
            return false;
        }
        DriveId driveId = (DriveId) obj;
        if (driveId.d == this.d) {
            return (driveId.c == -1 && this.c == -1) ? driveId.b.equals(this.b) : driveId.c == this.c;
        }
        Log.w("DriveId", "Attempt to compare invalid DriveId detected. Has local storage been cleared?");
        return false;
    }

    public int hashCode() {
        return this.c == -1 ? this.b.hashCode() : (String.valueOf(this.d) + String.valueOf(this.c)).hashCode();
    }

    public String toString() {
        return a();
    }

    public void writeToParcel(Parcel parcel, int i) {
        b.a(this, parcel, i);
    }
}
