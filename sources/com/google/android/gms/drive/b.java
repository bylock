package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;

public class b implements Parcelable.Creator {
    static void a(DriveId driveId, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, driveId.a);
        c.a(parcel, 2, driveId.b, false);
        c.a(parcel, 3, driveId.c);
        c.a(parcel, 4, driveId.d);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public DriveId createFromParcel(Parcel parcel) {
        long j = 0;
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        long j2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    j2 = a.i(parcel, a);
                    break;
                case 4:
                    j = a.i(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new DriveId(i, str, j2, j);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public DriveId[] newArray(int i) {
        return new DriveId[i];
    }
}
