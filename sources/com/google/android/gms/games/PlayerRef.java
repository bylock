package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.e;

public final class PlayerRef extends e implements Player {
    private final e c;

    public PlayerRef(DataHolder dataHolder, int i) {
        this(dataHolder, i, null);
    }

    public PlayerRef(DataHolder dataHolder, int i, String str) {
        super(dataHolder, i);
        this.c = new e(str);
    }

    @Override // com.google.android.gms.games.Player
    public String b() {
        return e(this.c.a);
    }

    @Override // com.google.android.gms.games.Player
    public String c() {
        return e(this.c.b);
    }

    @Override // com.google.android.gms.games.Player
    public Uri d() {
        return g(this.c.c);
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.Player
    public String e() {
        return e(this.c.d);
    }

    @Override // com.google.android.gms.common.data.e
    public boolean equals(Object obj) {
        return PlayerEntity.a(this, obj);
    }

    @Override // com.google.android.gms.games.Player
    public Uri f() {
        return g(this.c.e);
    }

    @Override // com.google.android.gms.games.Player
    public String g() {
        return e(this.c.f);
    }

    @Override // com.google.android.gms.games.Player
    public long h() {
        return b(this.c.g);
    }

    @Override // com.google.android.gms.common.data.e
    public int hashCode() {
        return PlayerEntity.a(this);
    }

    @Override // com.google.android.gms.games.Player
    public long i() {
        if (!a_(this.c.i)) {
            return -1;
        }
        return b(this.c.i);
    }

    @Override // com.google.android.gms.games.Player
    public int j() {
        return c(this.c.h);
    }

    /* renamed from: k */
    public Player a() {
        return new PlayerEntity(this);
    }

    public String toString() {
        return PlayerEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((PlayerEntity) a()).writeToParcel(parcel, i);
    }
}
