package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;

final class c extends d {
    c() {
    }

    @Override // com.google.android.gms.games.d
    /* renamed from: a */
    public PlayerEntity createFromParcel(Parcel parcel) {
        if (PlayerEntity.a(PlayerEntity.m()) || PlayerEntity.a(PlayerEntity.class.getCanonicalName())) {
            return super.createFromParcel(parcel);
        }
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        String readString3 = parcel.readString();
        String readString4 = parcel.readString();
        return new PlayerEntity(4, readString, readString2, readString3 == null ? null : Uri.parse(readString3), readString4 == null ? null : Uri.parse(readString4), parcel.readLong(), -1, -1, null, null);
    }
}
