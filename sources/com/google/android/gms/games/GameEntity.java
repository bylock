package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.internal.ar;

public final class GameEntity extends GamesDowngradeableSafeParcel implements Game {
    public static final Parcelable.Creator CREATOR = new a();
    private final int a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private final String g;
    private final Uri h;
    private final Uri i;
    private final Uri j;
    private final boolean k;
    private final boolean l;
    private final String m;
    private final int n;
    private final int o;
    private final int p;
    private final boolean q;
    private final boolean r;
    private final String s;
    private final String t;
    private final String u;
    private final boolean v;
    private final boolean w;

    GameEntity(int i2, String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Uri uri2, Uri uri3, boolean z, boolean z2, String str7, int i3, int i4, int i5, boolean z3, boolean z4, String str8, String str9, String str10, boolean z5, boolean z6) {
        this.a = i2;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = uri;
        this.s = str8;
        this.i = uri2;
        this.t = str9;
        this.j = uri3;
        this.u = str10;
        this.k = z;
        this.l = z2;
        this.m = str7;
        this.n = i3;
        this.o = i4;
        this.p = i5;
        this.q = z3;
        this.r = z4;
        this.v = z5;
        this.w = z6;
    }

    public GameEntity(Game game) {
        this.a = 3;
        this.b = game.b();
        this.d = game.d();
        this.e = game.e();
        this.f = game.f();
        this.g = game.g();
        this.c = game.c();
        this.h = game.h();
        this.s = game.i();
        this.i = game.j();
        this.t = game.k();
        this.j = game.l();
        this.u = game.m();
        this.k = game.n();
        this.l = game.q();
        this.m = game.r();
        this.n = game.s();
        this.o = game.t();
        this.p = game.u();
        this.q = game.v();
        this.r = game.w();
        this.v = game.o();
        this.w = game.p();
    }

    static int a(Game game) {
        return ar.a(game.b(), game.c(), game.d(), game.e(), game.f(), game.g(), game.h(), game.j(), game.l(), Boolean.valueOf(game.n()), Boolean.valueOf(game.q()), game.r(), Integer.valueOf(game.s()), Integer.valueOf(game.t()), Integer.valueOf(game.u()), Boolean.valueOf(game.v()), Boolean.valueOf(game.w()), Boolean.valueOf(game.o()), Boolean.valueOf(game.p()));
    }

    static boolean a(Game game, Object obj) {
        if (!(obj instanceof Game)) {
            return false;
        }
        if (game == obj) {
            return true;
        }
        Game game2 = (Game) obj;
        if (ar.a(game2.b(), game.b()) && ar.a(game2.c(), game.c()) && ar.a(game2.d(), game.d()) && ar.a(game2.e(), game.e()) && ar.a(game2.f(), game.f()) && ar.a(game2.g(), game.g()) && ar.a(game2.h(), game.h()) && ar.a(game2.j(), game.j()) && ar.a(game2.l(), game.l()) && ar.a(Boolean.valueOf(game2.n()), Boolean.valueOf(game.n())) && ar.a(Boolean.valueOf(game2.q()), Boolean.valueOf(game.q())) && ar.a(game2.r(), game.r()) && ar.a(Integer.valueOf(game2.s()), Integer.valueOf(game.s())) && ar.a(Integer.valueOf(game2.t()), Integer.valueOf(game.t())) && ar.a(Integer.valueOf(game2.u()), Integer.valueOf(game.u())) && ar.a(Boolean.valueOf(game2.v()), Boolean.valueOf(game.v()))) {
            if (ar.a(Boolean.valueOf(game2.w()), Boolean.valueOf(game.w() && ar.a(Boolean.valueOf(game2.o()), Boolean.valueOf(game.o())) && ar.a(Boolean.valueOf(game2.p()), Boolean.valueOf(game.p()))))) {
                return true;
            }
        }
        return false;
    }

    static String b(Game game) {
        return ar.a(game).a("ApplicationId", game.b()).a("DisplayName", game.c()).a("PrimaryCategory", game.d()).a("SecondaryCategory", game.e()).a("Description", game.f()).a("DeveloperName", game.g()).a("IconImageUri", game.h()).a("IconImageUrl", game.i()).a("HiResImageUri", game.j()).a("HiResImageUrl", game.k()).a("FeaturedImageUri", game.l()).a("FeaturedImageUrl", game.m()).a("PlayEnabledGame", Boolean.valueOf(game.n())).a("InstanceInstalled", Boolean.valueOf(game.q())).a("InstancePackageName", game.r()).a("AchievementTotalCount", Integer.valueOf(game.t())).a("LeaderboardCount", Integer.valueOf(game.u())).a("RealTimeMultiplayerEnabled", Boolean.valueOf(game.v())).a("TurnBasedMultiplayerEnabled", Boolean.valueOf(game.w())).toString();
    }

    @Override // com.google.android.gms.games.Game
    public String b() {
        return this.b;
    }

    @Override // com.google.android.gms.games.Game
    public String c() {
        return this.c;
    }

    @Override // com.google.android.gms.games.Game
    public String d() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.Game
    public String e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    @Override // com.google.android.gms.games.Game
    public String f() {
        return this.f;
    }

    @Override // com.google.android.gms.games.Game
    public String g() {
        return this.g;
    }

    @Override // com.google.android.gms.games.Game
    public Uri h() {
        return this.h;
    }

    public int hashCode() {
        return a(this);
    }

    @Override // com.google.android.gms.games.Game
    public String i() {
        return this.s;
    }

    @Override // com.google.android.gms.games.Game
    public Uri j() {
        return this.i;
    }

    @Override // com.google.android.gms.games.Game
    public String k() {
        return this.t;
    }

    @Override // com.google.android.gms.games.Game
    public Uri l() {
        return this.j;
    }

    @Override // com.google.android.gms.games.Game
    public String m() {
        return this.u;
    }

    @Override // com.google.android.gms.games.Game
    public boolean n() {
        return this.k;
    }

    @Override // com.google.android.gms.games.Game
    public boolean o() {
        return this.v;
    }

    @Override // com.google.android.gms.games.Game
    public boolean p() {
        return this.w;
    }

    @Override // com.google.android.gms.games.Game
    public boolean q() {
        return this.l;
    }

    @Override // com.google.android.gms.games.Game
    public String r() {
        return this.m;
    }

    @Override // com.google.android.gms.games.Game
    public int s() {
        return this.n;
    }

    @Override // com.google.android.gms.games.Game
    public int t() {
        return this.o;
    }

    public String toString() {
        return b(this);
    }

    @Override // com.google.android.gms.games.Game
    public int u() {
        return this.p;
    }

    @Override // com.google.android.gms.games.Game
    public boolean v() {
        return this.q;
    }

    @Override // com.google.android.gms.games.Game
    public boolean w() {
        return this.r;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        String str = null;
        if (!C()) {
            b.a(this, parcel, i2);
            return;
        }
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h == null ? null : this.h.toString());
        parcel.writeString(this.i == null ? null : this.i.toString());
        if (this.j != null) {
            str = this.j.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.k ? 1 : 0);
        if (!this.l) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeString(this.m);
        parcel.writeInt(this.n);
        parcel.writeInt(this.o);
        parcel.writeInt(this.p);
    }

    public int x() {
        return this.a;
    }

    /* renamed from: y */
    public Game a() {
        return this;
    }
}
