package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.b.k;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;

public class b implements Parcelable.Creator {
    static void a(GameEntity gameEntity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, gameEntity.b(), false);
        c.a(parcel, 2, gameEntity.c(), false);
        c.a(parcel, 3, gameEntity.d(), false);
        c.a(parcel, 4, gameEntity.e(), false);
        c.a(parcel, 5, gameEntity.f(), false);
        c.a(parcel, 6, gameEntity.g(), false);
        c.a(parcel, 7, (Parcelable) gameEntity.h(), i, false);
        c.a(parcel, 8, (Parcelable) gameEntity.j(), i, false);
        c.a(parcel, 9, (Parcelable) gameEntity.l(), i, false);
        c.a(parcel, 10, gameEntity.n());
        c.a(parcel, 11, gameEntity.q());
        c.a(parcel, 12, gameEntity.r(), false);
        c.a(parcel, 13, gameEntity.s());
        c.a(parcel, 14, gameEntity.t());
        c.a(parcel, 15, gameEntity.u());
        c.a(parcel, 17, gameEntity.w());
        c.a(parcel, 16, gameEntity.v());
        c.a(parcel, 1000, gameEntity.x());
        c.a(parcel, 19, gameEntity.k(), false);
        c.a(parcel, 18, gameEntity.i(), false);
        c.a(parcel, 21, gameEntity.o());
        c.a(parcel, 20, gameEntity.m(), false);
        c.a(parcel, 22, gameEntity.p());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public GameEntity createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        Uri uri = null;
        Uri uri2 = null;
        Uri uri3 = null;
        boolean z = false;
        boolean z2 = false;
        String str7 = null;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        boolean z3 = false;
        boolean z4 = false;
        String str8 = null;
        String str9 = null;
        String str10 = null;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str = a.n(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    str3 = a.n(parcel, a);
                    break;
                case 4:
                    str4 = a.n(parcel, a);
                    break;
                case 5:
                    str5 = a.n(parcel, a);
                    break;
                case 6:
                    str6 = a.n(parcel, a);
                    break;
                case 7:
                    uri = (Uri) a.a(parcel, a, Uri.CREATOR);
                    break;
                case 8:
                    uri2 = (Uri) a.a(parcel, a, Uri.CREATOR);
                    break;
                case 9:
                    uri3 = (Uri) a.a(parcel, a, Uri.CREATOR);
                    break;
                case 10:
                    z = a.c(parcel, a);
                    break;
                case 11:
                    z2 = a.c(parcel, a);
                    break;
                case 12:
                    str7 = a.n(parcel, a);
                    break;
                case 13:
                    i2 = a.g(parcel, a);
                    break;
                case 14:
                    i3 = a.g(parcel, a);
                    break;
                case 15:
                    i4 = a.g(parcel, a);
                    break;
                case 16:
                    z3 = a.c(parcel, a);
                    break;
                case k.ActionBar_progressBarPadding:
                    z4 = a.c(parcel, a);
                    break;
                case k.ActionBar_itemPadding:
                    str8 = a.n(parcel, a);
                    break;
                case 19:
                    str9 = a.n(parcel, a);
                    break;
                case 20:
                    str10 = a.n(parcel, a);
                    break;
                case 21:
                    z5 = a.c(parcel, a);
                    break;
                case 22:
                    z6 = a.c(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new GameEntity(i, str, str2, str3, str4, str5, str6, uri, uri2, uri3, z, z2, str7, i2, i3, i4, z3, z4, str8, str9, str10, z5, z6);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public GameEntity[] newArray(int i) {
        return new GameEntity[i];
    }
}
