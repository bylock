package com.google.android.gms.games.request;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.internal.ar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class GameRequestEntity implements SafeParcelable, GameRequest {
    public static final a a = new a();
    private final int b;
    private final GameEntity c;
    private final PlayerEntity d;
    private final byte[] e;
    private final String f;
    private final ArrayList g;
    private final int h;
    private final long i;
    private final long j;
    private final Bundle k;
    private final int l;

    GameRequestEntity(int i2, GameEntity gameEntity, PlayerEntity playerEntity, byte[] bArr, String str, ArrayList arrayList, int i3, long j2, long j3, Bundle bundle, int i4) {
        this.b = i2;
        this.c = gameEntity;
        this.d = playerEntity;
        this.e = bArr;
        this.f = str;
        this.g = arrayList;
        this.h = i3;
        this.i = j2;
        this.j = j3;
        this.k = bundle;
        this.l = i4;
    }

    public GameRequestEntity(GameRequest gameRequest) {
        this.b = 2;
        this.c = new GameEntity(gameRequest.e());
        this.d = new PlayerEntity(gameRequest.f());
        this.f = gameRequest.d();
        this.h = gameRequest.i();
        this.i = gameRequest.j();
        this.j = gameRequest.k();
        this.l = gameRequest.l();
        byte[] h2 = gameRequest.h();
        if (h2 == null) {
            this.e = null;
        } else {
            this.e = new byte[h2.length];
            System.arraycopy(h2, 0, this.e, 0, h2.length);
        }
        List n = gameRequest.n();
        int size = n.size();
        this.g = new ArrayList(size);
        this.k = new Bundle();
        for (int i2 = 0; i2 < size; i2++) {
            Player player = (Player) ((Player) n.get(i2)).a();
            String b2 = player.b();
            this.g.add((PlayerEntity) player);
            this.k.putInt(b2, gameRequest.a(b2));
        }
    }

    static int a(GameRequest gameRequest) {
        return ar.a(gameRequest.e(), gameRequest.n(), gameRequest.d(), gameRequest.f(), c(gameRequest), Integer.valueOf(gameRequest.i()), Long.valueOf(gameRequest.j()), Long.valueOf(gameRequest.k()));
    }

    static boolean a(GameRequest gameRequest, Object obj) {
        if (!(obj instanceof GameRequest)) {
            return false;
        }
        if (gameRequest == obj) {
            return true;
        }
        GameRequest gameRequest2 = (GameRequest) obj;
        return ar.a(gameRequest2.e(), gameRequest.e()) && ar.a(gameRequest2.n(), gameRequest.n()) && ar.a(gameRequest2.d(), gameRequest.d()) && ar.a(gameRequest2.f(), gameRequest.f()) && Arrays.equals(c(gameRequest2), c(gameRequest)) && ar.a(Integer.valueOf(gameRequest2.i()), Integer.valueOf(gameRequest.i())) && ar.a(Long.valueOf(gameRequest2.j()), Long.valueOf(gameRequest.j())) && ar.a(Long.valueOf(gameRequest2.k()), Long.valueOf(gameRequest.k()));
    }

    static String b(GameRequest gameRequest) {
        return ar.a(gameRequest).a("Game", gameRequest.e()).a("Sender", gameRequest.f()).a("Recipients", gameRequest.n()).a("Data", gameRequest.h()).a("RequestId", gameRequest.d()).a("Type", Integer.valueOf(gameRequest.i())).a("CreationTimestamp", Long.valueOf(gameRequest.j())).a("ExpirationTimestamp", Long.valueOf(gameRequest.k())).toString();
    }

    private static int[] c(GameRequest gameRequest) {
        List n = gameRequest.n();
        int size = n.size();
        int[] iArr = new int[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr[i2] = gameRequest.a(((Player) n.get(i2)).b());
        }
        return iArr;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public int a(String str) {
        return this.k.getInt(str, 0);
    }

    public int b() {
        return this.b;
    }

    public Bundle c() {
        return this.k;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public String d() {
        return this.f;
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public Game e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public Player f() {
        return this.d;
    }

    /* renamed from: g */
    public GameRequest a() {
        return this;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public byte[] h() {
        return this.e;
    }

    public int hashCode() {
        return a(this);
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public int i() {
        return this.h;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public long j() {
        return this.i;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public long k() {
        return this.j;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public int l() {
        return this.l;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public List n() {
        return new ArrayList(this.g);
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        a.a(this, parcel, i2);
    }
}
