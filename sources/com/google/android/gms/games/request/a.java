package com.google.android.gms.games.request;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.PlayerEntity;
import java.util.ArrayList;

public class a implements Parcelable.Creator {
    static void a(GameRequestEntity gameRequestEntity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, (Parcelable) gameRequestEntity.e(), i, false);
        c.a(parcel, 1000, gameRequestEntity.b());
        c.a(parcel, 2, (Parcelable) gameRequestEntity.f(), i, false);
        c.a(parcel, 3, gameRequestEntity.h(), false);
        c.a(parcel, 4, gameRequestEntity.d(), false);
        c.b(parcel, 5, gameRequestEntity.n(), false);
        c.a(parcel, 7, gameRequestEntity.i());
        c.a(parcel, 9, gameRequestEntity.j());
        c.a(parcel, 10, gameRequestEntity.k());
        c.a(parcel, 11, gameRequestEntity.c(), false);
        c.a(parcel, 12, gameRequestEntity.l());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public GameRequestEntity createFromParcel(Parcel parcel) {
        int b = com.google.android.gms.common.internal.safeparcel.a.b(parcel);
        int i = 0;
        GameEntity gameEntity = null;
        PlayerEntity playerEntity = null;
        byte[] bArr = null;
        String str = null;
        ArrayList arrayList = null;
        int i2 = 0;
        long j = 0;
        long j2 = 0;
        Bundle bundle = null;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = com.google.android.gms.common.internal.safeparcel.a.a(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.a.a(a)) {
                case 1:
                    gameEntity = (GameEntity) com.google.android.gms.common.internal.safeparcel.a.a(parcel, a, GameEntity.CREATOR);
                    break;
                case 2:
                    playerEntity = (PlayerEntity) com.google.android.gms.common.internal.safeparcel.a.a(parcel, a, PlayerEntity.CREATOR);
                    break;
                case 3:
                    bArr = com.google.android.gms.common.internal.safeparcel.a.q(parcel, a);
                    break;
                case 4:
                    str = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 5:
                    arrayList = com.google.android.gms.common.internal.safeparcel.a.c(parcel, a, PlayerEntity.CREATOR);
                    break;
                case 7:
                    i2 = com.google.android.gms.common.internal.safeparcel.a.g(parcel, a);
                    break;
                case 9:
                    j = com.google.android.gms.common.internal.safeparcel.a.i(parcel, a);
                    break;
                case 10:
                    j2 = com.google.android.gms.common.internal.safeparcel.a.i(parcel, a);
                    break;
                case 11:
                    bundle = com.google.android.gms.common.internal.safeparcel.a.p(parcel, a);
                    break;
                case 12:
                    i3 = com.google.android.gms.common.internal.safeparcel.a.g(parcel, a);
                    break;
                case 1000:
                    i = com.google.android.gms.common.internal.safeparcel.a.g(parcel, a);
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new GameRequestEntity(i, gameEntity, playerEntity, bArr, str, arrayList, i2, j, j2, bundle, i3);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public GameRequestEntity[] newArray(int i) {
        return new GameRequestEntity[i];
    }
}
