package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class d implements Parcelable.Creator {
    static void a(PlayerEntity playerEntity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, playerEntity.b(), false);
        c.a(parcel, 1000, playerEntity.k());
        c.a(parcel, 2, playerEntity.c(), false);
        c.a(parcel, 3, (Parcelable) playerEntity.d(), i, false);
        c.a(parcel, 4, (Parcelable) playerEntity.f(), i, false);
        c.a(parcel, 5, playerEntity.h());
        c.a(parcel, 6, playerEntity.j());
        c.a(parcel, 7, playerEntity.i());
        c.a(parcel, 8, playerEntity.e(), false);
        c.a(parcel, 9, playerEntity.g(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public PlayerEntity createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        Uri uri = null;
        Uri uri2 = null;
        long j = 0;
        int i2 = 0;
        long j2 = 0;
        String str3 = null;
        String str4 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str = a.n(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    uri = (Uri) a.a(parcel, a, Uri.CREATOR);
                    break;
                case 4:
                    uri2 = (Uri) a.a(parcel, a, Uri.CREATOR);
                    break;
                case 5:
                    j = a.i(parcel, a);
                    break;
                case 6:
                    i2 = a.g(parcel, a);
                    break;
                case 7:
                    j2 = a.i(parcel, a);
                    break;
                case 8:
                    str3 = a.n(parcel, a);
                    break;
                case 9:
                    str4 = a.n(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new PlayerEntity(i, str, str2, uri, uri2, j, i2, j2, str3, str4);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public PlayerEntity[] newArray(int i) {
        return new PlayerEntity[i];
    }
}
