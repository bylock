package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.d;

public interface Game extends Parcelable, d {
    String b();

    String c();

    String d();

    String e();

    String f();

    String g();

    Uri h();

    @Deprecated
    String i();

    Uri j();

    @Deprecated
    String k();

    Uri l();

    @Deprecated
    String m();

    boolean n();

    boolean o();

    boolean p();

    boolean q();

    String r();

    int s();

    int t();

    int u();

    boolean v();

    boolean w();
}
