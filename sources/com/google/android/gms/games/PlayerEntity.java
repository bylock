package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.internal.ao;
import com.google.android.gms.internal.ar;

public final class PlayerEntity extends GamesDowngradeableSafeParcel implements Player {
    public static final Parcelable.Creator CREATOR = new c();
    private final int a;
    private final String b;
    private final String c;
    private final Uri d;
    private final Uri e;
    private final long f;
    private final int g;
    private final long h;
    private final String i;
    private final String j;

    PlayerEntity(int i2, String str, String str2, Uri uri, Uri uri2, long j2, int i3, long j3, String str3, String str4) {
        this.a = i2;
        this.b = str;
        this.c = str2;
        this.d = uri;
        this.i = str3;
        this.e = uri2;
        this.j = str4;
        this.f = j2;
        this.g = i3;
        this.h = j3;
    }

    public PlayerEntity(Player player) {
        this.a = 4;
        this.b = player.b();
        this.c = player.c();
        this.d = player.d();
        this.i = player.e();
        this.e = player.f();
        this.j = player.g();
        this.f = player.h();
        this.g = player.j();
        this.h = player.i();
        ao.a((Object) this.b);
        ao.a((Object) this.c);
        ao.a(this.f > 0);
    }

    static int a(Player player) {
        return ar.a(player.b(), player.c(), player.d(), player.f(), Long.valueOf(player.h()));
    }

    static boolean a(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return ar.a(player2.b(), player.b()) && ar.a(player2.c(), player.c()) && ar.a(player2.d(), player.d()) && ar.a(player2.f(), player.f()) && ar.a(Long.valueOf(player2.h()), Long.valueOf(player.h()));
    }

    static String b(Player player) {
        return ar.a(player).a("PlayerId", player.b()).a("DisplayName", player.c()).a("IconImageUri", player.d()).a("IconImageUrl", player.e()).a("HiResImageUri", player.f()).a("HiResImageUrl", player.g()).a("RetrievedTimestamp", Long.valueOf(player.h())).toString();
    }

    @Override // com.google.android.gms.games.Player
    public String b() {
        return this.b;
    }

    @Override // com.google.android.gms.games.Player
    public String c() {
        return this.c;
    }

    @Override // com.google.android.gms.games.Player
    public Uri d() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.Player
    public String e() {
        return this.i;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    @Override // com.google.android.gms.games.Player
    public Uri f() {
        return this.e;
    }

    @Override // com.google.android.gms.games.Player
    public String g() {
        return this.j;
    }

    @Override // com.google.android.gms.games.Player
    public long h() {
        return this.f;
    }

    public int hashCode() {
        return a(this);
    }

    @Override // com.google.android.gms.games.Player
    public long i() {
        return this.h;
    }

    @Override // com.google.android.gms.games.Player
    public int j() {
        return this.g;
    }

    public int k() {
        return this.a;
    }

    /* renamed from: l */
    public Player a() {
        return this;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        String str = null;
        if (!C()) {
            d.a(this, parcel, i2);
            return;
        }
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d == null ? null : this.d.toString());
        if (this.e != null) {
            str = this.e.toString();
        }
        parcel.writeString(str);
        parcel.writeLong(this.f);
    }
}
