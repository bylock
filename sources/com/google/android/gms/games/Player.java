package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.d;

public interface Player extends Parcelable, d {
    String b();

    String c();

    Uri d();

    @Deprecated
    String e();

    Uri f();

    @Deprecated
    String g();

    long h();

    long i();

    int j();
}
