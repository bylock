package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.internal.a.a;
import com.google.android.gms.internal.aw;

public final class ParticipantResult implements SafeParcelable {
    public static final e a = new e();
    private final int b;
    private final String c;
    private final int d;
    private final int e;

    public ParticipantResult(int i, String str, int i2, int i3) {
        this.b = i;
        this.c = (String) aw.a((Object) str);
        aw.a(a.a(i2));
        this.d = i2;
        this.e = i3;
    }

    public ParticipantResult(String str, int i, int i2) {
        this(1, str, i, i2);
    }

    public int a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }

    public int c() {
        return this.d;
    }

    public int d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        e.a(this, parcel, i);
    }
}
