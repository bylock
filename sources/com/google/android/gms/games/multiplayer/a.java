package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

final class a extends b {
    a() {
    }

    @Override // com.google.android.gms.games.multiplayer.b
    /* renamed from: a */
    public InvitationEntity createFromParcel(Parcel parcel) {
        if ((InvitationEntity.b(InvitationEntity.B())) || (InvitationEntity.b(InvitationEntity.class.getCanonicalName()))) {
            return super.createFromParcel(parcel);
        }
        GameEntity gameEntity = (GameEntity) GameEntity.CREATOR.createFromParcel(parcel);
        String readString = parcel.readString();
        long readLong = parcel.readLong();
        int readInt = parcel.readInt();
        ParticipantEntity participantEntity = (ParticipantEntity) ParticipantEntity.CREATOR.createFromParcel(parcel);
        int readInt2 = parcel.readInt();
        ArrayList arrayList = new ArrayList(readInt2);
        for (int i = 0; i < readInt2; i++) {
            arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
        }
        return new InvitationEntity(2, gameEntity, readString, readLong, readInt, participantEntity, arrayList, -1, 0);
    }
}
