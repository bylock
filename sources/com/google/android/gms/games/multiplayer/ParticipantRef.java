package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class ParticipantRef extends e implements Participant {
    private final PlayerRef c;

    public ParticipantRef(DataHolder dataHolder, int i) {
        super(dataHolder, i);
        this.c = new PlayerRef(dataHolder, i);
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public int b() {
        return c("player_status");
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public String c() {
        return e("client_address");
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public int d() {
        return c("capabilities");
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public boolean e() {
        return c("connected") > 0;
    }

    @Override // com.google.android.gms.common.data.e
    public boolean equals(Object obj) {
        return ParticipantEntity.a(this, obj);
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public String f() {
        return h("external_player_id") ? e("default_display_name") : this.c.c();
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public Uri g() {
        return h("external_player_id") ? g("default_display_image_uri") : this.c.d();
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public String h() {
        return h("external_player_id") ? e("default_display_image_url") : this.c.e();
    }

    @Override // com.google.android.gms.common.data.e
    public int hashCode() {
        return ParticipantEntity.a(this);
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public Uri i() {
        return h("external_player_id") ? g("default_display_hi_res_image_uri") : this.c.f();
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public String j() {
        return h("external_player_id") ? e("default_display_hi_res_image_url") : this.c.g();
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public String k() {
        return e("external_participant_id");
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public Player l() {
        if (h("external_player_id")) {
            return null;
        }
        return this.c;
    }

    @Override // com.google.android.gms.games.multiplayer.Participant
    public ParticipantResult m() {
        if (h("result_type")) {
            return null;
        }
        return new ParticipantResult(k(), c("result_type"), c("placing"));
    }

    /* renamed from: n */
    public Participant a() {
        return new ParticipantEntity(this);
    }

    public String toString() {
        return ParticipantEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((ParticipantEntity) a()).writeToParcel(parcel, i);
    }
}
