package com.google.android.gms.games.multiplayer;

import android.os.Parcelable;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Game;

public interface Invitation extends Parcelable, d, f {
    Game d();

    String e();

    Participant f();

    long g();

    int h();

    int i();

    int j();
}
