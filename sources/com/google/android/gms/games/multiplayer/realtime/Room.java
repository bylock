package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.multiplayer.f;

public interface Room extends Parcelable, d, f {
    String b();

    String c();

    long d();

    int e();

    String f();

    int g();

    Bundle h();

    int i();
}
