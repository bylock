package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.multiplayer.ParticipantRef;
import java.util.ArrayList;

public final class RoomRef extends e implements Room {
    private final int c;

    @Override // com.google.android.gms.games.multiplayer.realtime.Room
    public String b() {
        return e("external_match_id");
    }

    @Override // com.google.android.gms.games.multiplayer.realtime.Room
    public String c() {
        return e("creator_external");
    }

    @Override // com.google.android.gms.games.multiplayer.realtime.Room
    public long d() {
        return b("creation_timestamp");
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.multiplayer.realtime.Room
    public int e() {
        return c("status");
    }

    @Override // com.google.android.gms.common.data.e
    public boolean equals(Object obj) {
        return RoomEntity.a(this, obj);
    }

    @Override // com.google.android.gms.games.multiplayer.realtime.Room
    public String f() {
        return e("description");
    }

    @Override // com.google.android.gms.games.multiplayer.realtime.Room
    public int g() {
        return c("variant");
    }

    @Override // com.google.android.gms.games.multiplayer.realtime.Room
    public Bundle h() {
        if (!d("has_automatch_criteria")) {
            return null;
        }
        return b.a(c("automatch_min_players"), c("automatch_max_players"), b("automatch_bit_mask"));
    }

    @Override // com.google.android.gms.common.data.e
    public int hashCode() {
        return RoomEntity.a(this);
    }

    @Override // com.google.android.gms.games.multiplayer.realtime.Room
    public int i() {
        return c("automatch_wait_estimate_sec");
    }

    /* renamed from: j */
    public Room a() {
        return new RoomEntity(this);
    }

    @Override // com.google.android.gms.games.multiplayer.f
    public ArrayList l() {
        ArrayList arrayList = new ArrayList(this.c);
        for (int i = 0; i < this.c; i++) {
            arrayList.add(new ParticipantRef(this.a, this.b + i));
        }
        return arrayList;
    }

    public String toString() {
        return RoomEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((RoomEntity) a()).writeToParcel(parcel, i);
    }
}
