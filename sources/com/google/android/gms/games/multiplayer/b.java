package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

public class b implements Parcelable.Creator {
    static void a(InvitationEntity invitationEntity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, (Parcelable) invitationEntity.d(), i, false);
        c.a(parcel, 1000, invitationEntity.b());
        c.a(parcel, 2, invitationEntity.e(), false);
        c.a(parcel, 3, invitationEntity.g());
        c.a(parcel, 4, invitationEntity.h());
        c.a(parcel, 5, (Parcelable) invitationEntity.f(), i, false);
        c.b(parcel, 6, invitationEntity.l(), false);
        c.a(parcel, 7, invitationEntity.i());
        c.a(parcel, 8, invitationEntity.j());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public InvitationEntity createFromParcel(Parcel parcel) {
        ArrayList arrayList = null;
        int i = 0;
        int b = a.b(parcel);
        long j = 0;
        int i2 = 0;
        ParticipantEntity participantEntity = null;
        int i3 = 0;
        String str = null;
        GameEntity gameEntity = null;
        int i4 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    gameEntity = (GameEntity) a.a(parcel, a, GameEntity.CREATOR);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    j = a.i(parcel, a);
                    break;
                case 4:
                    i3 = a.g(parcel, a);
                    break;
                case 5:
                    participantEntity = (ParticipantEntity) a.a(parcel, a, ParticipantEntity.CREATOR);
                    break;
                case 6:
                    arrayList = a.c(parcel, a, ParticipantEntity.CREATOR);
                    break;
                case 7:
                    i2 = a.g(parcel, a);
                    break;
                case 8:
                    i = a.g(parcel, a);
                    break;
                case 1000:
                    i4 = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new InvitationEntity(i4, gameEntity, str, j, i3, participantEntity, arrayList, i2, i);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public InvitationEntity[] newArray(int i) {
        return new InvitationEntity[i];
    }
}
