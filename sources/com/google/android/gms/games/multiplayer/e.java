package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class e implements Parcelable.Creator {
    static void a(ParticipantResult participantResult, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, participantResult.b(), false);
        c.a(parcel, 1000, participantResult.a());
        c.a(parcel, 2, participantResult.c());
        c.a(parcel, 3, participantResult.d());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ParticipantResult createFromParcel(Parcel parcel) {
        int i = 0;
        int b = a.b(parcel);
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str = a.n(parcel, a);
                    break;
                case 2:
                    i2 = a.g(parcel, a);
                    break;
                case 3:
                    i = a.g(parcel, a);
                    break;
                case 1000:
                    i3 = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ParticipantResult(i3, str, i2, i);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ParticipantResult[] newArray(int i) {
        return new ParticipantResult[i];
    }
}
