package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.games.PlayerEntity;

public class d implements Parcelable.Creator {
    static void a(ParticipantEntity participantEntity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, participantEntity.k(), false);
        c.a(parcel, 2, participantEntity.f(), false);
        c.a(parcel, 3, (Parcelable) participantEntity.g(), i, false);
        c.a(parcel, 4, (Parcelable) participantEntity.i(), i, false);
        c.a(parcel, 5, participantEntity.b());
        c.a(parcel, 6, participantEntity.c(), false);
        c.a(parcel, 7, participantEntity.e());
        c.a(parcel, 8, (Parcelable) participantEntity.l(), i, false);
        c.a(parcel, 9, participantEntity.d());
        c.a(parcel, 10, (Parcelable) participantEntity.m(), i, false);
        c.a(parcel, 11, participantEntity.h(), false);
        c.a(parcel, 12, participantEntity.j(), false);
        c.a(parcel, 1000, participantEntity.n());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ParticipantEntity createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        Uri uri = null;
        Uri uri2 = null;
        int i2 = 0;
        String str3 = null;
        boolean z = false;
        PlayerEntity playerEntity = null;
        int i3 = 0;
        ParticipantResult participantResult = null;
        String str4 = null;
        String str5 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str = a.n(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    uri = (Uri) a.a(parcel, a, Uri.CREATOR);
                    break;
                case 4:
                    uri2 = (Uri) a.a(parcel, a, Uri.CREATOR);
                    break;
                case 5:
                    i2 = a.g(parcel, a);
                    break;
                case 6:
                    str3 = a.n(parcel, a);
                    break;
                case 7:
                    z = a.c(parcel, a);
                    break;
                case 8:
                    playerEntity = (PlayerEntity) a.a(parcel, a, PlayerEntity.CREATOR);
                    break;
                case 9:
                    i3 = a.g(parcel, a);
                    break;
                case 10:
                    participantResult = (ParticipantResult) a.a(parcel, a, ParticipantResult.a);
                    break;
                case 11:
                    str4 = a.n(parcel, a);
                    break;
                case 12:
                    str5 = a.n(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ParticipantEntity(i, str, str2, uri, uri2, i2, str3, z, playerEntity, i3, participantResult, str4, str5);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ParticipantEntity[] newArray(int i) {
        return new ParticipantEntity[i];
    }
}
