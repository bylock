package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.ParticipantRef;
import java.util.ArrayList;

public final class TurnBasedMatchRef extends e implements TurnBasedMatch {
    private final Game c;
    private final int d;

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public Game b() {
        return this.c;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String c() {
        return e("external_match_id");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String d() {
        return e("creator_external");
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public long e() {
        return b("creation_timestamp");
    }

    @Override // com.google.android.gms.common.data.e
    public boolean equals(Object obj) {
        return TurnBasedMatchEntity.a(this, obj);
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int f() {
        return c("status");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int g() {
        return c("user_match_status");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String h() {
        return e("description");
    }

    @Override // com.google.android.gms.common.data.e
    public int hashCode() {
        return TurnBasedMatchEntity.a(this);
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int i() {
        return c("variant");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String j() {
        return e("last_updater_external");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public long k() {
        return b("last_updated_timestamp");
    }

    @Override // com.google.android.gms.games.multiplayer.f
    public ArrayList l() {
        ArrayList arrayList = new ArrayList(this.d);
        for (int i = 0; i < this.d; i++) {
            arrayList.add(new ParticipantRef(this.a, this.b + i));
        }
        return arrayList;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String m() {
        return e("pending_participant_external");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public byte[] n() {
        return f("data");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int o() {
        return c("version");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String p() {
        return e("rematch_id");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public byte[] q() {
        return f("previous_match_data");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int r() {
        return c("match_number");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public Bundle s() {
        if (!d("has_automatch_criteria")) {
            return null;
        }
        return a.a(c("automatch_min_players"), c("automatch_max_players"), b("automatch_bit_mask"));
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int t() {
        if (!d("has_automatch_criteria")) {
            return 0;
        }
        return c("automatch_max_players");
    }

    public String toString() {
        return TurnBasedMatchEntity.b(this);
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public boolean u() {
        return d("upsync_required");
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String v() {
        return e("description_participant_id");
    }

    /* renamed from: w */
    public TurnBasedMatch a() {
        return new TurnBasedMatchEntity(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((TurnBasedMatchEntity) a()).writeToParcel(parcel, i);
    }
}
