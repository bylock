package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import com.google.android.gms.internal.ar;
import java.util.ArrayList;

public final class TurnBasedMatchEntity implements SafeParcelable, TurnBasedMatch {
    public static final b b = new b();
    private final int c;
    private final GameEntity d;
    private final String e;
    private final String f;
    private final long g;
    private final String h;
    private final long i;
    private final String j;
    private final int k;
    private final int l;
    private final int m;
    private final byte[] n;
    private final ArrayList o;
    private final String p;
    private final byte[] q;
    private final int r;
    private final Bundle s;
    private final int t;
    private final boolean u;
    private final String v;
    private final String w;

    TurnBasedMatchEntity(int i2, GameEntity gameEntity, String str, String str2, long j2, String str3, long j3, String str4, int i3, int i4, int i5, byte[] bArr, ArrayList arrayList, String str5, byte[] bArr2, int i6, Bundle bundle, int i7, boolean z, String str6, String str7) {
        this.c = i2;
        this.d = gameEntity;
        this.e = str;
        this.f = str2;
        this.g = j2;
        this.h = str3;
        this.i = j3;
        this.j = str4;
        this.k = i3;
        this.t = i7;
        this.l = i4;
        this.m = i5;
        this.n = bArr;
        this.o = arrayList;
        this.p = str5;
        this.q = bArr2;
        this.r = i6;
        this.s = bundle;
        this.u = z;
        this.v = str6;
        this.w = str7;
    }

    public TurnBasedMatchEntity(TurnBasedMatch turnBasedMatch) {
        this.c = 2;
        this.d = new GameEntity(turnBasedMatch.b());
        this.e = turnBasedMatch.c();
        this.f = turnBasedMatch.d();
        this.g = turnBasedMatch.e();
        this.h = turnBasedMatch.j();
        this.i = turnBasedMatch.k();
        this.j = turnBasedMatch.m();
        this.k = turnBasedMatch.f();
        this.t = turnBasedMatch.g();
        this.l = turnBasedMatch.i();
        this.m = turnBasedMatch.o();
        this.p = turnBasedMatch.p();
        this.r = turnBasedMatch.r();
        this.s = turnBasedMatch.s();
        this.u = turnBasedMatch.u();
        this.v = turnBasedMatch.h();
        this.w = turnBasedMatch.v();
        byte[] n2 = turnBasedMatch.n();
        if (n2 == null) {
            this.n = null;
        } else {
            this.n = new byte[n2.length];
            System.arraycopy(n2, 0, this.n, 0, n2.length);
        }
        byte[] q2 = turnBasedMatch.q();
        if (q2 == null) {
            this.q = null;
        } else {
            this.q = new byte[q2.length];
            System.arraycopy(q2, 0, this.q, 0, q2.length);
        }
        ArrayList l2 = turnBasedMatch.l();
        int size = l2.size();
        this.o = new ArrayList(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.o.add((ParticipantEntity) ((Participant) l2.get(i2)).a());
        }
    }

    static int a(TurnBasedMatch turnBasedMatch) {
        return ar.a(turnBasedMatch.b(), turnBasedMatch.c(), turnBasedMatch.d(), Long.valueOf(turnBasedMatch.e()), turnBasedMatch.j(), Long.valueOf(turnBasedMatch.k()), turnBasedMatch.m(), Integer.valueOf(turnBasedMatch.f()), Integer.valueOf(turnBasedMatch.g()), turnBasedMatch.h(), Integer.valueOf(turnBasedMatch.i()), Integer.valueOf(turnBasedMatch.o()), turnBasedMatch.l(), turnBasedMatch.p(), Integer.valueOf(turnBasedMatch.r()), turnBasedMatch.s(), Integer.valueOf(turnBasedMatch.t()), Boolean.valueOf(turnBasedMatch.u()));
    }

    static boolean a(TurnBasedMatch turnBasedMatch, Object obj) {
        if (!(obj instanceof TurnBasedMatch)) {
            return false;
        }
        if (turnBasedMatch == obj) {
            return true;
        }
        TurnBasedMatch turnBasedMatch2 = (TurnBasedMatch) obj;
        return ar.a(turnBasedMatch2.b(), turnBasedMatch.b()) && ar.a(turnBasedMatch2.c(), turnBasedMatch.c()) && ar.a(turnBasedMatch2.d(), turnBasedMatch.d()) && ar.a(Long.valueOf(turnBasedMatch2.e()), Long.valueOf(turnBasedMatch.e())) && ar.a(turnBasedMatch2.j(), turnBasedMatch.j()) && ar.a(Long.valueOf(turnBasedMatch2.k()), Long.valueOf(turnBasedMatch.k())) && ar.a(turnBasedMatch2.m(), turnBasedMatch.m()) && ar.a(Integer.valueOf(turnBasedMatch2.f()), Integer.valueOf(turnBasedMatch.f())) && ar.a(Integer.valueOf(turnBasedMatch2.g()), Integer.valueOf(turnBasedMatch.g())) && ar.a(turnBasedMatch2.h(), turnBasedMatch.h()) && ar.a(Integer.valueOf(turnBasedMatch2.i()), Integer.valueOf(turnBasedMatch.i())) && ar.a(Integer.valueOf(turnBasedMatch2.o()), Integer.valueOf(turnBasedMatch.o())) && ar.a(turnBasedMatch2.l(), turnBasedMatch.l()) && ar.a(turnBasedMatch2.p(), turnBasedMatch.p()) && ar.a(Integer.valueOf(turnBasedMatch2.r()), Integer.valueOf(turnBasedMatch.r())) && ar.a(turnBasedMatch2.s(), turnBasedMatch.s()) && ar.a(Integer.valueOf(turnBasedMatch2.t()), Integer.valueOf(turnBasedMatch.t())) && ar.a(Boolean.valueOf(turnBasedMatch2.u()), Boolean.valueOf(turnBasedMatch.u()));
    }

    static String b(TurnBasedMatch turnBasedMatch) {
        return ar.a(turnBasedMatch).a("Game", turnBasedMatch.b()).a("MatchId", turnBasedMatch.c()).a("CreatorId", turnBasedMatch.d()).a("CreationTimestamp", Long.valueOf(turnBasedMatch.e())).a("LastUpdaterId", turnBasedMatch.j()).a("LastUpdatedTimestamp", Long.valueOf(turnBasedMatch.k())).a("PendingParticipantId", turnBasedMatch.m()).a("MatchStatus", Integer.valueOf(turnBasedMatch.f())).a("TurnStatus", Integer.valueOf(turnBasedMatch.g())).a("Description", turnBasedMatch.h()).a("Variant", Integer.valueOf(turnBasedMatch.i())).a("Data", turnBasedMatch.n()).a("Version", Integer.valueOf(turnBasedMatch.o())).a("Participants", turnBasedMatch.l()).a("RematchId", turnBasedMatch.p()).a("PreviousData", turnBasedMatch.q()).a("MatchNumber", Integer.valueOf(turnBasedMatch.r())).a("AutoMatchCriteria", turnBasedMatch.s()).a("AvailableAutoMatchSlots", Integer.valueOf(turnBasedMatch.t())).a("LocallyModified", Boolean.valueOf(turnBasedMatch.u())).a("DescriptionParticipantId", turnBasedMatch.v()).toString();
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public Game b() {
        return this.d;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String c() {
        return this.e;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String d() {
        return this.f;
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public long e() {
        return this.g;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int f() {
        return this.k;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int g() {
        return this.t;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String h() {
        return this.v;
    }

    public int hashCode() {
        return a(this);
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int i() {
        return this.l;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String j() {
        return this.h;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public long k() {
        return this.i;
    }

    @Override // com.google.android.gms.games.multiplayer.f
    public ArrayList l() {
        return new ArrayList(this.o);
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String m() {
        return this.j;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public byte[] n() {
        return this.n;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int o() {
        return this.m;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String p() {
        return this.p;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public byte[] q() {
        return this.q;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int r() {
        return this.r;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public Bundle s() {
        return this.s;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public int t() {
        if (this.s == null) {
            return 0;
        }
        return this.s.getInt("max_automatch_players");
    }

    public String toString() {
        return b(this);
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public boolean u() {
        return this.u;
    }

    @Override // com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch
    public String v() {
        return this.w;
    }

    public int w() {
        return this.c;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        b.a(this, parcel, i2);
    }

    /* renamed from: x */
    public TurnBasedMatch a() {
        return this;
    }
}
