package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.b.k;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;

public class b implements Parcelable.Creator {
    static void a(TurnBasedMatchEntity turnBasedMatchEntity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, (Parcelable) turnBasedMatchEntity.b(), i, false);
        c.a(parcel, 2, turnBasedMatchEntity.c(), false);
        c.a(parcel, 3, turnBasedMatchEntity.d(), false);
        c.a(parcel, 4, turnBasedMatchEntity.e());
        c.a(parcel, 5, turnBasedMatchEntity.j(), false);
        c.a(parcel, 6, turnBasedMatchEntity.k());
        c.a(parcel, 7, turnBasedMatchEntity.m(), false);
        c.a(parcel, 8, turnBasedMatchEntity.f());
        c.a(parcel, 10, turnBasedMatchEntity.i());
        c.a(parcel, 11, turnBasedMatchEntity.o());
        c.a(parcel, 12, turnBasedMatchEntity.n(), false);
        c.b(parcel, 13, turnBasedMatchEntity.l(), false);
        c.a(parcel, 14, turnBasedMatchEntity.p(), false);
        c.a(parcel, 15, turnBasedMatchEntity.q(), false);
        c.a(parcel, 17, turnBasedMatchEntity.s(), false);
        c.a(parcel, 16, turnBasedMatchEntity.r());
        c.a(parcel, 1000, turnBasedMatchEntity.w());
        c.a(parcel, 19, turnBasedMatchEntity.u());
        c.a(parcel, 18, turnBasedMatchEntity.g());
        c.a(parcel, 21, turnBasedMatchEntity.v(), false);
        c.a(parcel, 20, turnBasedMatchEntity.h(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public TurnBasedMatchEntity createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        GameEntity gameEntity = null;
        String str = null;
        String str2 = null;
        long j = 0;
        String str3 = null;
        long j2 = 0;
        String str4 = null;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        byte[] bArr = null;
        ArrayList arrayList = null;
        String str5 = null;
        byte[] bArr2 = null;
        int i5 = 0;
        Bundle bundle = null;
        int i6 = 0;
        boolean z = false;
        String str6 = null;
        String str7 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    gameEntity = (GameEntity) a.a(parcel, a, GameEntity.CREATOR);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    str2 = a.n(parcel, a);
                    break;
                case 4:
                    j = a.i(parcel, a);
                    break;
                case 5:
                    str3 = a.n(parcel, a);
                    break;
                case 6:
                    j2 = a.i(parcel, a);
                    break;
                case 7:
                    str4 = a.n(parcel, a);
                    break;
                case 8:
                    i2 = a.g(parcel, a);
                    break;
                case 10:
                    i3 = a.g(parcel, a);
                    break;
                case 11:
                    i4 = a.g(parcel, a);
                    break;
                case 12:
                    bArr = a.q(parcel, a);
                    break;
                case 13:
                    arrayList = a.c(parcel, a, ParticipantEntity.CREATOR);
                    break;
                case 14:
                    str5 = a.n(parcel, a);
                    break;
                case 15:
                    bArr2 = a.q(parcel, a);
                    break;
                case 16:
                    i5 = a.g(parcel, a);
                    break;
                case k.ActionBar_progressBarPadding:
                    bundle = a.p(parcel, a);
                    break;
                case k.ActionBar_itemPadding:
                    i6 = a.g(parcel, a);
                    break;
                case 19:
                    z = a.c(parcel, a);
                    break;
                case 20:
                    str6 = a.n(parcel, a);
                    break;
                case 21:
                    str7 = a.n(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new TurnBasedMatchEntity(i, gameEntity, str, str2, j, str3, j2, str4, i2, i3, i4, bArr, arrayList, str5, bArr2, i5, bundle, i6, z, str6, str7);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public TurnBasedMatchEntity[] newArray(int i) {
        return new TurnBasedMatchEntity[i];
    }
}
