package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.internal.ar;
import com.google.android.gms.internal.aw;
import java.util.ArrayList;

public final class InvitationEntity extends GamesDowngradeableSafeParcel implements Invitation {
    public static final Parcelable.Creator CREATOR = new a();
    private final int a;
    private final GameEntity b;
    private final String c;
    private final long d;
    private final int e;
    private final ParticipantEntity f;
    private final ArrayList g;
    private final int h;
    private final int i;

    InvitationEntity(int i2, GameEntity gameEntity, String str, long j, int i3, ParticipantEntity participantEntity, ArrayList arrayList, int i4, int i5) {
        this.a = i2;
        this.b = gameEntity;
        this.c = str;
        this.d = j;
        this.e = i3;
        this.f = participantEntity;
        this.g = arrayList;
        this.h = i4;
        this.i = i5;
    }

    InvitationEntity(Invitation invitation) {
        this.a = 2;
        this.b = new GameEntity(invitation.d());
        this.c = invitation.e();
        this.d = invitation.g();
        this.e = invitation.h();
        this.h = invitation.i();
        this.i = invitation.j();
        String k = invitation.f().k();
        Participant participant = null;
        ArrayList l = invitation.l();
        int size = l.size();
        this.g = new ArrayList(size);
        for (int i2 = 0; i2 < size; i2++) {
            Participant participant2 = (Participant) l.get(i2);
            if (participant2.k().equals(k)) {
                participant = participant2;
            }
            this.g.add((ParticipantEntity) participant2.a());
        }
        aw.a(participant, "Must have a valid inviter!");
        this.f = (ParticipantEntity) participant.a();
    }

    static int a(Invitation invitation) {
        return ar.a(invitation.d(), invitation.e(), Long.valueOf(invitation.g()), Integer.valueOf(invitation.h()), invitation.f(), invitation.l(), Integer.valueOf(invitation.i()), Integer.valueOf(invitation.j()));
    }

    static boolean a(Invitation invitation, Object obj) {
        if (!(obj instanceof Invitation)) {
            return false;
        }
        if (invitation == obj) {
            return true;
        }
        Invitation invitation2 = (Invitation) obj;
        return ar.a(invitation2.d(), invitation.d()) && ar.a(invitation2.e(), invitation.e()) && ar.a(Long.valueOf(invitation2.g()), Long.valueOf(invitation.g())) && ar.a(Integer.valueOf(invitation2.h()), Integer.valueOf(invitation.h())) && ar.a(invitation2.f(), invitation.f()) && ar.a(invitation2.l(), invitation.l()) && ar.a(Integer.valueOf(invitation2.i()), Integer.valueOf(invitation.i())) && ar.a(Integer.valueOf(invitation2.j()), Integer.valueOf(invitation.j()));
    }

    static String b(Invitation invitation) {
        return ar.a(invitation).a("Game", invitation.d()).a("InvitationId", invitation.e()).a("CreationTimestamp", Long.valueOf(invitation.g())).a("InvitationType", Integer.valueOf(invitation.h())).a("Inviter", invitation.f()).a("Participants", invitation.l()).a("Variant", Integer.valueOf(invitation.i())).a("AvailableAutoMatchSlots", Integer.valueOf(invitation.j())).toString();
    }

    public int b() {
        return this.a;
    }

    /* renamed from: c */
    public Invitation a() {
        return this;
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public Game d() {
        return this.b;
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public String e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public Participant f() {
        return this.f;
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public long g() {
        return this.d;
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public int h() {
        return this.e;
    }

    public int hashCode() {
        return a(this);
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public int i() {
        return this.h;
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public int j() {
        return this.i;
    }

    @Override // com.google.android.gms.games.multiplayer.f
    public ArrayList l() {
        return new ArrayList(this.g);
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (!C()) {
            b.a(this, parcel, i2);
            return;
        }
        this.b.writeToParcel(parcel, i2);
        parcel.writeString(this.c);
        parcel.writeLong(this.d);
        parcel.writeInt(this.e);
        this.f.writeToParcel(parcel, i2);
        int size = this.g.size();
        parcel.writeInt(size);
        for (int i3 = 0; i3 < size; i3++) {
            ((ParticipantEntity) this.g.get(i3)).writeToParcel(parcel, i2);
        }
    }
}
