package com.google.android.gms.games.internal.multiplayer;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationEntity;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.internal.ao;
import com.google.android.gms.internal.ar;
import java.util.ArrayList;

public final class ZInvitationCluster implements SafeParcelable, Invitation {
    public static final a a = new a();
    private final int b;
    private final ArrayList c;

    ZInvitationCluster(int i, ArrayList arrayList) {
        this.b = i;
        this.c = arrayList;
        m();
    }

    private void m() {
        ao.a(!this.c.isEmpty());
        Invitation invitation = (Invitation) this.c.get(0);
        int size = this.c.size();
        for (int i = 1; i < size; i++) {
            ao.a(invitation.f().equals(((Invitation) this.c.get(i)).f()), "All the invitations must be from the same inviter");
        }
    }

    public int b() {
        return this.b;
    }

    public ArrayList c() {
        return new ArrayList(this.c);
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public Game d() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public String e() {
        return ((InvitationEntity) this.c.get(0)).e();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ZInvitationCluster)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ZInvitationCluster zInvitationCluster = (ZInvitationCluster) obj;
        if (zInvitationCluster.c.size() != this.c.size()) {
            return false;
        }
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            if (!((Invitation) this.c.get(i)).equals((Invitation) zInvitationCluster.c.get(i))) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public Participant f() {
        return ((InvitationEntity) this.c.get(0)).f();
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public long g() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public int h() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    public int hashCode() {
        return ar.a(this.c.toArray());
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public int i() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    @Override // com.google.android.gms.games.multiplayer.Invitation
    public int j() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    /* renamed from: k */
    public Invitation a() {
        return this;
    }

    @Override // com.google.android.gms.games.multiplayer.f
    public ArrayList l() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    public void writeToParcel(Parcel parcel, int i) {
        a.a(this, parcel, i);
    }
}
