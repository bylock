package com.google.android.gms.games.internal.request;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.request.GameRequest;
import com.google.android.gms.games.request.GameRequestEntity;
import com.google.android.gms.internal.ao;
import com.google.android.gms.internal.ar;
import java.util.ArrayList;

public final class GameRequestCluster implements SafeParcelable, GameRequest {
    public static final a a = new a();
    private final int b;
    private final ArrayList c;

    GameRequestCluster(int i, ArrayList arrayList) {
        this.b = i;
        this.c = arrayList;
        o();
    }

    private void o() {
        ao.a(!this.c.isEmpty());
        GameRequest gameRequest = (GameRequest) this.c.get(0);
        int size = this.c.size();
        for (int i = 1; i < size; i++) {
            GameRequest gameRequest2 = (GameRequest) this.c.get(i);
            ao.a(gameRequest.i() == gameRequest2.i(), "All the requests must be of the same type");
            ao.a(gameRequest.f().equals(gameRequest2.f()), "All the requests must be from the same sender");
        }
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public int a(String str) {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    public int b() {
        return this.b;
    }

    public ArrayList c() {
        return new ArrayList(this.c);
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public String d() {
        return ((GameRequestEntity) this.c.get(0)).d();
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public Game e() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof GameRequestCluster)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        GameRequestCluster gameRequestCluster = (GameRequestCluster) obj;
        if (gameRequestCluster.c.size() != this.c.size()) {
            return false;
        }
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            if (!((GameRequest) this.c.get(i)).equals((GameRequest) gameRequestCluster.c.get(i))) {
                return false;
            }
        }
        return true;
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public Player f() {
        return ((GameRequestEntity) this.c.get(0)).f();
    }

    /* renamed from: g */
    public ArrayList n() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public byte[] h() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    public int hashCode() {
        return ar.a(this.c.toArray());
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public int i() {
        return ((GameRequestEntity) this.c.get(0)).i();
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public long j() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public long k() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    @Override // com.google.android.gms.games.request.GameRequest
    public int l() {
        throw new UnsupportedOperationException("Method not supported on a cluster");
    }

    /* renamed from: m */
    public GameRequest a() {
        return this;
    }

    public void writeToParcel(Parcel parcel, int i) {
        a.a(this, parcel, i);
    }
}
