package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcel;

final class c extends d {
    c() {
    }

    @Override // com.google.android.gms.games.internal.game.d
    /* renamed from: a */
    public GameBadgeEntity createFromParcel(Parcel parcel) {
        if ((GameBadgeEntity.b(GameBadgeEntity.B())) || (GameBadgeEntity.b(GameBadgeEntity.class.getCanonicalName()))) {
            return super.createFromParcel(parcel);
        }
        int readInt = parcel.readInt();
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        String readString3 = parcel.readString();
        return new GameBadgeEntity(1, readInt, readString, readString2, readString3 == null ? null : Uri.parse(readString3));
    }
}
