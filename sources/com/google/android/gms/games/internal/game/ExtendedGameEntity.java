package com.google.android.gms.games.internal.game;

import android.os.Parcel;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.internal.ar;
import java.util.ArrayList;

public final class ExtendedGameEntity extends GamesDowngradeableSafeParcel implements ExtendedGame {
    public static final b a = new a();
    private final int b;
    private final GameEntity c;
    private final int d;
    private final boolean e;
    private final int f;
    private final long g;
    private final long h;
    private final String i;
    private final long j;
    private final String k;
    private final ArrayList l;

    ExtendedGameEntity(int i2, GameEntity gameEntity, int i3, boolean z, int i4, long j2, long j3, String str, long j4, String str2, ArrayList arrayList) {
        this.b = i2;
        this.c = gameEntity;
        this.d = i3;
        this.e = z;
        this.f = i4;
        this.g = j2;
        this.h = j3;
        this.i = str;
        this.j = j4;
        this.k = str2;
        this.l = arrayList;
    }

    public ExtendedGameEntity(ExtendedGame extendedGame) {
        this.b = 1;
        Game b2 = extendedGame.b();
        this.c = b2 == null ? null : new GameEntity(b2);
        this.d = extendedGame.d();
        this.e = extendedGame.e();
        this.f = extendedGame.f();
        this.g = extendedGame.g();
        this.h = extendedGame.h();
        this.i = extendedGame.i();
        this.j = extendedGame.j();
        this.k = extendedGame.k();
        ArrayList c2 = extendedGame.c();
        int size = c2.size();
        this.l = new ArrayList(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.l.add((GameBadgeEntity) ((GameBadge) c2.get(i2)).a());
        }
    }

    static int a(ExtendedGame extendedGame) {
        return ar.a(extendedGame.b(), Integer.valueOf(extendedGame.d()), Boolean.valueOf(extendedGame.e()), Integer.valueOf(extendedGame.f()), Long.valueOf(extendedGame.g()), Long.valueOf(extendedGame.h()), extendedGame.i(), Long.valueOf(extendedGame.j()), extendedGame.k());
    }

    static boolean a(ExtendedGame extendedGame, Object obj) {
        if (!(obj instanceof ExtendedGame)) {
            return false;
        }
        if (extendedGame == obj) {
            return true;
        }
        ExtendedGame extendedGame2 = (ExtendedGame) obj;
        return ar.a(extendedGame2.b(), extendedGame.b()) && ar.a(Integer.valueOf(extendedGame2.d()), Integer.valueOf(extendedGame.d())) && ar.a(Boolean.valueOf(extendedGame2.e()), Boolean.valueOf(extendedGame.e())) && ar.a(Integer.valueOf(extendedGame2.f()), Integer.valueOf(extendedGame.f())) && ar.a(Long.valueOf(extendedGame2.g()), Long.valueOf(extendedGame.g())) && ar.a(Long.valueOf(extendedGame2.h()), Long.valueOf(extendedGame.h())) && ar.a(extendedGame2.i(), extendedGame.i()) && ar.a(Long.valueOf(extendedGame2.j()), Long.valueOf(extendedGame.j())) && ar.a(extendedGame2.k(), extendedGame.k());
    }

    static String b(ExtendedGame extendedGame) {
        return ar.a(extendedGame).a("Game", extendedGame.b()).a("Availability", Integer.valueOf(extendedGame.d())).a("Owned", Boolean.valueOf(extendedGame.e())).a("AchievementUnlockedCount", Integer.valueOf(extendedGame.f())).a("LastPlayedServerTimestamp", Long.valueOf(extendedGame.g())).a("PriceMicros", Long.valueOf(extendedGame.h())).a("FormattedPrice", extendedGame.i()).a("FullPriceMicros", Long.valueOf(extendedGame.j())).a("FormattedFullPrice", extendedGame.k()).toString();
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public ArrayList c() {
        return new ArrayList(this.l);
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public int d() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public boolean e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public int f() {
        return this.f;
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public long g() {
        return this.g;
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public long h() {
        return this.h;
    }

    public int hashCode() {
        return a(this);
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public String i() {
        return this.i;
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public long j() {
        return this.j;
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public String k() {
        return this.k;
    }

    public int l() {
        return this.b;
    }

    /* renamed from: m */
    public GameEntity b() {
        return this.c;
    }

    /* renamed from: n */
    public ExtendedGame a() {
        return this;
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (!C()) {
            b.a(this, parcel, i2);
            return;
        }
        this.c.writeToParcel(parcel, i2);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e ? 1 : 0);
        parcel.writeInt(this.f);
        parcel.writeLong(this.g);
        parcel.writeLong(this.h);
        parcel.writeString(this.i);
        parcel.writeLong(this.j);
        parcel.writeString(this.k);
        int size = this.l.size();
        parcel.writeInt(size);
        for (int i3 = 0; i3 < size; i3++) {
            ((GameBadgeEntity) this.l.get(i3)).writeToParcel(parcel, i2);
        }
    }
}
