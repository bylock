package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.d;

public interface GameBadge extends Parcelable, d {
    int b();

    String c();

    String d();

    Uri e();
}
