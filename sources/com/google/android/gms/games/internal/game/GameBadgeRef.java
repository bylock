package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.e;

public final class GameBadgeRef extends e implements GameBadge {
    GameBadgeRef(DataHolder dataHolder, int i) {
        super(dataHolder, i);
    }

    @Override // com.google.android.gms.games.internal.game.GameBadge
    public int b() {
        return c("badge_type");
    }

    @Override // com.google.android.gms.games.internal.game.GameBadge
    public String c() {
        return e("badge_title");
    }

    @Override // com.google.android.gms.games.internal.game.GameBadge
    public String d() {
        return e("badge_description");
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.internal.game.GameBadge
    public Uri e() {
        return g("badge_icon_image_uri");
    }

    @Override // com.google.android.gms.common.data.e
    public boolean equals(Object obj) {
        return GameBadgeEntity.a(this, obj);
    }

    /* renamed from: f */
    public GameBadge a() {
        return new GameBadgeEntity(this);
    }

    @Override // com.google.android.gms.common.data.e
    public int hashCode() {
        return GameBadgeEntity.a(this);
    }

    public String toString() {
        return GameBadgeEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((GameBadgeEntity) a()).writeToParcel(parcel, i);
    }
}
