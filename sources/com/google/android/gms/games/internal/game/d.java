package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class d implements Parcelable.Creator {
    static void a(GameBadgeEntity gameBadgeEntity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, gameBadgeEntity.b());
        c.a(parcel, 1000, gameBadgeEntity.f());
        c.a(parcel, 2, gameBadgeEntity.c(), false);
        c.a(parcel, 3, gameBadgeEntity.d(), false);
        c.a(parcel, 4, (Parcelable) gameBadgeEntity.e(), i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public GameBadgeEntity createFromParcel(Parcel parcel) {
        int i = 0;
        Uri uri = null;
        int b = a.b(parcel);
        String str = null;
        String str2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    str = a.n(parcel, a);
                    break;
                case 4:
                    uri = (Uri) a.a(parcel, a, Uri.CREATOR);
                    break;
                case 1000:
                    i2 = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new GameBadgeEntity(i2, i, str2, str, uri);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public GameBadgeEntity[] newArray(int i) {
        return new GameBadgeEntity[i];
    }
}
