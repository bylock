package com.google.android.gms.games.internal.game;

import android.os.Parcelable;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Game;
import java.util.ArrayList;

public interface ExtendedGame extends Parcelable, d {
    Game b();

    ArrayList c();

    int d();

    boolean e();

    int f();

    long g();

    long h();

    String i();

    long j();

    String k();
}
