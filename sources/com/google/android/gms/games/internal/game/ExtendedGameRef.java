package com.google.android.gms.games.internal.game;

import android.os.Parcel;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameRef;
import java.util.ArrayList;

public class ExtendedGameRef extends e implements ExtendedGame {
    private final GameRef c;
    private final int d;

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public Game b() {
        return this.c;
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public ArrayList c() {
        if (this.a.c("badge_title", this.b, this.a.a(this.b)) == null) {
            return new ArrayList(0);
        }
        ArrayList arrayList = new ArrayList(this.d);
        for (int i = 0; i < this.d; i++) {
            arrayList.add(new GameBadgeRef(this.a, this.b + i));
        }
        return arrayList;
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public int d() {
        return c("availability");
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public boolean e() {
        return d("owned");
    }

    @Override // com.google.android.gms.common.data.e
    public boolean equals(Object obj) {
        return ExtendedGameEntity.a(this, obj);
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public int f() {
        return c("achievement_unlocked_count");
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public long g() {
        return b("last_played_server_time");
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public long h() {
        return b("price_micros");
    }

    @Override // com.google.android.gms.common.data.e
    public int hashCode() {
        return ExtendedGameEntity.a(this);
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public String i() {
        return e("formatted_price");
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public long j() {
        return b("full_price_micros");
    }

    @Override // com.google.android.gms.games.internal.game.ExtendedGame
    public String k() {
        return e("formatted_full_price");
    }

    /* renamed from: l */
    public ExtendedGame a() {
        return new ExtendedGameEntity(this);
    }

    public String toString() {
        return ExtendedGameEntity.b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        ((ExtendedGameEntity) a()).writeToParcel(parcel, i);
    }
}
