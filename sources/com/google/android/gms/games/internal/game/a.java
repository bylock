package com.google.android.gms.games.internal.game;

import android.os.Parcel;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

final class a extends b {
    a() {
    }

    @Override // com.google.android.gms.games.internal.game.b
    /* renamed from: a */
    public ExtendedGameEntity createFromParcel(Parcel parcel) {
        if (ExtendedGameEntity.a(ExtendedGameEntity.o()) || ExtendedGameEntity.a(ExtendedGameEntity.class.getCanonicalName())) {
            return super.createFromParcel(parcel);
        }
        GameEntity gameEntity = (GameEntity) GameEntity.CREATOR.createFromParcel(parcel);
        int readInt = parcel.readInt();
        boolean z = parcel.readInt() == 1;
        int readInt2 = parcel.readInt();
        long readLong = parcel.readLong();
        long readLong2 = parcel.readLong();
        String readString = parcel.readString();
        long readLong3 = parcel.readLong();
        String readString2 = parcel.readString();
        int readInt3 = parcel.readInt();
        ArrayList arrayList = new ArrayList(readInt3);
        for (int i = 0; i < readInt3; i++) {
            arrayList.add(GameBadgeEntity.a.createFromParcel(parcel));
        }
        return new ExtendedGameEntity(1, gameEntity, readInt, z, readInt2, readLong, readLong2, readString, readLong3, readString2, arrayList);
    }
}
