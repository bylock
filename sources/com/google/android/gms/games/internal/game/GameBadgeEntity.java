package com.google.android.gms.games.internal.game;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.internal.ar;

public final class GameBadgeEntity extends GamesDowngradeableSafeParcel implements GameBadge {
    public static final d a = new c();
    private final int b;
    private int c;
    private String d;
    private String e;
    private Uri f;

    GameBadgeEntity(int i, int i2, String str, String str2, Uri uri) {
        this.b = i;
        this.c = i2;
        this.d = str;
        this.e = str2;
        this.f = uri;
    }

    public GameBadgeEntity(GameBadge gameBadge) {
        this.b = 1;
        this.c = gameBadge.b();
        this.d = gameBadge.c();
        this.e = gameBadge.d();
        this.f = gameBadge.e();
    }

    static int a(GameBadge gameBadge) {
        return ar.a(Integer.valueOf(gameBadge.b()), gameBadge.c(), gameBadge.d(), gameBadge.e());
    }

    static boolean a(GameBadge gameBadge, Object obj) {
        if (!(obj instanceof GameBadge)) {
            return false;
        }
        if (gameBadge == obj) {
            return true;
        }
        GameBadge gameBadge2 = (GameBadge) obj;
        return ar.a(Integer.valueOf(gameBadge2.b()), gameBadge.c()) && ar.a(gameBadge2.d(), gameBadge.e());
    }

    static String b(GameBadge gameBadge) {
        return ar.a(gameBadge).a("Type", Integer.valueOf(gameBadge.b())).a("Title", gameBadge.c()).a("Description", gameBadge.d()).a("IconImageUri", gameBadge.e()).toString();
    }

    @Override // com.google.android.gms.games.internal.game.GameBadge
    public int b() {
        return this.c;
    }

    @Override // com.google.android.gms.games.internal.game.GameBadge
    public String c() {
        return this.d;
    }

    @Override // com.google.android.gms.games.internal.game.GameBadge
    public String d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    @Override // com.google.android.gms.games.internal.game.GameBadge
    public Uri e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        return a(this, obj);
    }

    public int f() {
        return this.b;
    }

    /* renamed from: g */
    public GameBadge a() {
        return this;
    }

    public int hashCode() {
        return a(this);
    }

    public String toString() {
        return b(this);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (!C()) {
            d.a(this, parcel, i);
            return;
        }
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f == null ? null : this.f.toString());
    }
}
