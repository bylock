package com.google.android.gms.games.internal.game;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.games.GameEntity;
import java.util.ArrayList;

public class b implements Parcelable.Creator {
    static void a(ExtendedGameEntity extendedGameEntity, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, (Parcelable) extendedGameEntity.b(), i, false);
        c.a(parcel, 1000, extendedGameEntity.l());
        c.a(parcel, 2, extendedGameEntity.d());
        c.a(parcel, 3, extendedGameEntity.e());
        c.a(parcel, 4, extendedGameEntity.f());
        c.a(parcel, 5, extendedGameEntity.g());
        c.a(parcel, 6, extendedGameEntity.h());
        c.a(parcel, 7, extendedGameEntity.i(), false);
        c.a(parcel, 8, extendedGameEntity.j());
        c.a(parcel, 9, extendedGameEntity.k(), false);
        c.b(parcel, 10, extendedGameEntity.c(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ExtendedGameEntity createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        GameEntity gameEntity = null;
        int i2 = 0;
        boolean z = false;
        int i3 = 0;
        long j = 0;
        long j2 = 0;
        String str = null;
        long j3 = 0;
        String str2 = null;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    gameEntity = (GameEntity) a.a(parcel, a, GameEntity.CREATOR);
                    break;
                case 2:
                    i2 = a.g(parcel, a);
                    break;
                case 3:
                    z = a.c(parcel, a);
                    break;
                case 4:
                    i3 = a.g(parcel, a);
                    break;
                case 5:
                    j = a.i(parcel, a);
                    break;
                case 6:
                    j2 = a.i(parcel, a);
                    break;
                case 7:
                    str = a.n(parcel, a);
                    break;
                case 8:
                    j3 = a.i(parcel, a);
                    break;
                case 9:
                    str2 = a.n(parcel, a);
                    break;
                case 10:
                    arrayList = a.c(parcel, a, GameBadgeEntity.a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ExtendedGameEntity(i, gameEntity, i2, z, i3, j, j2, str, j3, str2, arrayList);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ExtendedGameEntity[] newArray(int i) {
        return new ExtendedGameEntity[i];
    }
}
