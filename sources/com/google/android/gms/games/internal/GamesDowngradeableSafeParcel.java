package com.google.android.gms.games.internal;

import com.google.android.gms.internal.bm;
import com.google.android.gms.internal.fe;

public abstract class GamesDowngradeableSafeParcel extends fe {
    /* access modifiers changed from: protected */
    public static boolean b(Integer num) {
        if (num == null) {
            return false;
        }
        return bm.a(num.intValue());
    }
}
