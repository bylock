package com.google.android.gms;

/* compiled from: MyApp */
public final class b {
    public static final int auth_client_needs_enabling_title = 2131492878;
    public static final int auth_client_needs_installation_title = 2131492879;
    public static final int auth_client_needs_update_title = 2131492880;
    public static final int auth_client_play_services_err_notification_msg = 2131492881;
    public static final int auth_client_requested_by_msg = 2131492882;
    public static final int auth_client_using_bad_version_title = 2131492877;
    public static final int common_google_play_services_enable_button = 2131492894;
    public static final int common_google_play_services_enable_text = 2131492893;
    public static final int common_google_play_services_enable_title = 2131492892;
    public static final int common_google_play_services_error_notification_requested_by_msg = 2131492887;
    public static final int common_google_play_services_install_button = 2131492891;
    public static final int common_google_play_services_install_text_phone = 2131492889;
    public static final int common_google_play_services_install_text_tablet = 2131492890;
    public static final int common_google_play_services_install_title = 2131492888;
    public static final int common_google_play_services_invalid_account_text = 2131492900;
    public static final int common_google_play_services_invalid_account_title = 2131492899;
    public static final int common_google_play_services_needs_enabling_title = 2131492886;
    public static final int common_google_play_services_network_error_text = 2131492898;
    public static final int common_google_play_services_network_error_title = 2131492897;
    public static final int common_google_play_services_notification_needs_installation_title = 2131492884;
    public static final int common_google_play_services_notification_needs_update_title = 2131492885;
    public static final int common_google_play_services_notification_ticker = 2131492883;
    public static final int common_google_play_services_unknown_issue = 2131492901;
    public static final int common_google_play_services_unsupported_date_text = 2131492904;
    public static final int common_google_play_services_unsupported_text = 2131492903;
    public static final int common_google_play_services_unsupported_title = 2131492902;
    public static final int common_google_play_services_update_button = 2131492905;
    public static final int common_google_play_services_update_text = 2131492896;
    public static final int common_google_play_services_update_title = 2131492895;
    public static final int common_signin_button_text = 2131492906;
    public static final int common_signin_button_text_long = 2131492907;
    public static final int wallet_buy_button_place_holder = 2131492908;
}
