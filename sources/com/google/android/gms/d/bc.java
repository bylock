package com.google.android.gms.d;

import android.util.LruCache;

/* access modifiers changed from: package-private */
public class bc implements fd {
    private LruCache a;

    bc(int i, fg fgVar) {
        this.a = new bd(this, i, fgVar);
    }

    @Override // com.google.android.gms.d.fd
    public Object a(Object obj) {
        return this.a.get(obj);
    }

    @Override // com.google.android.gms.d.fd
    public void a(Object obj, Object obj2) {
        this.a.put(obj, obj2);
    }
}
