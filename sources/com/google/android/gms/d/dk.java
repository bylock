package com.google.android.gms.d;

import java.util.Collections;
import java.util.List;

public class dk {
    private final List a;
    private final List b;
    private final List c;
    private final List d;
    private final List e;
    private final List f;
    private final List g;
    private final List h;
    private final List i;
    private final List j;

    private dk(List list, List list2, List list3, List list4, List list5, List list6, List list7, List list8, List list9, List list10) {
        this.a = Collections.unmodifiableList(list);
        this.b = Collections.unmodifiableList(list2);
        this.c = Collections.unmodifiableList(list3);
        this.d = Collections.unmodifiableList(list4);
        this.e = Collections.unmodifiableList(list5);
        this.f = Collections.unmodifiableList(list6);
        this.g = Collections.unmodifiableList(list7);
        this.h = Collections.unmodifiableList(list8);
        this.i = Collections.unmodifiableList(list9);
        this.j = Collections.unmodifiableList(list10);
    }

    public static dl a() {
        return new dl();
    }

    public List b() {
        return this.a;
    }

    public List c() {
        return this.b;
    }

    public List d() {
        return this.c;
    }

    public List e() {
        return this.d;
    }

    public List f() {
        return this.e;
    }

    public List g() {
        return this.g;
    }

    public List h() {
        return this.h;
    }

    public List i() {
        return this.i;
    }

    public List j() {
        return this.j;
    }

    public List k() {
        return this.f;
    }

    public String toString() {
        return "Positive predicates: " + b() + "  Negative predicates: " + c() + "  Add tags: " + d() + "  Remove tags: " + e() + "  Add macros: " + f() + "  Remove macros: " + k();
    }
}
