package com.google.android.gms.d;

import com.google.android.gms.a.a.a;
import com.google.android.gms.a.a.b;
import java.io.IOException;

/* access modifiers changed from: package-private */
public class u implements w {
    final /* synthetic */ t a;

    u(t tVar) {
        this.a = tVar;
    }

    @Override // com.google.android.gms.d.w
    public b a() {
        try {
            return a.b(this.a.f);
        } catch (IllegalStateException e) {
            bk.b("IllegalStateException getting Advertising Id Info");
            return null;
        } catch (com.google.android.gms.common.b e2) {
            bk.b("GooglePlayServicesRepairableException getting Advertising Id Info");
            return null;
        } catch (IOException e3) {
            bk.b("IOException getting Ad Id Info");
            return null;
        } catch (com.google.android.gms.common.a e4) {
            bk.b("GooglePlayServicesNotAvailableException getting Advertising Id Info");
            return null;
        } catch (Exception e5) {
            bk.b("Unknown exception. Could not get the Advertising Id Info.");
            return null;
        }
    }
}
