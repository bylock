package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.b.e;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* access modifiers changed from: package-private */
public class es extends ep {
    private static final String a = a.UNIVERSAL_ANALYTICS.toString();
    private static final String b = f.ACCOUNT.toString();
    private static final String c = f.ANALYTICS_PASS_THROUGH.toString();
    private static final String d = f.ANALYTICS_FIELDS.toString();
    private static final String e = f.TRACK_TRANSACTION.toString();
    private static final String f = f.TRANSACTION_DATALAYER_MAP.toString();
    private static final String g = f.TRANSACTION_ITEM_DATALAYER_MAP.toString();
    private static Map h;
    private static Map i;
    private final Set j;
    private final en k;
    private final i l;

    public es(Context context, i iVar) {
        this(context, iVar, new en(context));
    }

    es(Context context, i iVar, en enVar) {
        super(a, new String[0]);
        this.l = iVar;
        this.k = enVar;
        this.j = new HashSet();
        this.j.add("");
        this.j.add("0");
        this.j.add("false");
    }

    private String a(String str) {
        Object c2 = this.l.c(str);
        if (c2 == null) {
            return null;
        }
        return c2.toString();
    }

    private Map a(y yVar) {
        Object e2 = er.e(yVar);
        if (!(e2 instanceof Map)) {
            return null;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry entry : ((Map) e2).entrySet()) {
            linkedHashMap.put(entry.getKey().toString(), entry.getValue().toString());
        }
        return linkedHashMap;
    }

    private void a(e eVar, Map map) {
        String a2 = a("transactionId");
        if (a2 == null) {
            bk.a("Cannot find transactionId in data layer.");
            return;
        }
        LinkedList<Map> linkedList = new LinkedList();
        try {
            Map b2 = b((y) map.get(d));
            b2.put("&t", "transaction");
            for (Map.Entry entry : c(map).entrySet()) {
                a(b2, (String) entry.getValue(), a((String) entry.getKey()));
            }
            linkedList.add(b2);
            List<Map> d2 = d();
            if (d2 != null) {
                for (Map map2 : d2) {
                    if (map2.get("name") == null) {
                        bk.a("Unable to send transaction item hit due to missing 'name' field.");
                        return;
                    }
                    Map b3 = b((y) map.get(d));
                    b3.put("&t", "item");
                    b3.put("&ti", a2);
                    for (Map.Entry entry2 : d(map).entrySet()) {
                        a(b3, (String) entry2.getValue(), (String) map2.get(entry2.getKey()));
                    }
                    linkedList.add(b3);
                }
            }
            for (Map map3 : linkedList) {
                eVar.a(map3);
            }
        } catch (IllegalArgumentException e2) {
            bk.a("Unable to send transaction", e2);
        }
    }

    private void a(Map map, String str, String str2) {
        if (str2 != null) {
            map.put(str, str2);
        }
    }

    private boolean a(Map map, String str) {
        y yVar = (y) map.get(str);
        if (yVar == null) {
            return false;
        }
        return er.d(yVar).booleanValue();
    }

    private Map b(y yVar) {
        if (yVar == null) {
            return new HashMap();
        }
        Map a2 = a(yVar);
        if (a2 == null) {
            return new HashMap();
        }
        String str = (String) a2.get("&aip");
        if (str != null && this.j.contains(str.toLowerCase())) {
            a2.remove("&aip");
        }
        return a2;
    }

    private Map c(Map map) {
        y yVar = (y) map.get(f);
        if (yVar != null) {
            return a(yVar);
        }
        if (h == null) {
            HashMap hashMap = new HashMap();
            hashMap.put("transactionId", "&ti");
            hashMap.put("transactionAffiliation", "&ta");
            hashMap.put("transactionTax", "&tt");
            hashMap.put("transactionShipping", "&ts");
            hashMap.put("transactionTotal", "&tr");
            hashMap.put("transactionCurrency", "&cu");
            h = hashMap;
        }
        return h;
    }

    private List d() {
        Object c2 = this.l.c("transactionProducts");
        if (c2 == null) {
            return null;
        }
        if (!(c2 instanceof List)) {
            throw new IllegalArgumentException("transactionProducts should be of type List.");
        }
        for (Object obj : (List) c2) {
            if (!(obj instanceof Map)) {
                throw new IllegalArgumentException("Each element of transactionProducts should be of type Map.");
            }
        }
        return (List) c2;
    }

    private Map d(Map map) {
        y yVar = (y) map.get(g);
        if (yVar != null) {
            return a(yVar);
        }
        if (i == null) {
            HashMap hashMap = new HashMap();
            hashMap.put("name", "&in");
            hashMap.put("sku", "&ic");
            hashMap.put("category", "&iv");
            hashMap.put("price", "&ip");
            hashMap.put("quantity", "&iq");
            hashMap.put("currency", "&cu");
            i = hashMap;
        }
        return i;
    }

    @Override // com.google.android.gms.d.ep
    public void b(Map map) {
        e a2 = this.k.a("_GTM_DEFAULT_TRACKER_");
        if (a(map, c)) {
            a2.a(b((y) map.get(d)));
        } else if (a(map, e)) {
            a(a2, map);
        } else {
            bk.b("Ignoring unknown tag.");
        }
    }
}
