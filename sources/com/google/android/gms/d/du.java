package com.google.android.gms.d;

import com.google.android.gms.internal.y;

/* access modifiers changed from: package-private */
public class du {
    private cc a;
    private y b;

    public du(cc ccVar, y yVar) {
        this.a = ccVar;
        this.b = yVar;
    }

    public cc a() {
        return this.a;
    }

    public y b() {
        return this.b;
    }

    public int c() {
        return (this.b == null ? 0 : this.b.e()) + ((y) this.a.a()).e();
    }
}
