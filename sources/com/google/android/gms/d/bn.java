package com.google.android.gms.d;

import android.content.Context;
import android.provider.Settings;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class bn extends ag {
    private static final String a = a.MOBILE_ADWORDS_UNIQUE_ID.toString();
    private final Context b;

    public bn(Context context) {
        super(a, new String[0]);
        this.b = context;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        String a2 = a(this.b);
        return a2 == null ? er.e() : er.e(a2);
    }

    /* access modifiers changed from: protected */
    public String a(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
