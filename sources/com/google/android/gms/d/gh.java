package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.List;
import java.util.Map;

/* access modifiers changed from: package-private */
public class gh extends ep {
    private static final String a = a.DATA_LAYER_WRITE.toString();
    private static final String b = f.VALUE.toString();
    private static final String c = f.CLEAR_PERSISTENT_DATA_LAYER_PREFIX.toString();
    private final i d;

    public gh(i iVar) {
        super(a, b);
        this.d = iVar;
    }

    private void a(y yVar) {
        String a2;
        if (yVar != null && yVar != er.a() && (a2 = er.a(yVar)) != er.d()) {
            this.d.a(a2);
        }
    }

    private void b(y yVar) {
        if (!(yVar == null || yVar == er.a())) {
            Object e = er.e(yVar);
            if (e instanceof List) {
                for (Object obj : (List) e) {
                    if (obj instanceof Map) {
                        this.d.a((Map) obj);
                    }
                }
            }
        }
    }

    @Override // com.google.android.gms.d.ep
    public void b(Map map) {
        b((y) map.get(b));
        a((y) map.get(c));
    }
}
