package com.google.android.gms.d;

import android.content.Context;
import android.os.Process;
import com.google.android.gms.a.a.b;
import com.google.android.gms.internal.bk;
import com.google.android.gms.internal.bl;

class t {
    private static Object j = new Object();
    private static t k;
    private volatile long a;
    private volatile long b;
    private volatile boolean c;
    private volatile b d;
    private volatile long e;
    private final Context f;
    private final bk g;
    private final Thread h;
    private w i;

    private t(Context context) {
        this(context, null, bl.b());
    }

    t(Context context, w wVar, bk bkVar) {
        this.a = 900000;
        this.b = 30000;
        this.c = false;
        this.i = new u(this);
        this.g = bkVar;
        if (context != null) {
            this.f = context.getApplicationContext();
        } else {
            this.f = context;
        }
        if (wVar != null) {
            this.i = wVar;
        }
        this.h = new Thread(new v(this));
    }

    static t a(Context context) {
        if (k == null) {
            synchronized (j) {
                if (k == null) {
                    k = new t(context);
                    k.d();
                }
            }
        }
        return k;
    }

    /* access modifiers changed from: private */
    public void e() {
        Process.setThreadPriority(10);
        while (!this.c) {
            try {
                this.d = this.i.a();
                Thread.sleep(this.a);
            } catch (InterruptedException e2) {
                bk.c("sleep interrupted in AdvertiserDataPoller thread; continuing");
            }
        }
    }

    private void f() {
        if (this.g.a() - this.e >= this.b) {
            c();
            this.e = this.g.a();
        }
    }

    public String a() {
        f();
        if (this.d == null) {
            return null;
        }
        return this.d.a();
    }

    public boolean b() {
        f();
        if (this.d == null) {
            return true;
        }
        return this.d.b();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.h.interrupt();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.h.start();
    }
}
