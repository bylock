package com.google.android.gms.d;

import android.text.TextUtils;

class am {
    private final long a;
    private final long b;
    private final long c;
    private String d;

    am(long j, long j2, long j3) {
        this.a = j;
        this.b = j2;
        this.c = j3;
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (str != null && !TextUtils.isEmpty(str.trim())) {
            this.d = str;
        }
    }

    /* access modifiers changed from: package-private */
    public long b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.d;
    }
}
