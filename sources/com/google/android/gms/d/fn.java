package com.google.android.gms.d;

class fn implements fo {
    final /* synthetic */ boolean a;
    final /* synthetic */ fl b;

    fn(fl flVar, boolean z) {
        this.b = flVar;
        this.a = z;
    }

    @Override // com.google.android.gms.d.fo
    public boolean a(a aVar) {
        return this.a ? aVar.a() + 43200000 >= fl.a(this.b).a() : !aVar.b();
    }
}
