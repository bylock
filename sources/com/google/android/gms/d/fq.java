package com.google.android.gms.d;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.r;

class fq implements bi {
    final /* synthetic */ fl a;

    private fq(fl flVar) {
        this.a = flVar;
    }

    /* synthetic */ fq(fl flVar, fm fmVar) {
        this(flVar);
    }

    @Override // com.google.android.gms.d.bi
    public void a() {
    }

    @Override // com.google.android.gms.d.bi
    public void a(bj bjVar) {
        if (fl.f(this.a) != null) {
            this.a.a(fl.f(this.a));
        } else {
            this.a.a(this.a.a(Status.d));
        }
        fl.a(this.a, 3600000);
    }

    public void a(r rVar) {
        synchronized (this.a) {
            if (rVar.b == null) {
                if (fl.c(this.a).b == null) {
                    bk.a("Current resource is null; network resource is also null");
                    fl.a(this.a, 3600000);
                    return;
                }
                rVar.b = fl.c(this.a).b;
            }
            fl.a(this.a, rVar, fl.a(this.a).a(), false);
            bk.e("setting refresh time to current time: " + fl.d(this.a));
            if (!fl.e(this.a)) {
                fl.a(this.a, rVar);
            }
        }
    }
}
