package com.google.android.gms.d;

import com.google.android.gms.internal.y;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* access modifiers changed from: package-private */
public class eu {
    private static cc a(cc ccVar) {
        try {
            return new cc(er.e(a(er.a((y) ccVar.a()))), ccVar.b());
        } catch (UnsupportedEncodingException e) {
            bk.a("Escape URI: unsupported encoding", e);
            return ccVar;
        }
    }

    private static cc a(cc ccVar, int i) {
        if (!a((y) ccVar.a())) {
            bk.a("Escaping can only be applied to strings.");
            return ccVar;
        }
        switch (i) {
            case 12:
                return a(ccVar);
            default:
                bk.a("Unsupported Value Escaping: " + i);
                return ccVar;
        }
    }

    static cc a(cc ccVar, int... iArr) {
        for (int i : iArr) {
            ccVar = a(ccVar, i);
        }
        return ccVar;
    }

    static String a(String str) {
        return URLEncoder.encode(str, "UTF-8").replaceAll("\\+", "%20");
    }

    private static boolean a(y yVar) {
        return er.e(yVar) instanceof String;
    }
}
