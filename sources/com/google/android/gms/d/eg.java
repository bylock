package com.google.android.gms.d;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* access modifiers changed from: package-private */
public class eg implements fd {
    private final Map a = new HashMap();
    private final int b;
    private final fg c;
    private int d;

    eg(int i, fg fgVar) {
        this.b = i;
        this.c = fgVar;
    }

    @Override // com.google.android.gms.d.fd
    public synchronized Object a(Object obj) {
        return this.a.get(obj);
    }

    @Override // com.google.android.gms.d.fd
    public synchronized void a(Object obj, Object obj2) {
        if (obj == null || obj2 == null) {
            throw new NullPointerException("key == null || value == null");
        }
        this.d += this.c.a(obj, obj2);
        if (this.d > this.b) {
            Iterator it = this.a.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                this.d -= this.c.a(entry.getKey(), entry.getValue());
                it.remove();
                if (this.d <= this.b) {
                    break;
                }
            }
        }
        this.a.put(obj, obj2);
    }
}
