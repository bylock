package com.google.android.gms.d;

import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class dj {
    private final List a;
    private final Map b;
    private String c;
    private int d;

    private dj() {
        this.a = new ArrayList();
        this.b = new HashMap();
        this.c = "";
        this.d = 0;
    }

    public di a() {
        return new di(this.a, this.b, this.c, this.d);
    }

    public dj a(int i) {
        this.d = i;
        return this;
    }

    public dj a(dg dgVar) {
        String a2 = er.a((y) dgVar.b().get(f.INSTANCE_NAME.toString()));
        List list = (List) this.b.get(a2);
        if (list == null) {
            list = new ArrayList();
            this.b.put(a2, list);
        }
        list.add(dgVar);
        return this;
    }

    public dj a(dk dkVar) {
        this.a.add(dkVar);
        return this;
    }

    public dj a(String str) {
        this.c = str;
        return this;
    }
}
