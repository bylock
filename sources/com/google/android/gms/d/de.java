package com.google.android.gms.d;

import com.google.android.gms.internal.f;
import com.google.android.gms.internal.j;
import com.google.android.gms.internal.m;
import com.google.android.gms.internal.n;
import com.google.android.gms.internal.o;
import com.google.android.gms.internal.p;
import com.google.android.gms.internal.y;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* access modifiers changed from: package-private */
public class de {
    private static dg a(j jVar, n nVar, y[] yVarArr, int i) {
        dh a = dg.a();
        for (int i2 : jVar.a) {
            m mVar = (m) a(nVar.d, Integer.valueOf(i2).intValue(), "properties");
            String str = (String) a(nVar.b, mVar.a, "keys");
            y yVar = (y) a(yVarArr, mVar.b, "values");
            if (f.PUSH_AFTER_EVALUATE.toString().equals(str)) {
                a.a(yVar);
            } else {
                a.a(str, yVar);
            }
        }
        return a.a();
    }

    public static di a(n nVar) {
        y[] yVarArr = new y[nVar.c.length];
        for (int i = 0; i < nVar.c.length; i++) {
            a(i, nVar, yVarArr, new HashSet(0));
        }
        dj a = di.a();
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < nVar.f.length; i2++) {
            arrayList.add(a(nVar.f[i2], nVar, yVarArr, i2));
        }
        ArrayList arrayList2 = new ArrayList();
        for (int i3 = 0; i3 < nVar.g.length; i3++) {
            arrayList2.add(a(nVar.g[i3], nVar, yVarArr, i3));
        }
        ArrayList arrayList3 = new ArrayList();
        for (int i4 = 0; i4 < nVar.e.length; i4++) {
            dg a2 = a(nVar.e[i4], nVar, yVarArr, i4);
            a.a(a2);
            arrayList3.add(a2);
        }
        for (o oVar : nVar.h) {
            a.a(a(oVar, arrayList, arrayList3, arrayList2, nVar));
        }
        a.a(nVar.l);
        a.a(nVar.q);
        return a.a();
    }

    private static dk a(o oVar, List list, List list2, List list3, n nVar) {
        dl a = dk.a();
        for (int i : oVar.a) {
            a.a((dg) list3.get(Integer.valueOf(i).intValue()));
        }
        for (int i2 : oVar.b) {
            a.b((dg) list3.get(Integer.valueOf(i2).intValue()));
        }
        for (int i3 : oVar.c) {
            a.c((dg) list.get(Integer.valueOf(i3).intValue()));
        }
        for (int i4 : oVar.e) {
            a.a(nVar.c[Integer.valueOf(i4).intValue()].b);
        }
        for (int i5 : oVar.d) {
            a.d((dg) list.get(Integer.valueOf(i5).intValue()));
        }
        for (int i6 : oVar.f) {
            a.b(nVar.c[Integer.valueOf(i6).intValue()].b);
        }
        for (int i7 : oVar.g) {
            a.e((dg) list2.get(Integer.valueOf(i7).intValue()));
        }
        for (int i8 : oVar.i) {
            a.c(nVar.c[Integer.valueOf(i8).intValue()].b);
        }
        for (int i9 : oVar.h) {
            a.f((dg) list2.get(Integer.valueOf(i9).intValue()));
        }
        for (int i10 : oVar.j) {
            a.d(nVar.c[Integer.valueOf(i10).intValue()].b);
        }
        return a.a();
    }

    private static y a(int i, n nVar, y[] yVarArr, Set set) {
        int i2 = 0;
        if (set.contains(Integer.valueOf(i))) {
            a("Value cycle detected.  Current value reference: " + i + "." + "  Previous value references: " + set + ".");
        }
        y yVar = (y) a(nVar.c, i, "values");
        if (yVarArr[i] != null) {
            return yVarArr[i];
        }
        y yVar2 = null;
        set.add(Integer.valueOf(i));
        switch (yVar.a) {
            case 1:
            case 5:
            case 6:
            case 8:
                yVar2 = yVar;
                break;
            case 2:
                p b = b(yVar);
                yVar2 = a(yVar);
                yVar2.c = new y[b.b.length];
                int[] iArr = b.b;
                int length = iArr.length;
                int i3 = 0;
                while (i2 < length) {
                    yVar2.c[i3] = a(iArr[i2], nVar, yVarArr, set);
                    i2++;
                    i3++;
                }
                break;
            case 3:
                yVar2 = a(yVar);
                p b2 = b(yVar);
                if (b2.c.length != b2.d.length) {
                    a("Uneven map keys (" + b2.c.length + ") and map values (" + b2.d.length + ")");
                }
                yVar2.d = new y[b2.c.length];
                yVar2.e = new y[b2.c.length];
                int[] iArr2 = b2.c;
                int length2 = iArr2.length;
                int i4 = 0;
                int i5 = 0;
                while (i4 < length2) {
                    yVar2.d[i5] = a(iArr2[i4], nVar, yVarArr, set);
                    i4++;
                    i5++;
                }
                int[] iArr3 = b2.d;
                int length3 = iArr3.length;
                int i6 = 0;
                while (i2 < length3) {
                    yVar2.e[i6] = a(iArr3[i2], nVar, yVarArr, set);
                    i2++;
                    i6++;
                }
                break;
            case 4:
                yVar2 = a(yVar);
                yVar2.f = er.a(a(b(yVar).g, nVar, yVarArr, set));
                break;
            case 7:
                yVar2 = a(yVar);
                p b3 = b(yVar);
                yVar2.j = new y[b3.f.length];
                int[] iArr4 = b3.f;
                int length4 = iArr4.length;
                int i7 = 0;
                while (i2 < length4) {
                    yVar2.j[i7] = a(iArr4[i2], nVar, yVarArr, set);
                    i2++;
                    i7++;
                }
                break;
        }
        if (yVar2 == null) {
            a("Invalid value: " + yVar);
        }
        yVarArr[i] = yVar2;
        set.remove(Integer.valueOf(i));
        return yVar2;
    }

    public static y a(y yVar) {
        y yVar2 = new y();
        yVar2.a = yVar.a;
        yVar2.k = (int[]) yVar.k.clone();
        if (yVar.l) {
            yVar2.l = yVar.l;
        }
        return yVar2;
    }

    private static Object a(Object[] objArr, int i, String str) {
        if (i < 0 || i >= objArr.length) {
            a("Index out of bounds detected: " + i + " in " + str);
        }
        return objArr[i];
    }

    public static void a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    private static void a(String str) {
        bk.a(str);
        throw new dm(str);
    }

    private static p b(y yVar) {
        if (((p) yVar.a(p.a)) == null) {
            a("Expected a ServingValue and didn't get one. Value is: " + yVar);
        }
        return (p) yVar.a(p.a);
    }
}
