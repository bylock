package com.google.android.gms.d;

import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.internal.df;
import com.google.android.gms.internal.dz;
import com.google.android.gms.internal.ea;
import com.google.android.gms.internal.n;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;

/* access modifiers changed from: package-private */
public class db implements ft {
    private final Context a;
    private final String b;
    private final ExecutorService c = Executors.newSingleThreadExecutor();
    private bi d;

    db(Context context, String str) {
        this.a = context;
        this.b = str;
    }

    private di a(ByteArrayOutputStream byteArrayOutputStream) {
        try {
            return bb.a(byteArrayOutputStream.toString("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            bk.d("Tried to convert binary resource to string for JSON parsing; not UTF-8 format");
            return null;
        } catch (JSONException e2) {
            bk.b("Resource is a UTF-8 encoded string but doesn't contain a JSON container");
            return null;
        }
    }

    private di a(byte[] bArr) {
        try {
            return de.a(n.a(bArr));
        } catch (dz e) {
            bk.b("Resource doesn't contain a binary container");
            return null;
        } catch (dm e2) {
            bk.b("Resource doesn't contain a binary container");
            return null;
        }
    }

    @Override // com.google.android.gms.d.ft
    public di a(int i) {
        bk.e("Atttempting to load container from resource ID " + i);
        try {
            InputStream openRawResource = this.a.getResources().openRawResource(i);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            de.a(openRawResource, byteArrayOutputStream);
            di a2 = a(byteArrayOutputStream);
            return a2 != null ? a2 : a(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            bk.b("Error reading default container resource with ID " + i);
            return null;
        } catch (Resources.NotFoundException e2) {
            bk.b("No default container resource found.");
            return null;
        }
    }

    @Override // com.google.android.gms.common.api.d
    public synchronized void a() {
        this.c.shutdown();
    }

    @Override // com.google.android.gms.d.ft
    public void a(bi biVar) {
        this.d = biVar;
    }

    @Override // com.google.android.gms.d.ft
    public void a(df dfVar) {
        this.c.execute(new dd(this, dfVar));
    }

    @Override // com.google.android.gms.d.ft
    public void b() {
        this.c.execute(new dc(this));
    }

    /* access modifiers changed from: package-private */
    public boolean b(df dfVar) {
        boolean z = false;
        File d2 = d();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(d2);
            try {
                fileOutputStream.write(ea.a(dfVar));
                z = true;
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    bk.b("error closing stream for writing resource to disk");
                }
            } catch (IOException e2) {
                bk.b("Error writing resource to disk. Removing resource from disk.");
                d2.delete();
                try {
                    fileOutputStream.close();
                } catch (IOException e3) {
                    bk.b("error closing stream for writing resource to disk");
                }
            } catch (Throwable th) {
                try {
                    fileOutputStream.close();
                } catch (IOException e4) {
                    bk.b("error closing stream for writing resource to disk");
                }
                throw th;
            }
        } catch (FileNotFoundException e5) {
            bk.a("Error opening resource file for writing");
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.d == null) {
            throw new IllegalStateException("callback must be set before execute");
        }
        this.d.a();
        bk.e("Start loading resource from disk ...");
        if ((ck.a().b() == cl.CONTAINER || ck.a().b() == cl.CONTAINER_DEBUG) && this.b.equals(ck.a().d())) {
            this.d.a(bj.NOT_AVAILABLE);
            return;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(d());
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                de.a(fileInputStream, byteArrayOutputStream);
                this.d.a(df.a(byteArrayOutputStream.toByteArray()));
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    bk.b("error closing stream for reading resource from disk");
                }
            } catch (IOException e2) {
                bk.b("error reading resource from disk");
                this.d.a(bj.IO_ERROR);
                try {
                    fileInputStream.close();
                } catch (IOException e3) {
                    bk.b("error closing stream for reading resource from disk");
                }
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (IOException e4) {
                    bk.b("error closing stream for reading resource from disk");
                }
                throw th;
            }
            bk.e("Load resource from disk finished.");
        } catch (FileNotFoundException e5) {
            bk.d("resource not on disk");
            this.d.a(bj.NOT_AVAILABLE);
        }
    }

    /* access modifiers changed from: package-private */
    public File d() {
        return new File(this.a.getDir("google_tagmanager", 0), "resource_" + this.b);
    }
}
