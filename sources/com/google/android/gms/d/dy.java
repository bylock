package com.google.android.gms.d;

class dy implements cn {
    private final long a;
    private final int b;
    private double c;
    private long d;
    private final Object e;

    public dy() {
        this(60, 2000);
    }

    public dy(int i, long j) {
        this.e = new Object();
        this.b = i;
        this.c = (double) this.b;
        this.a = j;
    }

    @Override // com.google.android.gms.d.cn
    public boolean a() {
        boolean z;
        synchronized (this.e) {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.c < ((double) this.b)) {
                double d2 = ((double) (currentTimeMillis - this.d)) / ((double) this.a);
                if (d2 > 0.0d) {
                    this.c = Math.min((double) this.b, d2 + this.c);
                }
            }
            this.d = currentTimeMillis;
            if (this.c >= 1.0d) {
                this.c -= 1.0d;
                z = true;
            } else {
                bk.b("No more tokens available.");
                z = false;
            }
        }
        return z;
    }
}
