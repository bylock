package com.google.android.gms.d;

import com.google.android.gms.internal.y;
import java.util.HashMap;
import java.util.Map;

public class dh {
    private final Map a;
    private y b;

    private dh() {
        this.a = new HashMap();
    }

    public dg a() {
        return new dg(this.a, this.b);
    }

    public dh a(y yVar) {
        this.b = yVar;
        return this;
    }

    public dh a(String str, y yVar) {
        this.a.put(str, yVar);
        return this;
    }
}
