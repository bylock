package com.google.android.gms.d;

import android.os.Handler;
import android.os.Message;

/* access modifiers changed from: package-private */
public class ec implements Handler.Callback {
    final /* synthetic */ ea a;

    ec(ea eaVar) {
        this.a = eaVar;
    }

    public boolean handleMessage(Message message) {
        if (1 == message.what && ea.a.equals(message.obj)) {
            this.a.d();
            if (this.a.e > 0 && !(this.a.m)) {
                this.a.k.sendMessageDelayed(this.a.k.obtainMessage(1, ea.a), (long) this.a.e);
            }
        }
        return true;
    }
}
