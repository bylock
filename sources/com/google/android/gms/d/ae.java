package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ae extends ag {
    private static final String a = a.EVENT.toString();
    private final Cdo b;

    public ae(Cdo doVar) {
        super(a, new String[0]);
        this.b = doVar;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        String a2 = this.b.a();
        return a2 == null ? er.e() : er.e(a2);
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }
}
