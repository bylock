package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.internal.n;
import com.google.android.gms.internal.q;
import com.google.android.gms.internal.r;
import com.google.android.gms.internal.y;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class a {
    private final Context a;
    private final String b;
    private final i c;
    private Cdo d;
    private Map e = new HashMap();
    private Map f = new HashMap();
    private volatile long g;
    private volatile String h = "";

    a(Context context, i iVar, String str, long j, di diVar) {
        this.a = context;
        this.c = iVar;
        this.b = str;
        this.g = j;
        a(diVar);
    }

    a(Context context, i iVar, String str, long j, r rVar) {
        this.a = context;
        this.c = iVar;
        this.b = str;
        this.g = j;
        a(rVar.b);
        if (rVar.a != null) {
            a(rVar.a);
        }
    }

    private void a(di diVar) {
        this.h = diVar.c();
        a(new Cdo(this.a, diVar, this.c, new e(this), new f(this), e(this.h)));
    }

    private synchronized void a(Cdo doVar) {
        this.d = doVar;
    }

    private void a(n nVar) {
        if (nVar == null) {
            throw new NullPointerException();
        }
        try {
            a(de.a(nVar));
        } catch (dm e2) {
            bk.a("Not loading resource: " + nVar + " because it is invalid: " + e2.toString());
        }
    }

    private void a(q[] qVarArr) {
        ArrayList arrayList = new ArrayList();
        for (q qVar : qVarArr) {
            arrayList.add(qVar);
        }
        e().a(arrayList);
    }

    private synchronized Cdo e() {
        return this.d;
    }

    public long a() {
        return this.g;
    }

    public String a(String str) {
        Cdo e2 = e();
        if (e2 == null) {
            bk.a("getString called for closed container.");
            return er.d();
        }
        try {
            return er.a((y) e2.b(str).a());
        } catch (Exception e3) {
            bk.a("Calling getString() threw an exception: " + e3.getMessage() + " Returning default value.");
            return er.d();
        }
    }

    /* access modifiers changed from: package-private */
    public c b(String str) {
        c cVar;
        synchronized (this.e) {
            cVar = (c) this.e.get(str);
        }
        return cVar;
    }

    public boolean b() {
        return a() == 0;
    }

    /* access modifiers changed from: package-private */
    public d c(String str) {
        d dVar;
        synchronized (this.f) {
            dVar = (d) this.f.get(str);
        }
        return dVar;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.d = null;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        e().a(str);
    }

    /* access modifiers changed from: package-private */
    public ad e(String str) {
        if (ck.a().b().equals(cl.CONTAINER_DEBUG)) {
        }
        return new bt();
    }
}
