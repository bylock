package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Locale;
import java.util.Map;

/* access modifiers changed from: package-private */
public class be extends ag {
    private static final String a = a.LANGUAGE.toString();

    public be() {
        super(a, new String[0]);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        Locale locale = Locale.getDefault();
        if (locale == null) {
            return er.e();
        }
        String language = locale.getLanguage();
        return language == null ? er.e() : er.e(language.toLowerCase());
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }
}
