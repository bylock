package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ci extends ag {
    private static final String a = a.PLATFORM.toString();
    private static final y b = er.e("Android");

    public ci() {
        super(a, new String[0]);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        return b;
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
