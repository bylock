package com.google.android.gms.d;

import java.util.Map;

class e implements fy {
    final /* synthetic */ a a;

    private e(a aVar) {
        this.a = aVar;
    }

    @Override // com.google.android.gms.d.fy
    public Object a(String str, Map map) {
        c b = this.a.b(str);
        if (b == null) {
            return null;
        }
        return b.a(str, map);
    }
}
