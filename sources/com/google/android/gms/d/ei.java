package com.google.android.gms.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

class ei implements y {
    private final String a = a("GoogleTagManager", "4.00", Build.VERSION.RELEASE, a(Locale.getDefault()), Build.MODEL, Build.ID);
    private final HttpClient b;
    private final Context c;
    private ej d;

    ei(HttpClient httpClient, Context context, ej ejVar) {
        this.c = context.getApplicationContext();
        this.b = httpClient;
        this.d = ejVar;
    }

    static String a(Locale locale) {
        if (locale == null || locale.getLanguage() == null || locale.getLanguage().length() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(locale.getLanguage().toLowerCase());
        if (!(locale.getCountry() == null || locale.getCountry().length() == 0)) {
            sb.append("-").append(locale.getCountry().toLowerCase());
        }
        return sb.toString();
    }

    private HttpEntityEnclosingRequest a(URL url) {
        URISyntaxException e;
        BasicHttpEntityEnclosingRequest basicHttpEntityEnclosingRequest;
        try {
            basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("GET", url.toURI().toString());
            try {
                basicHttpEntityEnclosingRequest.addHeader("User-Agent", this.a);
            } catch (URISyntaxException e2) {
                e = e2;
                bk.b("Exception sending hit: " + e.getClass().getSimpleName());
                bk.b(e.getMessage());
                return basicHttpEntityEnclosingRequest;
            }
        } catch (URISyntaxException e3) {
            basicHttpEntityEnclosingRequest = null;
            e = e3;
            bk.b("Exception sending hit: " + e.getClass().getSimpleName());
            bk.b(e.getMessage());
            return basicHttpEntityEnclosingRequest;
        }
        return basicHttpEntityEnclosingRequest;
    }

    private void a(HttpEntityEnclosingRequest httpEntityEnclosingRequest) {
        int available;
        StringBuffer stringBuffer = new StringBuffer();
        for (Header header : httpEntityEnclosingRequest.getAllHeaders()) {
            stringBuffer.append(header.toString()).append("\n");
        }
        stringBuffer.append(httpEntityEnclosingRequest.getRequestLine().toString()).append("\n");
        if (httpEntityEnclosingRequest.getEntity() != null) {
            try {
                InputStream content = httpEntityEnclosingRequest.getEntity().getContent();
                if (content != null && (available = content.available()) > 0) {
                    byte[] bArr = new byte[available];
                    content.read(bArr);
                    stringBuffer.append("POST:\n");
                    stringBuffer.append(new String(bArr)).append("\n");
                }
            } catch (IOException e) {
                bk.e("Error Writing hit to log...");
            }
        }
        bk.e(stringBuffer.toString());
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2, String str3, String str4, String str5, String str6) {
        return String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", str, str2, str3, str4, str5, str6);
    }

    /* access modifiers changed from: package-private */
    public URL a(am amVar) {
        try {
            return new URL(amVar.c());
        } catch (MalformedURLException e) {
            bk.a("Error trying to parse the GTM url.");
            return null;
        }
    }

    @Override // com.google.android.gms.d.y
    public void a(List list) {
        boolean z;
        int min = Math.min(list.size(), 40);
        boolean z2 = true;
        int i = 0;
        while (i < min) {
            am amVar = (am) list.get(i);
            URL a2 = a(amVar);
            if (a2 == null) {
                bk.b("No destination: discarding hit.");
                this.d.b(amVar);
                z = z2;
            } else {
                HttpEntityEnclosingRequest a3 = a(a2);
                if (a3 == null) {
                    this.d.b(amVar);
                    z = z2;
                } else {
                    HttpHost httpHost = new HttpHost(a2.getHost(), a2.getPort(), a2.getProtocol());
                    a3.addHeader("Host", httpHost.toHostString());
                    a(a3);
                    if (z2) {
                        try {
                            bq.b(this.c);
                            z2 = false;
                        } catch (ClientProtocolException e) {
                            bk.b("ClientProtocolException sending hit; discarding hit...");
                            this.d.b(amVar);
                            z = z2;
                        } catch (IOException e2) {
                            bk.b("Exception sending hit: " + e2.getClass().getSimpleName());
                            bk.b(e2.getMessage());
                            this.d.c(amVar);
                            z = z2;
                        }
                    }
                    HttpResponse execute = this.b.execute(httpHost, a3);
                    int statusCode = execute.getStatusLine().getStatusCode();
                    HttpEntity entity = execute.getEntity();
                    if (entity != null) {
                        entity.consumeContent();
                    }
                    if (statusCode != 200) {
                        bk.b("Bad response: " + execute.getStatusLine().getStatusCode());
                        this.d.c(amVar);
                    } else {
                        this.d.a(amVar);
                    }
                    z = z2;
                }
            }
            i++;
            z2 = z;
        }
    }

    @Override // com.google.android.gms.d.y
    public boolean a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.c.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        bk.e("...no network connectivity");
        return false;
    }
}
