package com.google.android.gms.d;

import com.google.android.gms.internal.df;
import com.google.android.gms.internal.n;
import com.google.android.gms.internal.r;

class fp implements bi {
    final /* synthetic */ fl a;

    private fp(fl flVar) {
        this.a = flVar;
    }

    /* synthetic */ fp(fl flVar, fm fmVar) {
        this(flVar);
    }

    @Override // com.google.android.gms.d.bi
    public void a() {
    }

    @Override // com.google.android.gms.d.bi
    public void a(bj bjVar) {
        if (!fl.b(this.a)) {
            fl.a(this.a, 0);
        }
    }

    public void a(df dfVar) {
        r rVar;
        if (dfVar.c != null) {
            rVar = dfVar.c;
        } else {
            n nVar = dfVar.b;
            rVar = new r();
            rVar.b = nVar;
            rVar.a = null;
            rVar.c = nVar.l;
        }
        fl.a(this.a, rVar, dfVar.a, true);
    }
}
