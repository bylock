package com.google.android.gms.d;

import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public abstract class cj extends ag {
    private static final String a = f.ARG0.toString();
    private static final String b = f.ARG1.toString();

    public cj(String str) {
        super(str, a, b);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        for (y yVar : map.values()) {
            if (yVar == er.e()) {
                return er.e((Object) false);
            }
        }
        y yVar2 = (y) map.get(a);
        y yVar3 = (y) map.get(b);
        return er.e(Boolean.valueOf((yVar2 == null || yVar3 == null) ? false : a(yVar2, yVar3, map)));
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(y yVar, y yVar2, Map map);
}
