package com.google.android.gms.d;

import java.util.Map;

class f implements fy {
    final /* synthetic */ a a;

    private f(a aVar) {
        this.a = aVar;
    }

    @Override // com.google.android.gms.d.fy
    public Object a(String str, Map map) {
        d c = this.a.c(str);
        if (c != null) {
            c.a(str, map);
        }
        return er.d();
    }
}
