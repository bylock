package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class av extends ag {
    private static final String a = a.INSTALL_REFERRER.toString();
    private static final String b = f.COMPONENT.toString();
    private final Context c;

    public av(Context context) {
        super(a, new String[0]);
        this.c = context;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        String a2 = aw.a(this.c, ((y) map.get(b)) != null ? er.a((y) map.get(b)) : null);
        return a2 != null ? er.e(a2) : er.e();
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
