package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class fu extends ag {
    private static final String a = a.CONTAINER_VERSION.toString();
    private final String b;

    public fu(String str) {
        super(a, new String[0]);
        this.b = str;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        return this.b == null ? er.e() : er.e(this.b);
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
