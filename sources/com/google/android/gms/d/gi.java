package com.google.android.gms.d;

import android.util.Log;

class gi implements bl {
    private int a = 5;

    gi() {
    }

    @Override // com.google.android.gms.d.bl
    public void a(String str) {
        if (this.a <= 6) {
            Log.e("GoogleTagManager", str);
        }
    }

    @Override // com.google.android.gms.d.bl
    public void a(String str, Throwable th) {
        if (this.a <= 6) {
            Log.e("GoogleTagManager", str, th);
        }
    }

    @Override // com.google.android.gms.d.bl
    public void b(String str) {
        if (this.a <= 5) {
            Log.w("GoogleTagManager", str);
        }
    }

    @Override // com.google.android.gms.d.bl
    public void b(String str, Throwable th) {
        if (this.a <= 5) {
            Log.w("GoogleTagManager", str, th);
        }
    }

    @Override // com.google.android.gms.d.bl
    public void c(String str) {
        if (this.a <= 4) {
            Log.i("GoogleTagManager", str);
        }
    }

    @Override // com.google.android.gms.d.bl
    public void d(String str) {
        if (this.a <= 3) {
            Log.d("GoogleTagManager", str);
        }
    }

    @Override // com.google.android.gms.d.bl
    public void e(String str) {
        if (this.a <= 2) {
            Log.v("GoogleTagManager", str);
        }
    }
}
