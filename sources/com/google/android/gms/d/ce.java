package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ce extends ag {
    private static final String a = a.ADVERTISING_TRACKING_ENABLED.toString();
    private final t b;

    public ce(Context context) {
        this(t.a(context));
    }

    ce(t tVar) {
        super(a, new String[0]);
        this.b = tVar;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        return er.e(Boolean.valueOf(!this.b.b()));
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }
}
