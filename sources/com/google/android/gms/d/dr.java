package com.google.android.gms.d;

import java.util.List;
import java.util.Map;
import java.util.Set;

/* access modifiers changed from: package-private */
public class dr implements dt {
    final /* synthetic */ Map a;
    final /* synthetic */ Map b;
    final /* synthetic */ Map c;
    final /* synthetic */ Map d;
    final /* synthetic */ Cdo e;

    dr(Cdo doVar, Map map, Map map2, Map map3, Map map4) {
        this.e = doVar;
        this.a = map;
        this.b = map2;
        this.c = map3;
        this.d = map4;
    }

    @Override // com.google.android.gms.d.dt
    public void a(dk dkVar, Set set, Set set2, cu cuVar) {
        List list = (List) this.a.get(dkVar);
        List list2 = (List) this.b.get(dkVar);
        if (list != null) {
            set.addAll(list);
            cuVar.c().a(list, list2);
        }
        List list3 = (List) this.c.get(dkVar);
        List list4 = (List) this.d.get(dkVar);
        if (list3 != null) {
            set2.addAll(list3);
            cuVar.d().a(list3, list4);
        }
    }
}
