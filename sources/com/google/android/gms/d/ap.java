package com.google.android.gms.d;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.LinkedBlockingQueue;

/* access modifiers changed from: package-private */
public class ap extends Thread implements ao {
    private static ap d;
    private final LinkedBlockingQueue a = new LinkedBlockingQueue();
    private volatile boolean b = false;
    private volatile boolean c = false;
    private volatile ar e;
    private final Context f;

    private ap(Context context) {
        super("GAThread");
        if (context != null) {
            this.f = context.getApplicationContext();
        } else {
            this.f = context;
        }
        start();
    }

    static ap a(Context context) {
        if (d == null) {
            d = new ap(context);
        }
        return d;
    }

    private String a(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    @Override // com.google.android.gms.d.ao
    public void a(Runnable runnable) {
        this.a.add(runnable);
    }

    @Override // com.google.android.gms.d.ao
    public void a(String str) {
        a(str, System.currentTimeMillis());
    }

    /* access modifiers changed from: package-private */
    public void a(String str, long j) {
        a(new aq(this, this, j, str));
    }

    public void run() {
        while (!this.c) {
            try {
                Runnable runnable = (Runnable) this.a.take();
                if (!this.b) {
                    runnable.run();
                }
            } catch (InterruptedException e2) {
                bk.c(e2.toString());
            } catch (Throwable th) {
                bk.a("Error on GAThread: " + a(th));
                bk.a("Google Analytics is shutting down.");
                this.b = true;
            }
        }
    }
}
