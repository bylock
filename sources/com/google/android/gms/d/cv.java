package com.google.android.gms.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.gms.internal.r;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

class cv implements Runnable {
    private final Context a;
    private final bp b;
    private final String c;
    private final String d;
    private bi e;
    private volatile fw f;
    private volatile String g;
    private volatile String h;

    cv(Context context, String str, bp bpVar, fw fwVar) {
        this.a = context;
        this.b = bpVar;
        this.c = str;
        this.f = fwVar;
        this.d = "/r?id=" + str;
        this.g = this.d;
        this.h = null;
    }

    public cv(Context context, String str, fw fwVar) {
        this(context, str, new bp(), fwVar);
    }

    private boolean b() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        bk.e("...no network connectivity");
        return false;
    }

    private void c() {
        if (!b()) {
            this.e.a(bj.NOT_AVAILABLE);
            return;
        }
        bk.e("Start loading resource from network ...");
        String a2 = a();
        bo a3 = this.b.a();
        try {
            InputStream a4 = a3.a(a2);
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                de.a(a4, byteArrayOutputStream);
                r a5 = r.a(byteArrayOutputStream.toByteArray());
                bk.e("Successfully loaded supplemented resource: " + a5);
                if (a5.b == null && a5.a.length == 0) {
                    bk.e("No change for container: " + this.c);
                }
                this.e.a(a5);
                a3.a();
                bk.e("Load resource from network finished.");
            } catch (IOException e2) {
                bk.b("Error when parsing downloaded resources from url: " + a2 + " " + e2.getMessage(), e2);
                this.e.a(bj.SERVER_ERROR);
            }
        } catch (FileNotFoundException e3) {
            bk.b("No data is retrieved from the given url: " + a2 + ". Make sure container_id: " + this.c + " is correct.");
            this.e.a(bj.SERVER_ERROR);
        } catch (IOException e4) {
            bk.b("Error when loading resources from url: " + a2 + " " + e4.getMessage(), e4);
            this.e.a(bj.IO_ERROR);
        } finally {
            a3.a();
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        String str = this.f.a() + this.g + "&v=a65833898";
        if (this.h != null && !this.h.trim().equals("")) {
            str = str + "&pv=" + this.h;
        }
        return ck.a().b().equals(cl.CONTAINER_DEBUG) ? str + "&gtm_debug=x" : str;
    }

    /* access modifiers changed from: package-private */
    public void a(bi biVar) {
        this.e = biVar;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (str == null) {
            this.g = this.d;
            return;
        }
        bk.d("Setting CTFE URL path: " + str);
        this.g = str;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        bk.d("Setting previous container version: " + str);
        this.h = str;
    }

    public void run() {
        if (this.e == null) {
            throw new IllegalStateException("callback must be set before execute");
        }
        this.e.a();
        c();
    }
}
