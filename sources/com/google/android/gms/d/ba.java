package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ba extends ag {
    private static final String a = a.ADVERTISER_ID.toString();
    private final t b;

    public ba(Context context) {
        this(t.a(context));
    }

    ba(t tVar) {
        super(a, new String[0]);
        this.b = tVar;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        String a2 = this.b.a();
        return a2 == null ? er.e() : er.e(a2);
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }
}
