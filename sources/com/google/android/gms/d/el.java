package com.google.android.gms.d;

import com.google.android.gms.internal.y;
import java.util.Map;

abstract class el extends cj {
    public el(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.d.cj
    public boolean a(y yVar, y yVar2, Map map) {
        String a = er.a(yVar);
        String a2 = er.a(yVar2);
        if (a == er.d() || a2 == er.d()) {
            return false;
        }
        return a(a, a2, map);
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(String str, String str2, Map map);
}
