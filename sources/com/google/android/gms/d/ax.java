package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* access modifiers changed from: package-private */
public class ax extends ag {
    private static final String a = a.JOINER.toString();
    private static final String b = f.ARG0.toString();
    private static final String c = f.ITEM_SEPARATOR.toString();
    private static final String d = f.KEY_VALUE_SEPARATOR.toString();
    private static final String e = f.ESCAPE.toString();

    public ax() {
        super(a, b);
    }

    private String a(String str, az azVar, Set set) {
        switch (azVar) {
            case URL:
                try {
                    return eu.a(str);
                } catch (UnsupportedEncodingException e2) {
                    bk.a("Joiner: unsupported encoding", e2);
                    return str;
                }
            case BACKSLASH:
                String replace = str.replace("\\", "\\\\");
                Iterator it = set.iterator();
                while (it.hasNext()) {
                    String ch = ((Character) it.next()).toString();
                    replace = replace.replace(ch, "\\" + ch);
                }
                return replace;
            default:
                return str;
        }
    }

    private void a(StringBuilder sb, String str, az azVar, Set set) {
        sb.append(a(str, azVar, set));
    }

    private void a(Set set, String str) {
        for (int i = 0; i < str.length(); i++) {
            set.add(Character.valueOf(str.charAt(i)));
        }
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        HashSet hashSet;
        az azVar;
        y yVar = (y) map.get(b);
        if (yVar == null) {
            return er.e();
        }
        y yVar2 = (y) map.get(c);
        String a2 = yVar2 != null ? er.a(yVar2) : "";
        y yVar3 = (y) map.get(d);
        String a3 = yVar3 != null ? er.a(yVar3) : "=";
        az azVar2 = az.NONE;
        y yVar4 = (y) map.get(e);
        if (yVar4 != null) {
            String a4 = er.a(yVar4);
            if ("url".equals(a4)) {
                azVar = az.URL;
                hashSet = null;
            } else if ("backslash".equals(a4)) {
                azVar = az.BACKSLASH;
                hashSet = new HashSet();
                a(hashSet, a2);
                a(hashSet, a3);
                hashSet.remove('\\');
            } else {
                bk.a("Joiner: unsupported escape type: " + a4);
                return er.e();
            }
        } else {
            hashSet = null;
            azVar = azVar2;
        }
        StringBuilder sb = new StringBuilder();
        switch (yVar.a) {
            case 2:
                boolean z = true;
                y[] yVarArr = yVar.c;
                int length = yVarArr.length;
                int i = 0;
                while (i < length) {
                    y yVar5 = yVarArr[i];
                    if (!z) {
                        sb.append(a2);
                    }
                    a(sb, er.a(yVar5), azVar, hashSet);
                    i++;
                    z = false;
                }
                break;
            case 3:
                for (int i2 = 0; i2 < yVar.d.length; i2++) {
                    if (i2 > 0) {
                        sb.append(a2);
                    }
                    String a5 = er.a(yVar.d[i2]);
                    String a6 = er.a(yVar.e[i2]);
                    a(sb, a5, azVar, hashSet);
                    sb.append(a3);
                    a(sb, a6, azVar, hashSet);
                }
                break;
            default:
                a(sb, er.a(yVar), azVar, hashSet);
                break;
        }
        return er.e(sb.toString());
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
