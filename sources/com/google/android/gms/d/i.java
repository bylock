package com.google.android.gms.d;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class i {
    public static final Object a = new Object();
    static final String[] b = "gtm.lifetime".toString().split("\\.");
    private static final Pattern c = Pattern.compile("(\\d+)\\s*([smhd]?)");
    private final ConcurrentHashMap d;
    private final Map e;
    private final ReentrantLock f;
    private final LinkedList g;
    private final n h;
    private final CountDownLatch i;

    i() {
        this(new j());
    }

    i(n nVar) {
        this.h = nVar;
        this.d = new ConcurrentHashMap();
        this.e = new HashMap();
        this.f = new ReentrantLock();
        this.g = new LinkedList();
        this.i = new CountDownLatch(1);
        a();
    }

    public static Map a(Object... objArr) {
        if (objArr.length % 2 != 0) {
            throw new IllegalArgumentException("expected even number of key-value pairs");
        }
        HashMap hashMap = new HashMap();
        for (int i2 = 0; i2 < objArr.length; i2 += 2) {
            if (!(objArr[i2] instanceof String)) {
                throw new IllegalArgumentException("key is not a string: " + objArr[i2]);
            }
            hashMap.put((String) objArr[i2], objArr[i2 + 1]);
        }
        return hashMap;
    }

    private void a() {
        this.h.a(new k(this));
    }

    private void a(Map map, String str, Collection collection) {
        for (Map.Entry entry : map.entrySet()) {
            String str2 = str + (str.length() == 0 ? "" : ".") + ((String) entry.getKey());
            if (entry.getValue() instanceof Map) {
                a((Map) entry.getValue(), str2, collection);
            } else if (!str2.equals("gtm.lifetime")) {
                collection.add(new l(str2, entry.getValue()));
            }
        }
    }

    static Long b(String str) {
        long j;
        Matcher matcher = c.matcher(str);
        if (!matcher.matches()) {
            bk.c("unknown _lifetime: " + str);
            return null;
        }
        try {
            j = Long.parseLong(matcher.group(1));
        } catch (NumberFormatException e2) {
            bk.b("illegal number in _lifetime value: " + str);
            j = 0;
        }
        if (j <= 0) {
            bk.c("non-positive _lifetime: " + str);
            return null;
        }
        String group2 = matcher.group(2);
        if (group2.length() == 0) {
            return Long.valueOf(j);
        }
        switch (group2.charAt(0)) {
            case 'd':
                return Long.valueOf(j * 1000 * 60 * 60 * 24);
            case 'h':
                return Long.valueOf(j * 1000 * 60 * 60);
            case 'm':
                return Long.valueOf(j * 1000 * 60);
            case 's':
                return Long.valueOf(j * 1000);
            default:
                bk.b("unknown units in _lifetime: " + str);
                return null;
        }
    }

    private void b() {
        int i2 = 0;
        while (true) {
            Map map = (Map) this.g.poll();
            if (map != null) {
                g(map);
                i2++;
                if (i2 > 500) {
                    this.g.clear();
                    throw new RuntimeException("Seems like an infinite loop of pushing to the data layer");
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(Map map) {
        this.f.lock();
        try {
            this.g.offer(map);
            if (this.f.getHoldCount() == 1) {
                b();
            }
            c(map);
        } finally {
            this.f.unlock();
        }
    }

    private void c(Map map) {
        Long d2 = d(map);
        if (d2 != null) {
            List f2 = f(map);
            f2.remove("gtm.lifetime");
            this.h.a(f2, d2.longValue());
        }
    }

    private Long d(Map map) {
        Object e2 = e(map);
        if (e2 == null) {
            return null;
        }
        return b(e2.toString());
    }

    private Object e(Map map) {
        String[] strArr = b;
        int length = strArr.length;
        int i2 = 0;
        Object obj = map;
        while (i2 < length) {
            String str = strArr[i2];
            if (!(obj instanceof Map)) {
                return null;
            }
            i2++;
            obj = ((Map) obj).get(str);
        }
        return obj;
    }

    private List f(Map map) {
        ArrayList arrayList = new ArrayList();
        a(map, "", arrayList);
        return arrayList;
    }

    private void g(Map map) {
        synchronized (this.e) {
            for (String str : map.keySet()) {
                a(b(str, map.get(str)), this.e);
            }
        }
        h(map);
    }

    private void h(Map map) {
        for (m mVar : this.d.keySet()) {
            mVar.a(map);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(m mVar) {
        this.d.put(mVar, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        a(str, (Object) null);
        this.h.a(str);
    }

    public void a(String str, Object obj) {
        a(b(str, obj));
    }

    /* access modifiers changed from: package-private */
    public void a(List list, List list2) {
        while (list2.size() < list.size()) {
            list2.add(null);
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            Object obj = list.get(i2);
            if (obj instanceof List) {
                if (!(list2.get(i2) instanceof List)) {
                    list2.set(i2, new ArrayList());
                }
                a((List) obj, (List) list2.get(i2));
            } else if (obj instanceof Map) {
                if (!(list2.get(i2) instanceof Map)) {
                    list2.set(i2, new HashMap());
                }
                a((Map) obj, (Map) list2.get(i2));
            } else if (obj != a) {
                list2.set(i2, obj);
            }
        }
    }

    public void a(Map map) {
        try {
            this.i.await();
        } catch (InterruptedException e2) {
            bk.b("DataLayer.push: unexpected InterruptedException");
        }
        b(map);
    }

    /* access modifiers changed from: package-private */
    public void a(Map map, Map map2) {
        for (String str : map.keySet()) {
            Object obj = map.get(str);
            if (obj instanceof List) {
                if (!(map2.get(str) instanceof List)) {
                    map2.put(str, new ArrayList());
                }
                a((List) obj, (List) map2.get(str));
            } else if (obj instanceof Map) {
                if (!(map2.get(str) instanceof Map)) {
                    map2.put(str, new HashMap());
                }
                a((Map) obj, (Map) map2.get(str));
            } else {
                map2.put(str, obj);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Map b(String str, Object obj) {
        HashMap hashMap = new HashMap();
        String[] split = str.toString().split("\\.");
        int i2 = 0;
        HashMap hashMap2 = hashMap;
        while (i2 < split.length - 1) {
            HashMap hashMap3 = new HashMap();
            hashMap2.put(split[i2], hashMap3);
            i2++;
            hashMap2 = hashMap3;
        }
        hashMap2.put(split[split.length - 1], obj);
        return hashMap;
    }

    public Object c(String str) {
        synchronized (this.e) {
            Object obj = this.e;
            String[] split = str.split("\\.");
            int length = split.length;
            Object obj2 = obj;
            int i2 = 0;
            while (i2 < length) {
                String str2 = split[i2];
                if (!(obj2 instanceof Map)) {
                    return null;
                }
                Object obj3 = ((Map) obj2).get(str2);
                if (obj3 == null) {
                    return null;
                }
                i2++;
                obj2 = obj3;
            }
            return obj2;
        }
    }
}
