package com.google.android.gms.d;

import java.util.ArrayList;
import java.util.List;

public class dl {
    private final List a;
    private final List b;
    private final List c;
    private final List d;
    private final List e;
    private final List f;
    private final List g;
    private final List h;
    private final List i;
    private final List j;

    private dl() {
        this.a = new ArrayList();
        this.b = new ArrayList();
        this.c = new ArrayList();
        this.d = new ArrayList();
        this.e = new ArrayList();
        this.f = new ArrayList();
        this.g = new ArrayList();
        this.h = new ArrayList();
        this.i = new ArrayList();
        this.j = new ArrayList();
    }

    public dk a() {
        return new dk(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j);
    }

    public dl a(dg dgVar) {
        this.a.add(dgVar);
        return this;
    }

    public dl a(String str) {
        this.i.add(str);
        return this;
    }

    public dl b(dg dgVar) {
        this.b.add(dgVar);
        return this;
    }

    public dl b(String str) {
        this.j.add(str);
        return this;
    }

    public dl c(dg dgVar) {
        this.c.add(dgVar);
        return this;
    }

    public dl c(String str) {
        this.g.add(str);
        return this;
    }

    public dl d(dg dgVar) {
        this.d.add(dgVar);
        return this;
    }

    public dl d(String str) {
        this.h.add(str);
        return this;
    }

    public dl e(dg dgVar) {
        this.e.add(dgVar);
        return this;
    }

    public dl f(dg dgVar) {
        this.f.add(dgVar);
        return this;
    }
}
