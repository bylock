package com.google.android.gms.d;

import java.util.Map;

/* access modifiers changed from: package-private */
public class q implements m {
    final /* synthetic */ p a;

    q(p pVar) {
        this.a = pVar;
    }

    @Override // com.google.android.gms.d.m
    public void a(Map map) {
        Object obj = map.get("event");
        if (obj != null) {
            this.a.a((p) obj.toString());
        }
    }
}
