package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class dw extends ag {
    private static final String a = a.RUNTIME_VERSION.toString();

    public dw() {
        super(a, new String[0]);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        return er.e((Object) 65833898L);
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
