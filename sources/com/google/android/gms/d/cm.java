package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class cm extends ag {
    private static final String a = a.RANDOM.toString();
    private static final String b = f.MIN.toString();
    private static final String c = f.MAX.toString();

    public cm() {
        super(a, new String[0]);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        double d;
        double d2;
        y yVar = (y) map.get(b);
        y yVar2 = (y) map.get(c);
        if (!(yVar == null || yVar == er.e() || yVar2 == null || yVar2 == er.e())) {
            eq b2 = er.b(yVar);
            eq b3 = er.b(yVar2);
            if (!(b2 == er.c() || b3 == er.c())) {
                double doubleValue = b2.doubleValue();
                d = b3.doubleValue();
                if (doubleValue <= d) {
                    d2 = doubleValue;
                    return er.e(Long.valueOf(Math.round(((d - d2) * Math.random()) + d2)));
                }
            }
        }
        d = 2.147483647E9d;
        d2 = 0.0d;
        return er.e(Long.valueOf(Math.round(((d - d2) * Math.random()) + d2)));
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }
}
