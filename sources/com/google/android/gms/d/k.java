package com.google.android.gms.d;

import java.util.Iterator;
import java.util.List;

/* access modifiers changed from: package-private */
public class k implements o {
    final /* synthetic */ i a;

    k(i iVar) {
        this.a = iVar;
    }

    @Override // com.google.android.gms.d.o
    public void a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            l lVar = (l) it.next();
            this.a.b((i) this.a.b(lVar.a, lVar.b));
        }
        this.a.i.countDown();
    }
}
