package com.google.android.gms.d;

import android.content.Context;
import android.content.pm.PackageManager;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ex extends ag {
    private static final String a = a.APP_NAME.toString();
    private final Context b;

    public ex(Context context) {
        super(a, new String[0]);
        this.b = context;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        try {
            PackageManager packageManager = this.b.getPackageManager();
            return er.e(packageManager.getApplicationLabel(packageManager.getApplicationInfo(this.b.getPackageName(), 0)).toString());
        } catch (PackageManager.NameNotFoundException e) {
            bk.a("App name is not found.", e);
            return er.e();
        }
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
