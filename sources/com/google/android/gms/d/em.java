package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class em extends ag {
    private static final String a = a.TIME.toString();

    public em() {
        super(a, new String[0]);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        return er.e(Long.valueOf(System.currentTimeMillis()));
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }
}
