package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.HashMap;
import java.util.Map;

/* access modifiers changed from: package-private */
public class fx extends ag {
    private static final String a = a.FUNCTION_CALL.toString();
    private static final String b = f.FUNCTION_CALL_NAME.toString();
    private static final String c = f.ADDITIONAL_PARAMS.toString();
    private final fy d;

    public fx(fy fyVar) {
        super(a, b);
        this.d = fyVar;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        String a2 = er.a((y) map.get(b));
        HashMap hashMap = new HashMap();
        y yVar = (y) map.get(c);
        if (yVar != null) {
            Object e = er.e(yVar);
            if (!(e instanceof Map)) {
                bk.b("FunctionCallMacro: expected ADDITIONAL_PARAMS to be a map.");
                return er.e();
            }
            for (Map.Entry entry : ((Map) e).entrySet()) {
                hashMap.put(entry.getKey().toString(), entry.getValue());
            }
        }
        try {
            return er.e(this.d.a(a2, hashMap));
        } catch (Exception e2) {
            bk.b("Custom macro/tag " + a2 + " threw exception " + e2.getMessage());
            return er.e();
        }
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }
}
