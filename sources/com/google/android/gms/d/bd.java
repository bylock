package com.google.android.gms.d;

import android.util.LruCache;

class bd extends LruCache {
    final /* synthetic */ fg a;
    final /* synthetic */ bc b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bd(bc bcVar, int i, fg fgVar) {
        super(i);
        this.b = bcVar;
        this.a = fgVar;
    }

    /* access modifiers changed from: protected */
    @Override // android.util.LruCache
    public int sizeOf(Object obj, Object obj2) {
        return this.a.a(obj, obj2);
    }
}
