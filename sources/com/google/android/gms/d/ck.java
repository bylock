package com.google.android.gms.d;

class ck {
    private static ck a;
    private volatile cl b;
    private volatile String c;
    private volatile String d;
    private volatile String e;

    ck() {
        e();
    }

    static ck a() {
        ck ckVar;
        synchronized (ck.class) {
            if (a == null) {
                a = new ck();
            }
            ckVar = a;
        }
        return ckVar;
    }

    /* access modifiers changed from: package-private */
    public cl b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.b = cl.NONE;
        this.d = null;
        this.c = null;
        this.e = null;
    }
}
