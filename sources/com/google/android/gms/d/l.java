package com.google.android.gms.d;

import java.util.Arrays;

/* access modifiers changed from: package-private */
public final class l {
    public final String a;
    public final Object b;

    l(String str, Object obj) {
        this.a = str;
        this.b = obj;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        return this.a.equals(lVar.a) && this.b.equals(lVar.b);
    }

    public int hashCode() {
        return Arrays.hashCode(new Integer[]{Integer.valueOf(this.a.hashCode()), Integer.valueOf(this.b.hashCode())});
    }

    public String toString() {
        return "Key: " + this.a + " value: " + this.b.toString();
    }
}
