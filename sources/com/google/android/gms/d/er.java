package com.google.android.gms.d;

import com.google.android.gms.internal.y;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* access modifiers changed from: package-private */
public class er {
    private static final Object a = null;
    private static Long b = new Long(0);
    private static Double c = new Double(0.0d);
    private static eq d = eq.a(0);
    private static String e = new String("");
    private static Boolean f = new Boolean(false);
    private static List g = new ArrayList(0);
    private static Map h = new HashMap();
    private static y i = e(e);

    public static y a(String str) {
        y yVar = new y();
        yVar.a = 5;
        yVar.g = str;
        return yVar;
    }

    public static Object a() {
        return a;
    }

    public static String a(y yVar) {
        return a(e(yVar));
    }

    public static String a(Object obj) {
        return obj == null ? e : obj.toString();
    }

    public static eq b(y yVar) {
        return b(e(yVar));
    }

    public static eq b(Object obj) {
        return obj instanceof eq ? (eq) obj : h(obj) ? eq.a(i(obj)) : f(obj) ? eq.a(Double.valueOf(g(obj))) : b(a(obj));
    }

    private static eq b(String str) {
        try {
            return eq.a(str);
        } catch (NumberFormatException e2) {
            bk.a("Failed to convert '" + str + "' to a number.");
            return d;
        }
    }

    public static Long b() {
        return b;
    }

    public static eq c() {
        return d;
    }

    public static Long c(y yVar) {
        return c(e(yVar));
    }

    public static Long c(Object obj) {
        return h(obj) ? Long.valueOf(i(obj)) : c(a(obj));
    }

    private static Long c(String str) {
        eq b2 = b(str);
        return b2 == d ? b : Long.valueOf(b2.longValue());
    }

    public static Boolean d(y yVar) {
        return d(e(yVar));
    }

    public static Boolean d(Object obj) {
        return obj instanceof Boolean ? (Boolean) obj : d(a(obj));
    }

    private static Boolean d(String str) {
        return "true".equalsIgnoreCase(str) ? Boolean.TRUE : "false".equalsIgnoreCase(str) ? Boolean.FALSE : f;
    }

    public static String d() {
        return e;
    }

    public static y e() {
        return i;
    }

    public static y e(Object obj) {
        boolean z = false;
        y yVar = new y();
        if (obj instanceof y) {
            return (y) obj;
        }
        if (obj instanceof String) {
            yVar.a = 1;
            yVar.b = (String) obj;
        } else if (obj instanceof List) {
            yVar.a = 2;
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            boolean z2 = false;
            for (Object obj2 : list) {
                y e2 = e(obj2);
                if (e2 == i) {
                    return i;
                }
                boolean z3 = z2 || e2.l;
                arrayList.add(e2);
                z2 = z3;
            }
            yVar.c = (y[]) arrayList.toArray(new y[0]);
            z = z2;
        } else if (obj instanceof Map) {
            yVar.a = 3;
            Set<Map.Entry> entrySet = ((Map) obj).entrySet();
            ArrayList arrayList2 = new ArrayList(entrySet.size());
            ArrayList arrayList3 = new ArrayList(entrySet.size());
            boolean z4 = false;
            for (Map.Entry entry : entrySet) {
                y e3 = e(entry.getKey());
                y e4 = e(entry.getValue());
                if (e3 == i || e4 == i) {
                    return i;
                }
                boolean z5 = z4 || e3.l || e4.l;
                arrayList2.add(e3);
                arrayList3.add(e4);
                z4 = z5;
            }
            yVar.d = (y[]) arrayList2.toArray(new y[0]);
            yVar.e = (y[]) arrayList3.toArray(new y[0]);
            z = z4;
        } else if (f(obj)) {
            yVar.a = 1;
            yVar.b = obj.toString();
        } else if (h(obj)) {
            yVar.a = 6;
            yVar.h = i(obj);
        } else if (obj instanceof Boolean) {
            yVar.a = 8;
            yVar.i = ((Boolean) obj).booleanValue();
        } else {
            bk.a("Converting to Value from unknown object type: " + (obj == null ? "null" : obj.getClass().toString()));
            return i;
        }
        yVar.l = z;
        return yVar;
    }

    public static Object e(y yVar) {
        int i2 = 0;
        if (yVar == null) {
            return a;
        }
        switch (yVar.a) {
            case 1:
                return yVar.b;
            case 2:
                ArrayList arrayList = new ArrayList(yVar.c.length);
                y[] yVarArr = yVar.c;
                int length = yVarArr.length;
                while (i2 < length) {
                    Object e2 = e(yVarArr[i2]);
                    if (e2 == a) {
                        return a;
                    }
                    arrayList.add(e2);
                    i2++;
                }
                return arrayList;
            case 3:
                if (yVar.d.length != yVar.e.length) {
                    bk.a("Converting an invalid value to object: " + yVar.toString());
                    return a;
                }
                HashMap hashMap = new HashMap(yVar.e.length);
                while (i2 < yVar.d.length) {
                    Object e3 = e(yVar.d[i2]);
                    Object e4 = e(yVar.e[i2]);
                    if (e3 == a || e4 == a) {
                        return a;
                    }
                    hashMap.put(e3, e4);
                    i2++;
                }
                return hashMap;
            case 4:
                bk.a("Trying to convert a macro reference to object");
                return a;
            case 5:
                bk.a("Trying to convert a function id to object");
                return a;
            case 6:
                return Long.valueOf(yVar.h);
            case 7:
                StringBuffer stringBuffer = new StringBuffer();
                y[] yVarArr2 = yVar.j;
                int length2 = yVarArr2.length;
                while (i2 < length2) {
                    String a2 = a(yVarArr2[i2]);
                    if (a2 == e) {
                        return a;
                    }
                    stringBuffer.append(a2);
                    i2++;
                }
                return stringBuffer.toString();
            case 8:
                return Boolean.valueOf(yVar.i);
            default:
                bk.a("Failed to convert a value of type: " + yVar.a);
                return a;
        }
    }

    private static boolean f(Object obj) {
        return (obj instanceof Double) || (obj instanceof Float) || ((obj instanceof eq) && ((eq) obj).a());
    }

    private static double g(Object obj) {
        if (obj instanceof Number) {
            return ((Number) obj).doubleValue();
        }
        bk.a("getDouble received non-Number");
        return 0.0d;
    }

    private static boolean h(Object obj) {
        return (obj instanceof Byte) || (obj instanceof Short) || (obj instanceof Integer) || (obj instanceof Long) || ((obj instanceof eq) && ((eq) obj).b());
    }

    private static long i(Object obj) {
        if (obj instanceof Number) {
            return ((Number) obj).longValue();
        }
        bk.a("getInt64 received non-Number");
        return 0;
    }
}
