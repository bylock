package com.google.android.gms.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import java.util.HashMap;
import java.util.Map;

class aw {
    static Map a = new HashMap();
    private static String b;

    aw() {
    }

    static String a(Context context, String str) {
        if (b == null) {
            synchronized (aw.class) {
                if (b == null) {
                    SharedPreferences sharedPreferences = context.getSharedPreferences("gtm_install_referrer", 0);
                    if (sharedPreferences != null) {
                        b = sharedPreferences.getString("referrer", "");
                    } else {
                        b = "";
                    }
                }
            }
        }
        return a(b, str);
    }

    static String a(Context context, String str, String str2) {
        String str3 = (String) a.get(str);
        if (str3 == null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("gtm_click_referrers", 0);
            str3 = sharedPreferences != null ? sharedPreferences.getString(str, "") : "";
            a.put(str, str3);
        }
        return a(str3, str2);
    }

    static String a(String str, String str2) {
        if (str2 != null) {
            return Uri.parse("http://hostname/?" + str).getQueryParameter(str2);
        }
        if (str.length() > 0) {
            return str;
        }
        return null;
    }

    static void b(Context context, String str) {
        String a2 = a(str, "conv");
        if (a2 != null && a2.length() > 0) {
            a.put(a2, str);
            ee.a(context, "gtm_click_referrers", a2, str);
        }
    }
}
