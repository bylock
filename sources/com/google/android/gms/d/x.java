package com.google.android.gms.d;

import android.os.Build;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class x extends ag {
    private static final String a = a.DEVICE_NAME.toString();

    public x() {
        super(a, new String[0]);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        if (!str2.startsWith(str) && !str.equals("unknown")) {
            str2 = str + " " + str2;
        }
        return er.e(str2);
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
