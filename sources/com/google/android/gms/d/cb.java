package com.google.android.gms.d;

import com.google.android.gms.internal.y;
import java.util.Map;

abstract class cb extends cj {
    public cb(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(eq eqVar, eq eqVar2, Map map);

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.d.cj
    public boolean a(y yVar, y yVar2, Map map) {
        eq b = er.b(yVar);
        eq b2 = er.b(yVar2);
        if (b == er.c() || b2 == er.c()) {
            return false;
        }
        return a(b, b2, map);
    }
}
