package com.google.android.gms.d;

class aq implements Runnable {
    final /* synthetic */ ao a;
    final /* synthetic */ long b;
    final /* synthetic */ String c;
    final /* synthetic */ ap d;

    aq(ap apVar, ao aoVar, long j, String str) {
        this.d = apVar;
        this.a = aoVar;
        this.b = j;
        this.c = str;
    }

    public void run() {
        if (ap.a(this.d) == null) {
            ea b2 = ea.b();
            b2.a(ap.b(this.d), this.a);
            ap.a(this.d, b2.c());
        }
        ap.a(this.d).a(this.b, this.c);
    }
}
