package com.google.android.gms.d;

import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class bb {
    public static di a(String str) {
        y b = b(new JSONObject(str));
        dj a = di.a();
        for (int i = 0; i < b.d.length; i++) {
            a.a(dg.a().a(f.INSTANCE_NAME.toString(), b.d[i]).a(f.FUNCTION.toString(), er.a(fh.d())).a(fh.e(), b.e[i]).a());
        }
        return a.a();
    }

    static Object a(Object obj) {
        if (obj instanceof JSONArray) {
            throw new RuntimeException("JSONArrays are not supported");
        } else if (JSONObject.NULL.equals(obj)) {
            throw new RuntimeException("JSON nulls are not supported");
        } else if (!(obj instanceof JSONObject)) {
            return obj;
        } else {
            JSONObject jSONObject = (JSONObject) obj;
            HashMap hashMap = new HashMap();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, a(jSONObject.get(next)));
            }
            return hashMap;
        }
    }

    private static y b(Object obj) {
        return er.e(a(obj));
    }
}
