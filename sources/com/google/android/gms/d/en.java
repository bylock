package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.b.a;
import com.google.android.gms.b.e;

/* access modifiers changed from: package-private */
public class en {
    private a a;
    private Context b;
    private e c;

    en(Context context) {
        this.b = context;
    }

    private synchronized void b(String str) {
        if (this.a == null) {
            this.a = a.a(this.b);
            this.a.a(new eo());
            this.c = this.a.a(str);
        }
    }

    public e a(String str) {
        b(str);
        return this.c;
    }
}
