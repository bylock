package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ga extends ag {
    private static final String a = a.CUSTOM_VAR.toString();
    private static final String b = f.NAME.toString();
    private static final String c = f.DEFAULT_VALUE.toString();
    private final i d;

    public ga(i iVar) {
        super(a, b);
        this.d = iVar;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        Object c2 = this.d.c(er.a((y) map.get(b)));
        if (c2 != null) {
            return er.e(c2);
        }
        y yVar = (y) map.get(c);
        return yVar != null ? yVar : er.e();
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }
}
