package com.google.android.gms.d;

import android.content.Context;
import android.net.Uri;
import java.util.Map;

class eh implements m {
    private final Context a;

    public eh(Context context) {
        this.a = context;
    }

    @Override // com.google.android.gms.d.m
    public void a(Map map) {
        String queryParameter;
        Object obj;
        Object obj2 = map.get("gtm.url");
        Object obj3 = (obj2 != null || (obj = map.get("gtm")) == null || !(obj instanceof Map)) ? obj2 : ((Map) obj).get("url");
        if (obj3 != null && (obj3 instanceof String) && (queryParameter = Uri.parse((String) obj3).getQueryParameter("referrer")) != null) {
            aw.b(this.a, queryParameter);
        }
    }
}
