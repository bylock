package com.google.android.gms.d;

import com.google.android.gms.internal.y;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* access modifiers changed from: package-private */
public abstract class ag {
    private final Set a;
    private final String b;

    public ag(String str, String... strArr) {
        this.b = str;
        this.a = new HashSet(strArr.length);
        for (String str2 : strArr) {
            this.a.add(str2);
        }
    }

    public abstract y a(Map map);

    public abstract boolean a();

    /* access modifiers changed from: package-private */
    public boolean a(Set set) {
        return set.containsAll(this.a);
    }

    public String b() {
        return this.b;
    }

    public Set c() {
        return this.a;
    }
}
