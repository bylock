package com.google.android.gms.d;

/* access modifiers changed from: package-private */
public final class bk {
    static bl a = new gi();
    static int b;

    public static int a() {
        return b;
    }

    public static void a(String str) {
        a.a(str);
    }

    public static void a(String str, Throwable th) {
        a.a(str, th);
    }

    public static void b(String str) {
        a.b(str);
    }

    public static void b(String str, Throwable th) {
        a.b(str, th);
    }

    public static void c(String str) {
        a.c(str);
    }

    public static void d(String str) {
        a.d(str);
    }

    public static void e(String str) {
        a.e(str);
    }
}
