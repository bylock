package com.google.android.gms.d;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* access modifiers changed from: package-private */
public class dv {
    private final Set a = new HashSet();
    private final Map b = new HashMap();
    private final Map c = new HashMap();
    private final Map d = new HashMap();
    private final Map e = new HashMap();
    private dg f;

    public Set a() {
        return this.a;
    }

    public void a(dg dgVar) {
        this.f = dgVar;
    }

    public void a(dk dkVar) {
        this.a.add(dkVar);
    }

    public void a(dk dkVar, dg dgVar) {
        List list = (List) this.b.get(dkVar);
        if (list == null) {
            list = new ArrayList();
            this.b.put(dkVar, list);
        }
        list.add(dgVar);
    }

    public void a(dk dkVar, String str) {
        List list = (List) this.d.get(dkVar);
        if (list == null) {
            list = new ArrayList();
            this.d.put(dkVar, list);
        }
        list.add(str);
    }

    public Map b() {
        return this.b;
    }

    public void b(dk dkVar, dg dgVar) {
        List list = (List) this.c.get(dkVar);
        if (list == null) {
            list = new ArrayList();
            this.c.put(dkVar, list);
        }
        list.add(dgVar);
    }

    public void b(dk dkVar, String str) {
        List list = (List) this.e.get(dkVar);
        if (list == null) {
            list = new ArrayList();
            this.e.put(dkVar, list);
        }
        list.add(str);
    }

    public Map c() {
        return this.d;
    }

    public Map d() {
        return this.e;
    }

    public Map e() {
        return this.c;
    }

    public dg f() {
        return this.f;
    }
}
