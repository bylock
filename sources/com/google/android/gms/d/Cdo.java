package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.q;
import com.google.android.gms.internal.y;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: com.google.android.gms.d.do  reason: invalid class name */
class Cdo {
    private static final cc a = new cc(er.e(), true);
    private final di b;
    private final ad c;
    private final Map d;
    private final Map e;
    private final Map f;
    private final fd g;
    private final fd h;
    private final Set i;
    private final i j;
    private final Map k;
    private volatile String l;
    private int m;

    public Cdo(Context context, di diVar, i iVar, fy fyVar, fy fyVar2, ad adVar) {
        if (diVar == null) {
            throw new NullPointerException("resource cannot be null");
        }
        this.b = diVar;
        this.i = new HashSet(diVar.b());
        this.j = iVar;
        this.c = adVar;
        this.g = new fe().a(1048576, new dp(this));
        this.h = new fe().a(1048576, new dq(this));
        this.d = new HashMap();
        b(new ez(context));
        b(new fx(fyVar2));
        b(new gh(iVar));
        b(new es(context, iVar));
        this.e = new HashMap();
        c(new fv());
        c(new aa());
        c(new ab());
        c(new ai());
        c(new aj());
        c(new bf());
        c(new bg());
        c(new cp());
        c(new ek());
        this.f = new HashMap();
        a(new ba(context));
        a(new ce(context));
        a(new ev(context));
        a(new ew(context));
        a(new ex(context));
        a(new ey(context));
        a(new fh());
        a(new fu(this.b.c()));
        a(new fx(fyVar));
        a(new ga(iVar));
        a(new gk(context));
        a(new x());
        a(new z());
        a(new ae(this));
        a(new ak());
        a(new al());
        a(new av(context));
        a(new ax());
        a(new be());
        a(new bn(context));
        a(new cd());
        a(new ci());
        a(new cm());
        a(new co());
        a(new cq(context));
        a(new dw());
        a(new dx());
        a(new em());
        this.k = new HashMap();
        for (dk dkVar : this.i) {
            if (adVar.a()) {
                a(dkVar.f(), dkVar.g(), "add macro");
                a(dkVar.k(), dkVar.h(), "remove macro");
                a(dkVar.d(), dkVar.i(), "add tag");
                a(dkVar.e(), dkVar.j(), "remove tag");
            }
            for (int i2 = 0; i2 < dkVar.f().size(); i2++) {
                dg dgVar = (dg) dkVar.f().get(i2);
                String str = "Unknown";
                if (adVar.a() && i2 < dkVar.g().size()) {
                    str = (String) dkVar.g().get(i2);
                }
                dv a2 = a(this.k, a(dgVar));
                a2.a(dkVar);
                a2.a(dkVar, dgVar);
                a2.a(dkVar, str);
            }
            for (int i3 = 0; i3 < dkVar.k().size(); i3++) {
                dg dgVar2 = (dg) dkVar.k().get(i3);
                String str2 = "Unknown";
                if (adVar.a() && i3 < dkVar.h().size()) {
                    str2 = (String) dkVar.h().get(i3);
                }
                dv a3 = a(this.k, a(dgVar2));
                a3.a(dkVar);
                a3.b(dkVar, dgVar2);
                a3.b(dkVar, str2);
            }
        }
        for (Map.Entry entry : this.b.d().entrySet()) {
            for (dg dgVar3 : (List) entry.getValue()) {
                if (!er.d((y) dgVar3.b().get(f.NOT_DEFAULT_MACRO.toString())).booleanValue()) {
                    a(this.k, (String) entry.getKey()).a(dgVar3);
                }
            }
        }
    }

    private cc a(y yVar, Set set, et etVar) {
        if (!yVar.l) {
            return new cc(yVar, true);
        }
        switch (yVar.a) {
            case 2:
                y a2 = de.a(yVar);
                a2.c = new y[yVar.c.length];
                for (int i2 = 0; i2 < yVar.c.length; i2++) {
                    cc a3 = a(yVar.c[i2], set, etVar.a(i2));
                    if (a3 == a) {
                        return a;
                    }
                    a2.c[i2] = (y) a3.a();
                }
                return new cc(a2, false);
            case 3:
                y a4 = de.a(yVar);
                if (yVar.d.length != yVar.e.length) {
                    bk.a("Invalid serving value: " + yVar.toString());
                    return a;
                }
                a4.d = new y[yVar.d.length];
                a4.e = new y[yVar.d.length];
                for (int i3 = 0; i3 < yVar.d.length; i3++) {
                    cc a5 = a(yVar.d[i3], set, etVar.b(i3));
                    cc a6 = a(yVar.e[i3], set, etVar.c(i3));
                    if (a5 == a || a6 == a) {
                        return a;
                    }
                    a4.d[i3] = (y) a5.a();
                    a4.e[i3] = (y) a6.a();
                }
                return new cc(a4, false);
            case 4:
                if (set.contains(yVar.f)) {
                    bk.a("Macro cycle detected.  Current macro reference: " + yVar.f + "." + "  Previous macro references: " + set.toString() + ".");
                    return a;
                }
                set.add(yVar.f);
                cc a7 = eu.a(a(yVar.f, set, etVar.a()), yVar.k);
                set.remove(yVar.f);
                return a7;
            case 5:
            case 6:
            default:
                bk.a("Unknown type: " + yVar.a);
                return a;
            case 7:
                y a8 = de.a(yVar);
                a8.j = new y[yVar.j.length];
                for (int i4 = 0; i4 < yVar.j.length; i4++) {
                    cc a9 = a(yVar.j[i4], set, etVar.d(i4));
                    if (a9 == a) {
                        return a;
                    }
                    a8.j[i4] = (y) a9.a();
                }
                return new cc(a8, false);
        }
    }

    private cc a(String str, Set set, bm bmVar) {
        dg dgVar;
        this.m++;
        du duVar = (du) this.h.a(str);
        if (duVar == null || this.c.a()) {
            dv dvVar = (dv) this.k.get(str);
            if (dvVar == null) {
                bk.a(b() + "Invalid macro: " + str);
                this.m--;
                return a;
            }
            cc a2 = a(str, dvVar.a(), dvVar.b(), dvVar.c(), dvVar.e(), dvVar.d(), set, bmVar.b());
            if (((Set) a2.a()).isEmpty()) {
                dgVar = dvVar.f();
            } else {
                if (((Set) a2.a()).size() > 1) {
                    bk.b(b() + "Multiple macros active for macroName " + str);
                }
                dgVar = (dg) ((Set) a2.a()).iterator().next();
            }
            if (dgVar == null) {
                this.m--;
                return a;
            }
            cc a3 = a(this.f, dgVar, set, bmVar.a());
            cc ccVar = a3 == a ? a : new cc(a3.a(), a2.b() && a3.b());
            y c2 = dgVar.c();
            if (ccVar.b()) {
                this.h.a(str, new du(ccVar, c2));
            }
            a(c2, set);
            this.m--;
            return ccVar;
        }
        a(duVar.b(), set);
        this.m--;
        return duVar.a();
    }

    private cc a(Map map, dg dgVar, Set set, cr crVar) {
        boolean z;
        boolean z2 = true;
        y yVar = (y) dgVar.b().get(f.FUNCTION.toString());
        if (yVar == null) {
            bk.a("No function id in properties");
            return a;
        }
        String str = yVar.g;
        ag agVar = (ag) map.get(str);
        if (agVar == null) {
            bk.a(str + " has no backing implementation.");
            return a;
        }
        cc ccVar = (cc) this.g.a(dgVar);
        if (!(ccVar == null || this.c.a())) {
            return ccVar;
        }
        HashMap hashMap = new HashMap();
        boolean z3 = true;
        for (Map.Entry entry : dgVar.b().entrySet()) {
            cc a2 = a((y) entry.getValue(), set, crVar.a((String) entry.getKey()).a((y) entry.getValue()));
            if (a2 == a) {
                return a;
            }
            if (a2.b()) {
                dgVar.a((String) entry.getKey(), (y) a2.a());
                z = z3;
            } else {
                z = false;
            }
            hashMap.put(entry.getKey(), a2.a());
            z3 = z;
        }
        if (!agVar.a(hashMap.keySet())) {
            bk.a("Incorrect keys for function " + str + " required " + agVar.c() + " had " + hashMap.keySet());
            return a;
        }
        if (!z3 || !agVar.a()) {
            z2 = false;
        }
        cc ccVar2 = new cc(agVar.a(hashMap), z2);
        if (z2) {
            this.g.a(dgVar, ccVar2);
        }
        crVar.a((y) ccVar2.a());
        return ccVar2;
    }

    private cc a(Set set, Set set2, dt dtVar, dn dnVar) {
        Set hashSet = new HashSet();
        Set hashSet2 = new HashSet();
        Iterator it = set.iterator();
        boolean z = true;
        while (it.hasNext()) {
            dk dkVar = (dk) it.next();
            cu a2 = dnVar.a();
            cc a3 = a(dkVar, set2, a2);
            if (((Boolean) a3.a()).booleanValue()) {
                dtVar.a(dkVar, hashSet, hashSet2, a2);
            }
            z = z && a3.b();
        }
        hashSet.removeAll(hashSet2);
        dnVar.a(hashSet);
        return new cc(hashSet, z);
    }

    private static dv a(Map map, String str) {
        dv dvVar = (dv) map.get(str);
        if (dvVar != null) {
            return dvVar;
        }
        dv dvVar2 = new dv();
        map.put(str, dvVar2);
        return dvVar2;
    }

    private static String a(dg dgVar) {
        return er.a((y) dgVar.b().get(f.INSTANCE_NAME.toString()));
    }

    private void a(y yVar, Set set) {
        cc a2;
        if (yVar != null && (a2 = a(yVar, set, new ca())) != a) {
            Object e2 = er.e((y) a2.a());
            if (e2 instanceof Map) {
                this.j.a((Map) e2);
            } else if (e2 instanceof List) {
                for (Object obj : (List) e2) {
                    if (obj instanceof Map) {
                        this.j.a((Map) obj);
                    } else {
                        bk.b("pushAfterEvaluate: value not a Map");
                    }
                }
            } else {
                bk.b("pushAfterEvaluate: value not a Map or List");
            }
        }
    }

    private static void a(List list, List list2, String str) {
        if (list.size() != list2.size()) {
            bk.c("Invalid resource: imbalance of rule names of functions for " + str + " operation. Using default rule name instead");
        }
    }

    private static void a(Map map, ag agVar) {
        if (map.containsKey(agVar.b())) {
            throw new IllegalArgumentException("Duplicate function type name: " + agVar.b());
        }
        map.put(agVar.b(), agVar);
    }

    private String b() {
        if (this.m <= 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.toString(this.m));
        for (int i2 = 2; i2 < this.m; i2++) {
            sb.append(' ');
        }
        sb.append(": ");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public cc a(dg dgVar, Set set, cr crVar) {
        cc a2 = a(this.e, dgVar, set, crVar);
        Boolean d2 = er.d((y) a2.a());
        crVar.a(er.e(d2));
        return new cc(d2, a2.b());
    }

    /* access modifiers changed from: package-private */
    public cc a(dk dkVar, Set set, cu cuVar) {
        boolean z = true;
        for (dg dgVar : dkVar.c()) {
            cc a2 = a(dgVar, set, cuVar.a());
            if (((Boolean) a2.a()).booleanValue()) {
                cuVar.a(er.e((Object) false));
                return new cc(false, a2.b());
            }
            z = z && a2.b();
        }
        for (dg dgVar2 : dkVar.b()) {
            cc a3 = a(dgVar2, set, cuVar.b());
            if (!((Boolean) a3.a()).booleanValue()) {
                cuVar.a(er.e((Object) false));
                return new cc(false, a3.b());
            }
            z = z && a3.b();
        }
        cuVar.a(er.e((Object) true));
        return new cc(true, z);
    }

    /* access modifiers changed from: package-private */
    public cc a(String str, Set set, Map map, Map map2, Map map3, Map map4, Set set2, dn dnVar) {
        return a(set, set2, new dr(this, map, map2, map3, map4), dnVar);
    }

    /* access modifiers changed from: package-private */
    public cc a(Set set, dn dnVar) {
        return a(set, new HashSet(), new ds(this), dnVar);
    }

    /* access modifiers changed from: package-private */
    public synchronized String a() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void a(ag agVar) {
        a(this.f, agVar);
    }

    public synchronized void a(String str) {
        c(str);
        ac b2 = this.c.b(str);
        fz b3 = b2.b();
        for (dg dgVar : (Set) a(this.i, b3.b()).a()) {
            a(this.d, dgVar, new HashSet(), b3.a());
        }
        b2.c();
        c((String) null);
    }

    public synchronized void a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            q qVar = (q) it.next();
            if (qVar.a == null || !qVar.a.startsWith("gaExperiment:")) {
                bk.e("Ignored supplemental: " + qVar);
            } else {
                af.a(this.j, qVar);
            }
        }
    }

    public cc b(String str) {
        this.m = 0;
        ac a2 = this.c.a(str);
        cc a3 = a(str, new HashSet(), a2.a());
        a2.c();
        return a3;
    }

    /* access modifiers changed from: package-private */
    public void b(ag agVar) {
        a(this.d, agVar);
    }

    /* access modifiers changed from: package-private */
    public void c(ag agVar) {
        a(this.e, agVar);
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(String str) {
        this.l = str;
    }
}
