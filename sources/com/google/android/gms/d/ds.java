package com.google.android.gms.d;

import java.util.Set;

/* access modifiers changed from: package-private */
public class ds implements dt {
    final /* synthetic */ Cdo a;

    ds(Cdo doVar) {
        this.a = doVar;
    }

    @Override // com.google.android.gms.d.dt
    public void a(dk dkVar, Set set, Set set2, cu cuVar) {
        set.addAll(dkVar.d());
        set2.addAll(dkVar.e());
        cuVar.e().a(dkVar.d(), dkVar.i());
        cuVar.f().a(dkVar.e(), dkVar.j());
    }
}
