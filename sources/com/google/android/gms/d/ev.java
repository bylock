package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ev extends ag {
    private static final String a = a.ADWORDS_CLICK_REFERRER.toString();
    private static final String b = f.COMPONENT.toString();
    private static final String c = f.CONVERSION_ID.toString();
    private final Context d;

    public ev(Context context) {
        super(a, c);
        this.d = context;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        y yVar = (y) map.get(c);
        if (yVar == null) {
            return er.e();
        }
        String a2 = er.a(yVar);
        y yVar2 = (y) map.get(b);
        String a3 = aw.a(this.d, a2, yVar2 != null ? er.a(yVar2) : null);
        return a3 != null ? er.e(a3) : er.e();
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
