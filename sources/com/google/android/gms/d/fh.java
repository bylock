package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class fh extends ag {
    private static final String a = a.CONSTANT.toString();
    private static final String b = f.VALUE.toString();

    public fh() {
        super(a, b);
    }

    public static String d() {
        return a;
    }

    public static String e() {
        return b;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        return (y) map.get(b);
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
