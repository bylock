package com.google.android.gms.d;

import com.google.android.gms.internal.bk;

class bh implements cn {
    private final long a;
    private final long b;
    private final int c;
    private double d;
    private long e;
    private final Object f = new Object();
    private final String g;
    private final bk h;

    public bh(int i, long j, long j2, String str, bk bkVar) {
        this.c = i;
        this.d = (double) this.c;
        this.a = j;
        this.b = j2;
        this.g = str;
        this.h = bkVar;
    }

    @Override // com.google.android.gms.d.cn
    public boolean a() {
        boolean z = false;
        synchronized (this.f) {
            long a2 = this.h.a();
            if (a2 - this.e < this.b) {
                bk.b("Excessive " + this.g + " detected; call ignored.");
            } else {
                if (this.d < ((double) this.c)) {
                    double d2 = ((double) (a2 - this.e)) / ((double) this.a);
                    if (d2 > 0.0d) {
                        this.d = Math.min((double) this.c, d2 + this.d);
                    }
                }
                this.e = a2;
                if (this.d >= 1.0d) {
                    this.d -= 1.0d;
                    z = true;
                } else {
                    bk.b("Excessive " + this.g + " detected; call ignored.");
                }
            }
        }
        return z;
    }
}
