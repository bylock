package com.google.android.gms.d;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class cq extends ag {
    private static final String a = a.RESOLUTION.toString();
    private final Context b;

    public cq(Context context) {
        super(a, new String[0]);
        this.b = context;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.b.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.widthPixels;
        return er.e(i + "x" + displayMetrics.heightPixels);
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
