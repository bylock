package com.google.android.gms.d;

import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public abstract class ep extends ag {
    public ep(String str, String... strArr) {
        super(str, strArr);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        b(map);
        return er.e();
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return false;
    }

    public abstract void b(Map map);
}
