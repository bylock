package com.google.android.gms.d;

import com.google.android.gms.b.d;

class eo implements d {
    eo() {
    }

    private static int b(int i) {
        switch (i) {
            case 2:
                return 0;
            case 3:
            case 4:
                return 1;
            case 5:
                return 2;
            case 6:
            default:
                return 3;
        }
    }

    @Override // com.google.android.gms.b.d
    public int a() {
        return b(bk.a());
    }

    @Override // com.google.android.gms.b.d
    public void a(int i) {
        bk.b("GA uses GTM logger. Please use TagManager.setLogLevel(int) instead.");
    }

    @Override // com.google.android.gms.b.d
    public void a(String str) {
        bk.e(str);
    }

    @Override // com.google.android.gms.b.d
    public void b(String str) {
        bk.c(str);
    }

    @Override // com.google.android.gms.b.d
    public void c(String str) {
        bk.b(str);
    }

    @Override // com.google.android.gms.b.d
    public void d(String str) {
        bk.a(str);
    }
}
