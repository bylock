package com.google.android.gms.d;

import android.content.Context;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class gj implements an {
    private static gj a;
    private static final Object b = new Object();
    private String c;
    private String d;
    private cn e;
    private ao f;

    private gj(Context context) {
        this(ap.a(context), new dy());
    }

    gj(ao aoVar, cn cnVar) {
        this.f = aoVar;
        this.e = cnVar;
    }

    public static an a(Context context) {
        gj gjVar;
        synchronized (b) {
            if (a == null) {
                a = new gj(context);
            }
            gjVar = a;
        }
        return gjVar;
    }

    @Override // com.google.android.gms.d.an
    public boolean a(String str) {
        if (!this.e.a()) {
            bk.b("Too many urls sent too quickly with the TagManagerSender, rate limiting invoked.");
            return false;
        }
        if (!(this.c == null || this.d == null)) {
            try {
                str = this.c + "?" + this.d + "=" + URLEncoder.encode(str, "UTF-8");
                bk.e("Sending wrapped url hit: " + str);
            } catch (UnsupportedEncodingException e2) {
                bk.b("Error wrapping URL for testing.", e2);
                return false;
            }
        }
        this.f.a(str);
        return true;
    }
}
