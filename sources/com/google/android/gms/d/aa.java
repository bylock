package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import java.util.Map;

/* access modifiers changed from: package-private */
public class aa extends el {
    private static final String a = a.ENDS_WITH.toString();

    public aa() {
        super(a);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.d.el
    public boolean a(String str, String str2, Map map) {
        return str.endsWith(str2);
    }
}
