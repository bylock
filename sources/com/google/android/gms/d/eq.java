package com.google.android.gms.d;

/* access modifiers changed from: package-private */
public class eq extends Number implements Comparable {
    private double a;
    private long b;
    private boolean c = false;

    private eq(double d) {
        this.a = d;
    }

    private eq(long j) {
        this.b = j;
    }

    public static eq a(long j) {
        return new eq(j);
    }

    public static eq a(Double d) {
        return new eq(d.doubleValue());
    }

    public static eq a(String str) {
        try {
            return new eq(Long.parseLong(str));
        } catch (NumberFormatException e) {
            try {
                return new eq(Double.parseDouble(str));
            } catch (NumberFormatException e2) {
                throw new NumberFormatException(str + " is not a valid TypedNumber");
            }
        }
    }

    /* renamed from: a */
    public int compareTo(eq eqVar) {
        return (!b() || !eqVar.b()) ? Double.compare(doubleValue(), eqVar.doubleValue()) : new Long(this.b).compareTo(Long.valueOf(eqVar.b));
    }

    public boolean a() {
        return !b();
    }

    public boolean b() {
        return this.c;
    }

    public byte byteValue() {
        return (byte) ((int) longValue());
    }

    public long c() {
        return b() ? this.b : (long) this.a;
    }

    public int d() {
        return (int) longValue();
    }

    public double doubleValue() {
        return b() ? (double) this.b : this.a;
    }

    public short e() {
        return (short) ((int) longValue());
    }

    public boolean equals(Object obj) {
        return (obj instanceof eq) && compareTo((eq) obj) == 0;
    }

    public float floatValue() {
        return (float) doubleValue();
    }

    public int hashCode() {
        return new Long(longValue()).hashCode();
    }

    public int intValue() {
        return d();
    }

    public long longValue() {
        return c();
    }

    public short shortValue() {
        return e();
    }

    public String toString() {
        return b() ? Long.toString(this.b) : Double.toString(this.a);
    }
}
