package com.google.android.gms.d;

import android.content.Context;
import android.os.Handler;

class ea extends dz {
    private static final Object a = new Object();
    private static ea n;
    private Context b;
    private ar c;
    private volatile ao d;
    private int e = 1800000;
    private boolean f = true;
    private boolean g = false;
    private boolean h = true;
    private boolean i = true;
    private as j = new eb(this);
    private Handler k;
    private bq l;
    private boolean m = false;

    private ea() {
    }

    public static ea b() {
        if (n == null) {
            n = new ea();
        }
        return n;
    }

    private void f() {
        this.l = new bq(this);
        this.l.a(this.b);
    }

    private void g() {
        this.k = new Handler(this.b.getMainLooper(), new ec(this));
        if (this.e > 0) {
            this.k.sendMessageDelayed(this.k.obtainMessage(1, a), (long) this.e);
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.d.dz
    public synchronized void a() {
        if (!this.m && this.h && this.e > 0) {
            this.k.removeMessages(1, a);
            this.k.sendMessage(this.k.obtainMessage(1, a));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Context context, ao aoVar) {
        if (this.b == null) {
            this.b = context.getApplicationContext();
            if (this.d == null) {
                this.d = aoVar;
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.d.dz
    public synchronized void a(boolean z) {
        a(this.m, z);
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(boolean z, boolean z2) {
        if (!(this.m == z && this.h == z2)) {
            if ((z || !z2) && this.e > 0) {
                this.k.removeMessages(1, a);
            }
            if (!z && z2 && this.e > 0) {
                this.k.sendMessageDelayed(this.k.obtainMessage(1, a), (long) this.e);
            }
            bk.e("PowerSaveMode " + ((z || !z2) ? "initiated." : "terminated."));
            this.m = z;
            this.h = z2;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized ar c() {
        if (this.c == null) {
            if (this.b == null) {
                throw new IllegalStateException("Cant get a store unless we have a context");
            }
            this.c = new cf(this.j, this.b);
        }
        if (this.k == null) {
            g();
        }
        this.g = true;
        if (this.f) {
            d();
            this.f = false;
        }
        if (this.l == null && this.i) {
            f();
        }
        return this.c;
    }

    public synchronized void d() {
        if (!this.g) {
            bk.e("Dispatch call queued. Dispatch will run once initialization is complete.");
            this.f = true;
        } else {
            this.d.a(new ed(this));
        }
    }
}
