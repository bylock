package com.google.android.gms.d;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.i;
import com.google.android.gms.internal.bk;
import com.google.android.gms.internal.bl;
import com.google.android.gms.internal.df;
import com.google.android.gms.internal.n;
import com.google.android.gms.internal.r;

/* access modifiers changed from: package-private */
public class fl extends i {
    private final bk a;
    private final fr b;
    private final Looper c;
    private final cn d;
    private final int e;
    private final Context f;
    private final p g;
    private final String h;
    private ft i;
    private volatile fi j;
    private volatile boolean k;
    private r l;
    private long m;
    private String n;
    private fs o;
    private fo p;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fl(Context context, p pVar, Looper looper, String str, int i2, ft ftVar, fs fsVar, bk bkVar, cn cnVar) {
        super(looper == null ? Looper.getMainLooper() : looper);
        this.f = context;
        this.g = pVar;
        this.c = looper == null ? Looper.getMainLooper() : looper;
        this.h = str;
        this.e = i2;
        this.i = ftVar;
        this.o = fsVar;
        this.b = new fr(this, null);
        this.l = new r();
        this.a = bkVar;
        this.d = cnVar;
        if (e()) {
            a(ck.a().c());
        }
    }

    public fl(Context context, p pVar, Looper looper, String str, int i2, fw fwVar) {
        this(context, pVar, looper, str, i2, new db(context, str), new cw(context, str, fwVar), bl.b(), new bh(30, 900000, 5000, "refreshing", bl.b()));
    }

    /* access modifiers changed from: private */
    public synchronized void a(long j2) {
        if (this.o == null) {
            bk.b("Refresh requested, but no network load scheduler.");
        } else {
            this.o.a(j2, this.l.c);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(r rVar) {
        if (this.i != null) {
            df dfVar = new df();
            dfVar.a = this.m;
            dfVar.b = new n();
            dfVar.c = rVar;
            this.i.a(dfVar);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r8.k != false) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.google.android.gms.internal.r r9, long r10, boolean r12) {
        /*
        // Method dump skipped, instructions count: 115
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.d.fl.a(com.google.android.gms.internal.r, long, boolean):void");
    }

    private void a(boolean z) {
        this.i.a(new fp(this, null));
        this.o.a(new fq(this, null));
        di a2 = this.i.a(this.e);
        if (a2 != null) {
            this.j = new fi(this.g, this.c, new a(this.f, this.g.a(), this.h, 0, a2), this.b);
        }
        this.p = new fn(this, z);
        if (e()) {
            this.o.a(0, "");
        } else {
            this.i.b();
        }
    }

    /* access modifiers changed from: private */
    @Override // com.google.android.gms.common.api.i
    public boolean e() {
        ck a2 = ck.a();
        return (a2.b() == cl.CONTAINER || a2.b() == cl.CONTAINER_DEBUG) && this.h.equals(a2.d());
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(String str) {
        this.n = str;
        if (this.o != null) {
            this.o.a(str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public g a(Status status) {
        if (this.j != null) {
            return this.j;
        }
        if (status == Status.d) {
            bk.a("timer expired: setting result to failure");
        }
        return new fi(status);
    }

    public void d() {
        a(true);
    }
}
