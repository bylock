package com.google.android.gms.d;

import android.content.Context;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* access modifiers changed from: package-private */
public class cw implements fs {
    private final String a;
    private final Context b;
    private final ScheduledExecutorService c;
    private final cz d;
    private ScheduledFuture e;
    private boolean f;
    private fw g;
    private String h;
    private bi i;

    public cw(Context context, String str, fw fwVar) {
        this(context, str, fwVar, null, null);
    }

    cw(Context context, String str, fw fwVar, da daVar, cz czVar) {
        this.g = fwVar;
        this.b = context;
        this.a = str;
        this.c = (daVar == null ? new cx(this) : daVar).a();
        if (czVar == null) {
            this.d = new cy(this);
        } else {
            this.d = czVar;
        }
    }

    private cv b(String str) {
        cv a2 = this.d.a(this.g);
        a2.a(this.i);
        a2.a(this.h);
        a2.b(str);
        return a2;
    }

    private synchronized void b() {
        if (this.f) {
            throw new IllegalStateException("called method after closed");
        }
    }

    @Override // com.google.android.gms.common.api.d
    public synchronized void a() {
        b();
        if (this.e != null) {
            this.e.cancel(false);
        }
        this.c.shutdown();
        this.f = true;
    }

    @Override // com.google.android.gms.d.fs
    public synchronized void a(long j, String str) {
        bk.e("loadAfterDelay: containerId=" + this.a + " delay=" + j);
        b();
        if (this.i == null) {
            throw new IllegalStateException("callback must be set before loadAfterDelay() is called.");
        }
        if (this.e != null) {
            this.e.cancel(false);
        }
        this.e = this.c.schedule(b(str), j, TimeUnit.MILLISECONDS);
    }

    @Override // com.google.android.gms.d.fs
    public synchronized void a(bi biVar) {
        b();
        this.i = biVar;
    }

    @Override // com.google.android.gms.d.fs
    public synchronized void a(String str) {
        b();
        this.h = str;
    }
}
