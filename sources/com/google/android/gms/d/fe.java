package com.google.android.gms.d;

import android.os.Build;

/* access modifiers changed from: package-private */
public class fe {
    final fg a = new ff(this);

    /* access modifiers changed from: package-private */
    public int a() {
        return Build.VERSION.SDK_INT;
    }

    public fd a(int i, fg fgVar) {
        if (i > 0) {
            return a() < 12 ? new eg(i, fgVar) : new bc(i, fgVar);
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }
}
