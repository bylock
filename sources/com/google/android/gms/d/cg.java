package com.google.android.gms.d;

class cg implements ej {
    final /* synthetic */ cf a;

    cg(cf cfVar) {
        this.a = cfVar;
    }

    @Override // com.google.android.gms.d.ej
    public void a(am amVar) {
        cf.a(this.a, amVar.a());
    }

    @Override // com.google.android.gms.d.ej
    public void b(am amVar) {
        cf.a(this.a, amVar.a());
        bk.e("Permanent failure dispatching hitId: " + amVar.a());
    }

    @Override // com.google.android.gms.d.ej
    public void c(am amVar) {
        long b = amVar.b();
        if (b == 0) {
            cf.a(this.a, amVar.a(), cf.a(this.a).a());
        } else if (b + 14400000 < cf.a(this.a).a()) {
            cf.a(this.a, amVar.a());
            bk.e("Giving up on failed hitId: " + amVar.a());
        }
    }
}
