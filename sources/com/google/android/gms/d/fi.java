package com.google.android.gms.d;

import android.os.Looper;
import com.google.android.gms.common.api.Status;

/* access modifiers changed from: package-private */
public class fi implements g {
    private final Looper a;
    private a b;
    private a c;
    private Status d;
    private fk e;
    private fj f;
    private boolean g;
    private p h;

    public fi(Status status) {
        this.d = status;
        this.a = null;
    }

    public fi(p pVar, Looper looper, a aVar, fj fjVar) {
        this.h = pVar;
        this.a = looper == null ? Looper.getMainLooper() : looper;
        this.b = aVar;
        this.f = fjVar;
        this.d = Status.a;
        pVar.a(this);
    }

    private void e() {
        if (this.e != null) {
            this.e.a(this.c.c());
        }
    }

    @Override // com.google.android.gms.common.api.d
    public synchronized void a() {
        if (this.g) {
            bk.a("Releasing a released ContainerHolder.");
        } else {
            this.g = true;
            this.h.b(this);
            this.b.d();
            this.b = null;
            this.c = null;
            this.f = null;
            this.e = null;
        }
    }

    public synchronized void a(a aVar) {
        if (!this.g) {
            if (aVar == null) {
                bk.a("Unexpected null container.");
            } else {
                this.c = aVar;
                e();
            }
        }
    }

    @Override // com.google.android.gms.d.g
    public synchronized void a(h hVar) {
        if (this.g) {
            bk.a("ContainerHolder is released.");
        } else if (hVar == null) {
            this.e = null;
        } else {
            this.e = new fk(this, hVar, this.a);
            if (this.c != null) {
                e();
            }
        }
    }

    public synchronized void a(String str) {
        if (!this.g) {
            this.b.d(str);
        }
    }

    @Override // com.google.android.gms.common.api.e
    public Status b() {
        return this.d;
    }

    @Override // com.google.android.gms.d.g
    public synchronized a c() {
        a aVar = null;
        synchronized (this) {
            if (this.g) {
                bk.a("ContainerHolder is released.");
            } else {
                if (this.c != null) {
                    this.b = this.c;
                    this.c = null;
                }
                aVar = this.b;
            }
        }
        return aVar;
    }

    @Override // com.google.android.gms.d.g
    public synchronized void d() {
        if (this.g) {
            bk.a("Refreshing a released ContainerHolder.");
        } else {
            this.f.a();
        }
    }
}
