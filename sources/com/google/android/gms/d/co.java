package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/* access modifiers changed from: package-private */
public class co extends ag {
    private static final String a = a.REGEX_GROUP.toString();
    private static final String b = f.ARG0.toString();
    private static final String c = f.ARG1.toString();
    private static final String d = f.IGNORE_CASE.toString();
    private static final String e = f.GROUP.toString();

    public co() {
        super(a, b, c);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        int i;
        y yVar = (y) map.get(b);
        y yVar2 = (y) map.get(c);
        if (yVar == null || yVar == er.e() || yVar2 == null || yVar2 == er.e()) {
            return er.e();
        }
        int i2 = 64;
        if (er.d((y) map.get(d)).booleanValue()) {
            i2 = 66;
        }
        y yVar3 = (y) map.get(e);
        if (yVar3 != null) {
            Long c2 = er.c(yVar3);
            if (c2 == er.b()) {
                return er.e();
            }
            i = c2.intValue();
            if (i < 0) {
                return er.e();
            }
        } else {
            i = 1;
        }
        try {
            String a2 = er.a(yVar);
            String str = null;
            Matcher matcher = Pattern.compile(er.a(yVar2), i2).matcher(a2);
            if (matcher.find() && matcher.groupCount() >= i) {
                str = matcher.group(i);
            }
            return str == null ? er.e() : er.e(str);
        } catch (PatternSyntaxException e2) {
            return er.e();
        }
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
