package com.google.android.gms.d;

import android.content.Context;
import android.net.Uri;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* access modifiers changed from: package-private */
public class ez extends ep {
    static final String a = ("gtm_" + b + "_unrepeatable");
    private static final String b = a.ARBITRARY_PIXEL.toString();
    private static final String c = f.URL.toString();
    private static final String d = f.ADDITIONAL_PARAMS.toString();
    private static final String e = f.UNREPEATABLE.toString();
    private static final Set f = new HashSet();
    private final fb g;
    private final Context h;

    public ez(Context context) {
        this(context, new fa(context));
    }

    ez(Context context, fb fbVar) {
        super(b, c);
        this.g = fbVar;
        this.h = context;
    }

    private synchronized boolean c(String str) {
        boolean z = true;
        synchronized (this) {
            if (!b(str)) {
                if (a(str)) {
                    f.add(str);
                } else {
                    z = false;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return this.h.getSharedPreferences(a, 0).contains(str);
    }

    @Override // com.google.android.gms.d.ep
    public void b(Map map) {
        String a2 = map.get(e) != null ? er.a((y) map.get(e)) : null;
        if (a2 == null || !c(a2)) {
            Uri.Builder buildUpon = Uri.parse(er.a((y) map.get(c))).buildUpon();
            y yVar = (y) map.get(d);
            if (yVar != null) {
                Object e2 = er.e(yVar);
                if (!(e2 instanceof List)) {
                    bk.a("ArbitraryPixel: additional params not a list: not sending partial hit: " + buildUpon.build().toString());
                    return;
                }
                for (Object obj : (List) e2) {
                    if (!(obj instanceof Map)) {
                        bk.a("ArbitraryPixel: additional params contains non-map: not sending partial hit: " + buildUpon.build().toString());
                        return;
                    }
                    for (Map.Entry entry : ((Map) obj).entrySet()) {
                        buildUpon.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
                    }
                }
            }
            String uri = buildUpon.build().toString();
            this.g.a().a(uri);
            bk.e("ArbitraryPixel: url = " + uri);
            if (a2 != null) {
                synchronized (ez.class) {
                    f.add(a2);
                    ee.a(this.h, a, a2, "true");
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str) {
        return f.contains(str);
    }
}
