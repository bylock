package com.google.android.gms.d;

import android.os.Build;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class dx extends ag {
    private static final String a = a.SDK_VERSION.toString();

    public dx() {
        super(a, new String[0]);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        return er.e(Integer.valueOf(Build.VERSION.SDK_INT));
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
