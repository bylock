package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ai extends cb {
    private static final String a = a.GREATER_EQUALS.toString();

    public ai() {
        super(a);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.d.cb
    public boolean a(eq eqVar, eq eqVar2, Map map) {
        return eqVar.compareTo(eqVar2) >= 0;
    }
}
