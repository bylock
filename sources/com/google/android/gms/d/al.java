package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/* access modifiers changed from: package-private */
public class al extends ag {
    private static final String a = a.HASH.toString();
    private static final String b = f.ARG0.toString();
    private static final String c = f.ALGORITHM.toString();
    private static final String d = f.INPUT_FORMAT.toString();

    public al() {
        super(a, b);
    }

    private byte[] a(String str, byte[] bArr) {
        MessageDigest instance = MessageDigest.getInstance(str);
        instance.update(bArr);
        return instance.digest();
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        byte[] a2;
        y yVar = (y) map.get(b);
        if (yVar == null || yVar == er.e()) {
            return er.e();
        }
        String a3 = er.a(yVar);
        y yVar2 = (y) map.get(c);
        String a4 = yVar2 == null ? "MD5" : er.a(yVar2);
        y yVar3 = (y) map.get(d);
        String a5 = yVar3 == null ? "text" : er.a(yVar3);
        if ("text".equals(a5)) {
            a2 = a3.getBytes();
        } else if ("base16".equals(a5)) {
            a2 = fc.a(a3);
        } else {
            bk.a("Hash: unknown input format: " + a5);
            return er.e();
        }
        try {
            return er.e(fc.a(a(a4, a2)));
        } catch (NoSuchAlgorithmException e) {
            bk.a("Hash: unknown algorithm: " + a4);
            return er.e();
        }
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
