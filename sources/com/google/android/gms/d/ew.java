package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ew extends ag {
    private static final String a = a.APP_ID.toString();
    private final Context b;

    public ew(Context context) {
        super(a, new String[0]);
        this.b = context;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        return er.e(this.b.getPackageName());
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
