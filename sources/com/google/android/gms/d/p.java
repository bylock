package com.google.android.gms.d;

import android.content.Context;
import com.google.android.gms.common.api.b;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class p {
    private static p f;
    private final s a;
    private final Context b;
    private final i c;
    private final ConcurrentMap d;
    private final fw e;

    p(Context context, s sVar, i iVar) {
        if (context == null) {
            throw new NullPointerException("context cannot be null");
        }
        this.b = context.getApplicationContext();
        this.a = sVar;
        this.d = new ConcurrentHashMap();
        this.c = iVar;
        this.c.a(new q(this));
        this.c.a(new eh(this.b));
        this.e = new fw();
    }

    public static p a(Context context) {
        p pVar;
        synchronized (p.class) {
            if (f == null) {
                if (context == null) {
                    bk.a("TagManager.getInstance requires non-null context.");
                    throw new NullPointerException();
                }
                f = new p(context, new r(), new i(new gb(context)));
            }
            pVar = f;
        }
        return pVar;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        for (fi fiVar : this.d.keySet()) {
            fiVar.a(str);
        }
    }

    public b a(String str, int i) {
        fl a2 = this.a.a(this.b, this, null, str, i, this.e);
        a2.d();
        return a2;
    }

    public i a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(fi fiVar) {
        this.d.put(fiVar, true);
    }

    /* access modifiers changed from: package-private */
    public boolean b(fi fiVar) {
        return this.d.remove(fiVar) != null;
    }
}
