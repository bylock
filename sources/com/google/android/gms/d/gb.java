package com.google.android.gms.d;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.internal.bk;
import com.google.android.gms.internal.bl;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* access modifiers changed from: package-private */
public class gb implements n {
    private static final String a = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' STRING NOT NULL, '%s' BLOB NOT NULL, '%s' INTEGER NOT NULL);", "datalayer", "ID", "key", "value", "expires");
    private final Executor b;
    private final Context c;
    private gf d;
    private bk e;
    private int f;

    public gb(Context context) {
        this(context, bl.b(), "google_tagmanager.db", 2000, Executors.newSingleThreadExecutor());
    }

    gb(Context context, bk bkVar, String str, int i, Executor executor) {
        this.c = context;
        this.e = bkVar;
        this.f = i;
        this.b = executor;
        this.d = new gf(this, this.c, str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001c A[SYNTHETIC, Splitter:B:13:0x001c] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0029 A[SYNTHETIC, Splitter:B:20:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0038 A[SYNTHETIC, Splitter:B:27:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object a(byte[] r6) {
        /*
            r5 = this;
            r0 = 0
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream
            r2.<init>(r6)
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x0018, ClassNotFoundException -> 0x0025, all -> 0x0032 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0018, ClassNotFoundException -> 0x0025, all -> 0x0032 }
            java.lang.Object r0 = r1.readObject()     // Catch:{ IOException -> 0x0045, ClassNotFoundException -> 0x0043, all -> 0x0041 }
            if (r1 == 0) goto L_0x0014
            r1.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0014:
            r2.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0017:
            return r0
        L_0x0018:
            r1 = move-exception
            r1 = r0
        L_0x001a:
            if (r1 == 0) goto L_0x001f
            r1.close()     // Catch:{ IOException -> 0x0023 }
        L_0x001f:
            r2.close()     // Catch:{ IOException -> 0x0023 }
            goto L_0x0017
        L_0x0023:
            r1 = move-exception
            goto L_0x0017
        L_0x0025:
            r1 = move-exception
            r1 = r0
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ IOException -> 0x0030 }
        L_0x002c:
            r2.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0017
        L_0x0030:
            r1 = move-exception
            goto L_0x0017
        L_0x0032:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x003f }
        L_0x003b:
            r2.close()     // Catch:{ IOException -> 0x003f }
        L_0x003e:
            throw r0
        L_0x003f:
            r1 = move-exception
            goto L_0x003e
        L_0x0041:
            r0 = move-exception
            goto L_0x0036
        L_0x0043:
            r3 = move-exception
            goto L_0x0027
        L_0x0045:
            r3 = move-exception
            goto L_0x001a
        L_0x0047:
            r1 = move-exception
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.d.gb.a(byte[]):java.lang.Object");
    }

    private List a(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            gg ggVar = (gg) it.next();
            arrayList.add(new l(ggVar.a, a(ggVar.b)));
        }
        return arrayList;
    }

    private void a(int i) {
        int d2 = (d() - this.f) + i;
        if (d2 > 0) {
            List b2 = b(d2);
            bk.c("DataLayer store full, deleting " + b2.size() + " entries to make room.");
            a((String[]) b2.toArray(new String[0]));
        }
    }

    private void a(long j) {
        SQLiteDatabase c2 = c("Error opening database for deleteOlderThan.");
        if (c2 != null) {
            try {
                bk.e("Deleted " + c2.delete("datalayer", "expires <= ?", new String[]{Long.toString(j)}) + " expired items");
            } catch (SQLiteException e2) {
                bk.b("Error deleting old entries.");
            }
        }
    }

    private void a(String[] strArr) {
        SQLiteDatabase c2;
        if (strArr != null && strArr.length != 0 && (c2 = c("Error opening database for deleteEntries.")) != null) {
            try {
                c2.delete("datalayer", String.format("%s in (%s)", "ID", TextUtils.join(",", Collections.nCopies(strArr.length, "?"))), strArr);
            } catch (SQLiteException e2) {
                bk.b("Error deleting entries " + Arrays.toString(strArr));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001f A[SYNTHETIC, Splitter:B:13:0x001f] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002e A[SYNTHETIC, Splitter:B:20:0x002e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(java.lang.Object r6) {
        /*
            r5 = this;
            r0 = 0
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x001b, all -> 0x0028 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x001b, all -> 0x0028 }
            r1.writeObject(r6)     // Catch:{ IOException -> 0x0039, all -> 0x0037 }
            byte[] r0 = r2.toByteArray()     // Catch:{ IOException -> 0x0039, all -> 0x0037 }
            if (r1 == 0) goto L_0x0017
            r1.close()     // Catch:{ IOException -> 0x003b }
        L_0x0017:
            r2.close()     // Catch:{ IOException -> 0x003b }
        L_0x001a:
            return r0
        L_0x001b:
            r1 = move-exception
            r1 = r0
        L_0x001d:
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ IOException -> 0x0026 }
        L_0x0022:
            r2.close()     // Catch:{ IOException -> 0x0026 }
            goto L_0x001a
        L_0x0026:
            r1 = move-exception
            goto L_0x001a
        L_0x0028:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x002c:
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0031:
            r2.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0034:
            throw r0
        L_0x0035:
            r1 = move-exception
            goto L_0x0034
        L_0x0037:
            r0 = move-exception
            goto L_0x002c
        L_0x0039:
            r3 = move-exception
            goto L_0x001d
        L_0x003b:
            r1 = move-exception
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.d.gb.a(java.lang.Object):byte[]");
    }

    /* access modifiers changed from: private */
    public List b() {
        try {
            a(this.e.a());
            return a(c());
        } finally {
            e();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List b(int r14) {
        /*
        // Method dump skipped, instructions count: 138
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.d.gb.b(int):java.util.List");
    }

    private List b(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            l lVar = (l) it.next();
            arrayList.add(new gg(lVar.a, a(lVar.b)));
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        SQLiteDatabase c2 = c("Error opening database for clearKeysWithPrefix.");
        if (c2 != null) {
            try {
                bk.e("Cleared " + c2.delete("datalayer", "key = ? OR key LIKE ?", new String[]{str, str + ".%"}) + " items");
            } catch (SQLiteException e2) {
                bk.b("Error deleting entries with key prefix: " + str + " (" + e2 + ").");
            } finally {
                e();
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(List list, long j) {
        try {
            long a2 = this.e.a();
            a(a2);
            a(list.size());
            c(list, a2 + j);
        } finally {
            e();
        }
    }

    private SQLiteDatabase c(String str) {
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e2) {
            bk.b(str);
            return null;
        }
    }

    private List c() {
        SQLiteDatabase c2 = c("Error opening database for loadSerialized.");
        ArrayList arrayList = new ArrayList();
        if (c2 == null) {
            return arrayList;
        }
        Cursor query = c2.query("datalayer", new String[]{"key", "value"}, null, null, null, null, "ID", null);
        while (query.moveToNext()) {
            try {
                arrayList.add(new gg(query.getString(0), query.getBlob(1)));
            } finally {
                query.close();
            }
        }
        return arrayList;
    }

    private void c(List list, long j) {
        SQLiteDatabase c2 = c("Error opening database for writeEntryToDatabase.");
        if (c2 != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                gg ggVar = (gg) it.next();
                ContentValues contentValues = new ContentValues();
                contentValues.put("expires", Long.valueOf(j));
                contentValues.put("key", ggVar.a);
                contentValues.put("value", ggVar.b);
                c2.insert("datalayer", null, contentValues);
            }
        }
    }

    private int d() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase c2 = c("Error opening database for getNumStoredEntries.");
        if (c2 != null) {
            try {
                Cursor rawQuery = c2.rawQuery("SELECT COUNT(*) from datalayer", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e2) {
                bk.b("Error getting numStoredEntries");
                if (0 != 0) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (0 != 0) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    private void e() {
        try {
            this.d.close();
        } catch (SQLiteException e2) {
        }
    }

    @Override // com.google.android.gms.d.n
    public void a(o oVar) {
        this.b.execute(new gd(this, oVar));
    }

    @Override // com.google.android.gms.d.n
    public void a(String str) {
        this.b.execute(new ge(this, str));
    }

    @Override // com.google.android.gms.d.n
    public void a(List list, long j) {
        this.b.execute(new gc(this, b(list), j));
    }
}
