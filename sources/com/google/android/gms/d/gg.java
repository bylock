package com.google.android.gms.d;

import java.util.Arrays;

class gg {
    final String a;
    final byte[] b;

    gg(String str, byte[] bArr) {
        this.a = str;
        this.b = bArr;
    }

    public String toString() {
        return "KeyAndSerialized: key = " + this.a + " serialized hash = " + Arrays.hashCode(this.b);
    }
}
