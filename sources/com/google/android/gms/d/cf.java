package com.google.android.gms.d;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.internal.bk;
import com.google.android.gms.internal.bl;
import java.util.Collections;
import java.util.List;
import org.apache.http.impl.client.DefaultHttpClient;

/* access modifiers changed from: package-private */
public class cf implements ar {
    private static final String a = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL,'%s' INTEGER NOT NULL);", "gtm_hits", "hit_id", "hit_time", "hit_url", "hit_first_send_time");
    private final ch b;
    private volatile y c;
    private final as d;
    private final Context e;
    private final String f;
    private long g;
    private bk h;
    private final int i;

    cf(as asVar, Context context) {
        this(asVar, context, "gtm_urls.db", 2000);
    }

    cf(as asVar, Context context, String str, int i2) {
        this.e = context.getApplicationContext();
        this.f = str;
        this.d = asVar;
        this.h = bl.b();
        this.b = new ch(this, this.e, this.f);
        this.c = new ei(new DefaultHttpClient(), this.e, new cg(this));
        this.g = 0;
        this.i = i2;
    }

    private SQLiteDatabase a(String str) {
        try {
            return this.b.getWritableDatabase();
        } catch (SQLiteException e2) {
            bk.b(str);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void a(long j) {
        a(new String[]{String.valueOf(j)});
    }

    /* access modifiers changed from: private */
    public void a(long j, long j2) {
        SQLiteDatabase a2 = a("Error opening database for getNumStoredHits.");
        if (a2 != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_first_send_time", Long.valueOf(j2));
            try {
                a2.update("gtm_hits", contentValues, "hit_id=?", new String[]{String.valueOf(j)});
            } catch (SQLiteException e2) {
                bk.b("Error setting HIT_FIRST_DISPATCH_TIME for hitId: " + j);
                a(j);
            }
        }
    }

    private void b(long j, String str) {
        SQLiteDatabase a2 = a("Error opening database for putHit");
        if (a2 != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_time", Long.valueOf(j));
            contentValues.put("hit_url", str);
            contentValues.put("hit_first_send_time", (Integer) 0);
            try {
                a2.insert("gtm_hits", null, contentValues);
                this.d.a(false);
            } catch (SQLiteException e2) {
                bk.b("Error storing hit");
            }
        }
    }

    private void f() {
        int c2 = (c() - this.i) + 1;
        if (c2 > 0) {
            List a2 = a(c2);
            bk.e("Store full, deleting " + a2.size() + " hits to make room.");
            a((String[]) a2.toArray(new String[0]));
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List a(int r14) {
        /*
        // Method dump skipped, instructions count: 138
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.d.cf.a(int):java.util.List");
    }

    @Override // com.google.android.gms.d.ar
    public void a() {
        bk.e("GTM Dispatch running...");
        if (this.c.a()) {
            List b2 = b(40);
            if (b2.isEmpty()) {
                bk.e("...nothing to dispatch");
                this.d.a(true);
                return;
            }
            this.c.a(b2);
            if (d() > 0) {
                ea.b().d();
            }
        }
    }

    @Override // com.google.android.gms.d.ar
    public void a(long j, String str) {
        b();
        f();
        b(j, str);
    }

    /* access modifiers changed from: package-private */
    public void a(String[] strArr) {
        SQLiteDatabase a2;
        boolean z = true;
        if (strArr != null && strArr.length != 0 && (a2 = a("Error opening database for deleteHits.")) != null) {
            try {
                a2.delete("gtm_hits", String.format("HIT_ID in (%s)", TextUtils.join(",", Collections.nCopies(strArr.length, "?"))), strArr);
                as asVar = this.d;
                if (c() != 0) {
                    z = false;
                }
                asVar.a(z);
            } catch (SQLiteException e2) {
                bk.b("Error deleting hits");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int b() {
        boolean z = true;
        long a2 = this.h.a();
        if (a2 <= this.g + 86400000) {
            return 0;
        }
        this.g = a2;
        SQLiteDatabase a3 = a("Error opening database for deleteStaleHits.");
        if (a3 == null) {
            return 0;
        }
        int delete = a3.delete("gtm_hits", "HIT_TIME < ?", new String[]{Long.toString(this.h.a() - 2592000000L)});
        as asVar = this.d;
        if (c() != 0) {
            z = false;
        }
        asVar.a(z);
        return delete;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e8, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00f0, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0169, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x016a, code lost:
        r11 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x016f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0170, code lost:
        r2 = r1;
        r3 = r12;
        r1 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        return r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x013e A[Catch:{ all -> 0x015d }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0169 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:79:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List b(int r16) {
        /*
        // Method dump skipped, instructions count: 382
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.d.cf.b(int):java.util.List");
    }

    /* access modifiers changed from: package-private */
    public int c() {
        Cursor cursor = null;
        int i2 = 0;
        SQLiteDatabase a2 = a("Error opening database for getNumStoredHits.");
        if (a2 != null) {
            try {
                Cursor rawQuery = a2.rawQuery("SELECT COUNT(*) from gtm_hits", null);
                if (rawQuery.moveToFirst()) {
                    i2 = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e2) {
                bk.b("Error getting numStoredHits");
                if (0 != 0) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (0 != 0) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int d() {
        /*
            r10 = this;
            r8 = 0
            r9 = 0
            java.lang.String r0 = "Error opening database for getNumStoredHits."
            android.database.sqlite.SQLiteDatabase r0 = r10.a(r0)
            if (r0 != 0) goto L_0x000b
        L_0x000a:
            return r8
        L_0x000b:
            java.lang.String r1 = "gtm_hits"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x002f, all -> 0x003d }
            r3 = 0
            java.lang.String r4 = "hit_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x002f, all -> 0x003d }
            r3 = 1
            java.lang.String r4 = "hit_first_send_time"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x002f, all -> 0x003d }
            java.lang.String r3 = "hit_first_send_time=0"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x002f, all -> 0x003d }
            int r0 = r1.getCount()     // Catch:{ SQLiteException -> 0x004b, all -> 0x0044 }
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            r8 = r0
            goto L_0x000a
        L_0x002f:
            r0 = move-exception
            r0 = r9
        L_0x0031:
            java.lang.String r1 = "Error getting num untried hits"
            com.google.android.gms.d.bk.b(r1)     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x004e
            r0.close()
            r0 = r8
            goto L_0x002d
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            if (r9 == 0) goto L_0x0043
            r9.close()
        L_0x0043:
            throw r0
        L_0x0044:
            r0 = move-exception
            r9 = r1
            goto L_0x003e
        L_0x0047:
            r1 = move-exception
            r9 = r0
            r0 = r1
            goto L_0x003e
        L_0x004b:
            r0 = move-exception
            r0 = r1
            goto L_0x0031
        L_0x004e:
            r0 = r8
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.d.cf.d():int");
    }
}
