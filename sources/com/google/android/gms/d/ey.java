package com.google.android.gms.d;

import android.content.Context;
import android.content.pm.PackageManager;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class ey extends ag {
    private static final String a = a.APP_VERSION.toString();
    private final Context b;

    public ey(Context context) {
        super(a, new String[0]);
        this.b = context;
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        try {
            return er.e(Integer.valueOf(this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 0).versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            bk.a("Package name " + this.b.getPackageName() + " not found. " + e.getMessage());
            return er.e();
        }
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
