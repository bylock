package com.google.android.gms.d;

import com.google.android.gms.internal.k;
import com.google.android.gms.internal.l;
import com.google.android.gms.internal.q;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class af {
    private static Map a(y yVar) {
        Object e = er.e(yVar);
        if (e instanceof Map) {
            return (Map) e;
        }
        bk.b("value: " + e + " is not a map value, ignored.");
        return null;
    }

    private static void a(i iVar, l lVar) {
        for (y yVar : lVar.b) {
            iVar.a(er.a(yVar));
        }
    }

    public static void a(i iVar, q qVar) {
        if (qVar.c == null) {
            bk.b("supplemental missing experimentSupplemental");
            return;
        }
        a(iVar, qVar.c);
        b(iVar, qVar.c);
        c(iVar, qVar.c);
    }

    private static void b(i iVar, l lVar) {
        for (y yVar : lVar.a) {
            Map a = a(yVar);
            if (a != null) {
                iVar.a(a);
            }
        }
    }

    private static void c(i iVar, l lVar) {
        k[] kVarArr = lVar.c;
        for (k kVar : kVarArr) {
            if (kVar.a == null) {
                bk.b("GaExperimentRandom: No key");
            } else {
                Object c = iVar.c(kVar.a);
                Long valueOf = !(c instanceof Number) ? null : Long.valueOf(((Number) c).longValue());
                long j = kVar.b;
                long j2 = kVar.c;
                if (!kVar.d || valueOf == null || valueOf.longValue() < j || valueOf.longValue() > j2) {
                    if (j <= j2) {
                        c = Long.valueOf(Math.round((Math.random() * ((double) (j2 - j))) + ((double) j)));
                    } else {
                        bk.b("GaExperimentRandom: random range invalid");
                    }
                }
                iVar.a(kVar.a);
                Map b = iVar.b(kVar.a, c);
                if (kVar.e > 0) {
                    if (!b.containsKey("gtm")) {
                        b.put("gtm", i.a("lifetime", Long.valueOf(kVar.e)));
                    } else {
                        Object obj = b.get("gtm");
                        if (obj instanceof Map) {
                            ((Map) obj).put("lifetime", Long.valueOf(kVar.e));
                        } else {
                            bk.b("GaExperimentRandom: gtm not a map");
                        }
                    }
                }
                iVar.a(b);
            }
        }
    }
}
