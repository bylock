package com.google.android.gms.d;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class fk extends Handler {
    final /* synthetic */ fi a;
    private final h b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fk(fi fiVar, h hVar, Looper looper) {
        super(looper);
        this.a = fiVar;
        this.b = hVar;
    }

    public void a(String str) {
        sendMessage(obtainMessage(1, str));
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.b.a(this.a, str);
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                b((String) message.obj);
                return;
            default:
                bk.a("Don't know how to handle this message.");
                return;
        }
    }
}
