package com.google.android.gms.d;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/* access modifiers changed from: package-private */
public class cp extends el {
    private static final String a = a.REGEX.toString();
    private static final String b = f.IGNORE_CASE.toString();

    public cp() {
        super(a);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.d.el
    public boolean a(String str, String str2, Map map) {
        try {
            return Pattern.compile(str2, er.d((y) map.get(b)).booleanValue() ? 66 : 64).matcher(str).find();
        } catch (PatternSyntaxException e) {
            return false;
        }
    }
}
