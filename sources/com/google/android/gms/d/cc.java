package com.google.android.gms.d;

/* access modifiers changed from: package-private */
public class cc {
    private final Object a;
    private final boolean b;

    cc(Object obj, boolean z) {
        this.a = obj;
        this.b = z;
    }

    public Object a() {
        return this.a;
    }

    public boolean b() {
        return this.b;
    }
}
