package com.google.android.gms.d;

import android.content.SharedPreferences;

/* access modifiers changed from: package-private */
public final class ef implements Runnable {
    final /* synthetic */ SharedPreferences.Editor a;

    ef(SharedPreferences.Editor editor) {
        this.a = editor;
    }

    public void run() {
        this.a.commit();
    }
}
