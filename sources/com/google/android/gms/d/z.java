package com.google.android.gms.d;

import android.util.Base64;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.f;
import com.google.android.gms.internal.y;
import java.util.Map;

/* access modifiers changed from: package-private */
public class z extends ag {
    private static final String a = a.ENCODE.toString();
    private static final String b = f.ARG0.toString();
    private static final String c = f.NO_PADDING.toString();
    private static final String d = f.INPUT_FORMAT.toString();
    private static final String e = f.OUTPUT_FORMAT.toString();

    public z() {
        super(a, b);
    }

    @Override // com.google.android.gms.d.ag
    public y a(Map map) {
        byte[] decode;
        String encodeToString;
        y yVar = (y) map.get(b);
        if (yVar == null || yVar == er.e()) {
            return er.e();
        }
        String a2 = er.a(yVar);
        y yVar2 = (y) map.get(d);
        String a3 = yVar2 == null ? "text" : er.a(yVar2);
        y yVar3 = (y) map.get(e);
        String a4 = yVar3 == null ? "base16" : er.a(yVar3);
        y yVar4 = (y) map.get(c);
        int i = (yVar4 == null || !er.d(yVar4).booleanValue()) ? 2 : 3;
        try {
            if ("text".equals(a3)) {
                decode = a2.getBytes();
            } else if ("base16".equals(a3)) {
                decode = fc.a(a2);
            } else if ("base64".equals(a3)) {
                decode = Base64.decode(a2, i);
            } else if ("base64url".equals(a3)) {
                decode = Base64.decode(a2, i | 8);
            } else {
                bk.a("Encode: unknown input format: " + a3);
                return er.e();
            }
            if ("base16".equals(a4)) {
                encodeToString = fc.a(decode);
            } else if ("base64".equals(a4)) {
                encodeToString = Base64.encodeToString(decode, i);
            } else if ("base64url".equals(a4)) {
                encodeToString = Base64.encodeToString(decode, i | 8);
            } else {
                bk.a("Encode: unknown output format: " + a4);
                return er.e();
            }
            return er.e(encodeToString);
        } catch (IllegalArgumentException e2) {
            bk.a("Encode: invalid input:");
            return er.e();
        }
    }

    @Override // com.google.android.gms.d.ag
    public boolean a() {
        return true;
    }
}
