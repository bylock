package com.google.android.gms.d;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class di {
    private final List a;
    private final Map b;
    private final String c;
    private final int d;

    private di(List list, Map map, String str, int i) {
        this.a = Collections.unmodifiableList(list);
        this.b = Collections.unmodifiableMap(map);
        this.c = str;
        this.d = i;
    }

    public static dj a() {
        return new dj();
    }

    public List b() {
        return this.a;
    }

    public String c() {
        return this.c;
    }

    public Map d() {
        return this.b;
    }

    public String toString() {
        return "Rules: " + b() + "  Macros: " + this.b;
    }
}
