package com.google.android.gms.d;

import com.google.android.gms.internal.y;
import java.util.Collections;
import java.util.Map;

public class dg {
    private final Map a;
    private final y b;

    private dg(Map map, y yVar) {
        this.a = map;
        this.b = yVar;
    }

    public static dh a() {
        return new dh();
    }

    public void a(String str, y yVar) {
        this.a.put(str, yVar);
    }

    public Map b() {
        return Collections.unmodifiableMap(this.a);
    }

    public y c() {
        return this.b;
    }

    public String toString() {
        return "Properties: " + b() + " pushAfterEvaluate: " + this.b;
    }
}
