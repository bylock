package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.a.b;
import java.util.ArrayList;
import java.util.List;

public final class PolygonOptions implements SafeParcelable {
    public static final h a = new h();
    private final int b;
    private final List c;
    private final List d;
    private float e;
    private int f;
    private int g;
    private float h;
    private boolean i;
    private boolean j;

    public PolygonOptions() {
        this.e = 10.0f;
        this.f = -16777216;
        this.g = 0;
        this.h = 0.0f;
        this.i = true;
        this.j = false;
        this.b = 1;
        this.c = new ArrayList();
        this.d = new ArrayList();
    }

    PolygonOptions(int i2, List list, List list2, float f2, int i3, int i4, float f3, boolean z, boolean z2) {
        this.e = 10.0f;
        this.f = -16777216;
        this.g = 0;
        this.h = 0.0f;
        this.i = true;
        this.j = false;
        this.b = i2;
        this.c = list;
        this.d = list2;
        this.e = f2;
        this.f = i3;
        this.g = i4;
        this.h = f3;
        this.i = z;
        this.j = z2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public List b() {
        return this.d;
    }

    public List c() {
        return this.c;
    }

    public float d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public int e() {
        return this.f;
    }

    public int f() {
        return this.g;
    }

    public float g() {
        return this.h;
    }

    public boolean h() {
        return this.i;
    }

    public boolean i() {
        return this.j;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (b.a()) {
            z.a(this, parcel, i2);
        } else {
            h.a(this, parcel, i2);
        }
    }
}
