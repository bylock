package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;
import com.google.android.gms.internal.aw;

public class StreetViewPanoramaCamera implements SafeParcelable {
    public static final j a = new j();
    public final float b;
    public final float c;
    public final float d;
    private final int e;
    private StreetViewPanoramaOrientation f;

    StreetViewPanoramaCamera(int i, float f2, float f3, float f4) {
        aw.b(-90.0f <= f3 && f3 <= 90.0f, "Tilt needs to be between -90 and 90 inclusive");
        this.e = i;
        this.b = f2;
        this.c = 0.0f + f3;
        this.d = (((double) f4) <= 0.0d ? (f4 % 360.0f) + 360.0f : f4) % 360.0f;
        this.f = new m().a(f3).b(f4).a();
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaCamera)) {
            return false;
        }
        StreetViewPanoramaCamera streetViewPanoramaCamera = (StreetViewPanoramaCamera) obj;
        return Float.floatToIntBits(this.b) == Float.floatToIntBits(streetViewPanoramaCamera.b) && Float.floatToIntBits(this.c) == Float.floatToIntBits(streetViewPanoramaCamera.c) && Float.floatToIntBits(this.d) == Float.floatToIntBits(streetViewPanoramaCamera.d);
    }

    public int hashCode() {
        return ar.a(Float.valueOf(this.b), Float.valueOf(this.c), Float.valueOf(this.d));
    }

    public String toString() {
        return ar.a(this).a("zoom", Float.valueOf(this.b)).a("tilt", Float.valueOf(this.c)).a("bearing", Float.valueOf(this.d)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        j.a(this, parcel, i);
    }
}
