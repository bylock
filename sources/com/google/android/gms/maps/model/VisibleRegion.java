package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;
import com.google.android.gms.maps.a.b;

public final class VisibleRegion implements SafeParcelable {
    public static final s a = new s();
    public final LatLng b;
    public final LatLng c;
    public final LatLng d;
    public final LatLng e;
    public final LatLngBounds f;
    private final int g;

    VisibleRegion(int i, LatLng latLng, LatLng latLng2, LatLng latLng3, LatLng latLng4, LatLngBounds latLngBounds) {
        this.g = i;
        this.b = latLng;
        this.c = latLng2;
        this.d = latLng3;
        this.e = latLng4;
        this.f = latLngBounds;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.g;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VisibleRegion)) {
            return false;
        }
        VisibleRegion visibleRegion = (VisibleRegion) obj;
        return this.b.equals(visibleRegion.b) && this.c.equals(visibleRegion.c) && this.d.equals(visibleRegion.d) && this.e.equals(visibleRegion.e) && this.f.equals(visibleRegion.f);
    }

    public int hashCode() {
        return ar.a(this.b, this.c, this.d, this.e, this.f);
    }

    public String toString() {
        return ar.a(this).a("nearLeft", this.b).a("nearRight", this.c).a("farLeft", this.d).a("farRight", this.e).a("latLngBounds", this.f).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (b.a()) {
            ad.a(this, parcel, i);
        } else {
            s.a(this, parcel, i);
        }
    }
}
