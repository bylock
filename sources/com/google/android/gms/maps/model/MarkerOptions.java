package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.c.b;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class MarkerOptions implements SafeParcelable {
    public static final g a = new g();
    private final int b;
    private LatLng c;
    private String d;
    private String e;
    private a f;
    private float g;
    private float h;
    private boolean i;
    private boolean j;
    private boolean k;
    private float l;
    private float m;
    private float n;
    private float o;

    public MarkerOptions() {
        this.g = 0.5f;
        this.h = 1.0f;
        this.j = true;
        this.k = false;
        this.l = 0.0f;
        this.m = 0.5f;
        this.n = 0.0f;
        this.o = 1.0f;
        this.b = 1;
    }

    MarkerOptions(int i2, LatLng latLng, String str, String str2, IBinder iBinder, float f2, float f3, boolean z, boolean z2, boolean z3, float f4, float f5, float f6, float f7) {
        this.g = 0.5f;
        this.h = 1.0f;
        this.j = true;
        this.k = false;
        this.l = 0.0f;
        this.m = 0.5f;
        this.n = 0.0f;
        this.o = 1.0f;
        this.b = i2;
        this.c = latLng;
        this.d = str;
        this.e = str2;
        this.f = iBinder == null ? null : new a(b.a(iBinder));
        this.g = f2;
        this.h = f3;
        this.i = z;
        this.j = z2;
        this.k = z3;
        this.l = f4;
        this.m = f5;
        this.n = f6;
        this.o = f7;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public IBinder b() {
        if (this.f == null) {
            return null;
        }
        return this.f.a().asBinder();
    }

    public LatLng c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.e;
    }

    public float f() {
        return this.g;
    }

    public float g() {
        return this.h;
    }

    public boolean h() {
        return this.i;
    }

    public boolean i() {
        return this.j;
    }

    public boolean j() {
        return this.k;
    }

    public float k() {
        return this.l;
    }

    public float l() {
        return this.m;
    }

    public float m() {
        return this.n;
    }

    public float n() {
        return this.o;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (com.google.android.gms.maps.a.b.a()) {
            y.a(this, parcel, i2);
        } else {
            g.a(this, parcel, i2);
        }
    }
}
