package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;

public class StreetViewPanoramaLink implements SafeParcelable {
    public static final k a = new k();
    public final String b;
    public final float c;
    private final int d;

    StreetViewPanoramaLink(int i, String str, float f) {
        this.d = i;
        this.b = str;
        this.c = (((double) f) <= 0.0d ? (f % 360.0f) + 360.0f : f) % 360.0f;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaLink)) {
            return false;
        }
        StreetViewPanoramaLink streetViewPanoramaLink = (StreetViewPanoramaLink) obj;
        return this.b.equals(streetViewPanoramaLink.b) && Float.floatToIntBits(this.c) == Float.floatToIntBits(streetViewPanoramaLink.c);
    }

    public int hashCode() {
        return ar.a(this.b, Float.valueOf(this.c));
    }

    public String toString() {
        return ar.a(this).a("panoId", this.b).a("bearing", Float.valueOf(this.c)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        k.a(this, parcel, i);
    }
}
