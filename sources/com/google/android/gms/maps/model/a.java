package com.google.android.gms.maps.model;

import com.google.android.gms.internal.aw;

public final class a {
    private final com.google.android.gms.c.a a;

    a(com.google.android.gms.c.a aVar) {
        this.a = (com.google.android.gms.c.a) aw.a(aVar);
    }

    public com.google.android.gms.c.a a() {
        return this.a;
    }
}
