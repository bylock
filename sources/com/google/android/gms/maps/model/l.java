package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class l implements Parcelable.Creator {
    static void a(StreetViewPanoramaLocation streetViewPanoramaLocation, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, streetViewPanoramaLocation.a());
        c.a(parcel, 2, (Parcelable[]) streetViewPanoramaLocation.b, i, false);
        c.a(parcel, 3, (Parcelable) streetViewPanoramaLocation.c, i, false);
        c.a(parcel, 4, streetViewPanoramaLocation.d, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public StreetViewPanoramaLocation createFromParcel(Parcel parcel) {
        String n;
        LatLng latLng;
        StreetViewPanoramaLink[] streetViewPanoramaLinkArr;
        int i;
        String str = null;
        int b = a.b(parcel);
        int i2 = 0;
        LatLng latLng2 = null;
        StreetViewPanoramaLink[] streetViewPanoramaLinkArr2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    latLng = latLng2;
                    streetViewPanoramaLinkArr = streetViewPanoramaLinkArr2;
                    i = a.g(parcel, a);
                    n = str;
                    break;
                case 2:
                    i = i2;
                    streetViewPanoramaLinkArr = (StreetViewPanoramaLink[]) a.b(parcel, a, StreetViewPanoramaLink.a);
                    n = str;
                    latLng = latLng2;
                    break;
                case 3:
                    streetViewPanoramaLinkArr = streetViewPanoramaLinkArr2;
                    i = i2;
                    latLng = (LatLng) a.a(parcel, a, LatLng.a);
                    n = str;
                    break;
                case 4:
                    n = a.n(parcel, a);
                    latLng = latLng2;
                    streetViewPanoramaLinkArr = streetViewPanoramaLinkArr2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, a);
                    n = str;
                    latLng = latLng2;
                    streetViewPanoramaLinkArr = streetViewPanoramaLinkArr2;
                    i = i2;
                    break;
            }
            i2 = i;
            streetViewPanoramaLinkArr2 = streetViewPanoramaLinkArr;
            latLng2 = latLng;
            str = n;
        }
        if (parcel.dataPosition() == b) {
            return new StreetViewPanoramaLocation(i2, streetViewPanoramaLinkArr2, latLng2, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public StreetViewPanoramaLocation[] newArray(int i) {
        return new StreetViewPanoramaLocation[i];
    }
}
