package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.a.b;

public final class Tile implements SafeParcelable {
    public static final o a = new o();
    public final int b;
    public final int c;
    public final byte[] d;
    private final int e;

    Tile(int i, int i2, int i3, byte[] bArr) {
        this.e = i;
        this.b = i2;
        this.c = i3;
        this.d = bArr;
    }

    public Tile(int i, int i2, byte[] bArr) {
        this(1, i, i2, bArr);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (b.a()) {
            ab.a(this, parcel, i);
        } else {
            o.a(this, parcel, i);
        }
    }
}
