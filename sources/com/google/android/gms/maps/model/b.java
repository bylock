package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;

public class b implements Parcelable.Creator {
    static void a(CameraPosition cameraPosition, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, cameraPosition.a());
        c.a(parcel, 2, (Parcelable) cameraPosition.b, i, false);
        c.a(parcel, 3, cameraPosition.c);
        c.a(parcel, 4, cameraPosition.d);
        c.a(parcel, 5, cameraPosition.e);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public CameraPosition createFromParcel(Parcel parcel) {
        float f = 0.0f;
        int b = a.b(parcel);
        int i = 0;
        LatLng latLng = null;
        float f2 = 0.0f;
        float f3 = 0.0f;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    latLng = (LatLng) a.a(parcel, a, LatLng.a);
                    break;
                case 3:
                    f3 = a.k(parcel, a);
                    break;
                case 4:
                    f2 = a.k(parcel, a);
                    break;
                case 5:
                    f = a.k(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new CameraPosition(i, latLng, f3, f2, f);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public CameraPosition[] newArray(int i) {
        return new CameraPosition[i];
    }
}
