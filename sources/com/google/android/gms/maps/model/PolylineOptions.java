package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.a.b;
import java.util.ArrayList;
import java.util.List;

public final class PolylineOptions implements SafeParcelable {
    public static final i a = new i();
    private final int b;
    private final List c;
    private float d;
    private int e;
    private float f;
    private boolean g;
    private boolean h;

    public PolylineOptions() {
        this.d = 10.0f;
        this.e = -16777216;
        this.f = 0.0f;
        this.g = true;
        this.h = false;
        this.b = 1;
        this.c = new ArrayList();
    }

    PolylineOptions(int i, List list, float f2, int i2, float f3, boolean z, boolean z2) {
        this.d = 10.0f;
        this.e = -16777216;
        this.f = 0.0f;
        this.g = true;
        this.h = false;
        this.b = i;
        this.c = list;
        this.d = f2;
        this.e = i2;
        this.f = f3;
        this.g = z;
        this.h = z2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    public List b() {
        return this.c;
    }

    public float c() {
        return this.d;
    }

    public int d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public float e() {
        return this.f;
    }

    public boolean f() {
        return this.g;
    }

    public boolean g() {
        return this.h;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (b.a()) {
            aa.a(this, parcel, i);
        } else {
            i.a(this, parcel, i);
        }
    }
}
