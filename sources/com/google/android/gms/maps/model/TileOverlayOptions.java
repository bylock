package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.model.a.a;
import com.google.android.gms.maps.model.a.b;

public final class TileOverlayOptions implements SafeParcelable {
    public static final q a = new q();
    private final int b;
    private a c;
    private r d;
    private boolean e;
    private float f;
    private boolean g;

    public TileOverlayOptions() {
        this.e = true;
        this.g = true;
        this.b = 1;
    }

    TileOverlayOptions(int i, IBinder iBinder, boolean z, float f2, boolean z2) {
        this.e = true;
        this.g = true;
        this.b = i;
        this.c = b.a(iBinder);
        this.d = this.c == null ? null : new p(this);
        this.e = z;
        this.f = f2;
        this.g = z2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public IBinder b() {
        return this.c.asBinder();
    }

    public float c() {
        return this.f;
    }

    public boolean d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public boolean e() {
        return this.g;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (com.google.android.gms.maps.a.b.a()) {
            ac.a(this, parcel, i);
        } else {
            q.a(this, parcel, i);
        }
    }
}
