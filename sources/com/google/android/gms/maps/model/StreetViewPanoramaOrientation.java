package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;
import com.google.android.gms.internal.aw;

public class StreetViewPanoramaOrientation implements SafeParcelable {
    public static final n a = new n();
    public final float b;
    public final float c;
    private final int d;

    public StreetViewPanoramaOrientation(float f, float f2) {
        this(1, f, f2);
    }

    StreetViewPanoramaOrientation(int i, float f, float f2) {
        aw.b(-90.0f <= f && f <= 90.0f, "Tilt needs to be between -90 and 90 inclusive");
        this.d = i;
        this.b = 0.0f + f;
        this.c = (((double) f2) <= 0.0d ? (f2 % 360.0f) + 360.0f : f2) % 360.0f;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaOrientation)) {
            return false;
        }
        StreetViewPanoramaOrientation streetViewPanoramaOrientation = (StreetViewPanoramaOrientation) obj;
        return Float.floatToIntBits(this.b) == Float.floatToIntBits(streetViewPanoramaOrientation.b) && Float.floatToIntBits(this.c) == Float.floatToIntBits(streetViewPanoramaOrientation.c);
    }

    public int hashCode() {
        return ar.a(Float.valueOf(this.b), Float.valueOf(this.c));
    }

    public String toString() {
        return ar.a(this).a("tilt", Float.valueOf(this.b)).a("bearing", Float.valueOf(this.c)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        n.a(this, parcel, i);
    }
}
