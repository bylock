package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;
import com.google.android.gms.internal.aw;
import com.google.android.gms.maps.a.b;

public final class LatLngBounds implements SafeParcelable {
    public static final e a = new e();
    public final LatLng b;
    public final LatLng c;
    private final int d;

    LatLngBounds(int i, LatLng latLng, LatLng latLng2) {
        aw.a(latLng, "null southwest");
        aw.a(latLng2, "null northeast");
        aw.a(latLng2.b >= latLng.b, "southern latitude exceeds northern latitude (%s > %s)", Double.valueOf(latLng.b), Double.valueOf(latLng2.b));
        this.d = i;
        this.b = latLng;
        this.c = latLng2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLngBounds)) {
            return false;
        }
        LatLngBounds latLngBounds = (LatLngBounds) obj;
        return this.b.equals(latLngBounds.b) && this.c.equals(latLngBounds.c);
    }

    public int hashCode() {
        return ar.a(this.b, this.c);
    }

    public String toString() {
        return ar.a(this).a("southwest", this.b).a("northeast", this.c).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (b.a()) {
            w.a(this, parcel, i);
        } else {
            e.a(this, parcel, i);
        }
    }
}
