package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class s implements Parcelable.Creator {
    static void a(VisibleRegion visibleRegion, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, visibleRegion.a());
        c.a(parcel, 2, (Parcelable) visibleRegion.b, i, false);
        c.a(parcel, 3, (Parcelable) visibleRegion.c, i, false);
        c.a(parcel, 4, (Parcelable) visibleRegion.d, i, false);
        c.a(parcel, 5, (Parcelable) visibleRegion.e, i, false);
        c.a(parcel, 6, (Parcelable) visibleRegion.f, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public VisibleRegion createFromParcel(Parcel parcel) {
        LatLngBounds latLngBounds = null;
        int b = a.b(parcel);
        int i = 0;
        LatLng latLng = null;
        LatLng latLng2 = null;
        LatLng latLng3 = null;
        LatLng latLng4 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    latLng4 = (LatLng) a.a(parcel, a, LatLng.a);
                    break;
                case 3:
                    latLng3 = (LatLng) a.a(parcel, a, LatLng.a);
                    break;
                case 4:
                    latLng2 = (LatLng) a.a(parcel, a, LatLng.a);
                    break;
                case 5:
                    latLng = (LatLng) a.a(parcel, a, LatLng.a);
                    break;
                case 6:
                    latLngBounds = (LatLngBounds) a.a(parcel, a, LatLngBounds.a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new VisibleRegion(i, latLng4, latLng3, latLng2, latLng, latLngBounds);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public VisibleRegion[] newArray(int i) {
        return new VisibleRegion[i];
    }
}
