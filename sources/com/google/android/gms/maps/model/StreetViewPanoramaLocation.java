package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ar;

public class StreetViewPanoramaLocation implements SafeParcelable {
    public static final l a = new l();
    public final StreetViewPanoramaLink[] b;
    public final LatLng c;
    public final String d;
    private final int e;

    StreetViewPanoramaLocation(int i, StreetViewPanoramaLink[] streetViewPanoramaLinkArr, LatLng latLng, String str) {
        this.e = i;
        this.b = streetViewPanoramaLinkArr;
        this.c = latLng;
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaLocation)) {
            return false;
        }
        StreetViewPanoramaLocation streetViewPanoramaLocation = (StreetViewPanoramaLocation) obj;
        return this.d.equals(streetViewPanoramaLocation.d) && this.c.equals(streetViewPanoramaLocation.c);
    }

    public int hashCode() {
        return ar.a(this.c, this.d);
    }

    public String toString() {
        return ar.a(this).a("panoId", this.d).a("position", this.c.toString()).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        l.a(this, parcel, i);
    }
}
