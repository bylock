package com.google.android.gms.maps;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.a.a;
import com.google.android.gms.maps.a.b;
import com.google.android.gms.maps.model.CameraPosition;

public final class GoogleMapOptions implements SafeParcelable {
    public static final a a = new a();
    private final int b;
    private Boolean c;
    private Boolean d;
    private int e;
    private CameraPosition f;
    private Boolean g;
    private Boolean h;
    private Boolean i;
    private Boolean j;
    private Boolean k;
    private Boolean l;

    public GoogleMapOptions() {
        this.e = -1;
        this.b = 1;
    }

    GoogleMapOptions(int i2, byte b2, byte b3, int i3, CameraPosition cameraPosition, byte b4, byte b5, byte b6, byte b7, byte b8, byte b9) {
        this.e = -1;
        this.b = i2;
        this.c = a.a(b2);
        this.d = a.a(b3);
        this.e = i3;
        this.f = cameraPosition;
        this.g = a.a(b4);
        this.h = a.a(b5);
        this.i = a.a(b6);
        this.j = a.a(b7);
        this.k = a.a(b8);
        this.l = a.a(b9);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public byte b() {
        return a.a(this.c);
    }

    /* access modifiers changed from: package-private */
    public byte c() {
        return a.a(this.d);
    }

    /* access modifiers changed from: package-private */
    public byte d() {
        return a.a(this.g);
    }

    public int describeContents() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public byte e() {
        return a.a(this.h);
    }

    /* access modifiers changed from: package-private */
    public byte f() {
        return a.a(this.i);
    }

    /* access modifiers changed from: package-private */
    public byte g() {
        return a.a(this.j);
    }

    /* access modifiers changed from: package-private */
    public byte h() {
        return a.a(this.k);
    }

    /* access modifiers changed from: package-private */
    public byte i() {
        return a.a(this.l);
    }

    public int j() {
        return this.e;
    }

    public CameraPosition k() {
        return this.f;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (b.a()) {
            c.a(this, parcel, i2);
        } else {
            a.a(this, parcel, i2);
        }
    }
}
