package com.google.android.gms.maps;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.a.a;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;

public final class StreetViewPanoramaOptions implements SafeParcelable {
    public static final b a = new b();
    private final int b;
    private StreetViewPanoramaCamera c;
    private String d;
    private LatLng e;
    private Integer f;
    private Boolean g;
    private Boolean h;
    private Boolean i;
    private Boolean j;
    private Boolean k;

    public StreetViewPanoramaOptions() {
        this.g = true;
        this.h = true;
        this.i = true;
        this.j = true;
        this.b = 1;
    }

    StreetViewPanoramaOptions(int i2, StreetViewPanoramaCamera streetViewPanoramaCamera, String str, LatLng latLng, Integer num, byte b2, byte b3, byte b4, byte b5, byte b6) {
        this.g = true;
        this.h = true;
        this.i = true;
        this.j = true;
        this.b = i2;
        this.c = streetViewPanoramaCamera;
        this.e = latLng;
        this.f = num;
        this.d = str;
        this.g = a.a(b2);
        this.h = a.a(b3);
        this.i = a.a(b4);
        this.j = a.a(b5);
        this.k = a.a(b6);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public byte b() {
        return a.a(this.g);
    }

    /* access modifiers changed from: package-private */
    public byte c() {
        return a.a(this.h);
    }

    /* access modifiers changed from: package-private */
    public byte d() {
        return a.a(this.i);
    }

    public int describeContents() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public byte e() {
        return a.a(this.j);
    }

    /* access modifiers changed from: package-private */
    public byte f() {
        return a.a(this.k);
    }

    public StreetViewPanoramaCamera g() {
        return this.c;
    }

    public LatLng h() {
        return this.e;
    }

    public Integer i() {
        return this.f;
    }

    public String j() {
        return this.d;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        b.a(this, parcel, i2);
    }
}
