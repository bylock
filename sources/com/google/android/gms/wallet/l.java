package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.identity.intents.model.CountrySpecification;
import java.util.ArrayList;

public class l implements Parcelable.Creator {
    static void a(MaskedWalletRequest maskedWalletRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, maskedWalletRequest.a());
        c.a(parcel, 2, maskedWalletRequest.a, false);
        c.a(parcel, 3, maskedWalletRequest.b);
        c.a(parcel, 4, maskedWalletRequest.c);
        c.a(parcel, 5, maskedWalletRequest.d);
        c.a(parcel, 6, maskedWalletRequest.e, false);
        c.a(parcel, 7, maskedWalletRequest.f, false);
        c.a(parcel, 8, maskedWalletRequest.g, false);
        c.a(parcel, 9, (Parcelable) maskedWalletRequest.h, i, false);
        c.a(parcel, 10, maskedWalletRequest.i);
        c.a(parcel, 11, maskedWalletRequest.j);
        c.a(parcel, 12, (Parcelable[]) maskedWalletRequest.k, i, false);
        c.a(parcel, 13, maskedWalletRequest.l);
        c.a(parcel, 14, maskedWalletRequest.m);
        c.b(parcel, 15, maskedWalletRequest.n, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public MaskedWalletRequest createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        Cart cart = null;
        boolean z4 = false;
        boolean z5 = false;
        CountrySpecification[] countrySpecificationArr = null;
        boolean z6 = true;
        boolean z7 = true;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    z = a.c(parcel, a);
                    break;
                case 4:
                    z2 = a.c(parcel, a);
                    break;
                case 5:
                    z3 = a.c(parcel, a);
                    break;
                case 6:
                    str2 = a.n(parcel, a);
                    break;
                case 7:
                    str3 = a.n(parcel, a);
                    break;
                case 8:
                    str4 = a.n(parcel, a);
                    break;
                case 9:
                    cart = (Cart) a.a(parcel, a, Cart.CREATOR);
                    break;
                case 10:
                    z4 = a.c(parcel, a);
                    break;
                case 11:
                    z5 = a.c(parcel, a);
                    break;
                case 12:
                    countrySpecificationArr = (CountrySpecification[]) a.b(parcel, a, CountrySpecification.CREATOR);
                    break;
                case 13:
                    z6 = a.c(parcel, a);
                    break;
                case 14:
                    z7 = a.c(parcel, a);
                    break;
                case 15:
                    arrayList = a.c(parcel, a, CountrySpecification.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new MaskedWalletRequest(i, str, z, z2, z3, str2, str3, str4, cart, z4, z5, countrySpecificationArr, z6, z7, arrayList);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public MaskedWalletRequest[] newArray(int i) {
        return new MaskedWalletRequest[i];
    }
}
