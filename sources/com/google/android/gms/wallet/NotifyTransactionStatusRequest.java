package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class NotifyTransactionStatusRequest implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new m();
    final int a;
    String b;
    int c;
    String d;

    NotifyTransactionStatusRequest() {
        this.a = 1;
    }

    NotifyTransactionStatusRequest(int i, String str, int i2, String str2) {
        this.a = i;
        this.b = str;
        this.c = i2;
        this.d = str2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        m.a(this, parcel, i);
    }
}
