package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class a implements Parcelable.Creator {
    static void a(Address address, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, address.a());
        c.a(parcel, 2, address.a, false);
        c.a(parcel, 3, address.b, false);
        c.a(parcel, 4, address.c, false);
        c.a(parcel, 5, address.d, false);
        c.a(parcel, 6, address.e, false);
        c.a(parcel, 7, address.f, false);
        c.a(parcel, 8, address.g, false);
        c.a(parcel, 9, address.h, false);
        c.a(parcel, 10, address.i, false);
        c.a(parcel, 11, address.j);
        c.a(parcel, 12, address.k, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public Address createFromParcel(Parcel parcel) {
        int b = com.google.android.gms.common.internal.safeparcel.a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        String str9 = null;
        boolean z = false;
        String str10 = null;
        while (parcel.dataPosition() < b) {
            int a = com.google.android.gms.common.internal.safeparcel.a.a(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.a.a(a)) {
                case 1:
                    i = com.google.android.gms.common.internal.safeparcel.a.g(parcel, a);
                    break;
                case 2:
                    str = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 3:
                    str2 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 4:
                    str3 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 5:
                    str4 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 6:
                    str5 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 7:
                    str6 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 8:
                    str7 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 9:
                    str8 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 10:
                    str9 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 11:
                    z = com.google.android.gms.common.internal.safeparcel.a.c(parcel, a);
                    break;
                case 12:
                    str10 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new Address(i, str, str2, str3, str4, str5, str6, str7, str8, str9, z, str10);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public Address[] newArray(int i) {
        return new Address[i];
    }
}
