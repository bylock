package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class o implements Parcelable.Creator {
    static void a(ProxyCard proxyCard, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, proxyCard.a());
        c.a(parcel, 2, proxyCard.a, false);
        c.a(parcel, 3, proxyCard.b, false);
        c.a(parcel, 4, proxyCard.c);
        c.a(parcel, 5, proxyCard.d);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ProxyCard createFromParcel(Parcel parcel) {
        String str = null;
        int i = 0;
        int b = a.b(parcel);
        int i2 = 0;
        String str2 = null;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i3 = a.g(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    str = a.n(parcel, a);
                    break;
                case 4:
                    i2 = a.g(parcel, a);
                    break;
                case 5:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ProxyCard(i3, str2, str, i2, i);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ProxyCard[] newArray(int i) {
        return new ProxyCard[i];
    }
}
