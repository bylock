package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class g implements Parcelable.Creator {
    static void a(FullWalletRequest fullWalletRequest, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, fullWalletRequest.a());
        c.a(parcel, 2, fullWalletRequest.a, false);
        c.a(parcel, 3, fullWalletRequest.b, false);
        c.a(parcel, 4, (Parcelable) fullWalletRequest.c, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public FullWalletRequest createFromParcel(Parcel parcel) {
        Cart cart = null;
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    str = a.n(parcel, a);
                    break;
                case 4:
                    cart = (Cart) a.a(parcel, a, Cart.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new FullWalletRequest(i, str2, str, cart);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public FullWalletRequest[] newArray(int i) {
        return new FullWalletRequest[i];
    }
}
