package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.b.k;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.internal.bh;
import com.google.android.gms.internal.jm;
import com.google.android.gms.internal.jo;
import com.google.android.gms.internal.js;
import com.google.android.gms.internal.ju;
import com.google.android.gms.internal.jw;
import com.google.android.gms.internal.jy;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;

public class j implements Parcelable.Creator {
    static void a(LoyaltyWalletObject loyaltyWalletObject, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, loyaltyWalletObject.a());
        c.a(parcel, 2, loyaltyWalletObject.a, false);
        c.a(parcel, 3, loyaltyWalletObject.b, false);
        c.a(parcel, 4, loyaltyWalletObject.c, false);
        c.a(parcel, 5, loyaltyWalletObject.d, false);
        c.a(parcel, 6, loyaltyWalletObject.e, false);
        c.a(parcel, 7, loyaltyWalletObject.f, false);
        c.a(parcel, 8, loyaltyWalletObject.g, false);
        c.a(parcel, 9, loyaltyWalletObject.h, false);
        c.a(parcel, 10, loyaltyWalletObject.i, false);
        c.a(parcel, 11, loyaltyWalletObject.j, false);
        c.a(parcel, 12, loyaltyWalletObject.k);
        c.b(parcel, 13, loyaltyWalletObject.l, false);
        c.a(parcel, 14, (Parcelable) loyaltyWalletObject.m, i, false);
        c.b(parcel, 15, loyaltyWalletObject.n, false);
        c.a(parcel, 17, loyaltyWalletObject.p, false);
        c.a(parcel, 16, loyaltyWalletObject.o, false);
        c.a(parcel, 19, loyaltyWalletObject.r);
        c.b(parcel, 18, loyaltyWalletObject.q, false);
        c.b(parcel, 21, loyaltyWalletObject.t, false);
        c.b(parcel, 20, loyaltyWalletObject.s, false);
        c.a(parcel, 23, (Parcelable) loyaltyWalletObject.v, i, false);
        c.b(parcel, 22, loyaltyWalletObject.u, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public LoyaltyWalletObject createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        String str9 = null;
        String str10 = null;
        int i2 = 0;
        ArrayList a = bh.a();
        ju juVar = null;
        ArrayList a2 = bh.a();
        String str11 = null;
        String str12 = null;
        ArrayList a3 = bh.a();
        boolean z = false;
        ArrayList a4 = bh.a();
        ArrayList a5 = bh.a();
        ArrayList a6 = bh.a();
        jo joVar = null;
        while (parcel.dataPosition() < b) {
            int a7 = a.a(parcel);
            switch (a.a(a7)) {
                case 1:
                    i = a.g(parcel, a7);
                    break;
                case 2:
                    str = a.n(parcel, a7);
                    break;
                case 3:
                    str2 = a.n(parcel, a7);
                    break;
                case 4:
                    str3 = a.n(parcel, a7);
                    break;
                case 5:
                    str4 = a.n(parcel, a7);
                    break;
                case 6:
                    str5 = a.n(parcel, a7);
                    break;
                case 7:
                    str6 = a.n(parcel, a7);
                    break;
                case 8:
                    str7 = a.n(parcel, a7);
                    break;
                case 9:
                    str8 = a.n(parcel, a7);
                    break;
                case 10:
                    str9 = a.n(parcel, a7);
                    break;
                case 11:
                    str10 = a.n(parcel, a7);
                    break;
                case 12:
                    i2 = a.g(parcel, a7);
                    break;
                case 13:
                    a = a.c(parcel, a7, jy.CREATOR);
                    break;
                case 14:
                    juVar = (ju) a.a(parcel, a7, ju.CREATOR);
                    break;
                case 15:
                    a2 = a.c(parcel, a7, LatLng.a);
                    break;
                case 16:
                    str11 = a.n(parcel, a7);
                    break;
                case k.ActionBar_progressBarPadding:
                    str12 = a.n(parcel, a7);
                    break;
                case k.ActionBar_itemPadding:
                    a3 = a.c(parcel, a7, jm.CREATOR);
                    break;
                case 19:
                    z = a.c(parcel, a7);
                    break;
                case 20:
                    a4 = a.c(parcel, a7, jw.CREATOR);
                    break;
                case 21:
                    a5 = a.c(parcel, a7, js.CREATOR);
                    break;
                case 22:
                    a6 = a.c(parcel, a7, jw.CREATOR);
                    break;
                case 23:
                    joVar = (jo) a.a(parcel, a7, jo.CREATOR);
                    break;
                default:
                    a.b(parcel, a7);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new LoyaltyWalletObject(i, str, str2, str3, str4, str5, str6, str7, str8, str9, str10, i2, a, juVar, a2, str11, str12, a3, z, a4, a5, a6, joVar);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public LoyaltyWalletObject[] newArray(int i) {
        return new LoyaltyWalletObject[i];
    }
}
