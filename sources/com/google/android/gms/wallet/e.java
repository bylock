package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class e implements Parcelable.Creator {
    static void a(d dVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, dVar.a());
        c.a(parcel, 2, (Parcelable) dVar.a, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public d createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        LoyaltyWalletObject loyaltyWalletObject = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    loyaltyWalletObject = (LoyaltyWalletObject) a.a(parcel, a, LoyaltyWalletObject.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new d(i, loyaltyWalletObject);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public d[] newArray(int i) {
        return new d[i];
    }
}
