package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;

public class b implements Parcelable.Creator {
    static void a(Cart cart, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, cart.a());
        c.a(parcel, 2, cart.a, false);
        c.a(parcel, 3, cart.b, false);
        c.b(parcel, 4, cart.c, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public Cart createFromParcel(Parcel parcel) {
        String str = null;
        int b = a.b(parcel);
        int i = 0;
        ArrayList arrayList = new ArrayList();
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    str = a.n(parcel, a);
                    break;
                case 4:
                    arrayList = a.c(parcel, a, LineItem.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new Cart(i, str2, str, arrayList);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public Cart[] newArray(int i) {
        return new Cart[i];
    }
}
