package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class i implements Parcelable.Creator {
    static void a(LineItem lineItem, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, lineItem.a());
        c.a(parcel, 2, lineItem.a, false);
        c.a(parcel, 3, lineItem.b, false);
        c.a(parcel, 4, lineItem.c, false);
        c.a(parcel, 5, lineItem.d, false);
        c.a(parcel, 6, lineItem.e);
        c.a(parcel, 7, lineItem.f, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public LineItem createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int b = a.b(parcel);
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i2 = a.g(parcel, a);
                    break;
                case 2:
                    str5 = a.n(parcel, a);
                    break;
                case 3:
                    str4 = a.n(parcel, a);
                    break;
                case 4:
                    str3 = a.n(parcel, a);
                    break;
                case 5:
                    str2 = a.n(parcel, a);
                    break;
                case 6:
                    i = a.g(parcel, a);
                    break;
                case 7:
                    str = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new LineItem(i2, str5, str4, str3, str2, i, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public LineItem[] newArray(int i) {
        return new LineItem[i];
    }
}
