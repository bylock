package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class d implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new e();
    LoyaltyWalletObject a;
    private final int b;

    d() {
        this.b = 1;
    }

    d(int i, LoyaltyWalletObject loyaltyWalletObject) {
        this.b = i;
        this.a = loyaltyWalletObject;
    }

    public int a() {
        return this.b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        e.a(this, parcel, i);
    }
}
