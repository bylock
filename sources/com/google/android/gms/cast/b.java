package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;

public class b implements Parcelable.Creator {
    static void a(CastDevice castDevice, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, castDevice.a());
        c.a(parcel, 2, castDevice.b(), false);
        c.a(parcel, 3, castDevice.a, false);
        c.a(parcel, 4, castDevice.c(), false);
        c.a(parcel, 5, castDevice.d(), false);
        c.a(parcel, 6, castDevice.e(), false);
        c.a(parcel, 7, castDevice.f());
        c.b(parcel, 8, castDevice.g(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public CastDevice createFromParcel(Parcel parcel) {
        int i = 0;
        ArrayList arrayList = null;
        int b = a.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i2 = a.g(parcel, a);
                    break;
                case 2:
                    str5 = a.n(parcel, a);
                    break;
                case 3:
                    str4 = a.n(parcel, a);
                    break;
                case 4:
                    str3 = a.n(parcel, a);
                    break;
                case 5:
                    str2 = a.n(parcel, a);
                    break;
                case 6:
                    str = a.n(parcel, a);
                    break;
                case 7:
                    i = a.g(parcel, a);
                    break;
                case 8:
                    arrayList = a.c(parcel, a, WebImage.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new CastDevice(i2, str5, str4, str3, str2, str, i, arrayList);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public CastDevice[] newArray(int i) {
        return new CastDevice[i];
    }
}
