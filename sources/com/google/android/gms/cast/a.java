package com.google.android.gms.cast;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.images.WebImage;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;

public class a implements Parcelable.Creator {
    static void a(ApplicationMetadata applicationMetadata, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, applicationMetadata.a());
        c.a(parcel, 2, applicationMetadata.b(), false);
        c.a(parcel, 3, applicationMetadata.c(), false);
        c.b(parcel, 4, applicationMetadata.f(), false);
        c.a(parcel, 5, applicationMetadata.d, false);
        c.a(parcel, 6, applicationMetadata.d(), false);
        c.a(parcel, 7, (Parcelable) applicationMetadata.e(), i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ApplicationMetadata createFromParcel(Parcel parcel) {
        Uri uri = null;
        int b = com.google.android.gms.common.internal.safeparcel.a.b(parcel);
        int i = 0;
        String str = null;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = com.google.android.gms.common.internal.safeparcel.a.a(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.a.a(a)) {
                case 1:
                    i = com.google.android.gms.common.internal.safeparcel.a.g(parcel, a);
                    break;
                case 2:
                    str3 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 3:
                    str2 = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 4:
                    arrayList2 = com.google.android.gms.common.internal.safeparcel.a.c(parcel, a, WebImage.CREATOR);
                    break;
                case 5:
                    arrayList = com.google.android.gms.common.internal.safeparcel.a.A(parcel, a);
                    break;
                case 6:
                    str = com.google.android.gms.common.internal.safeparcel.a.n(parcel, a);
                    break;
                case 7:
                    uri = (Uri) com.google.android.gms.common.internal.safeparcel.a.a(parcel, a, Uri.CREATOR);
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ApplicationMetadata(i, str3, str2, arrayList2, arrayList, str, uri);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ApplicationMetadata[] newArray(int i) {
        return new ApplicationMetadata[i];
    }
}
