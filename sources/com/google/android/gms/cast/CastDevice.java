package com.google.android.gms.cast;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.aj;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CastDevice implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new b();
    String a;
    private final int b;
    private String c;
    private Inet4Address d;
    private String e;
    private String f;
    private String g;
    private int h;
    private List i;

    private CastDevice() {
        this(1, null, null, null, null, null, -1, new ArrayList());
    }

    CastDevice(int i2, String str, String str2, String str3, String str4, String str5, int i3, List list) {
        this.b = i2;
        this.c = str;
        this.a = str2;
        if (this.a != null) {
            try {
                InetAddress byName = InetAddress.getByName(this.a);
                if (byName instanceof Inet4Address) {
                    this.d = (Inet4Address) byName;
                }
            } catch (UnknownHostException e2) {
                this.d = null;
            }
        }
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = i3;
        this.i = list;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }

    public String c() {
        return this.e;
    }

    public String d() {
        return this.f;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.g;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CastDevice)) {
            return false;
        }
        CastDevice castDevice = (CastDevice) obj;
        return b() == null ? castDevice.b() == null : aj.a(this.c, castDevice.c) && aj.a(this.d, castDevice.d) && aj.a(this.f, castDevice.f) && aj.a(this.e, castDevice.e) && aj.a(this.g, castDevice.g) && this.h == castDevice.h && aj.a(this.i, castDevice.i);
    }

    public int f() {
        return this.h;
    }

    public List g() {
        return Collections.unmodifiableList(this.i);
    }

    public int hashCode() {
        if (this.c == null) {
            return 0;
        }
        return this.c.hashCode();
    }

    public String toString() {
        return String.format("\"%s\" (%s)", this.e, this.c);
    }

    public void writeToParcel(Parcel parcel, int i2) {
        b.a(this, parcel, i2);
    }
}
