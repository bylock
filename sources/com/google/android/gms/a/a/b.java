package com.google.android.gms.a.a;

public final class b {
    private final String a;
    private final boolean b;

    public b(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public String a() {
        return this.a;
    }

    public boolean b() {
        return this.b;
    }
}
