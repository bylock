package com.google.android.gms.a.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.c;
import com.google.android.gms.common.e;
import com.google.android.gms.internal.aw;
import com.google.android.gms.internal.eg;
import com.google.android.gms.internal.eh;
import java.io.IOException;

public final class a {
    static b a(Context context, e eVar) {
        try {
            eg a = eh.a(eVar.a());
            b bVar = new b(a.a(), a.a(true));
            try {
                context.unbindService(eVar);
            } catch (IllegalArgumentException e) {
                Log.i("AdvertisingIdClient", "getAdvertisingIdInfo unbindService failed.", e);
            }
            return bVar;
        } catch (RemoteException e2) {
            Log.i("AdvertisingIdClient", "GMS remote exception ", e2);
            throw new IOException("Remote exception");
        } catch (InterruptedException e3) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            try {
                context.unbindService(eVar);
            } catch (IllegalArgumentException e4) {
                Log.i("AdvertisingIdClient", "getAdvertisingIdInfo unbindService failed.", e4);
            }
            throw th;
        }
    }

    static e a(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            try {
                c.b(context);
                e eVar = new e();
                Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
                intent.setPackage("com.google.android.gms");
                if (context.bindService(intent, eVar, 1)) {
                    return eVar;
                }
                throw new IOException("Connection failure");
            } catch (com.google.android.gms.common.a e) {
                throw new IOException(e);
            }
        } catch (PackageManager.NameNotFoundException e2) {
            throw new com.google.android.gms.common.a(9);
        }
    }

    public static b b(Context context) {
        aw.b("Calling this from your main thread can lead to deadlock");
        return a(context, a(context));
    }
}
