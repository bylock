package com.google.android.gms.internal;

import android.support.v7.b.k;

public final class o extends dv {
    private static volatile o[] k;
    public int[] a;
    public int[] b;
    public int[] c;
    public int[] d;
    public int[] e;
    public int[] f;
    public int[] g;
    public int[] h;
    public int[] i;
    public int[] j;

    public o() {
        c();
    }

    public static o[] a() {
        if (k == null) {
            synchronized (dy.a) {
                if (k == null) {
                    k = new o[0];
                }
            }
        }
        return k;
    }

    /* renamed from: a */
    public o b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int a3 = ed.a(dsVar, 8);
                    int length = this.a == null ? 0 : this.a.length;
                    int[] iArr = new int[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.a, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = dsVar.e();
                        dsVar.a();
                        length++;
                    }
                    iArr[length] = dsVar.e();
                    this.a = iArr;
                    break;
                case 10:
                    int c2 = dsVar.c(dsVar.i());
                    int o = dsVar.o();
                    int i2 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i2++;
                    }
                    dsVar.e(o);
                    int length2 = this.a == null ? 0 : this.a.length;
                    int[] iArr2 = new int[(i2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.a, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = dsVar.e();
                        length2++;
                    }
                    this.a = iArr2;
                    dsVar.d(c2);
                    break;
                case 16:
                    int a4 = ed.a(dsVar, 16);
                    int length3 = this.b == null ? 0 : this.b.length;
                    int[] iArr3 = new int[(a4 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.b, 0, iArr3, 0, length3);
                    }
                    while (length3 < iArr3.length - 1) {
                        iArr3[length3] = dsVar.e();
                        dsVar.a();
                        length3++;
                    }
                    iArr3[length3] = dsVar.e();
                    this.b = iArr3;
                    break;
                case k.ActionBar_itemPadding:
                    int c3 = dsVar.c(dsVar.i());
                    int o2 = dsVar.o();
                    int i3 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i3++;
                    }
                    dsVar.e(o2);
                    int length4 = this.b == null ? 0 : this.b.length;
                    int[] iArr4 = new int[(i3 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.b, 0, iArr4, 0, length4);
                    }
                    while (length4 < iArr4.length) {
                        iArr4[length4] = dsVar.e();
                        length4++;
                    }
                    this.b = iArr4;
                    dsVar.d(c3);
                    break;
                case 24:
                    int a5 = ed.a(dsVar, 24);
                    int length5 = this.c == null ? 0 : this.c.length;
                    int[] iArr5 = new int[(a5 + length5)];
                    if (length5 != 0) {
                        System.arraycopy(this.c, 0, iArr5, 0, length5);
                    }
                    while (length5 < iArr5.length - 1) {
                        iArr5[length5] = dsVar.e();
                        dsVar.a();
                        length5++;
                    }
                    iArr5[length5] = dsVar.e();
                    this.c = iArr5;
                    break;
                case 26:
                    int c4 = dsVar.c(dsVar.i());
                    int o3 = dsVar.o();
                    int i4 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i4++;
                    }
                    dsVar.e(o3);
                    int length6 = this.c == null ? 0 : this.c.length;
                    int[] iArr6 = new int[(i4 + length6)];
                    if (length6 != 0) {
                        System.arraycopy(this.c, 0, iArr6, 0, length6);
                    }
                    while (length6 < iArr6.length) {
                        iArr6[length6] = dsVar.e();
                        length6++;
                    }
                    this.c = iArr6;
                    dsVar.d(c4);
                    break;
                case 32:
                    int a6 = ed.a(dsVar, 32);
                    int length7 = this.d == null ? 0 : this.d.length;
                    int[] iArr7 = new int[(a6 + length7)];
                    if (length7 != 0) {
                        System.arraycopy(this.d, 0, iArr7, 0, length7);
                    }
                    while (length7 < iArr7.length - 1) {
                        iArr7[length7] = dsVar.e();
                        dsVar.a();
                        length7++;
                    }
                    iArr7[length7] = dsVar.e();
                    this.d = iArr7;
                    break;
                case 34:
                    int c5 = dsVar.c(dsVar.i());
                    int o4 = dsVar.o();
                    int i5 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i5++;
                    }
                    dsVar.e(o4);
                    int length8 = this.d == null ? 0 : this.d.length;
                    int[] iArr8 = new int[(i5 + length8)];
                    if (length8 != 0) {
                        System.arraycopy(this.d, 0, iArr8, 0, length8);
                    }
                    while (length8 < iArr8.length) {
                        iArr8[length8] = dsVar.e();
                        length8++;
                    }
                    this.d = iArr8;
                    dsVar.d(c5);
                    break;
                case 40:
                    int a7 = ed.a(dsVar, 40);
                    int length9 = this.e == null ? 0 : this.e.length;
                    int[] iArr9 = new int[(a7 + length9)];
                    if (length9 != 0) {
                        System.arraycopy(this.e, 0, iArr9, 0, length9);
                    }
                    while (length9 < iArr9.length - 1) {
                        iArr9[length9] = dsVar.e();
                        dsVar.a();
                        length9++;
                    }
                    iArr9[length9] = dsVar.e();
                    this.e = iArr9;
                    break;
                case 42:
                    int c6 = dsVar.c(dsVar.i());
                    int o5 = dsVar.o();
                    int i6 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i6++;
                    }
                    dsVar.e(o5);
                    int length10 = this.e == null ? 0 : this.e.length;
                    int[] iArr10 = new int[(i6 + length10)];
                    if (length10 != 0) {
                        System.arraycopy(this.e, 0, iArr10, 0, length10);
                    }
                    while (length10 < iArr10.length) {
                        iArr10[length10] = dsVar.e();
                        length10++;
                    }
                    this.e = iArr10;
                    dsVar.d(c6);
                    break;
                case 48:
                    int a8 = ed.a(dsVar, 48);
                    int length11 = this.f == null ? 0 : this.f.length;
                    int[] iArr11 = new int[(a8 + length11)];
                    if (length11 != 0) {
                        System.arraycopy(this.f, 0, iArr11, 0, length11);
                    }
                    while (length11 < iArr11.length - 1) {
                        iArr11[length11] = dsVar.e();
                        dsVar.a();
                        length11++;
                    }
                    iArr11[length11] = dsVar.e();
                    this.f = iArr11;
                    break;
                case 50:
                    int c7 = dsVar.c(dsVar.i());
                    int o6 = dsVar.o();
                    int i7 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i7++;
                    }
                    dsVar.e(o6);
                    int length12 = this.f == null ? 0 : this.f.length;
                    int[] iArr12 = new int[(i7 + length12)];
                    if (length12 != 0) {
                        System.arraycopy(this.f, 0, iArr12, 0, length12);
                    }
                    while (length12 < iArr12.length) {
                        iArr12[length12] = dsVar.e();
                        length12++;
                    }
                    this.f = iArr12;
                    dsVar.d(c7);
                    break;
                case 56:
                    int a9 = ed.a(dsVar, 56);
                    int length13 = this.g == null ? 0 : this.g.length;
                    int[] iArr13 = new int[(a9 + length13)];
                    if (length13 != 0) {
                        System.arraycopy(this.g, 0, iArr13, 0, length13);
                    }
                    while (length13 < iArr13.length - 1) {
                        iArr13[length13] = dsVar.e();
                        dsVar.a();
                        length13++;
                    }
                    iArr13[length13] = dsVar.e();
                    this.g = iArr13;
                    break;
                case 58:
                    int c8 = dsVar.c(dsVar.i());
                    int o7 = dsVar.o();
                    int i8 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i8++;
                    }
                    dsVar.e(o7);
                    int length14 = this.g == null ? 0 : this.g.length;
                    int[] iArr14 = new int[(i8 + length14)];
                    if (length14 != 0) {
                        System.arraycopy(this.g, 0, iArr14, 0, length14);
                    }
                    while (length14 < iArr14.length) {
                        iArr14[length14] = dsVar.e();
                        length14++;
                    }
                    this.g = iArr14;
                    dsVar.d(c8);
                    break;
                case 64:
                    int a10 = ed.a(dsVar, 64);
                    int length15 = this.h == null ? 0 : this.h.length;
                    int[] iArr15 = new int[(a10 + length15)];
                    if (length15 != 0) {
                        System.arraycopy(this.h, 0, iArr15, 0, length15);
                    }
                    while (length15 < iArr15.length - 1) {
                        iArr15[length15] = dsVar.e();
                        dsVar.a();
                        length15++;
                    }
                    iArr15[length15] = dsVar.e();
                    this.h = iArr15;
                    break;
                case 66:
                    int c9 = dsVar.c(dsVar.i());
                    int o8 = dsVar.o();
                    int i9 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i9++;
                    }
                    dsVar.e(o8);
                    int length16 = this.h == null ? 0 : this.h.length;
                    int[] iArr16 = new int[(i9 + length16)];
                    if (length16 != 0) {
                        System.arraycopy(this.h, 0, iArr16, 0, length16);
                    }
                    while (length16 < iArr16.length) {
                        iArr16[length16] = dsVar.e();
                        length16++;
                    }
                    this.h = iArr16;
                    dsVar.d(c9);
                    break;
                case 72:
                    int a11 = ed.a(dsVar, 72);
                    int length17 = this.i == null ? 0 : this.i.length;
                    int[] iArr17 = new int[(a11 + length17)];
                    if (length17 != 0) {
                        System.arraycopy(this.i, 0, iArr17, 0, length17);
                    }
                    while (length17 < iArr17.length - 1) {
                        iArr17[length17] = dsVar.e();
                        dsVar.a();
                        length17++;
                    }
                    iArr17[length17] = dsVar.e();
                    this.i = iArr17;
                    break;
                case 74:
                    int c10 = dsVar.c(dsVar.i());
                    int o9 = dsVar.o();
                    int i10 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i10++;
                    }
                    dsVar.e(o9);
                    int length18 = this.i == null ? 0 : this.i.length;
                    int[] iArr18 = new int[(i10 + length18)];
                    if (length18 != 0) {
                        System.arraycopy(this.i, 0, iArr18, 0, length18);
                    }
                    while (length18 < iArr18.length) {
                        iArr18[length18] = dsVar.e();
                        length18++;
                    }
                    this.i = iArr18;
                    dsVar.d(c10);
                    break;
                case 80:
                    int a12 = ed.a(dsVar, 80);
                    int length19 = this.j == null ? 0 : this.j.length;
                    int[] iArr19 = new int[(a12 + length19)];
                    if (length19 != 0) {
                        System.arraycopy(this.j, 0, iArr19, 0, length19);
                    }
                    while (length19 < iArr19.length - 1) {
                        iArr19[length19] = dsVar.e();
                        dsVar.a();
                        length19++;
                    }
                    iArr19[length19] = dsVar.e();
                    this.j = iArr19;
                    break;
                case 82:
                    int c11 = dsVar.c(dsVar.i());
                    int o10 = dsVar.o();
                    int i11 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i11++;
                    }
                    dsVar.e(o10);
                    int length20 = this.j == null ? 0 : this.j.length;
                    int[] iArr20 = new int[(i11 + length20)];
                    if (length20 != 0) {
                        System.arraycopy(this.j, 0, iArr20, 0, length20);
                    }
                    while (length20 < iArr20.length) {
                        iArr20[length20] = dsVar.e();
                        length20++;
                    }
                    this.j = iArr20;
                    dsVar.d(c11);
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (this.a != null && this.a.length > 0) {
            for (int i2 = 0; i2 < this.a.length; i2++) {
                dtVar.a(1, this.a[i2]);
            }
        }
        if (this.b != null && this.b.length > 0) {
            for (int i3 = 0; i3 < this.b.length; i3++) {
                dtVar.a(2, this.b[i3]);
            }
        }
        if (this.c != null && this.c.length > 0) {
            for (int i4 = 0; i4 < this.c.length; i4++) {
                dtVar.a(3, this.c[i4]);
            }
        }
        if (this.d != null && this.d.length > 0) {
            for (int i5 = 0; i5 < this.d.length; i5++) {
                dtVar.a(4, this.d[i5]);
            }
        }
        if (this.e != null && this.e.length > 0) {
            for (int i6 = 0; i6 < this.e.length; i6++) {
                dtVar.a(5, this.e[i6]);
            }
        }
        if (this.f != null && this.f.length > 0) {
            for (int i7 = 0; i7 < this.f.length; i7++) {
                dtVar.a(6, this.f[i7]);
            }
        }
        if (this.g != null && this.g.length > 0) {
            for (int i8 = 0; i8 < this.g.length; i8++) {
                dtVar.a(7, this.g[i8]);
            }
        }
        if (this.h != null && this.h.length > 0) {
            for (int i9 = 0; i9 < this.h.length; i9++) {
                dtVar.a(8, this.h[i9]);
            }
        }
        if (this.i != null && this.i.length > 0) {
            for (int i10 = 0; i10 < this.i.length; i10++) {
                dtVar.a(9, this.i[i10]);
            }
        }
        if (this.j != null && this.j.length > 0) {
            for (int i11 = 0; i11 < this.j.length; i11++) {
                dtVar.a(10, this.j[i11]);
            }
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int i2;
        int b2 = super.b();
        if (this.a == null || this.a.length <= 0) {
            i2 = b2;
        } else {
            int i3 = 0;
            for (int i4 = 0; i4 < this.a.length; i4++) {
                i3 += dt.b(this.a[i4]);
            }
            i2 = b2 + i3 + (this.a.length * 1);
        }
        if (this.b != null && this.b.length > 0) {
            int i5 = 0;
            for (int i6 = 0; i6 < this.b.length; i6++) {
                i5 += dt.b(this.b[i6]);
            }
            i2 = i2 + i5 + (this.b.length * 1);
        }
        if (this.c != null && this.c.length > 0) {
            int i7 = 0;
            for (int i8 = 0; i8 < this.c.length; i8++) {
                i7 += dt.b(this.c[i8]);
            }
            i2 = i2 + i7 + (this.c.length * 1);
        }
        if (this.d != null && this.d.length > 0) {
            int i9 = 0;
            for (int i10 = 0; i10 < this.d.length; i10++) {
                i9 += dt.b(this.d[i10]);
            }
            i2 = i2 + i9 + (this.d.length * 1);
        }
        if (this.e != null && this.e.length > 0) {
            int i11 = 0;
            for (int i12 = 0; i12 < this.e.length; i12++) {
                i11 += dt.b(this.e[i12]);
            }
            i2 = i2 + i11 + (this.e.length * 1);
        }
        if (this.f != null && this.f.length > 0) {
            int i13 = 0;
            for (int i14 = 0; i14 < this.f.length; i14++) {
                i13 += dt.b(this.f[i14]);
            }
            i2 = i2 + i13 + (this.f.length * 1);
        }
        if (this.g != null && this.g.length > 0) {
            int i15 = 0;
            for (int i16 = 0; i16 < this.g.length; i16++) {
                i15 += dt.b(this.g[i16]);
            }
            i2 = i2 + i15 + (this.g.length * 1);
        }
        if (this.h != null && this.h.length > 0) {
            int i17 = 0;
            for (int i18 = 0; i18 < this.h.length; i18++) {
                i17 += dt.b(this.h[i18]);
            }
            i2 = i2 + i17 + (this.h.length * 1);
        }
        if (this.i != null && this.i.length > 0) {
            int i19 = 0;
            for (int i20 = 0; i20 < this.i.length; i20++) {
                i19 += dt.b(this.i[i20]);
            }
            i2 = i2 + i19 + (this.i.length * 1);
        }
        if (this.j != null && this.j.length > 0) {
            int i21 = 0;
            for (int i22 = 0; i22 < this.j.length; i22++) {
                i21 += dt.b(this.j[i22]);
            }
            i2 = i2 + i21 + (this.j.length * 1);
        }
        this.s = i2;
        return i2;
    }

    public o c() {
        this.a = ed.a;
        this.b = ed.a;
        this.c = ed.a;
        this.d = ed.a;
        this.e = ed.a;
        this.f = ed.a;
        this.g = ed.a;
        this.h = ed.a;
        this.i = ed.a;
        this.j = ed.a;
        this.r = null;
        this.s = -1;
        return this;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof o)) {
            return false;
        }
        o oVar = (o) obj;
        if (!dy.a(this.a, oVar.a) || !dy.a(this.b, oVar.b) || !dy.a(this.c, oVar.c) || !dy.a(this.d, oVar.d) || !dy.a(this.e, oVar.e) || !dy.a(this.f, oVar.f) || !dy.a(this.g, oVar.g) || !dy.a(this.h, oVar.h) || !dy.a(this.i, oVar.i) || !dy.a(this.j, oVar.j)) {
            return false;
        }
        return (this.r == null || this.r.isEmpty()) ? oVar.r == null || oVar.r.isEmpty() : this.r.equals(oVar.r);
    }

    public int hashCode() {
        return ((this.r == null || this.r.isEmpty()) ? 0 : this.r.hashCode()) + ((((((((((((((((((((dy.a(this.a) + 527) * 31) + dy.a(this.b)) * 31) + dy.a(this.c)) * 31) + dy.a(this.d)) * 31) + dy.a(this.e)) * 31) + dy.a(this.f)) * 31) + dy.a(this.g)) * 31) + dy.a(this.h)) * 31) + dy.a(this.i)) * 31) + dy.a(this.j)) * 31);
    }
}
