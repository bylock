package com.google.android.gms.internal;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class cx implements SafeParcelable {
    public static final w a = new w();
    public final int b;
    public final Bundle c;
    public final ah d;
    public final ak e;
    public final String f;
    public final ApplicationInfo g;
    public final PackageInfo h;
    public final String i;
    public final String j;
    public final String k;
    public final dx l;
    public final Bundle m;

    cx(int i2, Bundle bundle, ah ahVar, ak akVar, String str, ApplicationInfo applicationInfo, PackageInfo packageInfo, String str2, String str3, String str4, dx dxVar, Bundle bundle2) {
        this.b = i2;
        this.c = bundle;
        this.d = ahVar;
        this.e = akVar;
        this.f = str;
        this.g = applicationInfo;
        this.h = packageInfo;
        this.i = str2;
        this.j = str3;
        this.k = str4;
        this.l = dxVar;
        this.m = bundle2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        w.a(this, parcel, i2);
    }
}
