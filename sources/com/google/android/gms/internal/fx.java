package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public final class fx implements SafeParcelable, bb {
    public static final az a = new az();
    private final int b;
    private final HashMap c;
    private final HashMap d;
    private final ArrayList e;

    public final class a implements SafeParcelable {
        public static final ba a = new ba();
        final int b;
        final String c;
        final int d;

        a(int i, String str, int i2) {
            this.b = i;
            this.c = str;
            this.d = i2;
        }

        a(String str, int i) {
            this.b = 1;
            this.c = str;
            this.d = i;
        }

        public int describeContents() {
            ba baVar = a;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            ba baVar = a;
            ba.a(this, parcel, i);
        }
    }

    public fx() {
        this.b = 1;
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = null;
    }

    fx(int i, ArrayList arrayList) {
        this.b = i;
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = null;
        a(arrayList);
    }

    private void a(ArrayList arrayList) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            a aVar = (a) it.next();
            a(aVar.c, aVar.d);
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    public fx a(String str, int i) {
        this.c.put(str, Integer.valueOf(i));
        this.d.put(Integer.valueOf(i), str);
        return this;
    }

    public String a(Integer num) {
        String str = (String) this.d.get(num);
        return (str != null || !this.c.containsKey("gms_unknown")) ? str : "gms_unknown";
    }

    /* access modifiers changed from: package-private */
    public ArrayList b() {
        ArrayList arrayList = new ArrayList();
        for (String str : this.c.keySet()) {
            arrayList.add(new a(str, ((Integer) this.c.get(str)).intValue()));
        }
        return arrayList;
    }

    @Override // com.google.android.gms.internal.bb
    public int c() {
        return 7;
    }

    @Override // com.google.android.gms.internal.bb
    public int d() {
        return 0;
    }

    public int describeContents() {
        az azVar = a;
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        az azVar = a;
        az.a(this, parcel, i);
    }
}
