package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;

public class dj implements Parcelable.Creator {
    static void a(jm jmVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, jmVar.a());
        c.a(parcel, 2, jmVar.a, false);
        c.a(parcel, 3, jmVar.b, false);
        c.b(parcel, 4, jmVar.c, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public jm createFromParcel(Parcel parcel) {
        String str = null;
        int b = a.b(parcel);
        int i = 0;
        ArrayList a = bh.a();
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a2 = a.a(parcel);
            switch (a.a(a2)) {
                case 1:
                    i = a.g(parcel, a2);
                    break;
                case 2:
                    str2 = a.n(parcel, a2);
                    break;
                case 3:
                    str = a.n(parcel, a2);
                    break;
                case 4:
                    a = a.c(parcel, a2, jk.CREATOR);
                    break;
                default:
                    a.b(parcel, a2);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new jm(i, str2, str, a);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public jm[] newArray(int i) {
        return new jm[i];
    }
}
