package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;

public class ck implements Parcelable.Creator {
    static void a(hg hgVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.b(parcel, 1, hgVar.c, false);
        c.a(parcel, 1000, hgVar.b);
        c.a(parcel, 2, hgVar.a(), false);
        c.a(parcel, 3, hgVar.b());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public hg createFromParcel(Parcel parcel) {
        String str = null;
        boolean z = false;
        int b = a.b(parcel);
        ArrayList arrayList = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    arrayList = a.c(parcel, a, hm.bw);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    z = a.c(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new hg(i, arrayList, str, z);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public hg[] newArray(int i) {
        return new hg[i];
    }
}
