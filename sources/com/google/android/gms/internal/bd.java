package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.internal.ga;
import com.google.android.gms.internal.gd;

public class bd implements Parcelable.Creator {
    static void a(gd.b bVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, bVar.b);
        c.a(parcel, 2, bVar.c, false);
        c.a(parcel, 3, (Parcelable) bVar.d, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public gd.b createFromParcel(Parcel parcel) {
        ga.a aVar = null;
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    aVar = (ga.a) a.a(parcel, a, ga.a.i);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new gd.b(i, str, aVar);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public gd.b[] newArray(int i) {
        return new gd.b[i];
    }
}
