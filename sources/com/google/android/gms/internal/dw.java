package com.google.android.gms.internal;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class dw {
    protected final int a;
    protected final Class b;
    protected final int c;
    protected final boolean d;

    private dw(int i, Class cls, int i2, boolean z) {
        this.a = i;
        this.b = cls;
        this.c = i2;
        this.d = z;
    }

    public static dw a(int i, Class cls, int i2) {
        return new dw(i, cls, i2, false);
    }

    /* access modifiers changed from: protected */
    public Object a(ds dsVar) {
        Class<?> componentType = this.d ? this.b.getComponentType() : this.b;
        try {
            switch (this.a) {
                case 10:
                    ea eaVar = (ea) componentType.newInstance();
                    dsVar.a(eaVar, ed.b(this.c));
                    return eaVar;
                case 11:
                    ea eaVar2 = (ea) componentType.newInstance();
                    dsVar.a(eaVar2);
                    return eaVar2;
                default:
                    throw new IllegalArgumentException("Unknown type " + this.a);
            }
        } catch (InstantiationException e) {
            throw new IllegalArgumentException("Error creating instance of class " + componentType, e);
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("Error creating instance of class " + componentType, e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException("Error reading extension field", e3);
        }
    }

    /* access modifiers changed from: package-private */
    public final Object a(List list) {
        if (list == null) {
            return null;
        }
        if (this.d) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                ec ecVar = (ec) list.get(i);
                if (a(ecVar.a) && ecVar.b.length != 0) {
                    a(ecVar, arrayList);
                }
            }
            int size = arrayList.size();
            if (size == 0) {
                return null;
            }
            Object cast = this.b.cast(Array.newInstance(this.b.getComponentType(), size));
            for (int i2 = 0; i2 < size; i2++) {
                Array.set(cast, i2, arrayList.get(i2));
            }
            return cast;
        }
        int size2 = list.size() - 1;
        ec ecVar2 = null;
        while (ecVar2 == null && size2 >= 0) {
            ec ecVar3 = (ec) list.get(size2);
            if (!a(ecVar3.a) || ecVar3.b.length == 0) {
                ecVar3 = ecVar2;
            }
            size2--;
            ecVar2 = ecVar3;
        }
        if (ecVar2 == null) {
            return null;
        }
        return this.b.cast(a(ds.a(ecVar2.b)));
    }

    /* access modifiers changed from: protected */
    public void a(ec ecVar, List list) {
        list.add(a(ds.a(ecVar.b)));
    }

    /* access modifiers changed from: protected */
    public boolean a(int i) {
        return i == this.c;
    }
}
