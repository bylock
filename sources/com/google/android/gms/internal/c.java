package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;

public class c implements Parcelable.Creator {
    static void a(ak akVar, Parcel parcel, int i) {
        int a = com.google.android.gms.common.internal.safeparcel.c.a(parcel);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 1, akVar.b);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 2, akVar.c, false);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 3, akVar.d);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 4, akVar.e);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 5, akVar.f);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 6, akVar.g);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 7, akVar.h);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, 8, (Parcelable[]) akVar.i, i, false);
        com.google.android.gms.common.internal.safeparcel.c.a(parcel, a);
    }

    /* renamed from: a */
    public ak createFromParcel(Parcel parcel) {
        ak[] akVarArr = null;
        int i = 0;
        int b = a.b(parcel);
        int i2 = 0;
        boolean z = false;
        int i3 = 0;
        int i4 = 0;
        String str = null;
        int i5 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i5 = a.g(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    i4 = a.g(parcel, a);
                    break;
                case 4:
                    i3 = a.g(parcel, a);
                    break;
                case 5:
                    z = a.c(parcel, a);
                    break;
                case 6:
                    i2 = a.g(parcel, a);
                    break;
                case 7:
                    i = a.g(parcel, a);
                    break;
                case 8:
                    akVarArr = (ak[]) a.b(parcel, a, ak.a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ak(i5, str, i4, i3, z, i2, i, akVarArr);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ak[] newArray(int i) {
        return new ak[i];
    }
}
