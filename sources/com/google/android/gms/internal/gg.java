package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.internal.ga;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class gg extends ga implements SafeParcelable {
    public static final bg a = new bg();
    private final int b;
    private final Parcel c;
    private final int d = 2;
    private final gd e;
    private final String f;
    private int g;
    private int h;

    gg(int i, Parcel parcel, gd gdVar) {
        this.b = i;
        this.c = (Parcel) aw.a(parcel);
        this.e = gdVar;
        if (this.e == null) {
            this.f = null;
        } else {
            this.f = this.e.d();
        }
        this.g = 2;
    }

    public static HashMap a(Bundle bundle) {
        HashMap hashMap = new HashMap();
        for (String str : bundle.keySet()) {
            hashMap.put(str, bundle.getString(str));
        }
        return hashMap;
    }

    private static HashMap a(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        for (Map.Entry entry : hashMap.entrySet()) {
            hashMap2.put(Integer.valueOf(((ga.a) entry.getValue()).g()), entry);
        }
        return hashMap2;
    }

    private void a(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"").append(bn.a(obj.toString())).append("\"");
                return;
            case 8:
                sb.append("\"").append(bi.a((byte[]) obj)).append("\"");
                return;
            case 9:
                sb.append("\"").append(bi.b((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                bo.a(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown type = " + i);
        }
    }

    private void a(StringBuilder sb, ga.a aVar, Parcel parcel, int i) {
        switch (aVar.d()) {
            case 0:
                a(sb, aVar, a(aVar, Integer.valueOf(a.g(parcel, i))));
                return;
            case 1:
                a(sb, aVar, a(aVar, a.j(parcel, i)));
                return;
            case 2:
                a(sb, aVar, a(aVar, Long.valueOf(a.i(parcel, i))));
                return;
            case 3:
                a(sb, aVar, a(aVar, Float.valueOf(a.k(parcel, i))));
                return;
            case 4:
                a(sb, aVar, a(aVar, Double.valueOf(a.l(parcel, i))));
                return;
            case 5:
                a(sb, aVar, a(aVar, a.m(parcel, i)));
                return;
            case 6:
                a(sb, aVar, a(aVar, Boolean.valueOf(a.c(parcel, i))));
                return;
            case 7:
                a(sb, aVar, a(aVar, a.n(parcel, i)));
                return;
            case 8:
            case 9:
                a(sb, aVar, a(aVar, a.q(parcel, i)));
                return;
            case 10:
                a(sb, aVar, a(aVar, a(a.p(parcel, i))));
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown field out type = " + aVar.d());
        }
    }

    private void a(StringBuilder sb, ga.a aVar, Object obj) {
        if (aVar.c()) {
            a(sb, aVar, (ArrayList) obj);
        } else {
            a(sb, aVar.b(), obj);
        }
    }

    private void a(StringBuilder sb, ga.a aVar, ArrayList arrayList) {
        sb.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(",");
            }
            a(sb, aVar.b(), arrayList.get(i));
        }
        sb.append("]");
    }

    private void a(StringBuilder sb, String str, ga.a aVar, Parcel parcel, int i) {
        sb.append("\"").append(str).append("\":");
        if (aVar.j()) {
            a(sb, aVar, parcel, i);
        } else {
            b(sb, aVar, parcel, i);
        }
    }

    private void a(StringBuilder sb, HashMap hashMap, Parcel parcel) {
        HashMap a2 = a(hashMap);
        sb.append('{');
        int b2 = a.b(parcel);
        boolean z = false;
        while (parcel.dataPosition() < b2) {
            int a3 = a.a(parcel);
            Map.Entry entry = (Map.Entry) a2.get(Integer.valueOf(a.a(a3)));
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                a(sb, (String) entry.getKey(), (ga.a) entry.getValue(), parcel, a3);
                z = true;
            }
        }
        if (parcel.dataPosition() != b2) {
            throw new b("Overread allowed size end=" + b2, parcel);
        }
        sb.append('}');
    }

    private void b(StringBuilder sb, ga.a aVar, Parcel parcel, int i) {
        if (aVar.e()) {
            sb.append("[");
            switch (aVar.d()) {
                case 0:
                    bh.a(sb, a.t(parcel, i));
                    break;
                case 1:
                    bh.a(sb, a.v(parcel, i));
                    break;
                case 2:
                    bh.a(sb, a.u(parcel, i));
                    break;
                case 3:
                    bh.a(sb, a.w(parcel, i));
                    break;
                case 4:
                    bh.a(sb, a.x(parcel, i));
                    break;
                case 5:
                    bh.a(sb, a.y(parcel, i));
                    break;
                case 6:
                    bh.a(sb, a.s(parcel, i));
                    break;
                case 7:
                    bh.a(sb, a.z(parcel, i));
                    break;
                case 8:
                case 9:
                case 10:
                    throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                case 11:
                    Parcel[] C = a.C(parcel, i);
                    int length = C.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        if (i2 > 0) {
                            sb.append(",");
                        }
                        C[i2].setDataPosition(0);
                        a(sb, aVar.l(), C[i2]);
                    }
                    break;
                default:
                    throw new IllegalStateException("Unknown field type out.");
            }
            sb.append("]");
            return;
        }
        switch (aVar.d()) {
            case 0:
                sb.append(a.g(parcel, i));
                return;
            case 1:
                sb.append(a.j(parcel, i));
                return;
            case 2:
                sb.append(a.i(parcel, i));
                return;
            case 3:
                sb.append(a.k(parcel, i));
                return;
            case 4:
                sb.append(a.l(parcel, i));
                return;
            case 5:
                sb.append(a.m(parcel, i));
                return;
            case 6:
                sb.append(a.c(parcel, i));
                return;
            case 7:
                sb.append("\"").append(bn.a(a.n(parcel, i))).append("\"");
                return;
            case 8:
                sb.append("\"").append(bi.a(a.q(parcel, i))).append("\"");
                return;
            case 9:
                sb.append("\"").append(bi.b(a.q(parcel, i)));
                sb.append("\"");
                return;
            case 10:
                Bundle p = a.p(parcel, i);
                Set<String> keySet = p.keySet();
                keySet.size();
                sb.append("{");
                boolean z = true;
                for (String str : keySet) {
                    if (!z) {
                        sb.append(",");
                    }
                    sb.append("\"").append(str).append("\"");
                    sb.append(":");
                    sb.append("\"").append(bn.a(p.getString(str))).append("\"");
                    z = false;
                }
                sb.append("}");
                return;
            case 11:
                Parcel B = a.B(parcel, i);
                B.setDataPosition(0);
                a(sb, aVar.l(), B);
                return;
            default:
                throw new IllegalStateException("Unknown field type out");
        }
    }

    public int a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public Object a(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    @Override // com.google.android.gms.internal.ga
    public HashMap b() {
        if (this.e == null) {
            return null;
        }
        return this.e.a(this.f);
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public boolean b(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public int describeContents() {
        bg bgVar = a;
        return 0;
    }

    public Parcel e() {
        switch (this.g) {
            case 0:
                this.h = c.a(this.c);
                c.a(this.c, this.h);
                this.g = 2;
                break;
            case 1:
                c.a(this.c, this.h);
                this.g = 2;
                break;
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public gd f() {
        switch (this.d) {
            case 0:
                return null;
            case 1:
                return this.e;
            case 2:
                return this.e;
            default:
                throw new IllegalStateException("Invalid creation type: " + this.d);
        }
    }

    @Override // com.google.android.gms.internal.ga
    public String toString() {
        aw.a(this.e, "Cannot convert to JSON on client side.");
        Parcel e2 = e();
        e2.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        a(sb, this.e.a(this.f), e2);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        bg bgVar = a;
        bg.a(this, parcel, i);
    }
}
