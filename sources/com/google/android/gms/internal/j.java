package com.google.android.gms.internal;

public final class j extends dv {
    private static volatile j[] f;
    public int[] a;
    public int b;
    public int c;
    public boolean d;
    public boolean e;

    public j() {
        c();
    }

    public static j[] a() {
        if (f == null) {
            synchronized (dy.a) {
                if (f == null) {
                    f = new j[0];
                }
            }
        }
        return f;
    }

    /* renamed from: a */
    public j b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.e = dsVar.f();
                    break;
                case 16:
                    this.b = dsVar.e();
                    break;
                case 24:
                    int a3 = ed.a(dsVar, 24);
                    int length = this.a == null ? 0 : this.a.length;
                    int[] iArr = new int[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.a, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = dsVar.e();
                        dsVar.a();
                        length++;
                    }
                    iArr[length] = dsVar.e();
                    this.a = iArr;
                    break;
                case 26:
                    int c2 = dsVar.c(dsVar.i());
                    int o = dsVar.o();
                    int i = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i++;
                    }
                    dsVar.e(o);
                    int length2 = this.a == null ? 0 : this.a.length;
                    int[] iArr2 = new int[(i + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.a, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = dsVar.e();
                        length2++;
                    }
                    this.a = iArr2;
                    dsVar.d(c2);
                    break;
                case 32:
                    this.c = dsVar.e();
                    break;
                case 48:
                    this.d = dsVar.f();
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (this.e) {
            dtVar.a(1, this.e);
        }
        dtVar.a(2, this.b);
        if (this.a != null && this.a.length > 0) {
            for (int i = 0; i < this.a.length; i++) {
                dtVar.a(3, this.a[i]);
            }
        }
        if (this.c != 0) {
            dtVar.a(4, this.c);
        }
        if (this.d) {
            dtVar.a(6, this.d);
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int i;
        int i2 = 0;
        int b2 = super.b();
        if (this.e) {
            b2 += dt.b(1, this.e);
        }
        int b3 = dt.b(2, this.b) + b2;
        if (this.a == null || this.a.length <= 0) {
            i = b3;
        } else {
            for (int i3 = 0; i3 < this.a.length; i3++) {
                i2 += dt.b(this.a[i3]);
            }
            i = b3 + i2 + (this.a.length * 1);
        }
        if (this.c != 0) {
            i += dt.b(4, this.c);
        }
        if (this.d) {
            i += dt.b(6, this.d);
        }
        this.s = i;
        return i;
    }

    public j c() {
        this.a = ed.a;
        this.b = 0;
        this.c = 0;
        this.d = false;
        this.e = false;
        this.r = null;
        this.s = -1;
        return this;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        if (dy.a(this.a, jVar.a) && this.b == jVar.b && this.c == jVar.c && this.d == jVar.d && this.e == jVar.e) {
            return (this.r == null || this.r.isEmpty()) ? jVar.r == null || jVar.r.isEmpty() : this.r.equals(jVar.r);
        }
        return false;
    }

    public int hashCode() {
        int i = 1231;
        int a2 = ((this.d ? 1231 : 1237) + ((((((dy.a(this.a) + 527) * 31) + this.b) * 31) + this.c) * 31)) * 31;
        if (!this.e) {
            i = 1237;
        }
        return ((this.r == null || this.r.isEmpty()) ? 0 : this.r.hashCode()) + ((a2 + i) * 31);
    }
}
