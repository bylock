package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class dp implements Parcelable.Creator {
    static void a(jy jyVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, jyVar.a());
        c.a(parcel, 2, jyVar.a, false);
        c.a(parcel, 3, jyVar.b, false);
        c.a(parcel, 4, (Parcelable) jyVar.c, i, false);
        c.a(parcel, 5, (Parcelable) jyVar.d, i, false);
        c.a(parcel, 6, (Parcelable) jyVar.e, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public jy createFromParcel(Parcel parcel) {
        jw jwVar = null;
        int b = a.b(parcel);
        int i = 0;
        jw jwVar2 = null;
        ju juVar = null;
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    str = a.n(parcel, a);
                    break;
                case 4:
                    juVar = (ju) a.a(parcel, a, ju.CREATOR);
                    break;
                case 5:
                    jwVar2 = (jw) a.a(parcel, a, jw.CREATOR);
                    break;
                case 6:
                    jwVar = (jw) a.a(parcel, a, jw.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new jy(i, str2, str, juVar, jwVar2, jwVar);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public jy[] newArray(int i) {
        return new jy[i];
    }
}
