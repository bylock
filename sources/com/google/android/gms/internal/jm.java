package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.ArrayList;

public final class jm implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dj();
    String a;
    String b;
    ArrayList c;
    private final int d;

    jm() {
        this.d = 1;
        this.c = bh.a();
    }

    jm(int i, String str, String str2, ArrayList arrayList) {
        this.d = i;
        this.a = str;
        this.b = str2;
        this.c = arrayList;
    }

    public int a() {
        return this.d;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        dj.a(this, parcel, i);
    }
}
