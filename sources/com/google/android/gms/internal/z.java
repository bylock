package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.b.k;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;

public class z implements Parcelable.Creator {
    static void a(cz czVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, czVar.b);
        c.a(parcel, 2, czVar.c, false);
        c.a(parcel, 3, czVar.d, false);
        c.a(parcel, 4, czVar.e, false);
        c.a(parcel, 5, czVar.f);
        c.a(parcel, 6, czVar.g, false);
        c.a(parcel, 7, czVar.h);
        c.a(parcel, 8, czVar.i);
        c.a(parcel, 9, czVar.j);
        c.a(parcel, 10, czVar.k, false);
        c.a(parcel, 11, czVar.l);
        c.a(parcel, 12, czVar.m);
        c.a(parcel, 13, czVar.n, false);
        c.a(parcel, 14, czVar.o);
        c.a(parcel, 15, czVar.p, false);
        c.a(parcel, 19, czVar.r, false);
        c.a(parcel, 18, czVar.q);
        c.a(parcel, 21, czVar.s, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public cz createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        String str2 = null;
        ArrayList arrayList = null;
        int i2 = 0;
        ArrayList arrayList2 = null;
        long j = 0;
        boolean z = false;
        long j2 = 0;
        ArrayList arrayList3 = null;
        long j3 = 0;
        int i3 = 0;
        String str3 = null;
        long j4 = 0;
        String str4 = null;
        boolean z2 = false;
        String str5 = null;
        String str6 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    str2 = a.n(parcel, a);
                    break;
                case 4:
                    arrayList = a.A(parcel, a);
                    break;
                case 5:
                    i2 = a.g(parcel, a);
                    break;
                case 6:
                    arrayList2 = a.A(parcel, a);
                    break;
                case 7:
                    j = a.i(parcel, a);
                    break;
                case 8:
                    z = a.c(parcel, a);
                    break;
                case 9:
                    j2 = a.i(parcel, a);
                    break;
                case 10:
                    arrayList3 = a.A(parcel, a);
                    break;
                case 11:
                    j3 = a.i(parcel, a);
                    break;
                case 12:
                    i3 = a.g(parcel, a);
                    break;
                case 13:
                    str3 = a.n(parcel, a);
                    break;
                case 14:
                    j4 = a.i(parcel, a);
                    break;
                case 15:
                    str4 = a.n(parcel, a);
                    break;
                case 16:
                case k.ActionBar_progressBarPadding:
                case 20:
                default:
                    a.b(parcel, a);
                    break;
                case k.ActionBar_itemPadding:
                    z2 = a.c(parcel, a);
                    break;
                case 19:
                    str5 = a.n(parcel, a);
                    break;
                case 21:
                    str6 = a.n(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new cz(i, str, str2, arrayList, i2, arrayList2, j, z, j2, arrayList3, j3, i3, str3, j4, str4, z2, str5, str6);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public cz[] newArray(int i) {
        return new cz[i];
    }
}
