package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class kk implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dr();
    final int a;
    private final String b;
    private final String c;

    kk(int i, String str, String str2) {
        this.a = i;
        this.b = str;
        this.c = str2;
    }

    public String a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof kk)) {
            return false;
        }
        kk kkVar = (kk) obj;
        return kkVar.b.equals(this.b) && kkVar.c.equals(this.c);
    }

    public int hashCode() {
        return ((this.b.hashCode() + 629) * 37) + this.c.hashCode();
    }

    public String toString() {
        return "NodeParcelable{" + this.b + "," + this.c + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        dr.a(this, parcel, i);
    }
}
