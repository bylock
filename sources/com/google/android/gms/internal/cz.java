package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Collections;
import java.util.List;

public final class cz implements SafeParcelable {
    public static final z a = new z();
    public final int b;
    public final String c;
    public final String d;
    public final List e;
    public final int f;
    public final List g;
    public final long h;
    public final boolean i;
    public final long j;
    public final List k;
    public final long l;
    public final int m;
    public final String n;
    public final long o;
    public final String p;
    public final boolean q;
    public final String r;
    public final String s;

    cz(int i2, String str, String str2, List list, int i3, List list2, long j2, boolean z, long j3, List list3, long j4, int i4, String str3, long j5, String str4, boolean z2, String str5, String str6) {
        this.b = i2;
        this.c = str;
        this.d = str2;
        this.e = list != null ? Collections.unmodifiableList(list) : null;
        this.f = i3;
        this.g = list2 != null ? Collections.unmodifiableList(list2) : null;
        this.h = j2;
        this.i = z;
        this.j = j3;
        this.k = list3 != null ? Collections.unmodifiableList(list3) : null;
        this.l = j4;
        this.m = i4;
        this.n = str3;
        this.o = j5;
        this.p = str4;
        this.q = z2;
        this.r = str5;
        this.s = str6;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        z.a(this, parcel, i2);
    }
}
