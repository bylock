package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class iv implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dg();
    int[] a;
    private final int b;

    iv() {
        this(1, null);
    }

    iv(int i, int[] iArr) {
        this.b = i;
        this.a = iArr;
    }

    public int a() {
        return this.b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        dg.a(this, parcel, i);
    }
}
