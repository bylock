package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class jo implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dl();
    String a;
    jp b;
    String c;
    ju d;
    private final int e;

    jo() {
        this.e = 1;
    }

    jo(int i, String str, jp jpVar, String str2, ju juVar) {
        this.e = i;
        this.a = str;
        this.b = jpVar;
        this.c = str2;
        this.d = juVar;
    }

    public int a() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        dl.a(this, parcel, i);
    }
}
