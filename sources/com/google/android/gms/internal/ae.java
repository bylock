package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

final class ae implements Parcelable.Creator {
    ae() {
    }

    @Deprecated
    /* renamed from: a */
    public ef createFromParcel(Parcel parcel) {
        return new ef(parcel);
    }

    @Deprecated
    /* renamed from: a */
    public ef[] newArray(int i) {
        return new ef[i];
    }
}
