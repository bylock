package com.google.android.gms.internal;

public final class ds {
    private final byte[] a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g = Integer.MAX_VALUE;
    private int h;
    private int i = 64;
    private int j = 67108864;

    private ds(byte[] bArr, int i2, int i3) {
        this.a = bArr;
        this.b = i2;
        this.c = i2 + i3;
        this.e = i2;
    }

    public static long a(long j2) {
        return (j2 >>> 1) ^ (-(1 & j2));
    }

    public static ds a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public static ds a(byte[] bArr, int i2, int i3) {
        return new ds(bArr, i2, i3);
    }

    private void q() {
        this.c += this.d;
        int i2 = this.c;
        if (i2 > this.g) {
            this.d = i2 - this.g;
            this.c -= this.d;
            return;
        }
        this.d = 0;
    }

    public int a() {
        if (n()) {
            this.f = 0;
            return 0;
        }
        this.f = i();
        if (this.f != 0) {
            return this.f;
        }
        throw dz.d();
    }

    public void a(int i2) {
        if (this.f != i2) {
            throw dz.e();
        }
    }

    public void a(ea eaVar) {
        int i2 = i();
        if (this.h >= this.i) {
            throw dz.g();
        }
        int c2 = c(i2);
        this.h++;
        eaVar.b(this);
        a(0);
        this.h--;
        d(c2);
    }

    public void a(ea eaVar, int i2) {
        if (this.h >= this.i) {
            throw dz.g();
        }
        this.h++;
        eaVar.b(this);
        a(ed.a(i2, 4));
        this.h--;
    }

    public byte[] a(int i2, int i3) {
        if (i3 == 0) {
            return ed.h;
        }
        byte[] bArr = new byte[i3];
        System.arraycopy(this.a, this.b + i2, bArr, 0, i3);
        return bArr;
    }

    public void b() {
        int a2;
        do {
            a2 = a();
            if (a2 == 0) {
                return;
            }
        } while (b(a2));
    }

    public boolean b(int i2) {
        switch (ed.a(i2)) {
            case 0:
                e();
                return true;
            case 1:
                l();
                return true;
            case 2:
                g(i());
                return true;
            case 3:
                b();
                a(ed.a(ed.b(i2), 4));
                return true;
            case 4:
                return false;
            case 5:
                k();
                return true;
            default:
                throw dz.f();
        }
    }

    public float c() {
        return Float.intBitsToFloat(k());
    }

    public int c(int i2) {
        if (i2 < 0) {
            throw dz.b();
        }
        int i3 = this.e + i2;
        int i4 = this.g;
        if (i3 > i4) {
            throw dz.a();
        }
        this.g = i3;
        q();
        return i4;
    }

    public long d() {
        return j();
    }

    public void d(int i2) {
        this.g = i2;
        q();
    }

    public int e() {
        return i();
    }

    public void e(int i2) {
        if (i2 > this.e - this.b) {
            throw new IllegalArgumentException("Position " + i2 + " is beyond current " + (this.e - this.b));
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Bad position " + i2);
        } else {
            this.e = this.b + i2;
        }
    }

    public boolean f() {
        return i() != 0;
    }

    public byte[] f(int i2) {
        if (i2 < 0) {
            throw dz.b();
        } else if (this.e + i2 > this.g) {
            g(this.g - this.e);
            throw dz.a();
        } else if (i2 <= this.c - this.e) {
            byte[] bArr = new byte[i2];
            System.arraycopy(this.a, this.e, bArr, 0, i2);
            this.e += i2;
            return bArr;
        } else {
            throw dz.a();
        }
    }

    public String g() {
        int i2 = i();
        if (i2 > this.c - this.e || i2 <= 0) {
            return new String(f(i2), "UTF-8");
        }
        String str = new String(this.a, this.e, i2, "UTF-8");
        this.e = i2 + this.e;
        return str;
    }

    public void g(int i2) {
        if (i2 < 0) {
            throw dz.b();
        } else if (this.e + i2 > this.g) {
            g(this.g - this.e);
            throw dz.a();
        } else if (i2 <= this.c - this.e) {
            this.e += i2;
        } else {
            throw dz.a();
        }
    }

    public long h() {
        return a(j());
    }

    public int i() {
        byte p = p();
        if (p >= 0) {
            return p;
        }
        int i2 = p & Byte.MAX_VALUE;
        byte p2 = p();
        if (p2 >= 0) {
            return i2 | (p2 << 7);
        }
        int i3 = i2 | ((p2 & Byte.MAX_VALUE) << 7);
        byte p3 = p();
        if (p3 >= 0) {
            return i3 | (p3 << 14);
        }
        int i4 = i3 | ((p3 & Byte.MAX_VALUE) << 14);
        byte p4 = p();
        if (p4 >= 0) {
            return i4 | (p4 << 21);
        }
        int i5 = i4 | ((p4 & Byte.MAX_VALUE) << 21);
        byte p5 = p();
        int i6 = i5 | (p5 << 28);
        if (p5 >= 0) {
            return i6;
        }
        for (int i7 = 0; i7 < 5; i7++) {
            if (p() >= 0) {
                return i6;
            }
        }
        throw dz.c();
    }

    public long j() {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte p = p();
            j2 |= ((long) (p & Byte.MAX_VALUE)) << i2;
            if ((p & 128) == 0) {
                return j2;
            }
        }
        throw dz.c();
    }

    public int k() {
        return (p() & 255) | ((p() & 255) << 8) | ((p() & 255) << 16) | ((p() & 255) << 24);
    }

    public long l() {
        byte p = p();
        byte p2 = p();
        return ((((long) p2) & 255) << 8) | (((long) p) & 255) | ((((long) p()) & 255) << 16) | ((((long) p()) & 255) << 24) | ((((long) p()) & 255) << 32) | ((((long) p()) & 255) << 40) | ((((long) p()) & 255) << 48) | ((((long) p()) & 255) << 56);
    }

    public int m() {
        if (this.g == Integer.MAX_VALUE) {
            return -1;
        }
        return this.g - this.e;
    }

    public boolean n() {
        return this.e == this.c;
    }

    public int o() {
        return this.e - this.b;
    }

    public byte p() {
        if (this.e == this.c) {
            throw dz.a();
        }
        byte[] bArr = this.a;
        int i2 = this.e;
        this.e = i2 + 1;
        return bArr[i2];
    }
}
