package com.google.android.gms.internal;

import android.support.v7.b.k;

public final class l extends dv {
    public y[] a;
    public y[] b;
    public k[] c;

    public l() {
        a();
    }

    public l a() {
        this.a = y.a();
        this.b = y.a();
        this.c = k.a();
        this.r = null;
        this.s = -1;
        return this;
    }

    /* renamed from: a */
    public l b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    int a3 = ed.a(dsVar, 10);
                    int length = this.a == null ? 0 : this.a.length;
                    y[] yVarArr = new y[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.a, 0, yVarArr, 0, length);
                    }
                    while (length < yVarArr.length - 1) {
                        yVarArr[length] = new y();
                        dsVar.a(yVarArr[length]);
                        dsVar.a();
                        length++;
                    }
                    yVarArr[length] = new y();
                    dsVar.a(yVarArr[length]);
                    this.a = yVarArr;
                    break;
                case k.ActionBar_itemPadding:
                    int a4 = ed.a(dsVar, 18);
                    int length2 = this.b == null ? 0 : this.b.length;
                    y[] yVarArr2 = new y[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.b, 0, yVarArr2, 0, length2);
                    }
                    while (length2 < yVarArr2.length - 1) {
                        yVarArr2[length2] = new y();
                        dsVar.a(yVarArr2[length2]);
                        dsVar.a();
                        length2++;
                    }
                    yVarArr2[length2] = new y();
                    dsVar.a(yVarArr2[length2]);
                    this.b = yVarArr2;
                    break;
                case 26:
                    int a5 = ed.a(dsVar, 26);
                    int length3 = this.c == null ? 0 : this.c.length;
                    k[] kVarArr = new k[(a5 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.c, 0, kVarArr, 0, length3);
                    }
                    while (length3 < kVarArr.length - 1) {
                        kVarArr[length3] = new k();
                        dsVar.a(kVarArr[length3]);
                        dsVar.a();
                        length3++;
                    }
                    kVarArr[length3] = new k();
                    dsVar.a(kVarArr[length3]);
                    this.c = kVarArr;
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (this.a != null && this.a.length > 0) {
            for (int i = 0; i < this.a.length; i++) {
                y yVar = this.a[i];
                if (yVar != null) {
                    dtVar.a(1, yVar);
                }
            }
        }
        if (this.b != null && this.b.length > 0) {
            for (int i2 = 0; i2 < this.b.length; i2++) {
                y yVar2 = this.b[i2];
                if (yVar2 != null) {
                    dtVar.a(2, yVar2);
                }
            }
        }
        if (this.c != null && this.c.length > 0) {
            for (int i3 = 0; i3 < this.c.length; i3++) {
                k kVar = this.c[i3];
                if (kVar != null) {
                    dtVar.a(3, kVar);
                }
            }
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b();
        if (this.a != null && this.a.length > 0) {
            int i = b2;
            for (int i2 = 0; i2 < this.a.length; i2++) {
                y yVar = this.a[i2];
                if (yVar != null) {
                    i += dt.b(1, yVar);
                }
            }
            b2 = i;
        }
        if (this.b != null && this.b.length > 0) {
            int i3 = b2;
            for (int i4 = 0; i4 < this.b.length; i4++) {
                y yVar2 = this.b[i4];
                if (yVar2 != null) {
                    i3 += dt.b(2, yVar2);
                }
            }
            b2 = i3;
        }
        if (this.c != null && this.c.length > 0) {
            for (int i5 = 0; i5 < this.c.length; i5++) {
                k kVar = this.c[i5];
                if (kVar != null) {
                    b2 += dt.b(3, kVar);
                }
            }
        }
        this.s = b2;
        return b2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        if (!dy.a(this.a, lVar.a) || !dy.a(this.b, lVar.b) || !dy.a(this.c, lVar.c)) {
            return false;
        }
        return (this.r == null || this.r.isEmpty()) ? lVar.r == null || lVar.r.isEmpty() : this.r.equals(lVar.r);
    }

    public int hashCode() {
        return ((this.r == null || this.r.isEmpty()) ? 0 : this.r.hashCode()) + ((((((dy.a(this.a) + 527) * 31) + dy.a(this.b)) * 31) + dy.a(this.c)) * 31);
    }
}
