package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class dx implements SafeParcelable {
    public static final ab a = new ab();
    public final int b;
    public String c;
    public int d;
    public int e;
    public boolean f;

    dx(int i, String str, int i2, int i3, boolean z) {
        this.b = i;
        this.c = str;
        this.d = i2;
        this.e = i3;
        this.f = z;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ab.a(this, parcel, i);
    }
}
