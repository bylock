package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class s implements Parcelable.Creator {
    static void a(cb cbVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, cbVar.b);
        c.a(parcel, 2, cbVar.c, false);
        c.a(parcel, 3, cbVar.d, false);
        c.a(parcel, 4, cbVar.e, false);
        c.a(parcel, 5, cbVar.f, false);
        c.a(parcel, 6, cbVar.g, false);
        c.a(parcel, 7, cbVar.h, false);
        c.a(parcel, 8, cbVar.i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public cb createFromParcel(Parcel parcel) {
        String str = null;
        int b = a.b(parcel);
        int i = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str7 = a.n(parcel, a);
                    break;
                case 3:
                    str6 = a.n(parcel, a);
                    break;
                case 4:
                    str5 = a.n(parcel, a);
                    break;
                case 5:
                    str4 = a.n(parcel, a);
                    break;
                case 6:
                    str3 = a.n(parcel, a);
                    break;
                case 7:
                    str2 = a.n(parcel, a);
                    break;
                case 8:
                    str = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new cb(i, str7, str6, str5, str4, str3, str2, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public cb[] newArray(int i) {
        return new cb[i];
    }
}
