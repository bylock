package com.google.android.gms.internal;

import android.os.Parcel;
import android.support.v7.b.k;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ga;
import com.google.android.gms.plus.a.b.e;
import com.google.android.gms.plus.a.b.i;
import com.google.android.gms.plus.a.b.j;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class ih extends ga implements SafeParcelable, com.google.android.gms.plus.a.b.a {
    public static final cs a = new cs();
    private static final HashMap b = new HashMap();
    private List A;
    private boolean B;
    private final Set c;
    private final int d;
    private String e;
    private a f;
    private String g;
    private String h;
    private int i;
    private b j;
    private String k;
    private String l;
    private int m;
    private String n;
    private c o;
    private boolean p;
    private String q;
    private d r;
    private String s;
    private int t;
    private List u;
    private List v;
    private int w;
    private int x;
    private String y;
    private String z;

    public final class a extends ga implements SafeParcelable, com.google.android.gms.plus.a.b.b {
        public static final ct a = new ct();
        private static final HashMap b = new HashMap();
        private final Set c;
        private final int d;
        private int e;
        private int f;

        static {
            b.put("max", ga.a.a("max", 2));
            b.put("min", ga.a.a("min", 3));
        }

        public a() {
            this.d = 1;
            this.c = new HashSet();
        }

        a(Set set, int i, int i2, int i3) {
            this.c = set;
            this.d = i;
            this.e = i2;
            this.f = i3;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean a(ga.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object b(ga.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return Integer.valueOf(this.e);
                case 3:
                    return Integer.valueOf(this.f);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        @Override // com.google.android.gms.internal.ga
        public HashMap b() {
            return b;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            ct ctVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            a aVar = (a) obj;
            for (ga.a aVar2 : b.values()) {
                if (a(aVar2)) {
                    if (!aVar.a(aVar2)) {
                        return false;
                    }
                    if (!b(aVar2).equals(aVar.b(aVar2))) {
                        return false;
                    }
                } else if (aVar.a(aVar2)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public int g() {
            return this.e;
        }

        public int h() {
            return this.f;
        }

        public int hashCode() {
            int i = 0;
            for (ga.a aVar : b.values()) {
                i = a(aVar) ? b(aVar).hashCode() + i + aVar.g() : i;
            }
            return i;
        }

        /* renamed from: i */
        public a a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            ct ctVar = a;
            ct.a(this, parcel, i);
        }
    }

    public final class b extends ga implements SafeParcelable, com.google.android.gms.plus.a.b.c {
        public static final cu a = new cu();
        private static final HashMap b = new HashMap();
        private final Set c;
        private final int d;
        private a e;
        private C0000b f;
        private int g;

        public final class a extends ga implements SafeParcelable, com.google.android.gms.plus.a.b.d {
            public static final cv a = new cv();
            private static final HashMap b = new HashMap();
            private final Set c;
            private final int d;
            private int e;
            private int f;

            static {
                b.put("leftImageOffset", ga.a.a("leftImageOffset", 2));
                b.put("topImageOffset", ga.a.a("topImageOffset", 3));
            }

            public a() {
                this.d = 1;
                this.c = new HashSet();
            }

            a(Set set, int i, int i2, int i3) {
                this.c = set;
                this.d = i;
                this.e = i2;
                this.f = i3;
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.ga
            public Object a(String str) {
                return null;
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.ga
            public boolean a(ga.a aVar) {
                return this.c.contains(Integer.valueOf(aVar.g()));
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.ga
            public Object b(ga.a aVar) {
                switch (aVar.g()) {
                    case 2:
                        return Integer.valueOf(this.e);
                    case 3:
                        return Integer.valueOf(this.f);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
                }
            }

            @Override // com.google.android.gms.internal.ga
            public HashMap b() {
                return b;
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.ga
            public boolean b(String str) {
                return false;
            }

            public int describeContents() {
                cv cvVar = a;
                return 0;
            }

            /* access modifiers changed from: package-private */
            public Set e() {
                return this.c;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof a)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                a aVar = (a) obj;
                for (ga.a aVar2 : b.values()) {
                    if (a(aVar2)) {
                        if (!aVar.a(aVar2)) {
                            return false;
                        }
                        if (!b(aVar2).equals(aVar.b(aVar2))) {
                            return false;
                        }
                    } else if (aVar.a(aVar2)) {
                        return false;
                    }
                }
                return true;
            }

            /* access modifiers changed from: package-private */
            public int f() {
                return this.d;
            }

            public int g() {
                return this.e;
            }

            public int h() {
                return this.f;
            }

            public int hashCode() {
                int i = 0;
                for (ga.a aVar : b.values()) {
                    i = a(aVar) ? b(aVar).hashCode() + i + aVar.g() : i;
                }
                return i;
            }

            /* renamed from: i */
            public a a() {
                return this;
            }

            public void writeToParcel(Parcel parcel, int i) {
                cv cvVar = a;
                cv.a(this, parcel, i);
            }
        }

        /* renamed from: com.google.android.gms.internal.ih$b$b  reason: collision with other inner class name */
        public final class C0000b extends ga implements SafeParcelable, e {
            public static final cw a = new cw();
            private static final HashMap b = new HashMap();
            private final Set c;
            private final int d;
            private int e;
            private String f;
            private int g;

            static {
                b.put("height", ga.a.a("height", 2));
                b.put("url", ga.a.d("url", 3));
                b.put("width", ga.a.a("width", 4));
            }

            public C0000b() {
                this.d = 1;
                this.c = new HashSet();
            }

            C0000b(Set set, int i, int i2, String str, int i3) {
                this.c = set;
                this.d = i;
                this.e = i2;
                this.f = str;
                this.g = i3;
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.ga
            public Object a(String str) {
                return null;
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.ga
            public boolean a(ga.a aVar) {
                return this.c.contains(Integer.valueOf(aVar.g()));
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.ga
            public Object b(ga.a aVar) {
                switch (aVar.g()) {
                    case 2:
                        return Integer.valueOf(this.e);
                    case 3:
                        return this.f;
                    case 4:
                        return Integer.valueOf(this.g);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
                }
            }

            @Override // com.google.android.gms.internal.ga
            public HashMap b() {
                return b;
            }

            /* access modifiers changed from: protected */
            @Override // com.google.android.gms.internal.ga
            public boolean b(String str) {
                return false;
            }

            public int describeContents() {
                cw cwVar = a;
                return 0;
            }

            /* access modifiers changed from: package-private */
            public Set e() {
                return this.c;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof C0000b)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                C0000b bVar = (C0000b) obj;
                for (ga.a aVar : b.values()) {
                    if (a(aVar)) {
                        if (!bVar.a(aVar)) {
                            return false;
                        }
                        if (!b(aVar).equals(bVar.b(aVar))) {
                            return false;
                        }
                    } else if (bVar.a(aVar)) {
                        return false;
                    }
                }
                return true;
            }

            /* access modifiers changed from: package-private */
            public int f() {
                return this.d;
            }

            public int g() {
                return this.e;
            }

            public String h() {
                return this.f;
            }

            public int hashCode() {
                int i = 0;
                for (ga.a aVar : b.values()) {
                    i = a(aVar) ? b(aVar).hashCode() + i + aVar.g() : i;
                }
                return i;
            }

            public int i() {
                return this.g;
            }

            /* renamed from: j */
            public C0000b a() {
                return this;
            }

            public void writeToParcel(Parcel parcel, int i) {
                cw cwVar = a;
                cw.a(this, parcel, i);
            }
        }

        static {
            b.put("coverInfo", ga.a.a("coverInfo", 2, a.class));
            b.put("coverPhoto", ga.a.a("coverPhoto", 3, C0000b.class));
            b.put("layout", ga.a.a("layout", 4, new fx().a("banner", 0), false));
        }

        public b() {
            this.d = 1;
            this.c = new HashSet();
        }

        b(Set set, int i, a aVar, C0000b bVar, int i2) {
            this.c = set;
            this.d = i;
            this.e = aVar;
            this.f = bVar;
            this.g = i2;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean a(ga.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object b(ga.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return this.e;
                case 3:
                    return this.f;
                case 4:
                    return Integer.valueOf(this.g);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        @Override // com.google.android.gms.internal.ga
        public HashMap b() {
            return b;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            cu cuVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            b bVar = (b) obj;
            for (ga.a aVar : b.values()) {
                if (a(aVar)) {
                    if (!bVar.a(aVar)) {
                        return false;
                    }
                    if (!b(aVar).equals(bVar.b(aVar))) {
                        return false;
                    }
                } else if (bVar.a(aVar)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public a g() {
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public C0000b h() {
            return this.f;
        }

        public int hashCode() {
            int i = 0;
            for (ga.a aVar : b.values()) {
                i = a(aVar) ? b(aVar).hashCode() + i + aVar.g() : i;
            }
            return i;
        }

        public int i() {
            return this.g;
        }

        /* renamed from: j */
        public b a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            cu cuVar = a;
            cu.a(this, parcel, i);
        }
    }

    public final class c extends ga implements SafeParcelable, com.google.android.gms.plus.a.b.f {
        public static final cy a = new cy();
        private static final HashMap b = new HashMap();
        private final Set c;
        private final int d;
        private String e;

        static {
            b.put("url", ga.a.d("url", 2));
        }

        public c() {
            this.d = 1;
            this.c = new HashSet();
        }

        c(Set set, int i, String str) {
            this.c = set;
            this.d = i;
            this.e = str;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean a(ga.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object b(ga.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return this.e;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        @Override // com.google.android.gms.internal.ga
        public HashMap b() {
            return b;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            cy cyVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            c cVar = (c) obj;
            for (ga.a aVar : b.values()) {
                if (a(aVar)) {
                    if (!cVar.a(aVar)) {
                        return false;
                    }
                    if (!b(aVar).equals(cVar.b(aVar))) {
                        return false;
                    }
                } else if (cVar.a(aVar)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public String g() {
            return this.e;
        }

        /* renamed from: h */
        public c a() {
            return this;
        }

        public int hashCode() {
            int i = 0;
            for (ga.a aVar : b.values()) {
                i = a(aVar) ? b(aVar).hashCode() + i + aVar.g() : i;
            }
            return i;
        }

        public void writeToParcel(Parcel parcel, int i) {
            cy cyVar = a;
            cy.a(this, parcel, i);
        }
    }

    public final class d extends ga implements SafeParcelable, com.google.android.gms.plus.a.b.g {
        public static final da a = new da();
        private static final HashMap b = new HashMap();
        private final Set c;
        private final int d;
        private String e;
        private String f;
        private String g;
        private String h;
        private String i;
        private String j;

        static {
            b.put("familyName", ga.a.d("familyName", 2));
            b.put("formatted", ga.a.d("formatted", 3));
            b.put("givenName", ga.a.d("givenName", 4));
            b.put("honorificPrefix", ga.a.d("honorificPrefix", 5));
            b.put("honorificSuffix", ga.a.d("honorificSuffix", 6));
            b.put("middleName", ga.a.d("middleName", 7));
        }

        public d() {
            this.d = 1;
            this.c = new HashSet();
        }

        d(Set set, int i2, String str, String str2, String str3, String str4, String str5, String str6) {
            this.c = set;
            this.d = i2;
            this.e = str;
            this.f = str2;
            this.g = str3;
            this.h = str4;
            this.i = str5;
            this.j = str6;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean a(ga.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object b(ga.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return this.e;
                case 3:
                    return this.f;
                case 4:
                    return this.g;
                case 5:
                    return this.h;
                case 6:
                    return this.i;
                case 7:
                    return this.j;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        @Override // com.google.android.gms.internal.ga
        public HashMap b() {
            return b;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            da daVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof d)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            d dVar = (d) obj;
            for (ga.a aVar : b.values()) {
                if (a(aVar)) {
                    if (!dVar.a(aVar)) {
                        return false;
                    }
                    if (!b(aVar).equals(dVar.b(aVar))) {
                        return false;
                    }
                } else if (dVar.a(aVar)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public String g() {
            return this.e;
        }

        public String h() {
            return this.f;
        }

        public int hashCode() {
            int i2 = 0;
            for (ga.a aVar : b.values()) {
                i2 = a(aVar) ? b(aVar).hashCode() + i2 + aVar.g() : i2;
            }
            return i2;
        }

        public String i() {
            return this.g;
        }

        public String j() {
            return this.h;
        }

        public String k() {
            return this.i;
        }

        public String l() {
            return this.j;
        }

        /* renamed from: m */
        public d a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            da daVar = a;
            da.a(this, parcel, i2);
        }
    }

    public final class f extends ga implements SafeParcelable, com.google.android.gms.plus.a.b.h {
        public static final db a = new db();
        private static final HashMap b = new HashMap();
        private final Set c;
        private final int d;
        private String e;
        private String f;
        private String g;
        private String h;
        private String i;
        private boolean j;
        private String k;
        private String l;
        private int m;

        static {
            b.put("department", ga.a.d("department", 2));
            b.put("description", ga.a.d("description", 3));
            b.put("endDate", ga.a.d("endDate", 4));
            b.put("location", ga.a.d("location", 5));
            b.put("name", ga.a.d("name", 6));
            b.put("primary", ga.a.c("primary", 7));
            b.put("startDate", ga.a.d("startDate", 8));
            b.put("title", ga.a.d("title", 9));
            b.put("type", ga.a.a("type", 10, new fx().a("work", 0).a("school", 1), false));
        }

        public f() {
            this.d = 1;
            this.c = new HashSet();
        }

        f(Set set, int i2, String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, int i3) {
            this.c = set;
            this.d = i2;
            this.e = str;
            this.f = str2;
            this.g = str3;
            this.h = str4;
            this.i = str5;
            this.j = z;
            this.k = str6;
            this.l = str7;
            this.m = i3;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean a(ga.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object b(ga.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return this.e;
                case 3:
                    return this.f;
                case 4:
                    return this.g;
                case 5:
                    return this.h;
                case 6:
                    return this.i;
                case 7:
                    return Boolean.valueOf(this.j);
                case 8:
                    return this.k;
                case 9:
                    return this.l;
                case 10:
                    return Integer.valueOf(this.m);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        @Override // com.google.android.gms.internal.ga
        public HashMap b() {
            return b;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            db dbVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof f)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            f fVar = (f) obj;
            for (ga.a aVar : b.values()) {
                if (a(aVar)) {
                    if (!fVar.a(aVar)) {
                        return false;
                    }
                    if (!b(aVar).equals(fVar.b(aVar))) {
                        return false;
                    }
                } else if (fVar.a(aVar)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public String g() {
            return this.e;
        }

        public String h() {
            return this.f;
        }

        public int hashCode() {
            int i2 = 0;
            for (ga.a aVar : b.values()) {
                i2 = a(aVar) ? b(aVar).hashCode() + i2 + aVar.g() : i2;
            }
            return i2;
        }

        public String i() {
            return this.g;
        }

        public String j() {
            return this.h;
        }

        public String k() {
            return this.i;
        }

        public boolean l() {
            return this.j;
        }

        public String m() {
            return this.k;
        }

        public String n() {
            return this.l;
        }

        public int o() {
            return this.m;
        }

        /* renamed from: p */
        public f a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            db dbVar = a;
            db.a(this, parcel, i2);
        }
    }

    public final class g extends ga implements SafeParcelable, i {
        public static final dc a = new dc();
        private static final HashMap b = new HashMap();
        private final Set c;
        private final int d;
        private boolean e;
        private String f;

        static {
            b.put("primary", ga.a.c("primary", 2));
            b.put("value", ga.a.d("value", 3));
        }

        public g() {
            this.d = 1;
            this.c = new HashSet();
        }

        g(Set set, int i, boolean z, String str) {
            this.c = set;
            this.d = i;
            this.e = z;
            this.f = str;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean a(ga.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object b(ga.a aVar) {
            switch (aVar.g()) {
                case 2:
                    return Boolean.valueOf(this.e);
                case 3:
                    return this.f;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        @Override // com.google.android.gms.internal.ga
        public HashMap b() {
            return b;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            dc dcVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof g)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            g gVar = (g) obj;
            for (ga.a aVar : b.values()) {
                if (a(aVar)) {
                    if (!gVar.a(aVar)) {
                        return false;
                    }
                    if (!b(aVar).equals(gVar.b(aVar))) {
                        return false;
                    }
                } else if (gVar.a(aVar)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public boolean g() {
            return this.e;
        }

        public String h() {
            return this.f;
        }

        public int hashCode() {
            int i = 0;
            for (ga.a aVar : b.values()) {
                i = a(aVar) ? b(aVar).hashCode() + i + aVar.g() : i;
            }
            return i;
        }

        /* renamed from: i */
        public g a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            dc dcVar = a;
            dc.a(this, parcel, i);
        }
    }

    public final class h extends ga implements SafeParcelable, j {
        public static final dd a = new dd();
        private static final HashMap b = new HashMap();
        private final Set c;
        private final int d;
        private String e;
        private final int f;
        private int g;
        private String h;

        static {
            b.put("label", ga.a.d("label", 5));
            b.put("type", ga.a.a("type", 6, new fx().a("home", 0).a("work", 1).a("blog", 2).a("profile", 3).a("other", 4).a("otherProfile", 5).a("contributor", 6).a("website", 7), false));
            b.put("value", ga.a.d("value", 4));
        }

        public h() {
            this.f = 4;
            this.d = 2;
            this.c = new HashSet();
        }

        h(Set set, int i, String str, int i2, String str2, int i3) {
            this.f = 4;
            this.c = set;
            this.d = i;
            this.e = str;
            this.g = i2;
            this.h = str2;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object a(String str) {
            return null;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean a(ga.a aVar) {
            return this.c.contains(Integer.valueOf(aVar.g()));
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public Object b(ga.a aVar) {
            switch (aVar.g()) {
                case 4:
                    return this.h;
                case 5:
                    return this.e;
                case 6:
                    return Integer.valueOf(this.g);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            }
        }

        @Override // com.google.android.gms.internal.ga
        public HashMap b() {
            return b;
        }

        /* access modifiers changed from: protected */
        @Override // com.google.android.gms.internal.ga
        public boolean b(String str) {
            return false;
        }

        public int describeContents() {
            dd ddVar = a;
            return 0;
        }

        /* access modifiers changed from: package-private */
        public Set e() {
            return this.c;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof h)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            h hVar = (h) obj;
            for (ga.a aVar : b.values()) {
                if (a(aVar)) {
                    if (!hVar.a(aVar)) {
                        return false;
                    }
                    if (!b(aVar).equals(hVar.b(aVar))) {
                        return false;
                    }
                } else if (hVar.a(aVar)) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public int f() {
            return this.d;
        }

        public String g() {
            return this.e;
        }

        @Deprecated
        public int h() {
            return 4;
        }

        public int hashCode() {
            int i = 0;
            for (ga.a aVar : b.values()) {
                i = a(aVar) ? b(aVar).hashCode() + i + aVar.g() : i;
            }
            return i;
        }

        public int i() {
            return this.g;
        }

        public String j() {
            return this.h;
        }

        /* renamed from: k */
        public h a() {
            return this;
        }

        public void writeToParcel(Parcel parcel, int i) {
            dd ddVar = a;
            dd.a(this, parcel, i);
        }
    }

    static {
        b.put("aboutMe", ga.a.d("aboutMe", 2));
        b.put("ageRange", ga.a.a("ageRange", 3, a.class));
        b.put("birthday", ga.a.d("birthday", 4));
        b.put("braggingRights", ga.a.d("braggingRights", 5));
        b.put("circledByCount", ga.a.a("circledByCount", 6));
        b.put("cover", ga.a.a("cover", 7, b.class));
        b.put("currentLocation", ga.a.d("currentLocation", 8));
        b.put("displayName", ga.a.d("displayName", 9));
        b.put("gender", ga.a.a("gender", 12, new fx().a("male", 0).a("female", 1).a("other", 2), false));
        b.put("id", ga.a.d("id", 14));
        b.put("image", ga.a.a("image", 15, c.class));
        b.put("isPlusUser", ga.a.c("isPlusUser", 16));
        b.put("language", ga.a.d("language", 18));
        b.put("name", ga.a.a("name", 19, d.class));
        b.put("nickname", ga.a.d("nickname", 20));
        b.put("objectType", ga.a.a("objectType", 21, new fx().a("person", 0).a("page", 1), false));
        b.put("organizations", ga.a.b("organizations", 22, f.class));
        b.put("placesLived", ga.a.b("placesLived", 23, g.class));
        b.put("plusOneCount", ga.a.a("plusOneCount", 24));
        b.put("relationshipStatus", ga.a.a("relationshipStatus", 25, new fx().a("single", 0).a("in_a_relationship", 1).a("engaged", 2).a("married", 3).a("its_complicated", 4).a("open_relationship", 5).a("widowed", 6).a("in_domestic_partnership", 7).a("in_civil_union", 8), false));
        b.put("tagline", ga.a.d("tagline", 26));
        b.put("url", ga.a.d("url", 27));
        b.put("urls", ga.a.b("urls", 28, h.class));
        b.put("verified", ga.a.c("verified", 29));
    }

    public ih() {
        this.d = 2;
        this.c = new HashSet();
    }

    ih(Set set, int i2, String str, a aVar, String str2, String str3, int i3, b bVar, String str4, String str5, int i4, String str6, c cVar, boolean z2, String str7, d dVar, String str8, int i5, List list, List list2, int i6, int i7, String str9, String str10, List list3, boolean z3) {
        this.c = set;
        this.d = i2;
        this.e = str;
        this.f = aVar;
        this.g = str2;
        this.h = str3;
        this.i = i3;
        this.j = bVar;
        this.k = str4;
        this.l = str5;
        this.m = i4;
        this.n = str6;
        this.o = cVar;
        this.p = z2;
        this.q = str7;
        this.r = dVar;
        this.s = str8;
        this.t = i5;
        this.u = list;
        this.v = list2;
        this.w = i6;
        this.x = i7;
        this.y = str9;
        this.z = str10;
        this.A = list3;
        this.B = z3;
    }

    public String A() {
        return this.y;
    }

    public String B() {
        return this.z;
    }

    /* access modifiers changed from: package-private */
    public List C() {
        return this.A;
    }

    public boolean D() {
        return this.B;
    }

    /* renamed from: E */
    public ih a() {
        return this;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public Object a(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public boolean a(ga.a aVar) {
        return this.c.contains(Integer.valueOf(aVar.g()));
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public Object b(ga.a aVar) {
        switch (aVar.g()) {
            case 2:
                return this.e;
            case 3:
                return this.f;
            case 4:
                return this.g;
            case 5:
                return this.h;
            case 6:
                return Integer.valueOf(this.i);
            case 7:
                return this.j;
            case 8:
                return this.k;
            case 9:
                return this.l;
            case 10:
            case 11:
            case 13:
            case k.ActionBar_progressBarPadding:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            case 12:
                return Integer.valueOf(this.m);
            case 14:
                return this.n;
            case 15:
                return this.o;
            case 16:
                return Boolean.valueOf(this.p);
            case k.ActionBar_itemPadding:
                return this.q;
            case 19:
                return this.r;
            case 20:
                return this.s;
            case 21:
                return Integer.valueOf(this.t);
            case 22:
                return this.u;
            case 23:
                return this.v;
            case 24:
                return Integer.valueOf(this.w);
            case 25:
                return Integer.valueOf(this.x);
            case 26:
                return this.y;
            case 27:
                return this.z;
            case 28:
                return this.A;
            case 29:
                return Boolean.valueOf(this.B);
        }
    }

    @Override // com.google.android.gms.internal.ga
    public HashMap b() {
        return b;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public boolean b(String str) {
        return false;
    }

    public int describeContents() {
        cs csVar = a;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public Set e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ih)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ih ihVar = (ih) obj;
        for (ga.a aVar : b.values()) {
            if (a(aVar)) {
                if (!ihVar.a(aVar)) {
                    return false;
                }
                if (!b(aVar).equals(ihVar.b(aVar))) {
                    return false;
                }
            } else if (ihVar.a(aVar)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.d;
    }

    public String g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public a h() {
        return this.f;
    }

    public int hashCode() {
        int i2 = 0;
        for (ga.a aVar : b.values()) {
            i2 = a(aVar) ? b(aVar).hashCode() + i2 + aVar.g() : i2;
        }
        return i2;
    }

    public String i() {
        return this.g;
    }

    public String j() {
        return this.h;
    }

    public int k() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public b l() {
        return this.j;
    }

    public String m() {
        return this.k;
    }

    public String n() {
        return this.l;
    }

    public int o() {
        return this.m;
    }

    public String p() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public c q() {
        return this.o;
    }

    public boolean r() {
        return this.p;
    }

    public String s() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public d t() {
        return this.r;
    }

    public String u() {
        return this.s;
    }

    public int v() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public List w() {
        return this.u;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        cs csVar = a;
        cs.a(this, parcel, i2);
    }

    /* access modifiers changed from: package-private */
    public List x() {
        return this.v;
    }

    public int y() {
        return this.w;
    }

    public int z() {
        return this.x;
    }
}
