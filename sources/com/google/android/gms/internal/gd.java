package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ga;
import java.util.ArrayList;
import java.util.HashMap;

public class gd implements SafeParcelable {
    public static final be a = new be();
    private final int b;
    private final HashMap c;
    private final ArrayList d = null;
    private final String e;

    public class a implements SafeParcelable {
        public static final bf a = new bf();
        final int b;
        final String c;
        final ArrayList d;

        a(int i, String str, ArrayList arrayList) {
            this.b = i;
            this.c = str;
            this.d = arrayList;
        }

        a(String str, HashMap hashMap) {
            this.b = 1;
            this.c = str;
            this.d = a(hashMap);
        }

        private static ArrayList a(HashMap hashMap) {
            if (hashMap == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (String str : hashMap.keySet()) {
                arrayList.add(new b(str, (ga.a) hashMap.get(str)));
            }
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        public HashMap a() {
            HashMap hashMap = new HashMap();
            int size = this.d.size();
            for (int i = 0; i < size; i++) {
                b bVar = (b) this.d.get(i);
                hashMap.put(bVar.c, bVar.d);
            }
            return hashMap;
        }

        public int describeContents() {
            bf bfVar = a;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            bf bfVar = a;
            bf.a(this, parcel, i);
        }
    }

    public class b implements SafeParcelable {
        public static final bd a = new bd();
        final int b;
        final String c;
        final ga.a d;

        b(int i, String str, ga.a aVar) {
            this.b = i;
            this.c = str;
            this.d = aVar;
        }

        b(String str, ga.a aVar) {
            this.b = 1;
            this.c = str;
            this.d = aVar;
        }

        public int describeContents() {
            bd bdVar = a;
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            bd bdVar = a;
            bd.a(this, parcel, i);
        }
    }

    gd(int i, ArrayList arrayList, String str) {
        this.b = i;
        this.c = a(arrayList);
        this.e = (String) aw.a((Object) str);
        a();
    }

    private static HashMap a(ArrayList arrayList) {
        HashMap hashMap = new HashMap();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            a aVar = (a) arrayList.get(i);
            hashMap.put(aVar.c, aVar.a());
        }
        return hashMap;
    }

    public HashMap a(String str) {
        return (HashMap) this.c.get(str);
    }

    public void a() {
        for (String str : this.c.keySet()) {
            HashMap hashMap = (HashMap) this.c.get(str);
            for (String str2 : hashMap.keySet()) {
                ((ga.a) hashMap.get(str2)).a(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public ArrayList c() {
        ArrayList arrayList = new ArrayList();
        for (String str : this.c.keySet()) {
            arrayList.add(new a(str, (HashMap) this.c.get(str)));
        }
        return arrayList;
    }

    public String d() {
        return this.e;
    }

    public int describeContents() {
        be beVar = a;
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String str : this.c.keySet()) {
            sb.append(str).append(":\n");
            HashMap hashMap = (HashMap) this.c.get(str);
            for (String str2 : hashMap.keySet()) {
                sb.append("  ").append(str2).append(": ");
                sb.append(hashMap.get(str2));
            }
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        be beVar = a;
        be.a(this, parcel, i);
    }
}
