package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class hs implements SafeParcelable {
    public static final cp a = new cp();
    public final int b;
    public final String c;
    public final String d;

    public hs(int i, String str, String str2) {
        this.b = i;
        this.c = str;
        this.d = str2;
    }

    public int describeContents() {
        cp cpVar = a;
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof hs)) {
            return false;
        }
        hs hsVar = (hs) obj;
        return this.d.equals(hsVar.d) && this.c.equals(hsVar.c);
    }

    public int hashCode() {
        return ar.a(this.c, this.d);
    }

    public String toString() {
        return ar.a(this).a("clientPackageName", this.c).a("locale", this.d).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        cp cpVar = a;
        cp.a(this, parcel, i);
    }
}
