package com.google.android.gms.internal;

import android.support.v7.b.k;

public final class q extends dv {
    private static volatile q[] d;
    public String a;
    public y b;
    public l c;

    public q() {
        c();
    }

    public static q[] a() {
        if (d == null) {
            synchronized (dy.a) {
                if (d == null) {
                    d = new q[0];
                }
            }
        }
        return d;
    }

    /* renamed from: a */
    public q b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.a = dsVar.g();
                    break;
                case k.ActionBar_itemPadding:
                    if (this.b == null) {
                        this.b = new y();
                    }
                    dsVar.a(this.b);
                    break;
                case 26:
                    if (this.c == null) {
                        this.c = new l();
                    }
                    dsVar.a(this.c);
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (!this.a.equals("")) {
            dtVar.a(1, this.a);
        }
        if (this.b != null) {
            dtVar.a(2, this.b);
        }
        if (this.c != null) {
            dtVar.a(3, this.c);
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b();
        if (!this.a.equals("")) {
            b2 += dt.b(1, this.a);
        }
        if (this.b != null) {
            b2 += dt.b(2, this.b);
        }
        if (this.c != null) {
            b2 += dt.b(3, this.c);
        }
        this.s = b2;
        return b2;
    }

    public q c() {
        this.a = "";
        this.b = null;
        this.c = null;
        this.r = null;
        this.s = -1;
        return this;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof q)) {
            return false;
        }
        q qVar = (q) obj;
        if (this.a == null) {
            if (qVar.a != null) {
                return false;
            }
        } else if (!this.a.equals(qVar.a)) {
            return false;
        }
        if (this.b == null) {
            if (qVar.b != null) {
                return false;
            }
        } else if (!this.b.equals(qVar.b)) {
            return false;
        }
        if (this.c == null) {
            if (qVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(qVar.c)) {
            return false;
        }
        return (this.r == null || this.r.isEmpty()) ? qVar.r == null || qVar.r.isEmpty() : this.r.equals(qVar.r);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.c == null ? 0 : this.c.hashCode()) + (((this.b == null ? 0 : this.b.hashCode()) + (((this.a == null ? 0 : this.a.hashCode()) + 527) * 31)) * 31)) * 31;
        if (this.r != null && !this.r.isEmpty()) {
            i = this.r.hashCode();
        }
        return hashCode + i;
    }
}
