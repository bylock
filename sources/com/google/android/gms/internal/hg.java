package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class hg implements SafeParcelable {
    public static final ck a = new ck();
    final int b;
    final List c;
    private final String d;
    private final boolean e;
    private final Set f;

    hg(int i, List list, String str, boolean z) {
        this.b = i;
        this.c = list == null ? Collections.emptyList() : Collections.unmodifiableList(list);
        this.d = str == null ? "" : str;
        this.e = z;
        if (this.c.isEmpty()) {
            this.f = Collections.emptySet();
        } else {
            this.f = Collections.unmodifiableSet(new HashSet(this.c));
        }
    }

    @Deprecated
    public String a() {
        return this.d;
    }

    public boolean b() {
        return this.e;
    }

    public int describeContents() {
        ck ckVar = a;
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hg)) {
            return false;
        }
        hg hgVar = (hg) obj;
        return this.f.equals(hgVar.f) && this.e == hgVar.e;
    }

    public int hashCode() {
        return ar.a(this.f, Boolean.valueOf(this.e));
    }

    public String toString() {
        return ar.a(this).a("types", this.f).a("requireOpenNow", Boolean.valueOf(this.e)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        ck ckVar = a;
        ck.a(this, parcel, i);
    }
}
