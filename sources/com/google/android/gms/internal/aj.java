package com.google.android.gms.internal;

public final class aj {
    public static boolean a(Object obj, Object obj2) {
        return (obj == null && obj2 == null) || !(obj == null || obj2 == null || !obj.equals(obj2));
    }
}
