package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class ki implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dq();
    final int a;
    private final int b;
    private final String c;
    private final byte[] d;
    private final String e;

    ki(int i, int i2, String str, byte[] bArr, String str2) {
        this.a = i;
        this.b = i2;
        this.c = str;
        this.d = bArr;
        this.e = str2;
    }

    public int a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }

    public byte[] c() {
        return this.d;
    }

    public String d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "MessageEventParcelable[" + this.b + "," + this.c + (this.d == null ? "null" : Integer.valueOf(this.d.length)) + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        dq.a(this, parcel, i);
    }
}
