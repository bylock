package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class dl implements Parcelable.Creator {
    static void a(jo joVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, joVar.a());
        c.a(parcel, 2, joVar.a, false);
        c.a(parcel, 3, (Parcelable) joVar.b, i, false);
        c.a(parcel, 4, joVar.c, false);
        c.a(parcel, 5, (Parcelable) joVar.d, i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public jo createFromParcel(Parcel parcel) {
        ju juVar = null;
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        jp jpVar = null;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    jpVar = (jp) a.a(parcel, a, jp.CREATOR);
                    break;
                case 4:
                    str = a.n(parcel, a);
                    break;
                case 5:
                    juVar = (ju) a.a(parcel, a, ju.CREATOR);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new jo(i, str2, jpVar, str, juVar);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public jo[] newArray(int i) {
        return new jo[i];
    }
}
