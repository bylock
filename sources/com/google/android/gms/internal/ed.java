package com.google.android.gms.internal;

public final class ed {
    public static final int[] a = new int[0];
    public static final long[] b = new long[0];
    public static final float[] c = new float[0];
    public static final double[] d = new double[0];
    public static final boolean[] e = new boolean[0];
    public static final String[] f = new String[0];
    public static final byte[][] g = new byte[0][];
    public static final byte[] h = new byte[0];

    static int a(int i) {
        return i & 7;
    }

    static int a(int i, int i2) {
        return (i << 3) | i2;
    }

    public static final int a(ds dsVar, int i) {
        int i2 = 1;
        int o = dsVar.o();
        dsVar.b(i);
        while (dsVar.m() > 0 && dsVar.a() == i) {
            dsVar.b(i);
            i2++;
        }
        dsVar.e(o);
        return i2;
    }

    public static int b(int i) {
        return i >>> 3;
    }
}
