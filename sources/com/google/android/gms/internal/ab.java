package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class ab implements Parcelable.Creator {
    static void a(dx dxVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, dxVar.b);
        c.a(parcel, 2, dxVar.c, false);
        c.a(parcel, 3, dxVar.d);
        c.a(parcel, 4, dxVar.e);
        c.a(parcel, 5, dxVar.f);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public dx createFromParcel(Parcel parcel) {
        boolean z = false;
        int b = a.b(parcel);
        String str = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i3 = a.g(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 3:
                    i2 = a.g(parcel, a);
                    break;
                case 4:
                    i = a.g(parcel, a);
                    break;
                case 5:
                    z = a.c(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new dx(i3, str, i2, i, z);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public dx[] newArray(int i) {
        return new dx[i];
    }
}
