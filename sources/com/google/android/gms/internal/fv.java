package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class fv implements SafeParcelable {
    public static final ay a = new ay();
    private final int b;
    private final fx c;

    fv(int i, fx fxVar) {
        this.b = i;
        this.c = fxVar;
    }

    private fv(fx fxVar) {
        this.b = 1;
        this.c = fxVar;
    }

    public static fv a(bb bbVar) {
        if (bbVar instanceof fx) {
            return new fv((fx) bbVar);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public fx b() {
        return this.c;
    }

    public bb c() {
        if (this.c != null) {
            return this.c;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }

    public int describeContents() {
        ay ayVar = a;
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ay ayVar = a;
        ay.a(this, parcel, i);
    }
}
