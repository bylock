package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;

public class ef implements Parcelable {
    @Deprecated
    public static final Parcelable.Creator CREATOR = new ae();
    private String a;
    private String b;
    private String c;

    @Deprecated
    public ef() {
    }

    @Deprecated
    ef(Parcel parcel) {
        a(parcel);
    }

    public ef(String str, String str2, String str3) {
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    @Deprecated
    private void a(Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.c;
    }

    @Deprecated
    public int describeContents() {
        return 0;
    }

    @Deprecated
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
    }
}
