package com.google.android.gms.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public abstract class fe implements SafeParcelable {
    private static final Object a = new Object();
    private static ClassLoader b = null;
    private static Integer c = null;
    private boolean d = false;

    protected static ClassLoader A() {
        ClassLoader classLoader;
        synchronized (a) {
            classLoader = b;
        }
        return classLoader;
    }

    /* access modifiers changed from: protected */
    public static Integer B() {
        Integer num;
        synchronized (a) {
            num = c;
        }
        return num;
    }

    private static boolean a(Class cls) {
        try {
            return "SAFE_PARCELABLE_NULL_STRING".equals(cls.getField("NULL").get(null));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public static boolean b(String str) {
        ClassLoader A = A();
        if (A == null) {
            return true;
        }
        try {
            return a(A.loadClass(str));
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean C() {
        return this.d;
    }
}
