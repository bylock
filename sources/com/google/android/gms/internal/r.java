package com.google.android.gms.internal;

import android.support.v7.b.k;

public final class r extends dv {
    public q[] a;
    public n b;
    public String c;

    public r() {
        a();
    }

    public static r a(byte[] bArr) {
        return (r) ea.a(new r(), bArr);
    }

    public r a() {
        this.a = q.a();
        this.b = null;
        this.c = "";
        this.r = null;
        this.s = -1;
        return this;
    }

    /* renamed from: a */
    public r b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    int a3 = ed.a(dsVar, 10);
                    int length = this.a == null ? 0 : this.a.length;
                    q[] qVarArr = new q[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.a, 0, qVarArr, 0, length);
                    }
                    while (length < qVarArr.length - 1) {
                        qVarArr[length] = new q();
                        dsVar.a(qVarArr[length]);
                        dsVar.a();
                        length++;
                    }
                    qVarArr[length] = new q();
                    dsVar.a(qVarArr[length]);
                    this.a = qVarArr;
                    break;
                case k.ActionBar_itemPadding:
                    if (this.b == null) {
                        this.b = new n();
                    }
                    dsVar.a(this.b);
                    break;
                case 26:
                    this.c = dsVar.g();
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (this.a != null && this.a.length > 0) {
            for (int i = 0; i < this.a.length; i++) {
                q qVar = this.a[i];
                if (qVar != null) {
                    dtVar.a(1, qVar);
                }
            }
        }
        if (this.b != null) {
            dtVar.a(2, this.b);
        }
        if (!this.c.equals("")) {
            dtVar.a(3, this.c);
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b();
        if (this.a != null && this.a.length > 0) {
            for (int i = 0; i < this.a.length; i++) {
                q qVar = this.a[i];
                if (qVar != null) {
                    b2 += dt.b(1, qVar);
                }
            }
        }
        if (this.b != null) {
            b2 += dt.b(2, this.b);
        }
        if (!this.c.equals("")) {
            b2 += dt.b(3, this.c);
        }
        this.s = b2;
        return b2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof r)) {
            return false;
        }
        r rVar = (r) obj;
        if (!dy.a(this.a, rVar.a)) {
            return false;
        }
        if (this.b == null) {
            if (rVar.b != null) {
                return false;
            }
        } else if (!this.b.equals(rVar.b)) {
            return false;
        }
        if (this.c == null) {
            if (rVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(rVar.c)) {
            return false;
        }
        return (this.r == null || this.r.isEmpty()) ? rVar.r == null || rVar.r.isEmpty() : this.r.equals(rVar.r);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.c == null ? 0 : this.c.hashCode()) + (((this.b == null ? 0 : this.b.hashCode()) + ((dy.a(this.a) + 527) * 31)) * 31)) * 31;
        if (this.r != null && !this.r.isEmpty()) {
            i = this.r.hashCode();
        }
        return hashCode + i;
    }
}
