package com.google.android.gms.internal;

public final class bl implements bk {
    private static bl a;

    public static synchronized bk b() {
        bl blVar;
        synchronized (bl.class) {
            if (a == null) {
                a = new bl();
            }
            blVar = a;
        }
        return blVar;
    }

    @Override // com.google.android.gms.internal.bk
    public long a() {
        return System.currentTimeMillis();
    }
}
