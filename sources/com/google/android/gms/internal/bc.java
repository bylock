package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.internal.ga;

public class bc implements Parcelable.Creator {
    static void a(ga.a aVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, aVar.a());
        c.a(parcel, 2, aVar.b());
        c.a(parcel, 3, aVar.c());
        c.a(parcel, 4, aVar.d());
        c.a(parcel, 5, aVar.e());
        c.a(parcel, 6, aVar.f(), false);
        c.a(parcel, 7, aVar.g());
        c.a(parcel, 8, aVar.i(), false);
        c.a(parcel, 9, (Parcelable) aVar.k(), i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ga.a createFromParcel(Parcel parcel) {
        fv fvVar = null;
        int i = 0;
        int b = a.b(parcel);
        String str = null;
        String str2 = null;
        boolean z = false;
        int i2 = 0;
        boolean z2 = false;
        int i3 = 0;
        int i4 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i4 = a.g(parcel, a);
                    break;
                case 2:
                    i3 = a.g(parcel, a);
                    break;
                case 3:
                    z2 = a.c(parcel, a);
                    break;
                case 4:
                    i2 = a.g(parcel, a);
                    break;
                case 5:
                    z = a.c(parcel, a);
                    break;
                case 6:
                    str2 = a.n(parcel, a);
                    break;
                case 7:
                    i = a.g(parcel, a);
                    break;
                case 8:
                    str = a.n(parcel, a);
                    break;
                case 9:
                    fvVar = (fv) a.a(parcel, a, fv.a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ga.a(i4, i3, z2, i2, z, str2, i, str, fvVar);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ga.a[] newArray(int i) {
        return new ga.a[i];
    }
}
