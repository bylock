package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.internal.gd;
import java.util.ArrayList;

public class be implements Parcelable.Creator {
    static void a(gd gdVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, gdVar.b());
        c.b(parcel, 2, gdVar.c(), false);
        c.a(parcel, 3, gdVar.d(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public gd createFromParcel(Parcel parcel) {
        String str = null;
        int b = a.b(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    arrayList = a.c(parcel, a, gd.a.a);
                    break;
                case 3:
                    str = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new gd(i, arrayList, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public gd[] newArray(int i) {
        return new gd[i];
    }
}
