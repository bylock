package com.google.android.gms.internal;

public final class an {
    public final int a;
    public final int b;

    public an(int i, int i2) {
        this.a = i;
        this.b = i2;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof an)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        an anVar = (an) obj;
        return anVar.a == this.a && anVar.b == this.b;
    }

    public int hashCode() {
        return ar.a(Integer.valueOf(this.a), Integer.valueOf(this.b));
    }
}
