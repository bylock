package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class dh implements Parcelable.Creator {
    static void a(ix ixVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, ixVar.a());
        c.a(parcel, 2, ixVar.a, false);
        c.a(parcel, 3, ixVar.b, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ix createFromParcel(Parcel parcel) {
        String[] strArr = null;
        int b = a.b(parcel);
        int i = 0;
        byte[][] bArr = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    strArr = a.z(parcel, a);
                    break;
                case 3:
                    bArr = a.r(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ix(i, strArr, bArr);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ix[] newArray(int i) {
        return new ix[i];
    }
}
