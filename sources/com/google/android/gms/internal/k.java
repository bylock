package com.google.android.gms.internal;

public final class k extends dv {
    private static volatile k[] f;
    public String a;
    public long b;
    public long c;
    public boolean d;
    public long e;

    public k() {
        c();
    }

    public static k[] a() {
        if (f == null) {
            synchronized (dy.a) {
                if (f == null) {
                    f = new k[0];
                }
            }
        }
        return f;
    }

    /* renamed from: a */
    public k b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    this.a = dsVar.g();
                    break;
                case 16:
                    this.b = dsVar.d();
                    break;
                case 24:
                    this.c = dsVar.d();
                    break;
                case 32:
                    this.d = dsVar.f();
                    break;
                case 40:
                    this.e = dsVar.d();
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (!this.a.equals("")) {
            dtVar.a(1, this.a);
        }
        if (this.b != 0) {
            dtVar.a(2, this.b);
        }
        if (this.c != 2147483647L) {
            dtVar.a(3, this.c);
        }
        if (this.d) {
            dtVar.a(4, this.d);
        }
        if (this.e != 0) {
            dtVar.a(5, this.e);
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b();
        if (!this.a.equals("")) {
            b2 += dt.b(1, this.a);
        }
        if (this.b != 0) {
            b2 += dt.c(2, this.b);
        }
        if (this.c != 2147483647L) {
            b2 += dt.c(3, this.c);
        }
        if (this.d) {
            b2 += dt.b(4, this.d);
        }
        if (this.e != 0) {
            b2 += dt.c(5, this.e);
        }
        this.s = b2;
        return b2;
    }

    public k c() {
        this.a = "";
        this.b = 0;
        this.c = 2147483647L;
        this.d = false;
        this.e = 0;
        this.r = null;
        this.s = -1;
        return this;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof k)) {
            return false;
        }
        k kVar = (k) obj;
        if (this.a == null) {
            if (kVar.a != null) {
                return false;
            }
        } else if (!this.a.equals(kVar.a)) {
            return false;
        }
        if (this.b == kVar.b && this.c == kVar.c && this.d == kVar.d && this.e == kVar.e) {
            return (this.r == null || this.r.isEmpty()) ? kVar.r == null || kVar.r.isEmpty() : this.r.equals(kVar.r);
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((this.d ? 1231 : 1237) + (((((((this.a == null ? 0 : this.a.hashCode()) + 527) * 31) + ((int) (this.b ^ (this.b >>> 32)))) * 31) + ((int) (this.c ^ (this.c >>> 32)))) * 31)) * 31) + ((int) (this.e ^ (this.e >>> 32)))) * 31;
        if (this.r != null && !this.r.isEmpty()) {
            i = this.r.hashCode();
        }
        return hashCode + i;
    }
}
