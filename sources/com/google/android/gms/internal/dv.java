package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.List;

public abstract class dv extends ea {
    protected List r;

    public final Object a(dw dwVar) {
        return dwVar.a(this.r);
    }

    @Override // com.google.android.gms.internal.ea
    public void a(dt dtVar) {
        int size = this.r == null ? 0 : this.r.size();
        for (int i = 0; i < size; i++) {
            ec ecVar = (ec) this.r.get(i);
            dtVar.e(ecVar.a);
            dtVar.a(ecVar.b);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(ds dsVar, int i) {
        int o = dsVar.o();
        if (!dsVar.b(i)) {
            return false;
        }
        if (this.r == null) {
            this.r = new ArrayList();
        }
        this.r.add(new ec(i, dsVar.a(o, dsVar.o() - o)));
        return true;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ea
    public int d() {
        int size = this.r == null ? 0 : this.r.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            ec ecVar = (ec) this.r.get(i2);
            i = i + dt.f(ecVar.a) + ecVar.b.length;
        }
        return i;
    }
}
