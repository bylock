package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class t implements Parcelable.Creator {
    static void a(ce ceVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, ceVar.b);
        c.a(parcel, 2, (Parcelable) ceVar.c, i, false);
        c.a(parcel, 3, ceVar.a(), false);
        c.a(parcel, 4, ceVar.b(), false);
        c.a(parcel, 5, ceVar.c(), false);
        c.a(parcel, 6, ceVar.d(), false);
        c.a(parcel, 7, ceVar.h, false);
        c.a(parcel, 8, ceVar.i);
        c.a(parcel, 9, ceVar.j, false);
        c.a(parcel, 10, ceVar.f(), false);
        c.a(parcel, 11, ceVar.l);
        c.a(parcel, 12, ceVar.m);
        c.a(parcel, 13, ceVar.n, false);
        c.a(parcel, 14, (Parcelable) ceVar.o, i, false);
        c.a(parcel, 15, ceVar.e(), false);
        c.a(parcel, 16, ceVar.q, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ce createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        cb cbVar = null;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        IBinder iBinder3 = null;
        IBinder iBinder4 = null;
        String str = null;
        boolean z = false;
        String str2 = null;
        IBinder iBinder5 = null;
        int i2 = 0;
        int i3 = 0;
        String str3 = null;
        dx dxVar = null;
        IBinder iBinder6 = null;
        String str4 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    cbVar = (cb) a.a(parcel, a, cb.a);
                    break;
                case 3:
                    iBinder = a.o(parcel, a);
                    break;
                case 4:
                    iBinder2 = a.o(parcel, a);
                    break;
                case 5:
                    iBinder3 = a.o(parcel, a);
                    break;
                case 6:
                    iBinder4 = a.o(parcel, a);
                    break;
                case 7:
                    str = a.n(parcel, a);
                    break;
                case 8:
                    z = a.c(parcel, a);
                    break;
                case 9:
                    str2 = a.n(parcel, a);
                    break;
                case 10:
                    iBinder5 = a.o(parcel, a);
                    break;
                case 11:
                    i2 = a.g(parcel, a);
                    break;
                case 12:
                    i3 = a.g(parcel, a);
                    break;
                case 13:
                    str3 = a.n(parcel, a);
                    break;
                case 14:
                    dxVar = (dx) a.a(parcel, a, dx.a);
                    break;
                case 15:
                    iBinder6 = a.o(parcel, a);
                    break;
                case 16:
                    str4 = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ce(i, cbVar, iBinder, iBinder2, iBinder3, iBinder4, str, z, str2, iBinder5, i2, i3, str3, dxVar, iBinder6, str4);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ce[] newArray(int i) {
        return new ce[i];
    }
}
