package com.google.android.gms.internal;

import java.io.IOException;

public class dz extends IOException {
    public dz(String str) {
        super(str);
    }

    static dz a() {
        return new dz("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }

    static dz b() {
        return new dz("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static dz c() {
        return new dz("CodedInputStream encountered a malformed varint.");
    }

    static dz d() {
        return new dz("Protocol message contained an invalid tag (zero).");
    }

    static dz e() {
        return new dz("Protocol message end-group tag did not match expected tag.");
    }

    static dz f() {
        return new dz("Protocol message tag had invalid wire type.");
    }

    static dz g() {
        return new dz("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }
}
