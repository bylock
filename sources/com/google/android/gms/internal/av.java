package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class av implements SafeParcelable {
    public static final d a = new d();
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;
    public final int h;
    public final int i;
    public final int j;
    public final String k;
    public final int l;
    public final String m;
    public final int n;
    public final int o;
    public final String p;

    av(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, String str, int i11, String str2, int i12, int i13, String str3) {
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = i6;
        this.g = i7;
        this.h = i8;
        this.i = i9;
        this.j = i10;
        this.k = str;
        this.l = i11;
        this.m = str2;
        this.n = i12;
        this.o = i13;
        this.p = str3;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        d.a(this, parcel, i2);
    }
}
