package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class d implements Parcelable.Creator {
    static void a(av avVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, avVar.b);
        c.a(parcel, 2, avVar.c);
        c.a(parcel, 3, avVar.d);
        c.a(parcel, 4, avVar.e);
        c.a(parcel, 5, avVar.f);
        c.a(parcel, 6, avVar.g);
        c.a(parcel, 7, avVar.h);
        c.a(parcel, 8, avVar.i);
        c.a(parcel, 9, avVar.j);
        c.a(parcel, 10, avVar.k, false);
        c.a(parcel, 11, avVar.l);
        c.a(parcel, 12, avVar.m, false);
        c.a(parcel, 13, avVar.n);
        c.a(parcel, 14, avVar.o);
        c.a(parcel, 15, avVar.p, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public av createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        String str = null;
        int i10 = 0;
        String str2 = null;
        int i11 = 0;
        int i12 = 0;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    i2 = a.g(parcel, a);
                    break;
                case 3:
                    i3 = a.g(parcel, a);
                    break;
                case 4:
                    i4 = a.g(parcel, a);
                    break;
                case 5:
                    i5 = a.g(parcel, a);
                    break;
                case 6:
                    i6 = a.g(parcel, a);
                    break;
                case 7:
                    i7 = a.g(parcel, a);
                    break;
                case 8:
                    i8 = a.g(parcel, a);
                    break;
                case 9:
                    i9 = a.g(parcel, a);
                    break;
                case 10:
                    str = a.n(parcel, a);
                    break;
                case 11:
                    i10 = a.g(parcel, a);
                    break;
                case 12:
                    str2 = a.n(parcel, a);
                    break;
                case 13:
                    i11 = a.g(parcel, a);
                    break;
                case 14:
                    i12 = a.g(parcel, a);
                    break;
                case 15:
                    str3 = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new av(i, i2, i3, i4, i5, i6, i7, i8, i9, str, i10, str2, i11, i12, str3);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public av[] newArray(int i) {
        return new av[i];
    }
}
