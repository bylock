package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.HashSet;
import java.util.Set;

public class cr implements Parcelable.Creator {
    static void a(ie ieVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set e = ieVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, ieVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, ieVar.g(), true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, (Parcelable) ieVar.h(), i, true);
        }
        if (e.contains(5)) {
            c.a(parcel, 5, ieVar.i(), true);
        }
        if (e.contains(6)) {
            c.a(parcel, 6, (Parcelable) ieVar.j(), i, true);
        }
        if (e.contains(7)) {
            c.a(parcel, 7, ieVar.k(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ie createFromParcel(Parcel parcel) {
        String str = null;
        int b = a.b(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        ic icVar = null;
        String str2 = null;
        ic icVar2 = null;
        String str3 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    str3 = a.n(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                default:
                    a.b(parcel, a);
                    break;
                case 4:
                    hashSet.add(4);
                    icVar2 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 5:
                    str2 = a.n(parcel, a);
                    hashSet.add(5);
                    break;
                case 6:
                    hashSet.add(6);
                    icVar = (ic) a.a(parcel, a, ic.a);
                    break;
                case 7:
                    str = a.n(parcel, a);
                    hashSet.add(7);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ie(hashSet, i, str3, icVar2, str2, icVar, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ie[] newArray(int i) {
        return new ie[i];
    }
}
