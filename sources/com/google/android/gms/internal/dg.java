package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class dg implements Parcelable.Creator {
    static void a(iv ivVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, ivVar.a());
        c.a(parcel, 2, ivVar.a, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public iv createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        int[] iArr = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    iArr = a.t(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new iv(i, iArr);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public iv[] newArray(int i) {
        return new iv[i];
    }
}
