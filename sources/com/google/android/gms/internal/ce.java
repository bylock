package com.google.android.gms.internal;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.c.b;
import com.google.android.gms.c.d;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class ce implements SafeParcelable {
    public static final t a = new t();
    public final int b;
    public final cb c;
    public final ej d;
    public final u e;
    public final ac f;
    public final e g;
    public final String h;
    public final boolean i;
    public final String j;
    public final v k;
    public final int l;
    public final int m;
    public final String n;
    public final dx o;
    public final g p;
    public final String q;

    ce(int i2, cb cbVar, IBinder iBinder, IBinder iBinder2, IBinder iBinder3, IBinder iBinder4, String str, boolean z, String str2, IBinder iBinder5, int i3, int i4, String str3, dx dxVar, IBinder iBinder6, String str4) {
        this.b = i2;
        this.c = cbVar;
        this.d = (ej) d.a(b.a(iBinder));
        this.e = (u) d.a(b.a(iBinder2));
        this.f = (ac) d.a(b.a(iBinder3));
        this.g = (e) d.a(b.a(iBinder4));
        this.h = str;
        this.i = z;
        this.j = str2;
        this.k = (v) d.a(b.a(iBinder5));
        this.l = i3;
        this.m = i4;
        this.n = str3;
        this.o = dxVar;
        this.p = (g) d.a(b.a(iBinder6));
        this.q = str4;
    }

    /* access modifiers changed from: package-private */
    public IBinder a() {
        return d.a(this.d).asBinder();
    }

    /* access modifiers changed from: package-private */
    public IBinder b() {
        return d.a(this.e).asBinder();
    }

    /* access modifiers changed from: package-private */
    public IBinder c() {
        return d.a(this.f).asBinder();
    }

    /* access modifiers changed from: package-private */
    public IBinder d() {
        return d.a(this.g).asBinder();
    }

    public int describeContents() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public IBinder e() {
        return d.a(this.p).asBinder();
    }

    /* access modifiers changed from: package-private */
    public IBinder f() {
        return d.a(this.k).asBinder();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        t.a(this, parcel, i2);
    }
}
