package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class jy implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dp();
    String a;
    String b;
    ju c;
    jw d;
    jw e;
    private final int f;

    jy() {
        this.f = 1;
    }

    jy(int i, String str, String str2, ju juVar, jw jwVar, jw jwVar2) {
        this.f = i;
        this.a = str;
        this.b = str2;
        this.c = juVar;
        this.d = jwVar;
        this.e = jwVar2;
    }

    public int a() {
        return this.f;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        dp.a(this, parcel, i);
    }
}
