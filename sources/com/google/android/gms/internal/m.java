package com.google.android.gms.internal;

public final class m extends dv {
    private static volatile m[] c;
    public int a;
    public int b;

    public m() {
        c();
    }

    public static m[] a() {
        if (c == null) {
            synchronized (dy.a) {
                if (c == null) {
                    c = new m[0];
                }
            }
        }
        return c;
    }

    /* renamed from: a */
    public m b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.a = dsVar.e();
                    break;
                case 16:
                    this.b = dsVar.e();
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        dtVar.a(1, this.a);
        dtVar.a(2, this.b);
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b() + dt.b(1, this.a) + dt.b(2, this.b);
        this.s = b2;
        return b2;
    }

    public m c() {
        this.a = 0;
        this.b = 0;
        this.r = null;
        this.s = -1;
        return this;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof m)) {
            return false;
        }
        m mVar = (m) obj;
        if (this.a == mVar.a && this.b == mVar.b) {
            return (this.r == null || this.r.isEmpty()) ? mVar.r == null || mVar.r.isEmpty() : this.r.equals(mVar.r);
        }
        return false;
    }

    public int hashCode() {
        return ((this.r == null || this.r.isEmpty()) ? 0 : this.r.hashCode()) + ((((this.a + 527) * 31) + this.b) * 31);
    }
}
