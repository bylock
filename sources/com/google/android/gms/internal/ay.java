package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class ay implements Parcelable.Creator {
    static void a(fv fvVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, fvVar.a());
        c.a(parcel, 2, (Parcelable) fvVar.b(), i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public fv createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        fx fxVar = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    fxVar = (fx) a.a(parcel, a, fx.a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new fv(i, fxVar);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public fv[] newArray(int i) {
        return new fv[i];
    }
}
