package com.google.android.gms.internal;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class w implements Parcelable.Creator {
    static void a(cx cxVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, cxVar.b);
        c.a(parcel, 2, cxVar.c, false);
        c.a(parcel, 3, (Parcelable) cxVar.d, i, false);
        c.a(parcel, 4, (Parcelable) cxVar.e, i, false);
        c.a(parcel, 5, cxVar.f, false);
        c.a(parcel, 6, (Parcelable) cxVar.g, i, false);
        c.a(parcel, 7, (Parcelable) cxVar.h, i, false);
        c.a(parcel, 8, cxVar.i, false);
        c.a(parcel, 9, cxVar.j, false);
        c.a(parcel, 10, cxVar.k, false);
        c.a(parcel, 11, (Parcelable) cxVar.l, i, false);
        c.a(parcel, 12, cxVar.m, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public cx createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        Bundle bundle = null;
        ah ahVar = null;
        ak akVar = null;
        String str = null;
        ApplicationInfo applicationInfo = null;
        PackageInfo packageInfo = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        dx dxVar = null;
        Bundle bundle2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    bundle = a.p(parcel, a);
                    break;
                case 3:
                    ahVar = (ah) a.a(parcel, a, ah.a);
                    break;
                case 4:
                    akVar = (ak) a.a(parcel, a, ak.a);
                    break;
                case 5:
                    str = a.n(parcel, a);
                    break;
                case 6:
                    applicationInfo = (ApplicationInfo) a.a(parcel, a, ApplicationInfo.CREATOR);
                    break;
                case 7:
                    packageInfo = (PackageInfo) a.a(parcel, a, PackageInfo.CREATOR);
                    break;
                case 8:
                    str2 = a.n(parcel, a);
                    break;
                case 9:
                    str3 = a.n(parcel, a);
                    break;
                case 10:
                    str4 = a.n(parcel, a);
                    break;
                case 11:
                    dxVar = (dx) a.a(parcel, a, dx.a);
                    break;
                case 12:
                    bundle2 = a.p(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new cx(i, bundle, ahVar, akVar, str, applicationInfo, packageInfo, str2, str3, str4, dxVar, bundle2);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public cx[] newArray(int i) {
        return new cx[i];
    }
}
