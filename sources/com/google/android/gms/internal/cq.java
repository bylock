package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.b.k;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class cq implements Parcelable.Creator {
    static void a(ic icVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set e = icVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, icVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, (Parcelable) icVar.g(), i, true);
        }
        if (e.contains(3)) {
            c.a(parcel, 3, icVar.h(), true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, (Parcelable) icVar.i(), i, true);
        }
        if (e.contains(5)) {
            c.a(parcel, 5, icVar.j(), true);
        }
        if (e.contains(6)) {
            c.a(parcel, 6, icVar.k(), true);
        }
        if (e.contains(7)) {
            c.a(parcel, 7, icVar.l(), true);
        }
        if (e.contains(8)) {
            c.b(parcel, 8, icVar.m(), true);
        }
        if (e.contains(9)) {
            c.a(parcel, 9, icVar.n());
        }
        if (e.contains(10)) {
            c.b(parcel, 10, icVar.o(), true);
        }
        if (e.contains(11)) {
            c.a(parcel, 11, (Parcelable) icVar.p(), i, true);
        }
        if (e.contains(12)) {
            c.b(parcel, 12, icVar.q(), true);
        }
        if (e.contains(13)) {
            c.a(parcel, 13, icVar.r(), true);
        }
        if (e.contains(14)) {
            c.a(parcel, 14, icVar.s(), true);
        }
        if (e.contains(15)) {
            c.a(parcel, 15, (Parcelable) icVar.t(), i, true);
        }
        if (e.contains(17)) {
            c.a(parcel, 17, icVar.v(), true);
        }
        if (e.contains(16)) {
            c.a(parcel, 16, icVar.u(), true);
        }
        if (e.contains(19)) {
            c.b(parcel, 19, icVar.x(), true);
        }
        if (e.contains(18)) {
            c.a(parcel, 18, icVar.w(), true);
        }
        if (e.contains(21)) {
            c.a(parcel, 21, icVar.z(), true);
        }
        if (e.contains(20)) {
            c.a(parcel, 20, icVar.y(), true);
        }
        if (e.contains(23)) {
            c.a(parcel, 23, icVar.B(), true);
        }
        if (e.contains(22)) {
            c.a(parcel, 22, icVar.A(), true);
        }
        if (e.contains(25)) {
            c.a(parcel, 25, icVar.D(), true);
        }
        if (e.contains(24)) {
            c.a(parcel, 24, icVar.C(), true);
        }
        if (e.contains(27)) {
            c.a(parcel, 27, icVar.F(), true);
        }
        if (e.contains(26)) {
            c.a(parcel, 26, icVar.E(), true);
        }
        if (e.contains(29)) {
            c.a(parcel, 29, (Parcelable) icVar.H(), i, true);
        }
        if (e.contains(28)) {
            c.a(parcel, 28, icVar.G(), true);
        }
        if (e.contains(31)) {
            c.a(parcel, 31, icVar.J(), true);
        }
        if (e.contains(30)) {
            c.a(parcel, 30, icVar.I(), true);
        }
        if (e.contains(34)) {
            c.a(parcel, 34, (Parcelable) icVar.M(), i, true);
        }
        if (e.contains(32)) {
            c.a(parcel, 32, icVar.K(), true);
        }
        if (e.contains(33)) {
            c.a(parcel, 33, icVar.L(), true);
        }
        if (e.contains(38)) {
            c.a(parcel, 38, icVar.P());
        }
        if (e.contains(39)) {
            c.a(parcel, 39, icVar.Q(), true);
        }
        if (e.contains(36)) {
            c.a(parcel, 36, icVar.N());
        }
        if (e.contains(37)) {
            c.a(parcel, 37, (Parcelable) icVar.O(), i, true);
        }
        if (e.contains(42)) {
            c.a(parcel, 42, icVar.T(), true);
        }
        if (e.contains(43)) {
            c.a(parcel, 43, icVar.U(), true);
        }
        if (e.contains(40)) {
            c.a(parcel, 40, (Parcelable) icVar.R(), i, true);
        }
        if (e.contains(41)) {
            c.b(parcel, 41, icVar.S(), true);
        }
        if (e.contains(46)) {
            c.a(parcel, 46, (Parcelable) icVar.X(), i, true);
        }
        if (e.contains(47)) {
            c.a(parcel, 47, icVar.Y(), true);
        }
        if (e.contains(44)) {
            c.a(parcel, 44, icVar.V(), true);
        }
        if (e.contains(45)) {
            c.a(parcel, 45, icVar.W(), true);
        }
        if (e.contains(51)) {
            c.a(parcel, 51, icVar.ac(), true);
        }
        if (e.contains(50)) {
            c.a(parcel, 50, (Parcelable) icVar.ab(), i, true);
        }
        if (e.contains(49)) {
            c.a(parcel, 49, icVar.aa(), true);
        }
        if (e.contains(48)) {
            c.a(parcel, 48, icVar.Z(), true);
        }
        if (e.contains(55)) {
            c.a(parcel, 55, icVar.ag(), true);
        }
        if (e.contains(54)) {
            c.a(parcel, 54, icVar.af(), true);
        }
        if (e.contains(53)) {
            c.a(parcel, 53, icVar.ae(), true);
        }
        if (e.contains(52)) {
            c.a(parcel, 52, icVar.ad(), true);
        }
        if (e.contains(56)) {
            c.a(parcel, 56, icVar.ah(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ic createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        ic icVar = null;
        ArrayList arrayList = null;
        ic icVar2 = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        ArrayList arrayList2 = null;
        int i2 = 0;
        ArrayList arrayList3 = null;
        ic icVar3 = null;
        ArrayList arrayList4 = null;
        String str4 = null;
        String str5 = null;
        ic icVar4 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        ArrayList arrayList5 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        String str15 = null;
        String str16 = null;
        String str17 = null;
        ic icVar5 = null;
        String str18 = null;
        String str19 = null;
        String str20 = null;
        String str21 = null;
        ic icVar6 = null;
        double d = 0.0d;
        ic icVar7 = null;
        double d2 = 0.0d;
        String str22 = null;
        ic icVar8 = null;
        ArrayList arrayList6 = null;
        String str23 = null;
        String str24 = null;
        String str25 = null;
        String str26 = null;
        ic icVar9 = null;
        String str27 = null;
        String str28 = null;
        String str29 = null;
        ic icVar10 = null;
        String str30 = null;
        String str31 = null;
        String str32 = null;
        String str33 = null;
        String str34 = null;
        String str35 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    hashSet.add(2);
                    icVar = (ic) a.a(parcel, a, ic.a);
                    break;
                case 3:
                    arrayList = a.A(parcel, a);
                    hashSet.add(3);
                    break;
                case 4:
                    hashSet.add(4);
                    icVar2 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 5:
                    str = a.n(parcel, a);
                    hashSet.add(5);
                    break;
                case 6:
                    str2 = a.n(parcel, a);
                    hashSet.add(6);
                    break;
                case 7:
                    str3 = a.n(parcel, a);
                    hashSet.add(7);
                    break;
                case 8:
                    arrayList2 = a.c(parcel, a, ic.a);
                    hashSet.add(8);
                    break;
                case 9:
                    i2 = a.g(parcel, a);
                    hashSet.add(9);
                    break;
                case 10:
                    arrayList3 = a.c(parcel, a, ic.a);
                    hashSet.add(10);
                    break;
                case 11:
                    hashSet.add(11);
                    icVar3 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 12:
                    arrayList4 = a.c(parcel, a, ic.a);
                    hashSet.add(12);
                    break;
                case 13:
                    str4 = a.n(parcel, a);
                    hashSet.add(13);
                    break;
                case 14:
                    str5 = a.n(parcel, a);
                    hashSet.add(14);
                    break;
                case 15:
                    hashSet.add(15);
                    icVar4 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 16:
                    str6 = a.n(parcel, a);
                    hashSet.add(16);
                    break;
                case k.ActionBar_progressBarPadding:
                    str7 = a.n(parcel, a);
                    hashSet.add(17);
                    break;
                case k.ActionBar_itemPadding:
                    str8 = a.n(parcel, a);
                    hashSet.add(18);
                    break;
                case 19:
                    arrayList5 = a.c(parcel, a, ic.a);
                    hashSet.add(19);
                    break;
                case 20:
                    str9 = a.n(parcel, a);
                    hashSet.add(20);
                    break;
                case 21:
                    str10 = a.n(parcel, a);
                    hashSet.add(21);
                    break;
                case 22:
                    str11 = a.n(parcel, a);
                    hashSet.add(22);
                    break;
                case 23:
                    str12 = a.n(parcel, a);
                    hashSet.add(23);
                    break;
                case 24:
                    str13 = a.n(parcel, a);
                    hashSet.add(24);
                    break;
                case 25:
                    str14 = a.n(parcel, a);
                    hashSet.add(25);
                    break;
                case 26:
                    str15 = a.n(parcel, a);
                    hashSet.add(26);
                    break;
                case 27:
                    str16 = a.n(parcel, a);
                    hashSet.add(27);
                    break;
                case 28:
                    str17 = a.n(parcel, a);
                    hashSet.add(28);
                    break;
                case 29:
                    hashSet.add(29);
                    icVar5 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 30:
                    str18 = a.n(parcel, a);
                    hashSet.add(30);
                    break;
                case 31:
                    str19 = a.n(parcel, a);
                    hashSet.add(31);
                    break;
                case 32:
                    str20 = a.n(parcel, a);
                    hashSet.add(32);
                    break;
                case 33:
                    str21 = a.n(parcel, a);
                    hashSet.add(33);
                    break;
                case 34:
                    hashSet.add(34);
                    icVar6 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 35:
                default:
                    a.b(parcel, a);
                    break;
                case 36:
                    d = a.l(parcel, a);
                    hashSet.add(36);
                    break;
                case 37:
                    hashSet.add(37);
                    icVar7 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 38:
                    d2 = a.l(parcel, a);
                    hashSet.add(38);
                    break;
                case 39:
                    str22 = a.n(parcel, a);
                    hashSet.add(39);
                    break;
                case 40:
                    hashSet.add(40);
                    icVar8 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 41:
                    arrayList6 = a.c(parcel, a, ic.a);
                    hashSet.add(41);
                    break;
                case 42:
                    str23 = a.n(parcel, a);
                    hashSet.add(42);
                    break;
                case 43:
                    str24 = a.n(parcel, a);
                    hashSet.add(43);
                    break;
                case 44:
                    str25 = a.n(parcel, a);
                    hashSet.add(44);
                    break;
                case 45:
                    str26 = a.n(parcel, a);
                    hashSet.add(45);
                    break;
                case 46:
                    hashSet.add(46);
                    icVar9 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 47:
                    str27 = a.n(parcel, a);
                    hashSet.add(47);
                    break;
                case 48:
                    str28 = a.n(parcel, a);
                    hashSet.add(48);
                    break;
                case 49:
                    str29 = a.n(parcel, a);
                    hashSet.add(49);
                    break;
                case 50:
                    hashSet.add(50);
                    icVar10 = (ic) a.a(parcel, a, ic.a);
                    break;
                case 51:
                    str30 = a.n(parcel, a);
                    hashSet.add(51);
                    break;
                case 52:
                    str31 = a.n(parcel, a);
                    hashSet.add(52);
                    break;
                case 53:
                    str32 = a.n(parcel, a);
                    hashSet.add(53);
                    break;
                case 54:
                    str33 = a.n(parcel, a);
                    hashSet.add(54);
                    break;
                case 55:
                    str34 = a.n(parcel, a);
                    hashSet.add(55);
                    break;
                case 56:
                    str35 = a.n(parcel, a);
                    hashSet.add(56);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ic(hashSet, i, icVar, arrayList, icVar2, str, str2, str3, arrayList2, i2, arrayList3, icVar3, arrayList4, str4, str5, icVar4, str6, str7, str8, arrayList5, str9, str10, str11, str12, str13, str14, str15, str16, str17, icVar5, str18, str19, str20, str21, icVar6, d, icVar7, d2, str22, icVar8, arrayList6, str23, str24, str25, str26, icVar9, str27, str28, str29, icVar10, str30, str31, str32, str33, str34, str35);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ic[] newArray(int i) {
        return new ic[i];
    }
}
