package com.google.android.gms.internal;

import android.os.Parcel;
import android.support.v7.b.k;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ga;
import com.google.android.gms.plus.a.a.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class ic extends ga implements SafeParcelable, a {
    public static final cq a = new cq();
    private static final HashMap b = new HashMap();
    private String A;
    private String B;
    private String C;
    private String D;
    private String E;
    private ic F;
    private String G;
    private String H;
    private String I;
    private String J;
    private ic K;
    private double L;
    private ic M;
    private double N;
    private String O;
    private ic P;
    private List Q;
    private String R;
    private String S;
    private String T;
    private String U;
    private ic V;
    private String W;
    private String X;
    private String Y;
    private ic Z;
    private String aa;
    private String ab;
    private String ac;
    private String ad;
    private String ae;
    private String af;
    private final Set c;
    private final int d;
    private ic e;
    private List f;
    private ic g;
    private String h;
    private String i;
    private String j;
    private List k;
    private int l;
    private List m;
    private ic n;
    private List o;
    private String p;
    private String q;
    private ic r;
    private String s;
    private String t;
    private String u;
    private List v;
    private String w;
    private String x;
    private String y;
    private String z;

    static {
        b.put("about", ga.a.a("about", 2, ic.class));
        b.put("additionalName", ga.a.e("additionalName", 3));
        b.put("address", ga.a.a("address", 4, ic.class));
        b.put("addressCountry", ga.a.d("addressCountry", 5));
        b.put("addressLocality", ga.a.d("addressLocality", 6));
        b.put("addressRegion", ga.a.d("addressRegion", 7));
        b.put("associated_media", ga.a.b("associated_media", 8, ic.class));
        b.put("attendeeCount", ga.a.a("attendeeCount", 9));
        b.put("attendees", ga.a.b("attendees", 10, ic.class));
        b.put("audio", ga.a.a("audio", 11, ic.class));
        b.put("author", ga.a.b("author", 12, ic.class));
        b.put("bestRating", ga.a.d("bestRating", 13));
        b.put("birthDate", ga.a.d("birthDate", 14));
        b.put("byArtist", ga.a.a("byArtist", 15, ic.class));
        b.put("caption", ga.a.d("caption", 16));
        b.put("contentSize", ga.a.d("contentSize", 17));
        b.put("contentUrl", ga.a.d("contentUrl", 18));
        b.put("contributor", ga.a.b("contributor", 19, ic.class));
        b.put("dateCreated", ga.a.d("dateCreated", 20));
        b.put("dateModified", ga.a.d("dateModified", 21));
        b.put("datePublished", ga.a.d("datePublished", 22));
        b.put("description", ga.a.d("description", 23));
        b.put("duration", ga.a.d("duration", 24));
        b.put("embedUrl", ga.a.d("embedUrl", 25));
        b.put("endDate", ga.a.d("endDate", 26));
        b.put("familyName", ga.a.d("familyName", 27));
        b.put("gender", ga.a.d("gender", 28));
        b.put("geo", ga.a.a("geo", 29, ic.class));
        b.put("givenName", ga.a.d("givenName", 30));
        b.put("height", ga.a.d("height", 31));
        b.put("id", ga.a.d("id", 32));
        b.put("image", ga.a.d("image", 33));
        b.put("inAlbum", ga.a.a("inAlbum", 34, ic.class));
        b.put("latitude", ga.a.b("latitude", 36));
        b.put("location", ga.a.a("location", 37, ic.class));
        b.put("longitude", ga.a.b("longitude", 38));
        b.put("name", ga.a.d("name", 39));
        b.put("partOfTVSeries", ga.a.a("partOfTVSeries", 40, ic.class));
        b.put("performers", ga.a.b("performers", 41, ic.class));
        b.put("playerType", ga.a.d("playerType", 42));
        b.put("postOfficeBoxNumber", ga.a.d("postOfficeBoxNumber", 43));
        b.put("postalCode", ga.a.d("postalCode", 44));
        b.put("ratingValue", ga.a.d("ratingValue", 45));
        b.put("reviewRating", ga.a.a("reviewRating", 46, ic.class));
        b.put("startDate", ga.a.d("startDate", 47));
        b.put("streetAddress", ga.a.d("streetAddress", 48));
        b.put("text", ga.a.d("text", 49));
        b.put("thumbnail", ga.a.a("thumbnail", 50, ic.class));
        b.put("thumbnailUrl", ga.a.d("thumbnailUrl", 51));
        b.put("tickerSymbol", ga.a.d("tickerSymbol", 52));
        b.put("type", ga.a.d("type", 53));
        b.put("url", ga.a.d("url", 54));
        b.put("width", ga.a.d("width", 55));
        b.put("worstRating", ga.a.d("worstRating", 56));
    }

    public ic() {
        this.d = 1;
        this.c = new HashSet();
    }

    ic(Set set, int i2, ic icVar, List list, ic icVar2, String str, String str2, String str3, List list2, int i3, List list3, ic icVar3, List list4, String str4, String str5, ic icVar4, String str6, String str7, String str8, List list5, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, ic icVar5, String str18, String str19, String str20, String str21, ic icVar6, double d2, ic icVar7, double d3, String str22, ic icVar8, List list6, String str23, String str24, String str25, String str26, ic icVar9, String str27, String str28, String str29, ic icVar10, String str30, String str31, String str32, String str33, String str34, String str35) {
        this.c = set;
        this.d = i2;
        this.e = icVar;
        this.f = list;
        this.g = icVar2;
        this.h = str;
        this.i = str2;
        this.j = str3;
        this.k = list2;
        this.l = i3;
        this.m = list3;
        this.n = icVar3;
        this.o = list4;
        this.p = str4;
        this.q = str5;
        this.r = icVar4;
        this.s = str6;
        this.t = str7;
        this.u = str8;
        this.v = list5;
        this.w = str9;
        this.x = str10;
        this.y = str11;
        this.z = str12;
        this.A = str13;
        this.B = str14;
        this.C = str15;
        this.D = str16;
        this.E = str17;
        this.F = icVar5;
        this.G = str18;
        this.H = str19;
        this.I = str20;
        this.J = str21;
        this.K = icVar6;
        this.L = d2;
        this.M = icVar7;
        this.N = d3;
        this.O = str22;
        this.P = icVar8;
        this.Q = list6;
        this.R = str23;
        this.S = str24;
        this.T = str25;
        this.U = str26;
        this.V = icVar9;
        this.W = str27;
        this.X = str28;
        this.Y = str29;
        this.Z = icVar10;
        this.aa = str30;
        this.ab = str31;
        this.ac = str32;
        this.ad = str33;
        this.ae = str34;
        this.af = str35;
    }

    public String A() {
        return this.y;
    }

    public String B() {
        return this.z;
    }

    public String C() {
        return this.A;
    }

    public String D() {
        return this.B;
    }

    public String E() {
        return this.C;
    }

    public String F() {
        return this.D;
    }

    public String G() {
        return this.E;
    }

    /* access modifiers changed from: package-private */
    public ic H() {
        return this.F;
    }

    public String I() {
        return this.G;
    }

    public String J() {
        return this.H;
    }

    public String K() {
        return this.I;
    }

    public String L() {
        return this.J;
    }

    /* access modifiers changed from: package-private */
    public ic M() {
        return this.K;
    }

    public double N() {
        return this.L;
    }

    /* access modifiers changed from: package-private */
    public ic O() {
        return this.M;
    }

    public double P() {
        return this.N;
    }

    public String Q() {
        return this.O;
    }

    /* access modifiers changed from: package-private */
    public ic R() {
        return this.P;
    }

    /* access modifiers changed from: package-private */
    public List S() {
        return this.Q;
    }

    public String T() {
        return this.R;
    }

    public String U() {
        return this.S;
    }

    public String V() {
        return this.T;
    }

    public String W() {
        return this.U;
    }

    /* access modifiers changed from: package-private */
    public ic X() {
        return this.V;
    }

    public String Y() {
        return this.W;
    }

    public String Z() {
        return this.X;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public Object a(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public boolean a(ga.a aVar) {
        return this.c.contains(Integer.valueOf(aVar.g()));
    }

    public String aa() {
        return this.Y;
    }

    /* access modifiers changed from: package-private */
    public ic ab() {
        return this.Z;
    }

    public String ac() {
        return this.aa;
    }

    public String ad() {
        return this.ab;
    }

    public String ae() {
        return this.ac;
    }

    public String af() {
        return this.ad;
    }

    public String ag() {
        return this.ae;
    }

    public String ah() {
        return this.af;
    }

    /* renamed from: ai */
    public ic a() {
        return this;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public Object b(ga.a aVar) {
        switch (aVar.g()) {
            case 2:
                return this.e;
            case 3:
                return this.f;
            case 4:
                return this.g;
            case 5:
                return this.h;
            case 6:
                return this.i;
            case 7:
                return this.j;
            case 8:
                return this.k;
            case 9:
                return Integer.valueOf(this.l);
            case 10:
                return this.m;
            case 11:
                return this.n;
            case 12:
                return this.o;
            case 13:
                return this.p;
            case 14:
                return this.q;
            case 15:
                return this.r;
            case 16:
                return this.s;
            case k.ActionBar_progressBarPadding:
                return this.t;
            case k.ActionBar_itemPadding:
                return this.u;
            case 19:
                return this.v;
            case 20:
                return this.w;
            case 21:
                return this.x;
            case 22:
                return this.y;
            case 23:
                return this.z;
            case 24:
                return this.A;
            case 25:
                return this.B;
            case 26:
                return this.C;
            case 27:
                return this.D;
            case 28:
                return this.E;
            case 29:
                return this.F;
            case 30:
                return this.G;
            case 31:
                return this.H;
            case 32:
                return this.I;
            case 33:
                return this.J;
            case 34:
                return this.K;
            case 35:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            case 36:
                return Double.valueOf(this.L);
            case 37:
                return this.M;
            case 38:
                return Double.valueOf(this.N);
            case 39:
                return this.O;
            case 40:
                return this.P;
            case 41:
                return this.Q;
            case 42:
                return this.R;
            case 43:
                return this.S;
            case 44:
                return this.T;
            case 45:
                return this.U;
            case 46:
                return this.V;
            case 47:
                return this.W;
            case 48:
                return this.X;
            case 49:
                return this.Y;
            case 50:
                return this.Z;
            case 51:
                return this.aa;
            case 52:
                return this.ab;
            case 53:
                return this.ac;
            case 54:
                return this.ad;
            case 55:
                return this.ae;
            case 56:
                return this.af;
        }
    }

    @Override // com.google.android.gms.internal.ga
    public HashMap b() {
        return b;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public boolean b(String str) {
        return false;
    }

    public int describeContents() {
        cq cqVar = a;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public Set e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ic)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ic icVar = (ic) obj;
        for (ga.a aVar : b.values()) {
            if (a(aVar)) {
                if (!icVar.a(aVar)) {
                    return false;
                }
                if (!b(aVar).equals(icVar.b(aVar))) {
                    return false;
                }
            } else if (icVar.a(aVar)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public ic g() {
        return this.e;
    }

    public List h() {
        return this.f;
    }

    public int hashCode() {
        int i2 = 0;
        for (ga.a aVar : b.values()) {
            i2 = a(aVar) ? b(aVar).hashCode() + i2 + aVar.g() : i2;
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public ic i() {
        return this.g;
    }

    public String j() {
        return this.h;
    }

    public String k() {
        return this.i;
    }

    public String l() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public List m() {
        return this.k;
    }

    public int n() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public List o() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public ic p() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public List q() {
        return this.o;
    }

    public String r() {
        return this.p;
    }

    public String s() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public ic t() {
        return this.r;
    }

    public String u() {
        return this.s;
    }

    public String v() {
        return this.t;
    }

    public String w() {
        return this.u;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        cq cqVar = a;
        cq.a(this, parcel, i2);
    }

    /* access modifiers changed from: package-private */
    public List x() {
        return this.v;
    }

    public String y() {
        return this.w;
    }

    public String z() {
        return this.x;
    }
}
