package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class ix implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dh();
    String[] a;
    byte[][] b;
    private final int c;

    ix() {
        this(1, new String[0], new byte[0][]);
    }

    ix(int i, String[] strArr, byte[][] bArr) {
        this.c = i;
        this.a = strArr;
        this.b = bArr;
    }

    public int a() {
        return this.c;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        dh.a(this, parcel, i);
    }
}
