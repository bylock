package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.internal.fc;
import java.util.ArrayList;

public class au implements Parcelable.Creator {
    static void a(fc.a aVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, aVar.a(), false);
        c.a(parcel, 1000, aVar.e());
        c.a(parcel, 2, aVar.d(), false);
        c.a(parcel, 3, aVar.b());
        c.a(parcel, 4, aVar.c(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public fc.a createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int b = a.b(parcel);
        ArrayList arrayList = null;
        String str2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str2 = a.n(parcel, a);
                    break;
                case 2:
                    arrayList = a.A(parcel, a);
                    break;
                case 3:
                    i = a.g(parcel, a);
                    break;
                case 4:
                    str = a.n(parcel, a);
                    break;
                case 1000:
                    i2 = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new fc.a(i2, str2, arrayList, i, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public fc.a[] newArray(int i) {
        return new fc.a[i];
    }
}
