package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class dn implements Parcelable.Creator {
    static void a(ju juVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, juVar.a());
        c.a(parcel, 2, juVar.a);
        c.a(parcel, 3, juVar.b);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ju createFromParcel(Parcel parcel) {
        long j = 0;
        int b = a.b(parcel);
        int i = 0;
        long j2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    j2 = a.i(parcel, a);
                    break;
                case 3:
                    j = a.i(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ju(i, j2, j);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ju[] newArray(int i) {
        return new ju[i];
    }
}
