package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class dq implements Parcelable.Creator {
    static void a(ki kiVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, kiVar.a);
        c.a(parcel, 2, kiVar.a());
        c.a(parcel, 3, kiVar.b(), false);
        c.a(parcel, 4, kiVar.c(), false);
        c.a(parcel, 5, kiVar.d(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ki createFromParcel(Parcel parcel) {
        int i = 0;
        String str = null;
        int b = a.b(parcel);
        byte[] bArr = null;
        String str2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i2 = a.g(parcel, a);
                    break;
                case 2:
                    i = a.g(parcel, a);
                    break;
                case 3:
                    str2 = a.n(parcel, a);
                    break;
                case 4:
                    bArr = a.q(parcel, a);
                    break;
                case 5:
                    str = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ki(i2, i, str2, bArr, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ki[] newArray(int i) {
        return new ki[i];
    }
}
