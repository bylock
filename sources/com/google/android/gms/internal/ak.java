package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class ak implements SafeParcelable {
    public static final c a = new c();
    public final int b;
    public final String c;
    public final int d;
    public final int e;
    public final boolean f;
    public final int g;
    public final int h;
    public final ak[] i;

    public ak() {
        this(2, "interstitial_mb", 0, 0, true, 0, 0, null);
    }

    ak(int i2, String str, int i3, int i4, boolean z, int i5, int i6, ak[] akVarArr) {
        this.b = i2;
        this.c = str;
        this.d = i3;
        this.e = i4;
        this.f = z;
        this.g = i5;
        this.h = i6;
        this.i = akVarArr;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        c.a(this, parcel, i2);
    }
}
