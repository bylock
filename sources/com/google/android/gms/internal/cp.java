package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class cp implements Parcelable.Creator {
    static void a(hs hsVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, hsVar.c, false);
        c.a(parcel, 1000, hsVar.b);
        c.a(parcel, 2, hsVar.d, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public hs createFromParcel(Parcel parcel) {
        String str = null;
        int b = a.b(parcel);
        int i = 0;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str2 = a.n(parcel, a);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new hs(i, str2, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public hs[] newArray(int i) {
        return new hs[i];
    }
}
