package com.google.android.gms.internal;

import android.support.v7.b.k;

public final class n extends dv {
    public String[] a;
    public String[] b;
    public y[] c;
    public m[] d;
    public j[] e;
    public j[] f;
    public j[] g;
    public o[] h;
    public String i;
    public String j;
    public String k;
    public String l;
    public i m;
    public float n;
    public boolean o;
    public String[] p;
    public int q;

    public n() {
        a();
    }

    public static n a(byte[] bArr) {
        return (n) ea.a(new n(), bArr);
    }

    public n a() {
        this.a = ed.f;
        this.b = ed.f;
        this.c = y.a();
        this.d = m.a();
        this.e = j.a();
        this.f = j.a();
        this.g = j.a();
        this.h = o.a();
        this.i = "";
        this.j = "";
        this.k = "0";
        this.l = "";
        this.m = null;
        this.n = 0.0f;
        this.o = false;
        this.p = ed.f;
        this.q = 0;
        this.r = null;
        this.s = -1;
        return this;
    }

    /* renamed from: a */
    public n b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 10:
                    int a3 = ed.a(dsVar, 10);
                    int length = this.b == null ? 0 : this.b.length;
                    String[] strArr = new String[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, strArr, 0, length);
                    }
                    while (length < strArr.length - 1) {
                        strArr[length] = dsVar.g();
                        dsVar.a();
                        length++;
                    }
                    strArr[length] = dsVar.g();
                    this.b = strArr;
                    break;
                case k.ActionBar_itemPadding:
                    int a4 = ed.a(dsVar, 18);
                    int length2 = this.c == null ? 0 : this.c.length;
                    y[] yVarArr = new y[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.c, 0, yVarArr, 0, length2);
                    }
                    while (length2 < yVarArr.length - 1) {
                        yVarArr[length2] = new y();
                        dsVar.a(yVarArr[length2]);
                        dsVar.a();
                        length2++;
                    }
                    yVarArr[length2] = new y();
                    dsVar.a(yVarArr[length2]);
                    this.c = yVarArr;
                    break;
                case 26:
                    int a5 = ed.a(dsVar, 26);
                    int length3 = this.d == null ? 0 : this.d.length;
                    m[] mVarArr = new m[(a5 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.d, 0, mVarArr, 0, length3);
                    }
                    while (length3 < mVarArr.length - 1) {
                        mVarArr[length3] = new m();
                        dsVar.a(mVarArr[length3]);
                        dsVar.a();
                        length3++;
                    }
                    mVarArr[length3] = new m();
                    dsVar.a(mVarArr[length3]);
                    this.d = mVarArr;
                    break;
                case 34:
                    int a6 = ed.a(dsVar, 34);
                    int length4 = this.e == null ? 0 : this.e.length;
                    j[] jVarArr = new j[(a6 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.e, 0, jVarArr, 0, length4);
                    }
                    while (length4 < jVarArr.length - 1) {
                        jVarArr[length4] = new j();
                        dsVar.a(jVarArr[length4]);
                        dsVar.a();
                        length4++;
                    }
                    jVarArr[length4] = new j();
                    dsVar.a(jVarArr[length4]);
                    this.e = jVarArr;
                    break;
                case 42:
                    int a7 = ed.a(dsVar, 42);
                    int length5 = this.f == null ? 0 : this.f.length;
                    j[] jVarArr2 = new j[(a7 + length5)];
                    if (length5 != 0) {
                        System.arraycopy(this.f, 0, jVarArr2, 0, length5);
                    }
                    while (length5 < jVarArr2.length - 1) {
                        jVarArr2[length5] = new j();
                        dsVar.a(jVarArr2[length5]);
                        dsVar.a();
                        length5++;
                    }
                    jVarArr2[length5] = new j();
                    dsVar.a(jVarArr2[length5]);
                    this.f = jVarArr2;
                    break;
                case 50:
                    int a8 = ed.a(dsVar, 50);
                    int length6 = this.g == null ? 0 : this.g.length;
                    j[] jVarArr3 = new j[(a8 + length6)];
                    if (length6 != 0) {
                        System.arraycopy(this.g, 0, jVarArr3, 0, length6);
                    }
                    while (length6 < jVarArr3.length - 1) {
                        jVarArr3[length6] = new j();
                        dsVar.a(jVarArr3[length6]);
                        dsVar.a();
                        length6++;
                    }
                    jVarArr3[length6] = new j();
                    dsVar.a(jVarArr3[length6]);
                    this.g = jVarArr3;
                    break;
                case 58:
                    int a9 = ed.a(dsVar, 58);
                    int length7 = this.h == null ? 0 : this.h.length;
                    o[] oVarArr = new o[(a9 + length7)];
                    if (length7 != 0) {
                        System.arraycopy(this.h, 0, oVarArr, 0, length7);
                    }
                    while (length7 < oVarArr.length - 1) {
                        oVarArr[length7] = new o();
                        dsVar.a(oVarArr[length7]);
                        dsVar.a();
                        length7++;
                    }
                    oVarArr[length7] = new o();
                    dsVar.a(oVarArr[length7]);
                    this.h = oVarArr;
                    break;
                case 74:
                    this.i = dsVar.g();
                    break;
                case 82:
                    this.j = dsVar.g();
                    break;
                case 98:
                    this.k = dsVar.g();
                    break;
                case 106:
                    this.l = dsVar.g();
                    break;
                case 114:
                    if (this.m == null) {
                        this.m = new i();
                    }
                    dsVar.a(this.m);
                    break;
                case 125:
                    this.n = dsVar.c();
                    break;
                case 130:
                    int a10 = ed.a(dsVar, 130);
                    int length8 = this.p == null ? 0 : this.p.length;
                    String[] strArr2 = new String[(a10 + length8)];
                    if (length8 != 0) {
                        System.arraycopy(this.p, 0, strArr2, 0, length8);
                    }
                    while (length8 < strArr2.length - 1) {
                        strArr2[length8] = dsVar.g();
                        dsVar.a();
                        length8++;
                    }
                    strArr2[length8] = dsVar.g();
                    this.p = strArr2;
                    break;
                case 136:
                    this.q = dsVar.e();
                    break;
                case 144:
                    this.o = dsVar.f();
                    break;
                case 154:
                    int a11 = ed.a(dsVar, 154);
                    int length9 = this.a == null ? 0 : this.a.length;
                    String[] strArr3 = new String[(a11 + length9)];
                    if (length9 != 0) {
                        System.arraycopy(this.a, 0, strArr3, 0, length9);
                    }
                    while (length9 < strArr3.length - 1) {
                        strArr3[length9] = dsVar.g();
                        dsVar.a();
                        length9++;
                    }
                    strArr3[length9] = dsVar.g();
                    this.a = strArr3;
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (this.b != null && this.b.length > 0) {
            for (int i2 = 0; i2 < this.b.length; i2++) {
                String str = this.b[i2];
                if (str != null) {
                    dtVar.a(1, str);
                }
            }
        }
        if (this.c != null && this.c.length > 0) {
            for (int i3 = 0; i3 < this.c.length; i3++) {
                y yVar = this.c[i3];
                if (yVar != null) {
                    dtVar.a(2, yVar);
                }
            }
        }
        if (this.d != null && this.d.length > 0) {
            for (int i4 = 0; i4 < this.d.length; i4++) {
                m mVar = this.d[i4];
                if (mVar != null) {
                    dtVar.a(3, mVar);
                }
            }
        }
        if (this.e != null && this.e.length > 0) {
            for (int i5 = 0; i5 < this.e.length; i5++) {
                j jVar = this.e[i5];
                if (jVar != null) {
                    dtVar.a(4, jVar);
                }
            }
        }
        if (this.f != null && this.f.length > 0) {
            for (int i6 = 0; i6 < this.f.length; i6++) {
                j jVar2 = this.f[i6];
                if (jVar2 != null) {
                    dtVar.a(5, jVar2);
                }
            }
        }
        if (this.g != null && this.g.length > 0) {
            for (int i7 = 0; i7 < this.g.length; i7++) {
                j jVar3 = this.g[i7];
                if (jVar3 != null) {
                    dtVar.a(6, jVar3);
                }
            }
        }
        if (this.h != null && this.h.length > 0) {
            for (int i8 = 0; i8 < this.h.length; i8++) {
                o oVar = this.h[i8];
                if (oVar != null) {
                    dtVar.a(7, oVar);
                }
            }
        }
        if (!this.i.equals("")) {
            dtVar.a(9, this.i);
        }
        if (!this.j.equals("")) {
            dtVar.a(10, this.j);
        }
        if (!this.k.equals("0")) {
            dtVar.a(12, this.k);
        }
        if (!this.l.equals("")) {
            dtVar.a(13, this.l);
        }
        if (this.m != null) {
            dtVar.a(14, this.m);
        }
        if (Float.floatToIntBits(this.n) != Float.floatToIntBits(0.0f)) {
            dtVar.a(15, this.n);
        }
        if (this.p != null && this.p.length > 0) {
            for (int i9 = 0; i9 < this.p.length; i9++) {
                String str2 = this.p[i9];
                if (str2 != null) {
                    dtVar.a(16, str2);
                }
            }
        }
        if (this.q != 0) {
            dtVar.a(17, this.q);
        }
        if (this.o) {
            dtVar.a(18, this.o);
        }
        if (this.a != null && this.a.length > 0) {
            for (int i10 = 0; i10 < this.a.length; i10++) {
                String str3 = this.a[i10];
                if (str3 != null) {
                    dtVar.a(19, str3);
                }
            }
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int i2;
        int b2 = super.b();
        if (this.b == null || this.b.length <= 0) {
            i2 = b2;
        } else {
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 < this.b.length; i5++) {
                String str = this.b[i5];
                if (str != null) {
                    i4++;
                    i3 += dt.b(str);
                }
            }
            i2 = b2 + i3 + (i4 * 1);
        }
        if (this.c != null && this.c.length > 0) {
            int i6 = i2;
            for (int i7 = 0; i7 < this.c.length; i7++) {
                y yVar = this.c[i7];
                if (yVar != null) {
                    i6 += dt.b(2, yVar);
                }
            }
            i2 = i6;
        }
        if (this.d != null && this.d.length > 0) {
            int i8 = i2;
            for (int i9 = 0; i9 < this.d.length; i9++) {
                m mVar = this.d[i9];
                if (mVar != null) {
                    i8 += dt.b(3, mVar);
                }
            }
            i2 = i8;
        }
        if (this.e != null && this.e.length > 0) {
            int i10 = i2;
            for (int i11 = 0; i11 < this.e.length; i11++) {
                j jVar = this.e[i11];
                if (jVar != null) {
                    i10 += dt.b(4, jVar);
                }
            }
            i2 = i10;
        }
        if (this.f != null && this.f.length > 0) {
            int i12 = i2;
            for (int i13 = 0; i13 < this.f.length; i13++) {
                j jVar2 = this.f[i13];
                if (jVar2 != null) {
                    i12 += dt.b(5, jVar2);
                }
            }
            i2 = i12;
        }
        if (this.g != null && this.g.length > 0) {
            int i14 = i2;
            for (int i15 = 0; i15 < this.g.length; i15++) {
                j jVar3 = this.g[i15];
                if (jVar3 != null) {
                    i14 += dt.b(6, jVar3);
                }
            }
            i2 = i14;
        }
        if (this.h != null && this.h.length > 0) {
            int i16 = i2;
            for (int i17 = 0; i17 < this.h.length; i17++) {
                o oVar = this.h[i17];
                if (oVar != null) {
                    i16 += dt.b(7, oVar);
                }
            }
            i2 = i16;
        }
        if (!this.i.equals("")) {
            i2 += dt.b(9, this.i);
        }
        if (!this.j.equals("")) {
            i2 += dt.b(10, this.j);
        }
        if (!this.k.equals("0")) {
            i2 += dt.b(12, this.k);
        }
        if (!this.l.equals("")) {
            i2 += dt.b(13, this.l);
        }
        if (this.m != null) {
            i2 += dt.b(14, this.m);
        }
        if (Float.floatToIntBits(this.n) != Float.floatToIntBits(0.0f)) {
            i2 += dt.b(15, this.n);
        }
        if (this.p != null && this.p.length > 0) {
            int i18 = 0;
            int i19 = 0;
            for (int i20 = 0; i20 < this.p.length; i20++) {
                String str2 = this.p[i20];
                if (str2 != null) {
                    i19++;
                    i18 += dt.b(str2);
                }
            }
            i2 = i2 + i18 + (i19 * 2);
        }
        if (this.q != 0) {
            i2 += dt.b(17, this.q);
        }
        if (this.o) {
            i2 += dt.b(18, this.o);
        }
        if (this.a != null && this.a.length > 0) {
            int i21 = 0;
            int i22 = 0;
            for (int i23 = 0; i23 < this.a.length; i23++) {
                String str3 = this.a[i23];
                if (str3 != null) {
                    i22++;
                    i21 += dt.b(str3);
                }
            }
            i2 = i2 + i21 + (i22 * 2);
        }
        this.s = i2;
        return i2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        if (!dy.a(this.a, nVar.a) || !dy.a(this.b, nVar.b) || !dy.a(this.c, nVar.c) || !dy.a(this.d, nVar.d) || !dy.a(this.e, nVar.e) || !dy.a(this.f, nVar.f) || !dy.a(this.g, nVar.g) || !dy.a(this.h, nVar.h)) {
            return false;
        }
        if (this.i == null) {
            if (nVar.i != null) {
                return false;
            }
        } else if (!this.i.equals(nVar.i)) {
            return false;
        }
        if (this.j == null) {
            if (nVar.j != null) {
                return false;
            }
        } else if (!this.j.equals(nVar.j)) {
            return false;
        }
        if (this.k == null) {
            if (nVar.k != null) {
                return false;
            }
        } else if (!this.k.equals(nVar.k)) {
            return false;
        }
        if (this.l == null) {
            if (nVar.l != null) {
                return false;
            }
        } else if (!this.l.equals(nVar.l)) {
            return false;
        }
        if (this.m == null) {
            if (nVar.m != null) {
                return false;
            }
        } else if (!this.m.equals(nVar.m)) {
            return false;
        }
        if (Float.floatToIntBits(this.n) == Float.floatToIntBits(nVar.n) && this.o == nVar.o && dy.a(this.p, nVar.p) && this.q == nVar.q) {
            return (this.r == null || this.r.isEmpty()) ? nVar.r == null || nVar.r.isEmpty() : this.r.equals(nVar.r);
        }
        return false;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((((((this.o ? 1231 : 1237) + (((((this.m == null ? 0 : this.m.hashCode()) + (((this.l == null ? 0 : this.l.hashCode()) + (((this.k == null ? 0 : this.k.hashCode()) + (((this.j == null ? 0 : this.j.hashCode()) + (((this.i == null ? 0 : this.i.hashCode()) + ((((((((((((((((dy.a(this.a) + 527) * 31) + dy.a(this.b)) * 31) + dy.a(this.c)) * 31) + dy.a(this.d)) * 31) + dy.a(this.e)) * 31) + dy.a(this.f)) * 31) + dy.a(this.g)) * 31) + dy.a(this.h)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + Float.floatToIntBits(this.n)) * 31)) * 31) + dy.a(this.p)) * 31) + this.q) * 31;
        if (this.r != null && !this.r.isEmpty()) {
            i2 = this.r.hashCode();
        }
        return hashCode + i2;
    }
}
