package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

/* renamed from: com.google.android.gms.internal.do  reason: invalid class name */
public class Cdo implements Parcelable.Creator {
    static void a(jw jwVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, jwVar.a());
        c.a(parcel, 2, jwVar.a, false);
        c.a(parcel, 3, jwVar.b, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public jw createFromParcel(Parcel parcel) {
        String str = null;
        int b = a.b(parcel);
        int i = 0;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    str2 = a.n(parcel, a);
                    break;
                case 3:
                    str = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new jw(i, str2, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public jw[] newArray(int i) {
        return new jw[i];
    }
}
