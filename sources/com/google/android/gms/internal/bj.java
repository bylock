package com.google.android.gms.internal;

import android.content.Context;

public final class bj {
    public static boolean a(Context context) {
        return context.getPackageManager().hasSystemFeature("android.hardware.type.watch");
    }
}
