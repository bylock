package com.google.android.gms.internal;

import android.support.v7.b.k;

public final class p extends dv {
    public static final dw a = dw.a(11, p.class, 810);
    private static final p[] i = new p[0];
    public int[] b;
    public int[] c;
    public int[] d;
    public int e;
    public int[] f;
    public int g;
    public int h;

    public p() {
        a();
    }

    public p a() {
        this.b = ed.a;
        this.c = ed.a;
        this.d = ed.a;
        this.e = 0;
        this.f = ed.a;
        this.g = 0;
        this.h = 0;
        this.r = null;
        this.s = -1;
        return this;
    }

    /* renamed from: a */
    public p b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int a3 = ed.a(dsVar, 8);
                    int length = this.b == null ? 0 : this.b.length;
                    int[] iArr = new int[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, iArr, 0, length);
                    }
                    while (length < iArr.length - 1) {
                        iArr[length] = dsVar.e();
                        dsVar.a();
                        length++;
                    }
                    iArr[length] = dsVar.e();
                    this.b = iArr;
                    break;
                case 10:
                    int c2 = dsVar.c(dsVar.i());
                    int o = dsVar.o();
                    int i2 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i2++;
                    }
                    dsVar.e(o);
                    int length2 = this.b == null ? 0 : this.b.length;
                    int[] iArr2 = new int[(i2 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.b, 0, iArr2, 0, length2);
                    }
                    while (length2 < iArr2.length) {
                        iArr2[length2] = dsVar.e();
                        length2++;
                    }
                    this.b = iArr2;
                    dsVar.d(c2);
                    break;
                case 16:
                    int a4 = ed.a(dsVar, 16);
                    int length3 = this.c == null ? 0 : this.c.length;
                    int[] iArr3 = new int[(a4 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.c, 0, iArr3, 0, length3);
                    }
                    while (length3 < iArr3.length - 1) {
                        iArr3[length3] = dsVar.e();
                        dsVar.a();
                        length3++;
                    }
                    iArr3[length3] = dsVar.e();
                    this.c = iArr3;
                    break;
                case k.ActionBar_itemPadding:
                    int c3 = dsVar.c(dsVar.i());
                    int o2 = dsVar.o();
                    int i3 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i3++;
                    }
                    dsVar.e(o2);
                    int length4 = this.c == null ? 0 : this.c.length;
                    int[] iArr4 = new int[(i3 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.c, 0, iArr4, 0, length4);
                    }
                    while (length4 < iArr4.length) {
                        iArr4[length4] = dsVar.e();
                        length4++;
                    }
                    this.c = iArr4;
                    dsVar.d(c3);
                    break;
                case 24:
                    int a5 = ed.a(dsVar, 24);
                    int length5 = this.d == null ? 0 : this.d.length;
                    int[] iArr5 = new int[(a5 + length5)];
                    if (length5 != 0) {
                        System.arraycopy(this.d, 0, iArr5, 0, length5);
                    }
                    while (length5 < iArr5.length - 1) {
                        iArr5[length5] = dsVar.e();
                        dsVar.a();
                        length5++;
                    }
                    iArr5[length5] = dsVar.e();
                    this.d = iArr5;
                    break;
                case 26:
                    int c4 = dsVar.c(dsVar.i());
                    int o3 = dsVar.o();
                    int i4 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i4++;
                    }
                    dsVar.e(o3);
                    int length6 = this.d == null ? 0 : this.d.length;
                    int[] iArr6 = new int[(i4 + length6)];
                    if (length6 != 0) {
                        System.arraycopy(this.d, 0, iArr6, 0, length6);
                    }
                    while (length6 < iArr6.length) {
                        iArr6[length6] = dsVar.e();
                        length6++;
                    }
                    this.d = iArr6;
                    dsVar.d(c4);
                    break;
                case 32:
                    this.e = dsVar.e();
                    break;
                case 40:
                    int a6 = ed.a(dsVar, 40);
                    int length7 = this.f == null ? 0 : this.f.length;
                    int[] iArr7 = new int[(a6 + length7)];
                    if (length7 != 0) {
                        System.arraycopy(this.f, 0, iArr7, 0, length7);
                    }
                    while (length7 < iArr7.length - 1) {
                        iArr7[length7] = dsVar.e();
                        dsVar.a();
                        length7++;
                    }
                    iArr7[length7] = dsVar.e();
                    this.f = iArr7;
                    break;
                case 42:
                    int c5 = dsVar.c(dsVar.i());
                    int o4 = dsVar.o();
                    int i5 = 0;
                    while (dsVar.m() > 0) {
                        dsVar.e();
                        i5++;
                    }
                    dsVar.e(o4);
                    int length8 = this.f == null ? 0 : this.f.length;
                    int[] iArr8 = new int[(i5 + length8)];
                    if (length8 != 0) {
                        System.arraycopy(this.f, 0, iArr8, 0, length8);
                    }
                    while (length8 < iArr8.length) {
                        iArr8[length8] = dsVar.e();
                        length8++;
                    }
                    this.f = iArr8;
                    dsVar.d(c5);
                    break;
                case 48:
                    this.g = dsVar.e();
                    break;
                case 56:
                    this.h = dsVar.e();
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (this.b != null && this.b.length > 0) {
            for (int i2 = 0; i2 < this.b.length; i2++) {
                dtVar.a(1, this.b[i2]);
            }
        }
        if (this.c != null && this.c.length > 0) {
            for (int i3 = 0; i3 < this.c.length; i3++) {
                dtVar.a(2, this.c[i3]);
            }
        }
        if (this.d != null && this.d.length > 0) {
            for (int i4 = 0; i4 < this.d.length; i4++) {
                dtVar.a(3, this.d[i4]);
            }
        }
        if (this.e != 0) {
            dtVar.a(4, this.e);
        }
        if (this.f != null && this.f.length > 0) {
            for (int i5 = 0; i5 < this.f.length; i5++) {
                dtVar.a(5, this.f[i5]);
            }
        }
        if (this.g != 0) {
            dtVar.a(6, this.g);
        }
        if (this.h != 0) {
            dtVar.a(7, this.h);
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int i2;
        int b2 = super.b();
        if (this.b == null || this.b.length <= 0) {
            i2 = b2;
        } else {
            int i3 = 0;
            for (int i4 = 0; i4 < this.b.length; i4++) {
                i3 += dt.b(this.b[i4]);
            }
            i2 = b2 + i3 + (this.b.length * 1);
        }
        if (this.c != null && this.c.length > 0) {
            int i5 = 0;
            for (int i6 = 0; i6 < this.c.length; i6++) {
                i5 += dt.b(this.c[i6]);
            }
            i2 = i2 + i5 + (this.c.length * 1);
        }
        if (this.d != null && this.d.length > 0) {
            int i7 = 0;
            for (int i8 = 0; i8 < this.d.length; i8++) {
                i7 += dt.b(this.d[i8]);
            }
            i2 = i2 + i7 + (this.d.length * 1);
        }
        if (this.e != 0) {
            i2 += dt.b(4, this.e);
        }
        if (this.f != null && this.f.length > 0) {
            int i9 = 0;
            for (int i10 = 0; i10 < this.f.length; i10++) {
                i9 += dt.b(this.f[i10]);
            }
            i2 = i2 + i9 + (this.f.length * 1);
        }
        if (this.g != 0) {
            i2 += dt.b(6, this.g);
        }
        if (this.h != 0) {
            i2 += dt.b(7, this.h);
        }
        this.s = i2;
        return i2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof p)) {
            return false;
        }
        p pVar = (p) obj;
        if (!dy.a(this.b, pVar.b) || !dy.a(this.c, pVar.c) || !dy.a(this.d, pVar.d) || this.e != pVar.e || !dy.a(this.f, pVar.f) || this.g != pVar.g || this.h != pVar.h) {
            return false;
        }
        return (this.r == null || this.r.isEmpty()) ? pVar.r == null || pVar.r.isEmpty() : this.r.equals(pVar.r);
    }

    public int hashCode() {
        return ((this.r == null || this.r.isEmpty()) ? 0 : this.r.hashCode()) + ((((((((((((((dy.a(this.b) + 527) * 31) + dy.a(this.c)) * 31) + dy.a(this.d)) * 31) + this.e) * 31) + dy.a(this.f)) * 31) + this.g) * 31) + this.h) * 31);
    }
}
