package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.List;

public final class ah implements SafeParcelable {
    public static final b a = new b();
    public final int b;
    public final long c;
    public final Bundle d;
    public final int e;
    public final List f;
    public final boolean g;
    public final int h;
    public final boolean i;
    public final String j;
    public final av k;
    public final Location l;
    public final String m;

    public ah(int i2, long j2, Bundle bundle, int i3, List list, boolean z, int i4, boolean z2, String str, av avVar, Location location, String str2) {
        this.b = i2;
        this.c = j2;
        this.d = bundle;
        this.e = i3;
        this.f = list;
        this.g = z;
        this.h = i4;
        this.i = z2;
        this.j = str;
        this.k = avVar;
        this.l = location;
        this.m = str2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        b.a(this, parcel, i2);
    }
}
