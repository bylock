package com.google.android.gms.internal;

import java.io.IOException;

public abstract class ea {
    protected volatile int s = -1;

    public static final ea a(ea eaVar, byte[] bArr) {
        return b(eaVar, bArr, 0, bArr.length);
    }

    public static final void a(ea eaVar, byte[] bArr, int i, int i2) {
        try {
            dt a = dt.a(bArr, i, i2);
            eaVar.a(a);
            a.b();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final byte[] a(ea eaVar) {
        byte[] bArr = new byte[eaVar.b()];
        a(eaVar, bArr, 0, bArr.length);
        return bArr;
    }

    public static final ea b(ea eaVar, byte[] bArr, int i, int i2) {
        try {
            ds a = ds.a(bArr, i, i2);
            eaVar.b(a);
            a.a(0);
            return eaVar;
        } catch (dz e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).");
        }
    }

    public void a(dt dtVar) {
    }

    public int b() {
        int d = d();
        this.s = d;
        return d;
    }

    public abstract ea b(ds dsVar);

    /* access modifiers changed from: protected */
    public int d() {
        return 0;
    }

    public int e() {
        if (this.s < 0) {
            b();
        }
        return this.s;
    }

    public String toString() {
        return eb.a(this);
    }
}
