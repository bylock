package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.concurrent.TimeUnit;

public final class hk implements SafeParcelable {
    public static final cm a = new cm();
    static final long b = TimeUnit.HOURS.toMillis(1);
    final int c;
    private final hg d;
    private final long e;
    private final int f;

    public hk(int i, hg hgVar, long j, int i2) {
        this.c = i;
        this.d = hgVar;
        this.e = j;
        this.f = i2;
    }

    public hg a() {
        return this.d;
    }

    public long b() {
        return this.e;
    }

    public int c() {
        return this.f;
    }

    public int describeContents() {
        cm cmVar = a;
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hk)) {
            return false;
        }
        hk hkVar = (hk) obj;
        return this.d.equals(hkVar.d) && this.e == hkVar.e && this.f == hkVar.f;
    }

    public int hashCode() {
        return ar.a(this.d, Long.valueOf(this.e), Integer.valueOf(this.f));
    }

    public String toString() {
        return ar.a(this).a("filter", this.d).a("interval", Long.valueOf(this.e)).a("priority", Integer.valueOf(this.f)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        cm cmVar = a;
        cm.a(this, parcel, i);
    }
}
