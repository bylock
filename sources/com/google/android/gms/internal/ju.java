package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class ju implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dn();
    long a;
    long b;
    private final int c;

    ju() {
        this.c = 1;
    }

    ju(int i, long j, long j2) {
        this.c = i;
        this.a = j;
        this.b = j2;
    }

    public int a() {
        return this.c;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        dn.a(this, parcel, i);
    }
}
