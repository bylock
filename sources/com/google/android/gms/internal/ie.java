package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ga;
import com.google.android.gms.plus.a.a.b;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public final class ie extends ga implements SafeParcelable, b {
    public static final cr a = new cr();
    private static final HashMap b = new HashMap();
    private final Set c;
    private final int d;
    private String e;
    private ic f;
    private String g;
    private ic h;
    private String i;

    static {
        b.put("id", ga.a.d("id", 2));
        b.put("result", ga.a.a("result", 4, ic.class));
        b.put("startDate", ga.a.d("startDate", 5));
        b.put("target", ga.a.a("target", 6, ic.class));
        b.put("type", ga.a.d("type", 7));
    }

    public ie() {
        this.d = 1;
        this.c = new HashSet();
    }

    ie(Set set, int i2, String str, ic icVar, String str2, ic icVar2, String str3) {
        this.c = set;
        this.d = i2;
        this.e = str;
        this.f = icVar;
        this.g = str2;
        this.h = icVar2;
        this.i = str3;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public Object a(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public boolean a(ga.a aVar) {
        return this.c.contains(Integer.valueOf(aVar.g()));
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public Object b(ga.a aVar) {
        switch (aVar.g()) {
            case 2:
                return this.e;
            case 3:
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + aVar.g());
            case 4:
                return this.f;
            case 5:
                return this.g;
            case 6:
                return this.h;
            case 7:
                return this.i;
        }
    }

    @Override // com.google.android.gms.internal.ga
    public HashMap b() {
        return b;
    }

    /* access modifiers changed from: protected */
    @Override // com.google.android.gms.internal.ga
    public boolean b(String str) {
        return false;
    }

    public int describeContents() {
        cr crVar = a;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public Set e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ie)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ie ieVar = (ie) obj;
        for (ga.a aVar : b.values()) {
            if (a(aVar)) {
                if (!ieVar.a(aVar)) {
                    return false;
                }
                if (!b(aVar).equals(ieVar.b(aVar))) {
                    return false;
                }
            } else if (ieVar.a(aVar)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.d;
    }

    public String g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public ic h() {
        return this.f;
    }

    public int hashCode() {
        int i2 = 0;
        for (ga.a aVar : b.values()) {
            i2 = a(aVar) ? b(aVar).hashCode() + i2 + aVar.g() : i2;
        }
        return i2;
    }

    public String i() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public ic j() {
        return this.h;
    }

    public String k() {
        return this.i;
    }

    /* renamed from: l */
    public ie a() {
        return this;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        cr crVar = a;
        cr.a(this, parcel, i2);
    }
}
