package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class cj implements Parcelable.Creator {
    static void a(hd hdVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, hdVar.f(), false);
        c.a(parcel, 1000, hdVar.a());
        c.a(parcel, 2, hdVar.g());
        c.a(parcel, 3, hdVar.b());
        c.a(parcel, 4, hdVar.c());
        c.a(parcel, 5, hdVar.d());
        c.a(parcel, 6, hdVar.e());
        c.a(parcel, 7, hdVar.h());
        c.a(parcel, 8, hdVar.i());
        c.a(parcel, 9, hdVar.j());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public hd createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        String str = null;
        int i2 = 0;
        short s = 0;
        double d = 0.0d;
        double d2 = 0.0d;
        float f = 0.0f;
        long j = 0;
        int i3 = 0;
        int i4 = -1;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str = a.n(parcel, a);
                    break;
                case 2:
                    j = a.i(parcel, a);
                    break;
                case 3:
                    s = a.f(parcel, a);
                    break;
                case 4:
                    d = a.l(parcel, a);
                    break;
                case 5:
                    d2 = a.l(parcel, a);
                    break;
                case 6:
                    f = a.k(parcel, a);
                    break;
                case 7:
                    i2 = a.g(parcel, a);
                    break;
                case 8:
                    i3 = a.g(parcel, a);
                    break;
                case 9:
                    i4 = a.g(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new hd(i, str, i2, s, d, d2, f, j, i3, i4);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public hd[] newArray(int i) {
        return new hd[i];
    }
}
