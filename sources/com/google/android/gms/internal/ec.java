package com.google.android.gms.internal;

import java.util.Arrays;

public final class ec {
    final int a;
    final byte[] b;

    ec(int i, byte[] bArr) {
        this.a = i;
        this.b = bArr;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ec)) {
            return false;
        }
        ec ecVar = (ec) obj;
        return this.a == ecVar.a && Arrays.equals(this.b, ecVar.b);
    }

    public int hashCode() {
        return ((this.a + 527) * 31) + Arrays.hashCode(this.b);
    }
}
