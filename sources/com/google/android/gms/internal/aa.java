package com.google.android.gms.internal;

import android.util.Log;

public final class aa {
    public static void a(String str) {
        if (a(3)) {
            Log.d("Ads", str);
        }
    }

    public static boolean a(int i) {
        return (i >= 5 || Log.isLoggable("Ads", i)) && i != 2;
    }

    public static void b(String str) {
        if (a(5)) {
            Log.w("Ads", str);
        }
    }
}
