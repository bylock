package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.ArrayList;
import java.util.List;

public final class fc {

    public final class a implements SafeParcelable {
        public static final au a = new au();
        private final int b;
        private final String c;
        private final List d = new ArrayList();
        private final int e;
        private final String f;

        a(int i, String str, List list, int i2, String str2) {
            this.b = i;
            this.c = str;
            this.d.addAll(list);
            this.e = i2;
            this.f = str2;
        }

        public String a() {
            return this.c;
        }

        public int b() {
            return this.e;
        }

        public String c() {
            return this.f;
        }

        public List d() {
            return new ArrayList(this.d);
        }

        public int describeContents() {
            return 0;
        }

        public int e() {
            return this.b;
        }

        public void writeToParcel(Parcel parcel, int i) {
            au.a(this, parcel, i);
        }
    }
}
