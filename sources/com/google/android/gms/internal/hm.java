package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class hm implements SafeParcelable {
    public static final hm A = a("courthouse");
    public static final hm B = a("dentist");
    public static final hm C = a("department_store");
    public static final hm D = a("doctor");
    public static final hm E = a("electrician");
    public static final hm F = a("electronics_store");
    public static final hm G = a("embassy");
    public static final hm H = a("establishment");
    public static final hm I = a("finance");
    public static final hm J = a("fire_station");
    public static final hm K = a("florist");
    public static final hm L = a("food");
    public static final hm M = a("funeral_home");
    public static final hm N = a("furniture_store");
    public static final hm O = a("gas_station");
    public static final hm P = a("general_contractor");
    public static final hm Q = a("grocery_or_supermarket");
    public static final hm R = a("gym");
    public static final hm S = a("hair_care");
    public static final hm T = a("hardware_store");
    public static final hm U = a("health");
    public static final hm V = a("hindu_temple");
    public static final hm W = a("home_goods_store");
    public static final hm X = a("hospital");
    public static final hm Y = a("insurance_agency");
    public static final hm Z = a("jewelry_store");
    public static final hm a = a("accounting");
    public static final hm aA = a("restaurant");
    public static final hm aB = a("roofing_contractor");
    public static final hm aC = a("rv_park");
    public static final hm aD = a("school");
    public static final hm aE = a("shoe_store");
    public static final hm aF = a("shopping_mall");
    public static final hm aG = a("spa");
    public static final hm aH = a("stadium");
    public static final hm aI = a("storage");
    public static final hm aJ = a("store");
    public static final hm aK = a("subway_station");
    public static final hm aL = a("synagogue");
    public static final hm aM = a("taxi_stand");
    public static final hm aN = a("train_station");
    public static final hm aO = a("travel_agency");
    public static final hm aP = a("university");
    public static final hm aQ = a("veterinary_care");
    public static final hm aR = a("zoo");
    public static final hm aS = a("administrative_area_level_1");
    public static final hm aT = a("administrative_area_level_2");
    public static final hm aU = a("administrative_area_level_3");
    public static final hm aV = a("colloquial_area");
    public static final hm aW = a("country");
    public static final hm aX = a("floor");
    public static final hm aY = a("geocode");
    public static final hm aZ = a("intersection");
    public static final hm aa = a("laundry");
    public static final hm ab = a("lawyer");
    public static final hm ac = a("library");
    public static final hm ad = a("liquor_store");
    public static final hm ae = a("local_government_office");
    public static final hm af = a("locksmith");
    public static final hm ag = a("lodging");
    public static final hm ah = a("meal_delivery");
    public static final hm ai = a("meal_takeaway");
    public static final hm aj = a("mosque");
    public static final hm ak = a("movie_rental");
    public static final hm al = a("movie_theater");
    public static final hm am = a("moving_company");
    public static final hm an = a("museum");
    public static final hm ao = a("night_club");
    public static final hm ap = a("painter");
    public static final hm aq = a("park");
    public static final hm ar = a("parking");
    public static final hm as = a("pet_store");
    public static final hm at = a("pharmacy");
    public static final hm au = a("physiotherapist");
    public static final hm av = a("place_of_worship");
    public static final hm aw = a("plumber");
    public static final hm ax = a("police");
    public static final hm ay = a("post_office");
    public static final hm az = a("real_estate_agency");
    public static final hm b = a("airport");
    public static final hm ba = a("locality");
    public static final hm bb = a("natural_feature");
    public static final hm bc = a("neighborhood");
    public static final hm bd = a("political");
    public static final hm be = a("point_of_interest");
    public static final hm bf = a("post_box");
    public static final hm bg = a("postal_code");
    public static final hm bh = a("postal_code_prefix");
    public static final hm bi = a("postal_town");
    public static final hm bj = a("premise");
    public static final hm bk = a("room");
    public static final hm bl = a("route");
    public static final hm bm = a("street_address");
    public static final hm bn = a("sublocality");
    public static final hm bo = a("sublocality_level_1");
    public static final hm bp = a("sublocality_level_2");
    public static final hm bq = a("sublocality_level_3");
    public static final hm br = a("sublocality_level_4");
    public static final hm bs = a("sublocality_level_5");
    public static final hm bt = a("subpremise");
    public static final hm bu = a("transit_station");
    public static final hm bv = a("other");
    public static final cn bw = new cn();
    public static final hm c = a("amusement_park");
    public static final hm d = a("aquarium");
    public static final hm e = a("art_gallery");
    public static final hm f = a("atm");
    public static final hm g = a("bakery");
    public static final hm h = a("bank");
    public static final hm i = a("bar");
    public static final hm j = a("beauty_salon");
    public static final hm k = a("bicycle_store");
    public static final hm l = a("book_store");
    public static final hm m = a("bowling_alley");
    public static final hm n = a("bus_station");
    public static final hm o = a("cafe");
    public static final hm p = a("campground");
    public static final hm q = a("car_dealer");
    public static final hm r = a("car_rental");
    public static final hm s = a("car_repair");
    public static final hm t = a("car_wash");
    public static final hm u = a("casino");
    public static final hm v = a("cemetery");
    public static final hm w = a("church");
    public static final hm x = a("city_hall");
    public static final hm y = a("clothing_store");
    public static final hm z = a("convenience_store");
    final int bx;
    final String by;

    hm(int i2, String str) {
        aw.a(str);
        this.bx = i2;
        this.by = str;
    }

    public static hm a(String str) {
        return new hm(0, str);
    }

    public int describeContents() {
        cn cnVar = bw;
        return 0;
    }

    public boolean equals(Object obj) {
        return (obj instanceof hm) && this.by.equals(((hm) obj).by);
    }

    public int hashCode() {
        return this.by.hashCode();
    }

    public String toString() {
        return this.by;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        cn cnVar = bw;
        cn.a(this, parcel, i2);
    }
}
