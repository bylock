package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class cm implements Parcelable.Creator {
    static void a(hk hkVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1000, hkVar.c);
        c.a(parcel, 2, (Parcelable) hkVar.a(), i, false);
        c.a(parcel, 3, hkVar.b());
        c.a(parcel, 4, hkVar.c());
        c.a(parcel, a);
    }

    /* renamed from: a */
    public hk createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        hg hgVar = null;
        long j = hk.b;
        int i2 = 102;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 2:
                    hgVar = (hg) a.a(parcel, a, hg.a);
                    break;
                case 3:
                    j = a.i(parcel, a);
                    break;
                case 4:
                    i2 = a.g(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new hk(i, hgVar, j, i2);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public hk[] newArray(int i) {
        return new hk[i];
    }
}
