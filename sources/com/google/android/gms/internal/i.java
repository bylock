package com.google.android.gms.internal;

public final class i extends dv {
    public int a;
    public int b;
    public int c;

    public i() {
        a();
    }

    public i a() {
        this.a = 1;
        this.b = 0;
        this.c = 0;
        this.r = null;
        this.s = -1;
        return this;
    }

    /* renamed from: a */
    public i b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int e = dsVar.e();
                    switch (e) {
                        case 1:
                        case 2:
                        case 3:
                            this.a = e;
                            continue;
                    }
                case 16:
                    this.b = dsVar.e();
                    break;
                case 24:
                    this.c = dsVar.e();
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        if (this.a != 1) {
            dtVar.a(1, this.a);
        }
        if (this.b != 0) {
            dtVar.a(2, this.b);
        }
        if (this.c != 0) {
            dtVar.a(3, this.c);
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b();
        if (this.a != 1) {
            b2 += dt.b(1, this.a);
        }
        if (this.b != 0) {
            b2 += dt.b(2, this.b);
        }
        if (this.c != 0) {
            b2 += dt.b(3, this.c);
        }
        this.s = b2;
        return b2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof i)) {
            return false;
        }
        i iVar = (i) obj;
        if (this.a == iVar.a && this.b == iVar.b && this.c == iVar.c) {
            return (this.r == null || this.r.isEmpty()) ? iVar.r == null || iVar.r.isEmpty() : this.r.equals(iVar.r);
        }
        return false;
    }

    public int hashCode() {
        return ((this.r == null || this.r.isEmpty()) ? 0 : this.r.hashCode()) + ((((((this.a + 527) * 31) + this.b) * 31) + this.c) * 31);
    }
}
