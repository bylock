package com.google.android.gms.internal;

import android.support.v7.b.k;

public final class df extends dv {
    public long a;
    public n b;
    public r c;

    public df() {
        a();
    }

    public static df a(byte[] bArr) {
        return (df) ea.a(new df(), bArr);
    }

    public df a() {
        this.a = 0;
        this.b = null;
        this.c = null;
        this.r = null;
        this.s = -1;
        return this;
    }

    /* renamed from: a */
    public df b(ds dsVar) {
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    this.a = dsVar.d();
                    break;
                case k.ActionBar_itemPadding:
                    if (this.b == null) {
                        this.b = new n();
                    }
                    dsVar.a(this.b);
                    break;
                case 26:
                    if (this.c == null) {
                        this.c = new r();
                    }
                    dsVar.a(this.c);
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        dtVar.a(1, this.a);
        if (this.b != null) {
            dtVar.a(2, this.b);
        }
        if (this.c != null) {
            dtVar.a(3, this.c);
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b() + dt.c(1, this.a);
        if (this.b != null) {
            b2 += dt.b(2, this.b);
        }
        if (this.c != null) {
            b2 += dt.b(3, this.c);
        }
        this.s = b2;
        return b2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof df)) {
            return false;
        }
        df dfVar = (df) obj;
        if (this.a != dfVar.a) {
            return false;
        }
        if (this.b == null) {
            if (dfVar.b != null) {
                return false;
            }
        } else if (!this.b.equals(dfVar.b)) {
            return false;
        }
        if (this.c == null) {
            if (dfVar.c != null) {
                return false;
            }
        } else if (!this.c.equals(dfVar.c)) {
            return false;
        }
        return (this.r == null || this.r.isEmpty()) ? dfVar.r == null || dfVar.r.isEmpty() : this.r.equals(dfVar.r);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.c == null ? 0 : this.c.hashCode()) + (((this.b == null ? 0 : this.b.hashCode()) + ((((int) (this.a ^ (this.a >>> 32))) + 527) * 31)) * 31)) * 31;
        if (this.r != null && !this.r.isEmpty()) {
            i = this.r.hashCode();
        }
        return hashCode + i;
    }
}
