package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class jp implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dk();
    int a;
    String b;
    double c;
    String d;
    long e;
    int f;
    private final int g;

    jp() {
        this.g = 1;
        this.f = -1;
        this.a = -1;
        this.c = -1.0d;
    }

    jp(int i, int i2, String str, double d2, String str2, long j, int i3) {
        this.g = i;
        this.a = i2;
        this.b = str;
        this.c = d2;
        this.d = str2;
        this.e = j;
        this.f = i3;
    }

    public int a() {
        return this.g;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        dk.a(this, parcel, i);
    }
}
