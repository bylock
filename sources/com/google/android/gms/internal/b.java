package com.google.android.gms.internal;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.c;
import java.util.ArrayList;

public class b implements Parcelable.Creator {
    static void a(ah ahVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, ahVar.b);
        c.a(parcel, 2, ahVar.c);
        c.a(parcel, 3, ahVar.d, false);
        c.a(parcel, 4, ahVar.e);
        c.a(parcel, 5, ahVar.f, false);
        c.a(parcel, 6, ahVar.g);
        c.a(parcel, 7, ahVar.h);
        c.a(parcel, 8, ahVar.i);
        c.a(parcel, 9, ahVar.j, false);
        c.a(parcel, 10, (Parcelable) ahVar.k, i, false);
        c.a(parcel, 11, (Parcelable) ahVar.l, i, false);
        c.a(parcel, 12, ahVar.m, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ah createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        long j = 0;
        Bundle bundle = null;
        int i2 = 0;
        ArrayList arrayList = null;
        boolean z = false;
        int i3 = 0;
        boolean z2 = false;
        String str = null;
        av avVar = null;
        Location location = null;
        String str2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    j = a.i(parcel, a);
                    break;
                case 3:
                    bundle = a.p(parcel, a);
                    break;
                case 4:
                    i2 = a.g(parcel, a);
                    break;
                case 5:
                    arrayList = a.A(parcel, a);
                    break;
                case 6:
                    z = a.c(parcel, a);
                    break;
                case 7:
                    i3 = a.g(parcel, a);
                    break;
                case 8:
                    z2 = a.c(parcel, a);
                    break;
                case 9:
                    str = a.n(parcel, a);
                    break;
                case 10:
                    avVar = (av) a.a(parcel, a, av.a);
                    break;
                case 11:
                    location = (Location) a.a(parcel, a, Location.CREATOR);
                    break;
                case 12:
                    str2 = a.n(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ah(i, j, bundle, i2, arrayList, z, i3, z2, str, avVar, location, str2);
        }
        throw new com.google.android.gms.common.internal.safeparcel.b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ah[] newArray(int i) {
        return new ah[i];
    }
}
