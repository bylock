package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.drive.metadata.g;
import com.google.android.gms.drive.metadata.internal.e;
import com.google.android.gms.drive.metadata.internal.i;
import com.google.android.gms.drive.metadata.internal.j;

public class bp {
    public static final a A = new j("webViewLink", 4300000);
    public static final a a = cg.a;
    public static final a b = new j("alternateLink", 4300000);
    public static final a c = new j("description", 4300000);
    public static final a d = new j("embedLink", 4300000);
    public static final a e = new j("fileExtension", 4300000);
    public static final a f = new e("fileSize", 4300000);
    public static final a g = new com.google.android.gms.drive.metadata.internal.a("hasThumbnail", 4300000);
    public static final a h = new j("indexableText", 4300000);
    public static final a i = new com.google.android.gms.drive.metadata.internal.a("isAppData", 4300000);
    public static final a j = new com.google.android.gms.drive.metadata.internal.a("isCopyable", 4300000);
    public static final a k = new com.google.android.gms.drive.metadata.internal.a("isEditable", 4100000);
    public static final br l = new br("isPinned", 4100000);
    public static final a m = new com.google.android.gms.drive.metadata.internal.a("isRestricted", 4300000);
    public static final a n = new com.google.android.gms.drive.metadata.internal.a("isShared", 4300000);
    public static final a o = new com.google.android.gms.drive.metadata.internal.a("isTrashable", 4400000);
    public static final a p = new com.google.android.gms.drive.metadata.internal.a("isViewed", 4300000);
    public static final bs q = new bs("mimeType", 4100000);
    public static final a r = new j("originalFilename", 4300000);
    public static final g s = new i("ownerNames", 4300000);
    public static final bt t = new bt("parents", 4100000);
    public static final bu u = new bu("quotaBytesUsed", 4300000);
    public static final bv v = new bv("starred", 4100000);
    public static final a w = new bq("thumbnail", 4400000);
    public static final bw x = new bw("title", 4100000);
    public static final bx y = new bx("trashed", 4100000);
    public static final a z = new j("webContentLink", 4300000);
}
