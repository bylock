package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class hi implements SafeParcelable {
    public static final cl a = new cl();
    final int b;
    private final String c;
    private final String d;

    hi(int i, String str, String str2) {
        this.b = i;
        this.c = str;
        this.d = str2;
    }

    public String a() {
        return this.c;
    }

    public String b() {
        return this.d;
    }

    public int describeContents() {
        cl clVar = a;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof hi)) {
            return false;
        }
        hi hiVar = (hi) obj;
        return ar.a(this.c, hiVar.c) && ar.a(this.d, hiVar.d);
    }

    public int hashCode() {
        return ar.a(this.c, this.d);
    }

    public String toString() {
        return ar.a(this).a("mPlaceId", this.c).a("mTag", this.d).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        cl clVar = a;
        cl.a(this, parcel, i);
    }
}
