package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.b.k;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.internal.ih;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class cs implements Parcelable.Creator {
    static void a(ih ihVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        Set e = ihVar.e();
        if (e.contains(1)) {
            c.a(parcel, 1, ihVar.f());
        }
        if (e.contains(2)) {
            c.a(parcel, 2, ihVar.g(), true);
        }
        if (e.contains(3)) {
            c.a(parcel, 3, (Parcelable) ihVar.h(), i, true);
        }
        if (e.contains(4)) {
            c.a(parcel, 4, ihVar.i(), true);
        }
        if (e.contains(5)) {
            c.a(parcel, 5, ihVar.j(), true);
        }
        if (e.contains(6)) {
            c.a(parcel, 6, ihVar.k());
        }
        if (e.contains(7)) {
            c.a(parcel, 7, (Parcelable) ihVar.l(), i, true);
        }
        if (e.contains(8)) {
            c.a(parcel, 8, ihVar.m(), true);
        }
        if (e.contains(9)) {
            c.a(parcel, 9, ihVar.n(), true);
        }
        if (e.contains(12)) {
            c.a(parcel, 12, ihVar.o());
        }
        if (e.contains(14)) {
            c.a(parcel, 14, ihVar.p(), true);
        }
        if (e.contains(15)) {
            c.a(parcel, 15, (Parcelable) ihVar.q(), i, true);
        }
        if (e.contains(16)) {
            c.a(parcel, 16, ihVar.r());
        }
        if (e.contains(19)) {
            c.a(parcel, 19, (Parcelable) ihVar.t(), i, true);
        }
        if (e.contains(18)) {
            c.a(parcel, 18, ihVar.s(), true);
        }
        if (e.contains(21)) {
            c.a(parcel, 21, ihVar.v());
        }
        if (e.contains(20)) {
            c.a(parcel, 20, ihVar.u(), true);
        }
        if (e.contains(23)) {
            c.b(parcel, 23, ihVar.x(), true);
        }
        if (e.contains(22)) {
            c.b(parcel, 22, ihVar.w(), true);
        }
        if (e.contains(25)) {
            c.a(parcel, 25, ihVar.z());
        }
        if (e.contains(24)) {
            c.a(parcel, 24, ihVar.y());
        }
        if (e.contains(27)) {
            c.a(parcel, 27, ihVar.B(), true);
        }
        if (e.contains(26)) {
            c.a(parcel, 26, ihVar.A(), true);
        }
        if (e.contains(29)) {
            c.a(parcel, 29, ihVar.D());
        }
        if (e.contains(28)) {
            c.b(parcel, 28, ihVar.C(), true);
        }
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ih createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        ih.a aVar = null;
        String str2 = null;
        String str3 = null;
        int i2 = 0;
        ih.b bVar = null;
        String str4 = null;
        String str5 = null;
        int i3 = 0;
        String str6 = null;
        ih.c cVar = null;
        boolean z = false;
        String str7 = null;
        ih.d dVar = null;
        String str8 = null;
        int i4 = 0;
        ArrayList arrayList = null;
        ArrayList arrayList2 = null;
        int i5 = 0;
        int i6 = 0;
        String str9 = null;
        String str10 = null;
        ArrayList arrayList3 = null;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    hashSet.add(1);
                    break;
                case 2:
                    str = a.n(parcel, a);
                    hashSet.add(2);
                    break;
                case 3:
                    hashSet.add(3);
                    aVar = (ih.a) a.a(parcel, a, ih.a.a);
                    break;
                case 4:
                    str2 = a.n(parcel, a);
                    hashSet.add(4);
                    break;
                case 5:
                    str3 = a.n(parcel, a);
                    hashSet.add(5);
                    break;
                case 6:
                    i2 = a.g(parcel, a);
                    hashSet.add(6);
                    break;
                case 7:
                    hashSet.add(7);
                    bVar = (ih.b) a.a(parcel, a, ih.b.a);
                    break;
                case 8:
                    str4 = a.n(parcel, a);
                    hashSet.add(8);
                    break;
                case 9:
                    str5 = a.n(parcel, a);
                    hashSet.add(9);
                    break;
                case 10:
                case 11:
                case 13:
                case k.ActionBar_progressBarPadding:
                default:
                    a.b(parcel, a);
                    break;
                case 12:
                    i3 = a.g(parcel, a);
                    hashSet.add(12);
                    break;
                case 14:
                    str6 = a.n(parcel, a);
                    hashSet.add(14);
                    break;
                case 15:
                    hashSet.add(15);
                    cVar = (ih.c) a.a(parcel, a, ih.c.a);
                    break;
                case 16:
                    z = a.c(parcel, a);
                    hashSet.add(16);
                    break;
                case k.ActionBar_itemPadding:
                    str7 = a.n(parcel, a);
                    hashSet.add(18);
                    break;
                case 19:
                    hashSet.add(19);
                    dVar = (ih.d) a.a(parcel, a, ih.d.a);
                    break;
                case 20:
                    str8 = a.n(parcel, a);
                    hashSet.add(20);
                    break;
                case 21:
                    i4 = a.g(parcel, a);
                    hashSet.add(21);
                    break;
                case 22:
                    arrayList = a.c(parcel, a, ih.f.a);
                    hashSet.add(22);
                    break;
                case 23:
                    arrayList2 = a.c(parcel, a, ih.g.a);
                    hashSet.add(23);
                    break;
                case 24:
                    i5 = a.g(parcel, a);
                    hashSet.add(24);
                    break;
                case 25:
                    i6 = a.g(parcel, a);
                    hashSet.add(25);
                    break;
                case 26:
                    str9 = a.n(parcel, a);
                    hashSet.add(26);
                    break;
                case 27:
                    str10 = a.n(parcel, a);
                    hashSet.add(27);
                    break;
                case 28:
                    arrayList3 = a.c(parcel, a, ih.h.a);
                    hashSet.add(28);
                    break;
                case 29:
                    z2 = a.c(parcel, a);
                    hashSet.add(29);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ih(hashSet, i, str, aVar, str2, str3, i2, bVar, str4, str5, i3, str6, cVar, z, str7, dVar, str8, i4, arrayList, arrayList2, i5, i6, str9, str10, arrayList3, z2);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ih[] newArray(int i) {
        return new ih[i];
    }
}
