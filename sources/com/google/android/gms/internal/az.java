package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.internal.fx;
import java.util.ArrayList;

public class az implements Parcelable.Creator {
    static void a(fx fxVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, fxVar.a());
        c.b(parcel, 2, fxVar.b(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public fx createFromParcel(Parcel parcel) {
        int b = a.b(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    arrayList = a.c(parcel, a, fx.a.a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new fx(i, arrayList);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public fx[] newArray(int i) {
        return new fx[i];
    }
}
