package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class js implements SafeParcelable {
    public static final Parcelable.Creator CREATOR = new dm();
    String a;
    String b;
    private final int c;

    js() {
        this.c = 1;
    }

    js(int i, String str, String str2) {
        this.c = i;
        this.a = str;
        this.b = str2;
    }

    public int a() {
        return this.c;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        dm.a(this, parcel, i);
    }
}
