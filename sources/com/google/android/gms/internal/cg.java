package com.google.android.gms.internal;

import com.google.android.gms.drive.metadata.internal.h;
import java.util.Arrays;

public class cg extends h {
    public static final cg a = new cg();

    private cg() {
        super("driveId", Arrays.asList("sqlId", "resourceId"), 4100000);
    }
}
