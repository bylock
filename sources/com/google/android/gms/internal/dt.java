package com.google.android.gms.internal;

import java.io.UnsupportedEncodingException;

public final class dt {
    private final byte[] a;
    private final int b;
    private int c;

    private dt(byte[] bArr, int i, int i2) {
        this.a = bArr;
        this.c = i;
        this.b = i + i2;
    }

    public static dt a(byte[] bArr, int i, int i2) {
        return new dt(bArr, i, i2);
    }

    public static int b(float f) {
        return 4;
    }

    public static int b(int i) {
        if (i >= 0) {
            return f(i);
        }
        return 10;
    }

    public static int b(int i, float f) {
        return d(i) + b(f);
    }

    public static int b(int i, int i2) {
        return d(i) + b(i2);
    }

    public static int b(int i, ea eaVar) {
        return d(i) + b(eaVar);
    }

    public static int b(int i, String str) {
        return d(i) + b(str);
    }

    public static int b(int i, boolean z) {
        return d(i) + b(z);
    }

    public static int b(ea eaVar) {
        int b2 = eaVar.b();
        return b2 + f(b2);
    }

    public static int b(String str) {
        try {
            byte[] bytes = str.getBytes("UTF-8");
            return bytes.length + f(bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported.");
        }
    }

    public static int b(boolean z) {
        return 1;
    }

    public static int c(int i, long j) {
        return d(i) + c(j);
    }

    public static int c(long j) {
        return f(j);
    }

    public static int d(int i) {
        return f(ed.a(i, 0));
    }

    public static int d(int i, long j) {
        return d(i) + d(j);
    }

    public static int d(long j) {
        return f(g(j));
    }

    public static int f(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    public static int f(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (Long.MIN_VALUE & j) == 0 ? 9 : 10;
    }

    public static long g(long j) {
        return (j << 1) ^ (j >> 63);
    }

    public int a() {
        return this.b - this.c;
    }

    public void a(byte b2) {
        if (this.c == this.b) {
            throw new du(this.c, this.b);
        }
        byte[] bArr = this.a;
        int i = this.c;
        this.c = i + 1;
        bArr[i] = b2;
    }

    public void a(float f) {
        g(Float.floatToIntBits(f));
    }

    public void a(int i) {
        if (i >= 0) {
            e(i);
        } else {
            e((long) i);
        }
    }

    public void a(int i, float f) {
        c(i, 5);
        a(f);
    }

    public void a(int i, int i2) {
        c(i, 0);
        a(i2);
    }

    public void a(int i, long j) {
        c(i, 0);
        a(j);
    }

    public void a(int i, ea eaVar) {
        c(i, 2);
        a(eaVar);
    }

    public void a(int i, String str) {
        c(i, 2);
        a(str);
    }

    public void a(int i, boolean z) {
        c(i, 0);
        a(z);
    }

    public void a(long j) {
        e(j);
    }

    public void a(ea eaVar) {
        e(eaVar.e());
        eaVar.a(this);
    }

    public void a(String str) {
        byte[] bytes = str.getBytes("UTF-8");
        e(bytes.length);
        a(bytes);
    }

    public void a(boolean z) {
        c(z ? 1 : 0);
    }

    public void a(byte[] bArr) {
        b(bArr, 0, bArr.length);
    }

    public void b() {
        if (a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    public void b(int i, long j) {
        c(i, 0);
        b(j);
    }

    public void b(long j) {
        e(g(j));
    }

    public void b(byte[] bArr, int i, int i2) {
        if (this.b - this.c >= i2) {
            System.arraycopy(bArr, i, this.a, this.c, i2);
            this.c += i2;
            return;
        }
        throw new du(this.c, this.b);
    }

    public void c(int i) {
        a((byte) i);
    }

    public void c(int i, int i2) {
        e(ed.a(i, i2));
    }

    public void e(int i) {
        while ((i & -128) != 0) {
            c((i & 127) | 128);
            i >>>= 7;
        }
        c(i);
    }

    public void e(long j) {
        while ((-128 & j) != 0) {
            c((((int) j) & 127) | 128);
            j >>>= 7;
        }
        c((int) j);
    }

    public void g(int i) {
        c(i & 255);
        c((i >> 8) & 255);
        c((i >> 16) & 255);
        c((i >> 24) & 255);
    }
}
