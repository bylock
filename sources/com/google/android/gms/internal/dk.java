package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class dk implements Parcelable.Creator {
    static void a(jp jpVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, jpVar.a());
        c.a(parcel, 2, jpVar.a);
        c.a(parcel, 3, jpVar.b, false);
        c.a(parcel, 4, jpVar.c);
        c.a(parcel, 5, jpVar.d, false);
        c.a(parcel, 6, jpVar.e);
        c.a(parcel, 7, jpVar.f);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public jp createFromParcel(Parcel parcel) {
        String str = null;
        int i = 0;
        int b = a.b(parcel);
        double d = 0.0d;
        long j = 0;
        int i2 = -1;
        String str2 = null;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i3 = a.g(parcel, a);
                    break;
                case 2:
                    i = a.g(parcel, a);
                    break;
                case 3:
                    str2 = a.n(parcel, a);
                    break;
                case 4:
                    d = a.l(parcel, a);
                    break;
                case 5:
                    str = a.n(parcel, a);
                    break;
                case 6:
                    j = a.i(parcel, a);
                    break;
                case 7:
                    i2 = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new jp(i3, i, str2, d, str, j, i2);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public jp[] newArray(int i) {
        return new jp[i];
    }
}
