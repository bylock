package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

public class bg implements Parcelable.Creator {
    static void a(gg ggVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, ggVar.a());
        c.a(parcel, 2, ggVar.e(), false);
        c.a(parcel, 3, (Parcelable) ggVar.f(), i, false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public gg createFromParcel(Parcel parcel) {
        gd gdVar = null;
        int b = a.b(parcel);
        int i = 0;
        Parcel parcel2 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    i = a.g(parcel, a);
                    break;
                case 2:
                    parcel2 = a.B(parcel, a);
                    break;
                case 3:
                    gdVar = (gd) a.a(parcel, a, gd.a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new gg(i, parcel2, gdVar);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public gg[] newArray(int i) {
        return new gg[i];
    }
}
