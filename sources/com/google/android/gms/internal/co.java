package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;

public class co implements Parcelable.Creator {
    static void a(ho hoVar, Parcel parcel, int i) {
        int a = c.a(parcel);
        c.a(parcel, 1, hoVar.a(), false);
        c.a(parcel, 1000, hoVar.a);
        c.a(parcel, 2, (Parcelable) hoVar.b(), i, false);
        c.a(parcel, 3, hoVar.c(), false);
        c.b(parcel, 4, hoVar.d(), false);
        c.a(parcel, 5, hoVar.e(), false);
        c.a(parcel, 6, hoVar.f(), false);
        c.a(parcel, a);
    }

    /* renamed from: a */
    public ho createFromParcel(Parcel parcel) {
        String str = null;
        int b = a.b(parcel);
        int i = 0;
        String str2 = null;
        ArrayList arrayList = null;
        String str3 = null;
        LatLng latLng = null;
        String str4 = null;
        while (parcel.dataPosition() < b) {
            int a = a.a(parcel);
            switch (a.a(a)) {
                case 1:
                    str4 = a.n(parcel, a);
                    break;
                case 2:
                    latLng = (LatLng) a.a(parcel, a, LatLng.a);
                    break;
                case 3:
                    str3 = a.n(parcel, a);
                    break;
                case 4:
                    arrayList = a.c(parcel, a, hm.bw);
                    break;
                case 5:
                    str2 = a.n(parcel, a);
                    break;
                case 6:
                    str = a.n(parcel, a);
                    break;
                case 1000:
                    i = a.g(parcel, a);
                    break;
                default:
                    a.b(parcel, a);
                    break;
            }
        }
        if (parcel.dataPosition() == b) {
            return new ho(i, str4, latLng, str3, arrayList, str2, str);
        }
        throw new b("Overread allowed size end=" + b, parcel);
    }

    /* renamed from: a */
    public ho[] newArray(int i) {
        return new ho[i];
    }
}
