package com.google.android.gms.internal;

import android.support.v7.b.k;

public final class y extends dv {
    private static volatile y[] m;
    public int a;
    public String b;
    public y[] c;
    public y[] d;
    public y[] e;
    public String f;
    public String g;
    public long h;
    public boolean i;
    public y[] j;
    public int[] k;
    public boolean l;

    public y() {
        c();
    }

    public static y[] a() {
        if (m == null) {
            synchronized (dy.a) {
                if (m == null) {
                    m = new y[0];
                }
            }
        }
        return m;
    }

    /* renamed from: a */
    public y b(ds dsVar) {
        int i2;
        while (true) {
            int a2 = dsVar.a();
            switch (a2) {
                case 0:
                    break;
                case 8:
                    int e2 = dsVar.e();
                    switch (e2) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                            this.a = e2;
                            continue;
                    }
                case k.ActionBar_itemPadding:
                    this.b = dsVar.g();
                    break;
                case 26:
                    int a3 = ed.a(dsVar, 26);
                    int length = this.c == null ? 0 : this.c.length;
                    y[] yVarArr = new y[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.c, 0, yVarArr, 0, length);
                    }
                    while (length < yVarArr.length - 1) {
                        yVarArr[length] = new y();
                        dsVar.a(yVarArr[length]);
                        dsVar.a();
                        length++;
                    }
                    yVarArr[length] = new y();
                    dsVar.a(yVarArr[length]);
                    this.c = yVarArr;
                    break;
                case 34:
                    int a4 = ed.a(dsVar, 34);
                    int length2 = this.d == null ? 0 : this.d.length;
                    y[] yVarArr2 = new y[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.d, 0, yVarArr2, 0, length2);
                    }
                    while (length2 < yVarArr2.length - 1) {
                        yVarArr2[length2] = new y();
                        dsVar.a(yVarArr2[length2]);
                        dsVar.a();
                        length2++;
                    }
                    yVarArr2[length2] = new y();
                    dsVar.a(yVarArr2[length2]);
                    this.d = yVarArr2;
                    break;
                case 42:
                    int a5 = ed.a(dsVar, 42);
                    int length3 = this.e == null ? 0 : this.e.length;
                    y[] yVarArr3 = new y[(a5 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.e, 0, yVarArr3, 0, length3);
                    }
                    while (length3 < yVarArr3.length - 1) {
                        yVarArr3[length3] = new y();
                        dsVar.a(yVarArr3[length3]);
                        dsVar.a();
                        length3++;
                    }
                    yVarArr3[length3] = new y();
                    dsVar.a(yVarArr3[length3]);
                    this.e = yVarArr3;
                    break;
                case 50:
                    this.f = dsVar.g();
                    break;
                case 58:
                    this.g = dsVar.g();
                    break;
                case 64:
                    this.h = dsVar.d();
                    break;
                case 72:
                    this.l = dsVar.f();
                    break;
                case 80:
                    int a6 = ed.a(dsVar, 80);
                    int[] iArr = new int[a6];
                    int i3 = 0;
                    int i4 = 0;
                    while (i3 < a6) {
                        if (i3 != 0) {
                            dsVar.a();
                        }
                        int e3 = dsVar.e();
                        switch (e3) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case k.ActionBar_progressBarPadding:
                                i2 = i4 + 1;
                                iArr[i4] = e3;
                                break;
                            default:
                                i2 = i4;
                                break;
                        }
                        i3++;
                        i4 = i2;
                    }
                    if (i4 != 0) {
                        int length4 = this.k == null ? 0 : this.k.length;
                        if (length4 != 0 || i4 != iArr.length) {
                            int[] iArr2 = new int[(length4 + i4)];
                            if (length4 != 0) {
                                System.arraycopy(this.k, 0, iArr2, 0, length4);
                            }
                            System.arraycopy(iArr, 0, iArr2, length4, i4);
                            this.k = iArr2;
                            break;
                        } else {
                            this.k = iArr;
                            break;
                        }
                    } else {
                        break;
                    }
                case 82:
                    int c2 = dsVar.c(dsVar.i());
                    int o = dsVar.o();
                    int i5 = 0;
                    while (dsVar.m() > 0) {
                        switch (dsVar.e()) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case k.ActionBar_progressBarPadding:
                                i5++;
                                break;
                        }
                    }
                    if (i5 != 0) {
                        dsVar.e(o);
                        int length5 = this.k == null ? 0 : this.k.length;
                        int[] iArr3 = new int[(i5 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.k, 0, iArr3, 0, length5);
                        }
                        while (dsVar.m() > 0) {
                            int e4 = dsVar.e();
                            switch (e4) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case k.ActionBar_progressBarPadding:
                                    iArr3[length5] = e4;
                                    length5++;
                                    break;
                            }
                        }
                        this.k = iArr3;
                    }
                    dsVar.d(c2);
                    break;
                case 90:
                    int a7 = ed.a(dsVar, 90);
                    int length6 = this.j == null ? 0 : this.j.length;
                    y[] yVarArr4 = new y[(a7 + length6)];
                    if (length6 != 0) {
                        System.arraycopy(this.j, 0, yVarArr4, 0, length6);
                    }
                    while (length6 < yVarArr4.length - 1) {
                        yVarArr4[length6] = new y();
                        dsVar.a(yVarArr4[length6]);
                        dsVar.a();
                        length6++;
                    }
                    yVarArr4[length6] = new y();
                    dsVar.a(yVarArr4[length6]);
                    this.j = yVarArr4;
                    break;
                case 96:
                    this.i = dsVar.f();
                    break;
                default:
                    if (a(dsVar, a2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    @Override // com.google.android.gms.internal.ea, com.google.android.gms.internal.dv
    public void a(dt dtVar) {
        dtVar.a(1, this.a);
        if (!this.b.equals("")) {
            dtVar.a(2, this.b);
        }
        if (this.c != null && this.c.length > 0) {
            for (int i2 = 0; i2 < this.c.length; i2++) {
                y yVar = this.c[i2];
                if (yVar != null) {
                    dtVar.a(3, yVar);
                }
            }
        }
        if (this.d != null && this.d.length > 0) {
            for (int i3 = 0; i3 < this.d.length; i3++) {
                y yVar2 = this.d[i3];
                if (yVar2 != null) {
                    dtVar.a(4, yVar2);
                }
            }
        }
        if (this.e != null && this.e.length > 0) {
            for (int i4 = 0; i4 < this.e.length; i4++) {
                y yVar3 = this.e[i4];
                if (yVar3 != null) {
                    dtVar.a(5, yVar3);
                }
            }
        }
        if (!this.f.equals("")) {
            dtVar.a(6, this.f);
        }
        if (!this.g.equals("")) {
            dtVar.a(7, this.g);
        }
        if (this.h != 0) {
            dtVar.a(8, this.h);
        }
        if (this.l) {
            dtVar.a(9, this.l);
        }
        if (this.k != null && this.k.length > 0) {
            for (int i5 = 0; i5 < this.k.length; i5++) {
                dtVar.a(10, this.k[i5]);
            }
        }
        if (this.j != null && this.j.length > 0) {
            for (int i6 = 0; i6 < this.j.length; i6++) {
                y yVar4 = this.j[i6];
                if (yVar4 != null) {
                    dtVar.a(11, yVar4);
                }
            }
        }
        if (this.i) {
            dtVar.a(12, this.i);
        }
        super.a(dtVar);
    }

    @Override // com.google.android.gms.internal.ea
    public int b() {
        int b2 = super.b() + dt.b(1, this.a);
        if (!this.b.equals("")) {
            b2 += dt.b(2, this.b);
        }
        if (this.c != null && this.c.length > 0) {
            int i2 = b2;
            for (int i3 = 0; i3 < this.c.length; i3++) {
                y yVar = this.c[i3];
                if (yVar != null) {
                    i2 += dt.b(3, yVar);
                }
            }
            b2 = i2;
        }
        if (this.d != null && this.d.length > 0) {
            int i4 = b2;
            for (int i5 = 0; i5 < this.d.length; i5++) {
                y yVar2 = this.d[i5];
                if (yVar2 != null) {
                    i4 += dt.b(4, yVar2);
                }
            }
            b2 = i4;
        }
        if (this.e != null && this.e.length > 0) {
            int i6 = b2;
            for (int i7 = 0; i7 < this.e.length; i7++) {
                y yVar3 = this.e[i7];
                if (yVar3 != null) {
                    i6 += dt.b(5, yVar3);
                }
            }
            b2 = i6;
        }
        if (!this.f.equals("")) {
            b2 += dt.b(6, this.f);
        }
        if (!this.g.equals("")) {
            b2 += dt.b(7, this.g);
        }
        if (this.h != 0) {
            b2 += dt.c(8, this.h);
        }
        if (this.l) {
            b2 += dt.b(9, this.l);
        }
        if (this.k != null && this.k.length > 0) {
            int i8 = 0;
            for (int i9 = 0; i9 < this.k.length; i9++) {
                i8 += dt.b(this.k[i9]);
            }
            b2 = b2 + i8 + (this.k.length * 1);
        }
        if (this.j != null && this.j.length > 0) {
            for (int i10 = 0; i10 < this.j.length; i10++) {
                y yVar4 = this.j[i10];
                if (yVar4 != null) {
                    b2 += dt.b(11, yVar4);
                }
            }
        }
        if (this.i) {
            b2 += dt.b(12, this.i);
        }
        this.s = b2;
        return b2;
    }

    public y c() {
        this.a = 1;
        this.b = "";
        this.c = a();
        this.d = a();
        this.e = a();
        this.f = "";
        this.g = "";
        this.h = 0;
        this.i = false;
        this.j = a();
        this.k = ed.a;
        this.l = false;
        this.r = null;
        this.s = -1;
        return this;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof y)) {
            return false;
        }
        y yVar = (y) obj;
        if (this.a != yVar.a) {
            return false;
        }
        if (this.b == null) {
            if (yVar.b != null) {
                return false;
            }
        } else if (!this.b.equals(yVar.b)) {
            return false;
        }
        if (!dy.a(this.c, yVar.c) || !dy.a(this.d, yVar.d) || !dy.a(this.e, yVar.e)) {
            return false;
        }
        if (this.f == null) {
            if (yVar.f != null) {
                return false;
            }
        } else if (!this.f.equals(yVar.f)) {
            return false;
        }
        if (this.g == null) {
            if (yVar.g != null) {
                return false;
            }
        } else if (!this.g.equals(yVar.g)) {
            return false;
        }
        if (this.h == yVar.h && this.i == yVar.i && dy.a(this.j, yVar.j) && dy.a(this.k, yVar.k) && this.l == yVar.l) {
            return (this.r == null || this.r.isEmpty()) ? yVar.r == null || yVar.r.isEmpty() : this.r.equals(yVar.r);
        }
        return false;
    }

    public int hashCode() {
        int i2 = 1231;
        int i3 = 0;
        int hashCode = ((((((this.i ? 1231 : 1237) + (((((this.g == null ? 0 : this.g.hashCode()) + (((this.f == null ? 0 : this.f.hashCode()) + (((((((((this.b == null ? 0 : this.b.hashCode()) + ((this.a + 527) * 31)) * 31) + dy.a(this.c)) * 31) + dy.a(this.d)) * 31) + dy.a(this.e)) * 31)) * 31)) * 31) + ((int) (this.h ^ (this.h >>> 32)))) * 31)) * 31) + dy.a(this.j)) * 31) + dy.a(this.k)) * 31;
        if (!this.l) {
            i2 = 1237;
        }
        int i4 = (hashCode + i2) * 31;
        if (this.r != null && !this.r.isEmpty()) {
            i3 = this.r.hashCode();
        }
        return i4 + i3;
    }
}
