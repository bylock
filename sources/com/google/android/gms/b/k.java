package com.google.android.gms.b;

import java.util.HashMap;
import java.util.Map;

class k {
    private final Map a = new HashMap();
    private final Map b = new HashMap();
    private final boolean c;
    private final String d;

    k(String str, boolean z) {
        this.c = z;
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        if (!this.c) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.d);
        for (String str : this.a.keySet()) {
            sb.append("&").append(str).append("=").append(this.a.get(str));
        }
        for (String str2 : this.b.keySet()) {
            sb.append("&").append(str2).append("=").append((String) this.b.get(str2));
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i) {
        if (this.c) {
            Integer num = (Integer) this.a.get(str);
            if (num == null) {
                num = 0;
            }
            this.a.put(str, Integer.valueOf(num.intValue() + i));
        }
    }
}
