package com.google.android.gms.b;

/* access modifiers changed from: package-private */
public enum av {
    CONNECTING,
    CONNECTED_SERVICE,
    CONNECTED_LOCAL,
    BLOCKED,
    PENDING_CONNECTION,
    PENDING_DISCONNECT,
    DISCONNECTED
}
