package com.google.android.gms.b;

import android.util.Log;

class aj implements d {
    private int a = 1;

    aj() {
    }

    private String e(String str) {
        return Thread.currentThread().toString() + ": " + str;
    }

    @Override // com.google.android.gms.b.d
    public int a() {
        return this.a;
    }

    @Override // com.google.android.gms.b.d
    public void a(int i) {
        this.a = i;
    }

    @Override // com.google.android.gms.b.d
    public void a(String str) {
        if (this.a <= 0) {
            Log.v("GAV4", e(str));
        }
    }

    @Override // com.google.android.gms.b.d
    public void b(String str) {
        if (this.a <= 1) {
            Log.i("GAV4", e(str));
        }
    }

    @Override // com.google.android.gms.b.d
    public void c(String str) {
        if (this.a <= 2) {
            Log.w("GAV4", e(str));
        }
    }

    @Override // com.google.android.gms.b.d
    public void d(String str) {
        if (this.a <= 3) {
            Log.e("GAV4", e(str));
        }
    }
}
