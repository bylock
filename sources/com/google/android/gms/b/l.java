package com.google.android.gms.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.internal.ef;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.impl.client.DefaultHttpClient;

/* access modifiers changed from: package-private */
public class l implements z {
    private static final String a = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", "hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id");
    private final n b;
    private volatile al c;
    private final aa d;
    private final Context e;
    private final String f;
    private k g;
    private long h;
    private final int i;
    private af j;

    l(aa aaVar, Context context) {
        this(aaVar, context, "google_analytics_v4.db", 2000);
    }

    l(aa aaVar, Context context, String str, int i2) {
        this.e = context.getApplicationContext();
        this.f = str;
        this.d = aaVar;
        this.j = new m(this);
        this.b = new n(this, this.e, this.f);
        this.c = new s(new DefaultHttpClient(), this.e);
        this.h = 0;
        this.i = i2;
    }

    private SQLiteDatabase a(String str) {
        try {
            return this.b.getWritableDatabase();
        } catch (SQLiteException e2) {
            j.d(str);
            return null;
        }
    }

    static String a(Map map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry entry : map.entrySet()) {
            arrayList.add(bk.a((String) entry.getKey()) + "=" + bk.a((String) entry.getValue()));
        }
        return TextUtils.join("&", arrayList);
    }

    private void a(Map map, long j2, String str) {
        long j3;
        SQLiteDatabase a2 = a("Error opening database for putHit");
        if (a2 != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_string", a(map));
            contentValues.put("hit_time", Long.valueOf(j2));
            if (map.containsKey("AppUID")) {
                try {
                    j3 = Long.parseLong((String) map.get("AppUID"));
                } catch (NumberFormatException e2) {
                    j3 = 0;
                }
            } else {
                j3 = 0;
            }
            contentValues.put("hit_app_id", Long.valueOf(j3));
            if (str == null) {
                str = "http://www.google-analytics.com/collect";
            }
            if (str.length() == 0) {
                j.d("Empty path: not sending hit");
                return;
            }
            contentValues.put("hit_url", str);
            try {
                a2.insert("hits2", null, contentValues);
                this.d.a(false);
            } catch (SQLiteException e3) {
                j.d("Error storing hit");
            }
        }
    }

    private void a(Map map, Collection collection) {
        String substring = "&_v".substring(1);
        if (collection != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                ef efVar = (ef) it.next();
                if ("appendVersion".equals(efVar.a())) {
                    map.put(substring, efVar.b());
                    return;
                }
            }
        }
    }

    private void f() {
        int b2 = (b() - this.i) + 1;
        if (b2 > 0) {
            List a2 = a(b2);
            j.c("Store full, deleting " + a2.size() + " hits to make room.");
            a((String[]) a2.toArray(new String[0]));
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        boolean z = true;
        long a2 = this.j.a();
        if (a2 <= this.h + 86400000) {
            return 0;
        }
        this.h = a2;
        SQLiteDatabase a3 = a("Error opening database for deleteStaleHits.");
        if (a3 == null) {
            return 0;
        }
        int delete = a3.delete("hits2", "HIT_TIME < ?", new String[]{Long.toString(this.j.a() - 2592000000L)});
        aa aaVar = this.d;
        if (b() != 0) {
            z = false;
        }
        aaVar.a(z);
        return delete;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List a(int r14) {
        /*
        // Method dump skipped, instructions count: 138
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.b.l.a(int):java.util.List");
    }

    @Override // com.google.android.gms.b.z
    public void a(long j2) {
        boolean z = true;
        SQLiteDatabase a2 = a("Error opening database for clearHits");
        if (a2 != null) {
            if (j2 == 0) {
                a2.delete("hits2", null, null);
            } else {
                a2.delete("hits2", "hit_app_id = ?", new String[]{Long.valueOf(j2).toString()});
            }
            aa aaVar = this.d;
            if (b() != 0) {
                z = false;
            }
            aaVar.a(z);
        }
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    public void a(Collection collection) {
        if (collection == null || collection.isEmpty()) {
            j.d("Empty/Null collection passed to deleteHits.");
            return;
        }
        String[] strArr = new String[collection.size()];
        Iterator it = collection.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            strArr[i2] = String.valueOf(((bj) it.next()).b());
            i2++;
        }
        a(strArr);
    }

    @Override // com.google.android.gms.b.z
    public void a(Map map, long j2, String str, Collection collection) {
        a();
        f();
        a(map, collection);
        a(map, j2, str);
    }

    /* access modifiers changed from: package-private */
    public void a(String[] strArr) {
        boolean z = true;
        if (strArr == null || strArr.length == 0) {
            j.d("Empty hitIds passed to deleteHits.");
            return;
        }
        SQLiteDatabase a2 = a("Error opening database for deleteHits.");
        if (a2 != null) {
            try {
                a2.delete("hits2", String.format("HIT_ID in (%s)", TextUtils.join(",", Collections.nCopies(strArr.length, "?"))), strArr);
                aa aaVar = this.d;
                if (b() != 0) {
                    z = false;
                }
                aaVar.a(z);
            } catch (SQLiteException e2) {
                j.d("Error deleting hits " + strArr);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int b() {
        Cursor cursor = null;
        int i2 = 0;
        SQLiteDatabase a2 = a("Error opening database for getNumStoredHits.");
        if (a2 != null) {
            try {
                Cursor rawQuery = a2.rawQuery("SELECT COUNT(*) from hits2", null);
                if (rawQuery.moveToFirst()) {
                    i2 = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e2) {
                j.d("Error getting numStoredHits");
                if (0 != 0) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (0 != 0) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00f2, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00fa, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0173, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0174, code lost:
        r11 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0179, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x017a, code lost:
        r2 = r1;
        r3 = r12;
        r1 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        return r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0148 A[Catch:{ all -> 0x0167 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x016a  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0173 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:79:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List b(int r16) {
        /*
        // Method dump skipped, instructions count: 392
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.b.l.b(int):java.util.List");
    }

    @Override // com.google.android.gms.b.z
    public void c() {
        boolean z = true;
        j.c("Dispatch running...");
        if (this.c.a()) {
            List b2 = b(40);
            if (b2.isEmpty()) {
                j.c("...nothing to dispatch");
                this.d.a(true);
                return;
            }
            if (this.g == null) {
                this.g = new k("_t=dispatch&_v=ma4.0.1", true);
            }
            if (b() > b2.size()) {
                z = false;
            }
            int a2 = this.c.a(b2, this.g, z);
            j.c("sent " + a2 + " of " + b2.size() + " hits");
            a(b2.subList(0, Math.min(a2, b2.size())));
            if (a2 != b2.size() || b() <= 0) {
                this.g = null;
            } else {
                a.a(this.e).e();
            }
        }
    }

    @Override // com.google.android.gms.b.z
    public al d() {
        return this.c;
    }
}
