package com.google.android.gms.b;

import android.content.Context;
import android.util.DisplayMetrics;

/* access modifiers changed from: package-private */
public class p implements ak {
    private static p a;
    private static Object b = new Object();
    private final Context c;

    protected p(Context context) {
        this.c = context;
    }

    public static p a() {
        p pVar;
        synchronized (b) {
            pVar = a;
        }
        return pVar;
    }

    public static void a(Context context) {
        synchronized (b) {
            if (a == null) {
                a = new p(context);
            }
        }
    }

    @Override // com.google.android.gms.b.ak
    public String a(String str) {
        if (str != null && str.equals("&sr")) {
            return b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String b() {
        DisplayMetrics displayMetrics = this.c.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels + "x" + displayMetrics.heightPixels;
    }
}
