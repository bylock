package com.google.android.gms.b;

import java.util.List;
import java.util.Map;

class ay {
    private final Map a;
    private final long b;
    private final String c;
    private final List d;

    public ay(Map map, long j, String str, List list) {
        this.a = map;
        this.b = j;
        this.c = str;
        this.d = list;
    }

    public Map a() {
        return this.a;
    }

    public long b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public List d() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PATH: ");
        sb.append(this.c);
        if (this.a != null) {
            sb.append("  PARAMS: ");
            for (Map.Entry entry : this.a.entrySet()) {
                sb.append((String) entry.getKey());
                sb.append("=");
                sb.append((String) entry.getValue());
                sb.append(",  ");
            }
        }
        return sb.toString();
    }
}
