package com.google.android.gms.b;

import android.os.Handler;
import android.os.Message;

/* access modifiers changed from: package-private */
public class aq implements Handler.Callback {
    final /* synthetic */ ao a;

    aq(ao aoVar) {
        this.a = aoVar;
    }

    public boolean handleMessage(Message message) {
        if (1 == message.what && ao.a.equals(message.obj)) {
            be.a().a(true);
            this.a.a();
            be.a().a(false);
            if (this.a.e > 0 && !(this.a.n)) {
                this.a.l.sendMessageDelayed(this.a.l.obtainMessage(1, ao.a), (long) (this.a.e * 1000));
            }
        }
        return true;
    }
}
