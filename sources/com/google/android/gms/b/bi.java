package com.google.android.gms.b;

class bi implements ag {
    String a;
    String b;
    String c;
    int d = -1;
    int e = -1;

    bi() {
    }

    public boolean a() {
        return this.a != null;
    }

    public String b() {
        return this.a;
    }

    public boolean c() {
        return this.b != null;
    }

    public String d() {
        return this.b;
    }

    public boolean e() {
        return this.c != null;
    }

    public String f() {
        return this.c;
    }

    public boolean g() {
        return this.d >= 0;
    }

    public int h() {
        return this.d;
    }

    public boolean i() {
        return this.e != -1;
    }

    public boolean j() {
        return this.e == 1;
    }
}
