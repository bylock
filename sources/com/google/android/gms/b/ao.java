package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;

class ao extends q {
    private static final Object a = new Object();
    private static ao o;
    private Context b;
    private z c;
    private volatile ab d;
    private int e = 1800;
    private boolean f = true;
    private boolean g;
    private String h;
    private boolean i = true;
    private boolean j = true;
    private aa k = new ap(this);
    private Handler l;
    private an m;
    private boolean n = false;

    private ao() {
    }

    public static ao c() {
        if (o == null) {
            o = new ao();
        }
        return o;
    }

    private void g() {
        this.m = new an(this);
        this.m.a(this.b);
    }

    private void h() {
        this.l = new Handler(this.b.getMainLooper(), new aq(this));
        if (this.e > 0) {
            this.l.sendMessageDelayed(this.l.obtainMessage(1, a), (long) (this.e * 1000));
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.b.q
    public synchronized void a() {
        if (this.d == null) {
            j.c("Dispatch call queued. Dispatch will run once initialization is complete.");
            this.f = true;
        } else {
            be.a().a(bf.DISPATCH);
            this.d.a();
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.b.q
    public synchronized void a(int i2) {
        if (this.l == null) {
            j.c("Dispatch period set with null handler. Dispatch will run once initialization is complete.");
            this.e = i2;
        } else {
            be.a().a(bf.SET_DISPATCH_PERIOD);
            if (!this.n && this.i && this.e > 0) {
                this.l.removeMessages(1, a);
            }
            this.e = i2;
            if (i2 > 0 && !this.n && this.i) {
                this.l.sendMessageDelayed(this.l.obtainMessage(1, a), (long) (i2 * 1000));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(Context context, ab abVar) {
        if (this.b == null) {
            this.b = context.getApplicationContext();
            if (this.d == null) {
                this.d = abVar;
                if (this.f) {
                    a();
                    this.f = false;
                }
                if (this.g) {
                    e();
                    this.g = false;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.b.q
    public synchronized void a(boolean z) {
        a(this.n, z);
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(boolean z, boolean z2) {
        if (!(this.n == z && this.i == z2)) {
            if ((z || !z2) && this.e > 0) {
                this.l.removeMessages(1, a);
            }
            if (!z && z2 && this.e > 0) {
                this.l.sendMessageDelayed(this.l.obtainMessage(1, a), (long) (this.e * 1000));
            }
            j.c("PowerSaveMode " + ((z || !z2) ? "initiated." : "terminated."));
            this.n = z;
            this.i = z2;
        }
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.b.q
    public synchronized void b() {
        if (!this.n && this.i && this.e > 0) {
            this.l.removeMessages(1, a);
            this.l.sendMessage(this.l.obtainMessage(1, a));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized z d() {
        if (this.c == null) {
            if (this.b == null) {
                throw new IllegalStateException("Cant get a store unless we have a context");
            }
            this.c = new l(this.k, this.b);
            if (this.h != null) {
                this.c.d().a(this.h);
                this.h = null;
            }
        }
        if (this.l == null) {
            h();
        }
        if (this.m == null && this.j) {
            g();
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.d == null) {
            j.c("setForceLocalDispatch() queued. It will be called once initialization is complete.");
            this.g = true;
            return;
        }
        be.a().a(bf.SET_FORCE_LOCAL_DISPATCH);
        this.d.b();
    }
}
