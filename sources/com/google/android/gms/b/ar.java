package com.google.android.gms.b;

import android.content.Context;
import android.content.Intent;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;
import java.util.concurrent.ConcurrentLinkedQueue;

/* access modifiers changed from: package-private */
public class ar implements r, x, y {
    private volatile long a;
    private volatile av b;
    private volatile u c;
    private z d;
    private z e;
    private final a f;
    private final ab g;
    private final Context h;
    private final Queue i;
    private volatile int j;
    private volatile Timer k;
    private volatile Timer l;
    private volatile Timer m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private af r;
    private long s;

    ar(Context context, ab abVar) {
        this(context, abVar, null, a.a(context));
    }

    ar(Context context, ab abVar, z zVar, a aVar) {
        this.i = new ConcurrentLinkedQueue();
        this.s = 300000;
        this.e = zVar;
        this.h = context;
        this.g = abVar;
        this.f = aVar;
        this.r = new as(this);
        this.j = 0;
        this.b = av.DISCONNECTED;
    }

    private Timer a(Timer timer) {
        if (timer == null) {
            return null;
        }
        timer.cancel();
        return null;
    }

    private void g() {
        this.k = a(this.k);
        this.l = a(this.l);
        this.m = a(this.m);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public synchronized void h() {
        if (Thread.currentThread().equals(this.g.d())) {
            if (this.o) {
                f();
            }
            switch (this.b) {
                case CONNECTED_LOCAL:
                    while (!this.i.isEmpty()) {
                        ay ayVar = (ay) this.i.poll();
                        j.c("Sending hit to store  " + ayVar);
                        this.d.a(ayVar.a(), ayVar.b(), ayVar.c(), ayVar.d());
                    }
                    if (this.n) {
                        i();
                        break;
                    }
                    break;
                case CONNECTED_SERVICE:
                    while (!this.i.isEmpty()) {
                        ay ayVar2 = (ay) this.i.peek();
                        j.c("Sending hit to service   " + ayVar2);
                        if (!this.f.b()) {
                            this.c.a(ayVar2.a(), ayVar2.b(), ayVar2.c(), ayVar2.d());
                        } else {
                            j.c("Dry run enabled. Hit not actually sent to service.");
                        }
                        this.i.poll();
                    }
                    this.a = this.r.a();
                    break;
                case DISCONNECTED:
                    j.c("Need to reconnect");
                    if (!this.i.isEmpty()) {
                        k();
                        break;
                    }
                    break;
            }
        } else {
            this.g.c().add(new at(this));
        }
    }

    private void i() {
        this.d.c();
        this.n = false;
    }

    /* access modifiers changed from: private */
    public synchronized void j() {
        if (this.b != av.CONNECTED_LOCAL) {
            g();
            j.c("falling back to local store");
            if (this.e != null) {
                this.d = this.e;
            } else {
                ao c2 = ao.c();
                c2.a(this.h, this.g);
                this.d = c2.d();
            }
            this.b = av.CONNECTED_LOCAL;
            h();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void k() {
        if (this.q || this.c == null || this.b == av.CONNECTED_LOCAL) {
            j.d("client not initialized.");
            j();
        } else {
            try {
                this.j++;
                a(this.l);
                this.b = av.CONNECTING;
                this.l = new Timer("Failed Connect");
                this.l.schedule(new ax(this, null), 3000);
                j.c("connecting to Analytics service");
                this.c.b();
            } catch (SecurityException e2) {
                j.d("security exception on connectToService");
                j();
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void l() {
        if (this.c != null && this.b == av.CONNECTED_SERVICE) {
            this.b = av.PENDING_DISCONNECT;
            this.c.c();
        }
    }

    private void m() {
        this.k = a(this.k);
        this.k = new Timer("Service Reconnect");
        this.k.schedule(new az(this, null), 5000);
    }

    @Override // com.google.android.gms.b.r
    public synchronized void a() {
        if (!this.q) {
            j.c("setForceLocalDispatch called.");
            this.q = true;
            switch (this.b) {
                case CONNECTED_SERVICE:
                    l();
                    break;
                case CONNECTING:
                    this.p = true;
                    break;
            }
        }
    }

    @Override // com.google.android.gms.b.y
    public synchronized void a(int i2, Intent intent) {
        this.b = av.PENDING_CONNECTION;
        if (this.j < 2) {
            j.d("Service unavailable (code=" + i2 + "), will retry.");
            m();
        } else {
            j.d("Service unavailable (code=" + i2 + "), using local store.");
            j();
        }
    }

    @Override // com.google.android.gms.b.r
    public void a(Map map, long j2, String str, List list) {
        j.c("putHit called");
        this.i.add(new ay(map, j2, str, list));
        h();
    }

    @Override // com.google.android.gms.b.r
    public void b() {
        switch (this.b) {
            case CONNECTED_LOCAL:
                i();
                return;
            case CONNECTED_SERVICE:
                return;
            default:
                this.n = true;
                return;
        }
    }

    @Override // com.google.android.gms.b.r
    public void c() {
        if (this.c == null) {
            this.c = new v(this.h, this, this);
            k();
        }
    }

    @Override // com.google.android.gms.b.x
    public synchronized void d() {
        this.l = a(this.l);
        this.j = 0;
        j.c("Connected to service");
        this.b = av.CONNECTED_SERVICE;
        if (this.p) {
            l();
            this.p = false;
        } else {
            h();
            this.m = a(this.m);
            this.m = new Timer("disconnect check");
            this.m.schedule(new aw(this, null), this.s);
        }
    }

    @Override // com.google.android.gms.b.x
    public synchronized void e() {
        if (this.b == av.PENDING_DISCONNECT) {
            j.c("Disconnected from service");
            g();
            this.b = av.DISCONNECTED;
        } else {
            j.c("Unexpected disconnect.");
            this.b = av.PENDING_CONNECTION;
            if (this.j < 2) {
                m();
            } else {
                j();
            }
        }
    }

    public void f() {
        j.c("clearHits called");
        this.i.clear();
        switch (this.b) {
            case CONNECTED_LOCAL:
                this.d.a(0);
                this.o = false;
                return;
            case CONNECTED_SERVICE:
                this.c.a();
                this.o = false;
                return;
            default:
                this.o = true;
                return;
        }
    }
}
