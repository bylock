package com.google.android.gms.b;

/* access modifiers changed from: package-private */
public class ae extends Thread {
    final /* synthetic */ ad a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ae(ad adVar, String str) {
        super(str);
        this.a = adVar;
    }

    public void run() {
        synchronized (this.a.f) {
            this.a.d = this.a.c();
            this.a.e = true;
            this.a.f.notifyAll();
        }
    }
}
