package com.google.android.gms.b;

import android.text.TextUtils;
import com.google.android.gms.internal.aw;
import java.util.HashMap;
import java.util.Map;

public class e {
    private final h a;
    private final Map b;
    private o c;
    private final ad d;
    private final p e;
    private final ac f;
    private f g;

    e(String str, h hVar) {
        this(str, hVar, ad.a(), p.a(), ac.a(), new bl("tracking"));
    }

    e(String str, h hVar, ad adVar, p pVar, ac acVar, o oVar) {
        this.b = new HashMap();
        this.a = hVar;
        if (str != null) {
            this.b.put("&tid", str);
        }
        this.b.put("useSecure", "1");
        this.d = adVar;
        this.e = pVar;
        this.f = acVar;
        this.c = oVar;
        this.g = new f(this);
    }

    public void a(String str, String str2) {
        aw.a(str, "Key should be non-null");
        be.a().a(bf.SET);
        this.b.put(str, str2);
    }

    public void a(Map map) {
        be.a().a(bf.SEND);
        HashMap hashMap = new HashMap();
        hashMap.putAll(this.b);
        if (map != null) {
            hashMap.putAll(map);
        }
        if (TextUtils.isEmpty((CharSequence) hashMap.get("&tid"))) {
            j.d(String.format("Missing tracking id (%s) parameter.", "&tid"));
        }
        String str = (String) hashMap.get("&t");
        if (TextUtils.isEmpty(str)) {
            j.d(String.format("Missing hit type (%s) parameter.", "&t"));
            str = "";
        }
        if (this.g.a()) {
            hashMap.put("&sc", "start");
        }
        if (str.equals("transaction") || str.equals("item") || this.c.a()) {
            this.a.a(hashMap);
        } else {
            j.d("Too many hits sent too quickly, rate limiting invoked.");
        }
    }
}
