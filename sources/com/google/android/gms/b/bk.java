package com.google.android.gms.b;

import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/* access modifiers changed from: package-private */
public class bk {
    static String a(bj bjVar, long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(bjVar.a());
        if (bjVar.c() > 0) {
            long c = j - bjVar.c();
            if (c >= 0) {
                sb.append("&qt").append("=").append(c);
            }
        }
        sb.append("&z").append("=").append(bjVar.b());
        return sb.toString();
    }

    static String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("URL encoding failed for: " + str);
        }
    }

    static Map a(Map map) {
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            if (((String) entry.getKey()).startsWith("&") && entry.getValue() != null) {
                String substring = ((String) entry.getKey()).substring(1);
                if (!TextUtils.isEmpty(substring)) {
                    hashMap.put(substring, entry.getValue());
                }
            }
        }
        return hashMap;
    }
}
