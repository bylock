package com.google.android.gms.b;

import java.util.SortedSet;
import java.util.TreeSet;

/* access modifiers changed from: package-private */
public class be {
    private static final be d = new be();
    private SortedSet a = new TreeSet();
    private StringBuilder b = new StringBuilder();
    private boolean c = false;

    private be() {
    }

    public static be a() {
        return d;
    }

    public synchronized void a(bf bfVar) {
        if (!this.c) {
            this.a.add(bfVar);
            this.b.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(bfVar.ordinal()));
        }
    }

    public synchronized void a(boolean z) {
        this.c = z;
    }

    public synchronized String b() {
        StringBuilder sb;
        sb = new StringBuilder();
        int i = 6;
        int i2 = 0;
        while (this.a.size() > 0) {
            bf bfVar = (bf) this.a.first();
            this.a.remove(bfVar);
            int ordinal = bfVar.ordinal();
            while (ordinal >= i) {
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(i2));
                i += 6;
                i2 = 0;
            }
            i2 += 1 << (bfVar.ordinal() % 6);
        }
        if (i2 > 0 || sb.length() == 0) {
            sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(i2));
        }
        this.a.clear();
        return sb.toString();
    }

    public synchronized String c() {
        String sb;
        if (this.b.length() > 0) {
            this.b.insert(0, ".");
        }
        sb = this.b.toString();
        this.b = new StringBuilder();
        return sb;
    }
}
