package com.google.android.gms.b;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.internal.ag;

final class w implements ServiceConnection {
    final /* synthetic */ v a;

    w(v vVar) {
        this.a = vVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        j.c("service connected, binder: " + iBinder);
        try {
            if ("com.google.android.gms.analytics.internal.IAnalyticsService".equals(iBinder.getInterfaceDescriptor())) {
                j.c("bound to service");
                this.a.e = ag.a(iBinder);
                this.a.g();
                return;
            }
        } catch (RemoteException e) {
        }
        this.a.d.unbindService(this);
        this.a.a = null;
        this.a.c.a(2, null);
    }

    public void onServiceDisconnected(ComponentName componentName) {
        j.c("service disconnected: " + componentName);
        this.a.a = null;
        this.a.b.e();
    }
}
