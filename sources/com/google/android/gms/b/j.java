package com.google.android.gms.b;

public class j {
    private static a a;

    public static void a(String str) {
        d b = b();
        if (b != null) {
            b.d(str);
        }
    }

    public static boolean a() {
        return b() != null && b().a() == 0;
    }

    private static d b() {
        if (a == null) {
            a = a.a();
        }
        if (a != null) {
            return a.d();
        }
        return null;
    }

    public static void b(String str) {
        d b = b();
        if (b != null) {
            b.b(str);
        }
    }

    public static void c(String str) {
        d b = b();
        if (b != null) {
            b.a(str);
        }
    }

    public static void d(String str) {
        d b = b();
        if (b != null) {
            b.c(str);
        }
    }
}
