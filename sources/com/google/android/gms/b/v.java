package com.google.android.gms.b;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.RemoteException;
import com.google.android.gms.internal.af;
import java.util.List;
import java.util.Map;

class v implements u {
    private ServiceConnection a;
    private x b;
    private y c;
    private Context d;
    private af e;

    public v(Context context, x xVar, y yVar) {
        this.d = context;
        if (xVar == null) {
            throw new IllegalArgumentException("onConnectedListener cannot be null");
        }
        this.b = xVar;
        if (yVar == null) {
            throw new IllegalArgumentException("onConnectionFailedListener cannot be null");
        }
        this.c = yVar;
    }

    private af f() {
        d();
        return this.e;
    }

    /* access modifiers changed from: private */
    public void g() {
        h();
    }

    private void h() {
        this.b.d();
    }

    @Override // com.google.android.gms.b.u
    public void a() {
        try {
            f().a();
        } catch (RemoteException e2) {
            j.a("clear hits failed: " + e2);
        }
    }

    @Override // com.google.android.gms.b.u
    public void a(Map map, long j, String str, List list) {
        try {
            f().a(map, j, str, list);
        } catch (RemoteException e2) {
            j.a("sendHit failed: " + e2);
        }
    }

    @Override // com.google.android.gms.b.u
    public void b() {
        Intent intent = new Intent("com.google.android.gms.analytics.service.START");
        intent.setComponent(new ComponentName("com.google.android.gms", "com.google.android.gms.analytics.service.AnalyticsService"));
        intent.putExtra("app_package_name", this.d.getPackageName());
        if (this.a != null) {
            j.a("Calling connect() while still connected, missing disconnect().");
            return;
        }
        this.a = new w(this);
        boolean bindService = this.d.bindService(intent, this.a, 129);
        j.c("connect: bindService returned " + bindService + " for " + intent);
        if (!bindService) {
            this.a = null;
            this.c.a(1, null);
        }
    }

    @Override // com.google.android.gms.b.u
    public void c() {
        this.e = null;
        if (this.a != null) {
            try {
                this.d.unbindService(this.a);
            } catch (IllegalArgumentException | IllegalStateException e2) {
            }
            this.a = null;
            this.b.e();
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (!e()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public boolean e() {
        return this.e != null;
    }
}
