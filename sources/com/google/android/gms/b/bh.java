package com.google.android.gms.b;

/* access modifiers changed from: package-private */
public class bh implements ai {
    private final bi a = new bi();

    @Override // com.google.android.gms.b.ai
    public void a(String str, int i) {
        if ("ga_dispatchPeriod".equals(str)) {
            this.a.d = i;
        } else {
            j.d("int configuration name not recognized:  " + str);
        }
    }

    @Override // com.google.android.gms.b.ai
    public void a(String str, String str2) {
    }

    @Override // com.google.android.gms.b.ai
    public void a(String str, boolean z) {
        if ("ga_dryRun".equals(str)) {
            this.a.e = z ? 1 : 0;
            return;
        }
        j.d("bool configuration name not recognized:  " + str);
    }

    /* renamed from: b */
    public bi a() {
        return this.a;
    }

    @Override // com.google.android.gms.b.ai
    public void b(String str, String str2) {
        if ("ga_appName".equals(str)) {
            this.a.a = str2;
        } else if ("ga_appVersion".equals(str)) {
            this.a.b = str2;
        } else if ("ga_logLevel".equals(str)) {
            this.a.c = str2;
        } else {
            j.d("string configuration name not recognized:  " + str);
        }
    }
}
