package com.google.android.gms.b;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public class c {
    private Map a = new HashMap();

    protected c() {
    }

    public c a(String str) {
        be.a().a(bf.MAP_BUILDER_SET_CAMPAIGN_PARAMS);
        String b = t.b(str);
        if (!TextUtils.isEmpty(b)) {
            Map a2 = t.a(b);
            a("&cc", (String) a2.get("utm_content"));
            a("&cm", (String) a2.get("utm_medium"));
            a("&cn", (String) a2.get("utm_campaign"));
            a("&cs", (String) a2.get("utm_source"));
            a("&ck", (String) a2.get("utm_term"));
            a("&ci", (String) a2.get("utm_id"));
            a("&gclid", (String) a2.get("gclid"));
            a("&dclid", (String) a2.get("dclid"));
            a("&gmob_t", (String) a2.get("gmob_t"));
        }
        return this;
    }

    public final c a(String str, String str2) {
        be.a().a(bf.MAP_BUILDER_SET);
        if (str != null) {
            this.a.put(str, str2);
        } else {
            j.d(" HitBuilder.set() called with a null paramName.");
        }
        return this;
    }

    public Map a() {
        return this.a;
    }
}
