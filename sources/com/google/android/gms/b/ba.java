package com.google.android.gms.b;

import android.content.Context;
import android.os.Process;
import android.text.TextUtils;
import com.google.android.gms.internal.ef;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

class ba extends Thread implements ab {
    private static ba g;
    private final LinkedBlockingQueue a = new LinkedBlockingQueue();
    private volatile boolean b = false;
    private volatile boolean c = false;
    private volatile List d;
    private volatile String e;
    private volatile String f;
    private volatile r h;
    private final Context i;

    private ba(Context context) {
        super("GAThread");
        if (context != null) {
            this.i = context.getApplicationContext();
        } else {
            this.i = context;
        }
        start();
    }

    static int a(String str) {
        int i2 = 1;
        if (!TextUtils.isEmpty(str)) {
            i2 = 0;
            for (int length = str.length() - 1; length >= 0; length--) {
                char charAt = str.charAt(length);
                i2 = ((i2 << 6) & 268435455) + charAt + (charAt << 14);
                int i3 = 266338304 & i2;
                if (i3 != 0) {
                    i2 ^= i3 >> 21;
                }
            }
        }
        return i2;
    }

    static ba a(Context context) {
        if (g == null) {
            g = new ba(context);
        }
        return g;
    }

    private String a(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    static String b(Context context) {
        try {
            FileInputStream openFileInput = context.openFileInput("gaInstallData");
            byte[] bArr = new byte[8192];
            int read = openFileInput.read(bArr, 0, 8192);
            if (openFileInput.available() > 0) {
                j.a("Too much campaign data, ignoring it.");
                openFileInput.close();
                context.deleteFile("gaInstallData");
                return null;
            }
            openFileInput.close();
            context.deleteFile("gaInstallData");
            if (read <= 0) {
                j.d("Campaign file is empty.");
                return null;
            }
            String str = new String(bArr, 0, read);
            j.b("Campaign found: " + str);
            return str;
        } catch (FileNotFoundException e2) {
            j.b("No campaign data found.");
            return null;
        } catch (IOException e3) {
            j.a("Error reading campaign data.");
            context.deleteFile("gaInstallData");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public String b(Map map) {
        return (!map.containsKey("useSecure") || t.a((String) map.get("useSecure"), true)) ? "https:" : "http:";
    }

    /* access modifiers changed from: private */
    public boolean c(Map map) {
        if (map.get("&sf") == null) {
            return false;
        }
        double a2 = t.a((String) map.get("&sf"), 100.0d);
        if (a2 >= 100.0d) {
            return false;
        }
        if (((double) (a((String) map.get("&cid")) % 10000)) < a2 * 100.0d) {
            return false;
        }
        j.c(String.format("%s hit sampled out", map.get("&t") == null ? "unknown" : (String) map.get("&t")));
        return true;
    }

    /* access modifiers changed from: private */
    public void d(Map map) {
        ak a2 = i.a(this.i);
        t.a(map, "&adid", a2.a("&adid"));
        t.a(map, "&ate", a2.a("&ate"));
    }

    /* access modifiers changed from: private */
    public void e(Map map) {
        ac a2 = ac.a();
        t.a(map, "&an", a2.a("&an"));
        t.a(map, "&av", a2.a("&av"));
        t.a(map, "&aid", a2.a("&aid"));
        t.a(map, "&aiid", a2.a("&aiid"));
        map.put("&v", "1");
    }

    @Override // com.google.android.gms.b.ab
    public void a() {
        a(new bc(this));
    }

    /* access modifiers changed from: package-private */
    public void a(Runnable runnable) {
        this.a.add(runnable);
    }

    @Override // com.google.android.gms.b.ab
    public void a(Map map) {
        HashMap hashMap = new HashMap(map);
        String str = (String) map.get("&ht");
        if (str != null) {
            try {
                Long.valueOf(str);
            } catch (NumberFormatException e2) {
                str = null;
            }
        }
        if (str == null) {
            hashMap.put("&ht", Long.toString(System.currentTimeMillis()));
        }
        a(new bb(this, hashMap));
    }

    @Override // com.google.android.gms.b.ab
    public void b() {
        a(new bd(this));
    }

    @Override // com.google.android.gms.b.ab
    public LinkedBlockingQueue c() {
        return this.a;
    }

    @Override // com.google.android.gms.b.ab
    public Thread d() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.h.c();
        this.d = new ArrayList();
        this.d.add(new ef("appendVersion", "&_v".substring(1), "ma4.0.1"));
        this.d.add(new ef("appendQueueTime", "&qt".substring(1), null));
        this.d.add(new ef("appendCacheBuster", "&z".substring(1), null));
    }

    public void run() {
        Process.setThreadPriority(10);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e2) {
            j.d("sleep interrupted in GAThread initialize");
        }
        try {
            if (this.h == null) {
                this.h = new ar(this.i, this);
            }
            e();
            this.f = ad.a().a("&cid");
            if (this.f == null) {
                this.b = true;
            }
            this.e = b(this.i);
            j.c("Initialized GA Thread");
        } catch (Throwable th) {
            j.a("Error initializing the GAThread: " + a(th));
            j.a("Google Analytics will not start up.");
            this.b = true;
        }
        while (!this.c) {
            try {
                Runnable runnable = (Runnable) this.a.take();
                if (!this.b) {
                    runnable.run();
                }
            } catch (InterruptedException e3) {
                j.b(e3.toString());
            } catch (Throwable th2) {
                j.a("Error on GAThread: " + a(th2));
                j.a("Google Analytics is shutting down.");
                this.b = true;
            }
        }
    }
}
