package com.google.android.gms.b;

import android.text.TextUtils;
import java.util.Map;

class bb implements Runnable {
    final /* synthetic */ Map a;
    final /* synthetic */ ba b;

    bb(ba baVar, Map map) {
        this.b = baVar;
        this.a = map;
    }

    public void run() {
        if (TextUtils.isEmpty((CharSequence) this.a.get("&cid"))) {
            this.a.put("&cid", this.b.f);
        }
        if (!a.a(this.b.i).c() && !(this.b.c(this.a))) {
            if (!TextUtils.isEmpty(this.b.e)) {
                be.a().a(true);
                this.a.putAll(new c().a(this.b.e).a());
                be.a().a(false);
                this.b.e = null;
            }
            this.b.e((ba) this.a);
            this.b.d((ba) this.a);
            this.b.h.a(bk.a(this.a), Long.valueOf((String) this.a.get("&ht")).longValue(), this.b.b(this.a), this.b.d);
        }
    }
}
