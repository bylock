package com.google.android.gms.b;

import android.text.TextUtils;

/* access modifiers changed from: package-private */
public class bj {
    private String a;
    private final long b;
    private final long c;
    private String d = "https:";

    bj(String str, long j, long j2) {
        this.a = str;
        this.b = j;
        this.c = j2;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: package-private */
    public long b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (str != null && !TextUtils.isEmpty(str.trim()) && str.toLowerCase().startsWith("http:")) {
            this.d = "http:";
        }
    }

    /* access modifiers changed from: package-private */
    public long c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.d;
    }
}
