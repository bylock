package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.a.a.a;
import com.google.android.gms.a.a.b;
import java.io.IOException;

/* access modifiers changed from: package-private */
public class i implements ak {
    private static Object a = new Object();
    private static i b;
    private b c;
    private long d;
    private Context e;

    i(Context context) {
        this.e = context;
    }

    private b a() {
        try {
            return a.b(this.e);
        } catch (IllegalStateException e2) {
            j.d("IllegalStateException getting Ad Id Info. If you would like to see Audience reports, please ensure that you have added '<meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />' to your application manifest file. See http://goo.gl/naFqQk for details.");
            return null;
        } catch (com.google.android.gms.common.b e3) {
            j.d("GooglePlayServicesRepairableException getting Ad Id Info");
            return null;
        } catch (IOException e4) {
            j.d("IOException getting Ad Id Info");
            return null;
        } catch (com.google.android.gms.common.a e5) {
            j.d("GooglePlayServicesNotAvailableException getting Ad Id Info");
            return null;
        } catch (Exception e6) {
            j.d("Unknown exception. Could not get the ad Id.");
            return null;
        }
    }

    public static ak a(Context context) {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new i(context);
                }
            }
        }
        return b;
    }

    @Override // com.google.android.gms.b.ak
    public String a(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.d > 1000) {
            this.c = a();
            this.d = currentTimeMillis;
        }
        if (this.c != null) {
            if ("&adid".equals(str)) {
                return this.c.a();
            }
            if ("&ate".equals(str)) {
                return this.c.b() ? "0" : "1";
            }
        }
        return null;
    }
}
