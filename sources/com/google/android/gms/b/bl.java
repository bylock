package com.google.android.gms.b;

/* access modifiers changed from: package-private */
public class bl implements o {
    private final long a;
    private final int b;
    private double c;
    private long d;
    private final Object e;
    private final String f;

    public bl(int i, long j, String str) {
        this.e = new Object();
        this.b = i;
        this.c = (double) this.b;
        this.a = j;
        this.f = str;
    }

    public bl(String str) {
        this(60, 2000, str);
    }

    @Override // com.google.android.gms.b.o
    public boolean a() {
        boolean z;
        synchronized (this.e) {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.c < ((double) this.b)) {
                double d2 = ((double) (currentTimeMillis - this.d)) / ((double) this.a);
                if (d2 > 0.0d) {
                    this.c = Math.min((double) this.b, d2 + this.c);
                }
            }
            this.d = currentTimeMillis;
            if (this.c >= 1.0d) {
                this.c -= 1.0d;
                z = true;
            } else {
                j.d("Excessive " + this.f + " detected; call ignored.");
                z = false;
            }
        }
        return z;
    }
}
