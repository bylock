package com.google.android.gms.b;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

class ad implements ak {
    private static ad a;
    private static final Object b = new Object();
    private final Context c;
    private String d;
    private boolean e = false;
    private final Object f = new Object();

    protected ad(Context context) {
        this.c = context;
        e();
    }

    public static ad a() {
        ad adVar;
        synchronized (b) {
            adVar = a;
        }
        return adVar;
    }

    public static void a(Context context) {
        synchronized (b) {
            if (a == null) {
                a = new ad(context);
            }
        }
    }

    private boolean b(String str) {
        try {
            j.c("Storing clientId.");
            FileOutputStream openFileOutput = this.c.openFileOutput("gaClientId", 0);
            openFileOutput.write(str.getBytes());
            openFileOutput.close();
            return true;
        } catch (FileNotFoundException e2) {
            j.a("Error creating clientId file.");
            return false;
        } catch (IOException e3) {
            j.a("Error writing to clientId file.");
            return false;
        }
    }

    private String d() {
        if (!this.e) {
            synchronized (this.f) {
                if (!this.e) {
                    j.c("Waiting for clientId to load");
                    do {
                        try {
                            this.f.wait();
                        } catch (InterruptedException e2) {
                            j.a("Exception while waiting for clientId: " + e2);
                        }
                    } while (!this.e);
                }
            }
        }
        j.c("Loaded clientId");
        return this.d;
    }

    private void e() {
        new ae(this, "client_id_fetcher").start();
    }

    @Override // com.google.android.gms.b.ak
    public String a(String str) {
        if ("&cid".equals(str)) {
            return d();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String b() {
        String lowerCase = UUID.randomUUID().toString().toLowerCase();
        try {
            return !b(lowerCase) ? "0" : lowerCase;
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public String c() {
        String str = null;
        try {
            FileInputStream openFileInput = this.c.openFileInput("gaClientId");
            byte[] bArr = new byte[128];
            int read = openFileInput.read(bArr, 0, 128);
            if (openFileInput.available() > 0) {
                j.a("clientId file seems corrupted, deleting it.");
                openFileInput.close();
                this.c.deleteFile("gaClientId");
            } else if (read <= 0) {
                j.a("clientId file seems empty, deleting it.");
                openFileInput.close();
                this.c.deleteFile("gaClientId");
            } else {
                String str2 = new String(bArr, 0, read);
                try {
                    openFileInput.close();
                    str = str2;
                } catch (FileNotFoundException e2) {
                    str = str2;
                } catch (IOException e3) {
                    str = str2;
                    j.a("Error reading clientId file, deleting it.");
                    this.c.deleteFile("gaClientId");
                }
            }
        } catch (FileNotFoundException e4) {
        } catch (IOException e5) {
            j.a("Error reading clientId file, deleting it.");
            this.c.deleteFile("gaClientId");
        }
        return str == null ? b() : str;
    }
}
