package com.google.android.gms.b;

import java.util.TimerTask;

class aw extends TimerTask {
    final /* synthetic */ ar a;

    private aw(ar arVar) {
        this.a = arVar;
    }

    /* synthetic */ aw(ar arVar, as asVar) {
        this(arVar);
    }

    public void run() {
        if (ar.b(this.a) != av.CONNECTED_SERVICE || !ar.e(this.a).isEmpty() || ar.f(this.a) + ar.g(this.a) >= ar.h(this.a).a()) {
            ar.j(this.a).schedule(new aw(this.a), ar.g(this.a));
            return;
        }
        j.c("Disconnecting due to inactivity");
        ar.i(this.a);
    }
}
