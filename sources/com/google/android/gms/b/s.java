package com.google.android.gms.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

/* access modifiers changed from: package-private */
public class s implements al {
    private final String a;
    private final HttpClient b;
    private final Context c;
    private a d;
    private URL e;

    s(HttpClient httpClient, Context context) {
        this(httpClient, a.a(context), context);
    }

    s(HttpClient httpClient, a aVar, Context context) {
        this.c = context.getApplicationContext();
        this.a = a("GoogleAnalytics", "3.0", Build.VERSION.RELEASE, t.a(Locale.getDefault()), Build.MODEL, Build.ID);
        this.b = httpClient;
        this.d = aVar;
    }

    private HttpEntityEnclosingRequest a(String str, String str2) {
        BasicHttpEntityEnclosingRequest basicHttpEntityEnclosingRequest;
        if (TextUtils.isEmpty(str)) {
            j.d("Empty hit, discarding.");
            return null;
        }
        String str3 = str2 + "?" + str;
        if (str3.length() < 2036) {
            basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("GET", str3);
        } else {
            basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("POST", str2);
            try {
                basicHttpEntityEnclosingRequest.setEntity(new StringEntity(str));
            } catch (UnsupportedEncodingException e2) {
                j.d("Encoding error, discarding hit");
                return null;
            }
        }
        basicHttpEntityEnclosingRequest.addHeader("User-Agent", this.a);
        return basicHttpEntityEnclosingRequest;
    }

    private void a(k kVar, URL url, boolean z) {
        URL url2;
        if (!TextUtils.isEmpty(kVar.a()) && b()) {
            if (url == null) {
                try {
                    url2 = this.e != null ? this.e : new URL("https://ssl.google-analytics.com/collect");
                } catch (MalformedURLException e2) {
                    return;
                }
            } else {
                url2 = url;
            }
            HttpHost httpHost = new HttpHost(url2.getHost(), url2.getPort(), url2.getProtocol());
            try {
                HttpEntityEnclosingRequest a2 = a(kVar.a(), url2.getPath());
                if (a2 != null) {
                    a2.addHeader("Host", httpHost.toHostString());
                    if (j.a()) {
                        a(a2);
                    }
                    if (z) {
                        an.b(this.c);
                    }
                    HttpResponse execute = this.b.execute(httpHost, a2);
                    int statusCode = execute.getStatusLine().getStatusCode();
                    HttpEntity entity = execute.getEntity();
                    if (entity != null) {
                        entity.consumeContent();
                    }
                    if (statusCode != 200) {
                        j.d("Bad response: " + execute.getStatusLine().getStatusCode());
                    }
                }
            } catch (ClientProtocolException e3) {
                j.d("ClientProtocolException sending monitoring hit.");
            } catch (IOException e4) {
                j.d("Exception sending monitoring hit: " + e4.getClass().getSimpleName());
                j.d(e4.getMessage());
            }
        }
    }

    private void a(HttpEntityEnclosingRequest httpEntityEnclosingRequest) {
        int available;
        StringBuffer stringBuffer = new StringBuffer();
        for (Header header : httpEntityEnclosingRequest.getAllHeaders()) {
            stringBuffer.append(header.toString()).append("\n");
        }
        stringBuffer.append(httpEntityEnclosingRequest.getRequestLine().toString()).append("\n");
        if (httpEntityEnclosingRequest.getEntity() != null) {
            try {
                InputStream content = httpEntityEnclosingRequest.getEntity().getContent();
                if (content != null && (available = content.available()) > 0) {
                    byte[] bArr = new byte[available];
                    content.read(bArr);
                    stringBuffer.append("POST:\n");
                    stringBuffer.append(new String(bArr)).append("\n");
                }
            } catch (IOException e2) {
                j.c("Error Writing hit to log...");
            }
        }
        j.c(stringBuffer.toString());
    }

    @Override // com.google.android.gms.b.al
    public int a(List list, k kVar, boolean z) {
        int i;
        URL url;
        int i2 = 0;
        int min = Math.min(list.size(), 40);
        kVar.a("_hr", list.size());
        int i3 = 0;
        URL url2 = null;
        boolean z2 = true;
        int i4 = 0;
        while (i4 < min) {
            bj bjVar = (bj) list.get(i4);
            URL a2 = a(bjVar);
            if (a2 == null) {
                if (j.a()) {
                    j.d("No destination: discarding hit: " + bjVar.a());
                } else {
                    j.d("No destination: discarding hit.");
                }
                i3++;
                i = i2 + 1;
                url = url2;
            } else {
                HttpHost httpHost = new HttpHost(a2.getHost(), a2.getPort(), a2.getProtocol());
                String path = a2.getPath();
                String a3 = TextUtils.isEmpty(bjVar.a()) ? "" : bk.a(bjVar, System.currentTimeMillis());
                HttpEntityEnclosingRequest a4 = a(a3, path);
                if (a4 == null) {
                    i3++;
                    i = i2 + 1;
                    url = a2;
                } else {
                    a4.addHeader("Host", httpHost.toHostString());
                    if (j.a()) {
                        a(a4);
                    }
                    if (a3.length() > 8192) {
                        j.d("Hit too long (> 8192 bytes)--not sent");
                        i3++;
                    } else if (this.d.b()) {
                        j.b("Dry run enabled. Hit not actually sent.");
                    } else {
                        if (z2) {
                            try {
                                an.b(this.c);
                                z2 = false;
                            } catch (ClientProtocolException e2) {
                                j.d("ClientProtocolException sending hit; discarding hit...");
                                kVar.a("_hd", i3);
                            } catch (IOException e3) {
                                j.d("Exception sending hit: " + e3.getClass().getSimpleName());
                                j.d(e3.getMessage());
                                kVar.a("_de", 1);
                                kVar.a("_hd", i3);
                                kVar.a("_hs", i2);
                                a(kVar, a2, z2);
                                return i2;
                            }
                        }
                        HttpResponse execute = this.b.execute(httpHost, a4);
                        int statusCode = execute.getStatusLine().getStatusCode();
                        HttpEntity entity = execute.getEntity();
                        if (entity != null) {
                            entity.consumeContent();
                        }
                        if (statusCode != 200) {
                            j.d("Bad response: " + execute.getStatusLine().getStatusCode());
                        }
                    }
                    kVar.a("_td", a3.getBytes().length);
                    i = i2 + 1;
                    url = a2;
                }
            }
            i4++;
            i2 = i;
            url2 = url;
        }
        kVar.a("_hd", i3);
        kVar.a("_hs", i2);
        if (z) {
            a(kVar, url2, z2);
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2, String str3, String str4, String str5, String str6) {
        return String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", str, str2, str3, str4, str5, str6);
    }

    /* access modifiers changed from: package-private */
    public URL a(bj bjVar) {
        if (this.e != null) {
            return this.e;
        }
        try {
            return new URL("http:".equals(bjVar.d()) ? "http://www.google-analytics.com/collect" : "https://ssl.google-analytics.com/collect");
        } catch (MalformedURLException e2) {
            j.a("Error trying to parse the hardcoded host url. This really shouldn't happen.");
            return null;
        }
    }

    @Override // com.google.android.gms.b.al
    public void a(String str) {
        try {
            this.e = new URL(str);
        } catch (MalformedURLException e2) {
            this.e = null;
        }
    }

    @Override // com.google.android.gms.b.al
    public boolean a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.c.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        j.c("...no network connectivity");
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return Math.random() * 100.0d <= 1.0d;
    }
}
