package com.google.android.gms.b;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class a extends h {
    private static boolean a;
    private static a l;
    private boolean b;
    private ab c;
    private q d;
    private Context e;
    private volatile Boolean f;
    private d g;
    private String h;
    private String i;
    private Set j;
    private boolean k;

    protected a(Context context) {
        this(context, ba.a(context), ao.c());
    }

    private a(Context context, ab abVar, q qVar) {
        this.f = false;
        this.k = false;
        if (context == null) {
            throw new IllegalArgumentException("context cannot be null");
        }
        this.e = context.getApplicationContext();
        this.c = abVar;
        this.d = qVar;
        ac.a(this.e);
        p.a(this.e);
        ad.a(this.e);
        this.g = new aj();
        this.j = new HashSet();
        f();
    }

    static a a() {
        a aVar;
        synchronized (a.class) {
            aVar = l;
        }
        return aVar;
    }

    public static a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (l == null) {
                l = new a(context);
            }
            aVar = l;
        }
        return aVar;
    }

    private e a(e eVar) {
        if (this.i != null) {
            eVar.a("&an", this.i);
        }
        if (this.h != null) {
            eVar.a("&av", this.h);
        }
        return eVar;
    }

    private int b(String str) {
        String lowerCase = str.toLowerCase();
        if ("verbose".equals(lowerCase)) {
            return 0;
        }
        if ("info".equals(lowerCase)) {
            return 1;
        }
        if ("warning".equals(lowerCase)) {
            return 2;
        }
        return "error".equals(lowerCase) ? 3 : -1;
    }

    private void f() {
        ApplicationInfo applicationInfo;
        int i2;
        bi biVar;
        if (!a) {
            try {
                applicationInfo = this.e.getPackageManager().getApplicationInfo(this.e.getPackageName(), 129);
            } catch (PackageManager.NameNotFoundException e2) {
                j.c("PackageManager doesn't know about package: " + e2);
                applicationInfo = null;
            }
            if (applicationInfo == null) {
                j.d("Couldn't get ApplicationInfo to load gloabl config.");
                return;
            }
            Bundle bundle = applicationInfo.metaData;
            if (bundle != null && (i2 = bundle.getInt("com.google.android.gms.analytics.globalConfigResource")) > 0 && (biVar = (bi) new bg(this.e).a(i2)) != null) {
                a(biVar);
            }
        }
    }

    public e a(String str) {
        e a2;
        synchronized (this) {
            be.a().a(bf.GET_TRACKER);
            a2 = a(new e(str, this));
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public void a(bi biVar) {
        int b2;
        j.c("Loading global config values.");
        if (biVar.a()) {
            this.i = biVar.b();
            j.c("app name loaded: " + this.i);
        }
        if (biVar.c()) {
            this.h = biVar.d();
            j.c("app version loaded: " + this.h);
        }
        if (biVar.e() && (b2 = b(biVar.f())) >= 0) {
            j.c("log level loaded: " + b2);
            d().a(b2);
        }
        if (biVar.g()) {
            this.d.a(biVar.h());
        }
        if (biVar.i()) {
            a(biVar.j());
        }
    }

    public void a(d dVar) {
        be.a().a(bf.SET_LOGGER);
        this.g = dVar;
    }

    /* access modifiers changed from: package-private */
    @Override // com.google.android.gms.b.h
    public void a(Map map) {
        synchronized (this) {
            if (map == null) {
                throw new IllegalArgumentException("hit cannot be null");
            }
            t.a(map, "&ul", t.a(Locale.getDefault()));
            t.a(map, "&sr", p.a().a("&sr"));
            map.put("&_u", be.a().c());
            be.a().b();
            this.c.a(map);
        }
    }

    public void a(boolean z) {
        be.a().a(bf.SET_DRY_RUN);
        this.b = z;
    }

    public boolean b() {
        be.a().a(bf.GET_DRY_RUN);
        return this.b;
    }

    public boolean c() {
        be.a().a(bf.GET_APP_OPT_OUT);
        return this.f.booleanValue();
    }

    public d d() {
        return this.g;
    }

    @Deprecated
    public void e() {
        this.d.a();
    }
}
