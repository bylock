package android.support.v4.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ak;
import android.support.v4.view.av;
import android.support.v4.view.q;
import android.support.v4.view.v;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: MyApp */
public class DrawerLayout extends ViewGroup {
    private static final int[] a = {16842931};
    private int b;
    private int c;
    private float d;
    private Paint e;
    private final af f;
    private final af g;
    private final j h;
    private final j i;
    private int j;
    private boolean k;
    private boolean l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private g q;
    private float r;
    private float s;
    private Drawable t;
    private Drawable u;

    public void setScrimColor(int i2) {
        this.c = i2;
        invalidate();
    }

    public void setDrawerListener(g gVar) {
        this.q = gVar;
    }

    public void setDrawerLockMode(int i2) {
        a(i2, 3);
        a(i2, 5);
    }

    public void a(int i2, int i3) {
        int a2 = q.a(i3, av.e(this));
        if (a2 == 3) {
            this.m = i2;
        } else if (a2 == 5) {
            this.n = i2;
        }
        if (i2 != 0) {
            (a2 == 3 ? this.f : this.g).e();
        }
        switch (i2) {
            case 1:
                View a3 = a(a2);
                if (a3 != null) {
                    i(a3);
                    return;
                }
                return;
            case 2:
                View a4 = a(a2);
                if (a4 != null) {
                    h(a4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public int a(View view) {
        int e2 = e(view);
        if (e2 == 3) {
            return this.m;
        }
        if (e2 == 5) {
            return this.n;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, View view) {
        int i4 = 1;
        int a2 = this.f.a();
        int a3 = this.g.a();
        if (!(a2 == 1 || a3 == 1)) {
            i4 = (a2 == 2 || a3 == 2) ? 2 : 0;
        }
        if (view != null && i3 == 0) {
            h hVar = (h) view.getLayoutParams();
            if (hVar.b == 0.0f) {
                b(view);
            } else if (hVar.b == 1.0f) {
                c(view);
            }
        }
        if (i4 != this.j) {
            this.j = i4;
            if (this.q != null) {
                this.q.a(i4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view) {
        h hVar = (h) view.getLayoutParams();
        if (hVar.d) {
            hVar.d = false;
            if (this.q != null) {
                this.q.b(view);
            }
            sendAccessibilityEvent(32);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(View view) {
        h hVar = (h) view.getLayoutParams();
        if (!hVar.d) {
            hVar.d = true;
            if (this.q != null) {
                this.q.a(view);
            }
            view.sendAccessibilityEvent(32);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, float f2) {
        if (this.q != null) {
            this.q.a(view, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(View view, float f2) {
        h hVar = (h) view.getLayoutParams();
        if (f2 != hVar.b) {
            hVar.b = f2;
            a(view, f2);
        }
    }

    /* access modifiers changed from: package-private */
    public float d(View view) {
        return ((h) view.getLayoutParams()).b;
    }

    /* access modifiers changed from: package-private */
    public int e(View view) {
        return q.a(((h) view.getLayoutParams()).a, av.e(view));
    }

    /* access modifiers changed from: package-private */
    public boolean a(View view, int i2) {
        return (e(view) & i2) == i2;
    }

    /* access modifiers changed from: package-private */
    public View a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (((h) childAt.getLayoutParams()).d) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public View a(int i2) {
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            if ((e(childAt) & 7) == (i2 & 7)) {
                return childAt;
            }
        }
        return null;
    }

    static String b(int i2) {
        if ((i2 & 3) == 3) {
            return "LEFT";
        }
        if ((i2 & 5) == 5) {
            return "RIGHT";
        }
        return Integer.toHexString(i2);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.l = true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.l = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        if (r5 != 0) goto L_0x0046;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r12, int r13) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        float f2;
        this.k = true;
        int i7 = i4 - i2;
        int childCount = getChildCount();
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            if (childAt.getVisibility() != 8) {
                h hVar = (h) childAt.getLayoutParams();
                if (f(childAt)) {
                    childAt.layout(hVar.leftMargin, hVar.topMargin, hVar.leftMargin + childAt.getMeasuredWidth(), hVar.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (a(childAt, 3)) {
                        i6 = ((int) (((float) measuredWidth) * hVar.b)) + (-measuredWidth);
                        f2 = ((float) (measuredWidth + i6)) / ((float) measuredWidth);
                    } else {
                        i6 = i7 - ((int) (((float) measuredWidth) * hVar.b));
                        f2 = ((float) (i7 - i6)) / ((float) measuredWidth);
                    }
                    boolean z2 = f2 != hVar.b;
                    switch (hVar.a & 112) {
                        case 16:
                            int i9 = i5 - i3;
                            int i10 = (i9 - measuredHeight) / 2;
                            if (i10 < hVar.topMargin) {
                                i10 = hVar.topMargin;
                            } else if (i10 + measuredHeight > i9 - hVar.bottomMargin) {
                                i10 = (i9 - hVar.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i6, i10, measuredWidth + i6, measuredHeight + i10);
                            break;
                        case 80:
                            int i11 = i5 - i3;
                            childAt.layout(i6, (i11 - hVar.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i6, i11 - hVar.bottomMargin);
                            break;
                        default:
                            childAt.layout(i6, hVar.topMargin, measuredWidth + i6, measuredHeight);
                            break;
                    }
                    if (z2) {
                        b(childAt, f2);
                    }
                    int i12 = hVar.b > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i12) {
                        childAt.setVisibility(i12);
                    }
                }
            }
        }
        this.k = false;
        this.l = false;
    }

    public void requestLayout() {
        if (!this.k) {
            super.requestLayout();
        }
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f2 = 0.0f;
        for (int i2 = 0; i2 < childCount; i2++) {
            f2 = Math.max(f2, ((h) getChildAt(i2).getLayoutParams()).b);
        }
        this.d = f2;
        if (this.f.a(true) || this.g.a(true)) {
            av.b(this);
        }
    }

    private static boolean k(View view) {
        Drawable background = view.getBackground();
        if (background == null || background.getOpacity() != -1) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        int i2;
        int height = getHeight();
        boolean f2 = f(view);
        int i3 = 0;
        int width = getWidth();
        int save = canvas.save();
        if (f2) {
            int childCount = getChildCount();
            int i4 = 0;
            while (i4 < childCount) {
                View childAt = getChildAt(i4);
                if (childAt != view && childAt.getVisibility() == 0 && k(childAt) && g(childAt)) {
                    if (childAt.getHeight() < height) {
                        i2 = width;
                    } else if (a(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right <= i3) {
                            right = i3;
                        }
                        i3 = right;
                        i2 = width;
                    } else {
                        i2 = childAt.getLeft();
                        if (i2 < width) {
                        }
                    }
                    i4++;
                    width = i2;
                }
                i2 = width;
                i4++;
                width = i2;
            }
            canvas.clipRect(i3, 0, width, getHeight());
        }
        boolean drawChild = super.drawChild(canvas, view, j2);
        canvas.restoreToCount(save);
        if (this.d > 0.0f && f2) {
            this.e.setColor((((int) (((float) ((this.c & -16777216) >>> 24)) * this.d)) << 24) | (this.c & 16777215));
            canvas.drawRect((float) i3, 0.0f, (float) width, (float) getHeight(), this.e);
        } else if (this.t != null && a(view, 3)) {
            int intrinsicWidth = this.t.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.f.b()), 1.0f));
            this.t.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.t.setAlpha((int) (255.0f * max));
            this.t.draw(canvas);
        } else if (this.u != null && a(view, 5)) {
            int intrinsicWidth2 = this.u.getIntrinsicWidth();
            int left = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left)) / ((float) this.g.b()), 1.0f));
            this.u.setBounds(left - intrinsicWidth2, view.getTop(), left, view.getBottom());
            this.u.setAlpha((int) (255.0f * max2));
            this.u.draw(canvas);
        }
        return drawChild;
    }

    /* access modifiers changed from: package-private */
    public boolean f(View view) {
        return ((h) view.getLayoutParams()).a == 0;
    }

    /* access modifiers changed from: package-private */
    public boolean g(View view) {
        return (q.a(((h) view.getLayoutParams()).a, av.e(view)) & 7) != 0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        int a2 = ak.a(motionEvent);
        boolean a3 = this.f.a(motionEvent) | this.g.a(motionEvent);
        switch (a2) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.r = x;
                this.s = y;
                if (this.d <= 0.0f || !f(this.f.d((int) x, (int) y))) {
                    z = false;
                } else {
                    z = true;
                }
                this.o = false;
                this.p = false;
                break;
            case 1:
            case 3:
                a(true);
                this.o = false;
                this.p = false;
                z = false;
                break;
            case 2:
                if (this.f.c(3)) {
                    this.h.a();
                    this.i.a();
                    z = false;
                    break;
                }
                z = false;
                break;
            default:
                z = false;
                break;
        }
        return a3 || z || d() || this.p;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View a2;
        this.f.b(motionEvent);
        this.g.b(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.r = x;
                this.s = y;
                this.o = false;
                this.p = false;
                break;
            case 1:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                View d2 = this.f.d((int) x2, (int) y2);
                if (d2 != null && f(d2)) {
                    float f2 = x2 - this.r;
                    float f3 = y2 - this.s;
                    int d3 = this.f.d();
                    if ((f2 * f2) + (f3 * f3) < ((float) (d3 * d3)) && (a2 = a()) != null) {
                        z = a(a2) == 2;
                        a(z);
                        this.o = false;
                        break;
                    }
                }
                z = true;
                a(z);
                this.o = false;
            case 3:
                a(true);
                this.o = false;
                this.p = false;
                break;
        }
        return true;
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        this.o = z;
        if (z) {
            a(true);
        }
    }

    public void b() {
        a(false);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            h hVar = (h) childAt.getLayoutParams();
            if (g(childAt) && (!z || hVar.c)) {
                int width = childAt.getWidth();
                if (a(childAt, 3)) {
                    z2 |= this.f.a(childAt, -width, childAt.getTop());
                } else {
                    z2 |= this.g.a(childAt, getWidth(), childAt.getTop());
                }
                hVar.c = false;
            }
        }
        this.h.a();
        this.i.a();
        if (z2) {
            invalidate();
        }
    }

    public void h(View view) {
        if (!g(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.l) {
            h hVar = (h) view.getLayoutParams();
            hVar.b = 1.0f;
            hVar.d = true;
        } else if (a(view, 3)) {
            this.f.a(view, 0, view.getTop());
        } else {
            this.g.a(view, getWidth() - view.getWidth(), view.getTop());
        }
        invalidate();
    }

    public void i(View view) {
        if (!g(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.l) {
            h hVar = (h) view.getLayoutParams();
            hVar.b = 0.0f;
            hVar.d = false;
        } else if (a(view, 3)) {
            this.f.a(view, -view.getWidth(), view.getTop());
        } else {
            this.g.a(view, getWidth(), view.getTop());
        }
        invalidate();
    }

    public boolean j(View view) {
        if (g(view)) {
            return ((h) view.getLayoutParams()).b > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    private boolean d() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (((h) getChildAt(i2).getLayoutParams()).c) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new h(-1, -1);
    }

    /* access modifiers changed from: protected */
    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof h) {
            return new h((h) layoutParams);
        }
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new h((ViewGroup.MarginLayoutParams) layoutParams) : new h(layoutParams);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof h) && super.checkLayoutParams(layoutParams);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new h(getContext(), attributeSet);
    }

    private boolean e() {
        return f() != null;
    }

    private View f() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (g(childAt) && j(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !e()) {
            return super.onKeyDown(i2, keyEvent);
        }
        v.b(keyEvent);
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyUp(i2, keyEvent);
        }
        View f2 = f();
        if (f2 != null && a(f2) == 0) {
            b();
        }
        return f2 != null;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View a2;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.a == 0 || (a2 = a(savedState.a)) == null)) {
            h(a2);
        }
        a(savedState.b, 3);
        a(savedState.c, 5);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        int childCount = getChildCount();
        int i2 = 0;
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            if (g(childAt)) {
                h hVar = (h) childAt.getLayoutParams();
                if (hVar.d) {
                    savedState.a = hVar.a;
                    break;
                }
            }
            i2++;
        }
        savedState.b = this.m;
        savedState.c = this.n;
        return savedState;
    }

    /* compiled from: MyApp */
    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new i();
        int a = 0;
        int b = 0;
        int c = 0;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
        }
    }
}
