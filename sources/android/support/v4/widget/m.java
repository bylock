package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;

/* compiled from: MyApp */
class m implements n {
    m() {
    }

    @Override // android.support.v4.widget.n
    public Object a(Context context) {
        return o.a(context);
    }

    @Override // android.support.v4.widget.n
    public void a(Object obj, int i, int i2) {
        o.a(obj, i, i2);
    }

    @Override // android.support.v4.widget.n
    public boolean a(Object obj) {
        return o.a(obj);
    }

    @Override // android.support.v4.widget.n
    public void b(Object obj) {
        o.b(obj);
    }

    @Override // android.support.v4.widget.n
    public boolean a(Object obj, float f) {
        return o.a(obj, f);
    }

    @Override // android.support.v4.widget.n
    public boolean c(Object obj) {
        return o.c(obj);
    }

    @Override // android.support.v4.widget.n
    public boolean a(Object obj, Canvas canvas) {
        return o.a(obj, canvas);
    }
}
