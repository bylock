package android.support.v4.widget;

import android.support.v4.view.av;
import android.view.View;

/* compiled from: MyApp */
class x implements Runnable {
    final View a;
    final /* synthetic */ SlidingPaneLayout b;

    x(SlidingPaneLayout slidingPaneLayout, View view) {
        this.b = slidingPaneLayout;
        this.a = view;
    }

    public void run() {
        if (this.a.getParent() == this.b) {
            av.a(this.a, 0, null);
            SlidingPaneLayout.a(this.b, this.a);
        }
        SlidingPaneLayout.a(this.b).remove(this);
    }
}
