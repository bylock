package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* compiled from: MyApp */
public class y extends ViewGroup.MarginLayoutParams {
    private static final int[] e = {16843137};
    public float a = 0.0f;
    boolean b;
    boolean c;
    Paint d;

    public y() {
        super(-1, -1);
    }

    public y(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public y(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }

    public y(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e);
        this.a = obtainStyledAttributes.getFloat(0, 0.0f);
        obtainStyledAttributes.recycle();
    }
}
