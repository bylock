package android.support.v4.widget;

import android.database.ContentObserver;
import android.os.Handler;

/* compiled from: MyApp */
class c extends ContentObserver {
    final /* synthetic */ a a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(a aVar) {
        super(new Handler());
        this.a = aVar;
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean z) {
        this.a.b();
    }
}
