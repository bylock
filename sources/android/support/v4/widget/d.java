package android.support.v4.widget;

import android.database.DataSetObserver;

/* compiled from: MyApp */
class d extends DataSetObserver {
    final /* synthetic */ a a;

    private d(a aVar) {
        this.a = aVar;
    }

    public void onChanged() {
        this.a.a = true;
        this.a.notifyDataSetChanged();
    }

    public void onInvalidated() {
        this.a.a = false;
        this.a.notifyDataSetInvalidated();
    }
}
