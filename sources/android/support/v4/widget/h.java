package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* compiled from: MyApp */
public class h extends ViewGroup.MarginLayoutParams {
    public int a = 0;
    float b;
    boolean c;
    boolean d;

    public h(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.c());
        this.a = obtainStyledAttributes.getInt(0, 0);
        obtainStyledAttributes.recycle();
    }

    public h(int i, int i2) {
        super(i, i2);
    }

    public h(h hVar) {
        super((ViewGroup.MarginLayoutParams) hVar);
        this.a = hVar.a;
    }

    public h(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public h(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
