package android.support.v4.widget;

import android.view.View;

/* compiled from: MyApp */
class j extends ah {
    final /* synthetic */ DrawerLayout a;
    private final int b;
    private af c;
    private final Runnable d;

    public void a() {
        this.a.removeCallbacks(this.d);
    }

    @Override // android.support.v4.widget.ah
    public boolean a(View view, int i) {
        return this.a.g(view) && this.a.a(view, this.b) && this.a.a(view) == 0;
    }

    @Override // android.support.v4.widget.ah
    public void a(int i) {
        this.a.a(this.b, i, this.c.c());
    }

    @Override // android.support.v4.widget.ah
    public void a(View view, int i, int i2, int i3, int i4) {
        float width;
        int width2 = view.getWidth();
        if (this.a.a(view, 3)) {
            width = ((float) (width2 + i)) / ((float) width2);
        } else {
            width = ((float) (this.a.getWidth() - i)) / ((float) width2);
        }
        this.a.b(view, width);
        view.setVisibility(width == 0.0f ? 4 : 0);
        this.a.invalidate();
    }

    @Override // android.support.v4.widget.ah
    public void b(View view, int i) {
        ((h) view.getLayoutParams()).c = false;
        b();
    }

    private void b() {
        int i = 3;
        if (this.b == 3) {
            i = 5;
        }
        View a2 = this.a.a(i);
        if (a2 != null) {
            this.a.i(a2);
        }
    }

    @Override // android.support.v4.widget.ah
    public void a(View view, float f, float f2) {
        int width;
        float d2 = this.a.d(view);
        int width2 = view.getWidth();
        if (this.a.a(view, 3)) {
            width = (f > 0.0f || (f == 0.0f && d2 > 0.5f)) ? 0 : -width2;
        } else {
            width = this.a.getWidth();
            if (f < 0.0f || (f == 0.0f && d2 < 0.5f)) {
                width -= width2;
            }
        }
        this.c.a(width, view.getTop());
        this.a.invalidate();
    }

    @Override // android.support.v4.widget.ah
    public void a(int i, int i2) {
        this.a.postDelayed(this.d, 160);
    }

    @Override // android.support.v4.widget.ah
    public boolean b(int i) {
        return false;
    }

    @Override // android.support.v4.widget.ah
    public void b(int i, int i2) {
        View a2;
        if ((i & 1) == 1) {
            a2 = this.a.a(3);
        } else {
            a2 = this.a.a(5);
        }
        if (a2 != null && this.a.a(a2) == 0) {
            this.c.a(a2, i2);
        }
    }

    @Override // android.support.v4.widget.ah
    public int a(View view) {
        return view.getWidth();
    }

    @Override // android.support.v4.widget.ah
    public int a(View view, int i, int i2) {
        if (this.a.a(view, 3)) {
            return Math.max(-view.getWidth(), Math.min(i, 0));
        }
        int width = this.a.getWidth();
        return Math.max(width - view.getWidth(), Math.min(i, width));
    }

    @Override // android.support.v4.widget.ah
    public int b(View view, int i, int i2) {
        return view.getTop();
    }
}
