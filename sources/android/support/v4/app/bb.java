package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.a.a;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: MyApp */
public class bb implements Iterable {
    private static final bd a;
    private final ArrayList b = new ArrayList();
    private final Context c;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            a = new bf();
        } else {
            a = new be();
        }
    }

    private bb(Context context) {
        this.c = context;
    }

    public static bb a(Context context) {
        return new bb(context);
    }

    public bb a(Intent intent) {
        this.b.add(intent);
        return this;
    }

    public bb a(Activity activity) {
        Intent intent;
        Intent intent2 = null;
        if (activity instanceof bc) {
            intent2 = ((bc) activity).a();
        }
        if (intent2 == null) {
            intent = af.a(activity);
        } else {
            intent = intent2;
        }
        if (intent != null) {
            ComponentName component = intent.getComponent();
            if (component == null) {
                component = intent.resolveActivity(this.c.getPackageManager());
            }
            a(component);
            a(intent);
        }
        return this;
    }

    public bb a(ComponentName componentName) {
        int size = this.b.size();
        try {
            Intent a2 = af.a(this.c, componentName);
            while (a2 != null) {
                this.b.add(size, a2);
                a2 = af.a(this.c, a2.getComponent());
            }
            return this;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e);
        }
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return this.b.iterator();
    }

    public void a() {
        a((Bundle) null);
    }

    public void a(Bundle bundle) {
        if (this.b.isEmpty()) {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
        Intent[] intentArr = (Intent[]) this.b.toArray(new Intent[this.b.size()]);
        intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
        if (!a.a(this.c, intentArr, bundle)) {
            Intent intent = new Intent(intentArr[intentArr.length - 1]);
            intent.addFlags(268435456);
            this.c.startActivity(intent);
        }
    }
}
