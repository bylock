package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public final class BackStackState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new f();
    final int[] a;
    final int b;
    final int c;
    final String d;
    final int e;
    final int f;
    final CharSequence g;
    final int h;
    final CharSequence i;

    public BackStackState(r rVar, d dVar) {
        int i2 = 0;
        for (e eVar = dVar.b; eVar != null; eVar = eVar.a) {
            if (eVar.i != null) {
                i2 += eVar.i.size();
            }
        }
        this.a = new int[(i2 + (dVar.d * 7))];
        if (!dVar.k) {
            throw new IllegalStateException("Not on back stack");
        }
        int i3 = 0;
        for (e eVar2 = dVar.b; eVar2 != null; eVar2 = eVar2.a) {
            int i4 = i3 + 1;
            this.a[i3] = eVar2.c;
            int i5 = i4 + 1;
            this.a[i4] = eVar2.d != null ? eVar2.d.f : -1;
            int i6 = i5 + 1;
            this.a[i5] = eVar2.e;
            int i7 = i6 + 1;
            this.a[i6] = eVar2.f;
            int i8 = i7 + 1;
            this.a[i7] = eVar2.g;
            int i9 = i8 + 1;
            this.a[i8] = eVar2.h;
            if (eVar2.i != null) {
                int size = eVar2.i.size();
                int i10 = i9 + 1;
                this.a[i9] = size;
                int i11 = 0;
                while (i11 < size) {
                    this.a[i10] = ((Fragment) eVar2.i.get(i11)).f;
                    i11++;
                    i10++;
                }
                i3 = i10;
            } else {
                i3 = i9 + 1;
                this.a[i9] = 0;
            }
        }
        this.b = dVar.i;
        this.c = dVar.j;
        this.d = dVar.m;
        this.e = dVar.o;
        this.f = dVar.p;
        this.g = dVar.q;
        this.h = dVar.r;
        this.i = dVar.s;
    }

    public BackStackState(Parcel parcel) {
        this.a = parcel.createIntArray();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
    }

    public d a(r rVar) {
        d dVar = new d(rVar);
        int i2 = 0;
        int i3 = 0;
        while (i3 < this.a.length) {
            e eVar = new e();
            int i4 = i3 + 1;
            eVar.c = this.a[i3];
            if (r.a) {
                Log.v("FragmentManager", "Instantiate " + dVar + " op #" + i2 + " base fragment #" + this.a[i4]);
            }
            int i5 = i4 + 1;
            int i6 = this.a[i4];
            if (i6 >= 0) {
                eVar.d = (Fragment) rVar.f.get(i6);
            } else {
                eVar.d = null;
            }
            int i7 = i5 + 1;
            eVar.e = this.a[i5];
            int i8 = i7 + 1;
            eVar.f = this.a[i7];
            int i9 = i8 + 1;
            eVar.g = this.a[i8];
            int i10 = i9 + 1;
            eVar.h = this.a[i9];
            int i11 = i10 + 1;
            int i12 = this.a[i10];
            if (i12 > 0) {
                eVar.i = new ArrayList(i12);
                int i13 = 0;
                while (i13 < i12) {
                    if (r.a) {
                        Log.v("FragmentManager", "Instantiate " + dVar + " set remove fragment #" + this.a[i11]);
                    }
                    eVar.i.add((Fragment) rVar.f.get(this.a[i11]));
                    i13++;
                    i11++;
                }
            }
            dVar.a(eVar);
            i2++;
            i3 = i11;
        }
        dVar.i = this.b;
        dVar.j = this.c;
        dVar.m = this.d;
        dVar.o = this.e;
        dVar.k = true;
        dVar.p = this.f;
        dVar.q = this.g;
        dVar.r = this.h;
        dVar.s = this.i;
        dVar.a(1);
        return dVar;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        TextUtils.writeToParcel(this.g, parcel, 0);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
    }
}
