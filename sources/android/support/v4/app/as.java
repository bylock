package android.support.v4.app;

import android.app.Notification;

/* compiled from: MyApp */
class as implements ar {
    as() {
    }

    @Override // android.support.v4.app.ar
    public Notification a(ap apVar) {
        Notification notification = apVar.r;
        notification.setLatestEventInfo(apVar.a, apVar.b, apVar.c, apVar.d);
        if (apVar.j > 0) {
            notification.flags |= 128;
        }
        return notification;
    }
}
