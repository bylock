package android.support.v4.app;

import android.view.animation.Animation;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class t implements Animation.AnimationListener {
    final /* synthetic */ Fragment a;
    final /* synthetic */ r b;

    t(r rVar, Fragment fragment) {
        this.b = rVar;
        this.a = fragment;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.a.b != null) {
            this.a.b = null;
            this.b.a(this.a, this.a.c, 0, 0, false);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
