package android.support.v4.app;

import android.app.Notification;
import java.util.Iterator;

/* compiled from: MyApp */
class av implements ar {
    av() {
    }

    @Override // android.support.v4.app.ar
    public Notification a(ap apVar) {
        az azVar = new az(apVar.a, apVar.r, apVar.b, apVar.c, apVar.h, apVar.f, apVar.i, apVar.d, apVar.e, apVar.g, apVar.n, apVar.o, apVar.p, apVar.k, apVar.j, apVar.m);
        Iterator it = apVar.q.iterator();
        while (it.hasNext()) {
            am amVar = (am) it.next();
            azVar.a(amVar.a, amVar.b, amVar.c);
        }
        if (apVar.l != null) {
            if (apVar.l instanceof ao) {
                ao aoVar = (ao) apVar.l;
                azVar.a(aoVar.d, aoVar.f, aoVar.e, aoVar.a);
            } else if (apVar.l instanceof aq) {
                aq aqVar = (aq) apVar.l;
                azVar.a(aqVar.d, aqVar.f, aqVar.e, aqVar.a);
            } else if (apVar.l instanceof an) {
                an anVar = (an) apVar.l;
                azVar.a(anVar.d, anVar.f, anVar.e, anVar.a, anVar.b, anVar.c);
            }
        }
        return azVar.a();
    }
}
