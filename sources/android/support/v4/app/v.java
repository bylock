package android.support.v4.app;

import android.os.Parcelable;
import android.support.v4.view.ap;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: MyApp */
public abstract class v extends ap {
    private final p a;
    private aa b = null;
    private Fragment c = null;

    public abstract Fragment a(int i);

    public v(p pVar) {
        this.a = pVar;
    }

    @Override // android.support.v4.view.ap
    public void a(ViewGroup viewGroup) {
    }

    @Override // android.support.v4.view.ap
    public Object a(ViewGroup viewGroup, int i) {
        if (this.b == null) {
            this.b = this.a.a();
        }
        long b2 = b(i);
        Fragment a2 = this.a.a(a(viewGroup.getId(), b2));
        if (a2 != null) {
            this.b.b(a2);
        } else {
            a2 = a(i);
            this.b.a(viewGroup.getId(), a2, a(viewGroup.getId(), b2));
        }
        if (a2 != this.c) {
            a2.c(false);
            a2.d(false);
        }
        return a2;
    }

    @Override // android.support.v4.view.ap
    public void a(ViewGroup viewGroup, int i, Object obj) {
        if (this.b == null) {
            this.b = this.a.a();
        }
        this.b.a((Fragment) obj);
    }

    @Override // android.support.v4.view.ap
    public void b(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.c) {
            if (this.c != null) {
                this.c.c(false);
                this.c.d(false);
            }
            if (fragment != null) {
                fragment.c(true);
                fragment.d(true);
            }
            this.c = fragment;
        }
    }

    @Override // android.support.v4.view.ap
    public void b(ViewGroup viewGroup) {
        if (this.b != null) {
            this.b.b();
            this.b = null;
            this.a.b();
        }
    }

    @Override // android.support.v4.view.ap
    public boolean a(View view, Object obj) {
        return ((Fragment) obj).g() == view;
    }

    @Override // android.support.v4.view.ap
    public Parcelable a() {
        return null;
    }

    @Override // android.support.v4.view.ap
    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    public long b(int i) {
        return (long) i;
    }

    private static String a(int i, long j) {
        return "android:switcher:" + i + ":" + j;
    }
}
