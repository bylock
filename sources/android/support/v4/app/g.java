package android.support.v4.app;

import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class g implements o {
    final /* synthetic */ Fragment a;

    g(Fragment fragment) {
        this.a = fragment;
    }

    @Override // android.support.v4.app.o
    public View a(int i) {
        if (this.a.I != null) {
            return this.a.I.findViewById(i);
        }
        throw new IllegalStateException("Fragment does not have a view");
    }
}
