package android.support.v4.app;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: MyApp */
public class j extends Activity {
    final Handler a = new k(this);
    final r b = new r();
    final o c = new l(this);
    boolean d;
    boolean e;
    boolean f;
    boolean g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    HashMap l;
    ad m;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        this.b.i();
        int i4 = i2 >> 16;
        if (i4 != 0) {
            int i5 = i4 - 1;
            if (this.b.f == null || i5 < 0 || i5 >= this.b.f.size()) {
                Log.w("FragmentActivity", "Activity result fragment index out of range: 0x" + Integer.toHexString(i2));
                return;
            }
            Fragment fragment = (Fragment) this.b.f.get(i5);
            if (fragment == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for index: 0x" + Integer.toHexString(i2));
            } else {
                fragment.a(65535 & i2, i3, intent);
            }
        } else {
            super.onActivityResult(i2, i3, intent);
        }
    }

    public void onBackPressed() {
        if (!this.b.c()) {
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.b.a(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        ArrayList arrayList;
        this.b.a(this, this.c, (Fragment) null);
        if (getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle);
        n nVar = (n) getLastNonConfigurationInstance();
        if (nVar != null) {
            this.l = nVar.e;
        }
        if (bundle != null) {
            Parcelable parcelable = bundle.getParcelable("android:support:fragments");
            r rVar = this.b;
            if (nVar != null) {
                arrayList = nVar.d;
            } else {
                arrayList = null;
            }
            rVar.a(parcelable, arrayList);
        }
        this.b.j();
    }

    public boolean onCreatePanelMenu(int i2, Menu menu) {
        if (i2 != 0) {
            return super.onCreatePanelMenu(i2, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i2, menu) | this.b.a(menu, getMenuInflater());
        if (Build.VERSION.SDK_INT >= 11) {
            return onCreatePanelMenu;
        }
        return true;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:14:0x005c */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x0064 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:19:0x006e */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0, types: [android.view.View] */
    /* JADX WARN: Type inference failed for: r1v1 */
    /* JADX WARN: Type inference failed for: r1v2 */
    /* JADX WARN: Type inference failed for: r1v3, types: [android.support.v4.app.Fragment, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r3v3, types: [android.support.v4.app.r] */
    /* JADX WARN: Type inference failed for: r1v19, types: [android.support.v4.app.Fragment] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View onCreateView(java.lang.String r10, android.content.Context r11, android.util.AttributeSet r12) {
        /*
        // Method dump skipped, instructions count: 359
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.j.onCreateView(java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a(false);
        this.b.r();
        if (this.m != null) {
            this.m.h();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 5 || i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i2, keyEvent);
        }
        onBackPressed();
        return true;
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.b.s();
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        switch (i2) {
            case 0:
                return this.b.a(menuItem);
            case 6:
                return this.b.b(menuItem);
            default:
                return false;
        }
    }

    public void onPanelClosed(int i2, Menu menu) {
        switch (i2) {
            case 0:
                this.b.b(menu);
                break;
        }
        super.onPanelClosed(i2, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.e = false;
        if (this.a.hasMessages(2)) {
            this.a.removeMessages(2);
            a_();
        }
        this.b.n();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.b.i();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.a.sendEmptyMessage(2);
        this.e = true;
        this.b.e();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.a.removeMessages(2);
        a_();
        this.b.e();
    }

    /* access modifiers changed from: protected */
    public void a_() {
        this.b.m();
    }

    public boolean onPreparePanel(int i2, View view, Menu menu) {
        if (i2 != 0 || menu == null) {
            return super.onPreparePanel(i2, view, menu);
        }
        if (this.i) {
            this.i = false;
            menu.clear();
            onCreatePanelMenu(i2, menu);
        }
        return super.onPreparePanel(i2, view, menu) | this.b.a(menu);
    }

    public final Object onRetainNonConfigurationInstance() {
        int i2 = 0;
        if (this.f) {
            a(true);
        }
        Object b2 = b();
        ArrayList g2 = this.b.g();
        if (this.l != null) {
            ad[] adVarArr = new ad[this.l.size()];
            this.l.values().toArray(adVarArr);
            if (adVarArr != null) {
                int i3 = 0;
                while (i2 < adVarArr.length) {
                    ad adVar = adVarArr[i2];
                    if (adVar.g) {
                        i3 = 1;
                    } else {
                        adVar.h();
                        this.l.remove(adVar.d);
                    }
                    i2++;
                }
                i2 = i3;
            }
        }
        if (g2 == null && i2 == 0 && b2 == null) {
            return null;
        }
        n nVar = new n();
        nVar.a = null;
        nVar.b = b2;
        nVar.c = null;
        nVar.d = g2;
        nVar.e = this.l;
        return nVar;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Parcelable h2 = this.b.h();
        if (h2 != null) {
            bundle.putParcelable("android:support:fragments", h2);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.f = false;
        this.g = false;
        this.a.removeMessages(1);
        if (!this.d) {
            this.d = true;
            this.b.k();
        }
        this.b.i();
        this.b.e();
        if (!this.k) {
            this.k = true;
            if (this.m != null) {
                this.m.b();
            } else if (!this.j) {
                this.m = a((String) null, this.k, false);
                if (this.m != null && !this.m.f) {
                    this.m.b();
                }
            }
            this.j = true;
        }
        this.b.l();
        if (this.l != null) {
            ad[] adVarArr = new ad[this.l.size()];
            this.l.values().toArray(adVarArr);
            if (adVarArr != null) {
                for (ad adVar : adVarArr) {
                    adVar.e();
                    adVar.g();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.f = true;
        this.a.sendEmptyMessage(1);
        this.b.o();
    }

    public Object b() {
        return null;
    }

    public void c() {
        if (Build.VERSION.SDK_INT >= 11) {
            b.a(this);
        } else {
            this.i = true;
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (Build.VERSION.SDK_INT >= 11) {
        }
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.d);
        printWriter.print("mResumed=");
        printWriter.print(this.e);
        printWriter.print(" mStopped=");
        printWriter.print(this.f);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.g);
        printWriter.print(str2);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.k);
        if (this.m != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.m)));
            printWriter.println(":");
            this.m.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
        this.b.a(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.println("View Hierarchy:");
        a(str + "  ", printWriter, getWindow().getDecorView());
    }

    private static String a(View view) {
        char c2;
        char c3;
        char c4;
        char c5;
        char c6;
        char c7;
        char c8;
        String str;
        char c9 = 'F';
        char c10 = '.';
        StringBuilder sb = new StringBuilder(128);
        sb.append(view.getClass().getName());
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(view)));
        sb.append(' ');
        switch (view.getVisibility()) {
            case 0:
                sb.append('V');
                break;
            case 4:
                sb.append('I');
                break;
            case 8:
                sb.append('G');
                break;
            default:
                sb.append('.');
                break;
        }
        if (view.isFocusable()) {
            c2 = 'F';
        } else {
            c2 = '.';
        }
        sb.append(c2);
        if (view.isEnabled()) {
            c3 = 'E';
        } else {
            c3 = '.';
        }
        sb.append(c3);
        sb.append(view.willNotDraw() ? '.' : 'D');
        if (view.isHorizontalScrollBarEnabled()) {
            c4 = 'H';
        } else {
            c4 = '.';
        }
        sb.append(c4);
        if (view.isVerticalScrollBarEnabled()) {
            c5 = 'V';
        } else {
            c5 = '.';
        }
        sb.append(c5);
        if (view.isClickable()) {
            c6 = 'C';
        } else {
            c6 = '.';
        }
        sb.append(c6);
        if (view.isLongClickable()) {
            c7 = 'L';
        } else {
            c7 = '.';
        }
        sb.append(c7);
        sb.append(' ');
        if (!view.isFocused()) {
            c9 = '.';
        }
        sb.append(c9);
        if (view.isSelected()) {
            c8 = 'S';
        } else {
            c8 = '.';
        }
        sb.append(c8);
        if (view.isPressed()) {
            c10 = 'P';
        }
        sb.append(c10);
        sb.append(' ');
        sb.append(view.getLeft());
        sb.append(',');
        sb.append(view.getTop());
        sb.append('-');
        sb.append(view.getRight());
        sb.append(',');
        sb.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            sb.append(" #");
            sb.append(Integer.toHexString(id));
            Resources resources = view.getResources();
            if (!(id == 0 || resources == null)) {
                switch (-16777216 & id) {
                    case 16777216:
                        str = "android";
                        String resourceTypeName = resources.getResourceTypeName(id);
                        String resourceEntryName = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName);
                        sb.append("/");
                        sb.append(resourceEntryName);
                        break;
                    case 2130706432:
                        str = "app";
                        String resourceTypeName2 = resources.getResourceTypeName(id);
                        String resourceEntryName2 = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName2);
                        sb.append("/");
                        sb.append(resourceEntryName2);
                        break;
                    default:
                        try {
                            str = resources.getResourcePackageName(id);
                            String resourceTypeName22 = resources.getResourceTypeName(id);
                            String resourceEntryName22 = resources.getResourceEntryName(id);
                            sb.append(" ");
                            sb.append(str);
                            sb.append(":");
                            sb.append(resourceTypeName22);
                            sb.append("/");
                            sb.append(resourceEntryName22);
                            break;
                        } catch (Resources.NotFoundException e2) {
                            break;
                        }
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void a(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        printWriter.println(a(view));
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String str2 = str + "  ";
            for (int i2 = 0; i2 < childCount; i2++) {
                a(str2, printWriter, viewGroup.getChildAt(i2));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (!this.g) {
            this.g = true;
            this.h = z;
            this.a.removeMessages(1);
            d();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.k) {
            this.k = false;
            if (this.m != null) {
                if (!this.h) {
                    this.m.c();
                } else {
                    this.m.d();
                }
            }
        }
        this.b.p();
    }

    public void a(Fragment fragment) {
    }

    public p e() {
        return this.b;
    }

    public void startActivityForResult(Intent intent, int i2) {
        if (i2 == -1 || (-65536 & i2) == 0) {
            super.startActivityForResult(intent, i2);
            return;
        }
        throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
    }

    public void a(Fragment fragment, Intent intent, int i2) {
        if (i2 == -1) {
            super.startActivityForResult(intent, -1);
        } else if ((-65536 & i2) != 0) {
            throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
        } else {
            super.startActivityForResult(intent, ((fragment.f + 1) << 16) + (65535 & i2));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        ad adVar;
        if (this.l != null && (adVar = (ad) this.l.get(str)) != null && !adVar.g) {
            adVar.h();
            this.l.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    public ad a(String str, boolean z, boolean z2) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        ad adVar = (ad) this.l.get(str);
        if (adVar != null) {
            adVar.a(this);
            return adVar;
        } else if (!z2) {
            return adVar;
        } else {
            ad adVar2 = new ad(str, this, z);
            this.l.put(str, adVar2);
            return adVar2;
        }
    }
}
