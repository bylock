package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public final class FragmentState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new w();
    final String a;
    final int b;
    final boolean c;
    final int d;
    final int e;
    final String f;
    final boolean g;
    final boolean h;
    final Bundle i;
    Bundle j;
    Fragment k;

    public FragmentState(Fragment fragment) {
        this.a = fragment.getClass().getName();
        this.b = fragment.f;
        this.c = fragment.o;
        this.d = fragment.w;
        this.e = fragment.x;
        this.f = fragment.y;
        this.g = fragment.B;
        this.h = fragment.A;
        this.i = fragment.h;
    }

    public FragmentState(Parcel parcel) {
        boolean z;
        boolean z2 = true;
        this.a = parcel.readString();
        this.b = parcel.readInt();
        this.c = parcel.readInt() != 0;
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readString();
        if (parcel.readInt() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.g = z;
        this.h = parcel.readInt() == 0 ? false : z2;
        this.i = parcel.readBundle();
        this.j = parcel.readBundle();
    }

    public Fragment a(j jVar, Fragment fragment) {
        if (this.k != null) {
            return this.k;
        }
        if (this.i != null) {
            this.i.setClassLoader(jVar.getClassLoader());
        }
        this.k = Fragment.a(jVar, this.a, this.i);
        if (this.j != null) {
            this.j.setClassLoader(jVar.getClassLoader());
            this.k.d = this.j;
        }
        this.k.a(this.b, fragment);
        this.k.o = this.c;
        this.k.q = true;
        this.k.w = this.d;
        this.k.x = this.e;
        this.k.y = this.f;
        this.k.B = this.g;
        this.k.A = this.h;
        this.k.s = jVar.b;
        if (r.a) {
            Log.v("FragmentManager", "Instantiated fragment " + this.k);
        }
        return this.k;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3;
        int i4 = 1;
        parcel.writeString(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c ? 1 : 0);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        if (this.g) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        parcel.writeInt(i3);
        if (!this.h) {
            i4 = 0;
        }
        parcel.writeInt(i4);
        parcel.writeBundle(this.i);
        parcel.writeBundle(this.j);
    }
}
