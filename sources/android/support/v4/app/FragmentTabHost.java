package android.support.v4.app;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

/* compiled from: MyApp */
public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    private final ArrayList a;
    private Context b;
    private p c;
    private int d;
    private TabHost.OnTabChangeListener e;
    private z f;
    private boolean g;

    /* access modifiers changed from: package-private */
    /* compiled from: MyApp */
    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new y();
        String a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readString();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.a);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.a + "}";
        }
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.e = onTabChangeListener;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        aa aaVar = null;
        for (int i = 0; i < this.a.size(); i++) {
            z zVar = (z) this.a.get(i);
            zVar.d = this.c.a(zVar.a);
            if (zVar.d != null && !zVar.d.e()) {
                if (zVar.a.equals(currentTabTag)) {
                    this.f = zVar;
                } else {
                    if (aaVar == null) {
                        aaVar = this.c.a();
                    }
                    aaVar.a(zVar.d);
                }
            }
        }
        this.g = true;
        aa a2 = a(currentTabTag, aaVar);
        if (a2 != null) {
            a2.a();
            this.c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = getCurrentTabTag();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.a);
    }

    public void onTabChanged(String str) {
        aa a2;
        if (this.g && (a2 = a(str, null)) != null) {
            a2.a();
        }
        if (this.e != null) {
            this.e.onTabChanged(str);
        }
    }

    private aa a(String str, aa aaVar) {
        z zVar = null;
        int i = 0;
        while (i < this.a.size()) {
            z zVar2 = (z) this.a.get(i);
            if (!zVar2.a.equals(str)) {
                zVar2 = zVar;
            }
            i++;
            zVar = zVar2;
        }
        if (zVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.f != zVar) {
            if (aaVar == null) {
                aaVar = this.c.a();
            }
            if (!(this.f == null || this.f.d == null)) {
                aaVar.a(this.f.d);
            }
            if (zVar != null) {
                if (zVar.d == null) {
                    zVar.d = Fragment.a(this.b, zVar.b.getName(), zVar.c);
                    aaVar.a(this.d, zVar.d, zVar.a);
                } else {
                    aaVar.b(zVar.d);
                }
            }
            this.f = zVar;
        }
        return aaVar;
    }
}
