package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.RemoteViews;
import java.util.ArrayList;

/* compiled from: MyApp */
public class ap {
    Context a;
    CharSequence b;
    CharSequence c;
    PendingIntent d;
    PendingIntent e;
    RemoteViews f;
    Bitmap g;
    CharSequence h;
    int i;
    int j;
    boolean k;
    aw l;
    CharSequence m;
    int n;
    int o;
    boolean p;
    ArrayList q = new ArrayList();
    Notification r = new Notification();

    public ap(Context context) {
        this.a = context;
        this.r.when = System.currentTimeMillis();
        this.r.audioStreamType = -1;
        this.j = 0;
    }

    public ap a(int i2) {
        this.r.icon = i2;
        return this;
    }

    public ap a(CharSequence charSequence) {
        this.b = charSequence;
        return this;
    }

    public ap b(CharSequence charSequence) {
        this.c = charSequence;
        return this;
    }

    public ap a(PendingIntent pendingIntent) {
        this.d = pendingIntent;
        return this;
    }

    public ap c(CharSequence charSequence) {
        this.r.tickerText = charSequence;
        return this;
    }

    public ap a(boolean z) {
        a(2, z);
        return this;
    }

    public ap b(boolean z) {
        a(8, z);
        return this;
    }

    public ap c(boolean z) {
        a(16, z);
        return this;
    }

    private void a(int i2, boolean z) {
        if (z) {
            this.r.flags |= i2;
            return;
        }
        this.r.flags &= i2 ^ -1;
    }

    public Notification a() {
        return al.a().a(this);
    }
}
