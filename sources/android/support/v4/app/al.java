package android.support.v4.app;

import android.os.Build;

/* compiled from: MyApp */
public class al {
    private static final ar a;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            a = new av();
        } else if (Build.VERSION.SDK_INT >= 14) {
            a = new au();
        } else if (Build.VERSION.SDK_INT >= 11) {
            a = new at();
        } else {
            a = new as();
        }
    }
}
