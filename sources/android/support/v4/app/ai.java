package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

/* compiled from: MyApp */
class ai extends ah {
    ai() {
    }

    @Override // android.support.v4.app.ah, android.support.v4.app.ag
    public Intent a(Activity activity) {
        Intent a = aj.a(activity);
        if (a == null) {
            return b(activity);
        }
        return a;
    }

    /* access modifiers changed from: package-private */
    public Intent b(Activity activity) {
        return super.a(activity);
    }

    @Override // android.support.v4.app.ah, android.support.v4.app.ag
    public boolean a(Activity activity, Intent intent) {
        return aj.a(activity, intent);
    }

    @Override // android.support.v4.app.ah, android.support.v4.app.ag
    public void b(Activity activity, Intent intent) {
        aj.b(activity, intent);
    }

    @Override // android.support.v4.app.ah, android.support.v4.app.ag
    public String a(Context context, ActivityInfo activityInfo) {
        String a = aj.a(activityInfo);
        if (a == null) {
            return super.a(context, activityInfo);
        }
        return a;
    }
}
