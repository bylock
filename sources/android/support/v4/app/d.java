package android.support.v4.app;

import android.support.v4.d.b;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public final class d extends aa implements Runnable {
    final r a;
    e b;
    e c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    boolean k;
    boolean l = true;
    String m;
    boolean n;
    int o = -1;
    int p;
    CharSequence q;
    int r;
    CharSequence s;

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.o >= 0) {
            sb.append(" #");
            sb.append(this.o);
        }
        if (this.m != null) {
            sb.append(" ");
            sb.append(this.m);
        }
        sb.append("}");
        return sb.toString();
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        a(str, printWriter, true);
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.m);
            printWriter.print(" mIndex=");
            printWriter.print(this.o);
            printWriter.print(" mCommitted=");
            printWriter.println(this.n);
            if (this.i != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.i));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.j));
            }
            if (!(this.e == 0 && this.f == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.e));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f));
            }
            if (!(this.g == 0 && this.h == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.g));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.h));
            }
            if (!(this.p == 0 && this.q == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.p));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.q);
            }
            if (!(this.r == 0 && this.s == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.r));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.s);
            }
        }
        if (this.b != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str3 = str + "    ";
            int i2 = 0;
            e eVar = this.b;
            while (eVar != null) {
                switch (eVar.c) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + eVar.c;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(eVar.d);
                if (z) {
                    if (!(eVar.e == 0 && eVar.f == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(eVar.e));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(eVar.f));
                    }
                    if (!(eVar.g == 0 && eVar.h == 0)) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(eVar.g));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(eVar.h));
                    }
                }
                if (eVar.i != null && eVar.i.size() > 0) {
                    for (int i3 = 0; i3 < eVar.i.size(); i3++) {
                        printWriter.print(str3);
                        if (eVar.i.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i3 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str3);
                            printWriter.print("  #");
                            printWriter.print(i3);
                            printWriter.print(": ");
                        }
                        printWriter.println(eVar.i.get(i3));
                    }
                }
                eVar = eVar.a;
                i2++;
            }
        }
    }

    public d(r rVar) {
        this.a = rVar;
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar) {
        if (this.b == null) {
            this.c = eVar;
            this.b = eVar;
        } else {
            eVar.b = this.c;
            this.c.a = eVar;
            this.c = eVar;
        }
        eVar.e = this.e;
        eVar.f = this.f;
        eVar.g = this.g;
        eVar.h = this.h;
        this.d++;
    }

    @Override // android.support.v4.app.aa
    public aa a(int i2, Fragment fragment, String str) {
        a(i2, fragment, str, 1);
        return this;
    }

    private void a(int i2, Fragment fragment, String str, int i3) {
        fragment.s = this.a;
        if (str != null) {
            if (fragment.y == null || str.equals(fragment.y)) {
                fragment.y = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.y + " now " + str);
            }
        }
        if (i2 != 0) {
            if (fragment.w == 0 || fragment.w == i2) {
                fragment.w = i2;
                fragment.x = i2;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.w + " now " + i2);
            }
        }
        e eVar = new e();
        eVar.c = i3;
        eVar.d = fragment;
        a(eVar);
    }

    @Override // android.support.v4.app.aa
    public aa a(Fragment fragment) {
        e eVar = new e();
        eVar.c = 6;
        eVar.d = fragment;
        a(eVar);
        return this;
    }

    @Override // android.support.v4.app.aa
    public aa b(Fragment fragment) {
        e eVar = new e();
        eVar.c = 7;
        eVar.d = fragment;
        a(eVar);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.k) {
            if (r.a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            for (e eVar = this.b; eVar != null; eVar = eVar.a) {
                if (eVar.d != null) {
                    eVar.d.r += i2;
                    if (r.a) {
                        Log.v("FragmentManager", "Bump nesting of " + eVar.d + " to " + eVar.d.r);
                    }
                }
                if (eVar.i != null) {
                    for (int size = eVar.i.size() - 1; size >= 0; size--) {
                        Fragment fragment = (Fragment) eVar.i.get(size);
                        fragment.r += i2;
                        if (r.a) {
                            Log.v("FragmentManager", "Bump nesting of " + fragment + " to " + fragment.r);
                        }
                    }
                }
            }
        }
    }

    @Override // android.support.v4.app.aa
    public int a() {
        return a(false);
    }

    @Override // android.support.v4.app.aa
    public int b() {
        return a(true);
    }

    /* access modifiers changed from: package-private */
    public int a(boolean z) {
        if (this.n) {
            throw new IllegalStateException("commit already called");
        }
        if (r.a) {
            Log.v("FragmentManager", "Commit: " + this);
            a("  ", (FileDescriptor) null, new PrintWriter(new b("FragmentManager")), (String[]) null);
        }
        this.n = true;
        if (this.k) {
            this.o = this.a.a(this);
        } else {
            this.o = -1;
        }
        this.a.a(this, z);
        return this.o;
    }

    public void run() {
        Fragment fragment;
        if (r.a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        if (!this.k || this.o >= 0) {
            a(1);
            for (e eVar = this.b; eVar != null; eVar = eVar.a) {
                switch (eVar.c) {
                    case 1:
                        Fragment fragment2 = eVar.d;
                        fragment2.G = eVar.e;
                        this.a.a(fragment2, false);
                        break;
                    case 2:
                        Fragment fragment3 = eVar.d;
                        if (this.a.g != null) {
                            fragment = fragment3;
                            for (int i2 = 0; i2 < this.a.g.size(); i2++) {
                                Fragment fragment4 = (Fragment) this.a.g.get(i2);
                                if (r.a) {
                                    Log.v("FragmentManager", "OP_REPLACE: adding=" + fragment + " old=" + fragment4);
                                }
                                if (fragment == null || fragment4.x == fragment.x) {
                                    if (fragment4 == fragment) {
                                        fragment = null;
                                        eVar.d = null;
                                    } else {
                                        if (eVar.i == null) {
                                            eVar.i = new ArrayList();
                                        }
                                        eVar.i.add(fragment4);
                                        fragment4.G = eVar.f;
                                        if (this.k) {
                                            fragment4.r++;
                                            if (r.a) {
                                                Log.v("FragmentManager", "Bump nesting of " + fragment4 + " to " + fragment4.r);
                                            }
                                        }
                                        this.a.a(fragment4, this.i, this.j);
                                    }
                                }
                            }
                        } else {
                            fragment = fragment3;
                        }
                        if (fragment != null) {
                            fragment.G = eVar.e;
                            this.a.a(fragment, false);
                            break;
                        } else {
                            break;
                        }
                    case 3:
                        Fragment fragment5 = eVar.d;
                        fragment5.G = eVar.f;
                        this.a.a(fragment5, this.i, this.j);
                        break;
                    case 4:
                        Fragment fragment6 = eVar.d;
                        fragment6.G = eVar.f;
                        this.a.b(fragment6, this.i, this.j);
                        break;
                    case 5:
                        Fragment fragment7 = eVar.d;
                        fragment7.G = eVar.e;
                        this.a.c(fragment7, this.i, this.j);
                        break;
                    case 6:
                        Fragment fragment8 = eVar.d;
                        fragment8.G = eVar.f;
                        this.a.d(fragment8, this.i, this.j);
                        break;
                    case 7:
                        Fragment fragment9 = eVar.d;
                        fragment9.G = eVar.e;
                        this.a.e(fragment9, this.i, this.j);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + eVar.c);
                }
            }
            this.a.a(this.a.n, this.i, this.j, true);
            if (this.k) {
                this.a.b(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    public void b(boolean z) {
        if (r.a) {
            Log.v("FragmentManager", "popFromBackStack: " + this);
            a("  ", (FileDescriptor) null, new PrintWriter(new b("FragmentManager")), (String[]) null);
        }
        a(-1);
        for (e eVar = this.c; eVar != null; eVar = eVar.b) {
            switch (eVar.c) {
                case 1:
                    Fragment fragment = eVar.d;
                    fragment.G = eVar.h;
                    this.a.a(fragment, r.c(this.i), this.j);
                    break;
                case 2:
                    Fragment fragment2 = eVar.d;
                    if (fragment2 != null) {
                        fragment2.G = eVar.h;
                        this.a.a(fragment2, r.c(this.i), this.j);
                    }
                    if (eVar.i != null) {
                        for (int i2 = 0; i2 < eVar.i.size(); i2++) {
                            Fragment fragment3 = (Fragment) eVar.i.get(i2);
                            fragment3.G = eVar.g;
                            this.a.a(fragment3, false);
                        }
                        break;
                    } else {
                        break;
                    }
                case 3:
                    Fragment fragment4 = eVar.d;
                    fragment4.G = eVar.g;
                    this.a.a(fragment4, false);
                    break;
                case 4:
                    Fragment fragment5 = eVar.d;
                    fragment5.G = eVar.g;
                    this.a.c(fragment5, r.c(this.i), this.j);
                    break;
                case 5:
                    Fragment fragment6 = eVar.d;
                    fragment6.G = eVar.h;
                    this.a.b(fragment6, r.c(this.i), this.j);
                    break;
                case 6:
                    Fragment fragment7 = eVar.d;
                    fragment7.G = eVar.g;
                    this.a.e(fragment7, r.c(this.i), this.j);
                    break;
                case 7:
                    Fragment fragment8 = eVar.d;
                    fragment8.G = eVar.g;
                    this.a.d(fragment8, r.c(this.i), this.j);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + eVar.c);
            }
        }
        if (z) {
            this.a.a(this.a.n, r.c(this.i), this.j, true);
        }
        if (this.o >= 0) {
            this.a.b(this.o);
            this.o = -1;
        }
    }

    public String c() {
        return this.m;
    }
}
