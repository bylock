package android.support.v4.b.a;

import android.support.v4.view.n;
import android.view.MenuItem;
import android.view.View;

/* compiled from: MyApp */
public interface b extends MenuItem {
    b a(n nVar);

    boolean expandActionView();

    View getActionView();

    boolean isActionViewExpanded();

    @Override // android.view.MenuItem
    MenuItem setActionView(int i);

    @Override // android.view.MenuItem
    MenuItem setActionView(View view);

    void setShowAsAction(int i);
}
