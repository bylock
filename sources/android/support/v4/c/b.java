package android.support.v4.c;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: MyApp */
class b implements Parcelable.Creator {
    final c a;

    public b(c cVar) {
        this.a = cVar;
    }

    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return this.a.a(parcel, null);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return this.a.a(i);
    }
}
