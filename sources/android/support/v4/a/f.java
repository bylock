package android.support.v4.a;

import android.content.ComponentName;
import android.content.Intent;

/* compiled from: MyApp */
class f implements e {
    f() {
    }

    @Override // android.support.v4.a.e
    public Intent a(ComponentName componentName) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setComponent(componentName);
        intent.addCategory("android.intent.category.LAUNCHER");
        return intent;
    }
}
