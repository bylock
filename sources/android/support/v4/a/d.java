package android.support.v4.a;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;

/* compiled from: MyApp */
public class d {
    private static final e a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 15) {
            a = new h();
        } else if (i >= 11) {
            a = new g();
        } else {
            a = new f();
        }
    }

    public static Intent a(ComponentName componentName) {
        return a.a(componentName);
    }
}
