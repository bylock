package android.support.v4.a;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

/* compiled from: MyApp */
public class a {
    public static boolean a(Context context, Intent[] intentArr, Bundle bundle) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 16) {
            c.a(context, intentArr, bundle);
            return true;
        } else if (i < 11) {
            return false;
        } else {
            b.a(context, intentArr);
            return true;
        }
    }
}
