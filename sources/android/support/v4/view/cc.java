package android.support.v4.view;

import android.view.View;
import java.util.Comparator;

/* compiled from: MyApp */
class cc implements Comparator {
    cc() {
    }

    /* renamed from: a */
    public int compare(View view, View view2) {
        bu buVar = (bu) view.getLayoutParams();
        bu buVar2 = (bu) view2.getLayoutParams();
        if (buVar.a != buVar2.a) {
            return buVar.a ? 1 : -1;
        }
        return buVar.e - buVar2.e;
    }
}
