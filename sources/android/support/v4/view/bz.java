package android.support.v4.view;

import android.database.DataSetObserver;

/* compiled from: MyApp */
class bz extends DataSetObserver {
    final /* synthetic */ ViewPager a;

    private bz(ViewPager viewPager) {
        this.a = viewPager;
    }

    /* synthetic */ bz(ViewPager viewPager, bp bpVar) {
        this(viewPager);
    }

    public void onChanged() {
        this.a.b();
    }

    public void onInvalidated() {
        this.a.b();
    }
}
