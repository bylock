package android.support.v4.view;

import android.view.KeyEvent;

/* compiled from: MyApp */
class w implements z {
    w() {
    }

    private static int a(int i, int i2, int i3, int i4, int i5) {
        boolean z = true;
        boolean z2 = (i2 & i3) != 0;
        int i6 = i4 | i5;
        if ((i2 & i6) == 0) {
            z = false;
        }
        if (z2) {
            if (!z) {
                return i & (i6 ^ -1);
            }
            throw new IllegalArgumentException("bad arguments");
        } else if (z) {
            return i & (i3 ^ -1);
        } else {
            return i;
        }
    }

    public int a(int i) {
        int i2;
        if ((i & 192) != 0) {
            i2 = i | 1;
        } else {
            i2 = i;
        }
        if ((i2 & 48) != 0) {
            i2 |= 2;
        }
        return i2 & 247;
    }

    @Override // android.support.v4.view.z
    public boolean a(int i, int i2) {
        if (a(a(a(i) & 247, i2, 1, 64, 128), i2, 2, 16, 32) == i2) {
            return true;
        }
        return false;
    }

    @Override // android.support.v4.view.z
    public boolean b(int i) {
        return (a(i) & 247) == 0;
    }

    @Override // android.support.v4.view.z
    public void a(KeyEvent keyEvent) {
    }
}
