package android.support.v4.view;

import android.view.MenuItem;

/* compiled from: MyApp */
class aj {
    public static boolean a(MenuItem menuItem) {
        return menuItem.expandActionView();
    }

    public static boolean b(MenuItem menuItem) {
        return menuItem.isActionViewExpanded();
    }
}
