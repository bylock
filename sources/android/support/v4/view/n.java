package android.support.v4.view;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* compiled from: MyApp */
public abstract class n {
    private final Context a;
    private o b;
    private p c;

    public abstract View b();

    public Context a() {
        return this.a;
    }

    public View a(MenuItem menuItem) {
        return b();
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        return true;
    }

    public void e() {
        if (this.c != null && c()) {
            this.c.a(d());
        }
    }

    public boolean f() {
        return false;
    }

    public boolean g() {
        return false;
    }

    public void a(SubMenu subMenu) {
    }

    public void a(o oVar) {
        this.b = oVar;
    }

    public void a(p pVar) {
        if (!(this.c == null || pVar == null)) {
            Log.w("ActionProvider(support)", "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.c = pVar;
    }
}
