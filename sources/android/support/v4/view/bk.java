package android.support.v4.view;

import android.os.Build;
import android.view.ViewConfiguration;

/* compiled from: MyApp */
public class bk {
    static final bn a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            a = new bm();
        } else {
            a = new bl();
        }
    }

    public static int a(ViewConfiguration viewConfiguration) {
        return a.a(viewConfiguration);
    }
}
