package android.support.v4.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.c.a;
import android.support.v4.widget.k;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* compiled from: MyApp */
public class ViewPager extends ViewGroup {
    private static final int[] a = {16842931};
    private static final cc af = new cc();
    private static final Comparator c = new bp();
    private static final Interpolator d = new bq();
    private boolean A;
    private boolean B;
    private int C;
    private int D;
    private int E;
    private float F;
    private float G;
    private float H;
    private float I;
    private int J = -1;
    private VelocityTracker K;
    private int L;
    private int M;
    private int N;
    private int O;
    private boolean P;
    private k Q;
    private k R;
    private boolean S = true;
    private boolean T = false;
    private boolean U;
    private int V;
    private bx W;
    private bx Z;
    private bw aa;
    private by ab;
    private Method ac;
    private int ad;
    private ArrayList ae;
    private final Runnable ag = new br(this);
    private int ah = 0;
    private int b;
    private final ArrayList e = new ArrayList();
    private final bt f = new bt();
    private final Rect g = new Rect();
    private ap h;
    private int i;
    private int j = -1;
    private Parcelable k = null;
    private ClassLoader l = null;
    private Scroller m;
    private bz n;
    private int o;
    private Drawable p;
    private int q;
    private int r;
    private float s = -3.4028235E38f;
    private float t = Float.MAX_VALUE;
    private int u;
    private int v;
    private boolean w;
    private boolean x;
    private boolean y;
    private int z = 1;

    public ViewPager(Context context) {
        super(context);
        a();
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.m = new Scroller(context, d);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.E = bk.a(viewConfiguration);
        this.L = (int) (400.0f * f2);
        this.M = viewConfiguration.getScaledMaximumFlingVelocity();
        this.Q = new k(context);
        this.R = new k(context);
        this.N = (int) (25.0f * f2);
        this.O = (int) (2.0f * f2);
        this.C = (int) (16.0f * f2);
        av.a(this, new bv(this));
        if (av.c(this) == 0) {
            av.b(this, 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.ag);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void setScrollState(int i2) {
        if (this.ah != i2) {
            this.ah = i2;
            if (this.ab != null) {
                b(i2 != 0);
            }
            if (this.W != null) {
                this.W.b(i2);
            }
        }
    }

    public void setAdapter(ap apVar) {
        if (this.h != null) {
            this.h.b(this.n);
            this.h.a((ViewGroup) this);
            for (int i2 = 0; i2 < this.e.size(); i2++) {
                bt btVar = (bt) this.e.get(i2);
                this.h.a((ViewGroup) this, btVar.b, btVar.a);
            }
            this.h.b((ViewGroup) this);
            this.e.clear();
            g();
            this.i = 0;
            scrollTo(0, 0);
        }
        ap apVar2 = this.h;
        this.h = apVar;
        this.b = 0;
        if (this.h != null) {
            if (this.n == null) {
                this.n = new bz(this, null);
            }
            this.h.a((DataSetObserver) this.n);
            this.y = false;
            boolean z2 = this.S;
            this.S = true;
            this.b = this.h.b();
            if (this.j >= 0) {
                this.h.a(this.k, this.l);
                a(this.j, false, true);
                this.j = -1;
                this.k = null;
                this.l = null;
            } else if (!z2) {
                c();
            } else {
                requestLayout();
            }
        }
        if (!(this.aa == null || apVar2 == apVar)) {
            this.aa.a(apVar2, apVar);
        }
    }

    private void g() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < getChildCount()) {
                if (!((bu) getChildAt(i3).getLayoutParams()).a) {
                    removeViewAt(i3);
                    i3--;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public ap getAdapter() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void setOnAdapterChangeListener(bw bwVar) {
        this.aa = bwVar;
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    public void setCurrentItem(int i2) {
        boolean z2;
        this.y = false;
        if (!this.S) {
            z2 = true;
        } else {
            z2 = false;
        }
        a(i2, z2, false);
    }

    public void a(int i2, boolean z2) {
        this.y = false;
        a(i2, z2, false);
    }

    public int getCurrentItem() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3, int i3) {
        boolean z4 = false;
        if (this.h == null || this.h.b() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z3 || this.i != i2 || this.e.size() == 0) {
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.h.b()) {
                i2 = this.h.b() - 1;
            }
            int i4 = this.z;
            if (i2 > this.i + i4 || i2 < this.i - i4) {
                for (int i5 = 0; i5 < this.e.size(); i5++) {
                    ((bt) this.e.get(i5)).c = true;
                }
            }
            if (this.i != i2) {
                z4 = true;
            }
            if (this.S) {
                this.i = i2;
                if (z4 && this.W != null) {
                    this.W.a(i2);
                }
                if (z4 && this.Z != null) {
                    this.Z.a(i2);
                }
                requestLayout();
                return;
            }
            a(i2);
            a(i2, z2, i3, z4);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    private void a(int i2, boolean z2, int i3, boolean z3) {
        int i4;
        bt b2 = b(i2);
        if (b2 != null) {
            i4 = (int) (Math.max(this.s, Math.min(b2.e, this.t)) * ((float) getClientWidth()));
        } else {
            i4 = 0;
        }
        if (z2) {
            a(i4, 0, i3);
            if (z3 && this.W != null) {
                this.W.a(i2);
            }
            if (z3 && this.Z != null) {
                this.Z.a(i2);
                return;
            }
            return;
        }
        if (z3 && this.W != null) {
            this.W.a(i2);
        }
        if (z3 && this.Z != null) {
            this.Z.a(i2);
        }
        a(false);
        scrollTo(i4, 0);
        d(i4);
    }

    public void setOnPageChangeListener(bx bxVar) {
        this.W = bxVar;
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z2) {
        if (Build.VERSION.SDK_INT >= 7) {
            if (this.ac == null) {
                try {
                    this.ac = ViewGroup.class.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
                } catch (NoSuchMethodException e2) {
                    Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", e2);
                }
            }
            try {
                this.ac.invoke(this, Boolean.valueOf(z2));
            } catch (Exception e3) {
                Log.e("ViewPager", "Error changing children drawing order", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.ad == 2) {
            i3 = (i2 - 1) - i3;
        }
        return ((bu) ((View) this.ae.get(i3)).getLayoutParams()).f;
    }

    public int getOffscreenPageLimit() {
        return this.z;
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i2 + " too small; defaulting to " + 1);
            i2 = 1;
        }
        if (i2 != this.z) {
            this.z = i2;
            c();
        }
    }

    public void setPageMargin(int i2) {
        int i3 = this.o;
        this.o = i2;
        int width = getWidth();
        a(width, width, i2, i3);
        requestLayout();
    }

    public int getPageMargin() {
        return this.o;
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.p = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(getContext().getResources().getDrawable(i2));
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.p;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.p;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public float a(float f2) {
        return (float) Math.sin((double) ((float) (((double) (f2 - 0.5f)) * 0.4712389167638204d)));
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4) {
        int abs;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i5 = i2 - scrollX;
        int i6 = i3 - scrollY;
        if (i5 == 0 && i6 == 0) {
            a(false);
            c();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i7 = clientWidth / 2;
        float a2 = (((float) i7) * a(Math.min(1.0f, (((float) Math.abs(i5)) * 1.0f) / ((float) clientWidth)))) + ((float) i7);
        int abs2 = Math.abs(i4);
        if (abs2 > 0) {
            abs = Math.round(1000.0f * Math.abs(a2 / ((float) abs2))) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i5)) / ((((float) clientWidth) * this.h.c(this.i)) + ((float) this.o))) + 1.0f) * 100.0f);
        }
        this.m.startScroll(scrollX, scrollY, i5, i6, Math.min(abs, 600));
        av.b(this);
    }

    /* access modifiers changed from: package-private */
    public bt a(int i2, int i3) {
        bt btVar = new bt();
        btVar.b = i2;
        btVar.a = this.h.a((ViewGroup) this, i2);
        btVar.d = this.h.c(i2);
        if (i3 < 0 || i3 >= this.e.size()) {
            this.e.add(btVar);
        } else {
            this.e.add(i3, btVar);
        }
        return btVar;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        int i2;
        boolean z2;
        int i3;
        boolean z3;
        int b2 = this.h.b();
        this.b = b2;
        boolean z4 = this.e.size() < (this.z * 2) + 1 && this.e.size() < b2;
        boolean z5 = false;
        int i4 = this.i;
        boolean z6 = z4;
        int i5 = 0;
        while (i5 < this.e.size()) {
            bt btVar = (bt) this.e.get(i5);
            int a2 = this.h.a(btVar.a);
            if (a2 == -1) {
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = z6;
            } else if (a2 == -2) {
                this.e.remove(i5);
                int i6 = i5 - 1;
                if (!z5) {
                    this.h.a((ViewGroup) this);
                    z5 = true;
                }
                this.h.a((ViewGroup) this, btVar.b, btVar.a);
                if (this.i == btVar.b) {
                    i2 = i6;
                    z2 = z5;
                    i3 = Math.max(0, Math.min(this.i, b2 - 1));
                    z3 = true;
                } else {
                    i2 = i6;
                    z2 = z5;
                    i3 = i4;
                    z3 = true;
                }
            } else if (btVar.b != a2) {
                if (btVar.b == this.i) {
                    i4 = a2;
                }
                btVar.b = a2;
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = true;
            } else {
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = z6;
            }
            z6 = z3;
            i4 = i3;
            z5 = z2;
            i5 = i2 + 1;
        }
        if (z5) {
            this.h.b((ViewGroup) this);
        }
        Collections.sort(this.e, c);
        if (z6) {
            int childCount = getChildCount();
            for (int i7 = 0; i7 < childCount; i7++) {
                bu buVar = (bu) getChildAt(i7).getLayoutParams();
                if (!buVar.a) {
                    buVar.c = 0.0f;
                }
            }
            a(i4, false, true);
            requestLayout();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a(this.i);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ff, code lost:
        if (r2.b == r18.i) goto L_0x0101;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r19) {
        /*
        // Method dump skipped, instructions count: 835
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.a(int):void");
    }

    private void h() {
        if (this.ad != 0) {
            if (this.ae == null) {
                this.ae = new ArrayList();
            } else {
                this.ae.clear();
            }
            int childCount = getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                this.ae.add(getChildAt(i2));
            }
            Collections.sort(this.ae, af);
        }
    }

    private void a(bt btVar, int i2, bt btVar2) {
        bt btVar3;
        bt btVar4;
        int b2 = this.h.b();
        int clientWidth = getClientWidth();
        float f2 = clientWidth > 0 ? ((float) this.o) / ((float) clientWidth) : 0.0f;
        if (btVar2 != null) {
            int i3 = btVar2.b;
            if (i3 < btVar.b) {
                float f3 = btVar2.e + btVar2.d + f2;
                int i4 = i3 + 1;
                int i5 = 0;
                while (i4 <= btVar.b && i5 < this.e.size()) {
                    Object obj = this.e.get(i5);
                    while (true) {
                        btVar4 = (bt) obj;
                        if (i4 > btVar4.b && i5 < this.e.size() - 1) {
                            i5++;
                            obj = this.e.get(i5);
                        }
                    }
                    while (i4 < btVar4.b) {
                        f3 += this.h.c(i4) + f2;
                        i4++;
                    }
                    btVar4.e = f3;
                    f3 += btVar4.d + f2;
                    i4++;
                }
            } else if (i3 > btVar.b) {
                int size = this.e.size() - 1;
                float f4 = btVar2.e;
                int i6 = i3 - 1;
                while (i6 >= btVar.b && size >= 0) {
                    Object obj2 = this.e.get(size);
                    while (true) {
                        btVar3 = (bt) obj2;
                        if (i6 < btVar3.b && size > 0) {
                            size--;
                            obj2 = this.e.get(size);
                        }
                    }
                    while (i6 > btVar3.b) {
                        f4 -= this.h.c(i6) + f2;
                        i6--;
                    }
                    f4 -= btVar3.d + f2;
                    btVar3.e = f4;
                    i6--;
                }
            }
        }
        int size2 = this.e.size();
        float f5 = btVar.e;
        int i7 = btVar.b - 1;
        this.s = btVar.b == 0 ? btVar.e : -3.4028235E38f;
        this.t = btVar.b == b2 + -1 ? (btVar.e + btVar.d) - 1.0f : Float.MAX_VALUE;
        for (int i8 = i2 - 1; i8 >= 0; i8--) {
            bt btVar5 = (bt) this.e.get(i8);
            float f6 = f5;
            while (i7 > btVar5.b) {
                f6 -= this.h.c(i7) + f2;
                i7--;
            }
            f5 = f6 - (btVar5.d + f2);
            btVar5.e = f5;
            if (btVar5.b == 0) {
                this.s = f5;
            }
            i7--;
        }
        float f7 = btVar.e + btVar.d + f2;
        int i9 = btVar.b + 1;
        for (int i10 = i2 + 1; i10 < size2; i10++) {
            bt btVar6 = (bt) this.e.get(i10);
            float f8 = f7;
            while (i9 < btVar6.b) {
                f8 = this.h.c(i9) + f2 + f8;
                i9++;
            }
            if (btVar6.b == b2 - 1) {
                this.t = (btVar6.d + f8) - 1.0f;
            }
            btVar6.e = f8;
            f7 = f8 + btVar6.d + f2;
            i9++;
        }
        this.T = false;
    }

    /* compiled from: MyApp */
    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = a.a(new ca());
        int a;
        Parcelable b;
        ClassLoader c;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
            parcel.writeParcelable(this.b, i);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.a + "}";
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.a = parcel.readInt();
            this.b = parcel.readParcelable(classLoader);
            this.c = classLoader;
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = this.i;
        if (this.h != null) {
            savedState.b = this.h.a();
        }
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.h != null) {
            this.h.a(savedState.b, savedState.c);
            a(savedState.a, false, true);
            return;
        }
        this.j = savedState.a;
        this.k = savedState.b;
        this.l = savedState.c;
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2;
        if (!checkLayoutParams(layoutParams)) {
            layoutParams2 = generateLayoutParams(layoutParams);
        } else {
            layoutParams2 = layoutParams;
        }
        bu buVar = (bu) layoutParams2;
        buVar.a |= view instanceof bs;
        if (!this.w) {
            super.addView(view, i2, layoutParams2);
        } else if (buVar == null || !buVar.a) {
            buVar.d = true;
            addViewInLayout(view, i2, layoutParams2);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    public void removeView(View view) {
        if (this.w) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* access modifiers changed from: package-private */
    public bt a(View view) {
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            bt btVar = (bt) this.e.get(i2);
            if (this.h.a(view, btVar.a)) {
                return btVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public bt b(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return a(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public bt b(int i2) {
        for (int i3 = 0; i3 < this.e.size(); i3++) {
            bt btVar = (bt) this.e.get(i3);
            if (btVar.b == i2) {
                return btVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.S = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
        // Method dump skipped, instructions count: 275
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            a(i2, i4, this.o, this.o);
        }
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.e.isEmpty()) {
            bt b2 = b(this.i);
            int min = (int) ((b2 != null ? Math.min(b2.e, this.t) : 0.0f) * ((float) ((i2 - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int paddingLeft = (int) (((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4)) * (((float) getScrollX()) / ((float) (((i3 - getPaddingLeft()) - getPaddingRight()) + i5))));
        scrollTo(paddingLeft, getScrollY());
        if (!this.m.isFinished()) {
            this.m.startScroll(paddingLeft, 0, (int) (b(this.i).e * ((float) i2)), 0, this.m.getDuration() - this.m.timePassed());
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        bt a2;
        int i6;
        int i7;
        int i8;
        int measuredHeight;
        int i9;
        int i10;
        int childCount = getChildCount();
        int i11 = i4 - i2;
        int i12 = i5 - i3;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i13 = 0;
        int i14 = 0;
        while (i14 < childCount) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                bu buVar = (bu) childAt.getLayoutParams();
                if (buVar.a) {
                    int i15 = buVar.b & 7;
                    int i16 = buVar.b & 112;
                    switch (i15) {
                        case 1:
                            i8 = Math.max((i11 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            i8 = paddingLeft;
                            break;
                        case 3:
                            i8 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i11 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i8 = measuredWidth;
                            break;
                    }
                    switch (i16) {
                        case 16:
                            measuredHeight = Math.max((i12 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            i9 = paddingTop;
                            i10 = paddingBottom;
                            break;
                        case 48:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            i10 = paddingBottom;
                            i9 = measuredHeight2;
                            measuredHeight = paddingTop;
                            break;
                        case 80:
                            measuredHeight = (i12 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i9 = paddingTop;
                            i10 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            i9 = paddingTop;
                            i10 = paddingBottom;
                            break;
                    }
                    int i17 = i8 + scrollX;
                    childAt.layout(i17, measuredHeight, childAt.getMeasuredWidth() + i17, childAt.getMeasuredHeight() + measuredHeight);
                    i6 = i13 + 1;
                    i7 = i9;
                    paddingBottom = i10;
                    i14++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i7;
                    i13 = i6;
                }
            }
            i6 = i13;
            i7 = paddingTop;
            i14++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i7;
            i13 = i6;
        }
        int i18 = (i11 - paddingLeft) - paddingRight;
        for (int i19 = 0; i19 < childCount; i19++) {
            View childAt2 = getChildAt(i19);
            if (childAt2.getVisibility() != 8) {
                bu buVar2 = (bu) childAt2.getLayoutParams();
                if (!buVar2.a && (a2 = a(childAt2)) != null) {
                    int i20 = ((int) (a2.e * ((float) i18))) + paddingLeft;
                    if (buVar2.d) {
                        buVar2.d = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (buVar2.c * ((float) i18)), 1073741824), View.MeasureSpec.makeMeasureSpec((i12 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i20, paddingTop, childAt2.getMeasuredWidth() + i20, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.q = paddingTop;
        this.r = i12 - paddingBottom;
        this.V = i13;
        if (this.S) {
            a(this.i, false, 0, false);
        }
        this.S = false;
    }

    public void computeScroll() {
        if (this.m.isFinished() || !this.m.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.m.getCurrX();
        int currY = this.m.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!d(currX)) {
                this.m.abortAnimation();
                scrollTo(0, currY);
            }
        }
        av.b(this);
    }

    private boolean d(int i2) {
        if (this.e.size() == 0) {
            this.U = false;
            a(0, 0.0f, 0);
            if (this.U) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        bt i3 = i();
        int clientWidth = getClientWidth();
        int i4 = this.o + clientWidth;
        float f2 = ((float) this.o) / ((float) clientWidth);
        int i5 = i3.b;
        float f3 = ((((float) i2) / ((float) clientWidth)) - i3.e) / (i3.d + f2);
        this.U = false;
        a(i5, f3, (int) (((float) i4) * f3));
        if (this.U) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    /* access modifiers changed from: protected */
    public void a(int i2, float f2, int i3) {
        int measuredWidth;
        int i4;
        int i5;
        if (this.V > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i6 = 0;
            while (i6 < childCount) {
                View childAt = getChildAt(i6);
                bu buVar = (bu) childAt.getLayoutParams();
                if (!buVar.a) {
                    i4 = paddingLeft;
                    i5 = paddingRight;
                } else {
                    switch (buVar.b & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            i4 = paddingLeft;
                            i5 = paddingRight;
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            i4 = paddingLeft;
                            i5 = paddingRight;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            i5 = paddingRight;
                            i4 = width2;
                            measuredWidth = paddingLeft;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i4 = paddingLeft;
                            i5 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i6++;
                paddingLeft = i4;
                paddingRight = i5;
            }
        }
        if (this.W != null) {
            this.W.a(i2, f2, i3);
        }
        if (this.Z != null) {
            this.Z.a(i2, f2, i3);
        }
        if (this.ab != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i7 = 0; i7 < childCount2; i7++) {
                View childAt2 = getChildAt(i7);
                if (!((bu) childAt2.getLayoutParams()).a) {
                    this.ab.a(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getClientWidth()));
                }
            }
        }
        this.U = true;
    }

    private void a(boolean z2) {
        boolean z3 = this.ah == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            this.m.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.m.getCurrX();
            int currY = this.m.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
            }
        }
        this.y = false;
        boolean z4 = z3;
        for (int i2 = 0; i2 < this.e.size(); i2++) {
            bt btVar = (bt) this.e.get(i2);
            if (btVar.c) {
                btVar.c = false;
                z4 = true;
            }
        }
        if (!z4) {
            return;
        }
        if (z2) {
            av.a(this, this.ag);
        } else {
            this.ag.run();
        }
    }

    private boolean a(float f2, float f3) {
        return (f2 < ((float) this.D) && f3 > 0.0f) || (f2 > ((float) (getWidth() - this.D)) && f3 < 0.0f);
    }

    private void b(boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            av.a(getChildAt(i2), z2 ? 2 : 0, null);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            this.A = false;
            this.B = false;
            this.J = -1;
            if (this.K == null) {
                return false;
            }
            this.K.recycle();
            this.K = null;
            return false;
        }
        if (action != 0) {
            if (this.A) {
                return true;
            }
            if (this.B) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x2 = motionEvent.getX();
                this.H = x2;
                this.F = x2;
                float y2 = motionEvent.getY();
                this.I = y2;
                this.G = y2;
                this.J = ak.b(motionEvent, 0);
                this.B = false;
                this.m.computeScrollOffset();
                if (this.ah == 2 && Math.abs(this.m.getFinalX() - this.m.getCurrX()) > this.O) {
                    this.m.abortAnimation();
                    this.y = false;
                    c();
                    this.A = true;
                    setScrollState(1);
                    break;
                } else {
                    a(false);
                    this.A = false;
                    break;
                }
                break;
            case 2:
                int i2 = this.J;
                if (i2 != -1) {
                    int a2 = ak.a(motionEvent, i2);
                    float c2 = ak.c(motionEvent, a2);
                    float f2 = c2 - this.F;
                    float abs = Math.abs(f2);
                    float d2 = ak.d(motionEvent, a2);
                    float abs2 = Math.abs(d2 - this.I);
                    if (f2 == 0.0f || a(this.F, f2) || !a(this, false, (int) f2, (int) c2, (int) d2)) {
                        if (abs > ((float) this.E) && 0.5f * abs > abs2) {
                            this.A = true;
                            setScrollState(1);
                            this.F = f2 > 0.0f ? this.H + ((float) this.E) : this.H - ((float) this.E);
                            this.G = d2;
                            setScrollingCacheEnabled(true);
                        } else if (abs2 > ((float) this.E)) {
                            this.B = true;
                        }
                        if (this.A && b(c2)) {
                            av.b(this);
                            break;
                        }
                    } else {
                        this.F = c2;
                        this.G = d2;
                        this.B = true;
                        return false;
                    }
                }
                break;
            case 6:
                a(motionEvent);
                break;
        }
        if (this.K == null) {
            this.K = VelocityTracker.obtain();
        }
        this.K.addMovement(motionEvent);
        return this.A;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f2;
        boolean z2 = false;
        if (this.P) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.h == null || this.h.b() == 0) {
            return false;
        }
        if (this.K == null) {
            this.K = VelocityTracker.obtain();
        }
        this.K.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.m.abortAnimation();
                this.y = false;
                c();
                this.A = true;
                setScrollState(1);
                float x2 = motionEvent.getX();
                this.H = x2;
                this.F = x2;
                float y2 = motionEvent.getY();
                this.I = y2;
                this.G = y2;
                this.J = ak.b(motionEvent, 0);
                break;
            case 1:
                if (this.A) {
                    VelocityTracker velocityTracker = this.K;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.M);
                    int a2 = (int) aq.a(velocityTracker, this.J);
                    this.y = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    bt i2 = i();
                    a(a(i2.b, ((((float) scrollX) / ((float) clientWidth)) - i2.e) / i2.d, a2, (int) (ak.c(motionEvent, ak.a(motionEvent, this.J)) - this.H)), true, true, a2);
                    this.J = -1;
                    j();
                    z2 = this.R.c() | this.Q.c();
                    break;
                }
                break;
            case 2:
                if (!this.A) {
                    int a3 = ak.a(motionEvent, this.J);
                    float c2 = ak.c(motionEvent, a3);
                    float abs = Math.abs(c2 - this.F);
                    float d2 = ak.d(motionEvent, a3);
                    float abs2 = Math.abs(d2 - this.G);
                    if (abs > ((float) this.E) && abs > abs2) {
                        this.A = true;
                        if (c2 - this.H > 0.0f) {
                            f2 = this.H + ((float) this.E);
                        } else {
                            f2 = this.H - ((float) this.E);
                        }
                        this.F = f2;
                        this.G = d2;
                        setScrollState(1);
                        setScrollingCacheEnabled(true);
                    }
                }
                if (this.A) {
                    z2 = false | b(ak.c(motionEvent, ak.a(motionEvent, this.J)));
                    break;
                }
                break;
            case 3:
                if (this.A) {
                    a(this.i, true, 0, false);
                    this.J = -1;
                    j();
                    z2 = this.R.c() | this.Q.c();
                    break;
                }
                break;
            case 5:
                int b2 = ak.b(motionEvent);
                this.F = ak.c(motionEvent, b2);
                this.J = ak.b(motionEvent, b2);
                break;
            case 6:
                a(motionEvent);
                this.F = ak.c(motionEvent, ak.a(motionEvent, this.J));
                break;
        }
        if (z2) {
            av.b(this);
        }
        return true;
    }

    private boolean b(float f2) {
        boolean z2;
        float f3;
        boolean z3 = true;
        boolean z4 = false;
        this.F = f2;
        float scrollX = ((float) getScrollX()) + (this.F - f2);
        int clientWidth = getClientWidth();
        float f4 = ((float) clientWidth) * this.s;
        float f5 = ((float) clientWidth) * this.t;
        bt btVar = (bt) this.e.get(0);
        bt btVar2 = (bt) this.e.get(this.e.size() - 1);
        if (btVar.b != 0) {
            f4 = btVar.e * ((float) clientWidth);
            z2 = false;
        } else {
            z2 = true;
        }
        if (btVar2.b != this.h.b() - 1) {
            f3 = btVar2.e * ((float) clientWidth);
            z3 = false;
        } else {
            f3 = f5;
        }
        if (scrollX < f4) {
            if (z2) {
                z4 = this.Q.a(Math.abs(f4 - scrollX) / ((float) clientWidth));
            }
        } else if (scrollX > f3) {
            if (z3) {
                z4 = this.R.a(Math.abs(scrollX - f3) / ((float) clientWidth));
            }
            f4 = f3;
        } else {
            f4 = scrollX;
        }
        this.F += f4 - ((float) ((int) f4));
        scrollTo((int) f4, getScrollY());
        d((int) f4);
        return z4;
    }

    private bt i() {
        float f2;
        int i2;
        bt btVar;
        int clientWidth = getClientWidth();
        float scrollX = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        if (clientWidth > 0) {
            f2 = ((float) this.o) / ((float) clientWidth);
        } else {
            f2 = 0.0f;
        }
        float f3 = 0.0f;
        float f4 = 0.0f;
        int i3 = -1;
        int i4 = 0;
        boolean z2 = true;
        bt btVar2 = null;
        while (i4 < this.e.size()) {
            bt btVar3 = (bt) this.e.get(i4);
            if (z2 || btVar3.b == i3 + 1) {
                i2 = i4;
                btVar = btVar3;
            } else {
                bt btVar4 = this.f;
                btVar4.e = f3 + f4 + f2;
                btVar4.b = i3 + 1;
                btVar4.d = this.h.c(btVar4.b);
                i2 = i4 - 1;
                btVar = btVar4;
            }
            float f5 = btVar.e;
            float f6 = btVar.d + f5 + f2;
            if (!z2 && scrollX < f5) {
                return btVar2;
            }
            if (scrollX < f6 || i2 == this.e.size() - 1) {
                return btVar;
            }
            f4 = f5;
            i3 = btVar.b;
            z2 = false;
            f3 = btVar.d;
            btVar2 = btVar;
            i4 = i2 + 1;
        }
        return btVar2;
    }

    private int a(int i2, float f2, int i3, int i4) {
        if (Math.abs(i4) <= this.N || Math.abs(i3) <= this.L) {
            i2 = (int) ((i2 >= this.i ? 0.4f : 0.6f) + ((float) i2) + f2);
        } else if (i3 <= 0) {
            i2++;
        }
        if (this.e.size() > 0) {
            return Math.max(((bt) this.e.get(0)).b, Math.min(i2, ((bt) this.e.get(this.e.size() - 1)).b));
        }
        return i2;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z2 = false;
        int a2 = av.a(this);
        if (a2 == 0 || (a2 == 1 && this.h != null && this.h.b() > 1)) {
            if (!this.Q.a()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.s * ((float) width));
                this.Q.a(height, width);
                z2 = false | this.Q.a(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.R.a()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.t + 1.0f)) * ((float) width2));
                this.R.a(height2, width2);
                z2 |= this.R.a(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.Q.b();
            this.R.b();
        }
        if (z2) {
            av.b(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        if (this.o > 0 && this.p != null && this.e.size() > 0 && this.h != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f3 = ((float) this.o) / ((float) width);
            bt btVar = (bt) this.e.get(0);
            float f4 = btVar.e;
            int size = this.e.size();
            int i2 = btVar.b;
            int i3 = ((bt) this.e.get(size - 1)).b;
            int i4 = 0;
            for (int i5 = i2; i5 < i3; i5++) {
                while (i5 > btVar.b && i4 < size) {
                    i4++;
                    btVar = (bt) this.e.get(i4);
                }
                if (i5 == btVar.b) {
                    f2 = (btVar.e + btVar.d) * ((float) width);
                    f4 = btVar.e + btVar.d + f3;
                } else {
                    float c2 = this.h.c(i5);
                    f2 = (f4 + c2) * ((float) width);
                    f4 += c2 + f3;
                }
                if (((float) this.o) + f2 > ((float) scrollX)) {
                    this.p.setBounds((int) f2, this.q, (int) (((float) this.o) + f2 + 0.5f), this.r);
                    this.p.draw(canvas);
                }
                if (f2 > ((float) (scrollX + width))) {
                    return;
                }
            }
        }
    }

    private void a(MotionEvent motionEvent) {
        int b2 = ak.b(motionEvent);
        if (ak.b(motionEvent, b2) == this.J) {
            int i2 = b2 == 0 ? 1 : 0;
            this.F = ak.c(motionEvent, i2);
            this.J = ak.b(motionEvent, i2);
            if (this.K != null) {
                this.K.clear();
            }
        }
    }

    private void j() {
        this.A = false;
        this.B = false;
        if (this.K != null) {
            this.K.recycle();
            this.K = null;
        }
    }

    private void setScrollingCacheEnabled(boolean z2) {
        if (this.x != z2) {
            this.x = z2;
        }
    }

    public boolean canScrollHorizontally(int i2) {
        boolean z2 = true;
        if (this.h == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i2 < 0) {
            if (scrollX <= ((int) (((float) clientWidth) * this.s))) {
                z2 = false;
            }
            return z2;
        } else if (i2 <= 0) {
            return false;
        } else {
            if (scrollX >= ((int) (((float) clientWidth) * this.t))) {
                z2 = false;
            }
            return z2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, boolean z2, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom() && a(childAt, true, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                    return true;
                }
            }
        }
        if (!z2 || !av.a(view, -i2)) {
            return false;
        }
        return true;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || a(keyEvent);
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return c(17);
            case 22:
                return c(66);
            case 61:
                if (Build.VERSION.SDK_INT < 11) {
                    return false;
                }
                if (v.a(keyEvent)) {
                    return c(2);
                }
                if (v.a(keyEvent, 1)) {
                    return c(1);
                }
                return false;
            default:
                return false;
        }
    }

    public boolean c(int i2) {
        View view;
        boolean z2;
        boolean z3;
        View findFocus = findFocus();
        if (findFocus == this) {
            view = null;
        } else {
            if (findFocus != null) {
                ViewParent parent = findFocus.getParent();
                while (true) {
                    if (!(parent instanceof ViewGroup)) {
                        z2 = false;
                        break;
                    } else if (parent == this) {
                        z2 = true;
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                if (!z2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(findFocus.getClass().getSimpleName());
                    for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                        sb.append(" => ").append(parent2.getClass().getSimpleName());
                    }
                    Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                    view = null;
                }
            }
            view = findFocus;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i2);
        if (findNextFocus == null || findNextFocus == view) {
            if (i2 == 17 || i2 == 1) {
                z3 = d();
            } else {
                if (i2 == 66 || i2 == 2) {
                    z3 = e();
                }
                z3 = false;
            }
        } else if (i2 == 17) {
            z3 = (view == null || a(this.g, findNextFocus).left < a(this.g, view).left) ? findNextFocus.requestFocus() : d();
        } else {
            if (i2 == 66) {
                z3 = (view == null || a(this.g, findNextFocus).left > a(this.g, view).left) ? findNextFocus.requestFocus() : e();
            }
            z3 = false;
        }
        if (z3) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i2));
        }
        return z3;
    }

    private Rect a(Rect rect, View view) {
        Rect rect2;
        if (rect == null) {
            rect2 = new Rect();
        } else {
            rect2 = rect;
        }
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        if (this.i <= 0) {
            return false;
        }
        a(this.i - 1, true);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        if (this.h == null || this.i >= this.h.b() - 1) {
            return false;
        }
        a(this.i + 1, true);
        return true;
    }

    @Override // android.view.View, android.view.ViewGroup
    public void addFocusables(ArrayList arrayList, int i2, int i3) {
        bt a2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.i) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void addTouchables(ArrayList arrayList) {
        bt a2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.i) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        bt a2;
        int i4 = -1;
        int childCount = getChildCount();
        if ((i2 & 2) != 0) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = childCount - 1;
            childCount = -1;
        }
        while (i3 != childCount) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.i && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i3 += i4;
        }
        return false;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        bt a2;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.i && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new bu();
    }

    /* access modifiers changed from: protected */
    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof bu) && super.checkLayoutParams(layoutParams);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new bu(getContext(), attributeSet);
    }
}
