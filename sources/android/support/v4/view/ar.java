package android.support.v4.view;

import android.view.VelocityTracker;

/* compiled from: MyApp */
class ar implements at {
    ar() {
    }

    @Override // android.support.v4.view.at
    public float a(VelocityTracker velocityTracker, int i) {
        return velocityTracker.getXVelocity();
    }

    @Override // android.support.v4.view.at
    public float b(VelocityTracker velocityTracker, int i) {
        return velocityTracker.getYVelocity();
    }
}
