package android.support.v4.view;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;

/* compiled from: MyApp */
class aw implements bd {
    aw() {
    }

    @Override // android.support.v4.view.bd
    public boolean a(View view, int i) {
        return false;
    }

    @Override // android.support.v4.view.bd
    public int a(View view) {
        return 2;
    }

    @Override // android.support.v4.view.bd
    public void a(View view, a aVar) {
    }

    @Override // android.support.v4.view.bd
    public void b(View view) {
        view.postInvalidateDelayed(a());
    }

    @Override // android.support.v4.view.bd
    public void a(View view, int i, int i2, int i3, int i4) {
        view.postInvalidateDelayed(a(), i, i2, i3, i4);
    }

    @Override // android.support.v4.view.bd
    public void a(View view, Runnable runnable) {
        view.postDelayed(runnable, a());
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return 10;
    }

    @Override // android.support.v4.view.bd
    public int c(View view) {
        return 0;
    }

    @Override // android.support.v4.view.bd
    public void b(View view, int i) {
    }

    @Override // android.support.v4.view.bd
    public void a(View view, int i, Paint paint) {
    }

    @Override // android.support.v4.view.bd
    public int d(View view) {
        return 0;
    }

    @Override // android.support.v4.view.bd
    public void a(View view, Paint paint) {
    }

    @Override // android.support.v4.view.bd
    public int e(View view) {
        return 0;
    }

    @Override // android.support.v4.view.bd
    public boolean f(View view) {
        Drawable background = view.getBackground();
        if (background == null || background.getOpacity() != -1) {
            return false;
        }
        return true;
    }
}
