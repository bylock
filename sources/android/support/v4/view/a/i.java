package android.support.v4.view.a;

import android.os.Build;
import android.os.Bundle;
import java.util.List;

/* compiled from: MyApp */
public class i {
    private static final j a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            a = new k();
        } else {
            a = new m();
        }
    }

    public i() {
        this.b = a.a(this);
    }

    public i(Object obj) {
        this.b = obj;
    }

    public Object a() {
        return this.b;
    }

    public a a(int i) {
        return null;
    }

    public boolean a(int i, int i2, Bundle bundle) {
        return false;
    }

    public List a(String str, int i) {
        return null;
    }
}
