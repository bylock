package android.support.v4.view.a;

import android.graphics.Rect;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public interface c {
    int a(Object obj);

    void a(Object obj, int i);

    void a(Object obj, Rect rect);

    void a(Object obj, CharSequence charSequence);

    void a(Object obj, boolean z);

    CharSequence b(Object obj);

    void b(Object obj, Rect rect);

    CharSequence c(Object obj);

    CharSequence d(Object obj);

    CharSequence e(Object obj);

    boolean f(Object obj);

    boolean g(Object obj);

    boolean h(Object obj);

    boolean i(Object obj);

    boolean j(Object obj);

    boolean k(Object obj);

    boolean l(Object obj);

    boolean m(Object obj);

    boolean n(Object obj);

    boolean o(Object obj);

    String p(Object obj);
}
