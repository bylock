package android.support.v4.view.a;

import android.os.Build;

/* compiled from: MyApp */
public class q {
    private static final t a;
    private final Object b;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            a = new u();
        } else if (Build.VERSION.SDK_INT >= 15) {
            a = new s();
        } else if (Build.VERSION.SDK_INT >= 14) {
            a = new r();
        } else {
            a = new v();
        }
    }

    public q(Object obj) {
        this.b = obj;
    }

    public static q a() {
        return new q(a.a());
    }

    public void a(boolean z) {
        a.a(this.b, z);
    }

    public void a(int i) {
        a.b(this.b, i);
    }

    public void b(int i) {
        a.a(this.b, i);
    }

    public void c(int i) {
        a.c(this.b, i);
    }

    public int hashCode() {
        if (this.b == null) {
            return 0;
        }
        return this.b.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        q qVar = (q) obj;
        return this.b == null ? qVar.b == null : this.b.equals(qVar.b);
    }
}
