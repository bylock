package android.support.v4.view.a;

import android.graphics.Rect;

/* compiled from: MyApp */
class b extends f {
    b() {
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public void a(Object obj, int i) {
        g.a(obj, i);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public int a(Object obj) {
        return g.a(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public void a(Object obj, Rect rect) {
        g.a(obj, rect);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public void b(Object obj, Rect rect) {
        g.b(obj, rect);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public CharSequence b(Object obj) {
        return g.b(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public CharSequence c(Object obj) {
        return g.c(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public CharSequence d(Object obj) {
        return g.d(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public CharSequence e(Object obj) {
        return g.e(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean f(Object obj) {
        return g.f(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean g(Object obj) {
        return g.g(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean h(Object obj) {
        return g.h(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean i(Object obj) {
        return g.i(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean j(Object obj) {
        return g.j(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean k(Object obj) {
        return g.k(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean l(Object obj) {
        return g.l(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean m(Object obj) {
        return g.m(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean n(Object obj) {
        return g.n(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public boolean o(Object obj) {
        return g.o(obj);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public void a(Object obj, CharSequence charSequence) {
        g.a(obj, charSequence);
    }

    @Override // android.support.v4.view.a.c, android.support.v4.view.a.f
    public void a(Object obj, boolean z) {
        g.a(obj, z);
    }
}
