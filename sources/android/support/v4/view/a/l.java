package android.support.v4.view.a;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

/* compiled from: MyApp */
class l implements p {
    final /* synthetic */ i a;
    final /* synthetic */ k b;

    l(k kVar, i iVar) {
        this.b = kVar;
        this.a = iVar;
    }

    @Override // android.support.v4.view.a.p
    public boolean a(int i, int i2, Bundle bundle) {
        return this.a.a(i, i2, bundle);
    }

    @Override // android.support.v4.view.a.p
    public List a(String str, int i) {
        List a2 = this.a.a(str, i);
        ArrayList arrayList = new ArrayList();
        int size = a2.size();
        for (int i2 = 0; i2 < size; i2++) {
            arrayList.add(((a) a2.get(i2)).a());
        }
        return arrayList;
    }

    @Override // android.support.v4.view.a.p
    public Object a(int i) {
        a a2 = this.a.a(i);
        if (a2 == null) {
            return null;
        }
        return a2.a();
    }
}
