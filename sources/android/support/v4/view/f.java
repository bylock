package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.i;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: MyApp */
class f implements m {
    final /* synthetic */ a a;
    final /* synthetic */ e b;

    f(e eVar, a aVar) {
        this.b = eVar;
        this.a = aVar;
    }

    @Override // android.support.v4.view.m
    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.a.b(view, accessibilityEvent);
    }

    @Override // android.support.v4.view.m
    public void b(View view, AccessibilityEvent accessibilityEvent) {
        this.a.d(view, accessibilityEvent);
    }

    @Override // android.support.v4.view.m
    public void a(View view, Object obj) {
        this.a.a(view, new a(obj));
    }

    @Override // android.support.v4.view.m
    public void c(View view, AccessibilityEvent accessibilityEvent) {
        this.a.c(view, accessibilityEvent);
    }

    @Override // android.support.v4.view.m
    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.a.a(viewGroup, view, accessibilityEvent);
    }

    @Override // android.support.v4.view.m
    public void a(View view, int i) {
        this.a.a(view, i);
    }

    @Override // android.support.v4.view.m
    public void d(View view, AccessibilityEvent accessibilityEvent) {
        this.a.a(view, accessibilityEvent);
    }

    @Override // android.support.v4.view.m
    public Object a(View view) {
        i a2 = this.a.a(view);
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    @Override // android.support.v4.view.m
    public boolean a(View view, int i, Bundle bundle) {
        return this.a.a(view, i, bundle);
    }
}
