package android.support.v4.view;

import android.support.v4.view.a.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: MyApp */
class b extends g {
    b() {
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public Object a() {
        return h.a();
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public Object a(a aVar) {
        return h.a(new c(this, aVar));
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public boolean a(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        return h.a(obj, view, accessibilityEvent);
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public void b(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        h.b(obj, view, accessibilityEvent);
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public void a(Object obj, View view, a aVar) {
        h.a(obj, view, aVar.a());
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public void c(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        h.c(obj, view, accessibilityEvent);
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public boolean a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return h.a(obj, viewGroup, view, accessibilityEvent);
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public void a(Object obj, View view, int i) {
        h.a(obj, view, i);
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public void d(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        h.d(obj, view, accessibilityEvent);
    }
}
