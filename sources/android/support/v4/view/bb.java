package android.support.v4.view;

import android.view.View;

/* compiled from: MyApp */
class bb extends ba {
    bb() {
    }

    @Override // android.support.v4.view.aw, android.support.v4.view.bd
    public void b(View view) {
        bi.a(view);
    }

    @Override // android.support.v4.view.aw, android.support.v4.view.bd
    public void a(View view, int i, int i2, int i3, int i4) {
        bi.a(view, i, i2, i3, i4);
    }

    @Override // android.support.v4.view.aw, android.support.v4.view.bd
    public void a(View view, Runnable runnable) {
        bi.a(view, runnable);
    }

    @Override // android.support.v4.view.aw, android.support.v4.view.bd
    public int c(View view) {
        return bi.b(view);
    }

    @Override // android.support.v4.view.aw, android.support.v4.view.bd
    public void b(View view, int i) {
        bi.a(view, i);
    }
}
