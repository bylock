package android.support.v4.view;

import android.os.Build;

/* compiled from: MyApp */
public class q {
    static final r a;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            a = new t();
        } else {
            a = new s();
        }
    }

    public static int a(int i, int i2) {
        return a.a(i, i2);
    }
}
