package android.support.v4.view;

import android.view.MotionEvent;

/* compiled from: MyApp */
class am implements an {
    am() {
    }

    @Override // android.support.v4.view.an
    public int a(MotionEvent motionEvent, int i) {
        return ao.a(motionEvent, i);
    }

    @Override // android.support.v4.view.an
    public int b(MotionEvent motionEvent, int i) {
        return ao.b(motionEvent, i);
    }

    @Override // android.support.v4.view.an
    public float c(MotionEvent motionEvent, int i) {
        return ao.c(motionEvent, i);
    }

    @Override // android.support.v4.view.an
    public float d(MotionEvent motionEvent, int i) {
        return ao.d(motionEvent, i);
    }

    @Override // android.support.v4.view.an
    public int a(MotionEvent motionEvent) {
        return ao.a(motionEvent);
    }
}
