package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.q;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: MyApp */
class bv extends a {
    final /* synthetic */ ViewPager b;

    bv(ViewPager viewPager) {
        this.b = viewPager;
    }

    @Override // android.support.v4.view.a
    public void d(View view, AccessibilityEvent accessibilityEvent) {
        super.d(view, accessibilityEvent);
        accessibilityEvent.setClassName(ViewPager.class.getName());
        q a = q.a();
        a.a(b());
        if (accessibilityEvent.getEventType() == 4096 && ViewPager.a(this.b) != null) {
            a.a(ViewPager.a(this.b).b());
            a.b(ViewPager.b(this.b));
            a.c(ViewPager.b(this.b));
        }
    }

    @Override // android.support.v4.view.a
    public void a(View view, a aVar) {
        super.a(view, aVar);
        aVar.a(ViewPager.class.getName());
        aVar.a(b());
        if (this.b.canScrollHorizontally(1)) {
            aVar.a(4096);
        }
        if (this.b.canScrollHorizontally(-1)) {
            aVar.a(8192);
        }
    }

    @Override // android.support.v4.view.a
    public boolean a(View view, int i, Bundle bundle) {
        if (super.a(view, i, bundle)) {
            return true;
        }
        switch (i) {
            case 4096:
                if (!this.b.canScrollHorizontally(1)) {
                    return false;
                }
                this.b.setCurrentItem(ViewPager.b(this.b) + 1);
                return true;
            case 8192:
                if (!this.b.canScrollHorizontally(-1)) {
                    return false;
                }
                this.b.setCurrentItem(ViewPager.b(this.b) - 1);
                return true;
            default:
                return false;
        }
    }

    private boolean b() {
        return ViewPager.a(this.b) != null && ViewPager.a(this.b).b() > 1;
    }
}
