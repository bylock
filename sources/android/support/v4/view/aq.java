package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

/* compiled from: MyApp */
public class aq {
    static final at a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            a = new as();
        } else {
            a = new ar();
        }
    }

    public static float a(VelocityTracker velocityTracker, int i) {
        return a.a(velocityTracker, i);
    }

    public static float b(VelocityTracker velocityTracker, int i) {
        return a.b(velocityTracker, i);
    }
}
