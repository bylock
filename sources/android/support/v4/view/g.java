package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.i;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class g implements d {
    g() {
    }

    @Override // android.support.v4.view.d
    public Object a() {
        return null;
    }

    @Override // android.support.v4.view.d
    public Object a(a aVar) {
        return null;
    }

    @Override // android.support.v4.view.d
    public boolean a(Object obj, View view, AccessibilityEvent accessibilityEvent) {
        return false;
    }

    @Override // android.support.v4.view.d
    public void b(Object obj, View view, AccessibilityEvent accessibilityEvent) {
    }

    @Override // android.support.v4.view.d
    public void a(Object obj, View view, a aVar) {
    }

    @Override // android.support.v4.view.d
    public void c(Object obj, View view, AccessibilityEvent accessibilityEvent) {
    }

    @Override // android.support.v4.view.d
    public boolean a(Object obj, ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return true;
    }

    @Override // android.support.v4.view.d
    public void a(Object obj, View view, int i) {
    }

    @Override // android.support.v4.view.d
    public void d(Object obj, View view, AccessibilityEvent accessibilityEvent) {
    }

    @Override // android.support.v4.view.d
    public i a(Object obj, View view) {
        return null;
    }

    @Override // android.support.v4.view.d
    public boolean a(Object obj, View view, int i, Bundle bundle) {
        return false;
    }
}
