package android.support.v4.view;

import android.view.MenuItem;
import android.view.View;

/* compiled from: MyApp */
class ae implements ag {
    ae() {
    }

    @Override // android.support.v4.view.ag
    public void a(MenuItem menuItem, int i) {
        ai.a(menuItem, i);
    }

    @Override // android.support.v4.view.ag
    public MenuItem a(MenuItem menuItem, View view) {
        return ai.a(menuItem, view);
    }

    @Override // android.support.v4.view.ag
    public MenuItem b(MenuItem menuItem, int i) {
        return ai.b(menuItem, i);
    }

    @Override // android.support.v4.view.ag
    public View a(MenuItem menuItem) {
        return ai.a(menuItem);
    }

    @Override // android.support.v4.view.ag
    public boolean b(MenuItem menuItem) {
        return false;
    }

    @Override // android.support.v4.view.ag
    public boolean c(MenuItem menuItem) {
        return false;
    }
}
