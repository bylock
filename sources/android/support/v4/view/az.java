package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

/* compiled from: MyApp */
class az extends ay {
    az() {
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v4.view.aw
    public long a() {
        return bg.a();
    }

    @Override // android.support.v4.view.aw, android.support.v4.view.bd
    public void a(View view, int i, Paint paint) {
        bg.a(view, i, paint);
    }

    @Override // android.support.v4.view.aw, android.support.v4.view.bd
    public int d(View view) {
        return bg.a(view);
    }

    @Override // android.support.v4.view.aw, android.support.v4.view.bd
    public void a(View view, Paint paint) {
        a(view, d(view), paint);
        view.invalidate();
    }
}
