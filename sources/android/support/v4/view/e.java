package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.i;
import android.view.View;

/* compiled from: MyApp */
class e extends b {
    e() {
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d, android.support.v4.view.b
    public Object a(a aVar) {
        return k.a(new f(this, aVar));
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public i a(Object obj, View view) {
        Object a = k.a(obj, view);
        if (a != null) {
            return new i(a);
        }
        return null;
    }

    @Override // android.support.v4.view.g, android.support.v4.view.d
    public boolean a(Object obj, View view, int i, Bundle bundle) {
        return k.a(obj, view, i, bundle);
    }
}
