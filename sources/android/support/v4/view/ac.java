package android.support.v4.view;

import android.os.Build;
import android.support.v4.b.a.b;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

/* compiled from: MyApp */
public class ac {
    static final ag a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 14) {
            a = new af();
        } else if (i >= 11) {
            a = new ae();
        } else {
            a = new ad();
        }
    }

    public static void a(MenuItem menuItem, int i) {
        if (menuItem instanceof b) {
            ((b) menuItem).setShowAsAction(i);
        } else {
            a.a(menuItem, i);
        }
    }

    public static MenuItem a(MenuItem menuItem, View view) {
        if (menuItem instanceof b) {
            return ((b) menuItem).setActionView(view);
        }
        return a.a(menuItem, view);
    }

    public static MenuItem b(MenuItem menuItem, int i) {
        if (menuItem instanceof b) {
            return ((b) menuItem).setActionView(i);
        }
        return a.b(menuItem, i);
    }

    public static View a(MenuItem menuItem) {
        if (menuItem instanceof b) {
            return ((b) menuItem).getActionView();
        }
        return a.a(menuItem);
    }

    public static MenuItem a(MenuItem menuItem, n nVar) {
        if (menuItem instanceof b) {
            return ((b) menuItem).a(nVar);
        }
        Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return menuItem;
    }

    public static boolean b(MenuItem menuItem) {
        if (menuItem instanceof b) {
            return ((b) menuItem).expandActionView();
        }
        return a.b(menuItem);
    }

    public static boolean c(MenuItem menuItem) {
        if (menuItem instanceof b) {
            return ((b) menuItem).isActionViewExpanded();
        }
        return a.c(menuItem);
    }
}
