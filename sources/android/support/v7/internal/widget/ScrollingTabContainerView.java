package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.v7.a.e;
import android.support.v7.b.c;
import android.support.v7.b.h;
import android.support.v7.internal.view.a;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/* compiled from: MyApp */
public class ScrollingTabContainerView extends HorizontalScrollView implements o {
    Runnable a;
    int b;
    int c;
    private aj d;
    private LinearLayout e;
    private ak f;
    private boolean g;
    private final LayoutInflater h;
    private int i;
    private int j;

    public void onMeasure(int i2, int i3) {
        boolean z = true;
        int mode = View.MeasureSpec.getMode(i2);
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.e.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.b = -1;
        } else {
            if (childCount > 2) {
                this.b = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.b = View.MeasureSpec.getSize(i2) / 2;
            }
            this.b = Math.min(this.b, this.c);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.i, 1073741824);
        if (z2 || !this.g) {
            z = false;
        }
        if (z) {
            this.e.measure(0, makeMeasureSpec);
            if (this.e.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                b();
            } else {
                c();
            }
        } else {
            c();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.j);
        }
    }

    private boolean a() {
        return this.f != null && this.f.getParent() == this;
    }

    public void setAllowCollapse(boolean z) {
        this.g = z;
    }

    private void b() {
        if (!a()) {
            if (this.f == null) {
                this.f = d();
            }
            removeView(this.e);
            addView(this.f, new ViewGroup.LayoutParams(-2, -1));
            if (this.f.e() == null) {
                this.f.a(new ai(this, null));
            }
            if (this.a != null) {
                removeCallbacks(this.a);
                this.a = null;
            }
            this.f.a(this.j);
        }
    }

    private boolean c() {
        if (a()) {
            removeView(this.f);
            addView(this.e, new ViewGroup.LayoutParams(-2, -1));
            setTabSelected(this.f.f());
        }
        return false;
    }

    public void setTabSelected(int i2) {
        this.j = i2;
        int childCount = this.e.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.e.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
        }
        if (this.f != null && i2 >= 0) {
            this.f.a(i2);
        }
    }

    public void setContentHeight(int i2) {
        this.i = i2;
        requestLayout();
    }

    private ak d() {
        ak akVar = new ak(getContext(), null, c.actionDropDownStyle);
        akVar.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        akVar.b((o) this);
        return akVar;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        a a2 = a.a(getContext());
        setContentHeight(a2.e());
        this.c = a2.g();
    }

    public void a(int i2) {
        View childAt = this.e.getChildAt(i2);
        if (this.a != null) {
            removeCallbacks(this.a);
        }
        this.a = new ah(this, childAt);
        post(this.a);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.a != null) {
            post(this.a);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.a != null) {
            removeCallbacks(this.a);
        }
    }

    /* access modifiers changed from: private */
    public TabView a(e eVar, boolean z) {
        TabView tabView = (TabView) this.h.inflate(h.abc_action_bar_tab, (ViewGroup) this.e, false);
        tabView.a(this, eVar, z);
        if (z) {
            tabView.setBackgroundDrawable(null);
            tabView.setLayoutParams(new AbsListView.LayoutParams(-1, this.i));
        } else {
            tabView.setFocusable(true);
            if (this.d == null) {
                this.d = new aj(this, null);
            }
            tabView.setOnClickListener(this.d);
        }
        return tabView;
    }

    @Override // android.support.v7.internal.widget.o
    public void a(l lVar, View view, int i2, long j2) {
        ((TabView) view).getTab().d();
    }

    /* compiled from: MyApp */
    public class TabView extends LinearLayout {
        private e a;
        private TextView b;
        private ImageView c;
        private View d;
        private ScrollingTabContainerView e;

        public TabView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        /* access modifiers changed from: package-private */
        public void a(ScrollingTabContainerView scrollingTabContainerView, e eVar, boolean z) {
            this.e = scrollingTabContainerView;
            this.a = eVar;
            if (z) {
                setGravity(19);
            }
            a();
        }

        public void a(e eVar) {
            this.a = eVar;
            a();
        }

        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            int i3 = this.e != null ? this.e.b : 0;
            if (i3 > 0 && getMeasuredWidth() > i3) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), i2);
            }
        }

        public void a() {
            e eVar = this.a;
            View c2 = eVar.c();
            if (c2 != null) {
                ViewParent parent = c2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(c2);
                    }
                    addView(c2);
                }
                this.d = c2;
                if (this.b != null) {
                    this.b.setVisibility(8);
                }
                if (this.c != null) {
                    this.c.setVisibility(8);
                    this.c.setImageDrawable(null);
                    return;
                }
                return;
            }
            if (this.d != null) {
                removeView(this.d);
                this.d = null;
            }
            Drawable a2 = eVar.a();
            CharSequence b2 = eVar.b();
            if (a2 != null) {
                if (this.c == null) {
                    ImageView imageView = new ImageView(getContext());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 16;
                    imageView.setLayoutParams(layoutParams);
                    addView(imageView, 0);
                    this.c = imageView;
                }
                this.c.setImageDrawable(a2);
                this.c.setVisibility(0);
            } else if (this.c != null) {
                this.c.setVisibility(8);
                this.c.setImageDrawable(null);
            }
            if (b2 != null) {
                if (this.b == null) {
                    s sVar = new s(getContext(), null, c.actionBarTabTextStyle);
                    sVar.setEllipsize(TextUtils.TruncateAt.END);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams2.gravity = 16;
                    sVar.setLayoutParams(layoutParams2);
                    addView(sVar);
                    this.b = sVar;
                }
                this.b.setText(b2);
                this.b.setVisibility(0);
            } else if (this.b != null) {
                this.b.setVisibility(8);
                this.b.setText((CharSequence) null);
            }
            if (this.c != null) {
                this.c.setContentDescription(eVar.e());
            }
        }

        public e getTab() {
            return this.a;
        }
    }
}
