package android.support.v7.internal.widget;

import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ap implements o {
    final /* synthetic */ ak a;
    final /* synthetic */ ao b;

    ap(ao aoVar, ak akVar) {
        this.b = aoVar;
        this.a = akVar;
    }

    @Override // android.support.v7.internal.widget.o
    public void a(l lVar, View view, int i, long j) {
        this.b.b.a(i);
        if (this.b.b.t != null) {
            this.b.b.a(view, i, this.b.d.getItemId(i));
        }
        this.b.d();
    }
}
