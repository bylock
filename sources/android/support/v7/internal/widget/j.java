package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.c.c;
import android.support.v7.internal.view.menu.af;
import android.support.v7.internal.view.menu.al;
import android.support.v7.internal.view.menu.q;
import android.support.v7.internal.view.menu.u;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class j implements af {
    q a;
    u b;
    final /* synthetic */ ActionBarView c;

    private j(ActionBarView actionBarView) {
        this.c = actionBarView;
    }

    /* synthetic */ j(ActionBarView actionBarView, g gVar) {
        this(actionBarView);
    }

    @Override // android.support.v7.internal.view.menu.af
    public void a(Context context, q qVar) {
        if (!(this.a == null || this.b == null)) {
            this.a.d(this.b);
        }
        this.a = qVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    @Override // android.support.v7.internal.view.menu.af
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(boolean r6) {
        /*
            r5 = this;
            r1 = 0
            android.support.v7.internal.view.menu.u r0 = r5.b
            if (r0 == 0) goto L_0x0028
            android.support.v7.internal.view.menu.q r0 = r5.a
            if (r0 == 0) goto L_0x002d
            android.support.v7.internal.view.menu.q r0 = r5.a
            int r3 = r0.size()
            r2 = r1
        L_0x0010:
            if (r2 >= r3) goto L_0x002d
            android.support.v7.internal.view.menu.q r0 = r5.a
            android.view.MenuItem r0 = r0.getItem(r2)
            android.support.v4.b.a.b r0 = (android.support.v4.b.a.b) r0
            android.support.v7.internal.view.menu.u r4 = r5.b
            if (r0 != r4) goto L_0x0029
            r0 = 1
        L_0x001f:
            if (r0 != 0) goto L_0x0028
            android.support.v7.internal.view.menu.q r0 = r5.a
            android.support.v7.internal.view.menu.u r1 = r5.b
            r5.b(r0, r1)
        L_0x0028:
            return
        L_0x0029:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0010
        L_0x002d:
            r0 = r1
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.j.c(boolean):void");
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean a(al alVar) {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public void a(q qVar, boolean z) {
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean g() {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean a(q qVar, u uVar) {
        this.c.g = uVar.getActionView();
        this.c.q.a(this.c.m.getConstantState().newDrawable(this.c.getResources()));
        this.b = uVar;
        if (this.c.g.getParent() != this.c) {
            this.c.addView(this.c.g);
        }
        if (this.c.q.getParent() != this.c) {
            this.c.addView(this.c.q);
        }
        this.c.p.setVisibility(8);
        if (this.c.r != null) {
            this.c.r.setVisibility(8);
        }
        if (this.c.x != null) {
            this.c.x.setVisibility(8);
        }
        if (this.c.v != null) {
            this.c.v.setVisibility(8);
        }
        if (this.c.y != null) {
            this.c.y.setVisibility(8);
        }
        this.c.requestLayout();
        uVar.e(true);
        if (this.c.g instanceof c) {
            ((c) this.c.g).a();
        }
        return true;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean b(q qVar, u uVar) {
        if (this.c.g instanceof c) {
            ((c) this.c.g).b();
        }
        this.c.removeView(this.c.g);
        this.c.removeView(this.c.q);
        this.c.g = null;
        if ((this.c.j & 2) != 0) {
            this.c.p.setVisibility(0);
        }
        if ((this.c.j & 8) != 0) {
            if (this.c.r == null) {
                this.c.n();
            } else {
                this.c.r.setVisibility(0);
            }
        }
        if (this.c.x != null && this.c.i == 2) {
            this.c.x.setVisibility(0);
        }
        if (this.c.v != null && this.c.i == 1) {
            this.c.v.setVisibility(0);
        }
        if (!(this.c.y == null || (this.c.j & 16) == 0)) {
            this.c.y.setVisibility(0);
        }
        this.c.q.a((Drawable) null);
        this.b = null;
        this.c.requestLayout();
        uVar.e(false);
        return true;
    }
}
