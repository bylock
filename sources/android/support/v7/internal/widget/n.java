package android.support.v7.internal.widget;

import android.database.DataSetObserver;
import android.os.Parcelable;

/* compiled from: MyApp */
class n extends DataSetObserver {
    final /* synthetic */ l a;
    private Parcelable b = null;

    n(l lVar) {
        this.a = lVar;
    }

    public void onChanged() {
        this.a.u = true;
        this.a.A = this.a.z;
        this.a.z = this.a.e().getCount();
        if (!this.a.e().hasStableIds() || this.b == null || this.a.A != 0 || this.a.z <= 0) {
            this.a.n();
        } else {
            this.a.onRestoreInstanceState(this.b);
            this.b = null;
        }
        this.a.i();
        this.a.requestLayout();
    }

    public void onInvalidated() {
        this.a.u = true;
        if (this.a.e().hasStableIds()) {
            this.b = this.a.onSaveInstanceState();
        }
        this.a.A = this.a.z;
        this.a.z = 0;
        this.a.x = -1;
        this.a.y = Long.MIN_VALUE;
        this.a.v = -1;
        this.a.w = Long.MIN_VALUE;
        this.a.p = false;
        this.a.i();
        this.a.requestLayout();
    }
}
