package android.support.v7.internal.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.util.Locale;

/* compiled from: MyApp */
public class u {
    private boolean A;
    int a = Integer.MAX_VALUE;
    private Context b;
    private PopupWindow c;
    private ListAdapter d;
    private x e;
    private int f = -2;
    private int g = -2;
    private int h;
    private int i;
    private boolean j;
    private boolean k = false;
    private boolean l = false;
    private View m;
    private int n = 0;
    private DataSetObserver o;
    private View p;
    private Drawable q;
    private AdapterView.OnItemClickListener r;
    private AdapterView.OnItemSelectedListener s;
    private final ac t = new ac(this, null);
    private final ab u = new ab(this, null);
    private final aa v = new aa(this, null);
    private final y w = new y(this, null);
    private Runnable x;
    private Handler y = new Handler();
    private Rect z = new Rect();

    public u(Context context, AttributeSet attributeSet, int i2) {
        this.b = context;
        this.c = new PopupWindow(context, attributeSet, i2);
        this.c.setInputMethodMode(1);
        Locale locale = this.b.getResources().getConfiguration().locale;
    }

    public void a(ListAdapter listAdapter) {
        if (this.o == null) {
            this.o = new z(this, null);
        } else if (this.d != null) {
            this.d.unregisterDataSetObserver(this.o);
        }
        this.d = listAdapter;
        if (this.d != null) {
            listAdapter.registerDataSetObserver(this.o);
        }
        if (this.e != null) {
            this.e.setAdapter(this.d);
        }
    }

    public void a(int i2) {
        this.n = i2;
    }

    public void a(boolean z2) {
        this.A = true;
        this.c.setFocusable(z2);
    }

    public Drawable a() {
        return this.c.getBackground();
    }

    public void a(Drawable drawable) {
        this.c.setBackgroundDrawable(drawable);
    }

    public View b() {
        return this.p;
    }

    public void a(View view) {
        this.p = view;
    }

    public void b(int i2) {
        this.h = i2;
    }

    public void c(int i2) {
        this.i = i2;
        this.j = true;
    }

    public void d(int i2) {
        this.g = i2;
    }

    public void e(int i2) {
        Drawable background = this.c.getBackground();
        if (background != null) {
            background.getPadding(this.z);
            this.g = this.z.left + this.z.right + i2;
            return;
        }
        d(i2);
    }

    public void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.r = onItemClickListener;
    }

    public void c() {
        int i2;
        int i3;
        int i4;
        boolean z2 = true;
        boolean z3 = false;
        int i5 = -1;
        int j2 = j();
        boolean g2 = g();
        if (this.c.isShowing()) {
            if (this.g == -1) {
                i4 = -1;
            } else if (this.g == -2) {
                i4 = b().getWidth();
            } else {
                i4 = this.g;
            }
            if (this.f == -1) {
                if (!g2) {
                    j2 = -1;
                }
                if (g2) {
                    PopupWindow popupWindow = this.c;
                    if (this.g != -1) {
                        i5 = 0;
                    }
                    popupWindow.setWindowLayoutMode(i5, 0);
                } else {
                    this.c.setWindowLayoutMode(this.g == -1 ? -1 : 0, -1);
                }
            } else if (this.f != -2) {
                j2 = this.f;
            }
            PopupWindow popupWindow2 = this.c;
            if (!this.l && !this.k) {
                z3 = true;
            }
            popupWindow2.setOutsideTouchable(z3);
            this.c.update(b(), this.h, this.i, i4, j2);
            return;
        }
        if (this.g == -1) {
            i2 = -1;
        } else if (this.g == -2) {
            this.c.setWidth(b().getWidth());
            i2 = 0;
        } else {
            this.c.setWidth(this.g);
            i2 = 0;
        }
        if (this.f == -1) {
            i3 = -1;
        } else if (this.f == -2) {
            this.c.setHeight(j2);
            i3 = 0;
        } else {
            this.c.setHeight(this.f);
            i3 = 0;
        }
        this.c.setWindowLayoutMode(i2, i3);
        PopupWindow popupWindow3 = this.c;
        if (this.l || this.k) {
            z2 = false;
        }
        popupWindow3.setOutsideTouchable(z2);
        this.c.setTouchInterceptor(this.u);
        this.c.showAsDropDown(b(), this.h, this.i);
        this.e.setSelection(-1);
        if (!this.A || this.e.isInTouchMode()) {
            e();
        }
        if (!this.A) {
            this.y.post(this.w);
        }
    }

    public void d() {
        this.c.dismiss();
        i();
        this.c.setContentView(null);
        this.e = null;
        this.y.removeCallbacks(this.t);
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.c.setOnDismissListener(onDismissListener);
    }

    private void i() {
        if (this.m != null) {
            ViewParent parent = this.m.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.m);
            }
        }
    }

    public void f(int i2) {
        this.c.setInputMethodMode(i2);
    }

    public void g(int i2) {
        x xVar = this.e;
        if (f() && xVar != null) {
            x.a(xVar, false);
            xVar.setSelection(i2);
            if (xVar.getChoiceMode() != 0) {
                xVar.setItemChecked(i2, true);
            }
        }
    }

    public void e() {
        x xVar = this.e;
        if (xVar != null) {
            x.a(xVar, true);
            xVar.requestLayout();
        }
    }

    public boolean f() {
        return this.c.isShowing();
    }

    public boolean g() {
        return this.c.getInputMethodMode() == 2;
    }

    public ListView h() {
        return this.e;
    }

    private int j() {
        int i2;
        int i3;
        int makeMeasureSpec;
        View view;
        int i4;
        boolean z2 = true;
        if (this.e == null) {
            Context context = this.b;
            this.x = new v(this);
            this.e = new x(context, !this.A);
            if (this.q != null) {
                this.e.setSelector(this.q);
            }
            this.e.setAdapter(this.d);
            this.e.setOnItemClickListener(this.r);
            this.e.setFocusable(true);
            this.e.setFocusableInTouchMode(true);
            this.e.setOnItemSelectedListener(new w(this));
            this.e.setOnScrollListener(this.v);
            if (this.s != null) {
                this.e.setOnItemSelectedListener(this.s);
            }
            View view2 = this.e;
            View view3 = this.m;
            if (view3 != null) {
                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(1);
                ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 0, 1.0f);
                switch (this.n) {
                    case 0:
                        linearLayout.addView(view3);
                        linearLayout.addView(view2, layoutParams);
                        break;
                    case 1:
                        linearLayout.addView(view2, layoutParams);
                        linearLayout.addView(view3);
                        break;
                    default:
                        Log.e("ListPopupWindow", "Invalid hint position " + this.n);
                        break;
                }
                view3.measure(View.MeasureSpec.makeMeasureSpec(this.g, Integer.MIN_VALUE), 0);
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) view3.getLayoutParams();
                i4 = layoutParams2.bottomMargin + view3.getMeasuredHeight() + layoutParams2.topMargin;
                view = linearLayout;
            } else {
                view = view2;
                i4 = 0;
            }
            this.c.setContentView(view);
            i2 = i4;
        } else {
            ViewGroup viewGroup = (ViewGroup) this.c.getContentView();
            View view4 = this.m;
            if (view4 != null) {
                LinearLayout.LayoutParams layoutParams3 = (LinearLayout.LayoutParams) view4.getLayoutParams();
                i2 = layoutParams3.bottomMargin + view4.getMeasuredHeight() + layoutParams3.topMargin;
            } else {
                i2 = 0;
            }
        }
        Drawable background = this.c.getBackground();
        if (background != null) {
            background.getPadding(this.z);
            int i5 = this.z.top + this.z.bottom;
            if (!this.j) {
                this.i = -this.z.top;
                i3 = i5;
            } else {
                i3 = i5;
            }
        } else {
            this.z.setEmpty();
            i3 = 0;
        }
        if (this.c.getInputMethodMode() != 2) {
            z2 = false;
        }
        int a2 = a(b(), this.i, z2);
        if (this.k || this.f == -1) {
            return a2 + i3;
        }
        switch (this.g) {
            case -2:
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.b.getResources().getDisplayMetrics().widthPixels - (this.z.left + this.z.right), Integer.MIN_VALUE);
                break;
            case -1:
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.b.getResources().getDisplayMetrics().widthPixels - (this.z.left + this.z.right), 1073741824);
                break;
            default:
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.g, 1073741824);
                break;
        }
        int a3 = this.e.a(makeMeasureSpec, 0, -1, a2 - i2, -1);
        if (a3 > 0) {
            i2 += i3;
        }
        return a3 + i2;
    }

    public int a(View view, int i2, boolean z2) {
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        int i3 = rect.bottom;
        if (z2) {
            i3 = view.getContext().getResources().getDisplayMetrics().heightPixels;
        }
        int max = Math.max((i3 - (iArr[1] + view.getHeight())) - i2, (iArr[1] - rect.top) + i2);
        if (this.c.getBackground() == null) {
            return max;
        }
        this.c.getBackground().getPadding(this.z);
        return max - (this.z.top + this.z.bottom);
    }
}
