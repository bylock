package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.SpinnerAdapter;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ao extends u implements aq {
    final /* synthetic */ ak b;
    private CharSequence c;
    private ListAdapter d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ao(ak akVar, Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = akVar;
        a(akVar);
        a(true);
        a(0);
        a(new p(akVar, new ap(this, akVar)));
    }

    @Override // android.support.v7.internal.widget.u, android.support.v7.internal.widget.aq
    public void a(ListAdapter listAdapter) {
        super.a(listAdapter);
        this.d = listAdapter;
    }

    @Override // android.support.v7.internal.widget.aq
    public void a(CharSequence charSequence) {
        this.c = charSequence;
    }

    @Override // android.support.v7.internal.widget.u, android.support.v7.internal.widget.aq
    public void c() {
        int paddingLeft = this.b.getPaddingLeft();
        if (this.b.E == -2) {
            int width = this.b.getWidth();
            e(Math.max(this.b.a((SpinnerAdapter) this.d, a()), (width - paddingLeft) - this.b.getPaddingRight()));
        } else if (this.b.E == -1) {
            e((this.b.getWidth() - paddingLeft) - this.b.getPaddingRight());
        } else {
            e(this.b.E);
        }
        Drawable a = a();
        int i = 0;
        if (a != null) {
            a.getPadding(this.b.I);
            i = -this.b.I.left;
        }
        b(i + paddingLeft);
        f(2);
        super.c();
        h().setChoiceMode(1);
        g(this.b.f());
    }
}
