package android.support.v7.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.internal.widget.AbsSpinnerICS;

/* compiled from: MyApp */
final class e implements Parcelable.Creator {
    e() {
    }

    /* renamed from: a */
    public AbsSpinnerICS.SavedState createFromParcel(Parcel parcel) {
        return new AbsSpinnerICS.SavedState(parcel);
    }

    /* renamed from: a */
    public AbsSpinnerICS.SavedState[] newArray(int i) {
        return new AbsSpinnerICS.SavedState[i];
    }
}
