package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.b.c;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class x extends ListView {
    private boolean a;
    private boolean b;

    public x(Context context, boolean z) {
        super(context, null, c.dropDownListViewStyle);
        this.b = z;
        setCacheColorHint(0);
    }

    public boolean isInTouchMode() {
        return (this.b && this.a) || super.isInTouchMode();
    }

    public boolean hasWindowFocus() {
        return this.b || super.hasWindowFocus();
    }

    public boolean isFocused() {
        return this.b || super.isFocused();
    }

    public boolean hasFocus() {
        return this.b || super.hasFocus();
    }

    public final int a(int i, int i2, int i3, int i4, int i5) {
        View view;
        int makeMeasureSpec;
        int i6;
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        getListPaddingLeft();
        getListPaddingRight();
        int dividerHeight = getDividerHeight();
        Drawable divider = getDivider();
        ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        int i7 = listPaddingBottom + listPaddingTop;
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        int i8 = 0;
        View view2 = null;
        int i9 = 0;
        int count = adapter.getCount();
        int i10 = 0;
        while (i10 < count) {
            int itemViewType = adapter.getItemViewType(i10);
            if (itemViewType != i9) {
                view = null;
                i9 = itemViewType;
            } else {
                view = view2;
            }
            view2 = adapter.getView(i10, view, this);
            ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
            if (layoutParams == null || layoutParams.height <= 0) {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            } else {
                makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
            }
            view2.measure(i, makeMeasureSpec);
            if (i10 > 0) {
                i6 = i7 + dividerHeight;
            } else {
                i6 = i7;
            }
            int measuredHeight = i6 + view2.getMeasuredHeight();
            if (measuredHeight >= i4) {
                return (i5 < 0 || i10 <= i5 || i8 <= 0 || measuredHeight == i4) ? i4 : i8;
            }
            if (i5 >= 0 && i10 >= i5) {
                i8 = measuredHeight;
            }
            i10++;
            i7 = measuredHeight;
        }
        return i7;
    }
}
