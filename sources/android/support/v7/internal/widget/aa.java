package android.support.v7.internal.widget;

import android.widget.AbsListView;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class aa implements AbsListView.OnScrollListener {
    final /* synthetic */ u a;

    private aa(u uVar) {
        this.a = uVar;
    }

    /* synthetic */ aa(u uVar, v vVar) {
        this(uVar);
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 1 && !this.a.g() && this.a.c.getContentView() != null) {
            this.a.y.removeCallbacks(this.a.t);
            this.a.t.run();
        }
    }
}
