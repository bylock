package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.b.b;
import android.support.v7.b.c;
import android.support.v7.b.d;
import android.support.v7.b.k;
import android.support.v7.internal.view.menu.ActionMenuPresenter;
import android.support.v7.internal.view.menu.ActionMenuView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public abstract class a extends ViewGroup {
    protected ActionMenuView a;
    protected ActionMenuPresenter b;
    protected ActionBarContainer c;
    protected boolean d;
    protected boolean e;
    protected int f;

    a(Context context) {
        super(context);
    }

    a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    a(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, k.ActionBar, c.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(1, 0));
        obtainStyledAttributes.recycle();
        if (this.e) {
            setSplitActionBar(getContext().getResources().getBoolean(d.abc_split_action_bar_is_narrow));
        }
        if (this.b != null) {
            this.b.a(configuration);
        }
    }

    public void setSplitActionBar(boolean z) {
        this.d = z;
    }

    public void setSplitWhenNarrow(boolean z) {
        this.e = z;
    }

    public void setContentHeight(int i) {
        this.f = i;
        requestLayout();
    }

    public int getContentHeight() {
        return this.f;
    }

    public void setSplitView(ActionBarContainer actionBarContainer) {
        this.c = actionBarContainer;
    }

    public int getAnimatedVisibility() {
        return getVisibility();
    }

    public void a(int i) {
        clearAnimation();
        if (i != getVisibility()) {
            Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), i == 0 ? b.abc_fade_in : b.abc_fade_out);
            startAnimation(loadAnimation);
            setVisibility(i);
            if (this.c != null && this.a != null) {
                this.a.startAnimation(loadAnimation);
                this.a.setVisibility(i);
            }
        }
    }

    public void setVisibility(int i) {
        if (i != getVisibility()) {
            super.setVisibility(i);
        }
    }

    public boolean a() {
        if (this.b != null) {
            return this.b.a();
        }
        return false;
    }

    public void b() {
        post(new b(this));
    }

    public boolean c() {
        if (this.b != null) {
            return this.b.b();
        }
        return false;
    }

    public boolean d() {
        if (this.b != null) {
            return this.b.e();
        }
        return false;
    }

    public boolean e() {
        return this.b != null && this.b.f();
    }

    public void f() {
        if (this.b != null) {
            this.b.c();
        }
    }

    /* access modifiers changed from: protected */
    public int a(View view, int i, int i2, int i3) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i, Integer.MIN_VALUE), i2);
        return Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    /* access modifiers changed from: protected */
    public int b(View view, int i, int i2, int i3) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = ((i3 - measuredHeight) / 2) + i2;
        view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        return measuredWidth;
    }

    /* access modifiers changed from: protected */
    public int c(View view, int i, int i2, int i3) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = ((i3 - measuredHeight) / 2) + i2;
        view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        return measuredWidth;
    }
}
