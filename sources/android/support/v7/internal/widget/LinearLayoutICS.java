package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.b.k;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

/* compiled from: MyApp */
public class LinearLayoutICS extends LinearLayout {
    private final Drawable a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LinearLayoutICS(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        boolean z = true;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, k.LinearLayoutICS);
        this.a = obtainStyledAttributes.getDrawable(0);
        if (this.a != null) {
            this.b = this.a.getIntrinsicWidth();
            this.c = this.a.getIntrinsicHeight();
        } else {
            this.b = 0;
            this.c = 0;
        }
        this.d = obtainStyledAttributes.getInt(1, 0);
        this.e = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        obtainStyledAttributes.recycle();
        setWillNotDraw(this.a != null ? false : z);
    }

    public int getSupportDividerWidth() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.a != null) {
            if (getOrientation() == 1) {
                a(canvas);
            } else {
                b(canvas);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void measureChildWithMargins(View view, int i, int i2, int i3, int i4) {
        if (this.a != null) {
            int indexOfChild = indexOfChild(view);
            int childCount = getChildCount();
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
            if (getOrientation() == 1) {
                if (a(indexOfChild)) {
                    layoutParams.topMargin = this.c;
                } else if (indexOfChild == childCount - 1 && a(childCount)) {
                    layoutParams.bottomMargin = this.c;
                }
            } else if (a(indexOfChild)) {
                layoutParams.leftMargin = this.b;
            } else if (indexOfChild == childCount - 1 && a(childCount)) {
                layoutParams.rightMargin = this.b;
            }
        }
        super.measureChildWithMargins(view, i, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas) {
        int bottom;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (!(childAt == null || childAt.getVisibility() == 8 || !a(i))) {
                a(canvas, childAt.getTop() - ((LinearLayout.LayoutParams) childAt.getLayoutParams()).topMargin);
            }
        }
        if (a(childCount)) {
            View childAt2 = getChildAt(childCount - 1);
            if (childAt2 == null) {
                bottom = (getHeight() - getPaddingBottom()) - this.c;
            } else {
                bottom = childAt2.getBottom();
            }
            a(canvas, bottom);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Canvas canvas) {
        int right;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (!(childAt == null || childAt.getVisibility() == 8 || !a(i))) {
                b(canvas, childAt.getLeft() - ((LinearLayout.LayoutParams) childAt.getLayoutParams()).leftMargin);
            }
        }
        if (a(childCount)) {
            View childAt2 = getChildAt(childCount - 1);
            if (childAt2 == null) {
                right = (getWidth() - getPaddingRight()) - this.b;
            } else {
                right = childAt2.getRight();
            }
            b(canvas, right);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas, int i) {
        this.a.setBounds(getPaddingLeft() + this.e, i, (getWidth() - getPaddingRight()) - this.e, this.c + i);
        this.a.draw(canvas);
    }

    /* access modifiers changed from: package-private */
    public void b(Canvas canvas, int i) {
        this.a.setBounds(i, getPaddingTop() + this.e, this.b + i, (getHeight() - getPaddingBottom()) - this.e);
        this.a.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public boolean a(int i) {
        if (i == 0) {
            return (this.d & 1) != 0;
        }
        if (i == getChildCount()) {
            return (this.d & 4) != 0;
        }
        if ((this.d & 2) == 0) {
            return false;
        }
        for (int i2 = i - 1; i2 >= 0; i2--) {
            if (getChildAt(i2).getVisibility() != 8) {
                return true;
            }
        }
        return false;
    }
}
