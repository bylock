package android.support.v7.internal.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public abstract class AbsSpinnerICS extends l {
    private DataSetObserver E;
    SpinnerAdapter a;
    int b;
    int c;
    boolean d;
    int e = 0;
    int f = 0;
    int g = 0;
    int h = 0;
    final Rect i = new Rect();
    final d j = new d(this);

    AbsSpinnerICS(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        o();
    }

    private void o() {
        setFocusable(true);
        setWillNotDraw(false);
    }

    public void a(SpinnerAdapter spinnerAdapter) {
        int i2 = -1;
        if (this.a != null) {
            this.a.unregisterDataSetObserver(this.E);
            a();
        }
        this.a = spinnerAdapter;
        this.B = -1;
        this.C = Long.MIN_VALUE;
        if (this.a != null) {
            this.A = this.z;
            this.z = this.a.getCount();
            i();
            this.E = new n(this);
            this.a.registerDataSetObserver(this.E);
            if (this.z > 0) {
                i2 = 0;
            }
            c(i2);
            d(i2);
            if (this.z == 0) {
                l();
            }
        } else {
            i();
            a();
            l();
        }
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.internal.widget.l
    public void a() {
        this.u = false;
        this.p = false;
        removeAllViewsInLayout();
        this.B = -1;
        this.C = Long.MIN_VALUE;
        c(-1);
        d(-1);
        invalidate();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r10, int r11) {
        /*
        // Method dump skipped, instructions count: 229
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.AbsSpinnerICS.onMeasure(int, int):void");
    }

    /* access modifiers changed from: package-private */
    public int a(View view) {
        return view.getMeasuredHeight();
    }

    /* access modifiers changed from: package-private */
    public int b(View view) {
        return view.getMeasuredWidth();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.LayoutParams(-1, -2);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        int childCount = getChildCount();
        d dVar = this.j;
        int i2 = this.k;
        for (int i3 = 0; i3 < childCount; i3++) {
            dVar.a(i2 + i3, getChildAt(i3));
        }
    }

    public void a(int i2) {
        d(i2);
        requestLayout();
        invalidate();
    }

    @Override // android.support.v7.internal.widget.l
    public View c() {
        if (this.z <= 0 || this.x < 0) {
            return null;
        }
        return getChildAt(this.x - this.k);
    }

    public void requestLayout() {
        if (!this.d) {
            super.requestLayout();
        }
    }

    /* renamed from: d */
    public SpinnerAdapter e() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    /* compiled from: MyApp */
    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new e();
        long a;
        int b;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readLong();
            this.b = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeLong(this.a);
            parcel.writeInt(this.b);
        }

        public String toString() {
            return "AbsSpinner.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " selectedId=" + this.a + " position=" + this.b + "}";
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = g();
        if (savedState.a >= 0) {
            savedState.b = f();
        } else {
            savedState.b = -1;
        }
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.a >= 0) {
            this.u = true;
            this.p = true;
            this.n = savedState.a;
            this.m = savedState.b;
            this.q = 0;
            requestLayout();
        }
    }
}
