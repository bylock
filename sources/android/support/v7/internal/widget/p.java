package android.support.v7.internal.widget;

import android.view.View;
import android.widget.AdapterView;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class p implements AdapterView.OnItemClickListener {
    final /* synthetic */ l a;
    private final o b;

    public p(l lVar, o oVar) {
        this.a = lVar;
        this.b = oVar;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.a(this.a, view, i, j);
    }
}
