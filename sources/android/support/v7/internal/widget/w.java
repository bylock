package android.support.v7.internal.widget;

import android.view.View;
import android.widget.AdapterView;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class w implements AdapterView.OnItemSelectedListener {
    final /* synthetic */ u a;

    w(u uVar) {
        this.a = uVar;
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        x xVar;
        if (i != -1 && (xVar = this.a.e) != null) {
            xVar.a = false;
        }
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView adapterView) {
    }
}
