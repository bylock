package android.support.v7.internal.widget;

import android.support.v7.a.e;
import android.support.v7.internal.widget.ScrollingTabContainerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ai extends BaseAdapter {
    final /* synthetic */ ScrollingTabContainerView a;

    private ai(ScrollingTabContainerView scrollingTabContainerView) {
        this.a = scrollingTabContainerView;
    }

    /* synthetic */ ai(ScrollingTabContainerView scrollingTabContainerView, ah ahVar) {
        this(scrollingTabContainerView);
    }

    public int getCount() {
        return this.a.e.getChildCount();
    }

    public Object getItem(int i) {
        return ((ScrollingTabContainerView.TabView) this.a.e.getChildAt(i)).getTab();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            return this.a.a((e) getItem(i), true);
        }
        ((ScrollingTabContainerView.TabView) view).a((e) getItem(i));
        return view;
    }
}
