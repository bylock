package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.v7.a.a;
import android.support.v7.b.c;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/* compiled from: MyApp */
public class ActionBarOverlayLayout extends FrameLayout {
    static final int[] a = {c.actionBarSize};
    private int b;
    private a c;
    private final Rect d = new Rect(0, 0, 0, 0);

    public ActionBarOverlayLayout(Context context) {
        super(context);
        a(context);
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(a);
        this.b = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        obtainStyledAttributes.recycle();
    }

    public void setActionBar(a aVar) {
        this.c = aVar;
    }
}
