package android.support.v7.internal.widget;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class af implements Runnable {
    final /* synthetic */ ProgressBarICS a;
    private int b;
    private int c;
    private boolean d;

    af(ProgressBarICS progressBarICS, int i, int i2, boolean z) {
        this.a = progressBarICS;
        this.b = i;
        this.c = i2;
        this.d = z;
    }

    public void run() {
        this.a.a(this.b, this.c, this.d, true);
        this.a.u = this;
    }

    public void a(int i, int i2, boolean z) {
        this.b = i;
        this.c = i2;
        this.d = z;
    }
}
