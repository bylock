package android.support.v7.internal.widget;

import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ah implements Runnable {
    final /* synthetic */ View a;
    final /* synthetic */ ScrollingTabContainerView b;

    ah(ScrollingTabContainerView scrollingTabContainerView, View view) {
        this.b = scrollingTabContainerView;
        this.a = view;
    }

    public void run() {
        this.b.smoothScrollTo(this.a.getLeft() - ((this.b.getWidth() - this.a.getWidth()) / 2), 0);
        this.b.a = null;
    }
}
