package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.b.c;
import android.support.v7.b.f;
import android.support.v7.b.h;
import android.support.v7.b.k;
import android.support.v7.c.a;
import android.support.v7.internal.view.menu.ActionMenuPresenter;
import android.support.v7.internal.view.menu.ActionMenuView;
import android.support.v7.internal.view.menu.q;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/* compiled from: MyApp */
public class ActionBarContextView extends a {
    private CharSequence g;
    private CharSequence h;
    private View i;
    private View j;
    private LinearLayout k;
    private TextView l;
    private TextView m;
    private int n;
    private int o;
    private Drawable p;
    private boolean q;

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void a(int i2) {
        super.a(i2);
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ boolean e() {
        return super.e();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void f() {
        super.f();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void setSplitView(ActionBarContainer actionBarContainer) {
        super.setSplitView(actionBarContainer);
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void setSplitWhenNarrow(boolean z) {
        super.setSplitWhenNarrow(z);
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void setVisibility(int i2) {
        super.setVisibility(i2);
    }

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, c.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, k.ActionMode, i2, 0);
        setBackgroundDrawable(obtainStyledAttributes.getDrawable(3));
        this.n = obtainStyledAttributes.getResourceId(1, 0);
        this.o = obtainStyledAttributes.getResourceId(2, 0);
        this.f = obtainStyledAttributes.getLayoutDimension(0, 0);
        this.p = obtainStyledAttributes.getDrawable(4);
        obtainStyledAttributes.recycle();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.b != null) {
            this.b.b();
            this.b.d();
        }
    }

    @Override // android.support.v7.internal.widget.a
    public void setSplitActionBar(boolean z) {
        if (this.d != z) {
            if (this.b != null) {
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
                if (!z) {
                    this.a = (ActionMenuView) this.b.a(this);
                    this.a.setBackgroundDrawable(null);
                    ViewGroup viewGroup = (ViewGroup) this.a.getParent();
                    if (viewGroup != null) {
                        viewGroup.removeView(this.a);
                    }
                    addView(this.a, layoutParams);
                } else {
                    this.b.a(getContext().getResources().getDisplayMetrics().widthPixels, true);
                    this.b.a(Integer.MAX_VALUE);
                    layoutParams.width = -1;
                    layoutParams.height = this.f;
                    this.a = (ActionMenuView) this.b.a(this);
                    this.a.setBackgroundDrawable(this.p);
                    ViewGroup viewGroup2 = (ViewGroup) this.a.getParent();
                    if (viewGroup2 != null) {
                        viewGroup2.removeView(this.a);
                    }
                    this.c.addView(this.a, layoutParams);
                }
            }
            super.setSplitActionBar(z);
        }
    }

    @Override // android.support.v7.internal.widget.a
    public void setContentHeight(int i2) {
        this.f = i2;
    }

    public void setCustomView(View view) {
        if (this.j != null) {
            removeView(this.j);
        }
        this.j = view;
        if (this.k != null) {
            removeView(this.k);
            this.k = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    public void setTitle(CharSequence charSequence) {
        this.g = charSequence;
        i();
    }

    public void setSubtitle(CharSequence charSequence) {
        this.h = charSequence;
        i();
    }

    public CharSequence getTitle() {
        return this.g;
    }

    public CharSequence getSubtitle() {
        return this.h;
    }

    private void i() {
        int i2;
        int i3 = 8;
        boolean z = true;
        if (this.k == null) {
            LayoutInflater.from(getContext()).inflate(h.abc_action_bar_title_item, this);
            this.k = (LinearLayout) getChildAt(getChildCount() - 1);
            this.l = (TextView) this.k.findViewById(f.action_bar_title);
            this.m = (TextView) this.k.findViewById(f.action_bar_subtitle);
            if (this.n != 0) {
                this.l.setTextAppearance(getContext(), this.n);
            }
            if (this.o != 0) {
                this.m.setTextAppearance(getContext(), this.o);
            }
        }
        this.l.setText(this.g);
        this.m.setText(this.h);
        boolean z2 = !TextUtils.isEmpty(this.g);
        if (TextUtils.isEmpty(this.h)) {
            z = false;
        }
        TextView textView = this.m;
        if (z) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        textView.setVisibility(i2);
        LinearLayout linearLayout = this.k;
        if (z2 || z) {
            i3 = 0;
        }
        linearLayout.setVisibility(i3);
        if (this.k.getParent() == null) {
            addView(this.k);
        }
    }

    public void a(a aVar) {
        if (this.i == null) {
            this.i = LayoutInflater.from(getContext()).inflate(h.abc_action_mode_close_item, (ViewGroup) this, false);
            addView(this.i);
        } else if (this.i.getParent() == null) {
            addView(this.i);
        }
        this.i.findViewById(f.action_mode_close_button).setOnClickListener(new f(this, aVar));
        q qVar = (q) aVar.a();
        if (this.b != null) {
            this.b.c();
        }
        this.b = new ActionMenuPresenter(getContext());
        this.b.a(true);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
        if (!this.d) {
            qVar.a(this.b);
            this.a = (ActionMenuView) this.b.a(this);
            this.a.setBackgroundDrawable(null);
            addView(this.a, layoutParams);
            return;
        }
        this.b.a(getContext().getResources().getDisplayMetrics().widthPixels, true);
        this.b.a(Integer.MAX_VALUE);
        layoutParams.width = -1;
        layoutParams.height = this.f;
        qVar.a(this.b);
        this.a = (ActionMenuView) this.b.a(this);
        this.a.setBackgroundDrawable(this.p);
        this.c.addView(this.a, layoutParams);
    }

    public void g() {
        if (this.i == null) {
            h();
        }
    }

    public void h() {
        removeAllViews();
        if (this.c != null) {
            this.c.removeView(this.a);
        }
        this.j = null;
        this.a = null;
    }

    @Override // android.support.v7.internal.widget.a
    public boolean a() {
        if (this.b != null) {
            return this.b.a();
        }
        return false;
    }

    @Override // android.support.v7.internal.widget.a
    public boolean c() {
        if (this.b != null) {
            return this.b.b();
        }
        return false;
    }

    @Override // android.support.v7.internal.widget.a
    public boolean d() {
        if (this.b != null) {
            return this.b.e();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6 = 1073741824;
        int i7 = 0;
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_width=\"FILL_PARENT\" (or fill_parent)");
        } else if (View.MeasureSpec.getMode(i3) == 0) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_height=\"wrap_content\"");
        } else {
            int size = View.MeasureSpec.getSize(i2);
            int size2 = this.f > 0 ? this.f : View.MeasureSpec.getSize(i3);
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int i8 = size2 - paddingTop;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i8, Integer.MIN_VALUE);
            if (this.i != null) {
                int a = a(this.i, paddingLeft, makeMeasureSpec, 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.i.getLayoutParams();
                paddingLeft = a - (marginLayoutParams.rightMargin + marginLayoutParams.leftMargin);
            }
            if (this.a != null && this.a.getParent() == this) {
                paddingLeft = a(this.a, paddingLeft, makeMeasureSpec, 0);
            }
            if (this.k != null && this.j == null) {
                if (this.q) {
                    this.k.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.k.getMeasuredWidth();
                    boolean z = measuredWidth <= paddingLeft;
                    if (z) {
                        paddingLeft -= measuredWidth;
                    }
                    this.k.setVisibility(z ? 0 : 8);
                } else {
                    paddingLeft = a(this.k, paddingLeft, makeMeasureSpec, 0);
                }
            }
            if (this.j != null) {
                ViewGroup.LayoutParams layoutParams = this.j.getLayoutParams();
                if (layoutParams.width != -2) {
                    i4 = 1073741824;
                } else {
                    i4 = Integer.MIN_VALUE;
                }
                if (layoutParams.width >= 0) {
                    paddingLeft = Math.min(layoutParams.width, paddingLeft);
                }
                if (layoutParams.height == -2) {
                    i6 = Integer.MIN_VALUE;
                }
                if (layoutParams.height >= 0) {
                    i5 = Math.min(layoutParams.height, i8);
                } else {
                    i5 = i8;
                }
                this.j.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i4), View.MeasureSpec.makeMeasureSpec(i5, i6));
            }
            if (this.f <= 0) {
                int childCount = getChildCount();
                int i9 = 0;
                while (i7 < childCount) {
                    int measuredHeight = getChildAt(i7).getMeasuredHeight() + paddingTop;
                    if (measuredHeight <= i9) {
                        measuredHeight = i9;
                    }
                    i7++;
                    i9 = measuredHeight;
                }
                setMeasuredDimension(size, i9);
                return;
            }
            setMeasuredDimension(size, size2);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i5 - i3) - getPaddingTop()) - getPaddingBottom();
        if (this.i == null || this.i.getVisibility() == 8) {
            i6 = paddingLeft;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.i.getLayoutParams();
            int i7 = paddingLeft + marginLayoutParams.leftMargin;
            i6 = marginLayoutParams.rightMargin + i7 + b(this.i, i7, paddingTop, paddingTop2);
        }
        if (!(this.k == null || this.j != null || this.k.getVisibility() == 8)) {
            i6 += b(this.k, i6, paddingTop, paddingTop2);
        }
        if (this.j != null) {
            int b = i6 + b(this.j, i6, paddingTop, paddingTop2);
        }
        int paddingRight = (i4 - i2) - getPaddingRight();
        if (this.a != null) {
            int c = paddingRight - c(this.a, paddingRight, paddingTop, paddingTop2);
        }
    }

    public void setTitleOptional(boolean z) {
        if (z != this.q) {
            requestLayout();
        }
        this.q = z;
    }
}
