package android.support.v7.internal.widget;

import android.database.DataSetObserver;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class z extends DataSetObserver {
    final /* synthetic */ u a;

    private z(u uVar) {
        this.a = uVar;
    }

    /* synthetic */ z(u uVar, v vVar) {
        this(uVar);
    }

    public void onChanged() {
        if (this.a.f()) {
            this.a.c();
        }
    }

    public void onInvalidated() {
        this.a.d();
    }
}
