package android.support.v7.internal.widget;

import android.support.v7.internal.view.menu.u;
import android.view.View;

/* compiled from: MyApp */
class h implements View.OnClickListener {
    final /* synthetic */ ActionBarView a;

    h(ActionBarView actionBarView) {
        this.a = actionBarView;
    }

    public void onClick(View view) {
        u uVar = this.a.R.b;
        if (uVar != null) {
            uVar.collapseActionView();
        }
    }
}
