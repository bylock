package android.support.v7.internal.widget;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.b.a.b;
import android.support.v7.a.d;
import android.support.v7.b.c;
import android.support.v7.b.f;
import android.support.v7.b.h;
import android.support.v7.b.i;
import android.support.v7.b.k;
import android.support.v7.internal.view.menu.ActionMenuPresenter;
import android.support.v7.internal.view.menu.ActionMenuView;
import android.support.v7.internal.view.menu.a;
import android.support.v7.internal.view.menu.ag;
import android.support.v7.internal.view.menu.q;
import android.support.v7.internal.view.menu.u;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

/* compiled from: MyApp */
public class ActionBarView extends a {
    private ProgressBarICS A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private boolean H;
    private boolean I;
    private boolean J;
    private boolean K;
    private q L;
    private ActionBarContextView M;
    private a N;
    private SpinnerAdapter O;
    private d P;
    private Runnable Q;
    private j R;
    private final q S = new g(this);
    private final View.OnClickListener T = new h(this);
    private final View.OnClickListener U = new i(this);
    View g;
    Window.Callback h;
    private int i;
    private int j = -1;
    private CharSequence k;
    private CharSequence l;
    private Drawable m;
    private Drawable n;
    private Context o;
    private HomeView p;
    private HomeView q;
    private LinearLayout r;
    private TextView s;
    private TextView t;
    private View u;
    private ak v;
    private LinearLayout w;
    private ScrollingTabContainerView x;
    private View y;
    private ProgressBarICS z;

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void a(int i2) {
        super.a(i2);
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ boolean a() {
        return super.a();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ boolean c() {
        return super.c();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ boolean d() {
        return super.d();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ boolean e() {
        return super.e();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void f() {
        super.f();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void setContentHeight(int i2) {
        super.setContentHeight(i2);
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void setSplitView(ActionBarContainer actionBarContainer) {
        super.setSplitView(actionBarContainer);
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void setSplitWhenNarrow(boolean z2) {
        super.setSplitWhenNarrow(z2);
    }

    @Override // android.support.v7.internal.widget.a
    public /* bridge */ /* synthetic */ void setVisibility(int i2) {
        super.setVisibility(i2);
    }

    public ActionBarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.o = context;
        setBackgroundResource(0);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, k.ActionBar, c.actionBarStyle, 0);
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        PackageManager packageManager = context.getPackageManager();
        this.i = obtainStyledAttributes.getInt(2, 0);
        this.k = obtainStyledAttributes.getText(0);
        this.l = obtainStyledAttributes.getText(4);
        this.n = obtainStyledAttributes.getDrawable(8);
        if (this.n == null && Build.VERSION.SDK_INT >= 9) {
            if (context instanceof Activity) {
                try {
                    this.n = packageManager.getActivityLogo(((Activity) context).getComponentName());
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e("ActionBarView", "Activity component name not found!", e);
                }
            }
            if (this.n == null) {
                this.n = applicationInfo.loadLogo(packageManager);
            }
        }
        this.m = obtainStyledAttributes.getDrawable(7);
        if (this.m == null) {
            if (context instanceof Activity) {
                try {
                    this.m = packageManager.getActivityIcon(((Activity) context).getComponentName());
                } catch (PackageManager.NameNotFoundException e2) {
                    Log.e("ActionBarView", "Activity component name not found!", e2);
                }
            }
            if (this.m == null) {
                this.m = applicationInfo.loadIcon(packageManager);
            }
        }
        LayoutInflater from = LayoutInflater.from(context);
        int resourceId = obtainStyledAttributes.getResourceId(14, h.abc_action_bar_home);
        this.p = (HomeView) from.inflate(resourceId, (ViewGroup) this, false);
        this.q = (HomeView) from.inflate(resourceId, (ViewGroup) this, false);
        this.q.a(true);
        this.q.setOnClickListener(this.T);
        this.q.setContentDescription(getResources().getText(i.abc_action_bar_up_description));
        this.D = obtainStyledAttributes.getResourceId(5, 0);
        this.E = obtainStyledAttributes.getResourceId(6, 0);
        this.F = obtainStyledAttributes.getResourceId(15, 0);
        this.G = obtainStyledAttributes.getResourceId(16, 0);
        this.B = obtainStyledAttributes.getDimensionPixelOffset(17, 0);
        this.C = obtainStyledAttributes.getDimensionPixelOffset(18, 0);
        setDisplayOptions(obtainStyledAttributes.getInt(3, 0));
        int resourceId2 = obtainStyledAttributes.getResourceId(13, 0);
        if (resourceId2 != 0) {
            this.y = from.inflate(resourceId2, (ViewGroup) this, false);
            this.i = 0;
            setDisplayOptions(this.j | 16);
        }
        this.f = obtainStyledAttributes.getLayoutDimension(1, 0);
        obtainStyledAttributes.recycle();
        this.N = new a(context, 0, 16908332, 0, 0, this.k);
        this.p.setOnClickListener(this.U);
        this.p.setClickable(true);
        this.p.setFocusable(true);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.internal.widget.a
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.s = null;
        this.t = null;
        this.u = null;
        if (this.r != null && this.r.getParent() == this) {
            removeView(this.r);
        }
        this.r = null;
        if ((this.j & 8) != 0) {
            n();
        }
        if (this.x != null && this.I) {
            ViewGroup.LayoutParams layoutParams = this.x.getLayoutParams();
            if (layoutParams != null) {
                layoutParams.width = -2;
                layoutParams.height = -1;
            }
            this.x.setAllowCollapse(true);
        }
        if (this.z != null) {
            removeView(this.z);
            g();
        }
        if (this.A != null) {
            removeView(this.A);
            h();
        }
    }

    public void setWindowCallback(Window.Callback callback) {
        this.h = callback;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.Q);
        if (this.b != null) {
            this.b.b();
            this.b.d();
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public void g() {
        this.z = new ProgressBarICS(this.o, null, 0, this.F);
        this.z.setId(f.progress_horizontal);
        this.z.setMax(10000);
        this.z.setVisibility(8);
        addView(this.z);
    }

    public void h() {
        this.A = new ProgressBarICS(this.o, null, 0, this.G);
        this.A.setId(f.progress_circular);
        this.A.setVisibility(8);
        addView(this.A);
    }

    @Override // android.support.v7.internal.widget.a
    public void setSplitActionBar(boolean z2) {
        if (this.d != z2) {
            if (this.a != null) {
                ViewGroup viewGroup = (ViewGroup) this.a.getParent();
                if (viewGroup != null) {
                    viewGroup.removeView(this.a);
                }
                if (z2) {
                    if (this.c != null) {
                        this.c.addView(this.a);
                    }
                    this.a.getLayoutParams().width = -1;
                } else {
                    addView(this.a);
                    this.a.getLayoutParams().width = -2;
                }
                this.a.requestLayout();
            }
            if (this.c != null) {
                this.c.setVisibility(z2 ? 0 : 8);
            }
            if (this.b != null) {
                if (!z2) {
                    this.b.b(getResources().getBoolean(android.support.v7.b.d.abc_action_bar_expanded_action_views_exclusive));
                } else {
                    this.b.b(false);
                    this.b.a(getContext().getResources().getDisplayMetrics().widthPixels, true);
                    this.b.a(Integer.MAX_VALUE);
                }
            }
            super.setSplitActionBar(z2);
        }
    }

    public boolean i() {
        return this.d;
    }

    public boolean j() {
        return this.I;
    }

    public void setEmbeddedTabView(ScrollingTabContainerView scrollingTabContainerView) {
        if (this.x != null) {
            removeView(this.x);
        }
        this.x = scrollingTabContainerView;
        this.I = scrollingTabContainerView != null;
        if (this.I && this.i == 2) {
            addView(this.x);
            ViewGroup.LayoutParams layoutParams = this.x.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -1;
            scrollingTabContainerView.setAllowCollapse(true);
        }
    }

    public void setCallback(d dVar) {
        this.P = dVar;
    }

    public void a(android.support.v4.b.a.a aVar, ag agVar) {
        ActionMenuView actionMenuView;
        ViewGroup viewGroup;
        if (aVar != this.L) {
            if (this.L != null) {
                this.L.b(this.b);
                this.L.b(this.R);
            }
            q qVar = (q) aVar;
            this.L = qVar;
            if (!(this.a == null || (viewGroup = (ViewGroup) this.a.getParent()) == null)) {
                viewGroup.removeView(this.a);
            }
            if (this.b == null) {
                this.b = new ActionMenuPresenter(this.o);
                this.b.a(agVar);
                this.b.b(f.action_menu_presenter);
                this.R = new j(this, null);
            }
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
            if (!this.d) {
                this.b.b(getResources().getBoolean(android.support.v7.b.d.abc_action_bar_expanded_action_views_exclusive));
                a(qVar);
                actionMenuView = (ActionMenuView) this.b.a(this);
                actionMenuView.a(qVar);
                ViewGroup viewGroup2 = (ViewGroup) actionMenuView.getParent();
                if (!(viewGroup2 == null || viewGroup2 == this)) {
                    viewGroup2.removeView(actionMenuView);
                }
                addView(actionMenuView, layoutParams);
            } else {
                this.b.b(false);
                this.b.a(getContext().getResources().getDisplayMetrics().widthPixels, true);
                this.b.a(Integer.MAX_VALUE);
                layoutParams.width = -1;
                a(qVar);
                actionMenuView = (ActionMenuView) this.b.a(this);
                if (this.c != null) {
                    ViewGroup viewGroup3 = (ViewGroup) actionMenuView.getParent();
                    if (!(viewGroup3 == null || viewGroup3 == this.c)) {
                        viewGroup3.removeView(actionMenuView);
                    }
                    actionMenuView.setVisibility(getAnimatedVisibility());
                    this.c.addView(actionMenuView, layoutParams);
                } else {
                    actionMenuView.setLayoutParams(layoutParams);
                }
            }
            this.a = actionMenuView;
        }
    }

    private void a(q qVar) {
        if (qVar != null) {
            qVar.a(this.b);
            qVar.a(this.R);
        } else {
            this.b.a(this.o, (q) null);
            this.R.a(this.o, (q) null);
        }
        this.b.c(true);
        this.R.c(true);
    }

    public boolean k() {
        return (this.R == null || this.R.b == null) ? false : true;
    }

    public void l() {
        u uVar = this.R == null ? null : this.R.b;
        if (uVar != null) {
            uVar.collapseActionView();
        }
    }

    public void setCustomNavigationView(View view) {
        boolean z2 = (this.j & 16) != 0;
        if (this.y != null && z2) {
            removeView(this.y);
        }
        this.y = view;
        if (this.y != null && z2) {
            addView(this.y);
        }
    }

    public CharSequence getTitle() {
        return this.k;
    }

    public void setTitle(CharSequence charSequence) {
        this.H = true;
        setTitleImpl(charSequence);
    }

    public void setWindowTitle(CharSequence charSequence) {
        if (!this.H) {
            setTitleImpl(charSequence);
        }
    }

    private void setTitleImpl(CharSequence charSequence) {
        int i2 = 0;
        this.k = charSequence;
        if (this.s != null) {
            this.s.setText(charSequence);
            boolean z2 = this.g == null && (this.j & 8) != 0 && (!TextUtils.isEmpty(this.k) || !TextUtils.isEmpty(this.l));
            LinearLayout linearLayout = this.r;
            if (!z2) {
                i2 = 8;
            }
            linearLayout.setVisibility(i2);
        }
        if (this.N != null) {
            this.N.setTitle(charSequence);
        }
    }

    public CharSequence getSubtitle() {
        return this.l;
    }

    public void setSubtitle(CharSequence charSequence) {
        boolean z2;
        int i2 = 0;
        this.l = charSequence;
        if (this.t != null) {
            this.t.setText(charSequence);
            this.t.setVisibility(charSequence != null ? 0 : 8);
            if (this.g != null || (this.j & 8) == 0 || (TextUtils.isEmpty(this.k) && TextUtils.isEmpty(this.l))) {
                z2 = false;
            } else {
                z2 = true;
            }
            LinearLayout linearLayout = this.r;
            if (!z2) {
                i2 = 8;
            }
            linearLayout.setVisibility(i2);
        }
    }

    public void setHomeButtonEnabled(boolean z2) {
        this.p.setEnabled(z2);
        this.p.setFocusable(z2);
        if (!z2) {
            this.p.setContentDescription(null);
        } else if ((this.j & 4) != 0) {
            this.p.setContentDescription(this.o.getResources().getText(i.abc_action_bar_up_description));
        } else {
            this.p.setContentDescription(this.o.getResources().getText(i.abc_action_bar_home_description));
        }
    }

    public void setDisplayOptions(int i2) {
        boolean z2;
        int i3;
        boolean z3;
        boolean z4;
        int i4 = 8;
        int i5 = -1;
        boolean z5 = true;
        if (this.j != -1) {
            i5 = this.j ^ i2;
        }
        this.j = i2;
        if ((i5 & 31) != 0) {
            if ((i2 & 2) != 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (!z2 || this.g != null) {
                i3 = 8;
            } else {
                i3 = 0;
            }
            this.p.setVisibility(i3);
            if ((i5 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                this.p.a(z4);
                if (z4) {
                    setHomeButtonEnabled(true);
                }
            }
            if ((i5 & 1) != 0) {
                this.p.a(this.n != null && (i2 & 1) != 0 ? this.n : this.m);
            }
            if ((i5 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    n();
                } else {
                    removeView(this.r);
                }
            }
            if (!(this.r == null || (i5 & 6) == 0)) {
                if ((this.j & 4) != 0) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                View view = this.u;
                if (!z2) {
                    i4 = z3 ? 0 : 4;
                }
                view.setVisibility(i4);
                LinearLayout linearLayout = this.r;
                if (z2 || !z3) {
                    z5 = false;
                }
                linearLayout.setEnabled(z5);
            }
            if (!((i5 & 16) == 0 || this.y == null)) {
                if ((i2 & 16) != 0) {
                    addView(this.y);
                } else {
                    removeView(this.y);
                }
            }
            requestLayout();
        } else {
            invalidate();
        }
        if (!this.p.isEnabled()) {
            this.p.setContentDescription(null);
        } else if ((i2 & 4) != 0) {
            this.p.setContentDescription(this.o.getResources().getText(i.abc_action_bar_up_description));
        } else {
            this.p.setContentDescription(this.o.getResources().getText(i.abc_action_bar_home_description));
        }
    }

    public void setIcon(Drawable drawable) {
        this.m = drawable;
        if (drawable != null && ((this.j & 1) == 0 || this.n == null)) {
            this.p.a(drawable);
        }
        if (this.g != null) {
            this.q.a(this.m.getConstantState().newDrawable(getResources()));
        }
    }

    public void setIcon(int i2) {
        setIcon(this.o.getResources().getDrawable(i2));
    }

    public void setLogo(Drawable drawable) {
        this.n = drawable;
        if (drawable != null && (this.j & 1) != 0) {
            this.p.a(drawable);
        }
    }

    public void setLogo(int i2) {
        setLogo(this.o.getResources().getDrawable(i2));
    }

    public void setNavigationMode(int i2) {
        int i3 = this.i;
        if (i2 != i3) {
            switch (i3) {
                case 1:
                    if (this.w != null) {
                        removeView(this.w);
                        break;
                    }
                    break;
                case 2:
                    if (this.x != null && this.I) {
                        removeView(this.x);
                        break;
                    }
            }
            switch (i2) {
                case 1:
                    if (this.v == null) {
                        this.v = new ak(this.o, null, c.actionDropDownStyle);
                        this.w = (LinearLayout) LayoutInflater.from(this.o).inflate(h.abc_action_bar_view_list_nav_layout, (ViewGroup) null);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
                        layoutParams.gravity = 17;
                        this.w.addView(this.v, layoutParams);
                    }
                    if (this.v.e() != this.O) {
                        this.v.a(this.O);
                    }
                    this.v.a(this.S);
                    addView(this.w);
                    break;
                case 2:
                    if (this.x != null && this.I) {
                        addView(this.x);
                        break;
                    }
            }
            this.i = i2;
            requestLayout();
        }
    }

    public void setDropdownAdapter(SpinnerAdapter spinnerAdapter) {
        this.O = spinnerAdapter;
        if (this.v != null) {
            this.v.a(spinnerAdapter);
        }
    }

    public SpinnerAdapter getDropdownAdapter() {
        return this.O;
    }

    public void setDropdownSelectedPosition(int i2) {
        this.v.a(i2);
    }

    public int getDropdownSelectedPosition() {
        return this.v.f();
    }

    public View getCustomNavigationView() {
        return this.y;
    }

    public int getNavigationMode() {
        return this.i;
    }

    public int getDisplayOptions() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new android.support.v7.a.c(19);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        ViewParent parent;
        super.onFinishInflate();
        addView(this.p);
        if (this.y != null && (this.j & 16) != 0 && (parent = this.y.getParent()) != this) {
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.y);
            }
            addView(this.y);
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        boolean z2;
        boolean z3 = true;
        if (this.r == null) {
            this.r = (LinearLayout) LayoutInflater.from(getContext()).inflate(h.abc_action_bar_title_item, (ViewGroup) this, false);
            this.s = (TextView) this.r.findViewById(f.action_bar_title);
            this.t = (TextView) this.r.findViewById(f.action_bar_subtitle);
            this.u = this.r.findViewById(f.up);
            this.r.setOnClickListener(this.U);
            if (this.D != 0) {
                this.s.setTextAppearance(this.o, this.D);
            }
            if (this.k != null) {
                this.s.setText(this.k);
            }
            if (this.E != 0) {
                this.t.setTextAppearance(this.o, this.E);
            }
            if (this.l != null) {
                this.t.setText(this.l);
                this.t.setVisibility(0);
            }
            boolean z4 = (this.j & 4) != 0;
            if ((this.j & 2) != 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.u.setVisibility(!z2 ? z4 ? 0 : 4 : 8);
            LinearLayout linearLayout = this.r;
            if (!z4 || z2) {
                z3 = false;
            }
            linearLayout.setEnabled(z3);
        }
        addView(this.r);
        if (this.g != null || (TextUtils.isEmpty(this.k) && TextUtils.isEmpty(this.l))) {
            this.r.setVisibility(8);
        }
    }

    public void setContextView(ActionBarContextView actionBarContextView) {
        this.M = actionBarContextView;
    }

    public void setCollapsable(boolean z2) {
        this.J = z2;
    }

    public boolean m() {
        return this.K;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x035e  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x039c  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x025a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r20, int r21) {
        /*
        // Method dump skipped, instructions count: 952
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.ActionBarView.onMeasure(int, int):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:100:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0150  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r10, int r11, int r12, int r13, int r14) {
        /*
        // Method dump skipped, instructions count: 494
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.ActionBarView.onLayout(boolean, int, int, int, int):void");
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new android.support.v7.a.c(getContext(), attributeSet);
    }

    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) {
            return generateDefaultLayoutParams();
        }
        return layoutParams;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (!(this.R == null || this.R.b == null)) {
            savedState.a = this.R.b.getItemId();
        }
        savedState.b = d();
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        b bVar;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.a == 0 || this.R == null || this.L == null || (bVar = (b) this.L.findItem(savedState.a)) == null)) {
            bVar.expandActionView();
        }
        if (savedState.b) {
            b();
        }
    }

    public void setHomeAsUpIndicator(Drawable drawable) {
        this.p.b(drawable);
    }

    public void setHomeAsUpIndicator(int i2) {
        this.p.a(i2);
    }

    /* compiled from: MyApp */
    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new k();
        int a;
        boolean b;

        /* synthetic */ SavedState(Parcel parcel, g gVar) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
            this.b = parcel.readInt() != 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
            parcel.writeInt(this.b ? 1 : 0);
        }
    }

    /* access modifiers changed from: package-private */
    /* compiled from: MyApp */
    public class HomeView extends FrameLayout {
        private ImageView a;
        private ImageView b;
        private int c;
        private int d;
        private Drawable e;

        public HomeView(Context context) {
            this(context, null);
        }

        public HomeView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public void a(boolean z) {
            this.a.setVisibility(z ? 0 : 8);
        }

        public void a(Drawable drawable) {
            this.b.setImageDrawable(drawable);
        }

        public void b(Drawable drawable) {
            ImageView imageView = this.a;
            if (drawable == null) {
                drawable = this.e;
            }
            imageView.setImageDrawable(drawable);
            this.d = 0;
        }

        public void a(int i) {
            this.d = i;
            this.a.setImageDrawable(i != 0 ? getResources().getDrawable(i) : this.e);
        }

        /* access modifiers changed from: protected */
        public void onConfigurationChanged(Configuration configuration) {
            super.onConfigurationChanged(configuration);
            if (this.d != 0) {
                a(this.d);
            }
        }

        public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            CharSequence contentDescription = getContentDescription();
            if (TextUtils.isEmpty(contentDescription)) {
                return true;
            }
            accessibilityEvent.getText().add(contentDescription);
            return true;
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            this.a = (ImageView) findViewById(f.up);
            this.b = (ImageView) findViewById(f.home);
            this.e = this.a.getDrawable();
        }

        public int a() {
            if (this.a.getVisibility() == 8) {
                return this.c;
            }
            return 0;
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            measureChildWithMargins(this.a, i, 0, i2, 0);
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.a.getLayoutParams();
            this.c = layoutParams.leftMargin + this.a.getMeasuredWidth() + layoutParams.rightMargin;
            int i3 = this.a.getVisibility() == 8 ? 0 : this.c;
            int measuredHeight = layoutParams.topMargin + this.a.getMeasuredHeight() + layoutParams.bottomMargin;
            measureChildWithMargins(this.b, i, i3, i2, 0);
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) this.b.getLayoutParams();
            int measuredWidth = i3 + layoutParams2.leftMargin + this.b.getMeasuredWidth() + layoutParams2.rightMargin;
            int max = Math.max(measuredHeight, layoutParams2.bottomMargin + layoutParams2.topMargin + this.b.getMeasuredHeight());
            int mode = View.MeasureSpec.getMode(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            switch (mode) {
                case Integer.MIN_VALUE:
                    size = Math.min(measuredWidth, size);
                    break;
                case 1073741824:
                    break;
                default:
                    size = measuredWidth;
                    break;
            }
            switch (mode2) {
                case Integer.MIN_VALUE:
                    size2 = Math.min(max, size2);
                    break;
                case 1073741824:
                    break;
                default:
                    size2 = max;
                    break;
            }
            setMeasuredDimension(size, size2);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            int i5 = 0;
            int i6 = (i4 - i2) / 2;
            int i7 = i3 - i;
            if (this.a.getVisibility() != 8) {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.a.getLayoutParams();
                int measuredHeight = this.a.getMeasuredHeight();
                int measuredWidth = this.a.getMeasuredWidth();
                int i8 = i6 - (measuredHeight / 2);
                this.a.layout(0, i8, measuredWidth, measuredHeight + i8);
                int i9 = layoutParams.rightMargin + layoutParams.leftMargin + measuredWidth;
                int i10 = i7 - i9;
                i += i9;
                i5 = i9;
            }
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) this.b.getLayoutParams();
            int measuredHeight2 = this.b.getMeasuredHeight();
            int measuredWidth2 = this.b.getMeasuredWidth();
            int max = i5 + Math.max(layoutParams2.leftMargin, ((i3 - i) / 2) - (measuredWidth2 / 2));
            int max2 = Math.max(layoutParams2.topMargin, i6 - (measuredHeight2 / 2));
            this.b.layout(max, max2, measuredWidth2 + max, measuredHeight2 + max2);
        }
    }
}
