package android.support.v7.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.internal.widget.ActionBarView;

/* compiled from: MyApp */
final class k implements Parcelable.Creator {
    k() {
    }

    /* renamed from: a */
    public ActionBarView.SavedState createFromParcel(Parcel parcel) {
        return new ActionBarView.SavedState(parcel, null);
    }

    /* renamed from: a */
    public ActionBarView.SavedState[] newArray(int i) {
        return new ActionBarView.SavedState[i];
    }
}
