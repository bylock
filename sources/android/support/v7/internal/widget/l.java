package android.support.v7.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Adapter;

/* compiled from: MyApp */
abstract class l extends ViewGroup {
    int A;
    int B = -1;
    long C = Long.MIN_VALUE;
    boolean D = false;
    private int a;
    private View b;
    private boolean c;
    private boolean d;
    private r e;
    @ViewDebug.ExportedProperty(category = "scrolling")
    int k = 0;
    int l;
    int m;
    long n = Long.MIN_VALUE;
    long o;
    boolean p = false;
    int q;
    boolean r = false;
    q s;
    o t;
    boolean u;
    @ViewDebug.ExportedProperty(category = "list")
    int v = -1;
    long w = Long.MIN_VALUE;
    @ViewDebug.ExportedProperty(category = "list")
    int x = -1;
    long y = Long.MIN_VALUE;
    @ViewDebug.ExportedProperty(category = "list")
    int z;

    public abstract View c();

    public abstract Adapter e();

    l(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void a(o oVar) {
        this.t = oVar;
    }

    public boolean a(View view, int i, long j) {
        if (this.t == null) {
            return false;
        }
        playSoundEffect(0);
        if (view != null) {
            view.sendAccessibilityEvent(1);
        }
        this.t.a(this, view, i, j);
        return true;
    }

    public void a(q qVar) {
        this.s = qVar;
    }

    public void addView(View view) {
        throw new UnsupportedOperationException("addView(View) is not supported in AdapterView");
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i) {
        throw new UnsupportedOperationException("addView(View, int) is not supported in AdapterView");
    }

    @Override // android.view.ViewGroup
    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("addView(View, LayoutParams) is not supported in AdapterView");
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("addView(View, int, LayoutParams) is not supported in AdapterView");
    }

    public void removeView(View view) {
        throw new UnsupportedOperationException("removeView(View) is not supported in AdapterView");
    }

    public void removeViewAt(int i) {
        throw new UnsupportedOperationException("removeViewAt(int) is not supported in AdapterView");
    }

    public void removeAllViews() {
        throw new UnsupportedOperationException("removeAllViews() is not supported in AdapterView");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        this.a = getHeight();
    }

    @ViewDebug.CapturedViewProperty
    public int f() {
        return this.v;
    }

    @ViewDebug.CapturedViewProperty
    public long g() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return false;
    }

    @Override // android.view.View
    public void setFocusable(boolean z2) {
        boolean z3 = true;
        Adapter e2 = e();
        boolean z4 = e2 == null || e2.getCount() == 0;
        this.c = z2;
        if (!z2) {
            this.d = false;
        }
        if (!z2 || (z4 && !h())) {
            z3 = false;
        }
        super.setFocusable(z3);
    }

    public void setFocusableInTouchMode(boolean z2) {
        boolean z3 = true;
        Adapter e2 = e();
        boolean z4 = e2 == null || e2.getCount() == 0;
        this.d = z2;
        if (z2) {
            this.c = true;
        }
        if (!z2 || (z4 && !h())) {
            z3 = false;
        }
        super.setFocusableInTouchMode(z3);
    }

    /* access modifiers changed from: package-private */
    public void i() {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = false;
        Adapter e2 = e();
        if (!(e2 == null || e2.getCount() == 0) || h()) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!z2 || !this.d) {
            z3 = false;
        } else {
            z3 = true;
        }
        super.setFocusableInTouchMode(z3);
        if (!z2 || !this.c) {
            z4 = false;
        } else {
            z4 = true;
        }
        super.setFocusable(z4);
        if (this.b != null) {
            if (e2 == null || e2.isEmpty()) {
                z5 = true;
            }
            a(z5);
        }
    }

    private void a(boolean z2) {
        if (h()) {
            z2 = false;
        }
        if (z2) {
            if (this.b != null) {
                this.b.setVisibility(0);
                setVisibility(8);
            } else {
                setVisibility(0);
            }
            if (this.u) {
                onLayout(false, getLeft(), getTop(), getRight(), getBottom());
                return;
            }
            return;
        }
        if (this.b != null) {
            this.b.setVisibility(8);
        }
        setVisibility(0);
    }

    public long b(int i) {
        Adapter e2 = e();
        if (e2 == null || i < 0) {
            return Long.MIN_VALUE;
        }
        return e2.getItemId(i);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new RuntimeException("Don't call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead");
    }

    /* access modifiers changed from: protected */
    @Override // android.view.View, android.view.ViewGroup
    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    @Override // android.view.View, android.view.ViewGroup
    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.e);
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (this.s != null) {
            if (this.r || this.D) {
                if (this.e == null) {
                    this.e = new r(this);
                }
                post(this.e);
            } else {
                a();
            }
        }
        if (this.x != -1 && isShown() && !isInTouchMode()) {
            sendAccessibilityEvent(4);
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.s != null) {
            int f = f();
            if (f >= 0) {
                this.s.a(this, c(), f, e().getItemId(f));
                return;
            }
            this.s.a(this);
        }
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        View c2 = c();
        if (c2 == null || c2.getVisibility() != 0 || !c2.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean canAnimate() {
        return super.canAnimate() && this.z > 0;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void k() {
        /*
            r8 = this;
            r6 = -9223372036854775808
            r5 = -1
            r2 = 1
            r1 = 0
            int r4 = r8.z
            if (r4 <= 0) goto L_0x0055
            boolean r0 = r8.p
            if (r0 == 0) goto L_0x0053
            r8.p = r1
            int r0 = r8.m()
            if (r0 < 0) goto L_0x0053
            int r3 = r8.a(r0, r2)
            if (r3 != r0) goto L_0x0053
            r8.d(r0)
            r3 = r2
        L_0x001f:
            if (r3 != 0) goto L_0x004f
            int r0 = r8.f()
            if (r0 < r4) goto L_0x0029
            int r0 = r4 + -1
        L_0x0029:
            if (r0 >= 0) goto L_0x002c
            r0 = r1
        L_0x002c:
            int r4 = r8.a(r0, r2)
            if (r4 >= 0) goto L_0x0051
            int r0 = r8.a(r0, r1)
        L_0x0036:
            if (r0 < 0) goto L_0x004f
            r8.d(r0)
            r8.l()
            r0 = r2
        L_0x003f:
            if (r0 != 0) goto L_0x004e
            r8.x = r5
            r8.y = r6
            r8.v = r5
            r8.w = r6
            r8.p = r1
            r8.l()
        L_0x004e:
            return
        L_0x004f:
            r0 = r3
            goto L_0x003f
        L_0x0051:
            r0 = r4
            goto L_0x0036
        L_0x0053:
            r3 = r1
            goto L_0x001f
        L_0x0055:
            r0 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.l.k():void");
    }

    /* access modifiers changed from: package-private */
    public void l() {
        if (this.x != this.B || this.y != this.C) {
            j();
            this.B = this.x;
            this.C = this.y;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 140
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:72)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.visit(BlockProcessor.java:46)
        */
    int m() {
        /*
        // Method dump skipped, instructions count: 109
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.l.m():int");
    }

    /* access modifiers changed from: package-private */
    public int a(int i, boolean z2) {
        return i;
    }

    /* access modifiers changed from: package-private */
    public void c(int i) {
        this.x = i;
        this.y = b(i);
    }

    /* access modifiers changed from: package-private */
    public void d(int i) {
        this.v = i;
        this.w = b(i);
        if (this.p && this.q == 0 && i >= 0) {
            this.m = i;
            this.n = this.w;
        }
    }

    /* access modifiers changed from: package-private */
    public void n() {
        if (getChildCount() > 0) {
            this.p = true;
            this.o = (long) this.a;
            if (this.x >= 0) {
                View childAt = getChildAt(this.x - this.k);
                this.n = this.w;
                this.m = this.v;
                if (childAt != null) {
                    this.l = childAt.getTop();
                }
                this.q = 0;
                return;
            }
            View childAt2 = getChildAt(0);
            Adapter e2 = e();
            if (this.k < 0 || this.k >= e2.getCount()) {
                this.n = -1;
            } else {
                this.n = e2.getItemId(this.k);
            }
            this.m = this.k;
            if (childAt2 != null) {
                this.l = childAt2.getTop();
            }
            this.q = 1;
        }
    }
}
