package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;

/* compiled from: MyApp */
public class ProgressBarICS extends View {
    private static final int[] f = {16843062, 16843063, 16843064, 16843065, 16843066, 16843067, 16843068, 16843069, 16843070, 16843071, 16843039, 16843072, 16843040, 16843073};
    int a;
    int b;
    int c;
    int d;
    Bitmap e;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private boolean l;
    private boolean m;
    private Transformation n;
    private AlphaAnimation o;
    private Drawable p;
    private Drawable q;
    private Drawable r;
    private boolean s;
    private Interpolator t;
    private af u;
    private long v = Thread.currentThread().getId();
    private boolean w;
    private long x;
    private boolean y;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressBarICS(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2);
        boolean z = false;
        c();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f, i2, i3);
        this.s = true;
        setMax(obtainStyledAttributes.getInt(0, this.i));
        setProgress(obtainStyledAttributes.getInt(1, this.g));
        setSecondaryProgress(obtainStyledAttributes.getInt(2, this.h));
        boolean z2 = obtainStyledAttributes.getBoolean(3, this.l);
        this.m = obtainStyledAttributes.getBoolean(4, this.m);
        Drawable drawable = obtainStyledAttributes.getDrawable(5);
        if (drawable != null) {
            setIndeterminateDrawable(a(drawable));
        }
        Drawable drawable2 = obtainStyledAttributes.getDrawable(6);
        if (drawable2 != null) {
            setProgressDrawable(a(drawable2, false));
        }
        this.k = obtainStyledAttributes.getInt(7, this.k);
        this.j = obtainStyledAttributes.getInt(8, this.j);
        this.a = obtainStyledAttributes.getDimensionPixelSize(9, this.a);
        this.b = obtainStyledAttributes.getDimensionPixelSize(10, this.b);
        this.c = obtainStyledAttributes.getDimensionPixelSize(11, this.c);
        this.d = obtainStyledAttributes.getDimensionPixelSize(12, this.d);
        int resourceId = obtainStyledAttributes.getResourceId(13, 17432587);
        if (resourceId > 0) {
            a(context, resourceId);
        }
        obtainStyledAttributes.recycle();
        this.s = false;
        setIndeterminate((this.m || z2) ? true : z);
    }

    private Drawable a(Drawable drawable, boolean z) {
        if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            int numberOfLayers = layerDrawable.getNumberOfLayers();
            Drawable[] drawableArr = new Drawable[numberOfLayers];
            for (int i2 = 0; i2 < numberOfLayers; i2++) {
                int id = layerDrawable.getId(i2);
                drawableArr[i2] = a(layerDrawable.getDrawable(i2), id == 16908301 || id == 16908303);
            }
            LayerDrawable layerDrawable2 = new LayerDrawable(drawableArr);
            for (int i3 = 0; i3 < numberOfLayers; i3++) {
                layerDrawable2.setId(i3, layerDrawable.getId(i3));
            }
            return layerDrawable2;
        } else if (!(drawable instanceof BitmapDrawable)) {
            return drawable;
        } else {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            if (this.e == null) {
                this.e = bitmap;
            }
            ShapeDrawable shapeDrawable = new ShapeDrawable(getDrawableShape());
            shapeDrawable.getPaint().setShader(new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.CLAMP));
            if (z) {
                return new ClipDrawable(shapeDrawable, 3, 1);
            }
            return shapeDrawable;
        }
    }

    /* access modifiers changed from: package-private */
    public Shape getDrawableShape() {
        return new RoundRectShape(new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, null, null);
    }

    private Drawable a(Drawable drawable) {
        if (!(drawable instanceof AnimationDrawable)) {
            return drawable;
        }
        AnimationDrawable animationDrawable = (AnimationDrawable) drawable;
        int numberOfFrames = animationDrawable.getNumberOfFrames();
        AnimationDrawable animationDrawable2 = new AnimationDrawable();
        animationDrawable2.setOneShot(animationDrawable.isOneShot());
        for (int i2 = 0; i2 < numberOfFrames; i2++) {
            Drawable a2 = a(animationDrawable.getFrame(i2), true);
            a2.setLevel(10000);
            animationDrawable2.addFrame(a2, animationDrawable.getDuration(i2));
        }
        animationDrawable2.setLevel(10000);
        return animationDrawable2;
    }

    private void c() {
        this.i = 100;
        this.g = 0;
        this.h = 0;
        this.l = false;
        this.m = false;
        this.k = 4000;
        this.j = 1;
        this.a = 24;
        this.b = 48;
        this.c = 24;
        this.d = 48;
    }

    public synchronized void setIndeterminate(boolean z) {
        if ((!this.m || !this.l) && z != this.l) {
            this.l = z;
            if (z) {
                this.r = this.p;
                a();
            } else {
                this.r = this.q;
                b();
            }
        }
    }

    public Drawable getIndeterminateDrawable() {
        return this.p;
    }

    public void setIndeterminateDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(this);
        }
        this.p = drawable;
        if (this.l) {
            this.r = drawable;
            postInvalidate();
        }
    }

    public Drawable getProgressDrawable() {
        return this.q;
    }

    public void setProgressDrawable(Drawable drawable) {
        boolean z;
        if (this.q == null || drawable == this.q) {
            z = false;
        } else {
            this.q.setCallback(null);
            z = true;
        }
        if (drawable != null) {
            drawable.setCallback(this);
            int minimumHeight = drawable.getMinimumHeight();
            if (this.d < minimumHeight) {
                this.d = minimumHeight;
                requestLayout();
            }
        }
        this.q = drawable;
        if (!this.l) {
            this.r = drawable;
            postInvalidate();
        }
        if (z) {
            a(getWidth(), getHeight());
            d();
            a(16908301, this.g, false, false);
            a(16908303, this.h, false, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return drawable == this.q || drawable == this.p || super.verifyDrawable(drawable);
    }

    public void postInvalidate() {
        if (!this.s) {
            super.postInvalidate();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(int i2, int i3, boolean z, boolean z2) {
        float f2;
        if (this.i > 0) {
            f2 = ((float) i3) / ((float) this.i);
        } else {
            f2 = 0.0f;
        }
        Drawable drawable = this.r;
        if (drawable != null) {
            Drawable drawable2 = null;
            if (drawable instanceof LayerDrawable) {
                drawable2 = ((LayerDrawable) drawable).findDrawableByLayerId(i2);
            }
            int i4 = (int) (f2 * 10000.0f);
            if (drawable2 != null) {
                drawable = drawable2;
            }
            drawable.setLevel(i4);
        } else {
            invalidate();
        }
    }

    private synchronized void a(int i2, int i3, boolean z) {
        af afVar;
        if (this.v == Thread.currentThread().getId()) {
            a(i2, i3, z, true);
        } else {
            if (this.u != null) {
                afVar = this.u;
                this.u = null;
                afVar.a(i2, i3, z);
            } else {
                afVar = new af(this, i2, i3, z);
            }
            post(afVar);
        }
    }

    public synchronized void setProgress(int i2) {
        a(i2, false);
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(int i2, boolean z) {
        int i3;
        if (!this.l) {
            if (i2 < 0) {
                i3 = 0;
            } else {
                i3 = i2;
            }
            if (i3 > this.i) {
                i3 = this.i;
            }
            if (i3 != this.g) {
                this.g = i3;
                a(16908301, this.g, z);
            }
        }
    }

    public synchronized void setSecondaryProgress(int i2) {
        int i3 = 0;
        synchronized (this) {
            if (!this.l) {
                if (i2 >= 0) {
                    i3 = i2;
                }
                if (i3 > this.i) {
                    i3 = this.i;
                }
                if (i3 != this.h) {
                    this.h = i3;
                    a(16908303, this.h, false);
                }
            }
        }
    }

    public synchronized int getProgress() {
        return this.l ? 0 : this.g;
    }

    public synchronized int getSecondaryProgress() {
        return this.l ? 0 : this.h;
    }

    public synchronized int getMax() {
        return this.i;
    }

    public synchronized void setMax(int i2) {
        if (i2 < 0) {
            i2 = 0;
        }
        if (i2 != this.i) {
            this.i = i2;
            postInvalidate();
            if (this.g > i2) {
                this.g = i2;
            }
            a(16908301, this.g, false);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (getVisibility() == 0) {
            if (this.p instanceof Animatable) {
                this.w = true;
                this.o = null;
            } else {
                if (this.t == null) {
                    this.t = new LinearInterpolator();
                }
                this.n = new Transformation();
                this.o = new AlphaAnimation(0.0f, 1.0f);
                this.o.setRepeatMode(this.j);
                this.o.setRepeatCount(-1);
                this.o.setDuration((long) this.k);
                this.o.setInterpolator(this.t);
                this.o.setStartTime(-1);
            }
            postInvalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.o = null;
        this.n = null;
        if (this.p instanceof Animatable) {
            ((Animatable) this.p).stop();
            this.w = false;
        }
        postInvalidate();
    }

    public void a(Context context, int i2) {
        setInterpolator(AnimationUtils.loadInterpolator(context, i2));
    }

    public void setInterpolator(Interpolator interpolator) {
        this.t = interpolator;
    }

    public Interpolator getInterpolator() {
        return this.t;
    }

    public void setVisibility(int i2) {
        if (getVisibility() != i2) {
            super.setVisibility(i2);
            if (!this.l) {
                return;
            }
            if (i2 == 8 || i2 == 4) {
                b();
            } else {
                a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i2) {
        super.onVisibilityChanged(view, i2);
        if (!this.l) {
            return;
        }
        if (i2 == 8 || i2 == 4) {
            b();
        } else {
            a();
        }
    }

    public void invalidateDrawable(Drawable drawable) {
        if (this.y) {
            return;
        }
        if (verifyDrawable(drawable)) {
            Rect bounds = drawable.getBounds();
            int scrollX = getScrollX() + getPaddingLeft();
            int scrollY = getScrollY() + getPaddingTop();
            invalidate(bounds.left + scrollX, bounds.top + scrollY, scrollX + bounds.right, bounds.bottom + scrollY);
            return;
        }
        super.invalidateDrawable(drawable);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        a(i2, i3);
    }

    private void a(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int paddingRight = (i2 - getPaddingRight()) - getPaddingLeft();
        int paddingBottom = (i3 - getPaddingBottom()) - getPaddingTop();
        if (this.p != null) {
            if (this.m && !(this.p instanceof AnimationDrawable)) {
                float intrinsicWidth = ((float) this.p.getIntrinsicWidth()) / ((float) this.p.getIntrinsicHeight());
                float f2 = ((float) i2) / ((float) i3);
                if (intrinsicWidth != f2) {
                    if (f2 > intrinsicWidth) {
                        int i8 = (int) (intrinsicWidth * ((float) i3));
                        i6 = (i2 - i8) / 2;
                        i4 = i8 + i6;
                        i5 = paddingBottom;
                        i7 = 0;
                    } else {
                        int i9 = (int) ((1.0f / intrinsicWidth) * ((float) i2));
                        int i10 = (i3 - i9) / 2;
                        i4 = paddingRight;
                        i5 = i9 + i10;
                        i7 = i10;
                        i6 = 0;
                    }
                    this.p.setBounds(i6, i7, i4, i5);
                }
            }
            i6 = 0;
            i4 = paddingRight;
            i5 = paddingBottom;
            i7 = 0;
            this.p.setBounds(i6, i7, i4, i5);
        } else {
            i4 = paddingRight;
            i5 = paddingBottom;
        }
        if (this.q != null) {
            this.q.setBounds(0, 0, i4, i5);
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Drawable drawable = this.r;
        if (drawable != null) {
            canvas.save();
            canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
            long drawingTime = getDrawingTime();
            if (this.o != null) {
                this.o.getTransformation(drawingTime, this.n);
                float alpha = this.n.getAlpha();
                try {
                    this.y = true;
                    drawable.setLevel((int) (alpha * 10000.0f));
                    this.y = false;
                    if (SystemClock.uptimeMillis() - this.x >= 200) {
                        this.x = SystemClock.uptimeMillis();
                        postInvalidateDelayed(200);
                    }
                } catch (Throwable th) {
                    this.y = false;
                    throw th;
                }
            }
            drawable.draw(canvas);
            canvas.restore();
            if (this.w && (drawable instanceof Animatable)) {
                ((Animatable) drawable).start();
                this.w = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int i2, int i3) {
        int i4;
        int i5 = 0;
        synchronized (this) {
            Drawable drawable = this.r;
            if (drawable != null) {
                i4 = Math.max(this.a, Math.min(this.b, drawable.getIntrinsicWidth()));
                i5 = Math.max(this.c, Math.min(this.d, drawable.getIntrinsicHeight()));
            } else {
                i4 = 0;
            }
            d();
            setMeasuredDimension(resolveSize(i4 + getPaddingLeft() + getPaddingRight(), i2), resolveSize(i5 + getPaddingTop() + getPaddingBottom(), i3));
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        d();
    }

    private void d() {
        int[] drawableState = getDrawableState();
        if (this.q != null && this.q.isStateful()) {
            this.q.setState(drawableState);
        }
        if (this.p != null && this.p.isStateful()) {
            this.p.setState(drawableState);
        }
    }

    /* compiled from: MyApp */
    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new ag();
        int a;
        int b;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readInt();
            this.b = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
            parcel.writeInt(this.b);
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = this.g;
        savedState.b = this.h;
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setProgress(savedState.a);
        setSecondaryProgress(savedState.b);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.l) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.l) {
            b();
        }
        if (this.u != null) {
            removeCallbacks(this.u);
        }
        super.onDetachedFromWindow();
    }
}
