package android.support.v7.internal.widget;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ListAdapter;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class am implements DialogInterface.OnClickListener, aq {
    final /* synthetic */ ak a;
    private AlertDialog b;
    private ListAdapter c;
    private CharSequence d;

    private am(ak akVar) {
        this.a = akVar;
    }

    @Override // android.support.v7.internal.widget.aq
    public void d() {
        this.b.dismiss();
        this.b = null;
    }

    @Override // android.support.v7.internal.widget.aq
    public boolean f() {
        if (this.b != null) {
            return this.b.isShowing();
        }
        return false;
    }

    @Override // android.support.v7.internal.widget.aq
    public void a(ListAdapter listAdapter) {
        this.c = listAdapter;
    }

    @Override // android.support.v7.internal.widget.aq
    public void a(CharSequence charSequence) {
        this.d = charSequence;
    }

    @Override // android.support.v7.internal.widget.aq
    public void c() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.getContext());
        if (this.d != null) {
            builder.setTitle(this.d);
        }
        this.b = builder.setSingleChoiceItems(this.c, this.a.f(), this).show();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a(i);
        if (this.a.t != null) {
            this.a.a((View) null, i, this.c.getItemId(i));
        }
        d();
    }
}
