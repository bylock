package android.support.v7.internal.widget;

import android.util.SparseArray;
import android.view.View;

/* compiled from: MyApp */
class d {
    final /* synthetic */ AbsSpinnerICS a;
    private final SparseArray b = new SparseArray();

    d(AbsSpinnerICS absSpinnerICS) {
        this.a = absSpinnerICS;
    }

    public void a(int i, View view) {
        this.b.put(i, view);
    }

    /* access modifiers changed from: package-private */
    public View a(int i) {
        View view = (View) this.b.get(i);
        if (view != null) {
            this.b.delete(i);
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        SparseArray sparseArray = this.b;
        int size = sparseArray.size();
        for (int i = 0; i < size; i++) {
            View view = (View) sparseArray.valueAt(i);
            if (view != null) {
                AbsSpinnerICS.a(this.a, view, true);
            }
        }
        sparseArray.clear();
    }
}
