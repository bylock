package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.b.f;
import android.support.v7.b.k;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/* compiled from: MyApp */
public class ActionBarContainer extends FrameLayout {
    private boolean a;
    private View b;
    private ActionBarView c;
    private Drawable d;
    private Drawable e;
    private Drawable f;
    private boolean g;
    private boolean h;

    public ActionBarContainer(Context context) {
        this(context, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActionBarContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        boolean z = true;
        setBackgroundDrawable(null);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, k.ActionBar);
        this.d = obtainStyledAttributes.getDrawable(10);
        this.e = obtainStyledAttributes.getDrawable(11);
        if (getId() == f.split_action_bar) {
            this.g = true;
            this.f = obtainStyledAttributes.getDrawable(12);
        }
        obtainStyledAttributes.recycle();
        if (this.g) {
            if (this.f != null) {
                z = false;
            }
        } else if (!(this.d == null && this.e == null)) {
            z = false;
        }
        setWillNotDraw(z);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (ActionBarView) findViewById(f.action_bar);
    }

    public void setPrimaryBackground(Drawable drawable) {
        boolean z = true;
        if (this.d != null) {
            this.d.setCallback(null);
            unscheduleDrawable(this.d);
        }
        this.d = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.c != null) {
                this.d.setBounds(this.c.getLeft(), this.c.getTop(), this.c.getRight(), this.c.getBottom());
            }
        }
        if (this.g) {
            if (this.f != null) {
                z = false;
            }
        } else if (!(this.d == null && this.e == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setStackedBackground(Drawable drawable) {
        boolean z = true;
        if (this.e != null) {
            this.e.setCallback(null);
            unscheduleDrawable(this.e);
        }
        this.e = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.h && this.e != null) {
                this.e.setBounds(this.b.getLeft(), this.b.getTop(), this.b.getRight(), this.b.getBottom());
            }
        }
        if (this.g) {
            if (this.f != null) {
                z = false;
            }
        } else if (!(this.d == null && this.e == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setSplitBackground(Drawable drawable) {
        boolean z = true;
        if (this.f != null) {
            this.f.setCallback(null);
            unscheduleDrawable(this.f);
        }
        this.f = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            if (this.g && this.f != null) {
                this.f.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            }
        }
        if (this.g) {
            if (this.f != null) {
                z = false;
            }
        } else if (!(this.d == null && this.e == null)) {
            z = false;
        }
        setWillNotDraw(z);
        invalidate();
    }

    public void setVisibility(int i) {
        boolean z;
        super.setVisibility(i);
        if (i == 0) {
            z = true;
        } else {
            z = false;
        }
        if (this.d != null) {
            this.d.setVisible(z, false);
        }
        if (this.e != null) {
            this.e.setVisible(z, false);
        }
        if (this.f != null) {
            this.f.setVisible(z, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return (drawable == this.d && !this.g) || (drawable == this.e && this.h) || ((drawable == this.f && this.g) || super.verifyDrawable(drawable));
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.d != null && this.d.isStateful()) {
            this.d.setState(getDrawableState());
        }
        if (this.e != null && this.e.isStateful()) {
            this.e.setState(getDrawableState());
        }
        if (this.f != null && this.f.isStateful()) {
            this.f.setState(getDrawableState());
        }
    }

    public void setTransitioning(boolean z) {
        this.a = z;
        setDescendantFocusability(z ? 393216 : 262144);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.a || super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        return true;
    }

    public void setTabContainer(ScrollingTabContainerView scrollingTabContainerView) {
        if (this.b != null) {
            removeView(this.b);
        }
        this.b = scrollingTabContainerView;
        if (scrollingTabContainerView != null) {
            addView(scrollingTabContainerView);
            ViewGroup.LayoutParams layoutParams = scrollingTabContainerView.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = -2;
            scrollingTabContainerView.setAllowCollapse(false);
        }
    }

    public View getTabContainer() {
        return this.b;
    }

    public void onDraw(Canvas canvas) {
        if (getWidth() != 0 && getHeight() != 0) {
            if (!this.g) {
                if (this.d != null) {
                    a(this.d, canvas);
                }
                if (this.e != null && this.h) {
                    a(this.e, canvas);
                }
            } else if (this.f != null) {
                a(this.f, canvas);
            }
        }
    }

    public void onMeasure(int i, int i2) {
        int measuredHeight;
        super.onMeasure(i, i2);
        if (this.c != null) {
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.c.getLayoutParams();
            if (this.c.m()) {
                measuredHeight = 0;
            } else {
                measuredHeight = layoutParams.bottomMargin + this.c.getMeasuredHeight() + layoutParams.topMargin;
            }
            if (this.b != null && this.b.getVisibility() != 8 && View.MeasureSpec.getMode(i2) == Integer.MIN_VALUE) {
                setMeasuredDimension(getMeasuredWidth(), Math.min(measuredHeight + this.b.getMeasuredHeight(), View.MeasureSpec.getSize(i2)));
            }
        }
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        boolean z2;
        boolean z3 = true;
        boolean z4 = false;
        super.onLayout(z, i, i2, i3, i4);
        boolean z5 = (this.b == null || this.b.getVisibility() == 8) ? false : true;
        if (!(this.b == null || this.b.getVisibility() == 8)) {
            int measuredHeight = getMeasuredHeight();
            int measuredHeight2 = this.b.getMeasuredHeight();
            if ((this.c.getDisplayOptions() & 2) == 0) {
                int childCount = getChildCount();
                for (int i5 = 0; i5 < childCount; i5++) {
                    View childAt = getChildAt(i5);
                    if (childAt != this.b && !this.c.m()) {
                        childAt.offsetTopAndBottom(measuredHeight2);
                    }
                }
                this.b.layout(i, 0, i3, measuredHeight2);
            } else {
                this.b.layout(i, measuredHeight - measuredHeight2, i3, measuredHeight);
            }
        }
        if (!this.g) {
            if (this.d != null) {
                this.d.setBounds(this.c.getLeft(), this.c.getTop(), this.c.getRight(), this.c.getBottom());
                z2 = true;
            } else {
                z2 = false;
            }
            if (z5 && this.e != null) {
                z4 = true;
            }
            this.h = z4;
            if (z4) {
                this.e.setBounds(this.b.getLeft(), this.b.getTop(), this.b.getRight(), this.b.getBottom());
            } else {
                z3 = z2;
            }
        } else if (this.f != null) {
            this.f.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
        } else {
            z3 = false;
        }
        if (z3) {
            invalidate();
        }
    }

    private void a(Drawable drawable, Canvas canvas) {
        Rect bounds = drawable.getBounds();
        if (!(drawable instanceof ColorDrawable) || bounds.isEmpty() || Build.VERSION.SDK_INT >= 11) {
            drawable.draw(canvas);
            return;
        }
        canvas.save();
        canvas.clipRect(bounds);
        drawable.draw(canvas);
        canvas.restore();
    }
}
