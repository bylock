package android.support.v7.internal.widget;

import android.view.MotionEvent;
import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ab implements View.OnTouchListener {
    final /* synthetic */ u a;

    private ab(u uVar) {
        this.a = uVar;
    }

    /* synthetic */ ab(u uVar, v vVar) {
        this(uVar);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (action == 0 && this.a.c != null && this.a.c.isShowing() && x >= 0 && x < this.a.c.getWidth() && y >= 0 && y < this.a.c.getHeight()) {
            this.a.y.postDelayed(this.a.t, 250);
            return false;
        } else if (action != 1) {
            return false;
        } else {
            this.a.y.removeCallbacks(this.a.t);
            return false;
        }
    }
}
