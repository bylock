package android.support.v7.internal.widget;

import android.support.v7.internal.widget.ScrollingTabContainerView;
import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class aj implements View.OnClickListener {
    final /* synthetic */ ScrollingTabContainerView a;

    private aj(ScrollingTabContainerView scrollingTabContainerView) {
        this.a = scrollingTabContainerView;
    }

    /* synthetic */ aj(ScrollingTabContainerView scrollingTabContainerView, ah ahVar) {
        this(scrollingTabContainerView);
    }

    public void onClick(View view) {
        ((ScrollingTabContainerView.TabView) view).getTab().d();
        int childCount = this.a.e.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.a.e.getChildAt(i);
            childAt.setSelected(childAt == view);
        }
    }
}
