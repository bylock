package android.support.v7.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.View;
import android.widget.LinearLayout;

/* compiled from: MyApp */
public class NativeActionModeAwareLayout extends LinearLayout {
    private ad a;

    public NativeActionModeAwareLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setActionModeForChildListener(ad adVar) {
        this.a = adVar;
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        if (this.a != null) {
            callback = this.a.a(callback);
        }
        return super.startActionModeForChild(view, callback);
    }
}
