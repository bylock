package android.support.v7.internal.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.b.c;
import android.support.v7.b.d;
import android.support.v7.b.e;
import android.support.v7.b.g;
import android.support.v7.b.k;

/* compiled from: MyApp */
public class a {
    private Context a;

    public static a a(Context context) {
        return new a(context);
    }

    private a(Context context) {
        this.a = context;
    }

    public int a() {
        return this.a.getResources().getInteger(g.abc_max_action_buttons);
    }

    public boolean b() {
        return Build.VERSION.SDK_INT >= 11;
    }

    public int c() {
        return this.a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    public boolean d() {
        return this.a.getResources().getBoolean(d.abc_action_bar_embed_tabs_pre_jb);
    }

    public int e() {
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(null, k.ActionBar, c.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(1, 0);
        Resources resources = this.a.getResources();
        if (!d()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(e.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    public boolean f() {
        return this.a.getApplicationInfo().targetSdkVersion < 14;
    }

    public int g() {
        return this.a.getResources().getDimensionPixelSize(e.abc_action_bar_stacked_tab_max_width);
    }
}
