package android.support.v7.internal.view;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.support.v4.b.a.a;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: MyApp */
public class f extends MenuInflater {
    private static final Class[] a = {Context.class};
    private static final Class[] b = a;
    private final Object[] c;
    private final Object[] d = this.c;
    private Context e;
    private Object f;

    public f(Context context) {
        super(context);
        this.e = context;
        this.f = context;
        this.c = new Object[]{context};
    }

    public void inflate(int i, Menu menu) {
        if (!(menu instanceof a)) {
            super.inflate(i, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.e.getResources().getLayout(i);
            a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    private void a(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) {
        boolean z;
        h hVar = new h(this, menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        Object obj = null;
        boolean z2 = false;
        int i = eventType;
        boolean z3 = false;
        while (!z3) {
            switch (i) {
                case 1:
                    throw new RuntimeException("Unexpected end of document");
                case 2:
                    if (z2) {
                        z = z2;
                        continue;
                    } else {
                        String name2 = xmlPullParser.getName();
                        if (name2.equals("group")) {
                            hVar.a(attributeSet);
                            z = z2;
                        } else if (name2.equals("item")) {
                            hVar.b(attributeSet);
                            z = z2;
                        } else if (name2.equals("menu")) {
                            a(xmlPullParser, attributeSet, hVar.c());
                            z = z2;
                        } else {
                            obj = name2;
                            z = true;
                        }
                    }
                    i = xmlPullParser.next();
                    z2 = z;
                case 3:
                    String name3 = xmlPullParser.getName();
                    if (!z2 || !name3.equals(obj)) {
                        if (name3.equals("group")) {
                            hVar.a();
                            z = z2;
                        } else if (name3.equals("item")) {
                            if (!hVar.d()) {
                                if (h.a(hVar) == null || !h.a(hVar).g()) {
                                    hVar.b();
                                    z = z2;
                                } else {
                                    hVar.c();
                                    z = z2;
                                }
                            }
                        } else if (name3.equals("menu")) {
                            z3 = true;
                            z = z2;
                        }
                        i = xmlPullParser.next();
                        z2 = z;
                    } else {
                        obj = null;
                        z = false;
                        continue;
                        i = xmlPullParser.next();
                        z2 = z;
                    }
                    break;
            }
            z = z2;
            i = xmlPullParser.next();
            z2 = z;
        }
    }
}
