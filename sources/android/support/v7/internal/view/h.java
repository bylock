package android.support.v7.internal.view;

import android.content.res.TypedArray;
import android.support.v4.view.ac;
import android.support.v4.view.n;
import android.support.v7.b.k;
import android.support.v7.internal.view.menu.u;
import android.support.v7.internal.view.menu.w;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class h {
    final /* synthetic */ f a;
    private Menu b;
    private int c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    private boolean h;
    private boolean i;
    private int j;
    private int k;
    private CharSequence l;
    private CharSequence m;
    private int n;
    private char o;
    private char p;
    private int q;
    private boolean r;
    private boolean s;
    private boolean t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    private n z;

    public h(f fVar, Menu menu) {
        this.a = fVar;
        this.b = menu;
        a();
    }

    public void a() {
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = true;
        this.h = true;
    }

    public void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = this.a.e.obtainStyledAttributes(attributeSet, k.MenuGroup);
        this.c = obtainStyledAttributes.getResourceId(1, 0);
        this.d = obtainStyledAttributes.getInt(3, 0);
        this.e = obtainStyledAttributes.getInt(4, 0);
        this.f = obtainStyledAttributes.getInt(5, 0);
        this.g = obtainStyledAttributes.getBoolean(2, true);
        this.h = obtainStyledAttributes.getBoolean(0, true);
        obtainStyledAttributes.recycle();
    }

    public void b(AttributeSet attributeSet) {
        boolean z2 = true;
        TypedArray obtainStyledAttributes = this.a.e.obtainStyledAttributes(attributeSet, k.MenuItem);
        this.j = obtainStyledAttributes.getResourceId(2, 0);
        this.k = (obtainStyledAttributes.getInt(5, this.d) & -65536) | (obtainStyledAttributes.getInt(6, this.e) & 65535);
        this.l = obtainStyledAttributes.getText(7);
        this.m = obtainStyledAttributes.getText(8);
        this.n = obtainStyledAttributes.getResourceId(0, 0);
        this.o = a(obtainStyledAttributes.getString(9));
        this.p = a(obtainStyledAttributes.getString(10));
        if (obtainStyledAttributes.hasValue(11)) {
            this.q = obtainStyledAttributes.getBoolean(11, false) ? 1 : 0;
        } else {
            this.q = this.f;
        }
        this.r = obtainStyledAttributes.getBoolean(3, false);
        this.s = obtainStyledAttributes.getBoolean(4, this.g);
        this.t = obtainStyledAttributes.getBoolean(1, this.h);
        this.u = obtainStyledAttributes.getInt(13, -1);
        this.y = obtainStyledAttributes.getString(12);
        this.v = obtainStyledAttributes.getResourceId(14, 0);
        this.w = obtainStyledAttributes.getString(15);
        this.x = obtainStyledAttributes.getString(16);
        if (this.x == null) {
            z2 = false;
        }
        if (z2 && this.v == 0 && this.w == null) {
            this.z = (n) a(this.x, f.b, this.a.d);
        } else {
            if (z2) {
                Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
            }
            this.z = null;
        }
        obtainStyledAttributes.recycle();
        this.i = false;
    }

    private char a(String str) {
        if (str == null) {
            return 0;
        }
        return str.charAt(0);
    }

    private void a(MenuItem menuItem) {
        boolean z2 = true;
        menuItem.setChecked(this.r).setVisible(this.s).setEnabled(this.t).setCheckable(this.q >= 1).setTitleCondensed(this.m).setIcon(this.n).setAlphabeticShortcut(this.o).setNumericShortcut(this.p);
        if (this.u >= 0) {
            ac.a(menuItem, this.u);
        }
        if (this.y != null) {
            if (this.a.e.isRestricted()) {
                throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
            }
            menuItem.setOnMenuItemClickListener(new g(this.a.f, this.y));
        }
        if (menuItem instanceof u) {
            u uVar = (u) menuItem;
        }
        if (this.q >= 2) {
            if (menuItem instanceof u) {
                ((u) menuItem).a(true);
            } else if (menuItem instanceof w) {
                ((w) menuItem).a(true);
            }
        }
        if (this.w != null) {
            ac.a(menuItem, (View) a(this.w, f.a, this.a.c));
        } else {
            z2 = false;
        }
        if (this.v > 0) {
            if (!z2) {
                ac.b(menuItem, this.v);
            } else {
                Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
            }
        }
        if (this.z != null) {
            ac.a(menuItem, this.z);
        }
    }

    public void b() {
        this.i = true;
        a(this.b.add(this.c, this.j, this.k, this.l));
    }

    public SubMenu c() {
        this.i = true;
        SubMenu addSubMenu = this.b.addSubMenu(this.c, this.j, this.k, this.l);
        a(addSubMenu.getItem());
        return addSubMenu;
    }

    public boolean d() {
        return this.i;
    }

    private Object a(String str, Class[] clsArr, Object[] objArr) {
        try {
            return this.a.e.getClassLoader().loadClass(str).getConstructor(clsArr).newInstance(objArr);
        } catch (Exception e2) {
            Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
            return null;
        }
    }
}
