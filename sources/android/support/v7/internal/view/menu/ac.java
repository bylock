package android.support.v7.internal.view.menu;

import android.support.v4.view.n;
import android.support.v4.view.p;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;

/* compiled from: MyApp */
class ac extends x implements p {
    ActionProvider.VisibilityListener c;
    final /* synthetic */ ab d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ac(ab abVar, n nVar) {
        super(abVar, nVar);
        this.d = abVar;
    }

    public View onCreateActionView(MenuItem menuItem) {
        return this.a.a(menuItem);
    }

    public boolean overridesItemVisibility() {
        return this.a.c();
    }

    public boolean isVisible() {
        return this.a.d();
    }

    public void refreshVisibility() {
        this.a.e();
    }

    public void setVisibilityListener(ActionProvider.VisibilityListener visibilityListener) {
        this.c = visibilityListener;
        n nVar = this.a;
        if (visibilityListener == null) {
            this = null;
        }
        nVar.a(this);
    }

    @Override // android.support.v4.view.p
    public void a(boolean z) {
        if (this.c != null) {
            this.c.onActionProviderVisibilityChanged(z);
        }
    }
}
