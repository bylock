package android.support.v7.internal.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

public class ae extends BaseAdapter {
    final /* synthetic */ ad a;
    private q b;
    private int c = -1;

    public ae(ad adVar, q qVar) {
        this.a = adVar;
        this.b = qVar;
        a();
    }

    public int getCount() {
        ArrayList l = this.a.i ? this.b.l() : this.b.i();
        if (this.c < 0) {
            return l.size();
        }
        return l.size() - 1;
    }

    /* renamed from: a */
    public u getItem(int i) {
        ArrayList l = this.a.i ? this.b.l() : this.b.i();
        if (this.c >= 0 && i >= this.c) {
            i++;
        }
        return (u) l.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            view2 = this.a.d.inflate(ad.b, viewGroup, false);
        } else {
            view2 = view;
        }
        ai aiVar = (ai) view2;
        if (this.a.c) {
            ((ListMenuItemView) view2).setForceShowIcon(true);
        }
        aiVar.a(getItem(i), 0);
        return view2;
    }

    public void a() {
        u r = this.a.f.r();
        if (r != null) {
            ArrayList l = this.a.f.l();
            int size = l.size();
            for (int i = 0; i < size; i++) {
                if (((u) l.get(i)) == r) {
                    this.c = i;
                    return;
                }
            }
        }
        this.c = -1;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
