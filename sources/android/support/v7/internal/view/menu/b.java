package android.support.v7.internal.view.menu;

import android.graphics.Rect;
import android.text.method.TransformationMethod;
import android.view.View;
import java.util.Locale;

/* compiled from: MyApp */
class b implements TransformationMethod {
    final /* synthetic */ ActionMenuItemView a;
    private Locale b;

    public b(ActionMenuItemView actionMenuItemView) {
        this.a = actionMenuItemView;
        this.b = actionMenuItemView.getContext().getResources().getConfiguration().locale;
    }

    public CharSequence getTransformation(CharSequence charSequence, View view) {
        if (charSequence != null) {
            return charSequence.toString().toUpperCase(this.b);
        }
        return null;
    }

    public void onFocusChanged(View view, CharSequence charSequence, boolean z, int i, Rect rect) {
    }
}
