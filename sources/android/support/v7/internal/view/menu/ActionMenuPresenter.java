package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.n;
import android.support.v4.view.o;
import android.support.v7.b.g;
import android.support.v7.b.h;
import android.support.v7.internal.view.a;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* compiled from: MyApp */
public class ActionMenuPresenter extends l implements o {
    final h a = new h(this);
    int b;
    private View i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    private final SparseBooleanArray t = new SparseBooleanArray();
    private View u;
    private g v;
    private d w;
    private e x;

    public ActionMenuPresenter(Context context) {
        super(context, h.abc_action_menu_layout, h.abc_action_menu_item_layout);
    }

    @Override // android.support.v7.internal.view.menu.af, android.support.v7.internal.view.menu.l
    public void a(Context context, q qVar) {
        super.a(context, qVar);
        Resources resources = context.getResources();
        a a2 = a.a(context);
        if (!this.k) {
            this.j = a2.b();
        }
        if (!this.q) {
            this.l = a2.c();
        }
        if (!this.o) {
            this.n = a2.a();
        }
        int i2 = this.l;
        if (this.j) {
            if (this.i == null) {
                this.i = new f(this, this.c);
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.i.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i2 -= this.i.getMeasuredWidth();
        } else {
            this.i = null;
        }
        this.m = i2;
        this.s = (int) (56.0f * resources.getDisplayMetrics().density);
        this.u = null;
    }

    public void a(Configuration configuration) {
        if (!this.o) {
            this.n = this.d.getResources().getInteger(g.abc_max_action_buttons);
        }
        if (this.e != null) {
            this.e.b(true);
        }
    }

    public void a(int i2, boolean z) {
        this.l = i2;
        this.p = z;
        this.q = true;
    }

    public void a(boolean z) {
        this.j = z;
        this.k = true;
    }

    public void a(int i2) {
        this.n = i2;
        this.o = true;
    }

    public void b(boolean z) {
        this.r = z;
    }

    @Override // android.support.v7.internal.view.menu.l
    public ah a(ViewGroup viewGroup) {
        ah a2 = super.a(viewGroup);
        ((ActionMenuView) a2).setPresenter(this);
        return a2;
    }

    @Override // android.support.v7.internal.view.menu.l
    public View a(u uVar, View view, ViewGroup viewGroup) {
        View actionView = uVar.getActionView();
        if (actionView == null || uVar.n()) {
            if (!(view instanceof ActionMenuItemView)) {
                view = null;
            }
            actionView = super.a(uVar, view, viewGroup);
        }
        actionView.setVisibility(uVar.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    @Override // android.support.v7.internal.view.menu.l
    public void a(u uVar, ai aiVar) {
        aiVar.a(uVar, 0);
        ((ActionMenuItemView) aiVar).setItemInvoker((ActionMenuView) this.h);
    }

    @Override // android.support.v7.internal.view.menu.l
    public boolean a(int i2, u uVar) {
        return uVar.i();
    }

    @Override // android.support.v7.internal.view.menu.af, android.support.v7.internal.view.menu.l
    public void c(boolean z) {
        boolean z2 = true;
        boolean z3 = false;
        super.c(z);
        if (this.h != null) {
            if (this.e != null) {
                ArrayList k2 = this.e.k();
                int size = k2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    n m2 = ((u) k2.get(i2)).m();
                    if (m2 != null) {
                        m2.a(this);
                    }
                }
            }
            ArrayList l2 = this.e != null ? this.e.l() : null;
            if (this.j && l2 != null) {
                int size2 = l2.size();
                if (size2 == 1) {
                    z3 = !((u) l2.get(0)).isActionViewExpanded();
                } else {
                    if (size2 <= 0) {
                        z2 = false;
                    }
                    z3 = z2;
                }
            }
            if (z3) {
                if (this.i == null) {
                    this.i = new f(this, this.c);
                }
                ViewGroup viewGroup = (ViewGroup) this.i.getParent();
                if (viewGroup != this.h) {
                    if (viewGroup != null) {
                        viewGroup.removeView(this.i);
                    }
                    ActionMenuView actionMenuView = (ActionMenuView) this.h;
                    actionMenuView.addView(this.i, actionMenuView.b());
                }
            } else if (this.i != null && this.i.getParent() == this.h) {
                ((ViewGroup) this.h).removeView(this.i);
            }
            ((ActionMenuView) this.h).setOverflowReserved(this.j);
        }
    }

    @Override // android.support.v7.internal.view.menu.l
    public boolean a(ViewGroup viewGroup, int i2) {
        if (viewGroup.getChildAt(i2) == this.i) {
            return false;
        }
        return super.a(viewGroup, i2);
    }

    @Override // android.support.v7.internal.view.menu.af, android.support.v7.internal.view.menu.l
    public boolean a(al alVar) {
        if (!alVar.hasVisibleItems()) {
            return false;
        }
        al alVar2 = alVar;
        while (alVar2.s() != this.e) {
            alVar2 = (al) alVar2.s();
        }
        if (a(alVar2.getItem()) == null) {
            if (this.i == null) {
                return false;
            }
            View view = this.i;
        }
        this.b = alVar.getItem().getItemId();
        this.w = new d(this, alVar);
        this.w.a(null);
        super.a(alVar);
        return true;
    }

    private View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if ((childAt instanceof ai) && ((ai) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public boolean a() {
        if (!this.j || e() || this.e == null || this.h == null || this.x != null) {
            return false;
        }
        this.x = new e(this, new g(this, this.d, this.e, this.i, true));
        ((View) this.h).post(this.x);
        super.a((al) null);
        return true;
    }

    public boolean b() {
        if (this.x == null || this.h == null) {
            g gVar = this.v;
            if (gVar == null) {
                return false;
            }
            gVar.b();
            return true;
        }
        ((View) this.h).removeCallbacks(this.x);
        this.x = null;
        return true;
    }

    public boolean c() {
        return b() | d();
    }

    public boolean d() {
        if (this.w == null) {
            return false;
        }
        this.w.a();
        return true;
    }

    public boolean e() {
        return this.v != null && this.v.c();
    }

    public boolean f() {
        return this.j;
    }

    @Override // android.support.v7.internal.view.menu.af, android.support.v7.internal.view.menu.l
    public boolean g() {
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z2;
        int i10;
        ArrayList i11 = this.e.i();
        int size = i11.size();
        int i12 = this.n;
        int i13 = this.m;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.h;
        int i14 = 0;
        int i15 = 0;
        boolean z3 = false;
        int i16 = 0;
        while (i16 < size) {
            u uVar = (u) i11.get(i16);
            if (uVar.k()) {
                i14++;
            } else if (uVar.j()) {
                i15++;
            } else {
                z3 = true;
            }
            if (!this.r || !uVar.isActionViewExpanded()) {
                i10 = i12;
            } else {
                i10 = 0;
            }
            i16++;
            i12 = i10;
        }
        if (this.j && (z3 || i14 + i15 > i12)) {
            i12--;
        }
        int i17 = i12 - i14;
        SparseBooleanArray sparseBooleanArray = this.t;
        sparseBooleanArray.clear();
        int i18 = 0;
        if (this.p) {
            i18 = i13 / this.s;
            i2 = ((i13 % this.s) / i18) + this.s;
        } else {
            i2 = 0;
        }
        int i19 = 0;
        int i20 = 0;
        int i21 = i18;
        while (i19 < size) {
            u uVar2 = (u) i11.get(i19);
            if (uVar2.k()) {
                View a2 = a(uVar2, this.u, viewGroup);
                if (this.u == null) {
                    this.u = a2;
                }
                if (this.p) {
                    i21 -= ActionMenuView.a(a2, i2, i21, makeMeasureSpec, 0);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i3 = a2.getMeasuredWidth();
                int i22 = i13 - i3;
                if (i20 != 0) {
                    i3 = i20;
                }
                int groupId = uVar2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                uVar2.d(true);
                i4 = i22;
                i5 = i17;
            } else if (uVar2.j()) {
                int groupId2 = uVar2.getGroupId();
                boolean z4 = sparseBooleanArray.get(groupId2);
                boolean z5 = (i17 > 0 || z4) && i13 > 0 && (!this.p || i21 > 0);
                if (z5) {
                    View a3 = a(uVar2, this.u, viewGroup);
                    if (this.u == null) {
                        this.u = a3;
                    }
                    if (this.p) {
                        int a4 = ActionMenuView.a(a3, i2, i21, makeMeasureSpec, 0);
                        int i23 = i21 - a4;
                        if (a4 == 0) {
                            z2 = false;
                        } else {
                            z2 = z5;
                        }
                        i9 = i23;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                        i9 = i21;
                        z2 = z5;
                    }
                    int measuredWidth = a3.getMeasuredWidth();
                    i13 -= measuredWidth;
                    if (i20 == 0) {
                        i20 = measuredWidth;
                    }
                    if (this.p) {
                        z = z2 & (i13 >= 0);
                        i6 = i20;
                        i7 = i9;
                    } else {
                        z = z2 & (i13 + i20 > 0);
                        i6 = i20;
                        i7 = i9;
                    }
                } else {
                    z = z5;
                    i6 = i20;
                    i7 = i21;
                }
                if (z && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                    i8 = i17;
                } else if (z4) {
                    sparseBooleanArray.put(groupId2, false);
                    int i24 = i17;
                    for (int i25 = 0; i25 < i19; i25++) {
                        u uVar3 = (u) i11.get(i25);
                        if (uVar3.getGroupId() == groupId2) {
                            if (uVar3.i()) {
                                i24++;
                            }
                            uVar3.d(false);
                        }
                    }
                    i8 = i24;
                } else {
                    i8 = i17;
                }
                if (z) {
                    i8--;
                }
                uVar2.d(z);
                i3 = i6;
                i4 = i13;
                i5 = i8;
                i21 = i7;
            } else {
                i3 = i20;
                i4 = i13;
                i5 = i17;
            }
            i19++;
            i13 = i4;
            i17 = i5;
            i20 = i3;
        }
        return true;
    }

    @Override // android.support.v7.internal.view.menu.af, android.support.v7.internal.view.menu.l
    public void a(q qVar, boolean z) {
        c();
        super.a(qVar, z);
    }

    /* access modifiers changed from: package-private */
    /* compiled from: MyApp */
    public class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new i();
        public int a;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.a = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
        }
    }
}
