package android.support.v7.internal.view.menu;

import android.os.Build;
import android.support.v4.b.a.b;
import android.support.v4.b.a.c;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

/* compiled from: MyApp */
public final class aj {
    public static Menu a(Menu menu) {
        if (Build.VERSION.SDK_INT >= 14) {
            return new ak(menu);
        }
        return menu;
    }

    public static MenuItem a(MenuItem menuItem) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new ab(menuItem);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return new w(menuItem);
        }
        return menuItem;
    }

    public static c a(SubMenu subMenu) {
        if (Build.VERSION.SDK_INT >= 14) {
            return new am(subMenu);
        }
        throw new UnsupportedOperationException();
    }

    public static b b(MenuItem menuItem) {
        if (Build.VERSION.SDK_INT >= 16) {
            return new ab(menuItem);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return new w(menuItem);
        }
        throw new UnsupportedOperationException();
    }
}
