package android.support.v7.internal.view.menu;

import android.graphics.drawable.Drawable;
import android.support.v4.b.a.c;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class am extends ak implements c {
    am(SubMenu subMenu) {
        super(subMenu);
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(int i) {
        ((SubMenu) this.a).setHeaderTitle(i);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        ((SubMenu) this.a).setHeaderTitle(charSequence);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(int i) {
        ((SubMenu) this.a).setHeaderIcon(i);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(Drawable drawable) {
        ((SubMenu) this.a).setHeaderIcon(drawable);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        ((SubMenu) this.a).setHeaderView(view);
        return this;
    }

    public void clearHeader() {
        ((SubMenu) this.a).clearHeader();
    }

    @Override // android.view.SubMenu
    public SubMenu setIcon(int i) {
        ((SubMenu) this.a).setIcon(i);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setIcon(Drawable drawable) {
        ((SubMenu) this.a).setIcon(drawable);
        return this;
    }

    public MenuItem getItem() {
        return a(((SubMenu) this.a).getItem());
    }
}
