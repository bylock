package android.support.v7.internal.view.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.IBinder;
import android.support.v7.b.h;
import android.support.v7.b.j;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/* compiled from: MyApp */
public class t implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnKeyListener, ag {
    private q a;
    o b;
    private AlertDialog c;
    private ag d;

    public t(q qVar) {
        this.a = qVar;
    }

    public void a(IBinder iBinder) {
        q qVar = this.a;
        AlertDialog.Builder builder = new AlertDialog.Builder(qVar.e());
        this.b = new o(h.abc_list_menu_item_layout, j.Theme_AppCompat_CompactMenu_Dialog);
        this.b.a(this);
        this.a.a(this.b);
        builder.setAdapter(this.b.a(), this);
        View o = qVar.o();
        if (o != null) {
            builder.setCustomTitle(o);
        } else {
            builder.setIcon(qVar.n()).setTitle(qVar.m());
        }
        builder.setOnKeyListener(this);
        this.c = builder.create();
        this.c.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.c.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.c.show();
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.a.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.a.performShortcut(i, keyEvent, 0);
    }

    public void a() {
        if (this.c != null) {
            this.c.dismiss();
        }
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.b.a(this.a, true);
    }

    @Override // android.support.v7.internal.view.menu.ag
    public void a(q qVar, boolean z) {
        if (z || qVar == this.a) {
            a();
        }
        if (this.d != null) {
            this.d.a(qVar, z);
        }
    }

    @Override // android.support.v7.internal.view.menu.ag
    public boolean b(q qVar) {
        if (this.d != null) {
            return this.d.b(qVar);
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a((u) this.b.a().getItem(i), 0);
    }
}
