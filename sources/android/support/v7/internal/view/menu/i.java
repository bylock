package android.support.v7.internal.view.menu;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.internal.view.menu.ActionMenuPresenter;

/* compiled from: MyApp */
final class i implements Parcelable.Creator {
    i() {
    }

    /* renamed from: a */
    public ActionMenuPresenter.SavedState createFromParcel(Parcel parcel) {
        return new ActionMenuPresenter.SavedState(parcel);
    }

    /* renamed from: a */
    public ActionMenuPresenter.SavedState[] newArray(int i) {
        return new ActionMenuPresenter.SavedState[i];
    }
}
