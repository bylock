package android.support.v7.internal.view.menu;

import android.support.v4.view.n;
import android.view.ActionProvider;
import android.view.SubMenu;
import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class x extends ActionProvider {
    final n a;
    final /* synthetic */ w b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(w wVar, n nVar) {
        super(nVar.a());
        this.b = wVar;
        this.a = nVar;
        if (wVar.b) {
            this.a.a(new y(this, wVar));
        }
    }

    public View onCreateActionView() {
        if (this.b.b) {
            this.b.c();
        }
        return this.a.b();
    }

    public boolean onPerformDefaultAction() {
        return this.a.f();
    }

    public boolean hasSubMenu() {
        return this.a.g();
    }

    public void onPrepareSubMenu(SubMenu subMenu) {
        this.a.a(this.b.a(subMenu));
    }
}
