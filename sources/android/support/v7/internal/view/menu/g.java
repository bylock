package android.support.v7.internal.view.menu;

import android.content.Context;
import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class g extends ad {
    final /* synthetic */ ActionMenuPresenter a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(ActionMenuPresenter actionMenuPresenter, Context context, q qVar, View view, boolean z) {
        super(context, qVar, view, z);
        this.a = actionMenuPresenter;
        a(actionMenuPresenter.a);
    }

    @Override // android.support.v7.internal.view.menu.ad
    public void onDismiss() {
        super.onDismiss();
        this.a.e.close();
        ActionMenuPresenter.a(this.a, (g) null);
    }
}
