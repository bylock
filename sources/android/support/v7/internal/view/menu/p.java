package android.support.v7.internal.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class p extends BaseAdapter {
    final /* synthetic */ o a;
    private int b = -1;

    public p(o oVar) {
        this.a = oVar;
        a();
    }

    public int getCount() {
        int size = this.a.c.l().size() - this.a.h;
        return this.b < 0 ? size : size - 1;
    }

    /* renamed from: a */
    public u getItem(int i) {
        ArrayList l = this.a.c.l();
        int i2 = this.a.h + i;
        if (this.b >= 0 && i2 >= this.b) {
            i2++;
        }
        return (u) l.get(i2);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            view2 = this.a.b.inflate(this.a.f, viewGroup, false);
        } else {
            view2 = view;
        }
        ((ai) view2).a(getItem(i), 0);
        return view2;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        u r = this.a.c.r();
        if (r != null) {
            ArrayList l = this.a.c.l();
            int size = l.size();
            for (int i = 0; i < size; i++) {
                if (((u) l.get(i)) == r) {
                    this.b = i;
                    return;
                }
            }
        }
        this.b = -1;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
