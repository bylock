package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v7.b.c;
import android.support.v7.b.k;
import android.support.v7.internal.widget.LinearLayoutICS;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: MyApp */
public class ActionMenuView extends LinearLayoutICS implements ah, s {
    private q a;
    private boolean b;
    private ActionMenuPresenter c;
    private boolean d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBaselineAligned(false);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.f = (int) (56.0f * f2);
        this.g = (int) (f2 * 4.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, k.ActionBar, c.actionBarStyle, 0);
        this.i = obtainStyledAttributes.getDimensionPixelSize(1, 0);
        obtainStyledAttributes.recycle();
    }

    public void setPresenter(ActionMenuPresenter actionMenuPresenter) {
        this.c = actionMenuPresenter;
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        this.c.c(false);
        if (this.c != null && this.c.e()) {
            this.c.b();
            this.c.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        boolean z = this.d;
        this.d = View.MeasureSpec.getMode(i2) == 1073741824;
        if (z != this.d) {
            this.e = 0;
        }
        int mode = View.MeasureSpec.getMode(i2);
        if (!(!this.d || this.a == null || mode == this.e)) {
            this.e = mode;
            this.a.b(true);
        }
        if (this.d) {
            a(i2, i3);
            return;
        }
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            k kVar = (k) getChildAt(i4).getLayoutParams();
            kVar.rightMargin = 0;
            kVar.leftMargin = 0;
        }
        super.onMeasure(i2, i3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:112:0x028f  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01e8  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01f7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r34, int r35) {
        /*
        // Method dump skipped, instructions count: 838
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.ActionMenuView.a(int, int):void");
    }

    static int a(View view, int i2, int i3, int i4, int i5) {
        boolean z;
        int i6;
        boolean z2 = false;
        k kVar = (k) view.getLayoutParams();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i4) - i5, View.MeasureSpec.getMode(i4));
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        if (actionMenuItemView == null || !actionMenuItemView.b()) {
            z = false;
        } else {
            z = true;
        }
        if (i3 <= 0 || (z && i3 < 2)) {
            i6 = 0;
        } else {
            view.measure(View.MeasureSpec.makeMeasureSpec(i2 * i3, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            i6 = measuredWidth / i2;
            if (measuredWidth % i2 != 0) {
                i6++;
            }
            if (z && i6 < 2) {
                i6 = 2;
            }
        }
        if (!kVar.a && z) {
            z2 = true;
        }
        kVar.d = z2;
        kVar.b = i6;
        view.measure(View.MeasureSpec.makeMeasureSpec(i6 * i2, 1073741824), makeMeasureSpec);
        return i6;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z2;
        if (!this.d) {
            super.onLayout(z, i2, i3, i4, i5);
            return;
        }
        int childCount = getChildCount();
        int i10 = (i3 + i5) / 2;
        int supportDividerWidth = getSupportDividerWidth();
        int i11 = 0;
        int i12 = 0;
        int paddingRight = ((i4 - i2) - getPaddingRight()) - getPaddingLeft();
        boolean z3 = false;
        int i13 = 0;
        while (i13 < childCount) {
            View childAt = getChildAt(i13);
            if (childAt.getVisibility() == 8) {
                z2 = z3;
                i7 = paddingRight;
                i8 = i12;
                i9 = i11;
            } else {
                k kVar = (k) childAt.getLayoutParams();
                if (kVar.a) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (a(i13)) {
                        measuredWidth += supportDividerWidth;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    int width = (getWidth() - getPaddingRight()) - kVar.rightMargin;
                    int i14 = i10 - (measuredHeight / 2);
                    childAt.layout(width - measuredWidth, i14, width, measuredHeight + i14);
                    i7 = paddingRight - measuredWidth;
                    z2 = true;
                    i8 = i12;
                    i9 = i11;
                } else {
                    int measuredWidth2 = childAt.getMeasuredWidth() + kVar.leftMargin + kVar.rightMargin;
                    int i15 = i11 + measuredWidth2;
                    int i16 = paddingRight - measuredWidth2;
                    if (a(i13)) {
                        i15 += supportDividerWidth;
                    }
                    i7 = i16;
                    i8 = i12 + 1;
                    i9 = i15;
                    z2 = z3;
                }
            }
            i13++;
            i11 = i9;
            i12 = i8;
            paddingRight = i7;
            z3 = z2;
        }
        if (childCount != 1 || z3) {
            int i17 = i12 - (z3 ? 0 : 1);
            int max = Math.max(0, i17 > 0 ? paddingRight / i17 : 0);
            int paddingLeft = getPaddingLeft();
            int i18 = 0;
            while (i18 < childCount) {
                View childAt2 = getChildAt(i18);
                k kVar2 = (k) childAt2.getLayoutParams();
                if (childAt2.getVisibility() == 8) {
                    i6 = paddingLeft;
                } else if (kVar2.a) {
                    i6 = paddingLeft;
                } else {
                    int i19 = paddingLeft + kVar2.leftMargin;
                    int measuredWidth3 = childAt2.getMeasuredWidth();
                    int measuredHeight2 = childAt2.getMeasuredHeight();
                    int i20 = i10 - (measuredHeight2 / 2);
                    childAt2.layout(i19, i20, i19 + measuredWidth3, measuredHeight2 + i20);
                    i6 = kVar2.rightMargin + measuredWidth3 + max + i19;
                }
                i18++;
                paddingLeft = i6;
            }
            return;
        }
        View childAt3 = getChildAt(0);
        int measuredWidth4 = childAt3.getMeasuredWidth();
        int measuredHeight3 = childAt3.getMeasuredHeight();
        int i21 = ((i4 - i2) / 2) - (measuredWidth4 / 2);
        int i22 = i10 - (measuredHeight3 / 2);
        childAt3.layout(i21, i22, measuredWidth4 + i21, measuredHeight3 + i22);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.c.c();
    }

    public void setOverflowReserved(boolean z) {
        this.b = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public k generateDefaultLayoutParams() {
        k kVar = new k(-2, -2);
        kVar.gravity = 16;
        return kVar;
    }

    /* renamed from: a */
    public k generateLayoutParams(AttributeSet attributeSet) {
        return new k(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public k generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (!(layoutParams instanceof k)) {
            return generateDefaultLayoutParams();
        }
        k kVar = new k((k) layoutParams);
        if (kVar.gravity > 0) {
            return kVar;
        }
        kVar.gravity = 16;
        return kVar;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams != null && (layoutParams instanceof k);
    }

    public k b() {
        k a2 = generateDefaultLayoutParams();
        a2.a = true;
        return a2;
    }

    @Override // android.support.v7.internal.view.menu.s
    public boolean a(u uVar) {
        return this.a.a(uVar, 0);
    }

    public int getWindowAnimations() {
        return 0;
    }

    @Override // android.support.v7.internal.view.menu.ah
    public void a(q qVar) {
        this.a = qVar;
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.internal.widget.LinearLayoutICS
    public boolean a(int i2) {
        View childAt = getChildAt(i2 - 1);
        View childAt2 = getChildAt(i2);
        boolean z = false;
        if (i2 < getChildCount() && (childAt instanceof j)) {
            z = false | ((j) childAt).d();
        }
        if (i2 <= 0 || !(childAt2 instanceof j)) {
            return z;
        }
        return ((j) childAt2).c() | z;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }
}
