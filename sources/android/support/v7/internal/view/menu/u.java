package android.support.v7.internal.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.b;
import android.support.v4.view.ah;
import android.support.v4.view.n;
import android.support.v4.view.p;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/* compiled from: MyApp */
public final class u implements b {
    private static String w;
    private static String x;
    private static String y;
    private static String z;
    private final int a;
    private final int b;
    private final int c;
    private final int d;
    private CharSequence e;
    private CharSequence f;
    private Intent g;
    private char h;
    private char i;
    private Drawable j;
    private int k = 0;
    private q l;
    private al m;
    private Runnable n;
    private MenuItem.OnMenuItemClickListener o;
    private int p = 16;
    private int q = 0;
    private View r;
    private n s;
    private ah t;
    private boolean u = false;
    private ContextMenu.ContextMenuInfo v;

    u(q qVar, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.l = qVar;
        this.a = i3;
        this.b = i2;
        this.c = i4;
        this.d = i5;
        this.e = charSequence;
        this.q = i6;
    }

    public boolean a() {
        if ((this.o != null && this.o.onMenuItemClick(this)) || this.l.a(this.l.p(), this)) {
            return true;
        }
        if (this.n != null) {
            this.n.run();
            return true;
        }
        if (this.g != null) {
            try {
                this.l.e().startActivity(this.g);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        if (this.s == null || !this.s.f()) {
            return false;
        }
        return true;
    }

    public boolean isEnabled() {
        return (this.p & 16) != 0;
    }

    public MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.p |= 16;
        } else {
            this.p &= -17;
        }
        this.l.b(false);
        return this;
    }

    public int getGroupId() {
        return this.b;
    }

    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.a;
    }

    public int getOrder() {
        return this.c;
    }

    public int b() {
        return this.d;
    }

    public Intent getIntent() {
        return this.g;
    }

    public MenuItem setIntent(Intent intent) {
        this.g = intent;
        return this;
    }

    public char getAlphabeticShortcut() {
        return this.i;
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        if (this.i != c2) {
            this.i = Character.toLowerCase(c2);
            this.l.b(false);
        }
        return this;
    }

    public char getNumericShortcut() {
        return this.h;
    }

    public MenuItem setNumericShortcut(char c2) {
        if (this.h != c2) {
            this.h = c2;
            this.l.b(false);
        }
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.h = c2;
        this.i = Character.toLowerCase(c3);
        this.l.b(false);
        return this;
    }

    /* access modifiers changed from: package-private */
    public char c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        char c2 = c();
        if (c2 == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(w);
        switch (c2) {
            case '\b':
                sb.append(y);
                break;
            case '\n':
                sb.append(x);
                break;
            case ' ':
                sb.append(z);
                break;
            default:
                sb.append(c2);
                break;
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.l.c() && c() != 0;
    }

    public SubMenu getSubMenu() {
        return this.m;
    }

    public boolean hasSubMenu() {
        return this.m != null;
    }

    /* access modifiers changed from: package-private */
    public void a(al alVar) {
        this.m = alVar;
        alVar.setHeaderTitle(getTitle());
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public CharSequence a(ai aiVar) {
        return (aiVar == null || !aiVar.a()) ? getTitle() : getTitleCondensed();
    }

    @Override // android.view.MenuItem
    public MenuItem setTitle(CharSequence charSequence) {
        this.e = charSequence;
        this.l.b(false);
        if (this.m != null) {
            this.m.setHeaderTitle(charSequence);
        }
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setTitle(int i2) {
        return setTitle(this.l.e().getString(i2));
    }

    public CharSequence getTitleCondensed() {
        return this.f != null ? this.f : this.e;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f = charSequence;
        if (charSequence == null) {
            CharSequence charSequence2 = this.e;
        }
        this.l.b(false);
        return this;
    }

    public Drawable getIcon() {
        if (this.j != null) {
            return this.j;
        }
        if (this.k == 0) {
            return null;
        }
        Drawable drawable = this.l.d().getDrawable(this.k);
        this.k = 0;
        this.j = drawable;
        return drawable;
    }

    @Override // android.view.MenuItem
    public MenuItem setIcon(Drawable drawable) {
        this.k = 0;
        this.j = drawable;
        this.l.b(false);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setIcon(int i2) {
        this.j = null;
        this.k = i2;
        this.l.b(false);
        return this;
    }

    public boolean isCheckable() {
        return (this.p & 1) == 1;
    }

    public MenuItem setCheckable(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 1 : 0) | (this.p & -2);
        if (i2 != this.p) {
            this.l.b(false);
        }
        return this;
    }

    public void a(boolean z2) {
        this.p = (z2 ? 4 : 0) | (this.p & -5);
    }

    public boolean f() {
        return (this.p & 4) != 0;
    }

    public boolean isChecked() {
        return (this.p & 2) == 2;
    }

    public MenuItem setChecked(boolean z2) {
        if ((this.p & 4) != 0) {
            this.l.a((MenuItem) this);
        } else {
            b(z2);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        int i2;
        int i3 = this.p;
        int i4 = this.p & -3;
        if (z2) {
            i2 = 2;
        } else {
            i2 = 0;
        }
        this.p = i2 | i4;
        if (i3 != this.p) {
            this.l.b(false);
        }
    }

    public boolean isVisible() {
        return (this.s == null || !this.s.c()) ? (this.p & 8) == 0 : (this.p & 8) == 0 && this.s.d();
    }

    /* access modifiers changed from: package-private */
    public boolean c(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 0 : 8) | (this.p & -9);
        if (i2 != this.p) {
            return true;
        }
        return false;
    }

    public MenuItem setVisible(boolean z2) {
        if (c(z2)) {
            this.l.a(this);
        }
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.o = onMenuItemClickListener;
        return this;
    }

    public String toString() {
        return this.e.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.v = contextMenuInfo;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.v;
    }

    public void g() {
        this.l.b(this);
    }

    public boolean h() {
        return this.l.q();
    }

    public boolean i() {
        return (this.p & 32) == 32;
    }

    public boolean j() {
        return (this.q & 1) == 1;
    }

    public boolean k() {
        return (this.q & 2) == 2;
    }

    public void d(boolean z2) {
        if (z2) {
            this.p |= 32;
        } else {
            this.p &= -33;
        }
    }

    public boolean l() {
        return (this.q & 4) == 4;
    }

    @Override // android.support.v4.b.a.b
    public void setShowAsAction(int i2) {
        switch (i2 & 3) {
            case 0:
            case 1:
            case 2:
                this.q = i2;
                this.l.b(this);
                return;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
    }

    /* renamed from: a */
    public b setActionView(View view) {
        this.r = view;
        this.s = null;
        if (view != null && view.getId() == -1 && this.a > 0) {
            view.setId(this.a);
        }
        this.l.b(this);
        return this;
    }

    /* renamed from: a */
    public b setActionView(int i2) {
        Context e2 = this.l.e();
        setActionView(LayoutInflater.from(e2).inflate(i2, (ViewGroup) new LinearLayout(e2), false));
        return this;
    }

    @Override // android.support.v4.b.a.b
    public View getActionView() {
        if (this.r != null) {
            return this.r;
        }
        if (this.s == null) {
            return null;
        }
        this.r = this.s.a(this);
        return this.r;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException("Implementation should use setSupportActionProvider!");
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("Implementation should use getSupportActionProvider!");
    }

    public n m() {
        return this.s;
    }

    @Override // android.support.v4.b.a.b
    public b a(n nVar) {
        if (this.s != nVar) {
            this.r = null;
            if (this.s != null) {
                this.s.a((p) null);
            }
            this.s = nVar;
            this.l.b(true);
            if (nVar != null) {
                nVar.a(new v(this));
            }
        }
        return this;
    }

    /* renamed from: b */
    public b setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    @Override // android.support.v4.b.a.b
    public boolean expandActionView() {
        if ((this.q & 8) == 0 || this.r == null) {
            return false;
        }
        if (this.t == null || this.t.a(this)) {
            return this.l.c(this);
        }
        return false;
    }

    public boolean collapseActionView() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null) {
            return true;
        }
        if (this.t == null || this.t.b(this)) {
            return this.l.d(this);
        }
        return false;
    }

    public boolean n() {
        return ((this.q & 8) == 0 || this.r == null) ? false : true;
    }

    public void e(boolean z2) {
        this.u = z2;
        this.l.b(false);
    }

    @Override // android.support.v4.b.a.b
    public boolean isActionViewExpanded() {
        return this.u;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException("Implementation should use setSupportOnActionExpandListener!");
    }
}
