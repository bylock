package android.support.v7.internal.view.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* compiled from: MyApp */
public abstract class l implements af {
    private ag a;
    private int b;
    protected Context c;
    protected Context d;
    protected q e;
    protected LayoutInflater f;
    protected LayoutInflater g;
    protected ah h;
    private int i;
    private int j;

    public abstract void a(u uVar, ai aiVar);

    public l(Context context, int i2, int i3) {
        this.c = context;
        this.f = LayoutInflater.from(context);
        this.b = i2;
        this.i = i3;
    }

    @Override // android.support.v7.internal.view.menu.af
    public void a(Context context, q qVar) {
        this.d = context;
        this.g = LayoutInflater.from(this.d);
        this.e = qVar;
    }

    public ah a(ViewGroup viewGroup) {
        if (this.h == null) {
            this.h = (ah) this.f.inflate(this.b, viewGroup, false);
            this.h.a(this.e);
            c(true);
        }
        return this.h;
    }

    @Override // android.support.v7.internal.view.menu.af
    public void c(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup != null) {
            if (this.e != null) {
                this.e.j();
                ArrayList i4 = this.e.i();
                int size = i4.size();
                int i5 = 0;
                i2 = 0;
                while (i5 < size) {
                    u uVar = (u) i4.get(i5);
                    if (a(i2, uVar)) {
                        View childAt = viewGroup.getChildAt(i2);
                        u itemData = childAt instanceof ai ? ((ai) childAt).getItemData() : null;
                        View a2 = a(uVar, childAt, viewGroup);
                        if (uVar != itemData) {
                            a2.setPressed(false);
                        }
                        if (a2 != childAt) {
                            a(a2, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i5++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.h).addView(view, i2);
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    public void a(ag agVar) {
        this.a = agVar;
    }

    public ai b(ViewGroup viewGroup) {
        return (ai) this.f.inflate(this.i, viewGroup, false);
    }

    public View a(u uVar, View view, ViewGroup viewGroup) {
        ai b2;
        if (view instanceof ai) {
            b2 = (ai) view;
        } else {
            b2 = b(viewGroup);
        }
        a(uVar, b2);
        return (View) b2;
    }

    public boolean a(int i2, u uVar) {
        return true;
    }

    @Override // android.support.v7.internal.view.menu.af
    public void a(q qVar, boolean z) {
        if (this.a != null) {
            this.a.a(qVar, z);
        }
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean a(al alVar) {
        if (this.a != null) {
            return this.a.b(alVar);
        }
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean g() {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean a(q qVar, u uVar) {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean b(q qVar, u uVar) {
        return false;
    }

    public void b(int i2) {
        this.j = i2;
    }
}
