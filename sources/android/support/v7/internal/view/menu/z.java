package android.support.v7.internal.view.menu;

import android.support.v7.c.c;
import android.view.CollapsibleActionView;
import android.view.View;
import android.widget.FrameLayout;

/* compiled from: MyApp */
class z extends FrameLayout implements CollapsibleActionView {
    final c a;

    z(View view) {
        super(view.getContext());
        this.a = (c) view;
        addView(view);
    }

    public void onActionViewExpanded() {
        this.a.a();
    }

    public void onActionViewCollapsed() {
        this.a.b();
    }

    /* access modifiers changed from: package-private */
    public View a() {
        return (View) this.a;
    }
}
