package android.support.v7.internal.view.menu;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class h implements ag {
    final /* synthetic */ ActionMenuPresenter a;

    private h(ActionMenuPresenter actionMenuPresenter) {
        this.a = actionMenuPresenter;
    }

    @Override // android.support.v7.internal.view.menu.ag
    public boolean b(q qVar) {
        if (qVar != null) {
            this.a.b = ((al) qVar).getItem().getItemId();
        }
        return false;
    }

    @Override // android.support.v7.internal.view.menu.ag
    public void a(q qVar, boolean z) {
        if (qVar instanceof al) {
            ((al) qVar).p().a(false);
        }
    }
}
