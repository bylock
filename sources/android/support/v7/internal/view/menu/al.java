package android.support.v7.internal.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

/* compiled from: MyApp */
public class al extends q implements SubMenu {
    private q d;
    private u e;

    public al(Context context, q qVar, u uVar) {
        super(context);
        this.d = qVar;
        this.e = uVar;
    }

    @Override // android.support.v7.internal.view.menu.q
    public void setQwertyMode(boolean z) {
        this.d.setQwertyMode(z);
    }

    @Override // android.support.v7.internal.view.menu.q
    public boolean b() {
        return this.d.b();
    }

    @Override // android.support.v7.internal.view.menu.q
    public boolean c() {
        return this.d.c();
    }

    public Menu s() {
        return this.d;
    }

    public MenuItem getItem() {
        return this.e;
    }

    @Override // android.support.v7.internal.view.menu.q
    public void a(r rVar) {
        this.d.a(rVar);
    }

    @Override // android.support.v7.internal.view.menu.q
    public q p() {
        return this.d;
    }

    @Override // android.support.v7.internal.view.menu.q
    public boolean a(q qVar, MenuItem menuItem) {
        return super.a(qVar, menuItem) || this.d.a(qVar, menuItem);
    }

    @Override // android.view.SubMenu
    public SubMenu setIcon(Drawable drawable) {
        this.e.setIcon(drawable);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setIcon(int i) {
        this.e.setIcon(i);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(Drawable drawable) {
        super.a(drawable);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(int i) {
        super.a(e().getResources().getDrawable(i));
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.a(charSequence);
        return this;
    }

    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(int i) {
        super.a(e().getResources().getString(i));
        return this;
    }

    public SubMenu setHeaderView(View view) {
        super.a(view);
        return this;
    }

    @Override // android.support.v7.internal.view.menu.q
    public void clearHeader() {
    }

    @Override // android.support.v7.internal.view.menu.q
    public boolean c(u uVar) {
        return this.d.c(uVar);
    }

    @Override // android.support.v7.internal.view.menu.q
    public boolean d(u uVar) {
        return this.d.d(uVar);
    }

    @Override // android.support.v7.internal.view.menu.q
    public String a() {
        int itemId = this.e != null ? this.e.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.a() + ":" + itemId;
    }
}
