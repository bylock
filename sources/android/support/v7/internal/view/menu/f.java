package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v7.b.c;
import android.widget.ImageButton;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class f extends ImageButton implements j {
    final /* synthetic */ ActionMenuPresenter a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(ActionMenuPresenter actionMenuPresenter, Context context) {
        super(context, null, c.actionOverflowButtonStyle);
        this.a = actionMenuPresenter;
        setClickable(true);
        setFocusable(true);
        setVisibility(0);
        setEnabled(true);
    }

    public boolean performClick() {
        if (!super.performClick()) {
            playSoundEffect(0);
            this.a.a();
        }
        return true;
    }

    @Override // android.support.v7.internal.view.menu.j
    public boolean c() {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.j
    public boolean d() {
        return false;
    }
}
