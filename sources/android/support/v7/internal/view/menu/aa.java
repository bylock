package android.support.v7.internal.view.menu;

import android.view.MenuItem;

/* compiled from: MyApp */
class aa extends n implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ w b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    aa(w wVar, MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        super(onMenuItemClickListener);
        this.b = wVar;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        return ((MenuItem.OnMenuItemClickListener) this.a).onMenuItemClick(this.b.a(menuItem));
    }
}
