package android.support.v7.internal.view.menu;

import android.support.v4.b.a.b;
import android.support.v4.b.a.c;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.HashMap;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public abstract class m extends n {
    private HashMap b;
    private HashMap c;

    m(Object obj) {
        super(obj);
    }

    /* access modifiers changed from: package-private */
    public final b a(MenuItem menuItem) {
        if (menuItem == null) {
            return null;
        }
        if (this.b == null) {
            this.b = new HashMap();
        }
        b bVar = (b) this.b.get(menuItem);
        if (bVar != null) {
            return bVar;
        }
        b b2 = aj.b(menuItem);
        this.b.put(menuItem, b2);
        return b2;
    }

    /* access modifiers changed from: package-private */
    public final SubMenu a(SubMenu subMenu) {
        if (subMenu == null) {
            return null;
        }
        if (this.c == null) {
            this.c = new HashMap();
        }
        SubMenu subMenu2 = (SubMenu) this.c.get(subMenu);
        if (subMenu2 != null) {
            return subMenu2;
        }
        c a = aj.a(subMenu);
        this.c.put(subMenu, a);
        return a;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.b != null) {
            this.b.clear();
        }
        if (this.c != null) {
            this.c.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        if (this.b != null) {
            Iterator it = this.b.keySet().iterator();
            while (it.hasNext()) {
                if (i == ((MenuItem) it.next()).getGroupId()) {
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        if (this.b != null) {
            Iterator it = this.b.keySet().iterator();
            while (it.hasNext()) {
                if (i == ((MenuItem) it.next()).getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }
}
