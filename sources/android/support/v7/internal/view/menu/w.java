package android.support.v7.internal.view.menu;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.b;
import android.support.v4.view.n;
import android.support.v7.c.c;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.reflect.Method;

/* compiled from: MyApp */
public class w extends m implements b {
    private final boolean b;
    private boolean c;
    private Method d;

    w(MenuItem menuItem, boolean z) {
        super(menuItem);
        this.c = menuItem.isVisible();
        this.b = z;
    }

    w(MenuItem menuItem) {
        this(menuItem, true);
    }

    public int getItemId() {
        return ((MenuItem) this.a).getItemId();
    }

    public int getGroupId() {
        return ((MenuItem) this.a).getGroupId();
    }

    public int getOrder() {
        return ((MenuItem) this.a).getOrder();
    }

    @Override // android.view.MenuItem
    public MenuItem setTitle(CharSequence charSequence) {
        ((MenuItem) this.a).setTitle(charSequence);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setTitle(int i) {
        ((MenuItem) this.a).setTitle(i);
        return this;
    }

    public CharSequence getTitle() {
        return ((MenuItem) this.a).getTitle();
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        ((MenuItem) this.a).setTitleCondensed(charSequence);
        return this;
    }

    public CharSequence getTitleCondensed() {
        return ((MenuItem) this.a).getTitleCondensed();
    }

    @Override // android.view.MenuItem
    public MenuItem setIcon(Drawable drawable) {
        ((MenuItem) this.a).setIcon(drawable);
        return this;
    }

    @Override // android.view.MenuItem
    public MenuItem setIcon(int i) {
        ((MenuItem) this.a).setIcon(i);
        return this;
    }

    public Drawable getIcon() {
        return ((MenuItem) this.a).getIcon();
    }

    public MenuItem setIntent(Intent intent) {
        ((MenuItem) this.a).setIntent(intent);
        return this;
    }

    public Intent getIntent() {
        return ((MenuItem) this.a).getIntent();
    }

    public MenuItem setShortcut(char c2, char c3) {
        ((MenuItem) this.a).setShortcut(c2, c3);
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        ((MenuItem) this.a).setNumericShortcut(c2);
        return this;
    }

    public char getNumericShortcut() {
        return ((MenuItem) this.a).getNumericShortcut();
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        ((MenuItem) this.a).setAlphabeticShortcut(c2);
        return this;
    }

    public char getAlphabeticShortcut() {
        return ((MenuItem) this.a).getAlphabeticShortcut();
    }

    public MenuItem setCheckable(boolean z) {
        ((MenuItem) this.a).setCheckable(z);
        return this;
    }

    public boolean isCheckable() {
        return ((MenuItem) this.a).isCheckable();
    }

    public MenuItem setChecked(boolean z) {
        ((MenuItem) this.a).setChecked(z);
        return this;
    }

    public boolean isChecked() {
        return ((MenuItem) this.a).isChecked();
    }

    public MenuItem setVisible(boolean z) {
        if (this.b) {
            this.c = z;
            if (c()) {
                return this;
            }
        }
        return b(z);
    }

    public boolean isVisible() {
        return ((MenuItem) this.a).isVisible();
    }

    public MenuItem setEnabled(boolean z) {
        ((MenuItem) this.a).setEnabled(z);
        return this;
    }

    public boolean isEnabled() {
        return ((MenuItem) this.a).isEnabled();
    }

    public boolean hasSubMenu() {
        return ((MenuItem) this.a).hasSubMenu();
    }

    public SubMenu getSubMenu() {
        return a(((MenuItem) this.a).getSubMenu());
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        ((MenuItem) this.a).setOnMenuItemClickListener(onMenuItemClickListener != null ? new aa(this, onMenuItemClickListener) : null);
        return this;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((MenuItem) this.a).getMenuInfo();
    }

    @Override // android.support.v4.b.a.b
    public void setShowAsAction(int i) {
        ((MenuItem) this.a).setShowAsAction(i);
    }

    public MenuItem setShowAsActionFlags(int i) {
        ((MenuItem) this.a).setShowAsActionFlags(i);
        return this;
    }

    @Override // android.support.v4.b.a.b, android.view.MenuItem
    public MenuItem setActionView(View view) {
        if (view instanceof c) {
            view = new z(view);
        }
        ((MenuItem) this.a).setActionView(view);
        return this;
    }

    @Override // android.support.v4.b.a.b, android.view.MenuItem
    public MenuItem setActionView(int i) {
        ((MenuItem) this.a).setActionView(i);
        View actionView = ((MenuItem) this.a).getActionView();
        if (actionView instanceof c) {
            ((MenuItem) this.a).setActionView(new z(actionView));
        }
        return this;
    }

    @Override // android.support.v4.b.a.b
    public View getActionView() {
        View actionView = ((MenuItem) this.a).getActionView();
        if (actionView instanceof z) {
            return ((z) actionView).a();
        }
        return actionView;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        ((MenuItem) this.a).setActionProvider(actionProvider);
        if (actionProvider != null && this.b) {
            c();
        }
        return this;
    }

    public ActionProvider getActionProvider() {
        return ((MenuItem) this.a).getActionProvider();
    }

    @Override // android.support.v4.b.a.b
    public boolean expandActionView() {
        return ((MenuItem) this.a).expandActionView();
    }

    public boolean collapseActionView() {
        return ((MenuItem) this.a).collapseActionView();
    }

    @Override // android.support.v4.b.a.b
    public boolean isActionViewExpanded() {
        return ((MenuItem) this.a).isActionViewExpanded();
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        ((MenuItem) this.a).setOnActionExpandListener(onActionExpandListener);
        return this;
    }

    @Override // android.support.v4.b.a.b
    public b a(n nVar) {
        ((MenuItem) this.a).setActionProvider(nVar != null ? b(nVar) : null);
        return this;
    }

    public n b() {
        x xVar = (x) ((MenuItem) this.a).getActionProvider();
        if (xVar != null) {
            return xVar.a;
        }
        return null;
    }

    public void a(boolean z) {
        try {
            if (this.d == null) {
                this.d = ((MenuItem) this.a).getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
            }
            this.d.invoke(this.a, Boolean.valueOf(z));
        } catch (Exception e) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e);
        }
    }

    /* access modifiers changed from: package-private */
    public x b(n nVar) {
        return new x(this, nVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        n b2;
        if (!this.c || (b2 = b()) == null || !b2.c() || b2.d()) {
            return false;
        }
        b(false);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final MenuItem b(boolean z) {
        return ((MenuItem) this.a).setVisible(z);
    }
}
