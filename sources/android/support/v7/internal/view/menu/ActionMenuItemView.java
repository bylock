package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.b.d;
import android.support.v7.b.k;
import android.support.v7.internal.widget.s;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

/* compiled from: MyApp */
public class ActionMenuItemView extends s implements ai, j, View.OnClickListener, View.OnLongClickListener {
    private u a;
    private CharSequence b;
    private Drawable c;
    private s d;
    private boolean e;
    private boolean f;
    private int g;
    private int h;

    public ActionMenuItemView(Context context) {
        this(context, null);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.e = context.getResources().getBoolean(d.abc_config_allowActionMenuItemTextWithIcon);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, k.ActionMenuItemView, 0, 0);
        this.g = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        obtainStyledAttributes.recycle();
        setOnClickListener(this);
        setOnLongClickListener(this);
        setTransformationMethod(new b(this));
        this.h = -1;
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.h = i;
        super.setPadding(i, i2, i3, i4);
    }

    @Override // android.support.v7.internal.view.menu.ai
    public u getItemData() {
        return this.a;
    }

    @Override // android.support.v7.internal.view.menu.ai
    public void a(u uVar, int i) {
        this.a = uVar;
        setIcon(uVar.getIcon());
        setTitle(uVar.a((ai) this));
        setId(uVar.getItemId());
        setVisibility(uVar.isVisible() ? 0 : 8);
        setEnabled(uVar.isEnabled());
    }

    public void onClick(View view) {
        if (this.d != null) {
            this.d.a(this.a);
        }
    }

    public void setItemInvoker(s sVar) {
        this.d = sVar;
    }

    @Override // android.support.v7.internal.view.menu.ai
    public boolean a() {
        return true;
    }

    public void setCheckable(boolean z) {
    }

    public void setChecked(boolean z) {
    }

    public void setExpandedFormat(boolean z) {
        if (this.f != z) {
            this.f = z;
            if (this.a != null) {
                this.a.g();
            }
        }
    }

    private void e() {
        boolean z = false;
        boolean z2 = !TextUtils.isEmpty(this.b);
        if (this.c == null || (this.a.l() && (this.e || this.f))) {
            z = true;
        }
        setText(z2 & z ? this.b : null);
    }

    public void setIcon(Drawable drawable) {
        this.c = drawable;
        setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        e();
    }

    public boolean b() {
        return !TextUtils.isEmpty(getText());
    }

    public void setTitle(CharSequence charSequence) {
        this.b = charSequence;
        setContentDescription(this.b);
        e();
    }

    @Override // android.support.v7.internal.view.menu.j
    public boolean c() {
        return b() && this.a.getIcon() == null;
    }

    @Override // android.support.v7.internal.view.menu.j
    public boolean d() {
        return b();
    }

    public boolean onLongClick(View view) {
        if (b()) {
            return false;
        }
        int[] iArr = new int[2];
        Rect rect = new Rect();
        getLocationOnScreen(iArr);
        getWindowVisibleDisplayFrame(rect);
        Context context = getContext();
        int width = getWidth();
        int height = getHeight();
        int i = iArr[1] + (height / 2);
        int i2 = context.getResources().getDisplayMetrics().widthPixels;
        Toast makeText = Toast.makeText(context, this.a.getTitle(), 0);
        if (i < rect.height()) {
            makeText.setGravity(53, (i2 - iArr[0]) - (width / 2), height);
        } else {
            makeText.setGravity(81, 0, height);
        }
        makeText.show();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        boolean b2 = b();
        if (b2 && this.h >= 0) {
            super.setPadding(this.h, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        super.onMeasure(i, i2);
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int measuredWidth = getMeasuredWidth();
        int min = mode == Integer.MIN_VALUE ? Math.min(size, this.g) : this.g;
        if (mode != 1073741824 && this.g > 0 && measuredWidth < min) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(min, 1073741824), i2);
        }
        if (!b2 && this.c != null) {
            super.setPadding((getMeasuredWidth() - this.c.getIntrinsicWidth()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
    }
}
