package android.support.v7.internal.view.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/* compiled from: MyApp */
public final class ExpandedMenuView extends ListView implements ah, s, AdapterView.OnItemClickListener {
    private q a;
    private int b;

    public ExpandedMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOnItemClickListener(this);
    }

    @Override // android.support.v7.internal.view.menu.ah
    public void a(q qVar) {
        this.a = qVar;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    @Override // android.support.v7.internal.view.menu.s
    public boolean a(u uVar) {
        return this.a.a(uVar, 0);
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        a((u) getAdapter().getItem(i));
    }

    public int getWindowAnimations() {
        return this.b;
    }
}
