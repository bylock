package android.support.v7.internal.view.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug;
import android.widget.LinearLayout;

/* compiled from: MyApp */
public class k extends LinearLayout.LayoutParams {
    @ViewDebug.ExportedProperty
    public boolean a;
    @ViewDebug.ExportedProperty
    public int b;
    @ViewDebug.ExportedProperty
    public int c;
    @ViewDebug.ExportedProperty
    public boolean d;
    @ViewDebug.ExportedProperty
    public boolean e;
    public boolean f;

    public k(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public k(k kVar) {
        super((LinearLayout.LayoutParams) kVar);
        this.a = kVar.a;
    }

    public k(int i, int i2) {
        super(i, i2);
        this.a = false;
    }
}
