package android.support.v7.internal.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.b.a.a;
import android.support.v4.view.ac;
import android.support.v4.view.n;
import android.support.v7.b.d;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: MyApp */
public class q implements a {
    private static final int[] d = {1, 4, 5, 3, 2, 0};
    CharSequence a;
    Drawable b;
    View c;
    private final Context e;
    private final Resources f;
    private boolean g;
    private boolean h;
    private r i;
    private ArrayList j;
    private ArrayList k;
    private boolean l;
    private ArrayList m;
    private ArrayList n;
    private boolean o;
    private int p = 0;
    private ContextMenu.ContextMenuInfo q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private ArrayList v = new ArrayList();
    private CopyOnWriteArrayList w = new CopyOnWriteArrayList();
    private u x;

    public q(Context context) {
        this.e = context;
        this.f = context.getResources();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.l = true;
        this.m = new ArrayList();
        this.n = new ArrayList();
        this.o = true;
        d(true);
    }

    public q a(int i2) {
        this.p = i2;
        return this;
    }

    public void a(af afVar) {
        this.w.add(new WeakReference(afVar));
        afVar.a(this.e, this);
        this.o = true;
    }

    public void b(af afVar) {
        Iterator it = this.w.iterator();
        while (it.hasNext()) {
            WeakReference weakReference = (WeakReference) it.next();
            af afVar2 = (af) weakReference.get();
            if (afVar2 == null || afVar2 == afVar) {
                this.w.remove(weakReference);
            }
        }
    }

    private void c(boolean z) {
        if (!this.w.isEmpty()) {
            g();
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                af afVar = (af) weakReference.get();
                if (afVar == null) {
                    this.w.remove(weakReference);
                } else {
                    afVar.c(z);
                }
            }
            h();
        }
    }

    private boolean a(al alVar) {
        boolean z = false;
        if (this.w.isEmpty()) {
            return false;
        }
        Iterator it = this.w.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            WeakReference weakReference = (WeakReference) it.next();
            af afVar = (af) weakReference.get();
            if (afVar == null) {
                this.w.remove(weakReference);
            } else if (!z2) {
                z2 = afVar.a(alVar);
            }
            z = z2;
        }
    }

    public void a(Bundle bundle) {
        int size = size();
        int i2 = 0;
        SparseArray<Parcelable> sparseArray = null;
        while (i2 < size) {
            MenuItem item = getItem(i2);
            View a2 = ac.a(item);
            if (!(a2 == null || a2.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray<>();
                }
                a2.saveHierarchyState(sparseArray);
                if (ac.c(item)) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            if (item.hasSubMenu()) {
                ((al) item.getSubMenu()).a(bundle);
            }
            i2++;
            sparseArray = sparseArray;
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(a(), sparseArray);
        }
    }

    public void b(Bundle bundle) {
        MenuItem findItem;
        if (bundle != null) {
            SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray(a());
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItem item = getItem(i2);
                View a2 = ac.a(item);
                if (!(a2 == null || a2.getId() == -1)) {
                    a2.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((al) item.getSubMenu()).b(bundle);
                }
            }
            int i3 = bundle.getInt("android:menu:expandedactionview");
            if (i3 > 0 && (findItem = findItem(i3)) != null) {
                ac.b(findItem);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "android:menu:actionviewstates";
    }

    public void a(r rVar) {
        this.i = rVar;
    }

    private MenuItem a(int i2, int i3, int i4, CharSequence charSequence) {
        int d2 = d(i4);
        u uVar = new u(this, i2, i3, i4, d2, charSequence, this.p);
        if (this.q != null) {
            uVar.a(this.q);
        }
        this.j.add(a(this.j, d2), uVar);
        b(true);
        return uVar;
    }

    @Override // android.view.Menu
    public MenuItem add(CharSequence charSequence) {
        return a(0, 0, 0, charSequence);
    }

    @Override // android.view.Menu
    public MenuItem add(int i2) {
        return a(0, 0, 0, this.f.getString(i2));
    }

    @Override // android.view.Menu
    public MenuItem add(int i2, int i3, int i4, CharSequence charSequence) {
        return a(i2, i3, i4, charSequence);
    }

    @Override // android.view.Menu
    public MenuItem add(int i2, int i3, int i4, int i5) {
        return a(i2, i3, i4, this.f.getString(i5));
    }

    @Override // android.view.Menu
    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2) {
        return addSubMenu(0, 0, 0, this.f.getString(i2));
    }

    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2, int i3, int i4, CharSequence charSequence) {
        u uVar = (u) a(i2, i3, i4, charSequence);
        al alVar = new al(this.e, this, uVar);
        uVar.a(alVar);
        return alVar;
    }

    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2, int i3, int i4, int i5) {
        return addSubMenu(i2, i3, i4, this.f.getString(i5));
    }

    public int addIntentOptions(int i2, int i3, int i4, ComponentName componentName, Intent[] intentArr, Intent intent, int i5, MenuItem[] menuItemArr) {
        Intent intent2;
        PackageManager packageManager = this.e.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i5 & 1) == 0) {
            removeGroup(i2);
        }
        for (int i6 = 0; i6 < size; i6++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i6);
            if (resolveInfo.specificIndex < 0) {
                intent2 = intent;
            } else {
                intent2 = intentArr[resolveInfo.specificIndex];
            }
            Intent intent3 = new Intent(intent2);
            intent3.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent4 = add(i2, i3, i4, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent3);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent4;
            }
        }
        return size;
    }

    public void removeItem(int i2) {
        a(b(i2), true);
    }

    public void removeGroup(int i2) {
        int c2 = c(i2);
        if (c2 >= 0) {
            int size = this.j.size() - c2;
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                if (i3 >= size || ((u) this.j.get(c2)).getGroupId() != i2) {
                    b(true);
                } else {
                    a(c2, false);
                    i3 = i4;
                }
            }
            b(true);
        }
    }

    private void a(int i2, boolean z) {
        if (i2 >= 0 && i2 < this.j.size()) {
            this.j.remove(i2);
            if (z) {
                b(true);
            }
        }
    }

    public void clear() {
        if (this.x != null) {
            d(this.x);
        }
        this.j.clear();
        b(true);
    }

    /* access modifiers changed from: package-private */
    public void a(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            u uVar = (u) this.j.get(i2);
            if (uVar.getGroupId() == groupId && uVar.f() && uVar.isCheckable()) {
                uVar.b(uVar == menuItem);
            }
        }
    }

    public void setGroupCheckable(int i2, boolean z, boolean z2) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            u uVar = (u) this.j.get(i3);
            if (uVar.getGroupId() == i2) {
                uVar.a(z2);
                uVar.setCheckable(z);
            }
        }
    }

    public void setGroupVisible(int i2, boolean z) {
        boolean z2;
        int size = this.j.size();
        int i3 = 0;
        boolean z3 = false;
        while (i3 < size) {
            u uVar = (u) this.j.get(i3);
            if (uVar.getGroupId() != i2 || !uVar.c(z)) {
                z2 = z3;
            } else {
                z2 = true;
            }
            i3++;
            z3 = z2;
        }
        if (z3) {
            b(true);
        }
    }

    public void setGroupEnabled(int i2, boolean z) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            u uVar = (u) this.j.get(i3);
            if (uVar.getGroupId() == i2) {
                uVar.setEnabled(z);
            }
        }
    }

    public boolean hasVisibleItems() {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (((u) this.j.get(i2)).isVisible()) {
                return true;
            }
        }
        return false;
    }

    public MenuItem findItem(int i2) {
        MenuItem findItem;
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            u uVar = (u) this.j.get(i3);
            if (uVar.getItemId() == i2) {
                return uVar;
            }
            if (uVar.hasSubMenu() && (findItem = uVar.getSubMenu().findItem(i2)) != null) {
                return findItem;
            }
        }
        return null;
    }

    public int b(int i2) {
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            if (((u) this.j.get(i3)).getItemId() == i2) {
                return i3;
            }
        }
        return -1;
    }

    public int c(int i2) {
        return a(i2, 0);
    }

    public int a(int i2, int i3) {
        int size = size();
        if (i3 < 0) {
            i3 = 0;
        }
        for (int i4 = i3; i4 < size; i4++) {
            if (((u) this.j.get(i4)).getGroupId() == i2) {
                return i4;
            }
        }
        return -1;
    }

    public int size() {
        return this.j.size();
    }

    public MenuItem getItem(int i2) {
        return (MenuItem) this.j.get(i2);
    }

    public boolean isShortcutKey(int i2, KeyEvent keyEvent) {
        return a(i2, keyEvent) != null;
    }

    public void setQwertyMode(boolean z) {
        this.g = z;
        b(false);
    }

    private static int d(int i2) {
        int i3 = (-65536 & i2) >> 16;
        if (i3 >= 0 && i3 < d.length) {
            return (d[i3] << 16) | (65535 & i2);
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.g;
    }

    private void d(boolean z) {
        boolean z2 = true;
        if (!z || this.f.getConfiguration().keyboard == 1 || !this.f.getBoolean(d.abc_config_showMenuShortcutsWhenKeyboardPresent)) {
            z2 = false;
        }
        this.h = z2;
    }

    public boolean c() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public Resources d() {
        return this.f;
    }

    public Context e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean a(q qVar, MenuItem menuItem) {
        return this.i != null && this.i.a(qVar, menuItem);
    }

    public void f() {
        if (this.i != null) {
            this.i.a(this);
        }
    }

    private static int a(ArrayList arrayList, int i2) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (((u) arrayList.get(size)).b() <= i2) {
                return size + 1;
            }
        }
        return 0;
    }

    public boolean performShortcut(int i2, KeyEvent keyEvent, int i3) {
        u a2 = a(i2, keyEvent);
        boolean z = false;
        if (a2 != null) {
            z = a(a2, i3);
        }
        if ((i3 & 2) != 0) {
            a(true);
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void a(List list, int i2, KeyEvent keyEvent) {
        boolean b2 = b();
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i2 == 67) {
            int size = this.j.size();
            for (int i3 = 0; i3 < size; i3++) {
                u uVar = (u) this.j.get(i3);
                if (uVar.hasSubMenu()) {
                    ((q) uVar.getSubMenu()).a(list, i2, keyEvent);
                }
                char alphabeticShortcut = b2 ? uVar.getAlphabeticShortcut() : uVar.getNumericShortcut();
                if ((metaState & 5) == 0 && alphabeticShortcut != 0 && ((alphabeticShortcut == keyData.meta[0] || alphabeticShortcut == keyData.meta[2] || (b2 && alphabeticShortcut == '\b' && i2 == 67)) && uVar.isEnabled())) {
                    list.add(uVar);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public u a(int i2, KeyEvent keyEvent) {
        ArrayList arrayList = this.v;
        arrayList.clear();
        a(arrayList, i2, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return (u) arrayList.get(0);
        }
        boolean b2 = b();
        for (int i3 = 0; i3 < size; i3++) {
            u uVar = (u) arrayList.get(i3);
            char alphabeticShortcut = b2 ? uVar.getAlphabeticShortcut() : uVar.getNumericShortcut();
            if (alphabeticShortcut == keyData.meta[0] && (metaState & 2) == 0) {
                return uVar;
            }
            if (alphabeticShortcut == keyData.meta[2] && (metaState & 2) != 0) {
                return uVar;
            }
            if (b2 && alphabeticShortcut == '\b' && i2 == 67) {
                return uVar;
            }
        }
        return null;
    }

    public boolean performIdentifierAction(int i2, int i3) {
        return a(findItem(i2), i3);
    }

    public boolean a(MenuItem menuItem, int i2) {
        boolean z;
        u uVar = (u) menuItem;
        if (uVar == null || !uVar.isEnabled()) {
            return false;
        }
        boolean a2 = uVar.a();
        n m2 = uVar.m();
        if (m2 == null || !m2.g()) {
            z = false;
        } else {
            z = true;
        }
        if (uVar.n()) {
            boolean expandActionView = uVar.expandActionView() | a2;
            if (!expandActionView) {
                return expandActionView;
            }
            a(true);
            return expandActionView;
        } else if (uVar.hasSubMenu() || z) {
            a(false);
            if (!uVar.hasSubMenu()) {
                uVar.a(new al(e(), this, uVar));
            }
            al alVar = (al) uVar.getSubMenu();
            if (z) {
                m2.a(alVar);
            }
            boolean a3 = a(alVar) | a2;
            if (a3) {
                return a3;
            }
            a(true);
            return a3;
        } else {
            if ((i2 & 1) == 0) {
                a(true);
            }
            return a2;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        if (!this.u) {
            this.u = true;
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                af afVar = (af) weakReference.get();
                if (afVar == null) {
                    this.w.remove(weakReference);
                } else {
                    afVar.a(this, z);
                }
            }
            this.u = false;
        }
    }

    public void close() {
        a(true);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        if (!this.r) {
            if (z) {
                this.l = true;
                this.o = true;
            }
            c(z);
            return;
        }
        this.s = true;
    }

    public void g() {
        if (!this.r) {
            this.r = true;
            this.s = false;
        }
    }

    public void h() {
        this.r = false;
        if (this.s) {
            this.s = false;
            b(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(u uVar) {
        this.l = true;
        b(true);
    }

    /* access modifiers changed from: package-private */
    public void b(u uVar) {
        this.o = true;
        b(true);
    }

    /* access modifiers changed from: package-private */
    public ArrayList i() {
        if (!this.l) {
            return this.k;
        }
        this.k.clear();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            u uVar = (u) this.j.get(i2);
            if (uVar.isVisible()) {
                this.k.add(uVar);
            }
        }
        this.l = false;
        this.o = true;
        return this.k;
    }

    public void j() {
        boolean g2;
        if (this.o) {
            Iterator it = this.w.iterator();
            boolean z = false;
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                af afVar = (af) weakReference.get();
                if (afVar == null) {
                    this.w.remove(weakReference);
                    g2 = z;
                } else {
                    g2 = afVar.g() | z;
                }
                z = g2;
            }
            if (z) {
                this.m.clear();
                this.n.clear();
                ArrayList i2 = i();
                int size = i2.size();
                for (int i3 = 0; i3 < size; i3++) {
                    u uVar = (u) i2.get(i3);
                    if (uVar.i()) {
                        this.m.add(uVar);
                    } else {
                        this.n.add(uVar);
                    }
                }
            } else {
                this.m.clear();
                this.n.clear();
                this.n.addAll(i());
            }
            this.o = false;
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList k() {
        j();
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public ArrayList l() {
        j();
        return this.n;
    }

    public void clearHeader() {
        this.b = null;
        this.a = null;
        this.c = null;
        b(false);
    }

    private void a(int i2, CharSequence charSequence, int i3, Drawable drawable, View view) {
        Resources d2 = d();
        if (view != null) {
            this.c = view;
            this.a = null;
            this.b = null;
        } else {
            if (i2 > 0) {
                this.a = d2.getText(i2);
            } else if (charSequence != null) {
                this.a = charSequence;
            }
            if (i3 > 0) {
                this.b = d2.getDrawable(i3);
            } else if (drawable != null) {
                this.b = drawable;
            }
            this.c = null;
        }
        b(false);
    }

    /* access modifiers changed from: protected */
    public q a(CharSequence charSequence) {
        a(0, charSequence, 0, null, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public q a(Drawable drawable) {
        a(0, null, 0, drawable, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public q a(View view) {
        a(0, null, 0, null, view);
        return this;
    }

    public CharSequence m() {
        return this.a;
    }

    public Drawable n() {
        return this.b;
    }

    public View o() {
        return this.c;
    }

    public q p() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return this.t;
    }

    public boolean c(u uVar) {
        boolean z = false;
        if (!this.w.isEmpty()) {
            g();
            Iterator it = this.w.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = z;
                    break;
                }
                WeakReference weakReference = (WeakReference) it.next();
                af afVar = (af) weakReference.get();
                if (afVar == null) {
                    this.w.remove(weakReference);
                    z = z;
                } else {
                    z = afVar.a(this, uVar);
                    if (z) {
                        break;
                    }
                }
            }
            h();
            if (z) {
                this.x = uVar;
            }
        }
        return z;
    }

    public boolean d(u uVar) {
        boolean z = false;
        if (!this.w.isEmpty() && this.x == uVar) {
            g();
            Iterator it = this.w.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = z;
                    break;
                }
                WeakReference weakReference = (WeakReference) it.next();
                af afVar = (af) weakReference.get();
                if (afVar == null) {
                    this.w.remove(weakReference);
                    z = z;
                } else {
                    z = afVar.b(this, uVar);
                    if (z) {
                        break;
                    }
                }
            }
            h();
            if (z) {
                this.x = null;
            }
        }
        return z;
    }

    public u r() {
        return this.x;
    }
}
