package android.support.v7.internal.view.menu;

import android.view.View;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class e implements Runnable {
    final /* synthetic */ ActionMenuPresenter a;
    private g b;

    public e(ActionMenuPresenter actionMenuPresenter, g gVar) {
        this.a = actionMenuPresenter;
        this.b = gVar;
    }

    public void run() {
        this.a.e.f();
        View view = (View) this.a.h;
        if (!(view == null || view.getWindowToken() == null || !this.b.a())) {
            ActionMenuPresenter.a(this.a, this.b);
        }
        ActionMenuPresenter.a(this.a, (e) null);
    }
}
