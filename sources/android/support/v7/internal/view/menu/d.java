package android.support.v7.internal.view.menu;

import android.content.DialogInterface;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class d extends t {
    final /* synthetic */ ActionMenuPresenter a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(ActionMenuPresenter actionMenuPresenter, al alVar) {
        super(alVar);
        this.a = actionMenuPresenter;
        actionMenuPresenter.a(actionMenuPresenter.a);
    }

    @Override // android.support.v7.internal.view.menu.t
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ActionMenuPresenter.a(this.a, (d) null);
        this.a.b = 0;
    }
}
