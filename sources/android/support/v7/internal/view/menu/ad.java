package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.b.c;
import android.support.v7.b.e;
import android.support.v7.b.h;
import android.support.v7.internal.widget.u;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.PopupWindow;

/* compiled from: MyApp */
public class ad implements af, View.OnKeyListener, ViewTreeObserver.OnGlobalLayoutListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {
    static final int b = h.abc_popup_menu_item_layout;
    private Context a;
    boolean c;
    private LayoutInflater d;
    private u e;
    private q f;
    private int g;
    private View h;
    private boolean i;
    private ViewTreeObserver j;
    private ae k;
    private ag l;
    private ViewGroup m;

    public ad(Context context, q qVar, View view, boolean z) {
        this.a = context;
        this.d = LayoutInflater.from(context);
        this.f = qVar;
        this.i = z;
        Resources resources = context.getResources();
        this.g = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(e.abc_config_prefDialogWidth));
        this.h = view;
        qVar.a(this);
    }

    public void a(boolean z) {
        this.c = z;
    }

    public boolean a() {
        boolean z = false;
        this.e = new u(this.a, null, c.popupMenuStyle);
        this.e.a((PopupWindow.OnDismissListener) this);
        this.e.a((AdapterView.OnItemClickListener) this);
        this.k = new ae(this, this.f);
        this.e.a(this.k);
        this.e.a(true);
        View view = this.h;
        if (view == null) {
            return false;
        }
        if (this.j == null) {
            z = true;
        }
        this.j = view.getViewTreeObserver();
        if (z) {
            this.j.addOnGlobalLayoutListener(this);
        }
        this.e.a(view);
        this.e.e(Math.min(a(this.k), this.g));
        this.e.f(2);
        this.e.c();
        this.e.h().setOnKeyListener(this);
        return true;
    }

    public void b() {
        if (c()) {
            this.e.d();
        }
    }

    public void onDismiss() {
        this.e = null;
        this.f.close();
        if (this.j != null) {
            if (!this.j.isAlive()) {
                this.j = this.h.getViewTreeObserver();
            }
            this.j.removeGlobalOnLayoutListener(this);
            this.j = null;
        }
    }

    public boolean c() {
        return this.e != null && this.e.f();
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        ae aeVar = this.k;
        ae.a(aeVar).a(aeVar.getItem(i2), 0);
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        b();
        return true;
    }

    private int a(ListAdapter listAdapter) {
        View view;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        int i2 = 0;
        int i3 = 0;
        View view2 = null;
        int i4 = 0;
        while (i2 < count) {
            int itemViewType = listAdapter.getItemViewType(i2);
            if (itemViewType != i3) {
                view = null;
            } else {
                itemViewType = i3;
                view = view2;
            }
            if (this.m == null) {
                this.m = new FrameLayout(this.a);
            }
            view2 = listAdapter.getView(i2, view, this.m);
            view2.measure(makeMeasureSpec, makeMeasureSpec2);
            i4 = Math.max(i4, view2.getMeasuredWidth());
            i2++;
            i3 = itemViewType;
        }
        return i4;
    }

    public void onGlobalLayout() {
        if (c()) {
            View view = this.h;
            if (view == null || !view.isShown()) {
                b();
            } else if (c()) {
                this.e.c();
            }
        }
    }

    @Override // android.support.v7.internal.view.menu.af
    public void a(Context context, q qVar) {
    }

    @Override // android.support.v7.internal.view.menu.af
    public void c(boolean z) {
        if (this.k != null) {
            this.k.notifyDataSetChanged();
        }
    }

    public void a(ag agVar) {
        this.l = agVar;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean a(al alVar) {
        boolean z;
        if (alVar.hasVisibleItems()) {
            ad adVar = new ad(this.a, alVar, this.h, false);
            adVar.a(this.l);
            int size = alVar.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    z = false;
                    break;
                }
                MenuItem item = alVar.getItem(i2);
                if (item.isVisible() && item.getIcon() != null) {
                    z = true;
                    break;
                }
                i2++;
            }
            adVar.a(z);
            if (adVar.a()) {
                if (this.l == null) {
                    return true;
                }
                this.l.b(alVar);
                return true;
            }
        }
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public void a(q qVar, boolean z) {
        if (qVar == this.f) {
            b();
            if (this.l != null) {
                this.l.a(qVar, z);
            }
        }
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean g() {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean a(q qVar, u uVar) {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean b(q qVar, u uVar) {
        return false;
    }
}
