package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v7.b.h;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;

/* compiled from: MyApp */
public class o implements af, AdapterView.OnItemClickListener {
    Context a;
    LayoutInflater b;
    q c;
    ExpandedMenuView d;
    int e;
    int f;
    p g;
    private int h;
    private ag i;

    public o(int i2, int i3) {
        this.f = i2;
        this.e = i3;
    }

    @Override // android.support.v7.internal.view.menu.af
    public void a(Context context, q qVar) {
        if (this.e != 0) {
            this.a = new ContextThemeWrapper(context, this.e);
            this.b = LayoutInflater.from(this.a);
        } else if (this.a != null) {
            this.a = context;
            if (this.b == null) {
                this.b = LayoutInflater.from(this.a);
            }
        }
        this.c = qVar;
        if (this.g != null) {
            this.g.notifyDataSetChanged();
        }
    }

    public ah a(ViewGroup viewGroup) {
        if (this.g == null) {
            this.g = new p(this);
        }
        if (this.g.isEmpty()) {
            return null;
        }
        if (this.d == null) {
            this.d = (ExpandedMenuView) this.b.inflate(h.abc_expanded_menu_layout, viewGroup, false);
            this.d.setAdapter((ListAdapter) this.g);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    public ListAdapter a() {
        if (this.g == null) {
            this.g = new p(this);
        }
        return this.g;
    }

    @Override // android.support.v7.internal.view.menu.af
    public void c(boolean z) {
        if (this.g != null) {
            this.g.notifyDataSetChanged();
        }
    }

    public void a(ag agVar) {
        this.i = agVar;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean a(al alVar) {
        if (!alVar.hasVisibleItems()) {
            return false;
        }
        new t(alVar).a(null);
        if (this.i != null) {
            this.i.b(alVar);
        }
        return true;
    }

    @Override // android.support.v7.internal.view.menu.af
    public void a(q qVar, boolean z) {
        if (this.i != null) {
            this.i.a(qVar, z);
        }
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i2, long j) {
        this.c.a(this.g.getItem(i2), 0);
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean g() {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean a(q qVar, u uVar) {
        return false;
    }

    @Override // android.support.v7.internal.view.menu.af
    public boolean b(q qVar, u uVar) {
        return false;
    }
}
