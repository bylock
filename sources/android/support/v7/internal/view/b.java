package android.support.v7.internal.view;

import android.content.Context;
import android.support.v7.c.a;
import android.support.v7.internal.view.menu.aj;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;

/* compiled from: MyApp */
public class b extends a {
    final MenuInflater a;
    final ActionMode b;

    public b(Context context, ActionMode actionMode) {
        this.b = actionMode;
        this.a = new f(context);
    }

    @Override // android.support.v7.c.a
    public void b() {
        this.b.finish();
    }

    @Override // android.support.v7.c.a
    public Menu a() {
        return aj.a(this.b.getMenu());
    }
}
