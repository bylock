package android.support.v7.internal.view;

import android.content.Context;
import android.support.v7.c.b;
import android.view.ActionMode;

/* compiled from: MyApp */
public class e extends c {
    public e(Context context, b bVar) {
        super(context, bVar);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v7.internal.view.c
    public b a(Context context, ActionMode actionMode) {
        return new d(context, actionMode);
    }
}
