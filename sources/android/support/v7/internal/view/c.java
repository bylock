package android.support.v7.internal.view;

import android.content.Context;
import android.support.v7.c.a;
import android.support.v7.c.b;
import android.support.v7.internal.view.menu.aj;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

/* compiled from: MyApp */
public class c implements ActionMode.Callback {
    final b a;
    final Context b;
    private b c;

    public c(Context context, b bVar) {
        this.b = context;
        this.a = bVar;
    }

    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        return this.a.a(a(actionMode), aj.a(menu));
    }

    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return this.a.b(a(actionMode), aj.a(menu));
    }

    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return this.a.a(a(actionMode), aj.a(menuItem));
    }

    public void onDestroyActionMode(ActionMode actionMode) {
        this.a.a(a(actionMode));
    }

    public void a(b bVar) {
        this.c = bVar;
    }

    private a a(ActionMode actionMode) {
        if (this.c == null || this.c.b != actionMode) {
            return a(this.b, actionMode);
        }
        return this.c;
    }

    /* access modifiers changed from: protected */
    public b a(Context context, ActionMode actionMode) {
        return new b(context, actionMode);
    }
}
