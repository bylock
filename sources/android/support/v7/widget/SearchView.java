package android.support.v7.widget;

import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.a;
import android.support.v7.b.e;
import android.support.v7.c.c;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.WeakHashMap;

/* compiled from: MyApp */
public class SearchView extends LinearLayout implements c {
    static final a a = new a();
    private Bundle A;
    private Runnable B;
    private Runnable C;
    private Runnable D;
    private final Intent E;
    private final Intent F;
    private final WeakHashMap G;
    private c b;
    private b c;
    private View.OnFocusChangeListener d;
    private d e;
    private View.OnClickListener f;
    private boolean g;
    private boolean h;
    private a i;
    private View j;
    private View k;
    private View l;
    private ImageView m;
    private View n;
    private View o;
    private SearchAutoComplete p;
    private ImageView q;
    private boolean r;
    private CharSequence s;
    private boolean t;
    private boolean u;
    private int v;
    private boolean w;
    private boolean x;
    private int y;
    private SearchableInfo z;

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.z = searchableInfo;
        if (this.z != null) {
            k();
            j();
        }
        this.w = e();
        if (this.w) {
            this.p.setPrivateImeOptions("nm");
        }
        a(c());
    }

    public void setAppSearchData(Bundle bundle) {
        this.A = bundle;
    }

    public void setImeOptions(int i2) {
        this.p.setImeOptions(i2);
    }

    public int getImeOptions() {
        return this.p.getImeOptions();
    }

    public void setInputType(int i2) {
        this.p.setInputType(i2);
    }

    public int getInputType() {
        return this.p.getInputType();
    }

    public boolean requestFocus(int i2, Rect rect) {
        if (this.u || !isFocusable()) {
            return false;
        }
        if (c()) {
            return super.requestFocus(i2, rect);
        }
        boolean requestFocus = this.p.requestFocus(i2, rect);
        if (requestFocus) {
            a(false);
        }
        return requestFocus;
    }

    public void clearFocus() {
        this.u = true;
        setImeVisibility(false);
        super.clearFocus();
        this.p.clearFocus();
        this.u = false;
    }

    public void setOnQueryTextListener(c cVar) {
        this.b = cVar;
    }

    public void setOnCloseListener(b bVar) {
        this.c = bVar;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.d = onFocusChangeListener;
    }

    public void setOnSuggestionListener(d dVar) {
        this.e = dVar;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.f = onClickListener;
    }

    public CharSequence getQuery() {
        return this.p.getText();
    }

    public void setQueryHint(CharSequence charSequence) {
        this.s = charSequence;
        j();
    }

    public CharSequence getQueryHint() {
        int hintId;
        if (this.s != null) {
            return this.s;
        }
        if (this.z == null || (hintId = this.z.getHintId()) == 0) {
            return null;
        }
        return getContext().getString(hintId);
    }

    public void setIconifiedByDefault(boolean z2) {
        if (this.g != z2) {
            this.g = z2;
            a(z2);
            j();
        }
    }

    public void setIconified(boolean z2) {
        if (z2) {
            l();
        } else {
            m();
        }
    }

    public boolean c() {
        return this.h;
    }

    public void setSubmitButtonEnabled(boolean z2) {
        this.r = z2;
        a(c());
    }

    public void setQueryRefinementEnabled(boolean z2) {
        this.t = z2;
        if (this.i instanceof e) {
            ((e) this.i).a(z2 ? 2 : 1);
        }
    }

    public void setSuggestionsAdapter(a aVar) {
        this.i = aVar;
        this.p.setAdapter(this.i);
    }

    public a getSuggestionsAdapter() {
        return this.i;
    }

    public void setMaxWidth(int i2) {
        this.v = i2;
        requestLayout();
    }

    public int getMaxWidth() {
        return this.v;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (c()) {
            super.onMeasure(i2, i3);
            return;
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.v <= 0) {
                    size = Math.min(getPreferredWidth(), size);
                    break;
                } else {
                    size = Math.min(this.v, size);
                    break;
                }
            case 0:
                if (this.v <= 0) {
                    size = getPreferredWidth();
                    break;
                } else {
                    size = this.v;
                    break;
                }
            case 1073741824:
                if (this.v > 0) {
                    size = Math.min(this.v, size);
                    break;
                }
                break;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), i3);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(e.abc_search_view_preferred_width);
    }

    private void a(boolean z2) {
        boolean z3;
        int i2;
        boolean z4 = true;
        int i3 = 8;
        this.h = z2;
        int i4 = z2 ? 0 : 8;
        if (!TextUtils.isEmpty(this.p.getText())) {
            z3 = true;
        } else {
            z3 = false;
        }
        this.j.setVisibility(i4);
        b(z3);
        View view = this.n;
        if (z2) {
            i2 = 8;
        } else {
            i2 = 0;
        }
        view.setVisibility(i2);
        ImageView imageView = this.q;
        if (!this.g) {
            i3 = 0;
        }
        imageView.setVisibility(i3);
        h();
        if (z3) {
            z4 = false;
        }
        c(z4);
        g();
    }

    private boolean e() {
        if (this.z == null || !this.z.getVoiceSearchEnabled()) {
            return false;
        }
        Intent intent = null;
        if (this.z.getVoiceSearchLaunchWebSearch()) {
            intent = this.E;
        } else if (this.z.getVoiceSearchLaunchRecognizer()) {
            intent = this.F;
        }
        if (intent == null || getContext().getPackageManager().resolveActivity(intent, 65536) == null) {
            return false;
        }
        return true;
    }

    private boolean f() {
        return (this.r || this.w) && !c();
    }

    private void b(boolean z2) {
        int i2 = 8;
        if (this.r && f() && hasFocus() && (z2 || !this.w)) {
            i2 = 0;
        }
        this.k.setVisibility(i2);
    }

    private void g() {
        int i2 = 8;
        if (f() && (this.k.getVisibility() == 0 || this.o.getVisibility() == 0)) {
            i2 = 0;
        }
        this.l.setVisibility(i2);
    }

    private void h() {
        boolean z2 = true;
        int i2 = 0;
        boolean z3 = !TextUtils.isEmpty(this.p.getText());
        if (!z3 && (!this.g || this.x)) {
            z2 = false;
        }
        ImageView imageView = this.m;
        if (!z2) {
            i2 = 8;
        }
        imageView.setVisibility(i2);
        this.m.getDrawable().setState(z3 ? ENABLED_STATE_SET : EMPTY_STATE_SET);
    }

    private void i() {
        post(this.C);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.C);
        post(this.D);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setImeVisibility(boolean z2) {
        if (z2) {
            post(this.B);
            return;
        }
        removeCallbacks(this.B);
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        setQuery(charSequence);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.z == null) {
            return false;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    private int getSearchIconId() {
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.support.v7.b.c.searchViewSearchIcon, typedValue, true);
        return typedValue.resourceId;
    }

    private CharSequence b(CharSequence charSequence) {
        if (!this.g) {
            return charSequence;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
        spannableStringBuilder.append(charSequence);
        Drawable drawable = getContext().getResources().getDrawable(getSearchIconId());
        int textSize = (int) (((double) this.p.getTextSize()) * 1.25d);
        drawable.setBounds(0, 0, textSize, textSize);
        spannableStringBuilder.setSpan(new ImageSpan(drawable), 1, 2, 33);
        return spannableStringBuilder;
    }

    private void j() {
        if (this.s != null) {
            this.p.setHint(b(this.s));
        } else if (this.z != null) {
            String str = null;
            int hintId = this.z.getHintId();
            if (hintId != 0) {
                str = getContext().getString(hintId);
            }
            if (str != null) {
                this.p.setHint(b(str));
            }
        } else {
            this.p.setHint(b(""));
        }
    }

    private void k() {
        int i2 = 1;
        this.p.setThreshold(this.z.getSuggestThreshold());
        this.p.setImeOptions(this.z.getImeOptions());
        int inputType = this.z.getInputType();
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.z.getSuggestAuthority() != null) {
                inputType = inputType | 65536 | 524288;
            }
        }
        this.p.setInputType(inputType);
        if (this.i != null) {
            this.i.a((Cursor) null);
        }
        if (this.z.getSuggestAuthority() != null) {
            this.i = new e(getContext(), this, this.z, this.G);
            this.p.setAdapter(this.i);
            e eVar = (e) this.i;
            if (this.t) {
                i2 = 2;
            }
            eVar.a(i2);
        }
    }

    private void c(boolean z2) {
        int i2;
        if (!this.w || c() || !z2) {
            i2 = 8;
        } else {
            i2 = 0;
            this.k.setVisibility(8);
        }
        this.o.setVisibility(i2);
    }

    private void l() {
        if (!TextUtils.isEmpty(this.p.getText())) {
            this.p.setText("");
            this.p.requestFocus();
            setImeVisibility(true);
        } else if (!this.g) {
        } else {
            if (this.c == null || !this.c.a()) {
                clearFocus();
                a(true);
            }
        }
    }

    private void m() {
        a(false);
        this.p.requestFocus();
        setImeVisibility(true);
        if (this.f != null) {
            this.f.onClick(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        a(c());
        i();
        if (this.p.hasFocus()) {
            n();
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        i();
    }

    @Override // android.support.v7.c.c
    public void b() {
        clearFocus();
        a(true);
        this.p.setImeOptions(this.y);
        this.x = false;
    }

    @Override // android.support.v7.c.c
    public void a() {
        if (!this.x) {
            this.x = true;
            this.y = this.p.getImeOptions();
            this.p.setImeOptions(this.y | 33554432);
            this.p.setText("");
            setIconified(false);
        }
    }

    private void setQuery(CharSequence charSequence) {
        this.p.setText(charSequence);
        this.p.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    private void n() {
        a.a(this.p);
        a.b(this.p);
    }

    static boolean a(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }

    /* compiled from: MyApp */
    public class SearchAutoComplete extends AutoCompleteTextView {
        private int a = getThreshold();
        private SearchView b;

        public SearchAutoComplete(Context context) {
            super(context);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
        }

        /* access modifiers changed from: package-private */
        public void setSearchView(SearchView searchView) {
            this.b = searchView;
        }

        public void setThreshold(int i) {
            super.setThreshold(i);
            this.a = i;
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        public void performCompletion() {
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.b.hasFocus() && getVisibility() == 0) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                if (SearchView.a(getContext())) {
                    SearchView.a.a(this, true);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.b.d();
        }

        public boolean enoughToFilter() {
            return this.a <= 0 || super.enoughToFilter();
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState == null) {
                        return true;
                    }
                    keyDispatcherState.startTracking(keyEvent, this);
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.b.clearFocus();
                        this.b.setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }
    }
}
