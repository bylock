package android.support.v7.widget;

import android.os.ResultReceiver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import java.lang.reflect.Method;

/* compiled from: MyApp */
class a {
    private Method a;
    private Method b;
    private Method c;
    private Method d;

    a() {
        try {
            this.a = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
            this.a.setAccessible(true);
        } catch (NoSuchMethodException e) {
        }
        try {
            this.b = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
            this.b.setAccessible(true);
        } catch (NoSuchMethodException e2) {
        }
        try {
            this.c = AutoCompleteTextView.class.getMethod("ensureImeVisible", Boolean.TYPE);
            this.c.setAccessible(true);
        } catch (NoSuchMethodException e3) {
        }
        try {
            this.d = InputMethodManager.class.getMethod("showSoftInputUnchecked", Integer.TYPE, ResultReceiver.class);
            this.d.setAccessible(true);
        } catch (NoSuchMethodException e4) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(AutoCompleteTextView autoCompleteTextView) {
        if (this.a != null) {
            try {
                this.a.invoke(autoCompleteTextView, new Object[0]);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(AutoCompleteTextView autoCompleteTextView) {
        if (this.b != null) {
            try {
                this.b.invoke(autoCompleteTextView, new Object[0]);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(AutoCompleteTextView autoCompleteTextView, boolean z) {
        if (this.c != null) {
            try {
                this.c.invoke(autoCompleteTextView, Boolean.valueOf(z));
            } catch (Exception e) {
            }
        }
    }
}
