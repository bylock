package android.support.v7.a;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.c.a;
import android.support.v7.c.b;
import android.support.v7.internal.view.c;
import android.support.v7.internal.view.menu.aj;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class n extends g {
    Menu d;

    n(f fVar) {
        super(fVar);
    }

    @Override // android.support.v7.a.g
    public a a() {
        return new v(this.a, this.a);
    }

    @Override // android.support.v7.a.g
    public void a(Bundle bundle) {
        if ("splitActionBarWhenNarrow".equals(i())) {
            this.a.getWindow().setUiOptions(1, 1);
        }
        super.a(bundle);
        if (this.b) {
            this.a.requestWindowFeature(8);
        }
        if (this.c) {
            this.a.requestWindowFeature(9);
        }
        Window window = this.a.getWindow();
        window.setCallback(a(window.getCallback()));
    }

    /* access modifiers changed from: package-private */
    public Window.Callback a(Window.Callback callback) {
        return new o(this, callback);
    }

    @Override // android.support.v7.a.g
    public void a(Configuration configuration) {
    }

    @Override // android.support.v7.a.g
    public void d() {
    }

    @Override // android.support.v7.a.g
    public void e() {
    }

    @Override // android.support.v7.a.g
    public void a(View view) {
        this.a.a(view);
    }

    @Override // android.support.v7.a.g
    public void a(int i) {
        this.a.a(i);
    }

    @Override // android.support.v7.a.g
    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        this.a.a(view, layoutParams);
    }

    @Override // android.support.v7.a.g
    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        this.a.b(view, layoutParams);
    }

    @Override // android.support.v7.a.g
    public void h() {
        this.a.h();
    }

    @Override // android.support.v7.a.g
    public View b(int i) {
        return null;
    }

    @Override // android.support.v7.a.g
    public boolean a(int i, Menu menu) {
        if (i != 0 && i != 8) {
            return this.a.a(i, menu);
        }
        if (this.d == null) {
            this.d = aj.a(menu);
        }
        return this.a.a(i, this.d);
    }

    @Override // android.support.v7.a.g
    public boolean a(int i, View view, Menu menu) {
        if (i == 0 || i == 8) {
            return this.a.a(i, view, this.d);
        }
        return this.a.a(i, view, menu);
    }

    @Override // android.support.v7.a.g
    public boolean a(int i, MenuItem menuItem) {
        if (i == 0) {
            menuItem = aj.a(menuItem);
        }
        return this.a.a(i, menuItem);
    }

    @Override // android.support.v7.a.g
    public void a(CharSequence charSequence) {
    }

    @Override // android.support.v7.a.g
    public a a(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        Context j = j();
        c a = a(j, bVar);
        ActionMode startActionMode = this.a.startActionMode(a);
        if (startActionMode == null) {
            return null;
        }
        android.support.v7.internal.view.b a2 = a(j, startActionMode);
        a.a(a2);
        return a2;
    }

    public void a(ActionMode actionMode) {
        this.a.a(a(j(), actionMode));
    }

    public void b(ActionMode actionMode) {
        this.a.b(a(j(), actionMode));
    }

    @Override // android.support.v7.a.g
    public void f() {
        this.d = null;
    }

    @Override // android.support.v7.a.g
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public c a(Context context, b bVar) {
        return new c(context, bVar);
    }

    /* access modifiers changed from: package-private */
    public android.support.v7.internal.view.b a(Context context, ActionMode actionMode) {
        return new android.support.v7.internal.view.b(context, actionMode);
    }
}
