package android.support.v7.a;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.b.c;
import android.support.v7.b.f;
import android.support.v7.c.a;
import android.support.v7.c.b;
import android.support.v7.internal.widget.ActionBarContainer;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ActionBarOverlayLayout;
import android.support.v7.internal.widget.ActionBarView;
import android.support.v7.internal.widget.ScrollingTabContainerView;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import java.util.ArrayList;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class r extends a {
    private b A;
    s a;
    a b;
    b c;
    final Handler d = new Handler();
    private Context e;
    private Context f;
    private f g;
    private ActionBarOverlayLayout h;
    private ActionBarContainer i;
    private ViewGroup j;
    private ActionBarView k;
    private ActionBarContextView l;
    private ActionBarContainer m;
    private ScrollingTabContainerView n;
    private ArrayList o = new ArrayList();
    private int p = -1;
    private boolean q;
    private ArrayList r = new ArrayList();
    private int s;
    private boolean t;
    private int u = 0;
    private boolean v;
    private boolean w;
    private boolean x;
    private boolean y = true;
    private boolean z;

    public r(f fVar, b bVar) {
        this.g = fVar;
        this.e = fVar;
        this.A = bVar;
        a(this.g);
    }

    private void a(f fVar) {
        boolean z2;
        boolean z3 = false;
        this.h = (ActionBarOverlayLayout) fVar.findViewById(f.action_bar_overlay_layout);
        if (this.h != null) {
            this.h.setActionBar(this);
        }
        this.k = (ActionBarView) fVar.findViewById(f.action_bar);
        this.l = (ActionBarContextView) fVar.findViewById(f.action_context_bar);
        this.i = (ActionBarContainer) fVar.findViewById(f.action_bar_container);
        this.j = (ViewGroup) fVar.findViewById(f.top_action_bar);
        if (this.j == null) {
            this.j = this.i;
        }
        this.m = (ActionBarContainer) fVar.findViewById(f.split_action_bar);
        if (this.k == null || this.l == null || this.i == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.k.setContextView(this.l);
        this.s = this.k.i() ? 1 : 0;
        if ((this.k.getDisplayOptions() & 4) != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2) {
            this.q = true;
        }
        android.support.v7.internal.view.a a2 = android.support.v7.internal.view.a.a(this.e);
        if (a2.f() || z2) {
            z3 = true;
        }
        e(z3);
        j(a2.d());
        a(this.g.getTitle());
    }

    public void a(Configuration configuration) {
        j(android.support.v7.internal.view.a.a(this.e).d());
    }

    private void j(boolean z2) {
        boolean z3;
        boolean z4 = true;
        this.t = z2;
        if (!this.t) {
            this.k.setEmbeddedTabView(null);
            this.i.setTabContainer(this.n);
        } else {
            this.i.setTabContainer(null);
            this.k.setEmbeddedTabView(this.n);
        }
        if (c() == 2) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (this.n != null) {
            if (z3) {
                this.n.setVisibility(0);
            } else {
                this.n.setVisibility(8);
            }
        }
        ActionBarView actionBarView = this.k;
        if (this.t || !z3) {
            z4 = false;
        }
        actionBarView.setCollapsable(z4);
    }

    @Override // android.support.v7.a.a
    public void a(View view) {
        this.k.setCustomNavigationView(view);
    }

    @Override // android.support.v7.a.a
    public void a(int i2) {
        this.k.setLogo(i2);
    }

    @Override // android.support.v7.a.a
    public void a(CharSequence charSequence) {
        this.k.setTitle(charSequence);
    }

    @Override // android.support.v7.a.a
    public void b(int i2) {
        if ((i2 & 4) != 0) {
            this.q = true;
        }
        this.k.setDisplayOptions(i2);
    }

    public void a(int i2, int i3) {
        int displayOptions = this.k.getDisplayOptions();
        if ((i3 & 4) != 0) {
            this.q = true;
        }
        this.k.setDisplayOptions((displayOptions & (i3 ^ -1)) | (i2 & i3));
    }

    @Override // android.support.v7.a.a
    public void a(boolean z2) {
        a(z2 ? 1 : 0, 1);
    }

    @Override // android.support.v7.a.a
    public void b(boolean z2) {
        a(z2 ? 2 : 0, 2);
    }

    @Override // android.support.v7.a.a
    public void c(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    @Override // android.support.v7.a.a
    public void d(boolean z2) {
        a(z2 ? 8 : 0, 8);
    }

    @Override // android.support.v7.a.a
    public void e(boolean z2) {
        this.k.setHomeButtonEnabled(z2);
    }

    public int c() {
        return this.k.getNavigationMode();
    }

    @Override // android.support.v7.a.a
    public int a() {
        return this.k.getDisplayOptions();
    }

    @Override // android.support.v7.a.a
    public Context b() {
        if (this.f == null) {
            TypedValue typedValue = new TypedValue();
            this.e.getTheme().resolveAttribute(c.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.f = new ContextThemeWrapper(this.e, i2);
            } else {
                this.f = this.e;
            }
        }
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (!this.x) {
            this.x = true;
            k(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.x) {
            this.x = false;
            k(false);
        }
    }

    public a a(b bVar) {
        if (this.a != null) {
            this.a.b();
        }
        this.l.h();
        s sVar = new s(this, bVar);
        if (!sVar.d()) {
            return null;
        }
        sVar.c();
        this.l.a(sVar);
        f(true);
        if (!(this.m == null || this.s != 1 || this.m.getVisibility() == 0)) {
            this.m.setVisibility(0);
        }
        this.l.sendAccessibilityEvent(32);
        this.a = sVar;
        return sVar;
    }

    /* access modifiers changed from: package-private */
    public void f(boolean z2) {
        int i2;
        int i3;
        int i4 = 8;
        if (z2) {
            d();
        } else {
            e();
        }
        ActionBarView actionBarView = this.k;
        if (z2) {
            i2 = 4;
        } else {
            i2 = 0;
        }
        actionBarView.a(i2);
        ActionBarContextView actionBarContextView = this.l;
        if (z2) {
            i3 = 0;
        } else {
            i3 = 8;
        }
        actionBarContextView.a(i3);
        if (this.n != null && !this.k.j() && this.k.m()) {
            ScrollingTabContainerView scrollingTabContainerView = this.n;
            if (!z2) {
                i4 = 0;
            }
            scrollingTabContainerView.setVisibility(i4);
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        if (z2 || z3) {
            return false;
        }
        return true;
    }

    private void k(boolean z2) {
        if (b(this.v, this.w, this.x)) {
            if (!this.y) {
                this.y = true;
                h(z2);
            }
        } else if (this.y) {
            this.y = false;
            i(z2);
        }
    }

    public void g(boolean z2) {
        this.z = z2;
        if (!z2) {
            this.j.clearAnimation();
            if (this.m != null) {
                this.m.clearAnimation();
            }
        }
    }

    public void h(boolean z2) {
        this.j.clearAnimation();
        if (this.j.getVisibility() != 0) {
            boolean z3 = f() || z2;
            if (z3) {
                this.j.startAnimation(AnimationUtils.loadAnimation(this.e, android.support.v7.b.b.abc_slide_in_top));
            }
            this.j.setVisibility(0);
            if (this.m != null && this.m.getVisibility() != 0) {
                if (z3) {
                    this.m.startAnimation(AnimationUtils.loadAnimation(this.e, android.support.v7.b.b.abc_slide_in_bottom));
                }
                this.m.setVisibility(0);
            }
        }
    }

    public void i(boolean z2) {
        this.j.clearAnimation();
        if (this.j.getVisibility() != 8) {
            boolean z3 = f() || z2;
            if (z3) {
                this.j.startAnimation(AnimationUtils.loadAnimation(this.e, android.support.v7.b.b.abc_slide_out_top));
            }
            this.j.setVisibility(8);
            if (this.m != null && this.m.getVisibility() != 8) {
                if (z3) {
                    this.m.startAnimation(AnimationUtils.loadAnimation(this.e, android.support.v7.b.b.abc_slide_out_bottom));
                }
                this.m.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.z;
    }
}
