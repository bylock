package android.support.v7.a;

import android.support.v7.b.f;
import android.support.v7.internal.widget.NativeActionModeAwareLayout;
import android.support.v7.internal.widget.ad;
import android.view.ActionMode;

/* compiled from: MyApp */
class t extends r implements ad {
    final NativeActionModeAwareLayout e;
    private ActionMode f;

    public t(f fVar, b bVar) {
        super(fVar, bVar);
        this.e = (NativeActionModeAwareLayout) fVar.findViewById(f.action_bar_root);
        if (this.e != null) {
            this.e.setActionModeForChildListener(this);
        }
    }

    @Override // android.support.v7.internal.widget.ad
    public ActionMode.Callback a(ActionMode.Callback callback) {
        return new u(this, callback);
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.a.r
    public boolean f() {
        return this.f == null && super.f();
    }
}
