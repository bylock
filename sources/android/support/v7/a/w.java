package android.support.v7.a;

import android.app.Activity;
import android.content.Context;
import android.view.View;

/* compiled from: MyApp */
public class w extends v {
    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ int a() {
        return super.a();
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void a(int i) {
        super.a(i);
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void a(View view) {
        super.a(view);
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void a(CharSequence charSequence) {
        super.a(charSequence);
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void a(boolean z) {
        super.a(z);
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ Context b() {
        return super.b();
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void b(int i) {
        super.b(i);
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void b(boolean z) {
        super.b(z);
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void c(boolean z) {
        super.c(z);
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void d(boolean z) {
        super.d(z);
    }

    @Override // android.support.v7.a.v, android.support.v7.a.a
    public /* bridge */ /* synthetic */ void e(boolean z) {
        super.e(z);
    }

    public w(Activity activity, b bVar) {
        super(activity, bVar, false);
    }
}
