package android.support.v7.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.af;
import android.support.v4.app.bb;
import android.support.v4.app.bc;
import android.support.v4.app.j;
import android.support.v7.c.a;
import android.support.v7.c.b;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: MyApp */
public class f extends j implements bc, b {
    g n;

    public a f() {
        return this.n.b();
    }

    public MenuInflater getMenuInflater() {
        return this.n.c();
    }

    @Override // android.app.Activity
    public void setContentView(int i) {
        this.n.a(i);
    }

    @Override // android.app.Activity
    public void setContentView(View view) {
        this.n.a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        this.n.a(view, layoutParams);
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        this.n.b(view, layoutParams);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onCreate(Bundle bundle) {
        this.n = g.a(this);
        super.onCreate(bundle);
        this.n.a(bundle);
    }

    @Override // android.support.v4.app.j
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.n.a(configuration);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onStop() {
        super.onStop();
        this.n.d();
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.j
    public void onPostResume() {
        super.onPostResume();
        this.n.e();
    }

    public View onCreatePanelView(int i) {
        if (i == 0) {
            return this.n.b(i);
        }
        return super.onCreatePanelView(i);
    }

    @Override // android.support.v4.app.j
    public final boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (this.n.a(i, menuItem)) {
            return true;
        }
        a f = f();
        if (menuItem.getItemId() != 16908332 || f == null || (f.a() & 4) == 0) {
            return false;
        }
        return g();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        this.n.a(charSequence);
    }

    @Override // android.support.v4.app.j
    public void c() {
        if (Build.VERSION.SDK_INT >= 14) {
            super.c();
        }
        this.n.f();
    }

    public void a(a aVar) {
    }

    public void b(a aVar) {
    }

    public a a(b bVar) {
        return this.n.a(bVar);
    }

    @Override // android.support.v4.app.j
    public boolean onCreatePanelMenu(int i, Menu menu) {
        return this.n.a(i, menu);
    }

    @Override // android.support.v4.app.j
    public boolean onPreparePanel(int i, View view, Menu menu) {
        return this.n.a(i, view, menu);
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        super.setContentView(i);
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        super.setContentView(view);
    }

    /* access modifiers changed from: package-private */
    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(view, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        super.addContentView(view, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, Menu menu) {
        return super.onCreatePanelMenu(i, menu);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, View view, Menu menu) {
        return super.onPreparePanel(i, view, menu);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, MenuItem menuItem) {
        return super.onMenuItemSelected(i, menuItem);
    }

    @Override // android.support.v4.app.j
    public void onBackPressed() {
        if (!this.n.g()) {
            super.onBackPressed();
        }
    }

    public void a(bb bbVar) {
        bbVar.a((Activity) this);
    }

    public void b(bb bbVar) {
    }

    public boolean g() {
        Intent a = a();
        if (a == null) {
            return false;
        }
        if (a(a)) {
            bb a2 = bb.a((Context) this);
            a(a2);
            b(a2);
            a2.a();
            try {
                android.support.v4.app.a.a(this);
            } catch (IllegalStateException e) {
                finish();
            }
        } else {
            b(a);
        }
        return true;
    }

    @Override // android.support.v4.app.bc
    public Intent a() {
        return af.a(this);
    }

    public boolean a(Intent intent) {
        return af.a(this, intent);
    }

    public void b(Intent intent) {
        af.b(this, intent);
    }

    public final void onContentChanged() {
        this.n.h();
    }

    public void h() {
    }
}
