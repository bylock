package android.support.v7.a;

import android.content.Context;
import android.support.v7.c.b;
import android.support.v7.internal.view.c;
import android.support.v7.internal.view.d;
import android.support.v7.internal.view.e;
import android.view.ActionMode;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class p extends n {
    p(f fVar) {
        super(fVar);
    }

    @Override // android.support.v7.a.g, android.support.v7.a.n
    public a a() {
        return new w(this.a, this.a);
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.a.n
    public c a(Context context, b bVar) {
        return new e(context, bVar);
    }

    /* access modifiers changed from: package-private */
    @Override // android.support.v7.a.n
    public android.support.v7.internal.view.b a(Context context, ActionMode actionMode) {
        return new d(context, actionMode);
    }
}
