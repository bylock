package android.support.v7.a;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.b.c;
import android.support.v7.b.d;
import android.support.v7.b.f;
import android.support.v7.b.h;
import android.support.v7.b.k;
import android.support.v7.c.a;
import android.support.v7.c.b;
import android.support.v7.internal.view.menu.ag;
import android.support.v7.internal.view.menu.ah;
import android.support.v7.internal.view.menu.aj;
import android.support.v7.internal.view.menu.o;
import android.support.v7.internal.view.menu.q;
import android.support.v7.internal.view.menu.r;
import android.support.v7.internal.widget.ActionBarContainer;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ActionBarView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class j extends g implements ag, r {
    private static final int[] d = {c.homeAsUpIndicator};
    private ActionBarView e;
    private o f;
    private q g;
    private a h;
    private boolean i;
    private CharSequence j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private boolean o;
    private Bundle p;

    j(f fVar) {
        super(fVar);
    }

    @Override // android.support.v7.a.g
    public a a() {
        k();
        return new r(this.a, this.a);
    }

    @Override // android.support.v7.a.g
    public void a(Configuration configuration) {
        if (this.b && this.i) {
            ((r) b()).a(configuration);
        }
    }

    @Override // android.support.v7.a.g
    public void d() {
        r rVar = (r) b();
        if (rVar != null) {
            rVar.g(false);
        }
    }

    @Override // android.support.v7.a.g
    public void e() {
        r rVar = (r) b();
        if (rVar != null) {
            rVar.g(true);
        }
    }

    @Override // android.support.v7.a.g
    public void a(View view) {
        k();
        ViewGroup viewGroup = (ViewGroup) this.a.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.a.h();
    }

    @Override // android.support.v7.a.g
    public void a(int i2) {
        k();
        ViewGroup viewGroup = (ViewGroup) this.a.findViewById(16908290);
        viewGroup.removeAllViews();
        this.a.getLayoutInflater().inflate(i2, viewGroup);
        this.a.h();
    }

    @Override // android.support.v7.a.g
    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        k();
        ViewGroup viewGroup = (ViewGroup) this.a.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.a.h();
    }

    @Override // android.support.v7.a.g
    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        k();
        ((ViewGroup) this.a.findViewById(16908290)).addView(view, layoutParams);
        this.a.h();
    }

    @Override // android.support.v7.a.g
    public void h() {
    }

    /* access modifiers changed from: package-private */
    public final void k() {
        boolean z;
        if (!this.i) {
            if (this.b) {
                if (this.c) {
                    this.a.a(h.abc_action_bar_decor_overlay);
                } else {
                    this.a.a(h.abc_action_bar_decor);
                }
                this.e = (ActionBarView) this.a.findViewById(f.action_bar);
                this.e.setWindowCallback(this.a);
                if (this.k) {
                    this.e.g();
                }
                if (this.l) {
                    this.e.h();
                }
                boolean equals = "splitActionBarWhenNarrow".equals(i());
                if (equals) {
                    z = this.a.getResources().getBoolean(d.abc_split_action_bar_is_narrow);
                } else {
                    TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(k.ActionBarWindow);
                    boolean z2 = obtainStyledAttributes.getBoolean(2, false);
                    obtainStyledAttributes.recycle();
                    z = z2;
                }
                ActionBarContainer actionBarContainer = (ActionBarContainer) this.a.findViewById(f.split_action_bar);
                if (actionBarContainer != null) {
                    this.e.setSplitView(actionBarContainer);
                    this.e.setSplitActionBar(z);
                    this.e.setSplitWhenNarrow(equals);
                    ActionBarContextView actionBarContextView = (ActionBarContextView) this.a.findViewById(f.action_context_bar);
                    actionBarContextView.setSplitView(actionBarContainer);
                    actionBarContextView.setSplitActionBar(z);
                    actionBarContextView.setSplitWhenNarrow(equals);
                }
            } else {
                this.a.a(h.abc_simple_decor);
            }
            this.a.findViewById(16908290).setId(-1);
            this.a.findViewById(f.action_bar_activity_content).setId(16908290);
            if (this.j != null) {
                this.e.setWindowTitle(this.j);
                this.j = null;
            }
            l();
            this.i = true;
            this.a.getWindow().getDecorView().post(new k(this));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void l() {
        /*
        // Method dump skipped, instructions count: 209
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.a.j.l():void");
    }

    @Override // android.support.v7.a.g
    public void a(CharSequence charSequence) {
        if (this.e != null) {
            this.e.setWindowTitle(charSequence);
        } else {
            this.j = charSequence;
        }
    }

    @Override // android.support.v7.a.g
    public View b(int i2) {
        if (i2 != 0 || !n()) {
            return null;
        }
        return (View) a(this.a, this);
    }

    @Override // android.support.v7.a.g
    public boolean a(int i2, Menu menu) {
        if (i2 != 0) {
            return this.a.a(i2, menu);
        }
        return false;
    }

    @Override // android.support.v7.a.g
    public boolean a(int i2, View view, Menu menu) {
        if (i2 != 0) {
            return this.a.a(i2, view, menu);
        }
        return false;
    }

    @Override // android.support.v7.a.g
    public boolean a(int i2, MenuItem menuItem) {
        if (i2 == 0) {
            menuItem = aj.a(menuItem);
        }
        return this.a.a(i2, menuItem);
    }

    @Override // android.support.v7.internal.view.menu.r
    public boolean a(q qVar, MenuItem menuItem) {
        return this.a.onMenuItemSelected(0, menuItem);
    }

    @Override // android.support.v7.internal.view.menu.r
    public void a(q qVar) {
        b(qVar, true);
    }

    @Override // android.support.v7.internal.view.menu.ag
    public void a(q qVar, boolean z) {
        if (!this.m) {
            this.m = true;
            this.a.closeOptionsMenu();
            this.e.f();
            this.m = false;
        }
    }

    @Override // android.support.v7.internal.view.menu.ag
    public boolean b(q qVar) {
        return false;
    }

    @Override // android.support.v7.a.g
    public a a(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        if (this.h != null) {
            this.h.b();
        }
        l lVar = new l(this, bVar);
        r rVar = (r) b();
        if (rVar != null) {
            this.h = rVar.a(lVar);
        }
        if (this.h != null) {
            this.a.a(this.h);
        }
        return this.h;
    }

    @Override // android.support.v7.a.g
    public void f() {
        if (this.g != null) {
            Bundle bundle = new Bundle();
            this.g.a(bundle);
            if (bundle.size() > 0) {
                this.p = bundle;
            }
            this.g.g();
            this.g.clear();
        }
        this.o = true;
        if (this.e != null) {
            this.n = false;
            n();
        }
    }

    private void b(q qVar, boolean z) {
        if (this.e == null || !this.e.e()) {
            qVar.close();
        } else if (this.e.d() && z) {
            this.e.c();
        } else if (this.e.getVisibility() == 0) {
            this.e.a();
        }
    }

    private ah a(Context context, ag agVar) {
        if (this.g == null) {
            return null;
        }
        if (this.f == null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(k.Theme);
            int resourceId = obtainStyledAttributes.getResourceId(4, android.support.v7.b.j.Theme_AppCompat_CompactMenu);
            obtainStyledAttributes.recycle();
            this.f = new o(h.abc_list_menu_item_layout, resourceId);
            this.f.a(agVar);
            this.g.a(this.f);
        } else {
            this.f.c(false);
        }
        return this.f.a(new FrameLayout(context));
    }

    @Override // android.support.v7.a.g
    public boolean g() {
        if (this.h != null) {
            this.h.b();
            return true;
        } else if (this.e == null || !this.e.k()) {
            return false;
        } else {
            this.e.l();
            return true;
        }
    }

    private boolean m() {
        this.g = new q(j());
        this.g.a(this);
        return true;
    }

    private boolean n() {
        if (this.n) {
            return true;
        }
        if (this.g == null || this.o) {
            if (this.g == null && (!m() || this.g == null)) {
                return false;
            }
            if (this.e != null) {
                this.e.a(this.g, this);
            }
            this.g.g();
            if (!this.a.a(0, this.g)) {
                this.g = null;
                if (this.e != null) {
                    this.e.a(null, this);
                }
                return false;
            }
            this.o = false;
        }
        this.g.g();
        if (this.p != null) {
            this.g.b(this.p);
            this.p = null;
        }
        if (!this.a.a(0, (View) null, this.g)) {
            if (this.e != null) {
                this.e.a(null, this);
            }
            this.g.h();
            return false;
        }
        this.g.h();
        this.n = true;
        return true;
    }
}
