package android.support.v7.a;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

/* compiled from: MyApp */
class u implements ActionMode.Callback {
    final /* synthetic */ t a;
    private final ActionMode.Callback b;

    u(t tVar, ActionMode.Callback callback) {
        this.a = tVar;
        this.b = callback;
    }

    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        boolean onCreateActionMode = this.b.onCreateActionMode(actionMode, menu);
        if (onCreateActionMode) {
            this.a.f = actionMode;
            this.a.d();
        }
        return onCreateActionMode;
    }

    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return this.b.onPrepareActionMode(actionMode, menu);
    }

    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return this.b.onActionItemClicked(actionMode, menuItem);
    }

    public void onDestroyActionMode(ActionMode actionMode) {
        this.b.onDestroyActionMode(actionMode);
        this.a.e();
        this.a.f = null;
    }
}
