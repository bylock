package android.support.v7.a;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import java.util.ArrayList;

/* compiled from: MyApp */
class v extends a {
    final Activity a;
    final b b;
    final ActionBar c;
    private ArrayList d;

    public v(Activity activity, b bVar) {
        this(activity, bVar, true);
    }

    v(Activity activity, b bVar, boolean z) {
        this.d = new ArrayList();
        this.a = activity;
        this.b = bVar;
        this.c = activity.getActionBar();
        if (z && (a() & 4) != 0) {
            e(true);
        }
    }

    @Override // android.support.v7.a.a
    public void a(View view) {
        this.c.setCustomView(view);
    }

    @Override // android.support.v7.a.a
    public void a(int i) {
        this.c.setLogo(i);
    }

    @Override // android.support.v7.a.a
    public void a(CharSequence charSequence) {
        this.c.setTitle(charSequence);
    }

    @Override // android.support.v7.a.a
    public void b(int i) {
        this.c.setDisplayOptions(i);
    }

    @Override // android.support.v7.a.a
    public void a(boolean z) {
        this.c.setDisplayUseLogoEnabled(z);
    }

    @Override // android.support.v7.a.a
    public void b(boolean z) {
        this.c.setDisplayShowHomeEnabled(z);
    }

    @Override // android.support.v7.a.a
    public void c(boolean z) {
        this.c.setDisplayHomeAsUpEnabled(z);
    }

    @Override // android.support.v7.a.a
    public void d(boolean z) {
        this.c.setDisplayShowTitleEnabled(z);
    }

    @Override // android.support.v7.a.a
    public int a() {
        return this.c.getDisplayOptions();
    }

    @Override // android.support.v7.a.a
    public Context b() {
        return this.c.getThemedContext();
    }

    @Override // android.support.v7.a.a
    public void e(boolean z) {
        this.c.setHomeButtonEnabled(z);
    }
}
