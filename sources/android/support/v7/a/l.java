package android.support.v7.a;

import android.support.v7.c.a;
import android.support.v7.c.b;
import android.view.Menu;
import android.view.MenuItem;

/* compiled from: MyApp */
class l implements b {
    final /* synthetic */ j a;
    private b b;

    public l(j jVar, b bVar) {
        this.a = jVar;
        this.b = bVar;
    }

    @Override // android.support.v7.c.b
    public boolean a(a aVar, Menu menu) {
        return this.b.a(aVar, menu);
    }

    @Override // android.support.v7.c.b
    public boolean b(a aVar, Menu menu) {
        return this.b.b(aVar, menu);
    }

    @Override // android.support.v7.c.b
    public boolean a(a aVar, MenuItem menuItem) {
        return this.b.a(aVar, menuItem);
    }

    @Override // android.support.v7.c.b
    public void a(a aVar) {
        this.b.a(aVar);
        this.a.a.b(aVar);
        this.a.h = null;
    }
}
