package android.support.v7.a;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.af;
import android.support.v7.b.k;
import android.support.v7.c.a;
import android.support.v7.c.b;
import android.support.v7.internal.view.f;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: MyApp */
abstract class g {
    final f a;
    boolean b;
    boolean c;
    private a d;
    private MenuInflater e;
    private boolean f;

    /* access modifiers changed from: package-private */
    public abstract a a();

    /* access modifiers changed from: package-private */
    public abstract a a(b bVar);

    /* access modifiers changed from: package-private */
    public abstract void a(int i);

    /* access modifiers changed from: package-private */
    public abstract void a(Configuration configuration);

    /* access modifiers changed from: package-private */
    public abstract void a(View view);

    /* access modifiers changed from: package-private */
    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    /* access modifiers changed from: package-private */
    public abstract void a(CharSequence charSequence);

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i, Menu menu);

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i, MenuItem menuItem);

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i, View view, Menu menu);

    /* access modifiers changed from: package-private */
    public abstract View b(int i);

    /* access modifiers changed from: package-private */
    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    /* access modifiers changed from: package-private */
    public abstract void d();

    /* access modifiers changed from: package-private */
    public abstract void e();

    /* access modifiers changed from: package-private */
    public abstract void f();

    /* access modifiers changed from: package-private */
    public abstract boolean g();

    /* access modifiers changed from: package-private */
    public abstract void h();

    static g a(f fVar) {
        if (Build.VERSION.SDK_INT >= 20) {
            return new h(fVar);
        }
        if (Build.VERSION.SDK_INT >= 18) {
            return new q(fVar);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            return new p(fVar);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            return new n(fVar);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            return new m(fVar);
        }
        return new j(fVar);
    }

    g(f fVar) {
        this.a = fVar;
    }

    /* access modifiers changed from: package-private */
    public final a b() {
        if (!this.b && !this.c) {
            this.d = null;
        } else if (this.d == null) {
            this.d = a();
            if (this.f) {
                this.d.c(true);
            }
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public MenuInflater c() {
        if (this.e == null) {
            this.e = new f(j());
        }
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void a(Bundle bundle) {
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(k.ActionBarWindow);
        if (!obtainStyledAttributes.hasValue(0)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        this.b = obtainStyledAttributes.getBoolean(0, false);
        this.c = obtainStyledAttributes.getBoolean(1, false);
        obtainStyledAttributes.recycle();
        if (af.b(this.a) == null) {
            return;
        }
        if (this.d == null) {
            this.f = true;
        } else {
            this.d.c(true);
        }
    }

    /* access modifiers changed from: protected */
    public final String i() {
        try {
            ActivityInfo activityInfo = this.a.getPackageManager().getActivityInfo(this.a.getComponentName(), 128);
            if (activityInfo.metaData != null) {
                return activityInfo.metaData.getString("android.support.UI_OPTIONS");
            }
            return null;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("ActionBarActivityDelegate", "getUiOptionsFromMetadata: Activity '" + this.a.getClass().getSimpleName() + "' not in manifest");
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final Context j() {
        f fVar = this.a;
        a b2 = b();
        if (b2 != null) {
            return b2.b();
        }
        return fVar;
    }
}
