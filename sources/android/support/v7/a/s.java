package android.support.v7.a;

import android.support.v7.c.a;
import android.support.v7.c.b;
import android.support.v7.internal.view.menu.q;
import android.support.v7.internal.view.menu.r;
import android.view.Menu;
import android.view.MenuItem;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class s extends a implements r {
    final /* synthetic */ r a;
    private b b;
    private q c;

    public s(r rVar, b bVar) {
        this.a = rVar;
        this.b = bVar;
        this.c = new q(rVar.b()).a(1);
        this.c.a(this);
    }

    @Override // android.support.v7.c.a
    public Menu a() {
        return this.c;
    }

    @Override // android.support.v7.c.a
    public void b() {
        if (this.a.a == this) {
            if (!(r.b(this.a.v, this.a.w, false))) {
                this.a.b = this;
                this.a.c = this.b;
            } else {
                this.b.a(this);
            }
            this.b = null;
            this.a.f(false);
            this.a.l.g();
            this.a.k.sendAccessibilityEvent(32);
            this.a.a = null;
        }
    }

    public void c() {
        this.c.g();
        try {
            this.b.b(this, this.c);
        } finally {
            this.c.h();
        }
    }

    public boolean d() {
        this.c.g();
        try {
            return this.b.a(this, this.c);
        } finally {
            this.c.h();
        }
    }

    @Override // android.support.v7.internal.view.menu.r
    public boolean a(q qVar, MenuItem menuItem) {
        if (this.b != null) {
            return this.b.a(this, menuItem);
        }
        return false;
    }

    @Override // android.support.v7.internal.view.menu.r
    public void a(q qVar) {
        if (this.b != null) {
            c();
            this.a.l.a();
        }
    }
}
