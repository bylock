package android.support.v7.a;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.b.k;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* compiled from: MyApp */
public class c extends ViewGroup.MarginLayoutParams {
    public int a;

    public c(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, k.ActionBarLayout);
        this.a = obtainStyledAttributes.getInt(0, -1);
        obtainStyledAttributes.recycle();
    }

    public c(int i, int i2, int i3) {
        super(i, i2);
        this.a = -1;
        this.a = i3;
    }

    public c(int i) {
        this(-2, -1, i);
    }
}
