package android.support.v13.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v13.app.FragmentTabHost;

/* compiled from: MyApp */
final class b implements Parcelable.Creator {
    b() {
    }

    /* renamed from: a */
    public FragmentTabHost.SavedState createFromParcel(Parcel parcel) {
        return new FragmentTabHost.SavedState(parcel);
    }

    /* renamed from: a */
    public FragmentTabHost.SavedState[] newArray(int i) {
        return new FragmentTabHost.SavedState[i];
    }
}
