package android.support.v13.app;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

/* compiled from: MyApp */
public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    private final ArrayList a;
    private Context b;
    private FragmentManager c;
    private int d;
    private TabHost.OnTabChangeListener e;
    private c f;
    private boolean g;

    /* access modifiers changed from: package-private */
    /* compiled from: MyApp */
    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new b();
        String a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readString();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.a);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.a + "}";
        }
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.e = onTabChangeListener;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        FragmentTransaction fragmentTransaction = null;
        for (int i = 0; i < this.a.size(); i++) {
            c cVar = (c) this.a.get(i);
            cVar.d = this.c.findFragmentByTag(cVar.a);
            if (cVar.d != null && !cVar.d.isDetached()) {
                if (cVar.a.equals(currentTabTag)) {
                    this.f = cVar;
                } else {
                    if (fragmentTransaction == null) {
                        fragmentTransaction = this.c.beginTransaction();
                    }
                    fragmentTransaction.detach(cVar.d);
                }
            }
        }
        this.g = true;
        FragmentTransaction a2 = a(currentTabTag, fragmentTransaction);
        if (a2 != null) {
            a2.commit();
            this.c.executePendingTransactions();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = getCurrentTabTag();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.a);
    }

    public void onTabChanged(String str) {
        FragmentTransaction a2;
        if (this.g && (a2 = a(str, null)) != null) {
            a2.commit();
        }
        if (this.e != null) {
            this.e.onTabChanged(str);
        }
    }

    private FragmentTransaction a(String str, FragmentTransaction fragmentTransaction) {
        c cVar = null;
        int i = 0;
        while (i < this.a.size()) {
            c cVar2 = (c) this.a.get(i);
            if (!cVar2.a.equals(str)) {
                cVar2 = cVar;
            }
            i++;
            cVar = cVar2;
        }
        if (cVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.f != cVar) {
            if (fragmentTransaction == null) {
                fragmentTransaction = this.c.beginTransaction();
            }
            if (!(this.f == null || this.f.d == null)) {
                fragmentTransaction.detach(this.f.d);
            }
            if (cVar != null) {
                if (cVar.d == null) {
                    cVar.d = Fragment.instantiate(this.b, cVar.b.getName(), cVar.c);
                    fragmentTransaction.add(this.d, cVar.d, cVar.a);
                } else {
                    fragmentTransaction.attach(cVar.d);
                }
            }
            this.f = cVar;
        }
        return fragmentTransaction;
    }
}
