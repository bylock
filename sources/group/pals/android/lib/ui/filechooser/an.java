package group.pals.android.lib.ui.filechooser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import group.pals.android.lib.ui.filechooser.a.a;
import group.pals.android.lib.ui.filechooser.a.b;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.d;
import group.pals.android.lib.ui.filechooser.utils.c;
import group.pals.android.lib.ui.filechooser.utils.f;
import java.util.List;

/* compiled from: MyApp */
public class an extends BaseAdapter {
    public static final String a = an.class.getName();
    private static /* synthetic */ int[] j;
    private final Integer[] b;
    private final d c;
    private final Context d;
    private final b e;
    private List f;
    private LayoutInflater g;
    private boolean h;
    private final View.OnLongClickListener i = new ao(this);

    static /* synthetic */ int[] c() {
        int[] iArr = j;
        if (iArr == null) {
            iArr = new int[d.values().length];
            try {
                iArr[d.DirectoriesOnly.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[d.FilesAndDirectories.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[d.FilesOnly.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            j = iArr;
        }
        return iArr;
    }

    public an(Context context, List list, d dVar, boolean z) {
        this.d = context;
        this.f = list;
        this.g = LayoutInflater.from(this.d);
        this.c = dVar;
        this.h = z;
        switch (c()[this.c.ordinal()]) {
            case 1:
            case 2:
                this.b = new Integer[]{Integer.valueOf(be.afc_cmd_advanced_selection_all), Integer.valueOf(be.afc_cmd_advanced_selection_none), Integer.valueOf(be.afc_cmd_advanced_selection_invert)};
                break;
            default:
                this.b = new Integer[]{Integer.valueOf(be.afc_cmd_advanced_selection_all), Integer.valueOf(be.afc_cmd_advanced_selection_none), Integer.valueOf(be.afc_cmd_advanced_selection_invert), Integer.valueOf(be.afc_cmd_select_all_files), Integer.valueOf(be.afc_cmd_select_all_folders)};
                break;
        }
        this.e = new b(a.d(this.d), a.e(this.d));
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }

    public void a() {
        this.e.a(a.d(this.d));
        this.e.b(a.e(this.d));
    }

    public int getCount() {
        if (this.f != null) {
            return this.f.size();
        }
        return 0;
    }

    /* renamed from: a */
    public av getItem(int i2) {
        if (this.f != null) {
            return (av) this.f.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public void a(av avVar) {
        if (this.f != null) {
            this.f.add(avVar);
        }
    }

    public void b(av avVar) {
        if (this.f != null) {
            this.f.remove(avVar);
        }
    }

    public void b() {
        if (this.f != null) {
            this.f.clear();
        }
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        au auVar;
        View view2;
        av a2 = getItem(i2);
        if (view == null) {
            view2 = this.g.inflate(bc.afc_file_item, (ViewGroup) null);
            auVar = new au(null);
            auVar.a = (ImageView) view2.findViewById(ba.afc_file_item_imageview_icon);
            auVar.b = (TextView) view2.findViewById(ba.afc_file_item_textview_filename);
            auVar.c = (TextView) view2.findViewById(ba.afc_file_item_textview_file_info);
            auVar.d = (CheckBox) view2.findViewById(ba.afc_file_item_checkbox_selection);
            view2.setTag(auVar);
        } else {
            auVar = (au) view.getTag();
            view2 = view;
        }
        a(viewGroup, view2, auVar, a2, a2.a());
        return view2;
    }

    private void a(ViewGroup viewGroup, View view, au auVar, av avVar, IFile iFile) {
        auVar.b.setSingleLine(viewGroup instanceof GridView);
        auVar.a.setImageResource(f.a(iFile));
        auVar.b.setText(iFile.getName());
        if (avVar.c()) {
            auVar.b.setPaintFlags(auVar.b.getPaintFlags() | 16);
        } else {
            auVar.b.setPaintFlags(auVar.b.getPaintFlags() & -17);
        }
        String a2 = c.a(this.d, iFile.lastModified(), this.e);
        if (iFile.isDirectory()) {
            auVar.c.setText(a2);
        } else {
            auVar.c.setText(String.format("%s, %s", group.pals.android.lib.ui.filechooser.utils.b.a((double) iFile.length()), a2));
        }
        if (!this.h) {
            auVar.d.setVisibility(8);
        } else if (!d.FilesOnly.equals(this.c) || !iFile.isDirectory()) {
            auVar.d.setVisibility(0);
            auVar.d.setFocusable(false);
            auVar.d.setOnCheckedChangeListener(new at(this, avVar));
            auVar.d.setOnLongClickListener(this.i);
            auVar.d.setChecked(avVar.b());
        } else {
            auVar.d.setVisibility(8);
        }
    }

    public void a(boolean z, group.pals.android.lib.ui.filechooser.io.a aVar) {
        for (int i2 = 0; i2 < getCount(); i2++) {
            av a2 = getItem(i2);
            a2.a(aVar == null ? true : aVar.a(a2.a()));
        }
        if (z) {
            notifyDataSetChanged();
        }
    }

    public void a(boolean z) {
        for (int i2 = 0; i2 < getCount(); i2++) {
            getItem(i2).a(false);
        }
        if (z) {
            notifyDataSetChanged();
        }
    }

    public void b(boolean z) {
        for (int i2 = 0; i2 < getCount(); i2++) {
            av a2 = getItem(i2);
            a2.a(!a2.b());
        }
        if (z) {
            notifyDataSetChanged();
        }
    }
}
