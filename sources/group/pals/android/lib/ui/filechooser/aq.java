package group.pals.android.lib.ui.filechooser;

import android.content.Context;
import group.pals.android.lib.ui.filechooser.utils.a.e;

/* compiled from: MyApp */
class aq extends e {
    final /* synthetic */ ap a;
    private final /* synthetic */ int b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    aq(ap apVar, Context context, int i, boolean z, int i2) {
        super(context, i, z);
        this.a = apVar;
        this.b = i2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object doInBackground(Void... voidArr) {
        if (this.b == be.afc_cmd_advanced_selection_all) {
            this.a.a.a.a(false, null);
        } else if (this.b == be.afc_cmd_advanced_selection_none) {
            this.a.a.a.a(false);
        } else if (this.b == be.afc_cmd_advanced_selection_invert) {
            this.a.a.a.b(false);
        } else if (this.b == be.afc_cmd_select_all_files) {
            this.a.a.a.a(false, new ar(this));
        } else if (this.b == be.afc_cmd_select_all_folders) {
            this.a.a.a.a(false, new as(this));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // android.os.AsyncTask, group.pals.android.lib.ui.filechooser.utils.a.e
    public void onPostExecute(Object obj) {
        super.onPostExecute(obj);
        this.a.a.a.notifyDataSetChanged();
    }
}
