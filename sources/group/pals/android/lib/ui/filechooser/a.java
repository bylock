package group.pals.android.lib.ui.filechooser;

import android.view.View;
import group.pals.android.lib.ui.filechooser.io.IFile;

/* compiled from: MyApp */
class a implements View.OnClickListener {
    final /* synthetic */ FileChooserActivity a;

    a(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public void onClick(View view) {
        IFile iFile;
        IFile a2 = FileChooserActivity.a(this.a);
        while (true) {
            iFile = (IFile) FileChooserActivity.b(this.a).e(a2);
            if (!a2.a(iFile)) {
                break;
            }
            FileChooserActivity.b(this.a).c(iFile);
        }
        if (iFile != null) {
            FileChooserActivity.a(this.a, iFile, new b(this));
        } else {
            FileChooserActivity.c(this.a).setEnabled(false);
        }
    }
}
