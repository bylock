package group.pals.android.lib.ui.filechooser.utils;

import android.content.Context;

/* compiled from: MyApp */
public class i {
    public static boolean a(Context context, String... strArr) {
        for (String str : strArr) {
            if (context.checkCallingOrSelfPermission(str) == -1) {
                return false;
            }
        }
        return true;
    }
}
