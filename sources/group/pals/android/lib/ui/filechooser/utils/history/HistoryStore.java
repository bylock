package group.pals.android.lib.ui.filechooser.utils.history;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: MyApp */
public class HistoryStore implements History {
    public static final Parcelable.Creator CREATOR = new c();
    private final ArrayList a;
    private final int b;
    private final List c;

    public HistoryStore(int i) {
        this.a = new ArrayList();
        this.c = new ArrayList();
        this.b = i <= 0 ? 100 : i;
    }

    public void a(Parcelable parcelable) {
        if (parcelable != null) {
            if (this.a.isEmpty() || this.a.indexOf(parcelable) != this.a.size() - 1) {
                this.a.add(parcelable);
                e();
            }
        }
    }

    public void b(Parcelable parcelable) {
        int indexOf;
        if (parcelable != null && (indexOf = this.a.indexOf(parcelable)) >= 0 && indexOf < this.a.size() - 1) {
            this.a.subList(indexOf + 1, this.a.size()).clear();
            e();
        }
    }

    public void c(Parcelable parcelable) {
        if (this.a.remove(parcelable)) {
            e();
        }
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.history.History
    public void a(a aVar) {
        boolean z;
        boolean z2 = false;
        int size = this.a.size() - 1;
        while (size >= 0) {
            if (aVar.a((Parcelable) this.a.get(size))) {
                this.a.remove(size);
                if (!z2) {
                    z = true;
                    size--;
                    z2 = z;
                }
            }
            z = z2;
            size--;
            z2 = z;
        }
        if (z2) {
            e();
        }
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.history.History
    public void e() {
        for (b bVar : this.c) {
            bVar.a(this);
        }
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.history.History
    public int a() {
        return this.a.size();
    }

    public int d(Parcelable parcelable) {
        return this.a.indexOf(parcelable);
    }

    public Parcelable e(Parcelable parcelable) {
        int indexOf = this.a.indexOf(parcelable);
        if (indexOf > 0) {
            return (Parcelable) this.a.get(indexOf - 1);
        }
        return null;
    }

    public Parcelable f(Parcelable parcelable) {
        int indexOf = this.a.indexOf(parcelable);
        if (indexOf < 0 || indexOf >= this.a.size() - 1) {
            return null;
        }
        return (Parcelable) this.a.get(indexOf + 1);
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.history.History
    public ArrayList b() {
        return (ArrayList) this.a.clone();
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.history.History
    public boolean c() {
        return this.a.isEmpty();
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.history.History
    public void d() {
        this.a.clear();
        e();
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.history.History
    public void a(b bVar) {
        this.c.add(bVar);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeInt(a());
        for (int i2 = 0; i2 < a(); i2++) {
            parcel.writeParcelable((Parcelable) this.a.get(i2), i);
        }
    }

    private HistoryStore(Parcel parcel) {
        this.a = new ArrayList();
        this.c = new ArrayList();
        this.b = parcel.readInt();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            this.a.add(parcel.readParcelable(null));
        }
    }

    /* synthetic */ HistoryStore(Parcel parcel, HistoryStore historyStore) {
        this(parcel);
    }
}
