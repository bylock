package group.pals.android.lib.ui.filechooser.utils.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import group.pals.android.lib.ui.filechooser.ay;
import group.pals.android.lib.ui.filechooser.bc;

/* compiled from: MyApp */
public class h extends BaseAdapter {
    private final Context a;
    private final Integer[] b;
    private final int c = this.a.getResources().getDimensionPixelSize(ay.afc_5dp);
    private final int d = this.a.getResources().getDimensionPixelSize(ay.afc_context_menu_item_padding_left);

    public h(Context context, Integer[] numArr) {
        this.a = context;
        this.b = numArr;
    }

    public int getCount() {
        return this.b.length;
    }

    public Object getItem(int i) {
        return this.b[i];
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            view2 = LayoutInflater.from(this.a).inflate(bc.afc_context_menu_tiem, (ViewGroup) null);
        } else {
            view2 = view;
        }
        ((TextView) view2).setText(this.b[i].intValue());
        view2.setPadding(this.d, this.c, this.c, this.c);
        return view2;
    }
}
