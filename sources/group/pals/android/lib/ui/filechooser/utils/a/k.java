package group.pals.android.lib.ui.filechooser.utils.a;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import group.pals.android.lib.ui.filechooser.an;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class k implements AdapterView.OnItemClickListener {
    private final /* synthetic */ i a;
    private final /* synthetic */ AlertDialog b;
    private final /* synthetic */ an c;

    k(i iVar, AlertDialog alertDialog, an anVar) {
        this.a = iVar;
        this.b = alertDialog;
        this.c = anVar;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.a != null) {
            this.b.dismiss();
            this.a.a(true, this.c.getItem(i).a());
        }
    }
}
