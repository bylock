package group.pals.android.lib.ui.filechooser.utils.history;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: MyApp */
class c implements Parcelable.Creator {
    c() {
    }

    /* renamed from: a */
    public HistoryStore createFromParcel(Parcel parcel) {
        return new HistoryStore(parcel, null);
    }

    /* renamed from: a */
    public HistoryStore[] newArray(int i) {
        return new HistoryStore[i];
    }
}
