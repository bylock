package group.pals.android.lib.ui.filechooser.utils;

/* compiled from: MyApp */
public class b {
    public static String a(double d) {
        if (d <= 0.0d) {
            return "0 B";
        }
        String[] strArr = {"B", "KB", "MB", "GB", "TB"};
        Short sh = 1024;
        int log10 = (int) (Math.log10(d) / Math.log10((double) sh.shortValue()));
        if (log10 >= strArr.length) {
            log10 = strArr.length - 1;
        }
        double pow = d / Math.pow((double) sh.shortValue(), (double) log10);
        Object[] objArr = new Object[1];
        objArr[0] = log10 == 0 ? "%,.0f" : "%,.2f";
        return String.format(String.format("%s %%s", objArr), Double.valueOf(pow), strArr[log10]);
    }
}
