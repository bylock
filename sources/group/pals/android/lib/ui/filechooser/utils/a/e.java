package group.pals.android.lib.ui.filechooser.utils.a;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

/* compiled from: MyApp */
public abstract class e extends AsyncTask {
    public static final String f = e.class.getName();
    private final ProgressDialog a;
    private int b;
    private boolean c;
    private Throwable d;

    public e(Context context, String str, boolean z) {
        this.b = 500;
        this.c = false;
        this.a = new ProgressDialog(context);
        this.a.setMessage(str);
        this.a.setIndeterminate(true);
        this.a.setCancelable(z);
        if (z) {
            this.a.setCanceledOnTouchOutside(true);
            this.a.setOnCancelListener(new f(this));
        }
    }

    public e(Context context, int i, boolean z) {
        this(context, context.getString(i), z);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        new Handler().postDelayed(new g(this), (long) b());
    }

    /* access modifiers changed from: protected */
    @Override // android.os.AsyncTask
    public void onPostExecute(Object obj) {
        a();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        a();
        super.onCancelled();
    }

    private void a() {
        this.c = true;
        try {
            this.a.dismiss();
        } catch (Throwable th) {
            Log.e(f, "doFinish() - dismiss dialog: " + th);
        }
    }

    public int b() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void a(Throwable th) {
        this.d = th;
    }
}
