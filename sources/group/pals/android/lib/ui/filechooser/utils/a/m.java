package group.pals.android.lib.ui.filechooser.utils.a;

import android.content.DialogInterface;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class m implements DialogInterface.OnCancelListener {
    private final /* synthetic */ i a;

    m(i iVar) {
        this.a = iVar;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.a != null) {
            this.a.a(true, null);
        }
    }
}
