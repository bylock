package group.pals.android.lib.ui.filechooser.utils.a;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import group.pals.android.lib.ui.filechooser.ba;
import group.pals.android.lib.ui.filechooser.bc;

/* compiled from: MyApp */
public class a {
    public static void a(Context context, int i, String str, Integer[] numArr, c cVar) {
        h hVar = new h(context, numArr);
        View inflate = LayoutInflater.from(context).inflate(bc.afc_context_menu_view, (ViewGroup) null);
        ListView listView = (ListView) inflate.findViewById(ba.afc_context_menu_view_listview_menu);
        listView.setAdapter((ListAdapter) hVar);
        AlertDialog a = d.a(context);
        a.setButton(-2, (CharSequence) null, (DialogInterface.OnClickListener) null);
        a.setCanceledOnTouchOutside(true);
        if (i > 0) {
            a.setIcon(i);
        }
        a.setTitle(str);
        a.setView(inflate);
        if (cVar != null) {
            listView.setOnItemClickListener(new b(a, cVar, numArr));
        }
        a.show();
    }

    public static void a(Context context, int i, int i2, Integer[] numArr, c cVar) {
        a(context, i, i2 > 0 ? context.getString(i2) : null, numArr, cVar);
    }
}
