package group.pals.android.lib.ui.filechooser.utils;

import group.pals.android.lib.ui.filechooser.az;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.c;
import java.util.HashMap;
import java.util.Map;

/* compiled from: MyApp */
public class f {
    private static final Map a = new HashMap();

    static {
        a.put("(?si).+\\.(mp[2-3]+|wav|aiff|au|m4a|ogg|raw|flac|mid|amr|aac|alac|atrac|awb|m4p|mmf|mpc|ra|rm|tta|vox|wma)", Integer.valueOf(az.afc_file_audio));
        a.put("(?si).+\\.(mp[4]+|flv|wmv|webm|m4v|3gp|mkv|mov|mpe?g|rmv?|ogv|avi)", Integer.valueOf(az.afc_file_video));
        a.put("(?si).+\\.(gif|jpe?g|png|tiff?|wmf|emf|jfif|exif|raw|bmp|ppm|pgm|pbm|pnm|webp|riff|tga|ilbm|img|pcx|ecw|sid|cd5|fits|pgf|xcf|svg|pns|jps|icon?|jp2|mng|xpm|djvu)", Integer.valueOf(az.afc_file_image));
        a.put("(?si).+\\.(zip|7z|lz?|[jrt]ar|gz|gzip|bzip|xz|cab|sfx|z|iso|bz?|rz|s7z|apk|dmg)", Integer.valueOf(az.afc_file_compressed));
        a.put("(?si).+\\.(txt|html?|json|csv|java|pas|php.*|c|cpp|bas|python|js|javascript|scala|xml|kml|css|ps|xslt?|tpl|tsv|bash|cmd|pl|pm|ps1|ps1xml|psc1|psd1|psm1|py|pyc|pyo|r|rb|sdl|sh|tcl|vbs|xpl|ada|adb|ads|clj|cls|cob|cbl|cxx|cs|csproj|d|e|el|go|h|hpp|hxx|l|m|url|ini|prop|conf|properties|rc)", Integer.valueOf(az.afc_file_plain_text));
    }

    public static int a(IFile iFile) {
        if (iFile == null || !iFile.exists()) {
            return 17301533;
        }
        if (iFile.isFile()) {
            String name = iFile.getName();
            for (String str : a.keySet()) {
                if (name.matches(str)) {
                    return ((Integer) a.get(str)).intValue();
                }
            }
            return az.afc_file;
        } else if (iFile.isDirectory()) {
            return az.afc_folder;
        } else {
            return 17301533;
        }
    }

    public static boolean a(String str) {
        return str != null && str.trim().matches("[^\\\\/?%*:|\"<>]+");
    }

    public static Thread a(IFile iFile, c cVar, boolean z) {
        return new g(iFile, z, cVar);
    }
}
