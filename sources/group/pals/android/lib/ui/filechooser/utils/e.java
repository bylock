package group.pals.android.lib.ui.filechooser.utils;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.f;
import java.util.Comparator;

/* compiled from: MyApp */
public class e implements Comparator {
    private static /* synthetic */ int[] c;
    private final f a;
    private final group.pals.android.lib.ui.filechooser.services.e b;

    static /* synthetic */ int[] a() {
        int[] iArr = c;
        if (iArr == null) {
            iArr = new int[f.values().length];
            try {
                iArr[f.SortByDate.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[f.SortByName.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[f.SortBySize.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            c = iArr;
        }
        return iArr;
    }

    public e(f fVar, group.pals.android.lib.ui.filechooser.services.e eVar) {
        this.a = fVar;
        this.b = eVar;
    }

    /* renamed from: a */
    public int compare(IFile iFile, IFile iFile2) {
        if ((!iFile.isDirectory() || !iFile2.isDirectory()) && (!iFile.isFile() || !iFile2.isFile())) {
            return !iFile.isDirectory() ? 1 : -1;
        }
        int compareToIgnoreCase = iFile.getName().compareToIgnoreCase(iFile2.getName());
        switch (a()[this.a.ordinal()]) {
            case 2:
                if (iFile.length() <= iFile2.length()) {
                    if (iFile.length() < iFile2.length()) {
                        compareToIgnoreCase = -1;
                        break;
                    }
                } else {
                    compareToIgnoreCase = 1;
                    break;
                }
                break;
            case 3:
                if (iFile.lastModified() <= iFile2.lastModified()) {
                    if (iFile.lastModified() < iFile2.lastModified()) {
                        compareToIgnoreCase = -1;
                        break;
                    }
                } else {
                    compareToIgnoreCase = 1;
                    break;
                }
                break;
        }
        if (this.b != group.pals.android.lib.ui.filechooser.services.e.Ascending) {
            compareToIgnoreCase = -compareToIgnoreCase;
        }
        return compareToIgnoreCase;
    }
}
