package group.pals.android.lib.ui.filechooser.utils.a;

import android.util.Log;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class g implements Runnable {
    final /* synthetic */ e a;

    g(e eVar) {
        this.a = eVar;
    }

    public void run() {
        if (!(this.a.c)) {
            try {
                this.a.a.show();
            } catch (Throwable th) {
                Log.e(e.f, "onPreExecute() - show dialog: " + th);
            }
        }
    }
}
