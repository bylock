package group.pals.android.lib.ui.filechooser.utils.a;

import android.content.DialogInterface;
import group.pals.android.lib.ui.filechooser.utils.history.History;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class l implements DialogInterface.OnClickListener {
    private final /* synthetic */ History a;

    l(History history) {
        this.a = history;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.cancel();
        this.a.d();
    }
}
