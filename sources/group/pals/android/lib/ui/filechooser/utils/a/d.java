package group.pals.android.lib.ui.filechooser.utils.a;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import group.pals.android.lib.ui.filechooser.be;

/* compiled from: MyApp */
public class d {
    private static Toast a;

    public static void a(Context context, CharSequence charSequence, int i) {
        if (a != null) {
            a.cancel();
        }
        a = Toast.makeText(context, charSequence, i);
        a.show();
    }

    public static void a(Context context, int i, int i2) {
        a(context, context.getString(i), i2);
    }

    public static void a(Context context, CharSequence charSequence, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog a2 = a(context);
        a2.setIcon(17301543);
        a2.setTitle(be.afc_title_error);
        a2.setMessage(charSequence);
        a2.setOnCancelListener(onCancelListener);
        a2.show();
    }

    public static void a(Context context, int i, DialogInterface.OnCancelListener onCancelListener) {
        a(context, context.getString(i), onCancelListener);
    }

    public static void a(Context context, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog a2 = a(context);
        a2.setIcon(17301543);
        a2.setTitle(be.afc_title_confirmation);
        a2.setMessage(charSequence);
        a2.setButton(-1, context.getString(17039379), onClickListener);
        a2.setOnCancelListener(onCancelListener);
        a2.show();
    }

    public static void a(Context context, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        a(context, charSequence, onClickListener, null);
    }

    public static AlertDialog a(Context context) {
        AlertDialog create = b(context).create();
        create.setCanceledOnTouchOutside(true);
        return create;
    }

    public static AlertDialog.Builder b(Context context) {
        return new AlertDialog.Builder(context);
    }
}
