package group.pals.android.lib.ui.filechooser.utils.a;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class b implements AdapterView.OnItemClickListener {
    private final /* synthetic */ AlertDialog a;
    private final /* synthetic */ c b;
    private final /* synthetic */ Integer[] c;

    b(AlertDialog alertDialog, c cVar, Integer[] numArr) {
        this.a = alertDialog;
        this.b = cVar;
        this.c = numArr;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.a.dismiss();
        this.b.a(this.c[i].intValue());
    }
}
