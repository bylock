package group.pals.android.lib.ui.filechooser.utils;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.c;
import java.util.List;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class g extends Thread {
    private final /* synthetic */ IFile a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ c c;

    g(IFile iFile, boolean z, c cVar) {
        this.a = iFile;
        this.b = z;
        this.c = cVar;
    }

    public void run() {
        a(this.a);
    }

    private void a(IFile iFile) {
        if (!isInterrupted()) {
            if (iFile.isFile()) {
                iFile.delete();
            } else if (iFile.isDirectory()) {
                if (!this.b) {
                    iFile.delete();
                    return;
                }
                try {
                    List<IFile> a2 = this.c.a(iFile);
                    if (a2 == null) {
                        iFile.delete();
                        return;
                    }
                    for (IFile iFile2 : a2) {
                        if (isInterrupted()) {
                            return;
                        }
                        if (iFile2.isFile()) {
                            iFile2.delete();
                        } else if (iFile2.isDirectory()) {
                            if (this.b) {
                                a(iFile2);
                            } else {
                                iFile2.delete();
                            }
                        }
                    }
                    iFile.delete();
                } catch (Throwable th) {
                }
            }
        }
    }
}
