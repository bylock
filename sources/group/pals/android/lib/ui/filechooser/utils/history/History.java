package group.pals.android.lib.ui.filechooser.utils.history;

import android.os.Parcelable;
import java.util.ArrayList;

/* compiled from: MyApp */
public interface History extends Parcelable {
    int a();

    void a(a aVar);

    void a(b bVar);

    void a(Object obj);

    ArrayList b();

    void b(Object obj);

    void c(Object obj);

    boolean c();

    int d(Object obj);

    void d();

    Object e(Object obj);

    void e();

    Object f(Object obj);
}
