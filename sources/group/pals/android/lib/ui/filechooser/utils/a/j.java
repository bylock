package group.pals.android.lib.ui.filechooser.utils.a;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import group.pals.android.lib.ui.filechooser.an;
import group.pals.android.lib.ui.filechooser.av;
import group.pals.android.lib.ui.filechooser.bc;
import group.pals.android.lib.ui.filechooser.be;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.c;
import group.pals.android.lib.ui.filechooser.services.d;
import group.pals.android.lib.ui.filechooser.utils.history.History;
import java.util.ArrayList;

/* compiled from: MyApp */
public class j {
    public static void a(Context context, c cVar, History history, IFile iFile, i iVar) {
        boolean z;
        if (!history.c()) {
            AlertDialog a = d.a(context);
            a.setButton(-2, (CharSequence) null, (DialogInterface.OnClickListener) null);
            a.setIcon(17301659);
            a.setTitle(be.afc_title_history);
            ArrayList arrayList = new ArrayList();
            ArrayList b = history.b();
            for (int size = b.size() - 1; size >= 0; size--) {
                IFile iFile2 = (IFile) b.get(size);
                if (iFile2 != iFile) {
                    int i = 0;
                    while (true) {
                        if (i >= arrayList.size()) {
                            z = false;
                            break;
                        } else if (iFile2.a(((av) arrayList.get(i)).a())) {
                            z = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (!z) {
                        arrayList.add(new av(iFile2));
                    }
                }
            }
            an anVar = new an(context, arrayList, d.DirectoriesOnly, false);
            ListView listView = (ListView) LayoutInflater.from(context).inflate(bc.afc_listview_files, (ViewGroup) null);
            listView.setBackgroundResource(0);
            listView.setFastScrollEnabled(true);
            listView.setAdapter((ListAdapter) anVar);
            listView.setOnItemClickListener(new k(iVar, a, anVar));
            a.setView(listView);
            a.setButton(-1, context.getString(be.afc_cmd_clear), new l(history));
            a.setOnCancelListener(new m(iVar));
            a.show();
        }
    }
}
