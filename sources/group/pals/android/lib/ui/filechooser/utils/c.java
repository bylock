package group.pals.android.lib.ui.filechooser.utils;

import android.content.Context;
import android.text.format.DateUtils;
import group.pals.android.lib.ui.filechooser.a.b;
import group.pals.android.lib.ui.filechooser.be;
import java.util.Calendar;

/* compiled from: MyApp */
public class c {
    public static String a(Context context, long j, b bVar) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        return a(context, instance, bVar);
    }

    public static String a(Context context, Calendar calendar, b bVar) {
        Calendar instance = Calendar.getInstance();
        instance.add(6, -1);
        if (DateUtils.isToday(calendar.getTimeInMillis())) {
            return DateUtils.formatDateTime(context, calendar.getTimeInMillis(), 65);
        }
        if (calendar.get(1) == instance.get(1) && calendar.get(6) == instance.get(6)) {
            return String.format("%s, %s", context.getString(be.afc_yesterday), DateUtils.formatDateTime(context, calendar.getTimeInMillis(), 65));
        } else if (calendar.get(1) == instance.get(1)) {
            if (bVar.a()) {
                return DateUtils.formatDateTime(context, calendar.getTimeInMillis(), 65625);
            }
            return DateUtils.formatDateTime(context, calendar.getTimeInMillis(), 65560);
        } else if (bVar.b()) {
            return DateUtils.formatDateTime(context, calendar.getTimeInMillis(), 65629);
        } else {
            return DateUtils.formatDateTime(context, calendar.getTimeInMillis(), 65564);
        }
    }
}
