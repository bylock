package group.pals.android.lib.ui.filechooser;

import android.content.DialogInterface;

/* compiled from: MyApp */
class s implements DialogInterface.OnCancelListener {
    final /* synthetic */ FileChooserActivity a;
    private final /* synthetic */ av b;

    s(FileChooserActivity fileChooserActivity, av avVar) {
        this.a = fileChooserActivity;
        this.b = avVar;
    }

    public void onCancel(DialogInterface dialogInterface) {
        FileChooserActivity.a(this.a, this.b);
    }
}
