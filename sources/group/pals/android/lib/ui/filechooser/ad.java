package group.pals.android.lib.ui.filechooser;

import android.widget.ImageView;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.utils.a.i;

/* compiled from: MyApp */
class ad implements i {
    final /* synthetic */ ac a;

    ad(ac acVar) {
        this.a = acVar;
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.a.i
    public void a(boolean z, Object obj) {
        boolean z2 = true;
        if (z) {
            this.a.a.J.setEnabled(true);
            ImageView imageView = this.a.a.K;
            if (this.a.a.y.f(this.a.a.p()) == null) {
                z2 = false;
            }
            imageView.setEnabled(z2);
            this.a.a.z.a((IFile) obj);
        }
    }
}
