package group.pals.android.lib.ui.filechooser;

import android.content.Context;
import android.os.Bundle;
import group.pals.android.lib.ui.filechooser.a.a;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.utils.a.e;

/* compiled from: MyApp */
class f extends e {
    final /* synthetic */ FileChooserActivity a;
    private final /* synthetic */ Bundle b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(FileChooserActivity fileChooserActivity, Context context, int i, boolean z, Bundle bundle) {
        super(context, i, z);
        this.a = fileChooserActivity;
        this.b = bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object doInBackground(Void... voidArr) {
        int i = 0;
        while (FileChooserActivity.f(this.a) == null) {
            i += 200;
            try {
                Thread.sleep(200);
                if (i >= 3000) {
                    return null;
                }
            } catch (InterruptedException e) {
                return null;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // android.os.AsyncTask, group.pals.android.lib.ui.filechooser.utils.a.e
    public void onPostExecute(Object obj) {
        IFile iFile;
        String g;
        IFile iFile2 = null;
        super.onPostExecute(obj);
        if (FileChooserActivity.f(this.a) == null) {
            FileChooserActivity.n(this.a);
            return;
        }
        FileChooserActivity.o(this.a);
        FileChooserActivity.p(this.a);
        FileChooserActivity.q(this.a);
        FileChooserActivity.r(this.a);
        IFile iFile3 = this.b != null ? (IFile) this.b.get(FileChooserActivity.o) : null;
        if (iFile3 == null) {
            IFile iFile4 = (IFile) this.a.getIntent().getParcelableExtra(FileChooserActivity.k);
            if (iFile4 != null && iFile4.exists()) {
                iFile3 = iFile4.a();
            }
            if (iFile3 == null) {
                iFile = iFile3;
            } else {
                iFile2 = iFile4;
                iFile = iFile3;
            }
        } else {
            iFile = iFile3;
        }
        if (iFile == null && a.f(this.a) && (g = a.g(this.a)) != null) {
            iFile = FileChooserActivity.f(this.a).b(g);
        }
        FileChooserActivity fileChooserActivity = this.a;
        if (iFile == null || !iFile.isDirectory()) {
            iFile = FileChooserActivity.s(this.a);
        }
        FileChooserActivity.a(fileChooserActivity, iFile, new g(this, iFile2, this.b), iFile2);
    }
}
