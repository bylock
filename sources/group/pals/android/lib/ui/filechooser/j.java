package group.pals.android.lib.ui.filechooser;

import android.content.DialogInterface;

/* compiled from: MyApp */
class j implements DialogInterface.OnCancelListener {
    final /* synthetic */ FileChooserActivity a;

    j(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.a.setResult(0);
        this.a.finish();
    }
}
