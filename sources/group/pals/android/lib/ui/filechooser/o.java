package group.pals.android.lib.ui.filechooser;

import android.content.DialogInterface;
import android.widget.EditText;
import group.pals.android.lib.ui.filechooser.utils.a.d;
import group.pals.android.lib.ui.filechooser.utils.a.i;
import group.pals.android.lib.ui.filechooser.utils.f;

/* compiled from: MyApp */
class o implements DialogInterface.OnClickListener {
    final /* synthetic */ FileChooserActivity a;
    private final /* synthetic */ EditText b;

    o(FileChooserActivity fileChooserActivity, EditText editText) {
        this.a = fileChooserActivity;
        this.b = editText;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String trim = this.b.getText().toString().trim();
        if (!f.a(trim)) {
            d.a(this.a, this.a.getString(be.afc_pmsg_filename_is_invalid, new Object[]{trim}), 0);
            return;
        }
        if (FileChooserActivity.f(this.a).b(String.format("%s/%s", FileChooserActivity.a(this.a).getAbsolutePath(), trim)).mkdir()) {
            d.a(this.a, this.a.getString(be.afc_msg_done), 0);
            FileChooserActivity.a(this.a, FileChooserActivity.a(this.a), (i) null);
            return;
        }
        d.a(this.a, this.a.getString(be.afc_pmsg_cannot_create_folder, new Object[]{trim}), 0);
    }
}
