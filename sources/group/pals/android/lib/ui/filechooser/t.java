package group.pals.android.lib.ui.filechooser;

import android.content.DialogInterface;
import group.pals.android.lib.ui.filechooser.io.IFile;

/* compiled from: MyApp */
class t implements DialogInterface.OnClickListener {
    final /* synthetic */ FileChooserActivity a;
    private final /* synthetic */ IFile b;

    t(FileChooserActivity fileChooserActivity, IFile iFile) {
        this.a = fileChooserActivity;
        this.b = iFile;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        FileChooserActivity.a(this.a, new IFile[]{this.b});
    }
}
