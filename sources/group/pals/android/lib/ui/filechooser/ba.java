package group.pals.android.lib.ui.filechooser;

/* compiled from: MyApp */
public final class ba {
    public static final int afc_context_menu_view_listview_menu = 2131165321;
    public static final int afc_file_item_checkbox_selection = 2131165341;
    public static final int afc_file_item_imageview_icon = 2131165338;
    public static final int afc_file_item_textview_file_info = 2131165340;
    public static final int afc_file_item_textview_filename = 2131165339;
    public static final int afc_filechooser_activity_button_go_back = 2131165323;
    public static final int afc_filechooser_activity_button_go_forward = 2131165324;
    public static final int afc_filechooser_activity_button_ok = 2131165337;
    public static final int afc_filechooser_activity_footer_view_divider = 2131165334;
    public static final int afc_filechooser_activity_header_view_divider = 2131165328;
    public static final int afc_filechooser_activity_menugroup_editor = 2131165421;
    public static final int afc_filechooser_activity_menugroup_navigator = 2131165414;
    public static final int afc_filechooser_activity_menugroup_sorter = 2131165417;
    public static final int afc_filechooser_activity_menugroup_view = 2131165419;
    public static final int afc_filechooser_activity_menuitem_home = 2131165415;
    public static final int afc_filechooser_activity_menuitem_new_folder = 2131165422;
    public static final int afc_filechooser_activity_menuitem_reload = 2131165416;
    public static final int afc_filechooser_activity_menuitem_sort = 2131165418;
    public static final int afc_filechooser_activity_menuitem_switch_viewmode = 2131165420;
    public static final int afc_filechooser_activity_textview_full_dir_name = 2131165329;
    public static final int afc_filechooser_activity_textview_saveas_filename = 2131165336;
    public static final int afc_filechooser_activity_view_files_container = 2131165332;
    public static final int afc_filechooser_activity_view_files_footer_view = 2131165333;
    public static final int afc_filechooser_activity_view_locations = 2131165327;
    public static final int afc_filechooser_activity_view_locations_container = 2131165326;
    public static final int afc_filechooser_activity_viewgroup_button_locations = 2131165325;
    public static final int afc_filechooser_activity_viewgroup_files = 2131165330;
    public static final int afc_filechooser_activity_viewgroup_footer = 2131165335;
    public static final int afc_filechooser_activity_viewgroup_footer_container = 2131165331;
    public static final int afc_filechooser_activity_viewgroup_header = 2131165322;
    public static final int afc_settings_sort_view_button_sort_by_date_asc = 2131165346;
    public static final int afc_settings_sort_view_button_sort_by_date_desc = 2131165347;
    public static final int afc_settings_sort_view_button_sort_by_name_asc = 2131165342;
    public static final int afc_settings_sort_view_button_sort_by_name_desc = 2131165343;
    public static final int afc_settings_sort_view_button_sort_by_size_asc = 2131165344;
    public static final int afc_settings_sort_view_button_sort_by_size_desc = 2131165345;
    public static final int afc_simple_text_input_view_text1 = 2131165348;
}
