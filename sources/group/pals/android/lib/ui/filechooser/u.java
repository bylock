package group.pals.android.lib.ui.filechooser;

import android.content.Context;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.utils.a.d;
import group.pals.android.lib.ui.filechooser.utils.a.e;
import group.pals.android.lib.ui.filechooser.utils.a.i;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: MyApp */
class u extends e {
    List a;
    boolean b = false;
    int c = -1;
    String d;
    final /* synthetic */ FileChooserActivity e;
    private final /* synthetic */ IFile g;
    private final /* synthetic */ IFile h;
    private final /* synthetic */ i i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    u(FileChooserActivity fileChooserActivity, Context context, int i2, boolean z, IFile iFile, IFile iFile2, i iVar) {
        super(context, i2, z);
        this.e = fileChooserActivity;
        this.g = iFile;
        this.h = iFile2;
        this.i = iVar;
        this.d = FileChooserActivity.a(fileChooserActivity) != null ? FileChooserActivity.a(fileChooserActivity).getAbsolutePath() : null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object doInBackground(Void... voidArr) {
        try {
            if (!this.g.isDirectory() || !this.g.canRead()) {
                this.a = null;
            } else {
                this.a = new ArrayList();
                FileChooserActivity.f(this.e).a(this.g, new v(this));
            }
            if (this.a != null) {
                Collections.sort(this.a, new group.pals.android.lib.ui.filechooser.utils.e(FileChooserActivity.f(this.e).d(), FileChooserActivity.f(this.e).e()));
                if (this.h != null && this.h.exists() && this.h.a().a(this.g)) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= this.a.size()) {
                            break;
                        } else if (((IFile) this.a.get(i2)).a(this.h)) {
                            this.c = i2;
                            break;
                        } else {
                            i2++;
                        }
                    }
                } else if (this.d != null && this.d.length() >= this.g.getAbsolutePath().length()) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= this.a.size()) {
                            break;
                        }
                        IFile iFile = (IFile) this.a.get(i3);
                        if (iFile.isDirectory() && this.d.startsWith(iFile.getAbsolutePath())) {
                            this.c = i3;
                            break;
                        }
                        i3++;
                    }
                }
            }
        } catch (Throwable th) {
            a(th);
            cancel(false);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // group.pals.android.lib.ui.filechooser.utils.a.e
    public void onCancelled() {
        super.onCancelled();
        d.a(this.e, be.afc_msg_cancelled, 0);
    }

    /* access modifiers changed from: protected */
    @Override // android.os.AsyncTask, group.pals.android.lib.ui.filechooser.utils.a.e
    public void onPostExecute(Object obj) {
        super.onPostExecute(obj);
        if (this.a == null) {
            d.a(this.e, this.e.getString(be.afc_pmsg_cannot_access_dir, new Object[]{this.g.getName()}), 0);
            if (this.i != null) {
                this.i.a(false, this.g);
                return;
            }
            return;
        }
        FileChooserActivity.x(this.e);
        for (IFile iFile : this.a) {
            FileChooserActivity.k(this.e).a(new av(iFile));
        }
        FileChooserActivity.k(this.e).notifyDataSetChanged();
        FileChooserActivity.y(this.e).setVisibility((this.b || FileChooserActivity.k(this.e).isEmpty()) ? 0 : 8);
        if (this.b) {
            FileChooserActivity.y(this.e).setText(this.e.getString(be.afc_pmsg_max_file_count_allowed, new Object[]{Integer.valueOf(FileChooserActivity.f(this.e).f())}));
        } else if (FileChooserActivity.k(this.e).isEmpty()) {
            FileChooserActivity.y(this.e).setText(be.afc_msg_empty);
        }
        FileChooserActivity.j(this.e).post(new w(this));
        FileChooserActivity.b(this.e, this.g);
        if (this.i != null) {
            this.i.a(true, this.g);
        }
    }
}
