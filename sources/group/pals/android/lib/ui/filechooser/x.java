package group.pals.android.lib.ui.filechooser;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.utils.a.i;

/* compiled from: MyApp */
class x implements i {
    IFile a;
    final /* synthetic */ FileChooserActivity b;
    private final /* synthetic */ IFile c;

    x(FileChooserActivity fileChooserActivity, IFile iFile) {
        this.b = fileChooserActivity;
        this.c = iFile;
        this.a = FileChooserActivity.a(fileChooserActivity);
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.a.i
    public void a(boolean z, Object obj) {
        if (z) {
            FileChooserActivity.b(this.b).b(this.a);
            FileChooserActivity.b(this.b).a(this.c);
            FileChooserActivity.e(this.b).a(this.c);
        }
    }
}
