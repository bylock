package group.pals.android.lib.ui.filechooser;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: MyApp */
class h implements View.OnTouchListener {
    final /* synthetic */ FileChooserActivity a;

    h(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        return FileChooserActivity.t(this.a).onTouchEvent(motionEvent);
    }
}
