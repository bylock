package group.pals.android.lib.ui.filechooser;

import android.view.View;
import group.pals.android.lib.ui.filechooser.io.IFile;

/* compiled from: MyApp */
class m implements View.OnClickListener {
    final /* synthetic */ FileChooserActivity a;

    m(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public void onClick(View view) {
        if (view.getTag() instanceof IFile) {
            FileChooserActivity.a(this.a, (IFile) view.getTag());
        }
    }
}
