package group.pals.android.lib.ui.filechooser;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.utils.a.i;

/* compiled from: MyApp */
class b implements i {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.a.i
    public void a(boolean z, Object obj) {
        if (z) {
            this.a.a.J.setEnabled(this.a.a.y.e(this.a.a.p()) != null);
            this.a.a.K.setEnabled(true);
            this.a.a.z.a((IFile) obj);
        }
    }
}
