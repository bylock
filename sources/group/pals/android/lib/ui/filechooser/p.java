package group.pals.android.lib.ui.filechooser;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import group.pals.android.lib.ui.filechooser.utils.f;

/* compiled from: MyApp */
class p implements TextWatcher {
    final /* synthetic */ FileChooserActivity a;
    private final /* synthetic */ Button b;

    p(FileChooserActivity fileChooserActivity, Button button) {
        this.a = fileChooserActivity;
        this.b = button;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        this.b.setEnabled(f.a(editable.toString().trim()));
    }
}
