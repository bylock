package group.pals.android.lib.ui.filechooser;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.io.a;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class v implements a {
    final /* synthetic */ u a;

    v(u uVar) {
        this.a = uVar;
    }

    @Override // group.pals.android.lib.ui.filechooser.io.a
    public boolean a(IFile iFile) {
        if (!this.a.e.s.b(iFile)) {
            return false;
        }
        if (this.a.a.size() < this.a.e.s.f()) {
            this.a.a.add(iFile);
            return false;
        }
        this.a.b = true;
        return false;
    }
}
