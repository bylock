package group.pals.android.lib.ui.filechooser;

import android.view.View;
import android.widget.AdapterView;
import group.pals.android.lib.ui.filechooser.io.IFile;

/* compiled from: MyApp */
class al implements AdapterView.OnItemClickListener {
    final /* synthetic */ FileChooserActivity a;

    al(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        av a2 = FileChooserActivity.k(this.a).getItem(i);
        if (a2.a().isDirectory()) {
            FileChooserActivity.a(this.a, a2.a());
            return;
        }
        if (FileChooserActivity.g(this.a)) {
            FileChooserActivity.h(this.a).setText(a2.a().getName());
        }
        if (!FileChooserActivity.l(this.a) && !FileChooserActivity.m(this.a)) {
            if (FileChooserActivity.g(this.a)) {
                FileChooserActivity.a(this.a, a2.a().getName());
                return;
            }
            FileChooserActivity.a(this.a, new IFile[]{a2.a()});
        }
    }
}
