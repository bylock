package group.pals.android.lib.ui.filechooser;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import group.pals.android.lib.ui.filechooser.a.a;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.LocalFileProvider;
import group.pals.android.lib.ui.filechooser.services.c;
import group.pals.android.lib.ui.filechooser.services.d;
import group.pals.android.lib.ui.filechooser.services.e;
import group.pals.android.lib.ui.filechooser.services.f;
import group.pals.android.lib.ui.filechooser.utils.a.i;
import group.pals.android.lib.ui.filechooser.utils.history.History;
import group.pals.android.lib.ui.filechooser.utils.history.HistoryStore;
import group.pals.android.lib.ui.filechooser.utils.history.b;
import java.io.File;
import java.util.ArrayList;

/* compiled from: MyApp */
public class FileChooserActivity extends Activity {
    private static final int[] L = {ba.afc_settings_sort_view_button_sort_by_name_asc, ba.afc_settings_sort_view_button_sort_by_name_desc, ba.afc_settings_sort_view_button_sort_by_size_asc, ba.afc_settings_sort_view_button_sort_by_size_desc, ba.afc_settings_sort_view_button_sort_by_date_asc, ba.afc_settings_sort_view_button_sort_by_date_desc};
    private static /* synthetic */ int[] X;
    private static /* synthetic */ int[] Y;
    private static /* synthetic */ int[] Z;
    public static final String a = FileChooserActivity.class.getName();
    public static final String b = (String.valueOf(a) + ".theme");
    public static final String c = (String.valueOf(a) + ".rootpath");
    public static final String d = (String.valueOf(a) + ".file_provider_class");
    public static final String e = d.class.getName();
    public static final String f = (String.valueOf(a) + ".max_file_count");
    public static final String g = (String.valueOf(a) + ".multi_selection");
    public static final String h = (String.valueOf(a) + ".regex_filename_filter");
    public static final String i = (String.valueOf(a) + ".display_hidden_files");
    public static final String j = (String.valueOf(a) + ".double_tap_to_choose_files");
    public static final String k = (String.valueOf(a) + ".select_file");
    public static final String l = (String.valueOf(a) + ".save_dialog");
    public static final String m = (String.valueOf(a) + ".default_filename");
    public static final String n = (String.valueOf(a) + ".results");
    static final String o = (String.valueOf(a) + ".current_location");
    static final String p = (String.valueOf(a) + ".history");
    static final String q = (String.valueOf(History.class.getName()) + "_full");
    private an A;
    private HorizontalScrollView B;
    private ViewGroup C;
    private ViewGroup D;
    private TextView E;
    private AbsListView F;
    private TextView G;
    private Button H;
    private EditText I;
    private ImageView J;
    private ImageView K;
    private final View.OnClickListener M = new a(this);
    private final View.OnClickListener N = new m(this);
    private final View.OnLongClickListener O = new aa(this);
    private final View.OnClickListener P = new ac(this);
    private final View.OnLongClickListener Q = new ae(this);
    private final TextView.OnEditorActionListener R = new ai(this);
    private final View.OnClickListener S = new aj(this);
    private final View.OnClickListener T = new ak(this);
    private GestureDetector U;
    private final AdapterView.OnItemClickListener V = new al(this);
    private final AdapterView.OnItemLongClickListener W = new c(this);
    private Class r;
    private c s;
    private ServiceConnection t;
    private IFile u;
    private boolean v;
    private boolean w;
    private boolean x;
    private History y;
    private History z;

    static /* synthetic */ int[] a() {
        int[] iArr = X;
        if (iArr == null) {
            iArr = new int[f.values().length];
            try {
                iArr[f.SortByDate.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[f.SortByName.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[f.SortBySize.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            X = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] b() {
        int[] iArr = Y;
        if (iArr == null) {
            iArr = new int[am.values().length];
            try {
                iArr[am.Grid.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[am.List.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            Y = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] c() {
        int[] iArr = Z;
        if (iArr == null) {
            iArr = new int[d.values().length];
            try {
                iArr[d.DirectoriesOnly.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[d.FilesAndDirectories.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[d.FilesOnly.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            Z = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle bundle) {
        int intExtra;
        if (getIntent().hasExtra(b)) {
            if (Build.VERSION.SDK_INT >= 14) {
                intExtra = getIntent().getIntExtra(b, 16974120);
            } else if (Build.VERSION.SDK_INT >= 11) {
                intExtra = getIntent().getIntExtra(b, 16973931);
            } else {
                intExtra = getIntent().getIntExtra(b, 16973829);
            }
            setTheme(intExtra);
        }
        super.onCreate(bundle);
        setContentView(bc.afc_file_chooser);
        r();
        this.r = (Class) getIntent().getSerializableExtra(d);
        if (this.r == null) {
            this.r = LocalFileProvider.class;
        }
        this.v = getIntent().getBooleanExtra(g, false);
        this.w = getIntent().getBooleanExtra(l, false);
        if (this.w) {
            this.v = false;
        }
        this.x = getIntent().getBooleanExtra(j, false);
        this.J = (ImageView) findViewById(ba.afc_filechooser_activity_button_go_back);
        this.K = (ImageView) findViewById(ba.afc_filechooser_activity_button_go_forward);
        this.C = (ViewGroup) findViewById(ba.afc_filechooser_activity_view_locations);
        this.B = (HorizontalScrollView) findViewById(ba.afc_filechooser_activity_view_locations_container);
        this.E = (TextView) findViewById(ba.afc_filechooser_activity_textview_full_dir_name);
        this.D = (ViewGroup) findViewById(ba.afc_filechooser_activity_view_files_container);
        this.G = (TextView) findViewById(ba.afc_filechooser_activity_view_files_footer_view);
        this.I = (EditText) findViewById(ba.afc_filechooser_activity_textview_saveas_filename);
        this.H = (Button) findViewById(ba.afc_filechooser_activity_button_ok);
        if (bundle == null || !(bundle.get(p) instanceof HistoryStore)) {
            this.y = new HistoryStore(51);
        } else {
            this.y = (History) bundle.getParcelable(p);
        }
        this.y.a((b) new d(this));
        if (bundle == null || !(bundle.get(q) instanceof HistoryStore)) {
            this.z = new HistoryStore(51) {
                /* class group.pals.android.lib.ui.filechooser.FileChooserActivity.AnonymousClass12 */

                public void a(IFile iFile) {
                    int d = d((Parcelable) iFile);
                    if (d >= 0) {
                        if (d != a() - 1) {
                            c((Parcelable) iFile);
                        } else {
                            return;
                        }
                    }
                    super.a((Parcelable) iFile);
                }
            };
        } else {
            this.z = (History) bundle.getParcelable(q);
        }
        setResult(0);
        a(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(bd.afc_file_chooser_activity, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        int i2;
        int i3;
        int i4;
        boolean c2 = a.c(this);
        MenuItem findItem = menu.findItem(ba.afc_filechooser_activity_menuitem_sort);
        switch (a()[a.b(this).ordinal()]) {
            case 1:
                if (c2) {
                    i4 = az.afc_ic_menu_sort_by_name_asc;
                } else {
                    i4 = az.afc_ic_menu_sort_by_name_desc;
                }
                findItem.setIcon(i4);
                break;
            case 2:
                if (c2) {
                    i3 = az.afc_ic_menu_sort_by_size_asc;
                } else {
                    i3 = az.afc_ic_menu_sort_by_size_desc;
                }
                findItem.setIcon(i3);
                break;
            case 3:
                if (c2) {
                    i2 = az.afc_ic_menu_sort_by_date_asc;
                } else {
                    i2 = az.afc_ic_menu_sort_by_date_desc;
                }
                findItem.setIcon(i2);
                break;
        }
        MenuItem findItem2 = menu.findItem(ba.afc_filechooser_activity_menuitem_switch_viewmode);
        switch (b()[a.a(this).ordinal()]) {
            case 1:
                findItem2.setIcon(az.afc_ic_menu_gridview);
                findItem2.setTitle(be.afc_cmd_grid_view);
                return true;
            case 2:
                findItem2.setIcon(az.afc_ic_menu_listview);
                findItem2.setTitle(be.afc_cmd_list_view);
                return true;
            default:
                return true;
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getGroupId() == ba.afc_filechooser_activity_menugroup_sorter) {
            l();
            return true;
        } else if (menuItem.getItemId() == ba.afc_filechooser_activity_menuitem_new_folder) {
            o();
            return true;
        } else if (menuItem.getItemId() == ba.afc_filechooser_activity_menuitem_switch_viewmode) {
            n();
            return true;
        } else if (menuItem.getItemId() == ba.afc_filechooser_activity_menuitem_home) {
            k();
            return true;
        } else if (menuItem.getItemId() != ba.afc_filechooser_activity_menuitem_reload) {
            return true;
        } else {
            i();
            return true;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putParcelable(o, p());
        bundle.putParcelable(p, this.y);
        bundle.putParcelable(q, this.z);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.v && !this.w && this.x) {
            group.pals.android.lib.ui.filechooser.utils.a.d.a(this, be.afc_hint_double_tap_to_select_file, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.s != null) {
            try {
                unbindService(this.t);
            } catch (Throwable th) {
                Log.e(a, "onDestroy() - unbindService() - exception: " + th);
            }
            try {
                stopService(new Intent(this, this.r));
            } catch (SecurityException e2) {
            }
        }
        super.onDestroy();
    }

    private void a(Bundle bundle) {
        if (startService(new Intent(this, this.r)) == null) {
            j();
            return;
        }
        this.t = new e(this);
        bindService(new Intent(this, this.r), this.t, 1);
        new f(this, this, be.afc_msg_loading, false, bundle).execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void d() {
        e eVar;
        if (getIntent().getParcelableExtra(c) != null) {
            this.u = (IFile) getIntent().getSerializableExtra(c);
        }
        if (this.u == null || !this.u.isDirectory()) {
            this.u = this.s.g();
        }
        d dVar = (d) getIntent().getSerializableExtra(e);
        if (dVar == null) {
            dVar = d.FilesOnly;
        }
        f b2 = a.b(this);
        boolean c2 = a.c(this);
        this.s.a(getIntent().getBooleanExtra(i, false));
        c cVar = this.s;
        if (this.w) {
            dVar = d.FilesOnly;
        }
        cVar.a(dVar);
        this.s.a(getIntent().getIntExtra(f, 1024));
        this.s.a(this.w ? null : getIntent().getStringExtra(h));
        c cVar2 = this.s;
        if (c2) {
            eVar = e.Ascending;
        } else {
            eVar = e.Descending;
        }
        cVar2.a(eVar);
        this.s.a(b2);
    }

    /* access modifiers changed from: private */
    public void e() {
        if (!this.w) {
            switch (c()[this.s.c().ordinal()]) {
                case 1:
                    setTitle(be.afc_title_choose_files);
                    break;
                case 2:
                    setTitle(be.afc_title_choose_directories);
                    break;
                case 3:
                    setTitle(be.afc_title_choose_files_and_directories);
                    break;
            }
        } else {
            setTitle(be.afc_title_save_as);
        }
        this.J.setEnabled(false);
        this.J.setOnClickListener(this.M);
        this.K.setEnabled(false);
        this.K.setOnClickListener(this.P);
        for (ImageView imageView : new ImageView[]{this.J, this.K}) {
            imageView.setOnLongClickListener(this.Q);
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        switch (b()[a.a(this).ordinal()]) {
            case 1:
                this.F = (AbsListView) getLayoutInflater().inflate(bc.afc_listview_files, (ViewGroup) null);
                break;
            case 2:
                this.F = (AbsListView) getLayoutInflater().inflate(bc.afc_gridview_files, (ViewGroup) null);
                break;
        }
        this.D.removeAllViews();
        this.D.addView(this.F, new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.F.setOnItemClickListener(this.V);
        this.F.setOnItemLongClickListener(this.W);
        this.F.setOnTouchListener(new h(this));
        g();
        this.G.setOnLongClickListener(new i(this));
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.A != null) {
            this.A.b();
        }
        this.A = new an(this, new ArrayList(), this.s.c(), this.v);
        if (this.F instanceof ListView) {
            ((ListView) this.F).setAdapter((ListAdapter) this.A);
        } else {
            ((GridView) this.F).setAdapter((ListAdapter) this.A);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        ViewGroup viewGroup = (ViewGroup) findViewById(ba.afc_filechooser_activity_viewgroup_footer_container);
        ViewGroup viewGroup2 = (ViewGroup) findViewById(ba.afc_filechooser_activity_viewgroup_footer);
        if (this.w) {
            viewGroup.setVisibility(0);
            viewGroup2.setVisibility(0);
            this.I.setVisibility(0);
            this.I.setText(getIntent().getStringExtra(m));
            this.I.setOnEditorActionListener(this.R);
            this.H.setVisibility(0);
            this.H.setOnClickListener(this.S);
            this.H.setBackgroundResource(az.afc_selector_button_ok_saveas);
            int dimensionPixelSize = getResources().getDimensionPixelSize(ay.afc_button_ok_saveas_size);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.H.getLayoutParams();
            layoutParams.width = dimensionPixelSize;
            layoutParams.height = dimensionPixelSize;
            this.H.setLayoutParams(layoutParams);
        } else if (this.v) {
            viewGroup.setVisibility(0);
            viewGroup2.setVisibility(0);
            ViewGroup.LayoutParams layoutParams2 = viewGroup2.getLayoutParams();
            layoutParams2.width = -2;
            viewGroup2.setLayoutParams(layoutParams2);
            this.H.setMinWidth(getResources().getDimensionPixelSize(ay.afc_single_button_min_width));
            this.H.setText(17039370);
            this.H.setVisibility(0);
            this.H.setOnClickListener(this.T);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        a(p(), (i) null);
    }

    /* access modifiers changed from: private */
    public void j() {
        group.pals.android.lib.ui.filechooser.utils.a.d.a(this, be.afc_msg_cannot_connect_to_file_provider_service, new j(this));
    }

    private void k() {
        a(this.u.b());
    }

    private void l() {
        int i2;
        int i3;
        AlertDialog a2 = group.pals.android.lib.ui.filechooser.utils.a.d.a(this);
        switch (a()[a.b(this).ordinal()]) {
            case 1:
                i2 = 0;
                break;
            case 2:
                i2 = 2;
                break;
            case 3:
                i2 = 4;
                break;
            default:
                i2 = 0;
                break;
        }
        if (!a.c(this)) {
            i3 = i2 + 1;
        } else {
            i3 = i2;
        }
        k kVar = new k(this, a2);
        View inflate = getLayoutInflater().inflate(bc.afc_settings_sort_view, (ViewGroup) null);
        for (int i4 = 0; i4 < L.length; i4++) {
            Button button = (Button) inflate.findViewById(L[i4]);
            button.setOnClickListener(kVar);
            if (i4 == i3) {
                button.setEnabled(false);
                if (Build.VERSION.SDK_INT >= 11) {
                    button.setText(be.afc_ellipsize);
                }
            }
        }
        a2.setTitle(be.afc_title_sort_by);
        a2.setView(inflate);
        a2.show();
    }

    /* access modifiers changed from: private */
    public void m() {
        if (!this.s.d().equals(a.b(this)) || this.s.e().a() != a.c(this)) {
            this.s.a(a.b(this));
            this.s.a(a.c(this) ? e.Ascending : e.Descending);
            i();
            if (Build.VERSION.SDK_INT >= 11) {
                group.pals.android.lib.ui.filechooser.utils.a.a(this);
            }
        }
    }

    private void n() {
        new l(this, this, be.afc_msg_loading, false).execute(new Void[0]);
    }

    private void o() {
        if (this.s instanceof LocalFileProvider) {
            if (!group.pals.android.lib.ui.filechooser.utils.i.a(this, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                group.pals.android.lib.ui.filechooser.utils.a.d.a(this, be.afc_msg_app_doesnot_have_permission_to_create_files, 0);
                return;
            }
        }
        AlertDialog a2 = group.pals.android.lib.ui.filechooser.utils.a.d.a(this);
        View inflate = getLayoutInflater().inflate(bc.afc_simple_text_input_view, (ViewGroup) null);
        EditText editText = (EditText) inflate.findViewById(ba.afc_simple_text_input_view_text1);
        editText.setHint(be.afc_hint_folder_name);
        editText.setOnEditorActionListener(new n(this, editText, a2));
        a2.setView(inflate);
        a2.setTitle(be.afc_cmd_new_folder);
        a2.setIcon(17301555);
        a2.setButton(-1, getString(17039370), new o(this, editText));
        a2.show();
        Button button = a2.getButton(-1);
        button.setEnabled(false);
        editText.addTextChangedListener(new p(this, button));
    }

    /* access modifiers changed from: private */
    public void a(av avVar) {
        avVar.b(false);
        this.A.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void b(av avVar) {
        String string;
        if (this.s instanceof LocalFileProvider) {
            if (!group.pals.android.lib.ui.filechooser.utils.i.a(this, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                a(avVar);
                group.pals.android.lib.ui.filechooser.utils.a.d.a(this, be.afc_msg_app_doesnot_have_permission_to_delete_files, 0);
                return;
            }
        }
        int i2 = be.afc_pmsg_confirm_delete_file;
        Object[] objArr = new Object[2];
        if (avVar.a().isFile()) {
            string = getString(be.afc_file);
        } else {
            string = getString(be.afc_folder);
        }
        objArr[0] = string;
        objArr[1] = avVar.a().getName();
        group.pals.android.lib.ui.filechooser.utils.a.d.a(this, getString(i2, objArr), new q(this, avVar), new s(this, avVar));
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (str.length() == 0) {
            group.pals.android.lib.ui.filechooser.utils.a.d.a(this, be.afc_msg_filename_is_empty, 0);
            return;
        }
        IFile b2 = this.s.b(String.valueOf(p().getAbsolutePath()) + File.separator + str);
        if (!group.pals.android.lib.ui.filechooser.utils.f.a(str)) {
            group.pals.android.lib.ui.filechooser.utils.a.d.a(this, getString(be.afc_pmsg_filename_is_invalid, new Object[]{str}), 0);
        } else if (b2.isFile()) {
            group.pals.android.lib.ui.filechooser.utils.a.d.a(this, getString(be.afc_pmsg_confirm_replace_file, new Object[]{b2.getName()}), new t(this, b2));
        } else if (b2.isDirectory()) {
            group.pals.android.lib.ui.filechooser.utils.a.d.a(this, getString(be.afc_pmsg_filename_is_directory, new Object[]{b2.getName()}), 0);
        } else {
            a(b2);
        }
    }

    /* access modifiers changed from: private */
    public IFile p() {
        return (IFile) this.C.getTag();
    }

    /* access modifiers changed from: private */
    public void a(IFile iFile, i iVar) {
        a(iFile, iVar, (IFile) null);
    }

    /* access modifiers changed from: private */
    public void a(IFile iFile, i iVar, IFile iFile2) {
        new u(this, this, be.afc_msg_loading, true, iFile, iFile2, iVar).execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public boolean a(IFile iFile) {
        if (iFile.a(p())) {
            return false;
        }
        a(iFile, new x(this, iFile));
        return true;
    }

    /* access modifiers changed from: private */
    public void b(IFile iFile) {
        String string;
        LinearLayout.LayoutParams layoutParams;
        this.C.setTag(iFile);
        this.C.removeAllViews();
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 17;
        LayoutInflater layoutInflater = getLayoutInflater();
        int dimensionPixelSize = getResources().getDimensionPixelSize(ay.afc_5dp);
        int i2 = 0;
        LinearLayout.LayoutParams layoutParams3 = null;
        while (iFile != null) {
            TextView textView = (TextView) layoutInflater.inflate(bc.afc_button_location, (ViewGroup) null);
            if (iFile.a() != null) {
                string = iFile.getName();
            } else {
                string = getString(be.afc_root);
            }
            textView.setText(string);
            textView.setTag(iFile);
            textView.setOnClickListener(this.N);
            textView.setOnLongClickListener(this.O);
            this.C.addView(textView, 0, layoutParams2);
            int i3 = i2 + 1;
            if (i2 == 0) {
                Rect rect = new Rect();
                textView.getPaint().getTextBounds(iFile.getName(), 0, iFile.getName().length(), rect);
                if (rect.width() >= (getResources().getDimensionPixelSize(ay.afc_button_location_max_width) - textView.getPaddingLeft()) - textView.getPaddingRight()) {
                    this.E.setText(iFile.getName());
                    this.E.setVisibility(0);
                } else {
                    this.E.setVisibility(8);
                }
            }
            iFile = iFile.a();
            if (iFile != null) {
                View inflate = layoutInflater.inflate(bc.afc_view_locations_divider, (ViewGroup) null);
                if (layoutParams3 == null) {
                    layoutParams = new LinearLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize);
                    layoutParams.gravity = 17;
                    layoutParams.setMargins(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
                } else {
                    layoutParams = layoutParams3;
                }
                this.C.addView(inflate, 0, layoutParams);
                i2 = i3;
                layoutParams3 = layoutParams;
            } else {
                i2 = i3;
            }
        }
        this.B.postDelayed(new y(this), 100);
    }

    /* access modifiers changed from: private */
    public void q() {
        z zVar = new z(this);
        this.y.a((group.pals.android.lib.ui.filechooser.utils.history.a) zVar);
        this.z.a((group.pals.android.lib.ui.filechooser.utils.history.a) zVar);
    }

    /* access modifiers changed from: private */
    public void a(IFile... iFileArr) {
        ArrayList arrayList = new ArrayList();
        for (IFile iFile : iFileArr) {
            arrayList.add(iFile);
        }
        a(arrayList);
    }

    /* access modifiers changed from: private */
    public void a(ArrayList arrayList) {
        if (arrayList == null || arrayList.isEmpty()) {
            setResult(0);
            finish();
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(n, arrayList);
        intent.putExtra(e, this.s.c());
        intent.putExtra(l, this.w);
        setResult(-1, intent);
        if (!a.f(this) || p() == null) {
            a.a(this, (String) null);
        } else {
            a.a(this, p().getAbsolutePath());
        }
        finish();
    }

    private void r() {
        this.U = new GestureDetector(this, new ab(this));
    }
}
