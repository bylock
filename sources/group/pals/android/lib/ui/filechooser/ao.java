package group.pals.android.lib.ui.filechooser;

import android.view.View;
import group.pals.android.lib.ui.filechooser.utils.a.a;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ao implements View.OnLongClickListener {
    final /* synthetic */ an a;

    ao(an anVar) {
        this.a = anVar;
    }

    public boolean onLongClick(View view) {
        a.a(view.getContext(), 0, be.afc_title_advanced_selection, this.a.b, new ap(this, view));
        return true;
    }
}
