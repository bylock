package group.pals.android.lib.ui.filechooser;

import android.view.View;
import group.pals.android.lib.ui.filechooser.utils.d;

/* compiled from: MyApp */
class i implements View.OnLongClickListener {
    final /* synthetic */ FileChooserActivity a;

    i(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public boolean onLongClick(View view) {
        d.a(this.a);
        return false;
    }
}
