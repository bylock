package group.pals.android.lib.ui.filechooser;

import android.content.DialogInterface;

/* compiled from: MyApp */
class q implements DialogInterface.OnClickListener {
    final /* synthetic */ FileChooserActivity a;
    private final /* synthetic */ av b;

    q(FileChooserActivity fileChooserActivity, av avVar) {
        this.a = fileChooserActivity;
        this.b = avVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        FileChooserActivity fileChooserActivity = this.a;
        FileChooserActivity fileChooserActivity2 = this.a;
        int i2 = be.afc_pmsg_deleting_file;
        Object[] objArr = new Object[2];
        objArr[0] = this.b.a().isFile() ? this.a.getString(be.afc_file) : this.a.getString(be.afc_folder);
        objArr[1] = this.b.a().getName();
        new r(this, fileChooserActivity, fileChooserActivity2.getString(i2, objArr), true, this.b).execute(new Void[0]);
    }
}
