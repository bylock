package group.pals.android.lib.ui.filechooser;

import android.view.View;
import android.widget.ListAdapter;
import java.util.ArrayList;

/* compiled from: MyApp */
class ak implements View.OnClickListener {
    final /* synthetic */ FileChooserActivity a;

    ak(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public void onClick(View view) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < ((ListAdapter) FileChooserActivity.j(this.a).getAdapter()).getCount(); i++) {
            Object item = ((ListAdapter) FileChooserActivity.j(this.a).getAdapter()).getItem(i);
            if (item instanceof av) {
                av avVar = (av) item;
                if (avVar.b()) {
                    arrayList.add(avVar.a());
                }
            }
        }
        FileChooserActivity.a(this.a, arrayList);
    }
}
