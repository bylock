package group.pals.android.lib.ui.filechooser.services;

import android.os.Environment;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.io.a;
import group.pals.android.lib.ui.filechooser.io.localfile.LocalFile;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* compiled from: MyApp */
public class LocalFileProvider extends a {
    private static /* synthetic */ int[] a;

    static /* synthetic */ int[] h() {
        int[] iArr = a;
        if (iArr == null) {
            iArr = new int[d.values().length];
            try {
                iArr[d.DirectoriesOnly.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[d.FilesAndDirectories.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[d.FilesOnly.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            a = iArr;
        }
        return iArr;
    }

    public void onCreate() {
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public IFile g() {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        return externalStorageDirectory == null ? b("/") : new LocalFile(externalStorageDirectory);
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public IFile b(String str) {
        return new LocalFile(str);
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public List a(IFile iFile) {
        if (!(iFile instanceof File) || !iFile.canRead()) {
            return null;
        }
        try {
            ArrayList arrayList = new ArrayList();
            if (((File) iFile).listFiles(new g(this, arrayList)) == null) {
                return null;
            }
            return arrayList;
        } catch (Throwable th) {
            return null;
        }
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public List a(IFile iFile, a aVar) {
        if (!(iFile instanceof File)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        try {
            if (((File) iFile).listFiles(new h(this, aVar, arrayList)) != null) {
                return arrayList;
            }
            return null;
        } catch (Throwable th) {
            return null;
        }
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public boolean b(IFile iFile) {
        if (!a() && iFile.getName().startsWith(".")) {
            return false;
        }
        switch (h()[c().ordinal()]) {
            case 1:
                if (b() == null || !iFile.isFile()) {
                    return true;
                }
                return iFile.getName().matches(b());
            case 2:
                return iFile.isDirectory();
            default:
                if (b() == null || !iFile.isFile()) {
                    return true;
                }
                return iFile.getName().matches(b());
        }
    }
}
