package group.pals.android.lib.ui.filechooser.services;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.io.a;
import java.util.List;

/* compiled from: MyApp */
public interface c {
    List a(IFile iFile);

    List a(IFile iFile, a aVar);

    void a(int i);

    void a(d dVar);

    void a(e eVar);

    void a(f fVar);

    void a(String str);

    void a(boolean z);

    IFile b(String str);

    boolean b(IFile iFile);

    d c();

    f d();

    e e();

    int f();

    IFile g();
}
