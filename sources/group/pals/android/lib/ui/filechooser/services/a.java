package group.pals.android.lib.ui.filechooser.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/* compiled from: MyApp */
public abstract class a extends Service implements c {
    private final IBinder a = new b(this);
    private boolean b = false;
    private String c = null;
    private d d = d.FilesOnly;
    private int e = 1024;
    private f f = f.SortByName;
    private e g = e.Ascending;

    public IBinder onBind(Intent intent) {
        return this.a;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public void a(boolean z) {
        this.b = z;
    }

    public boolean a() {
        return this.b;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public void a(String str) {
        this.c = str;
    }

    public String b() {
        return this.c;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public void a(d dVar) {
        this.d = dVar;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public d c() {
        return this.d;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public void a(f fVar) {
        this.f = fVar;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public f d() {
        return this.f;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public void a(e eVar) {
        this.g = eVar;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public e e() {
        return this.g;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public void a(int i) {
        this.e = i;
    }

    @Override // group.pals.android.lib.ui.filechooser.services.c
    public int f() {
        return this.e;
    }
}
