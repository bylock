package group.pals.android.lib.ui.filechooser.services;

/* compiled from: MyApp */
public enum e {
    Ascending(true),
    Descending(false);
    
    final boolean c;

    private e(boolean z) {
        this.c = z;
    }

    public boolean a() {
        return this.c;
    }
}
