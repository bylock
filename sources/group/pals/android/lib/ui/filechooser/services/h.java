package group.pals.android.lib.ui.filechooser.services;

import group.pals.android.lib.ui.filechooser.io.a;
import group.pals.android.lib.ui.filechooser.io.localfile.LocalFile;
import java.io.File;
import java.io.FileFilter;
import java.util.List;

/* compiled from: MyApp */
class h implements FileFilter {
    final /* synthetic */ LocalFileProvider a;
    private final /* synthetic */ a b;
    private final /* synthetic */ List c;

    h(LocalFileProvider localFileProvider, a aVar, List list) {
        this.a = localFileProvider;
        this.b = aVar;
        this.c = list;
    }

    public boolean accept(File file) {
        LocalFile localFile = new LocalFile(file);
        if (this.b != null && !this.b.a(localFile)) {
            return false;
        }
        this.c.add(localFile);
        return false;
    }
}
