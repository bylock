package group.pals.android.lib.ui.filechooser.services;

import group.pals.android.lib.ui.filechooser.io.localfile.LocalFile;
import java.io.File;
import java.io.FileFilter;
import java.util.List;

/* compiled from: MyApp */
class g implements FileFilter {
    final /* synthetic */ LocalFileProvider a;
    private final /* synthetic */ List b;

    g(LocalFileProvider localFileProvider, List list) {
        this.a = localFileProvider;
        this.b = list;
    }

    public boolean accept(File file) {
        this.b.add(new LocalFile(file));
        return false;
    }
}
