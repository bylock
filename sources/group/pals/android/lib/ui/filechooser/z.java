package group.pals.android.lib.ui.filechooser;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.utils.history.a;

/* compiled from: MyApp */
class z implements a {
    final /* synthetic */ FileChooserActivity a;

    z(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public boolean a(IFile iFile) {
        return !iFile.isDirectory();
    }
}
