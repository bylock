package group.pals.android.lib.ui.filechooser.io.localfile;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: MyApp */
class a implements Parcelable.Creator {
    a() {
    }

    /* renamed from: a */
    public LocalFile createFromParcel(Parcel parcel) {
        return new LocalFile(parcel, null);
    }

    /* renamed from: a */
    public LocalFile[] newArray(int i) {
        return new LocalFile[i];
    }
}
