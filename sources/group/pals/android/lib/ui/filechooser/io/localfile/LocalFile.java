package group.pals.android.lib.ui.filechooser.io.localfile;

import android.os.Parcel;
import android.os.Parcelable;
import group.pals.android.lib.ui.filechooser.io.IFile;
import java.io.File;

/* compiled from: MyApp */
public class LocalFile extends File implements IFile {
    public static final Parcelable.Creator CREATOR = new a();

    public LocalFile(String str) {
        super(str);
    }

    public LocalFile(File file) {
        this(file.getAbsolutePath());
    }

    @Override // group.pals.android.lib.ui.filechooser.io.IFile
    public IFile a() {
        if (getParent() == null) {
            return null;
        }
        return new LocalFile(getParent());
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    @Override // group.pals.android.lib.ui.filechooser.io.IFile
    public boolean a(IFile iFile) {
        if (iFile == null) {
            return false;
        }
        return getAbsolutePath().equals(iFile.getAbsolutePath());
    }

    @Override // group.pals.android.lib.ui.filechooser.io.IFile
    /* renamed from: b */
    public IFile clone() {
        return new LocalFile(getAbsolutePath());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getAbsolutePath());
    }

    /* synthetic */ LocalFile(Parcel parcel, LocalFile localFile) {
        this(parcel);
    }

    private LocalFile(Parcel parcel) {
        this(parcel.readString());
    }
}
