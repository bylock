package group.pals.android.lib.ui.filechooser;

/* compiled from: MyApp */
public final class be {
    public static final int afc_cmd_advanced_selection_all = 2131492917;
    public static final int afc_cmd_advanced_selection_invert = 2131492918;
    public static final int afc_cmd_advanced_selection_none = 2131492919;
    public static final int afc_cmd_clear = 2131492920;
    public static final int afc_cmd_grid_view = 2131492921;
    public static final int afc_cmd_home = 2131492922;
    public static final int afc_cmd_list_view = 2131492923;
    public static final int afc_cmd_new_folder = 2131492924;
    public static final int afc_cmd_reload = 2131492925;
    public static final int afc_cmd_remove_from_list = 2131492926;
    public static final int afc_cmd_select_all_files = 2131492927;
    public static final int afc_cmd_select_all_folders = 2131492928;
    public static final int afc_cmd_sort = 2131492929;
    public static final int afc_ellipsize = 2131492930;
    public static final int afc_file = 2131492931;
    public static final int afc_folder = 2131492932;
    public static final int afc_hint_double_tap_to_select_file = 2131492933;
    public static final int afc_hint_folder_name = 2131492934;
    public static final int afc_hint_save_as_filename = 2131492935;
    public static final int afc_lib_name = 2131492916;
    public static final int afc_msg_app_doesnot_have_permission_to_create_files = 2131492936;
    public static final int afc_msg_app_doesnot_have_permission_to_delete_files = 2131492937;
    public static final int afc_msg_cancelled = 2131492938;
    public static final int afc_msg_cannot_connect_to_file_provider_service = 2131492939;
    public static final int afc_msg_done = 2131492940;
    public static final int afc_msg_empty = 2131492941;
    public static final int afc_msg_filename_is_empty = 2131492942;
    public static final int afc_msg_loading = 2131492943;
    public static final int afc_pkey_display_last_location = 2131492915;
    public static final int afc_pkey_display_remember_last_location = 2131492914;
    public static final int afc_pkey_display_show_time_for_old_days = 2131492913;
    public static final int afc_pkey_display_show_time_for_old_days_this_year = 2131492912;
    public static final int afc_pkey_display_sort_ascending = 2131492911;
    public static final int afc_pkey_display_sort_type = 2131492910;
    public static final int afc_pkey_display_view_type = 2131492909;
    public static final int afc_pmsg_cannot_access_dir = 2131492944;
    public static final int afc_pmsg_cannot_create_folder = 2131492945;
    public static final int afc_pmsg_cannot_delete_file = 2131492946;
    public static final int afc_pmsg_confirm_delete_file = 2131492947;
    public static final int afc_pmsg_confirm_replace_file = 2131492948;
    public static final int afc_pmsg_deleting_file = 2131492949;
    public static final int afc_pmsg_file_has_been_deleted = 2131492950;
    public static final int afc_pmsg_filename_is_directory = 2131492951;
    public static final int afc_pmsg_filename_is_invalid = 2131492952;
    public static final int afc_pmsg_max_file_count_allowed = 2131492953;
    public static final int afc_pmsg_unknown_error = 2131492954;
    public static final int afc_pmsg_xxx_items = 2131492955;
    public static final int afc_root = 2131492956;
    public static final int afc_title_advanced_selection = 2131492957;
    public static final int afc_title_alert = 2131492958;
    public static final int afc_title_choose_directories = 2131492959;
    public static final int afc_title_choose_files = 2131492960;
    public static final int afc_title_choose_files_and_directories = 2131492961;
    public static final int afc_title_confirmation = 2131492962;
    public static final int afc_title_date = 2131492963;
    public static final int afc_title_error = 2131492964;
    public static final int afc_title_history = 2131492965;
    public static final int afc_title_info = 2131492966;
    public static final int afc_title_name = 2131492967;
    public static final int afc_title_save_as = 2131492968;
    public static final int afc_title_size = 2131492969;
    public static final int afc_title_sort_by = 2131492970;
    public static final int afc_yesterday = 2131492971;
}
