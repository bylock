package group.pals.android.lib.ui.filechooser;

import android.view.View;
import group.pals.android.lib.ui.filechooser.io.IFile;

/* compiled from: MyApp */
class ac implements View.OnClickListener {
    final /* synthetic */ FileChooserActivity a;

    ac(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public void onClick(View view) {
        IFile iFile;
        IFile a2 = FileChooserActivity.a(this.a);
        while (true) {
            iFile = (IFile) FileChooserActivity.b(this.a).f(a2);
            if (!a2.a(iFile)) {
                break;
            }
            FileChooserActivity.b(this.a).c(iFile);
        }
        if (iFile != null) {
            FileChooserActivity.a(this.a, iFile, new ad(this));
        } else {
            FileChooserActivity.d(this.a).setEnabled(false);
        }
    }
}
