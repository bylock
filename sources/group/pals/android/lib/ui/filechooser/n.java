package group.pals.android.lib.ui.filechooser;

import android.app.AlertDialog;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import group.pals.android.lib.ui.filechooser.utils.h;

/* compiled from: MyApp */
class n implements TextView.OnEditorActionListener {
    final /* synthetic */ FileChooserActivity a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ AlertDialog c;

    n(FileChooserActivity fileChooserActivity, EditText editText, AlertDialog alertDialog) {
        this.a = fileChooserActivity;
        this.b = editText;
        this.c = alertDialog;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        h.a(this.a, this.b.getWindowToken());
        this.c.getButton(-1).performClick();
        return true;
    }
}
