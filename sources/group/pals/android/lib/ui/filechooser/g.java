package group.pals.android.lib.ui.filechooser;

import android.os.Bundle;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.utils.a.i;

/* compiled from: MyApp */
class g implements i {
    final /* synthetic */ f a;
    private final /* synthetic */ IFile b;
    private final /* synthetic */ Bundle c;

    g(f fVar, IFile iFile, Bundle bundle) {
        this.a = fVar;
        this.b = iFile;
        this.c = bundle;
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.a.i
    public void a(boolean z, Object obj) {
        if (z && this.b != null && this.b.isFile() && (this.a.a.w)) {
            this.a.a.I.setText(this.b.getName());
        }
        if (this.c != null && obj.equals(this.c.get(FileChooserActivity.o))) {
            this.a.a.y.e();
            return;
        }
        this.a.a.y.a((IFile) obj);
        this.a.a.z.a((IFile) obj);
    }
}
