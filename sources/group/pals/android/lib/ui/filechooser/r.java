package group.pals.android.lib.ui.filechooser;

import android.content.Context;
import group.pals.android.lib.ui.filechooser.utils.a.d;
import group.pals.android.lib.ui.filechooser.utils.a.e;
import group.pals.android.lib.ui.filechooser.utils.f;

/* compiled from: MyApp */
class r extends e {
    final /* synthetic */ q a;
    private Thread b;
    private final boolean c;
    private final /* synthetic */ av d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    r(q qVar, Context context, String str, boolean z, av avVar) {
        super(context, str, z);
        this.a = qVar;
        this.d = avVar;
        this.b = f.a(avVar.a(), qVar.a.s, true);
        this.c = avVar.a().isFile();
    }

    private void a() {
        this.a.a.A.b(this.d);
        this.a.a.A.notifyDataSetChanged();
        this.a.a.q();
        FileChooserActivity fileChooserActivity = this.a.a;
        FileChooserActivity fileChooserActivity2 = this.a.a;
        int i = be.afc_pmsg_file_has_been_deleted;
        Object[] objArr = new Object[2];
        objArr[0] = this.c ? this.a.a.getString(be.afc_file) : this.a.a.getString(be.afc_folder);
        objArr[1] = this.d.a().getName();
        d.a(fileChooserActivity, fileChooserActivity2.getString(i, objArr), 0);
    }

    /* access modifiers changed from: protected */
    @Override // group.pals.android.lib.ui.filechooser.utils.a.e
    public void onPreExecute() {
        super.onPreExecute();
        this.b.start();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object doInBackground(Void... voidArr) {
        while (this.b.isAlive()) {
            try {
                this.b.join(10);
            } catch (InterruptedException e) {
                this.b.interrupt();
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // group.pals.android.lib.ui.filechooser.utils.a.e
    public void onCancelled() {
        this.b.interrupt();
        if (this.d.a().exists()) {
            this.a.a.a((FileChooserActivity) this.d);
            d.a(this.a.a, be.afc_msg_cancelled, 0);
        } else {
            a();
        }
        super.onCancelled();
    }

    /* access modifiers changed from: protected */
    @Override // android.os.AsyncTask, group.pals.android.lib.ui.filechooser.utils.a.e
    public void onPostExecute(Object obj) {
        String string;
        super.onPostExecute(obj);
        if (this.d.a().exists()) {
            this.a.a.a((FileChooserActivity) this.d);
            FileChooserActivity fileChooserActivity = this.a.a;
            FileChooserActivity fileChooserActivity2 = this.a.a;
            int i = be.afc_pmsg_cannot_delete_file;
            Object[] objArr = new Object[2];
            if (this.d.a().isFile()) {
                string = this.a.a.getString(be.afc_file);
            } else {
                string = this.a.a.getString(be.afc_folder);
            }
            objArr[0] = string;
            objArr[1] = this.d.a().getName();
            d.a(fileChooserActivity, fileChooserActivity2.getString(i, objArr), 0);
            return;
        }
        a();
    }
}
