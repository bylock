package group.pals.android.lib.ui.filechooser;

import android.widget.ImageView;
import group.pals.android.lib.ui.filechooser.utils.history.History;
import group.pals.android.lib.ui.filechooser.utils.history.b;

/* compiled from: MyApp */
class d implements b {
    final /* synthetic */ FileChooserActivity a;

    d(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.history.b
    public void a(History history) {
        boolean z;
        boolean z2 = true;
        int d = history.d(FileChooserActivity.a(this.a));
        ImageView c = FileChooserActivity.c(this.a);
        if (d > 0) {
            z = true;
        } else {
            z = false;
        }
        c.setEnabled(z);
        ImageView d2 = FileChooserActivity.d(this.a);
        if (d < 0 || d >= history.a() - 1) {
            z2 = false;
        }
        d2.setEnabled(z2);
    }
}
