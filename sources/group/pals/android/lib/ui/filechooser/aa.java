package group.pals.android.lib.ui.filechooser;

import android.view.View;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.d;

/* compiled from: MyApp */
class aa implements View.OnLongClickListener {
    final /* synthetic */ FileChooserActivity a;

    aa(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public boolean onLongClick(View view) {
        if (!d.FilesOnly.equals(FileChooserActivity.f(this.a).c()) && !FileChooserActivity.g(this.a)) {
            FileChooserActivity.a(this.a, new IFile[]{(IFile) view.getTag()});
        }
        return false;
    }
}
