package group.pals.android.lib.ui.filechooser;

import android.view.KeyEvent;
import android.widget.TextView;
import group.pals.android.lib.ui.filechooser.utils.h;

/* compiled from: MyApp */
class ai implements TextView.OnEditorActionListener {
    final /* synthetic */ FileChooserActivity a;

    ai(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        h.a(this.a, FileChooserActivity.h(this.a).getWindowToken());
        FileChooserActivity.i(this.a).performClick();
        return true;
    }
}
