package group.pals.android.lib.ui.filechooser;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import group.pals.android.lib.ui.filechooser.a.a;
import group.pals.android.lib.ui.filechooser.services.f;

/* compiled from: MyApp */
class k implements View.OnClickListener {
    final /* synthetic */ FileChooserActivity a;
    private final /* synthetic */ AlertDialog b;

    k(FileChooserActivity fileChooserActivity, AlertDialog alertDialog) {
        this.a = fileChooserActivity;
        this.b = alertDialog;
    }

    public void onClick(View view) {
        this.b.dismiss();
        FileChooserActivity fileChooserActivity = this.a;
        if (view.getId() == ba.afc_settings_sort_view_button_sort_by_name_asc) {
            a.a(fileChooserActivity, f.SortByName);
            a.a((Context) fileChooserActivity, (Boolean) true);
        } else if (view.getId() == ba.afc_settings_sort_view_button_sort_by_name_desc) {
            a.a(fileChooserActivity, f.SortByName);
            a.a((Context) fileChooserActivity, (Boolean) false);
        } else if (view.getId() == ba.afc_settings_sort_view_button_sort_by_size_asc) {
            a.a(fileChooserActivity, f.SortBySize);
            a.a((Context) fileChooserActivity, (Boolean) true);
        } else if (view.getId() == ba.afc_settings_sort_view_button_sort_by_size_desc) {
            a.a(fileChooserActivity, f.SortBySize);
            a.a((Context) fileChooserActivity, (Boolean) false);
        } else if (view.getId() == ba.afc_settings_sort_view_button_sort_by_date_asc) {
            a.a(fileChooserActivity, f.SortByDate);
            a.a((Context) fileChooserActivity, (Boolean) true);
        } else if (view.getId() == ba.afc_settings_sort_view_button_sort_by_date_desc) {
            a.a(fileChooserActivity, f.SortByDate);
            a.a((Context) fileChooserActivity, (Boolean) false);
        }
        FileChooserActivity.u(this.a);
    }
}
