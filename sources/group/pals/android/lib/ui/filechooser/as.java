package group.pals.android.lib.ui.filechooser;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.io.a;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class as implements a {
    final /* synthetic */ aq a;

    as(aq aqVar) {
        this.a = aqVar;
    }

    @Override // group.pals.android.lib.ui.filechooser.io.a
    public boolean a(IFile iFile) {
        return iFile.isDirectory();
    }
}
