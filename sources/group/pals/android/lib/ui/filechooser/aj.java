package group.pals.android.lib.ui.filechooser;

import android.view.View;
import group.pals.android.lib.ui.filechooser.utils.h;

/* compiled from: MyApp */
class aj implements View.OnClickListener {
    final /* synthetic */ FileChooserActivity a;

    aj(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public void onClick(View view) {
        h.a(this.a, FileChooserActivity.h(this.a).getWindowToken());
        FileChooserActivity.a(this.a, FileChooserActivity.h(this.a).getText().toString().trim());
    }
}
