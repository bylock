package group.pals.android.lib.ui.filechooser;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import group.pals.android.lib.ui.filechooser.services.b;
import group.pals.android.lib.ui.filechooser.services.c;

/* compiled from: MyApp */
class e implements ServiceConnection {
    final /* synthetic */ FileChooserActivity a;

    e(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            FileChooserActivity.a(this.a, ((b) iBinder).a());
        } catch (Throwable th) {
            Log.e(FileChooserActivity.a, "mServiceConnection.onServiceConnected() -> " + th);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        FileChooserActivity.a(this.a, (c) null);
    }
}
