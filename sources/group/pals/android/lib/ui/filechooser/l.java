package group.pals.android.lib.ui.filechooser;

import android.content.Context;
import android.os.Build;
import group.pals.android.lib.ui.filechooser.a.a;
import group.pals.android.lib.ui.filechooser.utils.a.e;

/* compiled from: MyApp */
class l extends e {
    private static /* synthetic */ int[] b;
    final /* synthetic */ FileChooserActivity a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    l(FileChooserActivity fileChooserActivity, Context context, int i, boolean z) {
        super(context, i, z);
        this.a = fileChooserActivity;
    }

    static /* synthetic */ int[] a() {
        int[] iArr = b;
        if (iArr == null) {
            iArr = new int[am.values().length];
            try {
                iArr[am.Grid.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[am.List.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            b = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    @Override // group.pals.android.lib.ui.filechooser.utils.a.e
    public void onPreExecute() {
        super.onPreExecute();
        switch (a()[a.a(this.a).ordinal()]) {
            case 1:
                a.a(this.a, am.Grid);
                break;
            case 2:
                a.a(this.a, am.List);
                break;
        }
        FileChooserActivity.q(this.a);
        if (Build.VERSION.SDK_INT >= 11) {
            group.pals.android.lib.ui.filechooser.utils.a.a(this.a);
        }
        FileChooserActivity.v(this.a);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object doInBackground(Void... voidArr) {
        return null;
    }
}
