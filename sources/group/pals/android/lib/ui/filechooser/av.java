package group.pals.android.lib.ui.filechooser;

import group.pals.android.lib.ui.filechooser.io.IFile;

/* compiled from: MyApp */
public class av {
    private IFile a;
    private boolean b;
    private boolean c;

    public av(IFile iFile) {
        this.a = iFile;
    }

    public IFile a() {
        return this.a;
    }

    public boolean b() {
        return this.b;
    }

    public void a(boolean z) {
        if (this.b != z) {
            this.b = z;
        }
    }

    public boolean c() {
        return this.c;
    }

    public void b(boolean z) {
        if (this.c != z) {
            this.c = z;
        }
    }
}
