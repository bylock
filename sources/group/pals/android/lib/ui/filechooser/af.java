package group.pals.android.lib.ui.filechooser;

import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.utils.a.i;
import group.pals.android.lib.ui.filechooser.utils.history.a;

/* compiled from: MyApp */
class af implements i {
    final /* synthetic */ ae a;

    af(ae aeVar) {
        this.a = aeVar;
    }

    @Override // group.pals.android.lib.ui.filechooser.utils.a.i
    public void a(boolean z, Object obj) {
        this.a.a.y.a((a) new ag(this));
        if (obj instanceof IFile) {
            this.a.a.a((FileChooserActivity) ((IFile) obj), (IFile) new ah(this));
        } else if (this.a.a.y.c()) {
            this.a.a.y.a(this.a.a.p());
            this.a.a.z.a(this.a.a.p());
        }
    }
}
