package group.pals.android.lib.ui.filechooser;

import android.view.View;
import group.pals.android.lib.ui.filechooser.utils.a.j;

/* access modifiers changed from: package-private */
/* compiled from: MyApp */
public class ae implements View.OnLongClickListener {
    final /* synthetic */ FileChooserActivity a;

    ae(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    public boolean onLongClick(View view) {
        j.a(this.a, FileChooserActivity.f(this.a), FileChooserActivity.e(this.a), FileChooserActivity.a(this.a), new af(this));
        return false;
    }
}
