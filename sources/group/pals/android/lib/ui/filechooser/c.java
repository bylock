package group.pals.android.lib.ui.filechooser;

import android.view.View;
import android.widget.AdapterView;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.d;

/* compiled from: MyApp */
class c implements AdapterView.OnItemLongClickListener {
    final /* synthetic */ FileChooserActivity a;

    c(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    @Override // android.widget.AdapterView.OnItemLongClickListener
    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        av a2 = FileChooserActivity.k(this.a).getItem(i);
        if (!FileChooserActivity.l(this.a) && !FileChooserActivity.g(this.a) && !FileChooserActivity.m(this.a) && a2.a().isDirectory() && (d.DirectoriesOnly.equals(FileChooserActivity.f(this.a).c()) || d.FilesAndDirectories.equals(FileChooserActivity.f(this.a).c()))) {
            FileChooserActivity.a(this.a, new IFile[]{a2.a()});
        }
        return true;
    }
}
