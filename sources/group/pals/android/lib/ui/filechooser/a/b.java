package group.pals.android.lib.ui.filechooser.a;

/* compiled from: MyApp */
public class b {
    private boolean a;
    private boolean b;

    public b(boolean z, boolean z2) {
        this.a = z;
        this.b = z2;
    }

    public boolean a() {
        return this.a;
    }

    public b a(boolean z) {
        this.a = z;
        return this;
    }

    public boolean b() {
        return this.b;
    }

    public b b(boolean z) {
        this.b = z;
        return this;
    }
}
