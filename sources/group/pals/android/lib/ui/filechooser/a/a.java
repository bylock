package group.pals.android.lib.ui.filechooser.a;

import android.content.Context;
import group.pals.android.lib.ui.filechooser.am;
import group.pals.android.lib.ui.filechooser.ax;
import group.pals.android.lib.ui.filechooser.bb;
import group.pals.android.lib.ui.filechooser.be;
import group.pals.android.lib.ui.filechooser.services.f;

/* compiled from: MyApp */
public class a extends c {
    public static am a(Context context) {
        return am.List.ordinal() == i(context).getInt(context.getString(be.afc_pkey_display_view_type), context.getResources().getInteger(bb.afc_pkey_display_view_type_def)) ? am.List : am.Grid;
    }

    public static void a(Context context, am amVar) {
        String string = context.getString(be.afc_pkey_display_view_type);
        if (amVar == null) {
            i(context).edit().putInt(string, context.getResources().getInteger(bb.afc_pkey_display_view_type_def)).commit();
        } else {
            i(context).edit().putInt(string, amVar.ordinal()).commit();
        }
    }

    public static f b(Context context) {
        f[] values = f.values();
        for (f fVar : values) {
            if (fVar.ordinal() == i(context).getInt(context.getString(be.afc_pkey_display_sort_type), context.getResources().getInteger(bb.afc_pkey_display_sort_type_def))) {
                return fVar;
            }
        }
        return f.SortByName;
    }

    public static void a(Context context, f fVar) {
        String string = context.getString(be.afc_pkey_display_sort_type);
        if (fVar == null) {
            i(context).edit().putInt(string, context.getResources().getInteger(bb.afc_pkey_display_sort_type_def)).commit();
        } else {
            i(context).edit().putInt(string, fVar.ordinal()).commit();
        }
    }

    public static boolean c(Context context) {
        return i(context).getBoolean(context.getString(be.afc_pkey_display_sort_ascending), context.getResources().getBoolean(ax.afc_pkey_display_sort_ascending_def));
    }

    public static void a(Context context, Boolean bool) {
        if (bool == null) {
            bool = Boolean.valueOf(context.getResources().getBoolean(ax.afc_pkey_display_sort_ascending_def));
        }
        i(context).edit().putBoolean(context.getString(be.afc_pkey_display_sort_ascending), bool.booleanValue()).commit();
    }

    public static boolean d(Context context) {
        return i(context).getBoolean(context.getString(be.afc_pkey_display_show_time_for_old_days_this_year), context.getResources().getBoolean(ax.afc_pkey_display_show_time_for_old_days_this_year_def));
    }

    public static boolean e(Context context) {
        return i(context).getBoolean(context.getString(be.afc_pkey_display_show_time_for_old_days), context.getResources().getBoolean(ax.afc_pkey_display_show_time_for_old_days_def));
    }

    public static boolean f(Context context) {
        return i(context).getBoolean(context.getString(be.afc_pkey_display_remember_last_location), context.getResources().getBoolean(ax.afc_pkey_display_remember_last_location_def));
    }

    public static String g(Context context) {
        return i(context).getString(context.getString(be.afc_pkey_display_last_location), null);
    }

    public static void a(Context context, String str) {
        i(context).edit().putString(context.getString(be.afc_pkey_display_last_location), str).commit();
    }
}
