package group.pals.android.lib.ui.filechooser.a;

import android.content.Context;
import android.content.SharedPreferences;
import group.pals.android.lib.ui.filechooser.be;

/* compiled from: MyApp */
public class c {
    public static final String h(Context context) {
        return String.format("%s_%s", context.getString(be.afc_lib_name), "9795e88b-2ab4-4b81-a548-409091a1e0c6");
    }

    public static SharedPreferences i(Context context) {
        return context.getApplicationContext().getSharedPreferences(h(context), 4);
    }
}
