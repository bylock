package group.pals.android.lib.ui.filechooser;

import android.graphics.Rect;
import android.view.GestureDetector;
import android.view.MotionEvent;
import group.pals.android.lib.ui.filechooser.io.IFile;
import group.pals.android.lib.ui.filechooser.services.d;

/* compiled from: MyApp */
class ab extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ FileChooserActivity a;

    ab(FileChooserActivity fileChooserActivity) {
        this.a = fileChooserActivity;
    }

    private Object a(float f, float f2) {
        int b = b(f, f2);
        if (b >= 0) {
            return FileChooserActivity.j(this.a).getItemAtPosition(b + FileChooserActivity.j(this.a).getFirstVisiblePosition());
        }
        return null;
    }

    private av a(MotionEvent motionEvent) {
        Object a2 = a(motionEvent.getX(), motionEvent.getY());
        if (a2 instanceof av) {
            return (av) a2;
        }
        return null;
    }

    private int b(float f, float f2) {
        Rect rect = new Rect();
        for (int i = 0; i < FileChooserActivity.j(this.a).getChildCount(); i++) {
            FileChooserActivity.j(this.a).getChildAt(i).getHitRect(rect);
            if (rect.contains((int) f, (int) f2)) {
                return i;
            }
        }
        return -1;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return false;
    }

    public boolean onDoubleTap(MotionEvent motionEvent) {
        av a2;
        if (!FileChooserActivity.l(this.a) || FileChooserActivity.m(this.a) || (a2 = a(motionEvent)) == null) {
            return false;
        }
        if (a2.a().isDirectory() && d.FilesOnly.equals(FileChooserActivity.f(this.a).c())) {
            return false;
        }
        if (!FileChooserActivity.g(this.a)) {
            FileChooserActivity.a(this.a, new IFile[]{a2.a()});
        } else if (!a2.a().isFile()) {
            return false;
        } else {
            FileChooserActivity.h(this.a).setText(a2.a().getName());
            FileChooserActivity.a(this.a, a2.a().getName());
        }
        return true;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (Math.abs(motionEvent.getY() - motionEvent2.getY()) >= 19.0f || Math.abs(motionEvent.getX() - motionEvent2.getX()) <= 80.0f || Math.abs(f) <= 200.0f) {
            return false;
        }
        Object a2 = a(motionEvent.getX(), motionEvent.getY());
        if (!(a2 instanceof av)) {
            return false;
        }
        ((av) a2).b(true);
        FileChooserActivity.k(this.a).notifyDataSetChanged();
        FileChooserActivity.b(this.a, (av) a2);
        return false;
    }
}
